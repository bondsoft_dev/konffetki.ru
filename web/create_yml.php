<?
$mysqli = new mysqli('localhost', 'konffetki_soft', 'aN3g6OcUUUZe', 'konffetki_soft');
if ($mysqli->connect_errno) {

    echo "Извините, возникла проблема на сайте";
    echo "Ошибка: Не удалась создать соединение с базой MySQL и вот почему: \n";
    echo "Номер ошибки: " . $mysqli->connect_errno . "\n";
    echo "Ошибка: " . $mysqli->connect_error . "\n";
    
    exit;
}

$arGoods = array();
$arSection = array();

$arLinks = array();
$arPhoto = array();

$list_section = array(
    "337",
    "338",
    "342",
    "343",
    "358",
    "345",

    "348",
    "349",
    "351",
    "363",
    "365",
    "369",
    "371",
    "372",
    "327",
    "306",
    "307",
    "308",
    "309",
    "310",
    "311",
    "312",
    "313",
    "314",
    "325",
    "324",
    "322",
    "323",
    "321",
    "326",
    "327",
);
//ВЫБОРКА СВЯЗЕЙ МЕЖДУ ТОВАРОМ И КАТЕГОРИЕЙ 
$sql = "SELECT * FROM `cl_section` WHERE 1";
$result = $mysqli->query($sql);
while($ar = $result->fetch_assoc()){
    $arLinks[$ar["goods_id"]] = $ar["section_id"];
}


//ВЫБОРКА ФОТОК
$sql = "SELECT * FROM `photogallery_photos` WHERE 1 ORDER BY `id` DESC";
$result = $mysqli->query($sql);
while($ar = $result->fetch_assoc()){
   // var_dump($ar);
    $arPhoto[$ar["album_id"]] = json_decode($ar["images_data"]);
}

//ВЫБОРКА КАТЕГОРИЙ
$sql = "SELECT * FROM `tree_section` WHERE 1";
$result = $mysqli->query($sql);
while($ar = $result->fetch_assoc()){
    if(!in_array($ar["id"],$list_section))
        continue;
    $arSection[$ar["id"]] = array(
        "ID" => $ar["id"],
        "NAME" => $ar["title"],
        "PARENT" => in_array($ar["parent"],$list_section)? $ar["parent"]: "",
        //------ОПЦИОНАЛЬНЫЕ ПОЛЯ, ВАЖНЫ В РАМКАХ ЭТОГО ПРОЕКТА-------
        "PATH" => $ar["alias_path"]
        //------------------------------------------------------------
    );
}



//ВЫБОРКА ТОВАРОВ
$sql = "SELECT * FROM `co_base_card` WHERE active = 1";
$result = $mysqli->query($sql);
while($ar = $result->fetch_assoc()){


    if(isset($arLinks[$ar["id"]])){
        $arGoods[$ar["id"]] = array(
            "ID" => $ar["id"],
            "NAME" => $ar["title"],
            "DESCRIPTION" => $ar["obj_description"],
            "PRICE" => str_replace(".00","",$ar["price"]),
            "PICTURE" =>  "https://www.konffetki.ru".$arPhoto[$ar["gallery"]]->medium->file,
            "URL" => "https://www.konffetki.ru{$arSection[$arLinks[$ar["id"]]]["PATH"]}{$ar["alias"]}/",
            "SECTION" => $arLinks[$ar["id"]]  ,
            "ARTICLE" => $ar["article"], 
        );
    }   
    else{
        //echo "ошибка : {$ar["id"]}--{$ar["title"]}";
    }

}


createTurboYml($arGoods,$arSection);

function createTurboYml($arOffers,$arSection){
    header ("Content-Type:text/xml");
?>
    <yml_catalog date="2020-02-07 10:10:02">
    <shop>
        <name>konffetki.ru</name>
        <company>konffetki.ru</company>
        <url>https://konffetki.ru</url>
        <platform>BSM/Yandex/Market</platform>
        <version>1.1.13</version>
        <currencies>
            <currency id="RUR" rate="1" />
        </currencies>
        <categories>
            <?foreach($arSection as $section):?>
            <category id="<?=$section["ID"]?>" <?if(isset($section["PARENT"]) && $section["PARENT"] != ""):?>parentId="<?=$section["PARENT"]?>"<?endif;?>><?=$section["NAME"]?></category>
            <?endforeach;?>
        </categories>
        <offers>
            <?foreach($arOffers as $offer):?>
            <offer id="<?=$offer["ID"];?>" type="vendor.model" available="true">
                <url>
                    <?=$offer["URL"];?>
                </url>
                <price><?=$offer["PRICE"];?></price>
                <currencyId>RUR</currencyId>
                <categoryId><?=$offer["SECTION"];?></categoryId>
                <picture>
                    <?=$offer["PICTURE"];?>
                </picture>
                <vendor>Konffetki</vendor>
                <model>
                    <?=
                    prepareName($offer["NAME"]);
                    ?>
                </model>
                <description>
                    <![CDATA[
                        <?=$offer["DESCRIPTION"];?>
                    ]]>
                </description>
                <?if($offer["ARTICLE"] != ""):?>
                    <param name="Артикул"><?=$offer["ARTICLE"];?></param>
                <?endif;?>
            </offer>
            <?endforeach;?>
        </offers>
    </shop>
</yml_catalog>
    
    <?
};


function prepareName($name){
    return str_replace("&","&amp;",$name);
}
?>