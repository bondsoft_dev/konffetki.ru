$(document).ready(function() {

    //Каталожный фильтр
    $('.js-mar').on('click', function() {        
        $(this).next('.js-slide').stop(true,true).slideToggle('slow');                                      
        $(this).find('ins').toggleClass('filter__mar-on');      
        
    });
    
    //Топбар
    function showTopBar() {
        var posMenutop = $('.column').position(),
            topBar = $('.js-topbar').height(),
            scrollTop = $(window).scrollTop();

        if (posMenutop.top <= scrollTop) {
            $('.js-topbar').stop(true,false).animate({'top': '0'}, '100');            
        } else {
            $('.js-topbar').stop(true,false).animate({'top': -topBar}, '100');            
        } //if
        };
    showTopBar();
    $(window).scroll(function() {
        showTopBar();
    });


    //Отслеживание целей яндекс метрики

    //Добавление товра в корзину
    $('.catalogbox__btn a').click(function(){
        yaCounter36941965.reachGoal('addToCart');
    });

    //Клик по номеру телефона в шапке
    $('.b-headphone').click(function(){
        yaCounter36941965.reachGoal('topPhoneClick');
    });

    //Успешная регистрация пользователя
    if($('.register-status').hasClass('success')){
        setTimeout( function(){
            yaCounter36941965.reachGoal('registerSendSuccess');
        }, 1000);

    }

    //Кнопка оформить заказ на странице оформления заказа
    $('.makeOrderComplite').click(function(event) {
        yaCounter36941965.reachGoal('clickMakeOrder');
    });

    $("[action='/cart/checkout/']").submit(function(){
        if(document.location.pathname=="/cart/checkout/")
        {
            yaCounter36941965.reachGoal('clickMakeOrder');
        }
    });
    $('[href="/cart/checkout/"]').click(function(){
        if(document.location.pathname=="/cart/")
        {
            yaCounter36941965.reachGoal('clickMakeOrderCart');
        }
    });

});