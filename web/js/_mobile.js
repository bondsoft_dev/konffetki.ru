$(document).ready(function() {
	
	
	$(".logo_m").html( '<div class="b-basketmain b-basketmain-head  ">'+$(".b-basketmain").html()+'</div>' );
	
	// Сформируем меню из того что есть
	$(".b-header .level-1 li").each(function(index, value) {
		
		if( index == 1 ) {
			
		var add = "";
		
		$(".b-menu ul li").each(function(index, value) {
			
			add += "<li><span>"+$(this).html()+"</span></li>";
			
		});
			
		$("#fmm").append( '<li><span><a href="#">Каталог</a></span></span><ul><li><span><a href="/shokoladnye-konfety/">Шоколадные конфеты</a></span></li><li><span><a href="/shokolad-ruchnoi-raboti-sobstvennogo-proizvodstva/">Шоколад ручной работы собственного производства</a></span></li><li><span><a href="/konfety-v-korobkah/">Конфеты в коробках</a></span></li><li><span><a href="/konfety-s-alkogolem/">Конфеты с алкоголем</a></span></li><li><span><a href="/shokolad/">Шоколад</a></span></li><li><span><a href="/poleznye-sladosti/">Полезные сладости без сахара</a></span></li><li><span><a href="/belevskaya-pastila/">Белевская пастила</a></span></li><li><span><a href="/marmelad/">Мармелад</a></span></li><li><span><a href="/vafli/">Вафли</a></span></li><li><span><a href="/halva-pechene-vafli/">Печенье, пряники</a></span></li><li><span><a href="/karamel/">Карамель</a></span></li><li><span><a href="/ledenci1/">Леденцы</a></span></li><li><span><a href="/zukati-varene/">Цукаты, варенье</a></span></li><li><span><a href="/zefir/">Зефир</a></span></li><li><span><a href="/kofe/">Кофе</a></span></li><li><span><a href="/chaj/">Чай</a></span></li><li><span><a href="/upakovka1/">Упаковка</a></span></li></ul></li>' );
		}
		
		$("#fmm").append( "<li>"+$(this).html()+"</li>" );

    });
	
	$(".top-mobile").html( $(".b-topbar .container__content").html() );
	$(".top-mobile .topbar__b1").html( $(".top-mobile .topbar__b1").html() + $(".top-mobile .topbar__b2").html() );
	$(".top-mobile .topbar__b2").hide();

  var mobileMenuObj = $("#for_mobile_meny").clone(); // код на главной странице;

  mobileMenuObj = $("<div id='my-menu'></div>").append(mobileMenuObj);
  $("#body-inner").append(mobileMenuObj);

  var phnMenuWrapper = $('<div class="contact-small"></div>');
  var phnMenu1 = $('<a />', {
    class: 'citi-phone',
    href: "tel:+79671245610",
    text: "+7 (967) 124-56-10"
  });
  phnMenuWrapper.append(phnMenu1);

  var $menu = $('#my-menu');
  $menu.mmenu({
    drag : {
      menu : {
        open: ($.mmenu.support.touch?true:false),
        width: {perc:0.8}
      }
    },
    navbar : {
      title : "<a href=\"/\" class=\"mLogo\">Меню</a></span>",

    },
    "navbars": [  {
        "position": "bottom",
        "content": [
           phnMenuWrapper
        ],
        "height": 1
      }
    ],
    "extensions": [
        "fx-menu-slide",
        "fx-panels-zoom"
    ]
  });
  var api = $("#my-menu").data( "mmenu" );
  $("#open_meny, .mobile-menu__button").click(function(e) {
    api.open();
    e.preventDefault();
  });
  $("#open_meny, .mobile-menu__button").click(function(e) {
    api.close();
    e.preventDefault();
  });
  api.bind('open:finish', function () {
    $('html').addClass('slideout-open');
  });
  api.bind('close:before', function () {
    $('html').removeClass('slideout-open');
  });
  
  
  resize_cart_table();
  
	$(".callback").click(function(){
		setTimeout( function() { resize_cart_table() } , 500);
	});

});


$(window).resize(function () { // пересчитываем во время изменения размеров экрана
	
	resize_cart_table();
	
});

function resize_cart_table(){
		
	if( document.body.clientWidth < 980 ) {

		$(".column__center .cart__total").attr('colspan', 2);
		$(".column__center .cart__table th").eq(3).html('Цена');
		$(".column__center .cart__table th").eq(4).html('Кол-во<br>шт.');
		$(".column__center .cart__table th").eq(6).html('Сумма');
		
		if( $("tr").hasClass("add_line") === false )
		$(".column__center .cart__table tr").each(function(index, value) {
			
			if( index > 0 && $( this ).hasClass("cart__totalrow") === false )
			$( this ).before('<tr class="add_line"><td colspan="4">'+ $( this ).children("td").eq(0).html() +'</td></tr>');
		
		});

	}
	
	if ( document.body.clientWidth > 979 ) {
		
		$(".column__center .cart__total").attr('colspan', 6);
		$(".column__center .cart__table th").eq(4).html('Кол-во');
		
		$(".cart__table .add_line").each(function(index, value) {
		$( this ).remove();
		});
	
	}
	
	if( document.body.clientWidth < 500 ) {
	$(".column__center .cart__table th").eq(0).html('Товар');
	$('.js_cart_remove').each(function(index, value) {
		$( this ).html('X');
	});
	$(".catalogbox__inputbox").css("min-width", "unset"); 
	}
	
	
	
	if( document.body.clientWidth < 980 ) {

		$("#callbackForm .cart__total").attr('colspan', 2);
		$("#callbackForm .cart__table th").eq(2).html('Цена');
		$("#callbackForm .cart__table th").eq(3).html('Кол-во<br>шт.');
		$("#callbackForm .cart__table th").eq(5).html('Сумма');
		
		if( $("tr").hasClass("add_line") === false )
		$("#callbackForm .cart__table tr").each(function(index, value) {
			
			if( index > 0 && $( this ).hasClass("cart__totalrow") === false )
			$( this ).before('<tr class="add_line"><td colspan="3">'+ $( this ).children("td").eq(0).html() +'</td></tr>');
		
		});

	}
	
	if ( document.body.clientWidth > 979 ) {
		
		$("#callbackForm .cart__total").attr('colspan', 6);
		$("#callbackForm .cart__table th").eq(3).html('Кол-во');
	
	}
	
	if( document.body.clientWidth < 500 ) {
	$("#callbackForm .cart__table th").eq(0).html('Товар');
	}

}

