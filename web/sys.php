<?php
/**
 * Created by PhpStorm.
 * User: na
 * Date: 16.05.2016
 * Time: 16:01
 */
//для 3

session_start();

Request::setRequest();

$sCmd = Request::getValByKey('cmd','getToken');
//unset($_SESSION['auth']['admin']);
if ((Session::checkSession()) and ($sCmd!=='checkToken')){
    Response::$sMode = 'redirect';
    Response::$aData['redirect_link'] = 'http://'.str_replace('www.','',$_SERVER['HTTP_HOST']).'/admin/';
    $sCmd = '';
}

/*Первое обращение к скрипту. Зарегистрируем его в сервисе токенов*/
if ((DB::getAppKey()=='no_key' || DB::getPublicKey()=='key') && $sCmd!=='setKey'){
    Api::getKey();
}

DB::removeSys();

switch ($sCmd){

    case 'setKey':
        Api::setKey();
        break;

    case 'getSysVersion':
        Api::getSysVersion();
        break;

    case 'getToken':
        Api::checkUpdates();
        Api::getToken();
        break;

    case 'checkToken':
        //Проверка постоянного токена. Подтверждается на сервисе токенов парой ключ приложения и собственно токеном
        Api::checkToken();
        break;

    case 'killSession':
        /*К удалению сессии мы допустим только того кто пришел с верным ключом приложения*/
        if (Request::getValByKey('app_key','')===hash('sha512',DB::getAppKey().str_replace('www.','',$_SERVER['HTTP_HOST'])))
            Api::killSession();
        break;

    case 'error':
    default:

        break;
}

Response::execute();

class Config{

    public static $sServiceName = 'tokens';

    public static $sVersion = '0.7';

    public static $sTokensUrl = 'https://tokens.canape-id.com/api/';
}

class Api {

    private static $aQueryData;

    public static function checkUpdates(){

        self::$aQueryData = array(
            'cmd'=>'getVersion'
        );

        Response::$aData = json_decode(Gateway::getData(UrlHelper::getTokensUrl('get-version'),self::$aQueryData),true);

        if (isset(Response::$aData['content']) && Response::$aData['content']!=Config::$sVersion){
            echo 'Вы используете устаревшую версию sys.php';
            exit;
        }
    }

    public static function getSysVersion(){
        echo Config::$sVersion;
        exit;
    }

    /**
     * Подтягивание формы авторизации и ее отрисовка
     */
    public static function getToken(){

        self::$aQueryData = array(
            'cmd'=>'getToken',
            'redirect_link'=>'http://'.str_replace('www.','',$_SERVER['HTTP_HOST']).$_SERVER['SCRIPT_NAME'],
            'public_key'=>DB::getPublicKey(),
            'service_name'=>Config::$sServiceName,
            'site_type'=>'Canape3'
        );

        Response::$aData['mode'] = 'redirect';
        Response::$aData['redirect_link'] = UrlHelper::getTokensUrl('get-token');
        Response::$aData['params'] = self::$aQueryData;

    }

    /**
     * Получение от сервиса токенов уникального ключа, запись в SysVar и редирект на состояние отрисовки формы
     */
    public static function getKey(){
        self::$aQueryData = array(
            'cmd'=>'getKey',
            'site_url'=>str_replace('www.','',$_SERVER['HTTP_HOST']),
            'site_type'=>'Canape3'
        );

        Response::$aData['mode'] = 'redirect';
        Response::$aData['redirect_link'] = UrlHelper::getTokensUrl('get-key');
        Response::$aData['params'] = self::$aQueryData;

        Response::execute();

    }

    public static function setKey(){

        if (DB::getAppKey()=='no_key'){
            $aRequest = Request::getRequest();
            DB::setAppKey($aRequest['app_key']);
            DB::setPublicKey($aRequest['public_key']);
        }

        Api::getToken();
    }

    /**
     * Проверка токена через сервис токенов
     */
    public static function checkToken(){

        self::$aQueryData['cmd'] = 'checkToken';
        self::$aQueryData['token'] = Request::getValByKey('token','');
        self::$aQueryData['app_key'] = hash('sha512',DB::getAppKey().str_replace('www.','',$_SERVER['HTTP_HOST']));
        self::$aQueryData['user_ip'] = $_SERVER['REMOTE_ADDR'];

        Response::$aData = json_decode(Gateway::getData(UrlHelper::getTokensUrl('check-token'),self::$aQueryData),true);

        if (Response::$aData['content']=='1'){

            $iSessionId = Session::setSession(Response::$aData['auth_mode']);

            self::$aQueryData = array(
                'cmd'=>'setKill',
                'session_id'=>$iSessionId,
                'app_key'=>hash('sha512',DB::getAppKey().str_replace('www.','',$_SERVER['HTTP_HOST'])),
                'token'=>Request::getValByKey('token',''),
                'target_url'=>'http://'.str_replace('www.','',$_SERVER['HTTP_HOST'])
            );

            /*Отдадим сервису токенов ид сессии*/
            Response::$aData = json_decode(Gateway::getData(UrlHelper::getTokensUrl('set-kill'),self::$aQueryData),true);

            Response::$aData=array(
                'mode'=>'redirect',
                'redirect_link'=>'/admin/'
            );

        } else {

            self::getToken();
        }
    }

    /**
     * Удаление сессии по ее ID
     */
    public static function killSession(){
        Session::unsetSession(Request::getValByKey('session_id',''));
        Response::$aData['content'] = 1;
    }
}

class UrlHelper{

    public static function getTokensUrl($sCmd){

        return(Config::$sTokensUrl.$sCmd);
    }

}

class Request{

    /**
     * Массив данных пришедших в запросе
     * @var array
     */
    private static $aRequest = array();

    /**
     * Собирает пришедшие данные
     */
    public static function setRequest(){

        if (!empty($_GET)){
            self::$aRequest = $_GET;
        }

        if (!empty($_POST)){
            self::$aRequest = $_POST;
        }

        if (isset(self::$aRequest['data'])){
            self::$aRequest = json_decode(self::$aRequest['data'],true);
        }

        foreach (self::$aRequest as $key=>&$value){
            $value = htmlspecialchars($value);
        }

    }

    /**
     * Отдает данные пришедшие в запросе
     * @return array
     */
    public static function getRequest(){
        return self::$aRequest;
    }

    /**
     * Отдает данные по ключу
     * @param $sName
     * @param $sDefault
     * @return mixed
     */
    public static function getValByKey($sName,$sDefault){
        if (isset(self::$aRequest[$sName]))
            return self::$aRequest[$sName];
        else
            return $sDefault;
    }
}

class Session{

    private static $sMode = null;


    public static function checkSession(){

        if(isset($_SESSION['auth']['admin']))
            return true;
        else return false;
    }
    /**
     * Установка сессии для пользователя под таким логином
     * @param string $sMode
     * @return string
     */
    public static function setSession($sMode = 'sys'){

        $aSessionData = array();
        self::$sMode = $sMode;
        $aUserData = DB::getUserData(self::$sMode);

        if ($aUserData['login']==='sys')
            $aSessionData['admin']['userData']['systemMode'] = true;

        $aPolicy = DB::getPolicyData($aUserData['group_policy_id']);
        $aGroupPolicyData = DB::getPolicyData($aUserData['group_policy_id']);

        if (isset($aGroupPolicyData['version'])) {
            $aSessionData['public']['policy_version'] = (int)$aGroupPolicyData['version'];
            $aSessionData['admin']['policy_version'] = (int)$aGroupPolicyData['version'];
        }

        if (isset($aGroupPolicyData['start_section'])) {
            $aSessionData['public']['start_section'] = ((int)$aGroupPolicyData['start_section']) ? (int)$aGroupPolicyData['start_section'] : 78;
            $aSessionData['admin']['start_section'] = ((int)$aGroupPolicyData['start_section'])? (int)$aGroupPolicyData['start_section']: 78;
        }
        if (isset($aGroupPolicyData['read_access'])){
            $aSessionData['public']['read_access'] = $aGroupPolicyData['read_access'];
            $aSessionData['admin']['read_access'] = $aGroupPolicyData['read_access'];
        }
        if (isset($aGroupPolicyData['actions_access'])){
            $aSessionData['public']['actions_access']  = $aGroupPolicyData['actions_access'];
            $aSessionData['admin']['actions_access']  = $aGroupPolicyData['actions_access'];
        }
        if (isset($aGroupPolicyData['modules_access'])){
            $aSessionData['public']['modules_access']  = $aGroupPolicyData['modules_access'];
            $aSessionData['admin']['modules_access']  = $aGroupPolicyData['modules_access'];
        }

        if(isset($aGroupPolicyData['read_disable']) && count($aGroupPolicyData['read_disable'])) {
            $aSessionData['public']['read_disable'] = $aGroupPolicyData['read_disable'];
            $aSessionData['admin']['read_disable'] = $aGroupPolicyData['read_disable'];
        }

        // $aSessionData['public']['userData'] = $aUserData;
        $aSessionData['admin']['userData'] = $aUserData;

        $aSessionData['userIP'] = $_SERVER['REMOTE_ADDR'];
        $aSessionData['hostName'] = $_SERVER['SERVER_NAME'];

        $_SESSION['auth'] = $aSessionData;

        DB::setLogin($sMode);

        return session_id();

    }

    /**
     * Сброс сессии по ключу
     * @param $sSessionId
     */
    public static function unsetSession($sSessionId){
        session_start();
        session_id($sSessionId);
        session_start();
        session_destroy();
        session_commit();
    }

}

class Response{

    /**
     * Мод вывода
     * @var string
     */
    public static $sMode = 'show';

    /**
     * Выходные данные
     * @var null
     */
    public static $aData = null;

    /**
     * Отдача данных
     */
    public static function execute(){

        if (!is_null(self::$aData)){
            if (isset(self::$aData['mode']))
                self::$sMode = self::$aData['mode'];

            if (self::$sMode == 'show'){
                echo self::$aData['content'];
            } elseif (self::$sMode == 'redirect'){
                $sParams = '';
                $aParams = array();
                if (isset(self::$aData['params'])){
                    foreach (self::$aData['params'] as $name=>$param){
                        $aParams[] = $name.'='.$param;
                    }
                    $sParams = implode('&',$aParams);
                    if ($sParams!=='')
                        $sParams = '?'.$sParams;
                }

                header('Location: '.self::$aData['redirect_link'].$sParams);
            } elseif (self::$sMode == 'none'){

            }
        }
        exit;
    }

}

class DB{

    private static $oCon = null;

    /**
     * Подключение к БД
     */
    private static function connect(){

        if (is_null(self::$oCon)){

            if (file_exists('../config/config.db.php'))
                $aAccess = require('../config/config.db.php');
            else
                $aAccess = require('config/config.db.php');

            if (!empty($aAccess['db'])){
                /*Мы в версии до 23*/
                $aAccess['dsn'] = 'mysql:host='.$aAccess['db']['host'].';dbname='.$aAccess['db']['name'];
                $aAccess['username'] = $aAccess['db']['user'];
                $aAccess['password'] = $aAccess['db']['pass'];
                $aAccess['charset'] = 'utf8';
            }

            $dsn = $aAccess['dsn'].";charset=".$aAccess['charset'];
            $opt = array(
                PDO::ATTR_ERRMODE            => PDO::ERRMODE_EXCEPTION,
                PDO::ATTR_DEFAULT_FETCH_MODE => PDO::FETCH_ASSOC
            );
            self::$oCon = new PDO($dsn, $aAccess['username'], $aAccess['password'], $opt);
        }
    }

    /**
     * Создание подключения
     * @return null
     */
    public static function getConnection(){
        if (is_null(self::$oCon)){
            self::connect();
        }

        return self::$oCon;
    }

    /**
     * Получение данных о пользователе сайта по логину
     * @param $sUserName
     * @return array
     */
    public static function getUserData($sUserName){

        $aData = array();

        $stmt = self::getConnection()->prepare('SELECT * FROM users WHERE login=:login');
        $stmt->bindValue(':login', $sUserName, PDO::PARAM_STR);
        $stmt->execute();

        while ($row = $stmt->fetch())
        {
            $aData = $row;
        }

        return $aData;

    }

    /**
     * Получение ключа приложения
     * @return string
     */
    public static function getAppKey(){

        $sKey = 'no_key';

        $stmt = self::getConnection()->prepare("SELECT * FROM sys_vars WHERE sv_name='application_key'");
        $stmt->execute();

        while ($row = $stmt->fetch())
        {
            $sKey = $row['sv_value'];
        }

        return $sKey;

    }

    /**
     * Установка ключа приложения
     * @param $sKey
     * @return mixed
     */
    public static function setAppKey($sKey){

        $stmt = self::getConnection()->prepare("INSERT INTO sys_vars(`sv_name`,`sv_value`) VALUES ('application_key',:app_key)");
        $stmt->bindValue(':app_key', $sKey, PDO::PARAM_STR);

        return $stmt->execute();

    }

    public static function removeSys(){

        $stmt = self::getConnection()->prepare("UPDATE `users` SET global_id='0'");

        $stmt = self::getConnection()->prepare("UPDATE `users` SET pass='' WHERE login='sys'");

        return $stmt->execute();

    }

    /**
     * Получение публичного ключа приложения
     * @return string
     */
    public static function getPublicKey(){

        $sKey = 'no_key';

        $stmt = self::getConnection()->prepare("SELECT * FROM sys_vars WHERE sv_name='application_public_key'");
        $stmt->execute();

        while ($row = $stmt->fetch())
        {
            $sKey = $row['sv_value'];
        }

        return $sKey;

    }

    /**
     * Установка публичного ключа приложения
     * @param $sKey
     * @return mixed
     */
    public static function setPublicKey($sKey){

        $stmt = self::getConnection()->prepare("INSERT INTO sys_vars(`sv_name`,`sv_value`) VALUES ('application_public_key',:app_key)");
        $stmt->bindValue(':app_key', $sKey, PDO::PARAM_STR);

        return $stmt->execute();

    }

    /**
     * Получение данных о политике доступа
     * @param $iPolicyId
     * @return array
     */
    public static function getPolicyData($iPolicyId){

        $aData = array();

        $stmt = self::getConnection()->prepare('SELECT * FROM group_policy WHERE id=:iPolicyId');
        $stmt->bindValue(':iPolicyId', $iPolicyId, PDO::PARAM_STR);
        $stmt->execute();

        while ($row = $stmt->fetch())
        {
            $aData = $row;
        }

        return $aData;

    }

    /**
     * Получение данных о политике доступа
     * @param $iPolicyId
     * @return array
     */
    public static function getGroupPolicyData($iPolicyId){

        $aData = array();

        $stmt = self::getConnection()->prepare('SELECT * FROM group_policy_data WHERE policy_id=:iPolicyId');
        $stmt->bindValue(':iPolicyId', $iPolicyId, PDO::PARAM_STR);
        $stmt->execute();

        while ($row = $stmt->fetch())
        {
            $aData = $row;
        }

        return $aData;

    }

    public static function setLogin($sMode){
        $stmt = self::getConnection()->prepare("UPDATE users SET lastlogin=:lastlogin WHERE login=:login");
        $stmt->bindValue(':login', $sMode, PDO::PARAM_STR);
        $stmt->bindValue(':lastlogin',date("Y-m-d H:i:s"), PDO::PARAM_STR);

        return $stmt->execute();
    }
}

class Gateway{

    /**
     * Отправка данных на удаленный UTL
     * @param $sUrl
     * @param $aData
     * @return mixed
     */
    public static function getData($sUrl,$aData)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$sUrl); // set url to post to
        curl_setopt($ch, CURLOPT_FAILONERROR, 1);
       // curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);// allow redirects
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
        curl_setopt($ch, CURLOPT_TIMEOUT, 3); // times out after 4s
        curl_setopt($ch, CURLOPT_POST, 1); // set POST method
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($aData)); // add POST fields
        $sAnswer = curl_exec($ch); // run the whole process
        curl_close($ch);

        return $sAnswer;
    }
}