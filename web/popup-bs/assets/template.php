<link href="/popup-bs/assets/style-bs.css" type="text/css" rel="stylesheet" />
<style>
    .primary-color span{
        color:  <?=$arConfig["STYLE"]["PRIMARY_COLOR"]?>
    }
    .main-bcg{
        background: <?=$arConfig["STYLE"]["MAIN_BACKGROUND"]?>
    }
</style>
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<div class="main-popup-bs-wrap">

    <div class="popup-bs-padding-wrap">
        <div class="popup-bs-wrap main-bcg">
            <div class="close-btn" onclick="hideModal()">
                <svg version="1.1" id="Capa_1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink"
                    x="0px" y="0px" viewBox="0 0 47.971 47.971" style="enable-background:new 0 0 47.971 47.971;"
                    xml:space="preserve">
                    <g>
                        <path d="M28.228,23.986L47.092,5.122c1.172-1.171,1.172-3.071,0-4.242c-1.172-1.172-3.07-1.172-4.242,0L23.986,19.744L5.121,0.88
    		c-1.172-1.172-3.07-1.172-4.242,0c-1.172,1.171-1.172,3.071,0,4.242l18.865,18.864L0.879,42.85c-1.172,1.171-1.172,3.071,0,4.242
    		C1.465,47.677,2.233,47.97,3,47.97s1.535-0.293,2.121-0.879l18.865-18.864L42.85,47.091c0.586,0.586,1.354,0.879,2.121,0.879
    		s1.535-0.293,2.121-0.879c1.172-1.171,1.172-3.071,0-4.242L28.228,23.986z"></path>
                    </g>
                </svg>
    
            </div>
            <div class="popup-row-bs">
                <div class="popup-col-bs popup-col-img" style="background-image:url(<?=$arConfig["IMAGE"]?>)">
                    <div class="popup-col">
                        <div>
    
                        </div>
                    </div>
                </div>
                <div class="popup-col-bs" >
                    <div class="popup-col" >
                        <div class="title-bs primary-color">
                            <?=$arConfig["TITLE"]?>
                        </div>
                        <div class="subtitle-bs primary-color">
                        <?=$arConfig["SUBTITLE"]?>
                        </div>
                        <div class="phone-bs">
                            <a href="tel:<?=str_replace(array("-"," ","(",")"),"", $arConfig["PHONE"]);?>"><?=$arConfig["PHONE"]?></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="popup-shadow-bs" onclick="hideModal()"></div>
</div>