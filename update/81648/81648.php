<?php

class PatchInstall extends PatchInstallPrototype implements skPatchInterface {

    public $sDescription = 'новые поля для типов доставок';

    public function execute() {

        \Yii::$app->db->createCommand()
            ->addColumn('orders_delivery', 'price', 'integer')
            ->execute();

        \Yii::$app->db->createCommand()
            ->addColumn('orders_delivery','active', 'integer')
            ->execute();
    }
}