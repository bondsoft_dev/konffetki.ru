<?php

class PatchInstall extends PatchInstallPrototype implements skPatchInterface {

    public $sDescription = 'вычищение реестра';

    public function execute() {

        $this->executeSQLQuery(
            "DELETE FROM `registry_storage`
            WHERE `registry_storage`.`name` = 'siteConfig'"
        );

        $this->executeSQLQuery(
            "DELETE FROM `registry_storage`
            WHERE `registry_storage`.`name` LIKE 'build_%'"
        );

    }

}