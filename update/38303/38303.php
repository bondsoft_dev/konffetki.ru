<?php

class PatchInstall extends PatchInstallPrototype implements skPatchInterface {

    public $sDescription = 'доп блок для верстальщиков';

    public function execute() {

        $this->addParameter(
            \Yii::$app->sections->root(),
            'addHead',
            '',
            '',
            '.',
            'Add block in html head'
        );

    }

}