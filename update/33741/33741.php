<?php

class PatchInstall extends PatchInstallPrototype implements skPatchInterface {

    public $sDescription = 'Закрыть возможность выполнения php файлов из нецелевых директорий';

    public $dirList = [
        'assets',
        'export',
        'files',
        'images',
        'sitemap_files'
    ];

    public $sHtaccess = "<Files *.php*>
deny from all
</Files>
    ";

    public function execute() {

        foreach($this->dirList as $sDir){
            if (file_exists(WEBPATH . $sDir) && is_dir(WEBPATH . $sDir)){
                $sFileName = WEBPATH . $sDir . '/.htaccess';

                $f = fopen($sFileName, file_exists($sFileName)?'a+':'w+');
                fwrite($f, $this->sHtaccess);
                fclose($f);

            }
        }

    }

}