<?php

/*

--Запрос данных--
SELECT
`category`,
`message`,
`value`,
`data`
FROM `language_values`
WHERE `language` LIKE 'de'
ORDER BY `category`, `message`

выдать в csv вида строки вида
"adm","add","hinzufügen","0"

--При появлении \" можно выполнить--
UPDATE `language_values`
SET `value` = REPLACE(`value`, '\\"', '"')
WHERE `value` LIKE '%\"%' AND `language`='de'

Текущее положене дел - стирания меток больше нет
Добавляются все метки

на 25 релиз #rel_25
сделаем переназначение override для немецких меток = 0, если значение как в сдлваре

на следующие #rel_25_plus
стирание будет идти только по системным языкам
переназначение немецких только при override=0


 */

use \skewer\build\Component\Installer;
use skewer\build\Component\I18N\models\LanguageValues;

class PatchInstall extends PatchInstallPrototype implements skPatchInterface {

    public $sDescription = 'обновление немецкого словаря';

    private $cDelimiter = ',';

    public $bUpdateCache = true;

    public function execute() {

        $sLanguage = 'de';

        /*
         * Добавляем немецкие метки
         */
        $aLangList = \skewer\build\Component\I18N\Languages::getLanguages();

        // если нет немецкого языка - выходим
        if ( !in_array($sLanguage, $aLangList) )
            return;

        $handle = fopen(__DIR__ . "/de.csv", "r");
        if ( $handle === FALSE )
            $this->fail('Ошибка при открытии файла [de.csv]');

        while (($aData = fgetcsv($handle, null, $this->cDelimiter)) !== FALSE) {

            if ( !$aData )
                continue;

            $sCategory = $aData[0];
            $sMessage = $aData[1];
            $sValue = $aData[2];
            $sData = (int)$aData[3];

            $sValue = str_replace('\"', '"', $sValue);

            /** @var LanguageValues|null $oItem */
            $oItem = LanguageValues::find()
                ->where(['category' => $sCategory])
                ->andWhere(['message' => $sMessage])
                ->andWhere(['language' => $sLanguage])
                ->one()
            ;

            // если запись новая
            if ( !$oItem ) {
                $oItem = new LanguageValues;
                $oItem->category = $sCategory;
                $oItem->message = $sMessage;
                $oItem->language = $sLanguage;
                $oItem->override = 0;
                $oItem->status = 1;
            } else {

                // #rel_25_plus это убрать после 25 - вычисляет измененные значения
                if ( \Yii::$app->language === $sLanguage ) {
                    // для немецких сайтов сохраняем измененные значения
                    if ($oItem->value !== $sValue)
                        $oItem->override = 1;
                    else
                        $oItem->override = 0;
                } else {
                    // для русских сайтов перебиваем все метки
                    $oItem->override = 0;
                }

            }

            // если данные были изменены - не сохраняем
            if ( $oItem->override )
                continue;

            $oItem->data = $sData;
            $oItem->save();

        }

        fclose($handle);

    }

}