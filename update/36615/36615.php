<?php

class PatchInstall extends PatchInstallPrototype implements skPatchInterface {

    public $sDescription = 'Удаление лишней метки таба Вопрос-ответ';

    public function execute() {

        $this->executeSQLQuery(
            "DELETE FROM `language_values`
             WHERE
                `language_values`.`language` = 'ru' AND
                `language_values`.`category` = 'faq' AND
                `language_values`.`message` = 'Page.tab_name'"
        );

    }

}
