<?php

class PatchInstall extends PatchInstallPrototype implements skPatchInterface {

    public $sDescription = 'Добавление типа вывода новостей на главную';

    public function execute() {

        $this->addParameter(\Yii::$app->sections->root(), 'onMainShowType', 'list', '', 'news');

    }

}