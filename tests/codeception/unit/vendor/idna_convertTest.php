<?php

/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 17.08.2015
 * Time: 10:36
 */
class idna_convertTest extends PHPUnit_Framework_TestCase {

    /**
     * @covers idna_convert
     */
    public function testDomain() {

        $idn = new \idna_convert(array('idn_version' => 2008));

        $this->assertSame( 'xn--d1acufc.xn--p1ai', $idn->encode('домен.рф'),
            'не работает замена русских символов' );

        $this->assertSame( 'домен.рф', $idn->decode('xn--d1acufc.xn--p1ai'),
            'не работает восстановление русских символов');

        $this->assertSame( 'domain.ru', $idn->decode('domain.ru'),
            'меняет латинские буквы');

    }

    /**
     * @covers skMailer::convertEmail
     */
    public function testEmail() {

        $idn = new \idna_convert(array('idn_version' => 2008));

        $this->assertSame( 'xn--80a1acny@xn--d1acufc.xn--p1ai', $idn->encode('почта@домен.рф'),
            'не работает замена для email');

    }

}
