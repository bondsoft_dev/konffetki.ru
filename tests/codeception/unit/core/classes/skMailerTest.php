<?php

/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 17.08.2015
 * Time: 10:36
 */
class skMailerTest extends PHPUnit_Framework_TestCase {

    /**
     * @covers skMailer::convertEmail
     */
    public function testEmail() {

        $this->assertSame(
            'qwe@qwe.ew',
            TestHelper::callPrivateMethod( new skMailer, 'convertEmail', ['qwe@qwe.ew'] ),
            'не работает на незаменяемых символах');

        $this->assertSame(
            'xn--80a1acny@xn--d1acufc.xn--p1ai',
            TestHelper::callPrivateMethod( new skMailer, 'convertEmail', ['почта@домен.рф'] ),
            'не работает замена для email');

        $this->assertSame(
            'xn--80a1acny@xn--d1acufc.xn--p1ai,qwe@qwe.qw,' .
            'xn--80a1acny@xn--d1acufc.xn--p1ai',
            TestHelper::callPrivateMethod( new skMailer, 'convertEmail',
                    ['почта@домен.рф, qwe@qwe.qw, почта@домен.рф']),
            'не работает замена по списку');

    }

}
