<?php

/**
 * Created by PhpStorm.
 * User: sam
 * Date: 13.08.15
 * Time: 14:26
 */
class skTwigTest extends PHPUnit_Framework_TestCase
{

    public function testPriceFormat(){

        $obHidePriceFractional = new ReflectionProperty('skTwig', 'bHidePriceFractional');
        $obHidePriceFractional->setAccessible(true);
        $obHidePriceFractional->setValue(false);
        $obHidePriceFractional->setAccessible(true);


        $this->assertEquals( \skTwig::priceFormat('123'), '123.00' );
        $this->assertEquals( \skTwig::priceFormat('1234'), '1 234.00' );
        $this->assertEquals( \skTwig::priceFormat('1234567'), '1 234 567.00' );
        $this->assertEquals( \skTwig::priceFormat('123.123'), '123.12' );
        $this->assertEquals( \skTwig::priceFormat('1234.3'), '1 234.30' );
        $this->assertEquals( \skTwig::priceFormat('1234567.56'), '1 234 567.56' );


        $obHidePriceFractional = new ReflectionProperty('skTwig', 'bHidePriceFractional');
        $obHidePriceFractional->setAccessible(true);
        $obHidePriceFractional->setValue(true);
        $obHidePriceFractional->setAccessible(true);


        $this->assertEquals( \skTwig::priceFormat('123'), '123' );
        $this->assertEquals( \skTwig::priceFormat('1234'), '1 234' );
        $this->assertEquals( \skTwig::priceFormat('1234567'), '1 234 567' );


    }


}
