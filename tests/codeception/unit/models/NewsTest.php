<?php

use skewer\build\Component\Section\Tree;

use skewer\models\News;

/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 11.09.2015
 * Time: 18:20
 */
class NewsTest extends PHPUnit_Framework_TestCase {


    /**
     * Проверка удаления нововстей с разделом
     * @covers skewer\models\News::removeSection
     */
    public function testRemoveSection() {

        $s = Tree::addSection( \Yii::$app->sections->topMenu(), 'news' );

        $n = new News();
        $n->parent_section = $s->id;
        $n->title = 'test';

        $this->assertTrue($n->save(), 'новость не добавилась');

        $this->assertNotEmpty( News::findOne($n->id) );

        $s->delete();

        $this->assertEmpty( News::findOne($n->id) );

    }

}
