<?php

use skewer\build\Component\Section\Tree;

use skewer\build\Page\Articles\Model\Articles;
use skewer\build\Page\Articles\Model\ArticlesRow;

/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 14.09.2015
 * Time: 14:10
 */
class ArticlesTest extends PHPUnit_Framework_TestCase {

    /**
     * Проверка удаления вопросов с разделом
     * @covers skewer\build\Page\Articles\Model\Articles::removeSection
     */
    public function testRemoveSection() {

        $s = Tree::addSection(\Yii::$app->sections->topMenu(), 'articles' );

        $r = new ArticlesRow();
        $r->parent_section = $s->id;

        $this->assertNotEmpty($r->save(), 'запись не добавилась');

        $this->assertNotEmpty( Articles::findOne(['id' => $r->id]) );

        $s->delete();

        $this->assertEmpty( Articles::findOne(['id' => $r->id]) );

    }

}
