<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 12.03.14
 * Time: 10:03
 */

class ModuleTest extends PHPUnit_Framework_TestCase {

    /**
     * @covers       Module::getClassOrExcept
     * @dataProvider providerGetClassOrExcept
     * @param $sClassName
     * @param $sName
     * @param $sLayer
     * @param string $sType
     */
    public function testGetClassOrExcept( $sClassName, $sName, $sLayer, $sType=null ) {
        if ( !$sClassName )
            $this->setExpectedException('\yii\web\ServerErrorHttpException');
        if ( func_num_args()==3 )
            $this->assertSame( $sClassName, Module::getClassOrExcept($sName, $sLayer) );
        else
            $this->assertSame( $sClassName, Module::getClassOrExcept($sName, $sLayer, $sType) );
    }


    public function providerGetClassOrExcept() {
        return array(

            /* namespace + */
            array( 'skewer\build\Page\CatalogViewer\Module', 'CatalogViewer', '' ),
            array( 'skewer\build\Page\CatalogViewer\Module', 'CatalogViewer', Layer::PAGE ),
            array( 'skewer\build\Adm\Catalog\Module', 'Catalog', Layer::ADM ),

            array( 'skewer\build\Adm\Catalog\Install', 'Catalog', Layer::ADM, 'Install' ),

            array( 'skewer\build\Adm\Catalog\Module', 'Adm\Catalog', Layer::ADM ),
            array( 'skewer\build\Page\CatalogViewer\Module', 'Page\CatalogViewer', Layer::PAGE ),

            array( 'skewer\build\LandingPage\Form\Module', 'skewer\build\LandingPage\Form\Module', Layer::LANDING_PAGE ),
            array( 'skewer\build\Adm\Params\Module', 'skewer\build\Adm\Params\Module', Layer::ADM ),

            array( '', 'Page\Catalog', Layer::ADM ),

            array( '', 'skewer\build\LandingPage1\Form\Module', Layer::LANDING_PAGE ),

            array( '\\skewer\\build\\Adm\\Articles\\Module', '\\skewer\\build\\Adm\\Articles\\Module', Layer::ADM ),
            array( '\skewer\build\Cms\Frame\Module', '\skewer\build\Cms\Frame\Module', '' ),

        );
    }


    /**
     * @covers       Module::getClass
     * @dataProvider providerGetClass
     * @param $sClassName
     * @param $sName
     * @param $sLayer
     * @param string $sType
     */
    public function testGetClass( $sClassName, $sName, $sLayer, $sType=null ) {
        if ( func_num_args()==3 )
            $this->assertSame( $sClassName, Module::getClass($sName, $sLayer) );
        else
            $this->assertSame( $sClassName, Module::getClass($sName, $sLayer, $sType) );
    }

    public function providerGetClass() {
        return array(

            /* namespace + */
            array( 'skewer\build\Page\CatalogViewer\Module', 'CatalogViewer', '' ),
            array( 'skewer\build\Page\CatalogViewer\Module', 'CatalogViewer', Layer::PAGE ),
            array( 'skewer\build\Adm\Catalog\Module', 'Catalog', Layer::ADM ),

            array( 'skewer\build\Adm\Catalog\Install', 'Catalog', Layer::ADM, 'Install' ),

            array( 'skewer\build\Adm\Catalog\Module', 'Adm\Catalog', Layer::ADM ),
            array( 'skewer\build\Page\CatalogViewer\Module', 'Page\CatalogViewer', Layer::PAGE ),

            array( 'skewer\build\LandingPage\Form\Module', 'skewer\build\LandingPage\Form\Module', Layer::LANDING_PAGE ),
            array( 'skewer\build\Adm\Params\Module', 'skewer\build\Adm\Params\Module', Layer::ADM ),

            array( 'skewer\build\Page\Catalog\Module', 'Page\Catalog', Layer::ADM ),

            array( 'skewer\build\LandingPage1\Form\Module', 'skewer\build\LandingPage1\Form\Module', Layer::LANDING_PAGE ),

            array( '\\skewer\\build\\Adm\\Articles\\Module', '\\skewer\\build\\Adm\\Articles\\Module', Layer::ADM ),
            array( '\skewer\build\Cms\Frame\Module', '\skewer\build\Cms\Frame\Module', '' ),

        );
    }

}
 