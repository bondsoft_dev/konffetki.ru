<?php

/**
 * Created by PhpStorm.
 * User: ���������
 * Date: 09.07.2015
 * Time: 17:40
 */
class CacheUpdaterTest extends PHPUnit_Framework_TestCase {
    /**
     * Sets up the fixture, for example, open a network connection.
     * This method is called before a test is executed.
     *
     */
    protected function setUp() {
        CacheUpdater::unsetUpdFlag();
    }


    /**
     * �������� ������ ������� ������� ���������� ����������
     */
    public function testFlags() {

        $this->assertFalse( CacheUpdater::hasUpdFlag() );
        $this->assertFalse( CacheUpdater::hasConfigFlag() );
        $this->assertFalse( CacheUpdater::hasCssFlag() );
        $this->assertFalse( CacheUpdater::hasParserFlag() );
        $this->assertFalse( CacheUpdater::hasAssetFlag() );

        CacheUpdater::setUpdFlag( CacheUpdater::type_parser );

        $this->assertTrue( CacheUpdater::hasUpdFlag() );
        $this->assertFalse( CacheUpdater::hasConfigFlag() );
        $this->assertFalse( CacheUpdater::hasCssFlag() );
        $this->assertTrue( CacheUpdater::hasParserFlag() );
        $this->assertFalse( CacheUpdater::hasAssetFlag() );

        CacheUpdater::setUpdFlag( CacheUpdater::type_css );

        $this->assertFalse( CacheUpdater::hasConfigFlag() );
        $this->assertTrue( CacheUpdater::hasCssFlag() );
        $this->assertTrue( CacheUpdater::hasParserFlag() );
        $this->assertFalse( CacheUpdater::hasAssetFlag() );

        CacheUpdater::setUpdFlag( CacheUpdater::type_config );

        $this->assertTrue( CacheUpdater::hasConfigFlag() );
        $this->assertTrue( CacheUpdater::hasCssFlag() );
        $this->assertTrue( CacheUpdater::hasParserFlag() );
        $this->assertFalse( CacheUpdater::hasAssetFlag() );

        CacheUpdater::setUpdFlag( CacheUpdater::type_assets );

        $this->assertTrue( CacheUpdater::hasConfigFlag() );
        $this->assertTrue( CacheUpdater::hasCssFlag() );
        $this->assertTrue( CacheUpdater::hasParserFlag() );
        $this->assertTrue( CacheUpdater::hasAssetFlag() );


    }

}
