<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 27.03.14
 * Time: 10:05
 */

class ImageResizeTest extends PHPUnit_Framework_TestCase {

    /**
     * @covers ImageResize::getSectionIdFromPath
     * @dataProvider providerGetSectionIdFromPath
     */
    public function testGetSectionIdFromPath( $sIn, $iOut ) {
        $this->assertSame( $iOut, ImageResize::getSectionIdFromPath($sIn) );
    }

    /**
     * предоствляет данные для теста метода достающего id радела из имени файла
     * @return array
     */
    public function providerGetSectionIdFromPath() {

        return array(
            array( '/files/123/manul.jpg', 123 ),
            array( '/files/cedas/321/manul.jpg', 321 ),
            array( '/files/cedas/manul.jpg', 0 ),
            array( '/files/cedas/321manul.jpg', 0 ),
        );

    }

}
 