<?php

use skewer\build\Component\Section\Tree;

use skewer\build\Adm\FAQ\ar\FAQRow;
use skewer\build\Adm\FAQ\ar\FAQ;

/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 11.09.2015
 * Time: 18:36
 */
class FAQTest extends PHPUnit_Framework_TestCase {

    /**
     * Проверка удаления вопросов с разделом
     * @covers skewer\models\News::removeSection
     */
    public function testRemoveSection() {

        $s = Tree::addSection( \Yii::$app->sections->topMenu(), 'news' );

        $r = new FAQRow();
        $r->parent = $s->id;

        $this->assertNotEmpty($r->save(), 'вопрос не добавилася');

        $this->assertNotEmpty( FAQ::findOne(['id' => $r->id]) );

        $s->delete();

        $this->assertEmpty( FAQ::findOne(['id' => $r->id]) );

    }

}
