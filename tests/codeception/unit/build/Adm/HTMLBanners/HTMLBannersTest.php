<?php

namespace skewer\build\Adm\HTMLBanners;
/**
 * Created by PhpStorm.
 * User: Тест1
 * Date: 14.08.2015
 * Time: 15:45
 */
use \skewer\build\Adm\HTMLBanners\models\Banners;
use skewer\build\Component\Section\Tree;


class HTMLBannersTest extends \PHPUnit_Framework_TestCase {


    protected function setUp()
    {
        Banners::deleteAll([]);

        parent::setUp(); //
    }


    public function testGetBannersOnInternal(){

        /** Добавим разделы */
        $oTree = Tree::addSection(\Yii::$app->sections->main(), 'test1', \Yii::$app->sections->tplNew());
        $iSection1 = $oTree->id;
        $oTree = Tree::addSection($iSection1, 'test1', \Yii::$app->sections->tplNew());
        $iSection2 = $oTree->id;
        $oTree = Tree::addSection($iSection2, 'test1', \Yii::$app->sections->tplNew());
        $iSection3 = $oTree->id;

        /** Добавим баннеры */
        /*Баннер на все страницы*/
        $oBanner = Banners::getBlankBanner();
        $oBanner->setAttributes([
            'section' => $iSection1,
            'on_main' => 1,
            'on_allpages' => 1,
            'on_include' => 1,
            'sort' => 2
        ]);
        $oBanner->save();
        $iBanner = $oBanner->id;

        $aBanners = Banners::getBanners($iSection1, [], 'left');

        $this->assertTrue( is_array($aBanners) );
        $this->assertEquals( count($aBanners), 1 );

        $this->assertEquals( $aBanners[0]['id'], $iBanner );

        $aBanners = Banners::getBanners($iSection2, [], 'right');
        $this->assertEquals( count($aBanners), 0 );

        $aBanners = array();
        /*Баннер тянем от родительского*/
        $oBanner = Banners::getBlankBanner();
        $oBanner->setAttributes([
            'section' => $iSection1,
            'on_main' => 1,
            'on_allpages' => 0,
            'on_include' => 1,
            'sort' => 1
        ]);
        $oBanner->save();
        $iBanner = $oBanner->id;

        $aBanners = Banners::getBanners($iSection2, [$iSection1], 'left');

        $this->assertTrue( is_array($aBanners) );
        $this->assertEquals( count($aBanners), 2 );

        $this->assertEquals( $aBanners[0]['id'], $iBanner );

        $aBanners = Banners::getBanners($iSection2, [], 'right');
        $this->assertEquals( count($aBanners), 0 );

        $aBanners = Banners::getBanners($iSection3, [$iSection1], 'left');

        $this->assertTrue( is_array($aBanners) );
        $this->assertEquals( count($aBanners), 2 );

        $this->assertEquals( $aBanners[0]['id'], $iBanner );

        $aBanners = Banners::getBanners($iSection3, [], 'right');
        $this->assertEquals( count($aBanners), 0 );

        /*Баннер тянем для всех страниц*/
        $aBanners = array();
        $oBanner = Banners::getBlankBanner();
        $oBanner->setAttributes([
            'section' => $iSection1,
            'on_main' => 0,
            'on_allpages' => 1,
            'on_include' => 0,
            'sort' => 3
        ]);
        $oBanner->save();
        $iBanner = $oBanner->id;

        $aBanners = Banners::getBanners($iSection2, [$iSection1], 'left');

        $this->assertTrue( is_array($aBanners) );
        $this->assertEquals( count($aBanners), 3 );

        $this->assertEquals( $aBanners[2]['id'], $iBanner );

        $aBanners = Banners::getBanners($iSection2, [], 'right');
        $this->assertEquals( count($aBanners), 0 );



    }

}
