<?php
/**
 * Created by PhpStorm.
 * User: ilya
 * Date: 19.03.14
 * Time: 12:45
 */

namespace skewer\build\Adm\Gallery;


use skewer\build\Page\Gallery\ImagesMapper;

class GalleryAdmServiceTest extends \PHPUnit_Framework_TestCase {

    /**
     * @covers skewer\build\Adm\Gallery\Service::findOldSourceImages
     */
    public function testDelOldSource() {

        $this->markTestSkipped('ImagesMapper');

        // добавить запись от сегодня + файл
        $sDir = 'files/gallery_test/';
        $sDirName = WEBPATH.$sDir;
        if ( !is_dir($sDirName) )
            mkdir($sDirName);
        if ( !is_dir($sDirName) )
            $this->fail( "Cannot create [$sDir] folder" );

        $sFileNameToday = $sDir.'today.png';
        $h = fopen(WEBPATH.$sFileNameToday,'w+');
        fclose($h);
        $this->assertFileExists(WEBPATH.$sFileNameToday);

        $aImage = array(
            'source' => $sFileNameToday,
            'creation_date' => date( 'Y-m-d H:i:s' )
        );

        $iTodayId = ImagesMapper::saveItem( $aImage );
        $this->assertNotEmpty($iTodayId);

        $aImage = ImagesMapper::getItem($iTodayId);
        $this->assertNotEmpty($aImage);

        
        
        // от недели назад + файл
        $sFileNameWeek = $sDir.'Week.png';
        $h = fopen(WEBPATH.$sFileNameWeek,'w+');
        fclose($h);
        $this->assertFileExists(WEBPATH.$sFileNameWeek);

        $aImage = array(
            'source' => $sFileNameWeek,
            'creation_date' => date( 'Y-m-d H:i:s', strtotime("-8 days") )
        );

        $iWeekId = ImagesMapper::saveItem( $aImage );
        $this->assertNotEmpty($iWeekId);

        $aImage = ImagesMapper::getItem($iWeekId);
        $this->assertNotEmpty($aImage);



        // от месяца назад + файл
        $sFileNameMonth = $sDir.'Month.png';
        $h = fopen(WEBPATH.$sFileNameMonth,'w+');
        fclose($h);
        $this->assertFileExists(WEBPATH.$sFileNameMonth);

        $aImage = array(
            'source' => $sFileNameMonth,
            'creation_date' => date( 'Y-m-d H:i:s', strtotime("-30 days") )
        );

        $iMonthId = ImagesMapper::saveItem( $aImage );
        $this->assertNotEmpty($iMonthId);

        $aImage = ImagesMapper::getItem($iMonthId);
        $this->assertNotEmpty($aImage);



        // запуск
        Service::findOldSourceImages();



        // сегодняшняя есть
        $this->assertNotEmpty( ImagesMapper::getItem($iTodayId) );
        $this->assertFileExists( WEBPATH.$sFileNameToday );

        // недельной нет
        $this->assertNotEmpty( ImagesMapper::getItem($iWeekId) );
        $this->assertFileNotExists( WEBPATH.$sFileNameWeek );

        // месячной нет
        $this->assertNotEmpty( ImagesMapper::getItem($iMonthId) );
        $this->assertFileNotExists( WEBPATH.$sFileNameMonth );

        unlink( WEBPATH.$sFileNameToday );
        rmdir($sDirName);
        if ( is_dir($sDirName) )
            $this->fail( "Cannot remove [$sDir] folder" );

    }

}
