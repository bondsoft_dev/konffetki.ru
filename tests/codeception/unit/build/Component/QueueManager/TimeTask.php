<?php

namespace tests\build\Component\QueueManager;


use skewer\build\Component\QueueManager;

class TimeTask extends TestTask{

    /** Время задержки на итерацию */
    const time = 20;

    /**
     * Выполнение задачи
     */
    public function execute() {

        $this->iCount++;

        \SysVar::set('TestTask.Param', $this->iCount);

        if ($this->iCount >= 3)
            $this->setStatus(static::stComplete);

        sleep(static::time);

    }

}