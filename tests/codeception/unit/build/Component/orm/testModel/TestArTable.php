<?php
/**
 * Created by JetBrains PhpStorm.
 * User: user
 * Date: 20.06.13
 * Time: 11:47
 * To change this template use File | Settings | File Templates.
 */

namespace tests\build\Component\orm\testModel;

use skewer\build\Component\orm\TablePrototype;
use skewer\build\libs\ft as ft;

/**
 * Class TestArTable
 * @package test\build\libs\ft\testModel
 */
class TestArTable extends TablePrototype {

    /** @var string Имя таблицы */
    protected static $sTableName = 'test_ar';

    protected static function initModel() {

        if ( !ft\Cache::exists( 'test_ar' ) )
            include_once( __DIR__ . '/testModel.php' );

        return ft\Cache::get( 'test_ar', 'tests\\build\\Component\\orm\\testModel' );

    }

    /**
     * Отдает новую запись языка
     * @param array $aData
     * @return TestArRow
     */
    public static function getNewRow( $aData = array() ) {
        return new TestArRow( $aData );
    }


//    /**
//     * Отдает модель текущей сущности
//     * @throws \Exception
//     * @return ft\Model
//     */
//    function getModelObject() {
//
//        if ( !ft\Cache::exists( 'test_ar' ) )
//            include_once( __DIR__ . '/testModel.php' );
//
//        return ft\Cache::get( 'test_ar', 'tests\\build\\libs\\ft\\testModel' );
//
//    }
//
//    /**
//     * Отдает имя таблицы для модели
//     * @return string
//     */
//    function getArClassName() {
//        return 'test\\build\\libs\\ft\\testModel\\TestArRow';
//    }

}