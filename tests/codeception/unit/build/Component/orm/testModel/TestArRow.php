<?php
/**
 * Created by JetBrains PhpStorm.
 * User: User
 * Date: 25.06.13
 * Time: 15:40
 * To change this template use File | Settings | File Templates.
 */

namespace tests\build\Component\orm\testModel;

use skewer\build\Component\orm\ActiveRecord;
use skewer\build\libs\ft\Model;
use skewer\build\libs\ft as ft;

class TestArRow extends ActiveRecord {

    public $id = 0;
    public $a = false;
    public $b = false;
    public $c = false;
    public $date = '';
    public $string = '';
    public $text = '';

    public function getTableName() {
        return 'test_ar';
    }

    public function insert() {
        return $this->save();
    }

    public function update() {
        return $this->save();
    }

}