<?php

namespace test\build\Component\orm;

use skewer\build\Component\orm\TablePrototype;


/**
 * Пример класса наследника для тестиорвания
 * Class TableName
 * @package test\build\Component\orm
 */
class TableName extends TablePrototype {

    protected static $sTableName = 'table_name';

    protected static $aFieldList = array(
        'id' => array(),
        'name' => array(),
        'date' => array(),
        'a' => array(),
        'b' => array(),
        'c' => array()
    );

} 