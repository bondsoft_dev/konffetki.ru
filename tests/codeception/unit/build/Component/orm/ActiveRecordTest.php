<?php
/**
 * Тест на объект запись
 * User: ilya
 * Date: 24.03.14
 * Time: 16:11
 */

namespace skewer\build\Component\orm;

use test\build\Component\orm as test_dir;


class ActiveRecordTest extends \PHPUnit_Framework_TestCase {

    protected function setUp() {

        require_once( __DIR__ . '/TableName.php' );

        Query::SQL(
            "CREATE TABLE IF NOT EXISTS `table_name` (
                      `id` int(11) NOT NULL AUTO_INCREMENT,
                      `name` varchar(255) NOT NULL,
                      `date` date NOT NULL,
                      `a` int(11) NOT NULL,
                      `b` int(11) NOT NULL,
                      `c` int(11) NOT NULL,
                      PRIMARY KEY (`id`)
                    ) ENGINE=MyISAM DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;"
        );

        Query::SQL(
            "INSERT INTO `table_name` (`id`, `name`, `date`, `a`, `b`, `c`) VALUES
                        (1, 'first', '2014-03-12', 1, 1, 1),
                        (2, 'second', '2014-03-13', 1, 0, 0),
                        (3, 'third', '2014-03-19', 1, 1, 0),
                        (4, 'qqq', '2014-03-19', 0, 1, 1);"
        );

    }


    protected function tearDown() {

        Query::SQL( "DROP TABLE `table_name`" );

    }

    protected function getRow() {
        $oRow = test_dir\TableName::find( 1 );
        return $oRow;
    }


    /**
     * Удаление записи
     * @covers \skewer\build\Component\orm\ActiveRecord::delete
     */
    public function testDelete() {

        $oRow = test_dir\TableName::find( 1 );
        $res = $oRow->delete();

        $this->assertSame( true, $res, 'Неверный ответ при удалении записи' );

        $oRow = test_dir\TableName::find( 1 );

        $this->assertSame( false, $oRow, 'не удалилась запись' );
    }


    /**
     * Попытка удаления несуществующей записи
     * @covers skewer\build\Component\orm\ActiveRecord::delete
     */
    public function testDeleteNonExsist() {

        $oRow = test_dir\TableName::find( 1 );
        $oRow->id = 10;
        $res = $oRow->delete();

        $this->assertSame( false, $res, 'Неверный ответ' );
    }

    /**
     * Сохранение новой записи
     * @covers skewer\build\Component\orm\ActiveRecord::save
     */
    public function testSaveNew() {

        $oRow = test_dir\TableName::getNewRow();
        $oRow->name = 'newrow';
        $id = $oRow->save();

        $this->assertSame( 5, $id, 'Неверный ответ функции' );

        $oRow = test_dir\TableName::find( $id );

        $this->assertSame( '5', $oRow->id, 'данные не сохранены' );
        $this->assertSame( 'newrow', $oRow->name, 'данные не сохранены' );

    }


    /**
     * обновление записи
     * @covers skewer\build\Component\orm\ActiveRecord::save
     */
    public function testSaveExsist() {

        $id = 3;
        $oRow = test_dir\TableName::find( $id );
        $oRow->name = 'newname';
        $res = $oRow->save();

        $this->assertSame( $id, $res, 'Неверный ответ функции' );
        $this->assertNotEmpty( $oRow->save(), '0 при повторном сохранении' );
        $this->assertSame( $id, (int)$oRow->id, 'ошибка при сохранении' );
        $this->assertSame( 'newname', $oRow->name, 'ошибка при сохранении' );

        $oRow = test_dir\TableName::find( $id );
        $this->assertSame( $id, (int)$oRow->id, 'данные не сохранены' );
        $this->assertSame( 'newname', $oRow->name, 'данные не сохранены' );
    }


    /**
     * обновление записи
     * @covers skewer\build\Component\orm\ActiveRecord
     */
    public function testSaveNewWithId() {

        $oRow = test_dir\TableName::getNewRow();
        $oRow->id = 10;
        $oRow->name = 'newrow';
        $id = $oRow->save();

        $this->assertSame( 10, $id, 'Неверный ответ функции' );

        $oRow = test_dir\TableName::find( $id );

        $this->assertSame( 10, (int)$oRow->id, 'данные не сохранены' );
        $this->assertSame( 'newrow', $oRow->name, 'данные не сохранены' );

    }


}
 