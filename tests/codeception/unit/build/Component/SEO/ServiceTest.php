<?php
/**
 * Created by PhpStorm.
 * User: user
 * Date: 20.02.2015
 * Time: 13:25
 */

namespace skewer\build\Component\SEO;

use skewer\build\Component\Search;
use skewer\build\Adm\Tree;
use skewer\build\Component\Section;

class ServiceTest extends \PHPUnit_Framework_TestCase {

    /**
     * @var Service
     */
    protected $object;

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp()
    {
        $this->object = new Service();
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown()
    {
    }
    /**
     * @covers skewer\build\Component\SEO\Service::rebuildSearchIndex
     */
    public function testRebuildSearchIndex(){

        $this->assertTrue(is_array(Search\Selector::create()->searchText('Новый раздел')->find()));//если есть всё норм

        $this->object->rebuildSearchIndex();

        Section\Tree::addSection( 70, 'Новый', 7, 'dsfsdfsdf' );

        $aFound = Search\Selector::create()->searchText('Новый раздел')->find();
        $this->assertArrayHasKey('count', $aFound);

        $this->assertSame(0, $aFound['count']);

    }
    /**
     * @covers skewer\build\Component\SEO\Service::makeSearchIndex
     */
    public function testMakeSearchIndex(){
       /* for($i = 1; $i <= 1000; $i++){
            \Tree::addSection(70,'Новый111111111',7,$i);
        }*/




        $this->object->rebuildSearchIndex();
        $this->object->makeSearchIndex();
        $this->assertTrue(is_array(Search\Selector::create()->searchText('Новый раздел')->find()),'fail');
        $this->assertInternalType('integer', $this->object->makeSearchIndex());

    }

    /**
     * @covers skewer\build\Component\SEO\Service::updateSearchIndex
     */

    public function testUpdateSearchIndex() {

       // $this->assertInternalType('integer', $this->object->updateSearchIndex());

    }
    /**
     * @covers skewer\build\Component\SEO\Service::updateSiteMap
     * @todo   testUpdateSiteMap().
     */
    public function testUpdateSiteMap() {
//        $this->assertInternalType('integer', $this->object->updateSiteMap());
    }

    /**
     * @covers skewer\build\Component\SEO\Service::makeSiteMap
     * @todo   testMakeSiteMap().
     */
    public function testMakeSiteMap(){
//        $this->assertInternalType('integer', $this->object->MakeSiteMap());
    }


    /**
     * @covers skewer\build\Component\SEO\Service::setNewDomainToSiteMap
     * @todo   testSetNewDomainToSiteMap().
     */
    public function testSetNewDomainToSiteMap(){
//        $this->assertInternalType('boolean', $this->object->SetNewDomainToSiteMap());
    }
    /**
     * @covers skewer\build\Component\SEO\Service::updateRobotsTxt
     * @todo   testUpdateRobotsTxt().
     */
    public function testUpdateRobotsTxt(){
//        $this->assertInternalType('boolean', $this->object->UpdateRobotsTxt('adawdwaaw'));
    }
    /**
     * @covers skewer\build\Component\SEO\Service::getPageList
     * @todo   testGetPageList().
     */
 /*   public function testGetPageListPrivate(){
        $this->object->rebuildSearchIndex();

        \Tree::addSection(70,'Новый111111111',7,'2342');
        $this->object->makeSearchIndex();
        $this->assertTrue(is_array(Search\Api::executeSearch('Новый раздел')),'fail');

        $this->assertInternalType('integer', $this->object->makeSearchIndex());

        for($i = 1; $i <= 1000; $i++) {
            $this->assertTrue(is_array($this->object->GetPageListPrivate($i)), 'Нифига');
        }
        $this->assertTrue(is_array($this->object->GetPageListPrivate(array('12324','dfsdf','1.4'))), 'Нифига');
    }*/



}
