<?php

namespace skewer\build\Component\Catalog;


class CardTest extends \PHPUnit_Framework_TestCase {

    protected function setUp() {

    }


    protected function setDown() {

    }


    public function testGetDictionaries() {

        $list = Card::getDictionaries();

        foreach ( $list as $oCard )
            $this->assertSame( (int)$oCard->type, Card::TypeDictionary );

    }


    public function testGetGoodsCards() {

        $list = Card::getGoodsCards();

        foreach ( $list as $oCard )
            $this->assertSame( (int)$oCard->type, Card::TypeExtended );

        $list = Card::getGoodsCards( true );

        foreach ( $list as $oCard )
            $this->assertNotSame( (int)$oCard->type, Card::TypeDictionary );

    }

}
