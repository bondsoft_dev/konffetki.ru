<?php

namespace skewer\build\Component\Catalog;

use skewer\build\Component\orm\Query;


class GoodsRowTest extends \PHPUnit_Framework_TestCase {

    const CARD = 'test_card';
    const FIELD = 'test_field';
    const VALUE = 'test_value';
    const VALUE2 = 'test_value2';
    const ALIAS = 'test_alias';

    protected function setUp() {

        $this->clearAllData();
        $this->createCard();

    }


    protected function tearDown() {

        if ( $oCard = Card::get( self::CARD ) )
            $oCard->delete();

        if ( $oCard = Card::get( Card::DEF_BASE_CARD ) )
            $oCard->delete();

    }


    public function createCard() {

        $base = Generator::genBaseCard();

        $card = Generator::createExtCard( $base, self::CARD );
        Generator::createField(
            $card->id,
            [
                'name' => self::FIELD,
                'editor' => 'string',
            ]
        );
        $card->updCache();


    }

    protected function clearAllData() {
        Query::truncateTable( 'c_entity' );
        Query::truncateTable( 'c_field' );
        Query::truncateTable( 'c_validator' );
        Query::truncateTable( 'c_field_attr' );
        Query::truncateTable( 'c_field_group' );
    }


    /**
     * @covers \skewer\build\Component\Catalog\GoodsRow::get
     */
    public function testGet() {

        $oGoods = GoodsRow::create( self::CARD );

        $oGoods->setData( [
            'title' => 'название123',
            'alias' => self::ALIAS,
            self::FIELD => self::VALUE
        ] );
        $oGoods->save();

        // get
        $oTestGoods = GoodsRow::get( $oGoods->getRowId(), self::CARD );

        $this->assertNotEmpty( $oTestGoods );
        $this->assertSame( $oTestGoods->getRowId(), $oGoods->getRowId() );
        //$this->assertSame( $oTestGoods->getFields(), $oGoods->getFields() );

        // getByAlias
        $oTestGoods = GoodsRow::getByAlias( self::ALIAS, Card::DEF_BASE_CARD );

        $this->assertNotEmpty( $oTestGoods );
        $this->assertSame( $oTestGoods->getRowId(), $oGoods->getRowId() );
        //$this->assertSame( $oTestGoods->getFields(), $oGoods->getFields() );

        // getByFields
        $oTestGoods = GoodsRow::getByFields( [self::FIELD => self::VALUE], self::CARD  );

        $this->assertNotEmpty( $oTestGoods );
        $this->assertSame( $oTestGoods->getRowId(), $oGoods->getRowId() );
        //$this->assertSame( $oTestGoods->getFields(), $oGoods->getFields() );


    }


    /**
     * @covers \skewer\build\Component\Catalog\GoodsRow::getData
     */
    public function testData() {

        $oGoods = GoodsRow::create( self::CARD );

        $oGoods->setData( ['alias' => self::ALIAS, self::FIELD => self::VALUE] );
        $oGoods->save();

        $aData = $oGoods->getData();

        $this->assertNotEmpty( $aData );

        $this->assertSame( $aData['alias'], self::ALIAS );
        $this->assertSame( $aData[self::FIELD], self::VALUE );

        $oGoods->setData( [self::FIELD => self::VALUE2] );
        $oGoods->save();

        $aData = $oGoods->getData();

        $this->assertSame( $aData[self::FIELD], self::VALUE2 );

    }


    public function testLinkSections() {

        // setViewSection/getViewSection
    }

    public function testLinkMainSection() {
        // getMainSection/setMainSection
    }


    public function testSave() {

    }


    public function testDelete() {

    }


    public function testAlias() {

    }
}
