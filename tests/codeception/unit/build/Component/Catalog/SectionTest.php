<?php

namespace skewer\build\Component\Catalog;

use skewer\build\Component\orm\Query;
use skewer\build\Component\Catalog\model\GoodsTable;
use skewer\build\Component\Catalog\model\SectionTable;
use skewer\build\Component\Section\Tree;


/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 14.09.2015
 * Time: 14:38
 */
class SectionTest extends \PHPUnit_Framework_TestCase {

    const CARD = 'test_card';
    const FIELD = 'test_field';
    const VALUE = 'test_value';
    const VALUE2 = 'test_value2';
    const ALIAS = 'test_alias';

    protected function setUp() {

        $this->clearAllData();
        $this->createCard();

    }


    protected function tearDown() {

        if ( $oCard = Card::get( self::CARD ) )
            $oCard->delete();

        if ( $oCard = Card::get( Card::DEF_BASE_CARD ) )
            $oCard->delete();

    }


    public function createCard() {

        $base = Generator::genBaseCard();

        $card = Generator::createExtCard( $base, self::CARD );
        Generator::createField(
            $card->id,
            [
                'name' => self::FIELD,
                'editor' => 'string',
            ]
        );
        $card->updCache();


    }

    protected function clearAllData() {
        Query::truncateTable( 'c_entity' );
        Query::truncateTable( 'c_field' );
        Query::truncateTable( 'c_validator' );
        Query::truncateTable( 'c_field_attr' );
        Query::truncateTable( 'c_field_group' );
    }


    /**
     * Проверка удаления товаров с разделом
     * @covers skewer\build\Component\Catalog\GoodsRow::removeSection
     */
    public function testRemove() {

        // создаем раздел
        $s = Tree::addSection( \Yii::$app->sections->topMenu(), 'news' );

        // добавляем товар и привязываем к разделу
        $g = GoodsRow::create(self::CARD);
        $g->setData( [
            'title' => 'название123',
            'alias' => self::ALIAS,
            self::FIELD => self::VALUE
        ] );
        $g->setMainSection($s->id);

        $this->assertNotEmpty($g->save(), 'товар не добавилася');

        $g->setViewSection( [$s->id] );

        // проверяем, что данные добавлены
        $this->assertNotEmpty( SectionTable::findOne(
            [
                'section_id' => $s->id,
                'goods_id' => $g->getRowId()
            ]) );
        $this->assertNotEmpty( GoodsTable::findOne(['base_id' => $g->getRowId()]) );

        // удаляем раздел
        $s->delete();

        // проверяем, что данные тоже стерлись
        $this->assertEmpty( SectionTable::findOne(
            [
                'section_id' => $s->id,
                'goods_id' => $g->getRowId()
            ]) );
        $this->assertEmpty( GoodsTable::findOne(['section' => $s->id]) );

    }

}
