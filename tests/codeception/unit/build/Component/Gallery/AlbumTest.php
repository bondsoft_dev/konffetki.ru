<?php

use skewer\build\Component\Section\Tree;

use skewer\build\Component\Gallery;

/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 14.09.2015
 * Time: 16:21
 */
class AlbumTest extends PHPUnit_Framework_TestCase {

    /**
     * Проверка удаления галереи с разделом
     * @covers skewer\build\Component\Gallery\Album::removeSection
     */
    public function testRemoveSection() {

        $s = Tree::addSection( \Yii::$app->sections->topMenu(), 'gallery' );

        $r = new Gallery\models\Albums();
        $r->profile_id = 7;
        $r->visible = 1;
        $r->section_id = $s->id;

        $this->assertNotEmpty($r->save(), 'галерея не добавилась');

        $this->assertNotEmpty( Gallery\Album::getBySection($s->id, false) );

        $s->delete();

        $this->assertEmpty( Gallery\Album::getBySection($s->id) );

    }

}
