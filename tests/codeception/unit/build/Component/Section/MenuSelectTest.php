<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 26.06.2015
 * Time: 13:56
 */

namespace skewer\build\Component\Section;



class MenuSelectTest extends \PHPUnit_Framework_TestCase {

    private $root_id;
    private $ids = [];

    protected function setUp() {

        $root = Tree::addSection( \Yii::$app->sections->main(), 'root' );

        $this->root_id = $root->id;

        $ids = [];

        foreach( ['1', '2', '3', '4'] as $level1 ) {

            $l1 =  Tree::addSection( $root->id, 's'.$level1 );
            $ids[$l1->title] = $l1->id;
            foreach( ['1', '2', '3', '4'] as $level2 ) {

                $l2 =  Tree::addSection( $l1->id, $l1->title.$level2 );
                $ids[$l2->title] = $l2->id;
                foreach( ['1', '2', '3', '4'] as $level3 ) {

                    $l3 =  Tree::addSection( $l2->id, $l2->title.$level3 );
                    $ids[$l3->title] = $l3->id;
                    foreach( ['1', '2', '3', '4'] as $level4 ) {

                        $l4 = Tree::addSection( $l3->id, $l3->title.$level4 );
                        $ids[$l4->title] = $l4->id;

                    }
                }
            }
        }


        \Policy::incPolicyVersion();
        \Policy::updateCache( \CurrentUser::getPolicyId() );
        \CurrentUser::reloadPolicy();
        Tree::dropCache();

        $this->ids = $ids;

    }

    protected function tearDown() {

        Tree::removeSection($this->root_id);

    }

    private function getFromTree( $name, $aTree ) {

        $l1 = isset($name[1]) ? ((int)$name[1]) - 1 : 0;
        $l2 = isset($name[2]) ? (int)$name[2] - 1 : 0;
        $l3 = isset($name[3]) ? (int)$name[3] - 1 : 0;
        $l4 = isset($name[4]) ? (int)$name[4] - 1 : 0;

        switch (strlen( $name )) {
            case 2:
                if ( isset($aTree[$l1]) )
                    return $aTree[$l1];
                else
                    return [];
                break;

            case 3:
                //var_dump( $aTree[$l1] );
                if ( isset($aTree[$l1]['items'][$l2]) )
                    return $aTree[$l1]['items'][$l2];
                else
                    return [];
                break;

            case 4:
                if ( isset($aTree[$l1]['items'][$l2]['items'][$l3]) )
                    return $aTree[$l1]['items'][$l2]['items'][$l3];
                else
                    return [];
                break;

            case 5:
                if ( isset($aTree[$l1]['items'][$l2]['items'][$l3]['items'][$l4]) )
                    return $aTree[$l1]['items'][$l2]['items'][$l3]['items'][$l4];
                else
                    return [];
                break;

            default:
                $this->fail("Кривое имя[$name]");

        }

        return [];

    }

    /**
     * Проверка запроса на 1 урорвень
     * @covers \skewer\build\Component\Section\Tree::getUserSectionTree
     */
    public function testNormal() {

        $t = Tree::getUserSectionTree( $this->root_id, 0, 1 );

        $this->assertNotEmpty( $this->getFromTree('s1', $t) );
        $this->assertNotEmpty( $this->getFromTree('s2', $t) );
        $this->assertNotEmpty( $this->getFromTree('s3', $t) );
        $this->assertNotEmpty( $this->getFromTree('s4', $t) );

        $this->assertEmpty( $this->getFromTree('s1234', $t) );
        $this->assertEmpty( $this->getFromTree('s11', $t) );
        $this->assertEmpty( $this->getFromTree('s111', $t) );

    }

    /**
     * Проверка запроса на 2 урорвеня
     * @covers \skewer\build\Component\Section\Tree::getUserSectionTree
     */
    public function test2Level() {

        $t = Tree::getUserSectionTree( $this->root_id, 0, 2 );

        $this->assertNotEmpty( $this->getFromTree('s1', $t) );
        $this->assertNotEmpty( $this->getFromTree('s2', $t) );
        $this->assertNotEmpty( $this->getFromTree('s3', $t) );
        $this->assertNotEmpty( $this->getFromTree('s4', $t) );

        $this->assertNotEmpty( $this->getFromTree('s11', $t) );
        $this->assertNotEmpty( $this->getFromTree('s23', $t) );
        $this->assertNotEmpty( $this->getFromTree('s34', $t) );
        $this->assertNotEmpty( $this->getFromTree('s41', $t) );

        $this->assertEmpty( $this->getFromTree('s1234', $t) );
        $this->assertEmpty( $this->getFromTree('s111', $t) );

    }

    /**
     * Проверка запроса на 4 урорвеня
     * @covers \skewer\build\Component\Section\Tree::getUserSectionTree
     */
    public function test4Level() {

        $t = Tree::getUserSectionTree( $this->root_id, 0, 4 );

        $this->assertNotEmpty( $this->getFromTree('s1', $t) );

        $this->assertNotEmpty( $this->getFromTree('s1234', $t) );
        $this->assertNotEmpty( $this->getFromTree('s111', $t) );

    }
    /**
     * Проверка запроса на 1 урорвень с открытой веткой
     * @covers \skewer\build\Component\Section\Tree::getUserSectionTree
     */
    public function test1LevelWith() {

        $t = Tree::getUserSectionTree( $this->root_id, $this->ids['s1234'], 1 );

        $this->assertNotEmpty( $this->getFromTree('s1', $t) );
        $this->assertNotEmpty( $this->getFromTree('s2', $t) );

        // 3 ветка отсутствует
        $this->assertEmpty( $this->getFromTree('s31', $t) );

        // целевая присутствует
        $this->assertNotEmpty( $this->getFromTree('s1234', $t) );

    }

}
