<?php

namespace tests\codeception\unit\build\Component\Section\Params;

use skewer\build\Component\Section;

/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 18.12.2015
 * Time: 13:59
 */
class ListSelectorTest extends \PHPUnit_Framework_TestCase {

    /**
     * Проверка запроса наследуемых по родителю (20 Type::paramInherit) записей
     * @covers skewer\build\Component\Section\Params\ListSelector::get
     */
    public function testInheritGet() {

        $this->markTestIncomplete('inherit param');

        // добавляем 3 вложенных раздела
        $s1 = Section\Tree::addSection(0, 'Section1' );
        $this->assertNotEmpty($s1);

        $s2 = Section\Tree::addSection($s1->id, 'Section2' );
        $this->assertNotEmpty($s2);

        $s3 = Section\Tree::addSection($s2->id, 'Section3' );
        $this->assertNotEmpty($s3);

        /*
         * Параметры раздела нижнего уровня Section3
         * 1-5 - наследуемые
         * 6 - реальный
         */
        Section\Parameters::setParams($s3->id, 'rec', 'a1', '', '', '', Section\Params\Type::paramInherit);
        Section\Parameters::setParams($s3->id, 'rec', 'a2', '', '', '', Section\Params\Type::paramInherit);
        Section\Parameters::setParams($s3->id, 'rec', 'a3', '', '', '', Section\Params\Type::paramInherit);
        Section\Parameters::setParams($s3->id, 'rec', 'a4', '', '', '', Section\Params\Type::paramInherit);
        Section\Parameters::setParams($s3->id, 'rec', 'a5', '', '', '', Section\Params\Type::paramSystem);
        Section\Parameters::setParams($s3->id, 'rec', 'a6', 'r3', 'r3_show', '', Section\Params\Type::paramSystem);

        /*
         * Параметры раздела среднего уровня Section2
         * 1 - реальное значение
         * 2 - наследуемый
         */
        Section\Parameters::setParams($s2->id, 'rec', 'a1', 'r2', 'r2_show', '', Section\Params\Type::paramSystem);
        Section\Parameters::setParams($s2->id, 'rec', 'a2', '', '', '', Section\Params\Type::paramInherit);

        /*
         * Параметры раздела верхнего уровня Section1
         * 2 - реальное значение
         * 3 - реальное значение
         * 4 - наследуемый
         */
        Section\Parameters::setParams($s1->id, 'rec', 'a1', 'r1', 'r1_show', '', Section\Params\Type::paramSystem);
        Section\Parameters::setParams($s1->id, 'rec', 'a2', 'r12', 'r12_show', '', Section\Params\Type::paramSystem);
        Section\Parameters::setParams($s1->id, 'rec', 'a3', 'r1', 'r1_show', '', Section\Params\Type::paramSystem);
        Section\Parameters::setParams($s1->id, 'rec', 'a4', '', '', '', Section\Params\Type::paramInherit);

        // запрос данных
        $aParams = Section\Parameters::getList( $s3->id )->rec()->asArray()->get();

        // ркскладка по массивам
        $aData = [];
        $aShowData = [];
        foreach ( $aParams as $aRow ) {
            $aData[$aRow['name']] = $aRow['value'];
            $aShowData[$aRow['name']] = $aRow['show_val'];
        }

        /*
         * Проверка корректности сборки
         * 1-3 - унаследованы со значениями
         * 4-5 - не нашлось значения в родителях
         * 6 - реальный
         */
        $this->assertSame('r2', $aData['a1']);      // берется из 2, но есть и в 1
        $this->assertSame('r12', $aData['a2']);     // в 2 тоже наследуется, берется из 1
        $this->assertSame('r1', $aData['a3']);      // берется из 1
        $this->assertSame('', $aData['a4']);        // не нашлось, наследуется и верхнем уровне
        $this->assertSame('', $aData['a5']);        // вообще не встречается, кроме как в 3
        $this->assertSame('r3', $aData['a6']);      // задан еще в 3

        // todo show_val -//-

    }

}