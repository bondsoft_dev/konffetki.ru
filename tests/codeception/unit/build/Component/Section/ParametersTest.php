<?php

namespace tests\build\Component\Section;


use skewer\build\Component\orm\Query;
use skewer\build\Component\Section\Parameters;
use skewer\build\Component\Section\Params\ListSelector;
use skewer\build\Component\Section\Tree;
use skewer\models\Parameters as Params;
use yii\helpers\ArrayHelper;

/**
 * Тест на параметры
 * Class ParametersTest
 * @package tests\build\Component\Section
 */
class ParametersTest extends \PHPUnit_Framework_TestCase {

    protected function setUp() {

        Params::deleteAll();

        parent::setUp();
    }


    /**
     * Создание - сохранение
     * @covers skewer\build\Component\Section\Parameters::createParam
     */
    public function testSave(){

        $oParamNew = Parameters::createParam([
            'parent' => 4, 'name' => 'name', 'group' => 'test',
            'title' => '12', 'value' => 2, 'show_val' => 'text',
            'access_level' => 3, 'skewer' => '123'
        ]);

        $this->assertEquals( $oParamNew->parent, 4 );
        $this->assertEquals( $oParamNew->name, 'name' );
        $this->assertEquals( $oParamNew->group, 'test' );
        $this->assertEquals( $oParamNew->title, '12' );
        $this->assertEquals( $oParamNew->value, 2 );
        $this->assertEquals( $oParamNew->show_val, 'text' );
        $this->assertEquals( $oParamNew->access_level, 3 );

        $this->assertObjectNotHasAttribute( 'skewer', $oParamNew );

        $oParamNew->save();

        $aRow = Query::SelectFrom('parameters')->getOne();

        $this->assertEquals( $oParamNew->parent, $aRow['parent'] );
        $this->assertEquals( $oParamNew->name, $aRow['name'] );
        $this->assertEquals( $oParamNew->group, $aRow['group'] );
        $this->assertEquals( $oParamNew->title, $aRow['title'] );
        $this->assertEquals( $oParamNew->value, $aRow['value'] );
        $this->assertEquals( $oParamNew->show_val, $aRow['show_val'] );
        $this->assertEquals( $oParamNew->access_level, $aRow['access_level'] );

        /** Без группы */
        $oParamNew = Parameters::createParam([
            'parent' => 4, 'name' => 'name',
            'title' => '12', 'value' => 23, 'show_val' => 'text1111111111',
            'access_level' => 10, 'skewer' => '123'
        ]);
        $this->assertFalse($oParamNew->save());

        /** Повтор раздела-группы-имени */
        $oParamNew = Parameters::createParam([
            'parent' => 4, 'name' => 'name', 'group' => 'test',
            'title' => '12', 'value' => 23, 'show_val' => 'text1111111111',
            'access_level' => 10, 'skewer' => '123'
        ]);
        $this->assertFalse($oParamNew->save());

        /** Повтор раздела-группы-имени */
        $oParamNew = Parameters::createParam([
            'parent' => 4, 'name' => 'name', 'group' => 'test222',
            'title' => '12', 'value' => 23, 'show_val' => 'text1111111111',
            'access_level' => 10, 'skewer' => '123'
        ]);
        $this->assertTrue($oParamNew->save());

        /** Все правила валидации проверять не буду, доверимся Yii */
    }


    /**
     * Проверка на циклы шаблонов
     * @covers skewer\build\Component\Section\Params\TemplateValidator::validateValue
     * @covers skewer\build\Component\Section\Params\TemplateValidator::validateAttributes
     * @expectedException \yii\base\InvalidConfigException
     */
    public function testErrorTpl1(){
        Parameters::setParams( 5, Parameters::settings, Parameters::template, 5 );
    }


    /**
     * Проверка на циклы шаблонов
     * @covers skewer\build\Component\Section\Params\TemplateValidator::validateValue
     * @covers skewer\build\Component\Section\Params\TemplateValidator::validateAttributes
     * @expectedException \yii\base\InvalidConfigException
     */
    public function testErrorTpl2(){

        $this->assertTrue( Parameters::setParams( 1, Parameters::settings, Parameters::template, 5 ) > 0 );
        $this->assertTrue( Parameters::setParams( 5, Parameters::settings, Parameters::template, 6 ) > 0 );
        $this->assertTrue( Parameters::setParams( 6, Parameters::settings, Parameters::template, 7 ) > 0 );

        /** Тут должна быть ошибка! */
        Parameters::setParams( 7, Parameters::settings, Parameters::template, 1 );
    }

    /**
     * Проверка на циклы шаблонов
     * @covers skewer\build\Component\Section\Params\TemplateValidator::validateValue
     * @covers skewer\build\Component\Section\Params\TemplateValidator::validateAttributes
     * @expectedException \yii\base\InvalidConfigException
     */
    public function testErrorTpl3(){
        Parameters::setParams( 5, Parameters::settings, Parameters::template, -5 );
    }


    /**
     * Проверка на циклы шаблонов
     * @covers skewer\build\Component\Section\Params\TemplateValidator::validateValue
     * @covers skewer\build\Component\Section\Params\TemplateValidator::validateAttributes
     * @expectedException \yii\base\InvalidConfigException
     */
    public function testErrorTpl4(){
        Parameters::setParams( 5, Parameters::settings, Parameters::template, 'template' );
    }


    /**
     * @covers skewer\build\Component\Section\Parameters::getById
     */
    public function testGetById(){

        $oParamNew = Parameters::createParam(['parent' => 1, 'name' => 2, 'group' => 4]);
        $oParamNew->save();

        $id = $oParamNew->id;

        $oParam = Parameters::getById( $id );

        $this->assertTrue( $oParam instanceof Params );
        $this->assertEquals( $oParam->id, $oParamNew->id );
        $this->assertEquals( $oParam->parent, $oParamNew->parent );
        $this->assertEquals( $oParam->name, $oParamNew->name );
        $this->assertEquals( $oParam->group, $oParamNew->group );

        $oParamNew->delete();

        $oParam = Parameters::getById( $id );
        $this->assertNull( $oParam );

        $oParam = Parameters::getById( '123b134k' );
        $this->assertNull( $oParam );

    }


    /**
     * @covers skewer\build\Component\Section\Parameters::getParamByName
     * @covers skewer\build\Component\Section\Parameters::getByName
     */
    public function testGetByName(){

        Parameters::setParams( 5, '4', '2', '1');
        Parameters::setParams( 1, Parameters::settings, Parameters::template, 5 );

        $oParamNew = Parameters::createParam(['parent' => 1, 'name' => 2, 'group' => 4, 'value' => 2]);
        $oParamNew->save();

        $oParam = Parameters::getByName( 1, 4, 2, false );

        $this->assertTrue( $oParam instanceof Params );
        $this->assertEquals( $oParam->id, $oParamNew->id );
        $this->assertEquals( $oParam->parent, $oParamNew->parent );
        $this->assertEquals( $oParam->name, $oParamNew->name );
        $this->assertEquals( $oParam->group, $oParamNew->group );
        $this->assertEquals( $oParam->value, '2' );

        $oParam = Parameters::getByName( 1, 4, 2, true );

        $this->assertTrue( $oParam instanceof Params );
        $this->assertEquals( $oParam->id, $oParamNew->id );
        $this->assertEquals( $oParam->parent, $oParamNew->parent );
        $this->assertEquals( $oParam->name, $oParamNew->name );
        $this->assertEquals( $oParam->group, $oParamNew->group );
        $this->assertEquals( $oParam->value, '2' );

        $oParamNew->delete();

        $oParam = Parameters::getByName( 1, 4, 2 );
        $this->assertFalse( $oParam );

        $oParam = Parameters::getByName( 1, 4, 2, true );

        $this->assertTrue( $oParam instanceof Params );
        $this->assertEquals( $oParam->parent, 5 );
        $this->assertEquals( $oParam->name, '2' );
        $this->assertEquals( $oParam->group, '4' );
        $this->assertEquals( $oParam->value, '1' );

    }


    /**
     * @covers skewer\build\Component\Section\Parameters::getParamByName
     * @covers skewer\build\Component\Section\Parameters::getValByName
     * @covers skewer\build\Component\Section\Parameters::getShowValByName
     */
    public function testGetVal(){

        Parameters::setParams( 5, '5', '2', '1', '111' );
        Parameters::setParams( 4, Parameters::settings, Parameters::template, 5);

        $oParamNew = Parameters::createParam([
            'parent' => 4, 'name' => 2, 'group' => 5,
            'value' => 'test', 'show_val' => 222
        ]);
        $oParamNew->save();

        $this->assertEquals( Parameters::getValByName( 4, 5, 2 ), 'test' );
        $this->assertEquals( Parameters::getShowValByName( 4, 5, 2 ), '222' );

        $this->assertEquals( Parameters::getValByName( 4, 5, 2, true ), 'test' );
        $this->assertEquals( Parameters::getShowValByName( 4, 5, 2, true ), '222' );

        $oParamNew->value = 111111;
        $oParamNew->show_val = 'skewer';
        $oParamNew->save();

        $this->assertEquals( Parameters::getValByName( 4, 5, 2 ), '111111' );
        $this->assertEquals( Parameters::getShowValByName( 4, 5, 2 ), 'skewer' );

        $oParamNew->delete();

        $this->assertFalse( Parameters::getValByName( 4, 5, 2 ) );
        $this->assertFalse( Parameters::getShowValByName( 4, 5, 2 ) );

        $this->assertEquals( Parameters::getValByName( 4, 5, 2, true ), '1' );
        $this->assertEquals( Parameters::getShowValByName( 4, 5, 2, true ), '111' );

    }


    /**
     * @covers skewer\build\Component\Section\Params\ListSelector::group
     * @covers skewer\build\Component\Section\Params\ListSelector::name
     * @covers skewer\build\Component\Section\Params\ListSelector::parent
     * @covers skewer\build\Component\Section\Params\ListSelector::asArray
     * @covers skewer\build\Component\Section\Params\ListSelector::get
     */
    public function testGetListByNameGroup(){

        $aData = [
            [
                'parent' => 4, 'name' => 2, 'group' => 5, 'value' => '123'
            ],
            [
                'parent' => 5, 'name' => 2, 'group' => 5, 'value' => '456'
            ],
            [
                'parent' => 7, 'name' => 2, 'group' => 5, 'value' => '789'
            ],
            [
                'parent' => 7, 'name' => 6, 'group' => 5, 'value' => '156'
            ],
        ];

        foreach( $aData as $aRec ){
            Parameters::createParam($aRec)->save();
        }

        $aData = Parameters::getList()->group(5)->name(2)->asArray()->get();

        $this->assertTrue( is_array($aData) );
        $this->assertEquals( count($aData), 3 );

        $aData = ArrayHelper::map($aData, 'parent', 'value');

        $this->assertEquals( $aData[4], '123' );
        $this->assertEquals( $aData[5], '456' );
        $this->assertEquals( $aData[7], '789' );

        $aData = Parameters::getList()->parent([4, 7])->group(5)->name(2)->asArray()->get();

        $this->assertTrue( is_array($aData) );
        $this->assertEquals( count($aData), 2 );

        $aData = ArrayHelper::map($aData, 'parent', 'value');

        $this->assertEquals( $aData[4], '123' );
        $this->assertEquals( $aData[7], '789' );

    }


    /**
     * @covers skewer\build\Component\Section\Params\ListSelector::group
     * @covers skewer\build\Component\Section\Params\ListSelector::get
     */
    public function testGetListByGroup(){

        $aData = [
            [
                'parent' => 4, 'name' => 'param2', 'group' => 'group1', 'value' => '123'
            ],
            [
                'parent' => 4, 'name' => 'param1', 'group' => 'group1', 'value' => '456'
            ],
            [
                'parent' => 7, 'name' => 2, 'group' => 'group1', 'value' => '789'
            ],
        ];

        foreach( $aData as $aRec ){
            Parameters::createParam($aRec)->save();
        }

        $aParams = Parameters::getList(4)->group('group1')->get();
        $this->assertEquals( count($aParams), 2 );
        foreach($aParams as $oParam){
            $this->assertTrue( $oParam instanceof Params );
            $this->assertEquals( $oParam->group, 'group1' );
            $this->assertEquals( $oParam->parent, 4 );
        }
    }


    /**
     * @covers skewer\build\Component\Section\Params\ListSelector::get
     * @covers skewer\build\Component\Section\Params\ListSelector::name
     */
    public function testGetListByName(){

        $aData = [
            [
                'parent' => 4, 'name' => 'param1', 'group' => 'group1', 'value' => '123'
            ],
            [
                'parent' => 4, 'name' => 'param1', 'group' => 'group2', 'value' => '456'
            ],
            [
                'parent' => 4, 'name' => 'param2', 'group' => 'group2', 'value' => '4567'
            ],
            [
                'parent' => 7, 'name' => 'param1', 'group' => 'group1', 'value' => '789'
            ],
        ];

        foreach( $aData as $aRec ){
            Parameters::createParam($aRec)->save();
        }

        $aParams = Parameters::getList(4)->name('param1')->get();
        $this->assertEquals( count($aParams), 2 );
        foreach($aParams as $oParam){
            $this->assertTrue( $oParam instanceof Params );
            $this->assertEquals( $oParam->name, 'param1' );
            $this->assertEquals( $oParam->parent, 4 );
        }

    }


    /**
     * Тест на выборку по разделам
     * @covers skewer\build\Component\Section\Params\ListSelector::get
     * @covers skewer\build\Component\Section\Params\ListSelector::rec
     * @covers skewer\build\Component\Section\Params\ListSelector::parent
     * @covers skewer\build\Component\Section\Params\ListSelector::groups
     */
    public function testGetByList(){

        /**
         * Шаблоны 5-6-7
         * Разделы 8-9
         */

        Parameters::setParams( 5, 'g1', 'n1', 'v51', 'test', 'title', 0 );
        Parameters::setParams( 5, 'g1', 'n2', 'v52', 'test2', 'title2', -3 );
        Parameters::setParams( 5, 'g1', 'n3', 'v53', 'test3', 'title3', 3 );
        Parameters::setParams( 5, 'g2', 'n4', 'v54' );
        Parameters::setParams( 5, 'g2', 'n5', 'v55' );
        Parameters::setParams( 5, 'g3', 'n6', 'v56' );

        Parameters::setParams( 6, 'g1', 'n1', 'v61' );
        Parameters::setParams( 6, 'g1', 'n2', 'v62', 'test4', 'title4', 4 );
        Parameters::setParams( 6, 'g3', 'n7', 'v63' );
        Parameters::setParams( 6, Parameters::settings, Parameters::template, 5 );

        Parameters::setParams( 7, 'g1', 'n2', 'v71' );
        Parameters::setParams( 7, 'g3', 'n8', 'v72' );
        Parameters::setParams( 7, Parameters::settings, Parameters::template, 6 );

        Parameters::setParams( 8, Parameters::settings, Parameters::template, 7 );

        $aParams = Parameters::getList(8)->rec()->groups()->get();

        $this->assertTrue( is_array($aParams) );
        $this->assertEquals( count($aParams), 4 );
        foreach( $aParams as $k=>$aGroup ){
            foreach( $aGroup as $aParam ){
                switch( $aParam->name ){
                    case "n1":
                        $this->assertEquals( $aParam->value, 'v61' );
                        $this->assertEquals( $aParam->parent, '6' );
                        $this->assertEquals( $aParam->group, $k );
                    break;
                    case "n2":
                        $this->assertEquals( $aParam->value, 'v71' );
                        $this->assertEquals( $aParam->parent, '7' );
                        $this->assertEquals( $aParam->group, $k );
                    break;
                    case "n3":
                        $this->assertEquals( $aParam->value, 'v53' );
                        $this->assertEquals( $aParam->parent, '5' );
                        $this->assertEquals( $aParam->group, $k );
                    break;
                    case "n4":
                        $this->assertEquals( $aParam->value, 'v54' );
                        $this->assertEquals( $aParam->parent, '5' );
                        $this->assertEquals( $aParam->group, $k );
                    break;
                    case "n5":
                        $this->assertEquals( $aParam->value, 'v55' );
                        $this->assertEquals( $aParam->parent, '5' );
                        $this->assertEquals( $aParam->group, $k );
                    break;
                    case "n6":
                        $this->assertEquals( $aParam->value, 'v56' );
                        $this->assertEquals( $aParam->parent, '5' );
                        $this->assertEquals( $aParam->group, $k );
                    break;
                    case "n7":
                        $this->assertEquals( $aParam->value, 'v63' );
                        $this->assertEquals( $aParam->parent, '6' );
                        $this->assertEquals( $aParam->group, $k );
                    break;
                    case "n8":
                        $this->assertEquals( $aParam->value, 'v72' );
                        $this->assertEquals( $aParam->parent, '7' );
                        $this->assertEquals( $aParam->group, $k );
                    break;
                }
            }
        }

        /** parent */
        $aParams = Parameters::getList([7, 8])->get();

        $this->assertTrue( is_array($aParams) );
        $this->assertEquals( count($aParams), 4 );

        /** оказывается yii пофиг $aParam->parent или $aParam['parent'] */
        foreach( $aParams as $aParam ){
            $this->assertTrue( in_array($aParam->parent, [7, 8]) );
        }

        $aParams = Parameters::getList([7, 8])->rec()->get();

        $this->assertTrue( is_array($aParams) );
        $this->assertEquals( count($aParams), 4 );

        /** group */
        $aParams = Parameters::getList()->group('g3')->get();

        $this->assertTrue( is_array($aParams) );
        $this->assertEquals( count($aParams), 3 );

        foreach( $aParams as $aParam ){
            $this->assertEquals( $aParam->group, 'g3' );
        }

        $aParams = Parameters::getList(6)->group('g1')->get();

        $this->assertTrue( is_array($aParams) );
        $this->assertEquals( count($aParams), 2 );

        foreach( $aParams as $aParam ){
            $this->assertEquals( $aParam->group, 'g1' );
            $this->assertEquals( $aParam->parent, '6' );
        }

        /** name */
        $aParams = Parameters::getList()->name('n2')->get();

        $this->assertTrue( is_array($aParams) );
        $this->assertEquals( count($aParams), 3 );

        foreach( $aParams as $aParam ){
            $this->assertEquals( $aParam->name, 'n2' );
        }

        $aParams = Parameters::getList()->name('2323n2')->get();

        $this->assertTrue( is_array($aParams) );
        $this->assertEquals( count($aParams), 0 );

        $aParams = Parameters::getList()->name(Parameters::template)->get();

        $this->assertTrue( is_array($aParams) );
        $this->assertEquals( count($aParams), 3 );

        foreach( $aParams as $aParam ){
            $this->assertEquals( $aParam->name, Parameters::template );
        }

        $aParams = Parameters::getList([6, 8])->name(Parameters::template)->get();

        $this->assertTrue( is_array($aParams) );
        $this->assertEquals( count($aParams), 2 );

        foreach( $aParams as $aParam ){
            $this->assertEquals( $aParam->name, Parameters::template );
            $this->assertTrue( in_array($aParam->parent, [6, 8]) );
        }

        /** level */
        $aParams = Parameters::getList(5)->get();
        $this->assertTrue( is_array($aParams) );
        $this->assertEquals( count($aParams), 6 );

        $aParams = Parameters::getList(5)->level(ListSelector::alEdit)->get();
        $this->assertTrue( is_array($aParams) );
        $this->assertEquals( count($aParams), 2 );

        foreach( $aParams as $aParam ){
            $this->assertTrue( $aParam->access_level != 0 );
        }

        $aParams = Parameters::getList(5)->level(ListSelector::alPos)->get();
        $this->assertTrue( is_array($aParams) );
        $this->assertEquals( count($aParams), 1 );

        foreach( $aParams as $aParam ){
            $this->assertTrue( $aParam->access_level > 0 );
        }

        $aParams = Parameters::getList()->level(ListSelector::alEdit)->get();
        $this->assertTrue( is_array($aParams) );
        $this->assertEquals( count($aParams), 3 );

        foreach( $aParams as $aParam ){
            $this->assertTrue( $aParam->access_level != 0 );
        }

        $aParams = Parameters::getList()->level(ListSelector::alPos)->get();
        $this->assertTrue( is_array($aParams) );
        $this->assertEquals( count($aParams), 2 );

        foreach( $aParams as $aParam ){
            $this->assertTrue( $aParam->access_level > 0 );
        }

        /** fields */

        $aParams = Parameters::getList()->get();
        $this->assertTrue( is_array($aParams) );

        foreach( $aParams as $aParam ){
            $this->assertTrue( isset($aParam->access_level) );
        }

        $aParams = Parameters::getList()->fields(['value'])->get();
        $this->assertTrue( is_array($aParams) );

        foreach( $aParams as $aParam ){
            $this->assertTrue( isset($aParam->value) );
            $this->assertFalse( isset($aParam->access_level) );
        }

        /** asArray */

        $aParams = Parameters::getList()->asArray()->get();
        $this->assertTrue( is_array($aParams) );

        foreach( $aParams as $aParam ){
            $this->assertTrue( is_array($aParam) );
        }

        $aParams = Parameters::getList()->get();
        $this->assertTrue( is_array($aParams) );

        foreach( $aParams as $aParam ){
            $this->assertTrue( $aParam instanceof Params );
        }

        /** other */

        $aParams = Parameters::getList(8)->groups()->get();

        $this->assertTrue( is_array($aParams) );
        $this->assertEquals( count($aParams), 1 );
        foreach( $aParams as $k=>$aGroup ){
            foreach( $aGroup as $aParam ){
                $this->assertEquals( $aParam->parent, '8' );
            }
        }
        Parameters::setParams( 8, 'g1', 'n3', 'v88' );
        Parameters::setParams( 8, 'g3', 'n7', 'v89' );
        Parameters::setParams( 8, 'g3', 'n9', 'v810' );

        $aParams = Parameters::getList(8)->rec()->groups()->get();

        $this->assertTrue( is_array($aParams) );
        $this->assertEquals( count($aParams), 4 );
        foreach( $aParams as $k=>$aGroup ){
            foreach( $aGroup as $aParam ){
                switch( $aParam->name ){
                    case "n1":
                        $this->assertEquals( $aParam->value, 'v61' );
                        $this->assertEquals( $aParam->parent, '6' );
                        $this->assertEquals( $aParam->group, $k );
                        break;
                    case "n2":
                        $this->assertEquals( $aParam->value, 'v71' );
                        $this->assertEquals( $aParam->parent, '7' );
                        $this->assertEquals( $aParam->group, $k );
                        break;
                    case "n3":
                        $this->assertEquals( $aParam->value, 'v88' );
                        $this->assertEquals( $aParam->parent, '8' );
                        $this->assertEquals( $aParam->group, $k );
                        break;
                    case "n4":
                        $this->assertEquals( $aParam->value, 'v54' );
                        $this->assertEquals( $aParam->parent, '5' );
                        $this->assertEquals( $aParam->group, $k );
                        break;
                    case "n5":
                        $this->assertEquals( $aParam->value, 'v55' );
                        $this->assertEquals( $aParam->parent, '5' );
                        $this->assertEquals( $aParam->group, $k );
                        break;
                    case "n6":
                        $this->assertEquals( $aParam->value, 'v56' );
                        $this->assertEquals( $aParam->parent, '5' );
                        $this->assertEquals( $aParam->group, $k );
                        break;
                    case "n7":
                        $this->assertEquals( $aParam->value, 'v89' );
                        $this->assertEquals( $aParam->parent, '8' );
                        $this->assertEquals( $aParam->group, $k );
                        break;
                    case "n8":
                        $this->assertEquals( $aParam->value, 'v72' );
                        $this->assertEquals( $aParam->parent, '7' );
                        $this->assertEquals( $aParam->group, $k );
                        break;
                    case "n9":
                        $this->assertEquals( $aParam->value, 'v810' );
                        $this->assertEquals( $aParam->parent, '8' );
                        $this->assertEquals( $aParam->group, $k );
                        break;
                }
            }
        }

        $aParams = Parameters::getList(8)->groups()->get();

        $this->assertTrue( is_array($aParams) );
        $this->assertEquals( count($aParams), 3 );
        foreach( $aParams as $k=>$aGroup ){
            foreach( $aGroup as $aParam ){
                $this->assertEquals( $aParam->parent, '8' );
            }
        }

    }


    /**
     * @covers skewer\build\Component\Section\Parameters::getListByModule
     * @covers skewer\build\Component\Section\Parameters::getChildrenList
     */
    public function testGetListByModule(){

        Parameters::setParams( 5, 'g1', Parameters::object, 'module1' );
        Parameters::setParams( 5, 'g2', Parameters::object, 'module2' );
        Parameters::setParams( 5, 'g3', Parameters::object, 'module3' );

        Parameters::setParams( 6, 'g1', Parameters::object, 'module4' );
        Parameters::setParams( 6, Parameters::settings, Parameters::template, 5 );

        Parameters::setParams( 7, 'g1', Parameters::object, 'module14' );
        Parameters::setParams( 7, Parameters::settings, Parameters::template, 5 );

        Parameters::setParams( 8, 'g3', Parameters::object, 'module1' );
        Parameters::setParams( 8, Parameters::settings, Parameters::template, 5 );

        Parameters::setParams( 9, Parameters::settings, Parameters::template, 6 );

        $aSections = Parameters::getListByModule( 'module1', 'g1' );
        sort($aSections);

        $this->assertEquals( count($aSections), 3 );
        $this->assertEquals( $aSections, [5, 8, 9] );

        $aSections = Parameters::getListByModule( 'module2', 'g2' );
        sort($aSections);

        $this->assertEquals( count($aSections), 5 );
        $this->assertEquals( $aSections, [5, 6, 7, 8, 9] );

        $aSections = Parameters::getListByModule( 'module3', 'g3' );
        sort($aSections);

        $this->assertEquals( count($aSections), 4 );
        $this->assertEquals( $aSections, [5, 6, 7, 9] );

    }


    /**
     * @covers skewer\build\Component\Section\Parameters::getChildrenList
     * @covers skewer\build\Component\Section\Parameters::getTpl
     * @covers skewer\build\Component\Section\Parameters::getParentTemplates
     */
    public function testTemplates(){

        Parameters::setParams( 6, Parameters::settings, Parameters::template, 5 );
        Parameters::setParams( 10, Parameters::settings, Parameters::template, 6 );
        Parameters::setParams( 18, Parameters::settings, Parameters::template, 10 );
        Parameters::setParams( 36, Parameters::settings, Parameters::template, 18 );
        Parameters::setParams( 45, Parameters::settings, Parameters::template, 36 );
        Parameters::setParams( 89, Parameters::settings, Parameters::template, 36 );
        Parameters::setParams( 62, Parameters::settings, Parameters::template, 89 );
        Parameters::setParams( 600, Parameters::settings, Parameters::template, 45 );
        Parameters::setParams( 11, Parameters::settings, Parameters::template, 600 );


        $this->assertFalse( Parameters::getTpl( 5 ) );
        $this->assertSame( [], Parameters::getParentTemplates( 5 ) );

        $this->assertEquals( Parameters::getTpl( 18 ), 10 );
        $this->assertEquals( Parameters::getTpl( 11 ), 600 );
        $this->assertEquals( Parameters::getTpl( 6 ), 5 );

        $aTemplates = Parameters::getParentTemplates( 18 );
        $this->assertEquals( count($aTemplates), 3 );
        $this->assertContains( 5, $aTemplates );
        $this->assertContains( 6, $aTemplates );
        $this->assertContains( 10, $aTemplates );

        $aTemplates = Parameters::getParentTemplates( 600 );
        $this->assertEquals( count($aTemplates), 6 );
        $this->assertContains( 5, $aTemplates );
        $this->assertContains( 6, $aTemplates );
        $this->assertContains( 10, $aTemplates );
        $this->assertContains( 18, $aTemplates );
        $this->assertContains( 36, $aTemplates );
        $this->assertContains( 45, $aTemplates );

        $this->assertFalse( Parameters::getTpl( 1111 ) );
        $this->assertSame( [], Parameters::getParentTemplates( 1111 ) );

        $aChilds = Parameters::getChildrenList( 11 );
        $this->assertFalse( $aChilds );

        $aChilds = Parameters::getChildrenList( 45 );
        $this->assertEquals( count($aChilds), 2 );
        $this->assertContains( '11', $aChilds );
        $this->assertContains( '600', $aChilds );

        $aChilds = Parameters::getChildrenList( 89 );
        $this->assertEquals( count($aChilds), 1 );
        $this->assertContains( '62', $aChilds );

        $aChilds = Parameters::getChildrenList( 10 );
        $this->assertEquals( count($aChilds), 7 );
        $this->assertContains( '62', $aChilds );
        $this->assertContains( '45', $aChilds );
        $this->assertContains( '11', $aChilds );

        $aChilds = Parameters::getChildrenList( 5 );
        $this->assertEquals( count($aChilds), 9 );
        $this->assertContains( '62', $aChilds );
        $this->assertContains( '45', $aChilds );
        $this->assertContains( '11', $aChilds );

    }


    /**
     * @covers skewer\build\Component\Section\Parameters::updateByName
     */
    public function testUpdateByName(){

        Parameters::setParams( 6, 'g1', 'n1', 5 );
        Parameters::setParams( 10, 'g1', 'n1', 6 );
        Parameters::setParams( 18, 'g1', 'n1', 10 );
        Parameters::setParams( 18, 'g2', 'n1', 10 );

        $this->assertEquals( Parameters::updateByName( 'g1', 'n999', 8 ), 0 );
        $this->assertEquals( Parameters::updateByName( '', 'n999', 8 ), 0 );
        $this->assertEquals( Parameters::updateByName( '', '', 8 ), 0 );
        $this->assertEquals( Parameters::updateByName( 'g1', 'n1', 8 ), 3 );

        $aParams = Parameters::getList()->group('g1')->name('n1')->asArray()->get();
        foreach( $aParams as $param ){
            $this->assertEquals( $param['value'], '8' );
        }

        $this->assertEquals( Parameters::getValByName( 18, 'g2', 'n1'), 10 );

    }


    /**
     * @covers skewer\build\Component\Section\Parameters::copyToSection
     */
    public function testCopy(){

        $oParam = Parameters::createParam([
            'parent' => 6, 'group' => 'g1', 'name' => 'n1', 'value' => 5,
        ]);
        $oParam->save();

        $this->assertFalse(Parameters::getByName( 8, 'g1', 'n1'));
        $this->assertFalse(Parameters::getValByName( 8, 'g1', 'n1'));

        $oParam = Parameters::copyToSection($oParam, 8);

        $this->assertTrue($oParam instanceof Params);
        $id = $oParam->id;

        $oParams = Parameters::getByName( 8, 'g1', 'n1');
        $this->assertTrue($oParams instanceof Params);
        $this->assertEquals( $oParam->id, $id );
        $this->assertEquals(Parameters::getValByName( 8, 'g1', 'n1'), 5);

        $this->assertFalse(Parameters::copyToSection($oParam, 8));

        $oParam = Parameters::copyToSection($oParam, 11, 'new_val');

        $this->assertTrue($oParam instanceof Params);
        $id = $oParam->id;

        $oParams = Parameters::getByName( 11, 'g1', 'n1');
        $this->assertTrue($oParams instanceof Params);
        $this->assertEquals( $oParam->id, $id );
        $this->assertEquals(Parameters::getValByName( 11, 'g1', 'n1'), 'new_val');
    }


    /**
     * @covers skewer\build\Component\Section\Parameters::setParams
     */
    public function testSetParam(){

        $this->assertFalse( Parameters::getByName( 3, 'g3', 'n5' ) );

        $this->assertTrue(Parameters::setParams( 3, 'g3', 'n5' ) > 0);

        $oParam = Parameters::getByName( 3, 'g3', 'n5' );
        $this->assertTrue($oParam instanceof Params);
        $this->assertEquals( $oParam->name, 'n5' );
        $this->assertEquals( $oParam->group, 'g3' );
        $this->assertEquals( $oParam->parent, 3 );

        $this->assertTrue(Parameters::setParams( 3, 'g3', 'n5', '123', 'rrr', 'qqq', 9 ) > 0 );

        $oParam = Parameters::getByName( 3, 'g3', 'n5' );
        $this->assertTrue($oParam instanceof Params);
        $this->assertEquals( $oParam->name, 'n5' );
        $this->assertEquals( $oParam->group, 'g3' );
        $this->assertEquals( $oParam->parent, 3 );
        $this->assertEquals( $oParam->value, '123' );
        $this->assertEquals( $oParam->show_val, 'rrr' );
        $this->assertEquals( $oParam->title, 'qqq' );
        $this->assertEquals( $oParam->access_level, 9 );

        $this->assertTrue(Parameters::setParams( 3, 'g3', 'n5', '1233333') > 0);

        $oParam = Parameters::getByName( 3, 'g3', 'n5' );
        $this->assertTrue($oParam instanceof Params);
        $this->assertEquals( $oParam->name, 'n5' );
        $this->assertEquals( $oParam->group, 'g3' );
        $this->assertEquals( $oParam->parent, 3 );
        $this->assertEquals( $oParam->value, '1233333' );
        $this->assertEquals( $oParam->show_val, 'rrr' );
        $this->assertEquals( $oParam->title, 'qqq' );
        $this->assertEquals( $oParam->access_level, 9 );

        $this->assertTrue(Parameters::setParams( 33, 'g8', 'n45', 'eeeee') > 0);

        $oParam = Parameters::getByName( 33, 'g8', 'n45' );
        $this->assertTrue($oParam instanceof Params);
        $this->assertEquals( $oParam->name, 'n45' );
        $this->assertEquals( $oParam->group, 'g8' );
        $this->assertEquals( $oParam->parent, 33 );
        $this->assertEquals( $oParam->value, 'eeeee' );

    }


    /**
     * @covers skewer\build\Component\Section\Parameters::removeByName
     * @covers skewer\build\Component\Section\Parameters::removeById
     */
    public function testRemove(){

        Parameters::setParams( 3, 'g3', 'n5', 'test' );
        Parameters::setParams( 3, 'g3', 'n45', 'test' );

        $this->assertEquals( Parameters::getValByName( 3, 'g3', 'n5'), 'test' );

        $this->assertEquals( Parameters::removeByName( 3, 'g3', 'n5' ), 1 );

        $this->assertFalse( Parameters::getValByName( 3, 'g3', 'n5') );
        $this->assertFalse( Parameters::getByName( 3, 'g3', 'n5') );

        $this->assertEquals( Parameters::removeByName( 3, 'g3', 'n5' ), 0 );

        $id1 = Parameters::setParams( 3, 'g3', 'n5', 'test' );
        $id2 = Parameters::setParams( 3, 'g3', 'n45', 'test' );

        $this->assertEquals( Parameters::removeById([$id1, $id2]), 2 );

        $this->assertNull( Parameters::getById($id1) );
        $this->assertNull( Parameters::getById($id2) );

        $id1 = Parameters::setParams( 3, 'g3', 'n5', 'test' );
        $id2 = Parameters::setParams( 3, 'g3', 'n45', 'test' );

        $this->assertEquals( Parameters::removeById($id1), 1 );

        $this->assertNull( Parameters::getById($id1) );
        $this->assertEquals( Parameters::getValByName( 3, 'g3', 'n45'), 'test' );

    }

    public function testDeleteSection(){

        $oSection1 = Tree::addSection(3, '12212', 3);
        $iSection1 = $oSection1->id;
        $oSection2 = Tree::addSection($iSection1, '12212', 3);
        $iSection2 = $oSection2->id;
        $oSection3 = Tree::addSection($iSection2, '12212', 3);
        $iSection3 = $oSection3->id;
        $oSection4 = Tree::addSection(3, '12212', 3);
        $iSection4 = $oSection4->id;

        Parameters::setParams($iSection1, 'g', 'n1', 1);
        Parameters::setParams($iSection1, 'g', 'n2', 1);
        Parameters::setParams($iSection1, 'g', 'n3', 1);

        Parameters::setParams($iSection2, 'g', 'n1', 1);
        Parameters::setParams($iSection2, 'g', 'n2', 1);
        Parameters::setParams($iSection2, 'g', 'n3', 1);

        Parameters::setParams($iSection3, 'g', 'n1', 1);
        Parameters::setParams($iSection3, 'g', 'n2', 1);
        Parameters::setParams($iSection3, 'g', 'n3', 1);

        Parameters::setParams($iSection4, 'g', 'n1', 1);
        Parameters::setParams($iSection4, 'g', 'n2', 1);
        Parameters::setParams($iSection4, 'g', 'n3', 1);

        $iC1 = count(Parameters::getList($iSection1)->get());
        $iC2 = count(Parameters::getList($iSection2)->get());
        $iC3 = count(Parameters::getList($iSection3)->get());

        $oSection4->delete();

        $this->assertEquals( $iC1, count(Parameters::getList($iSection1)->get()) );
        $this->assertEquals( $iC2, count(Parameters::getList($iSection2)->get()) );
        $this->assertEquals( $iC3, count(Parameters::getList($iSection3)->get()) );
        $this->assertEquals( 0, count(Parameters::getList($iSection4)->get()) );

        $oSection1->delete();

        $this->assertEquals( 0, count(Parameters::getList($iSection1)->get()) );
        $this->assertEquals( 0, count(Parameters::getList($iSection2)->get()) );
        $this->assertEquals( 0, count(Parameters::getList($iSection3)->get()) );
        $this->assertEquals( 0, count(Parameters::getList($iSection4)->get()) );

    }
}