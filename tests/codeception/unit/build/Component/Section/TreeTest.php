<?php

namespace skewer\build\Component\Section;

use skewer\build\Component\Search;

use skewer\build\Component\Search\SearchIndex;
use skewer\models\Parameters;
use skewer\models\TreeSection;

/**
 * @covers skewer\models\TreeSection
 */
class TreeTest extends \PHPUnit_Framework_TestCase {

    protected function setUp() {

    }


    protected function setDown() {

    }

    protected function createTree() {
        $a = Tree::addSection(\Yii::$app->sections->main(), 'a' );
        $b1 = Tree::addSection( $a->id, 'b1' );
        $b2 = Tree::addSection( $a->id, 'b2' );
        $c1 = Tree::addSection( $b1->id, 'c1' );
        $c2 = Tree::addSection( $b1->id, 'c2' );
        $d = Tree::addSection( $c2->id, 'd' );
        return $a->id;
    }


    public function testGetSection() {

        $section = Tree::getSection(\Yii::$app->sections->main() );
        $this->assertSame( $section->id,\Yii::$app->sections->main() );
        $this->assertSame( $section->title, 'Главная' );

        $section = Tree::getSection(\Yii::$app->sections->main(), true );
        $this->assertTrue( is_array($section) );
        $this->assertSame( 11, count($section) );
        $this->assertSame( 'Главная', $section['title'] );

        $section = Tree::getSection( 123123123 );
        $this->assertNull( $section );
    }


    public function testGetSections() {

        $s1 = Tree::addSection(\Yii::$app->sections->main(), 's1' );
        $s2 = Tree::addSection(\Yii::$app->sections->main(), 's2' );

        $list = Tree::getSections( [$s1->id, $s2->id] );

        $this->assertNotEmpty( $list );
        $this->assertSame( count($list), 2 );
        $this->assertSame( $list[$s1->id]->title, 's1' );
        $this->assertSame( $list[$s2->id]->title, 's2' );
    }


    public function testAddSection() {

        $sTitle = 'testsection';
        $section = Tree::addSection(\Yii::$app->sections->main(), $sTitle );

        $this->assertNotEmpty( $section );
        $this->assertNotEmpty( $section->id );

        $realSection = Tree::getSection( $section->id );

        $this->assertNotEmpty( $realSection );
        $this->assertSame( $section->alias, $realSection->alias );
        $this->assertSame( $section->parent, $realSection->parent );

        // todo
    }


    public function testCheckAlias() {

        $sTitle = 'testsection';
        $section = Tree::addSection(\Yii::$app->sections->main(), $sTitle );

        $sTitle = 'testsection';
        $wsection = Tree::addSection(\Yii::$app->sections->main(), $sTitle );

        $this->assertNotEmpty( $section->alias );
        $this->assertNotEmpty( $wsection->alias );
        $this->assertNotSame( $section->alias, $wsection->alias );


    }


    public function testGetSectionTitle() {

        $sTitle = 'testsection';
        $section = Tree::addSection(\Yii::$app->sections->main(), $sTitle );
        $sub_section = Tree::addSection( $section->id, 'sub_' . $sTitle );

        $this->assertNotEmpty( $section );
        $this->assertSame( $sTitle, Tree::getSectionsTitle( $section->id ) );

        //var_dump( Tree::getSectionTitle( $section->id, true ) );
    }

    public function testGetSectionTree() {

        $id = $this->createTree();

        $tree = Tree::getSectionTree( $id );

        $this->assertSame( count( $tree ), 2 );
        $this->assertSame( $tree[0]['title'], 'b1' );
        $this->assertSame( $tree[1]['title'], 'b2' );
        $this->assertSame( count($tree[0]['children']), 2 );
        $this->assertSame( $tree[0]['children'][0]['title'], 'c1' );
        $this->assertSame( $tree[0]['children'][1]['title'], 'c2' );
        $this->assertSame( count($tree[0]['children'][1]['children']), 1 );
        $this->assertSame( $tree[0]['children'][1]['children'][0]['title'], 'd' );

    }

    public function testGetSectionList() {

        $id = $this->createTree();

        $list = Tree::getSectionList( $id );

        $this->assertSame( count( $list ), 6 );

        $this->assertSame( $list[0]['title'], 'a' );
        $this->assertSame( $list[1]['title'], '-b1' );
        $this->assertSame( $list[2]['title'], '--c1' );
        $this->assertSame( $list[3]['title'], '--c2' );
        $this->assertSame( $list[4]['title'], '---d' );
        $this->assertSame( $list[5]['title'], '-b2' );

    }


    public function testAliasPath() {

        // init
        $id_a = Tree::addSection(\Yii::$app->sections->main(), 'apa', 0, 'test-section-a', 1 );
        $id_b1 = Tree::addSection( $id_a->id, 'apb1', 0, 'test-section-b1', 0 );
        $id_b2 = Tree::addSection( $id_a->id, 'apb2', 0, 'test-section-b2', 1 );
        $id_b3 = Tree::addSection( $id_a->id, 'apb3', 0, 'test-section-b3', 2 );
        $id_c1 = Tree::addSection( $id_b1->id, 'apc1', 0, 'test-section-c1', 1 );
        $id_d1 = Tree::addSection( $id_b2->id, 'apd1', 0, 'test-section-d1', 1 );
        $id_d2 = Tree::addSection( $id_b2->id, 'apd2', 0, 'test-section-d2', 1 );
        $id_e1 = Tree::addSection( $id_b3->id, 'ape1', 0, 'test-section-e1', 1 );


        $test_section = Tree::getSection( $id_c1->id );
        $this->assertSame( $test_section->alias_path, '/test-section-a/test-section-b1/test-section-c1/' );

        // change alias path
        $section = Tree::getSection( $id_a->id );
        $section->alias = 'test-section-new';
        $section->save();


        // test
        $test_section = Tree::getSection( $id_b1->id );
        $this->assertSame( $test_section->alias_path, '/test-section-new/test-section-b1/' );
        $test_section = Tree::getSection( $id_c1->id );
        $this->assertSame( $test_section->alias_path, '/test-section-new/test-section-b1/test-section-c1/' );
        $test_section = Tree::getSection( $id_d1->id );
        $this->assertSame( $test_section->alias_path, '/test-section-new/test-section-b2/test-section-d1/' );
        $test_section = Tree::getSection( $id_e1->id );
        $this->assertSame( $test_section->alias_path, '/test-section-new/test-section-e1/' );



    }

    /**
     * Проверка перестроения path при изменении родительского раздела
     */
    public function testParentChange() {

        /**
         * создать 2 раздела
         * созд третий в 1
         * перенести его во 2
         * проверить
         */

        /**
         * Было
         *  a
         *  |- b1
         *  |   \- c1
         *  |- b2
         *  |   \- d1
         *  \- b3
         *      \- e1
         *
         * Делаем
         *  a
         *  |- b1
         *  |- b2
         *  |   \- d1
         *  \- b3
         *      |- e1
         *      \- c1
         *
         */

        // init
        $id_a =  Tree::addSection(\Yii::$app->sections->main(), 'apa',  0, 'test-xsection-a',  1 );
        $id_b1 = Tree::addSection( $id_a->id,       'apb1', 0, 'test-xsection-b1', 1 );
        $id_b2 = Tree::addSection( $id_a->id,       'apb2', 0, 'test-xsection-b2', 1 );
        $id_b3 = Tree::addSection( $id_a->id,       'apb3', 0, 'test-xsection-b3', 1 );
        $id_c1 = Tree::addSection( $id_b1->id,      'apc1', 0, 'test-xsection-c1', 1 );
        $id_d1 = Tree::addSection( $id_b2->id,      'apd1', 0, 'test-xsection-d1', 1 );
        $id_d2 = Tree::addSection( $id_b2->id,      'apd2', 0, 'test-xsection-d2', 1 );
        $id_e1 = Tree::addSection( $id_b3->id,      'ape1', 0, 'test-xsection-e1', 1 );

        // проверяем правильность построения path для переносимого раздела (c1)
        $test_section = Tree::getSection( $id_c1->id );
        $this->assertSame( '/test-xsection-a/test-xsection-b1/test-xsection-c1/', $test_section->alias_path );

        // проверяем правильность построения path для приемника (b3)
        $new_root = Tree::getSection( $id_b3->id );
        $this->assertSame( '/test-xsection-a/test-xsection-b3/', $new_root->alias_path );

        $test_section->parent = $new_root->id;
        $test_section->save();

        // test
        $test_section = Tree::getSection( $id_c1->id );
        $this->assertSame( '/test-xsection-a/test-xsection-b3/test-xsection-c1/', $test_section->alias_path );

    }


    public function testChangePosition() {

        $a = Tree::addSection(\Yii::$app->sections->main(), 'a', 0, 'w-test-section-a' );
        $b = Tree::addSection( $a->id, 'b', 0, 'w-test-section-b' );
        $c = Tree::addSection( $b->id, 'c', 0, 'w-test-section-c' );
        $d = Tree::addSection( $a->id, 'd', 0, 'w-test-section-d' );
        $e = Tree::addSection( $d->id, 'e', 0, 'w-test-section-e' );
        $f = Tree::addSection( $d->id, 'f', 0, 'w-test-section-f' );


        $list = Tree::getSectionList( $a->id );

        $this->assertSame( count( $list ), 6 );

        $this->assertSame( $list[0]['title'], 'a' );
        $this->assertSame( $list[1]['title'], '-b' );
        $this->assertSame( $list[2]['title'], '--c' );
        $this->assertSame( $list[3]['title'], '-d' );
        $this->assertSame( $list[4]['title'], '--e' );
        $this->assertSame( $list[5]['title'], '--f' );

        $d->changePosition( $c, 'before' );

        $list = Tree::getSectionList( $a->id );

        $this->assertSame( count( $list ), 6 );

        $this->assertSame( $list[0]['title'], 'a' );
        $this->assertSame( $list[1]['title'], '-b' );
        $this->assertSame( $list[2]['title'], '--d' );
        $this->assertSame( $list[3]['title'], '---e' );
        $this->assertSame( $list[4]['title'], '---f' );
        $this->assertSame( $list[5]['title'], '--c' );

        $test_section = Tree::getSection( $d->id );
        $this->assertSame( $test_section->alias_path, '/w-test-section-a/w-test-section-b/w-test-section-d/' );
        $test_section = Tree::getSection( $f->id );
        $this->assertSame( $test_section->alias_path, '/w-test-section-a/w-test-section-b/w-test-section-d/w-test-section-f/' );

    }


    public function testPosition() {

        $q = Tree::addSection(\Yii::$app->sections->main(), 'q' );
        $a = Tree::addSection( $q->id, 'a' );
        $b = Tree::addSection( $q->id, 'b' );
        $c = Tree::addSection( $q->id, 'c' );
        $d = Tree::addSection( $q->id, 'd' );
        $e = Tree::addSection( $q->id, 'e' );

        $this->assertSame( $a->position, 1 );
        $this->assertSame( $b->position, 2 );
        $this->assertSame( $c->position, 3 );
        $this->assertSame( $d->position, 4 );
        $this->assertSame( $e->position, 5 );

    }

    /**
     * Проверка переназначения позиции при переносе
     * Раздела в другой подраздел (смене родительского раздела)
     */
    public function testPositionChangeOnMove() {

        $q1 = Tree::addSection(\Yii::$app->sections->main(), 'q' );
        $a = Tree::addSection( $q1->id, 'a' );
        $b = Tree::addSection( $q1->id, 'b' );

        $q2 = Tree::addSection(\Yii::$app->sections->main(), 'q' );
        $c = Tree::addSection( $q2->id, 'c' );
        $d = Tree::addSection( $q2->id, 'd' );
        $e = Tree::addSection( $q2->id, 'e' );

        $this->assertSame( $a->position, 1 );
        $this->assertSame( $b->position, 2 );
        $this->assertSame( $c->position, 1 );
        $this->assertSame( $d->position, 2 );
        $this->assertSame( $e->position, 3 );

        // переносим раздел "а" "q2", индекс доложен быть 3+1=4
        $a->parent = $q2->id;
        $a->save();
        $this->assertSame( $a->position, 4, 'не сменился индекс при переносе в другой подраздел' );

        // переносим раздел "d" "q1", индекс доложен быть 2+1=3
        $d->parent = $q1->id;
        $d->save();
        $this->assertSame( $d->position, 3, 'не сменился индекс при переносе в другой подраздел' );

        // переносим раздел "d" обратно в "q2", индекс доложен быть 4+1=5 (должен стать в конец)
        $d->parent = $q2->id;
        $d->save();
        $this->assertSame( $d->position, 5, 'не сменился индекс при переносе в другой подраздел' );

    }


    public function testShiftPosition() {

        $q = Tree::addSection(\Yii::$app->sections->main(), 'q' );
        $a = Tree::addSection( $q->id, 'a' );
        $b = Tree::addSection( $q->id, 'b' );
        $c = Tree::addSection( $q->id, 'c' );
        $d = Tree::addSection( $q->id, 'd' );

        $this->assertSame( $a->position, 1 );
        $this->assertSame( $b->position, 2 );
        $this->assertSame( $c->position, 3 );
        $this->assertSame( $d->position, 4 );

        $a->changePosition( $c, 'after' );

        $a->refresh();
        $b->refresh();
        $c->refresh();
        $d->refresh();

        $this->assertSame( $a->position, 4 );
        $this->assertSame( $b->position, 2 );
        $this->assertSame( $c->position, 3 );
        $this->assertSame( $d->position, 5 );


        $b->changePosition( $a, 'before' );

        $a->refresh();
        $b->refresh();
        $c->refresh();
        $d->refresh();

        $this->assertSame( $a->position, 5 );
        $this->assertSame( $b->position, 4 );
        $this->assertSame( $c->position, 3 );
        $this->assertSame( $d->position, 6 );

    }


    public function testLevel() {

        $q = Tree::addSection(\Yii::$app->sections->topMenu(), 'q' );

        $a = Tree::addSection( $q->id, 'a' );
        $b = Tree::addSection( $q->id, 'b' );
        $c = Tree::addSection( $a->id, 'c' );
        $d = Tree::addSection( $b->id, 'd' );
        $e = Tree::addSection( $d->id, 'e' );

        $this->assertSame( $q->level, 2 );
        $this->assertSame( $a->level, 3 );
        $this->assertSame( $b->level, 3 );
        $this->assertSame( $c->level, 4 );
        $this->assertSame( $d->level, 4 );
        $this->assertSame( $e->level, 5 );

    }

    /**
     * Проверка удаления ресурсов для раздела
     * @covers skewer\models\TreeSection::onSectionDelete
     */
    public function testDeleteForDir() {

        $s = Tree::addSection(\Yii::$app->sections->topMenu(), 'sect' );

        $sPath = FILEPATH.$s->id.'/';

        $this->assertFileNotExists($sPath);

        mkdir($sPath);
        mkdir($sPath.'a');
        touch($sPath.'file1.txt');
        touch($sPath.'a/file2.txt');

        $this->assertFileExists( $sPath );
        $this->assertFileExists( $sPath.'a' );
        $this->assertFileExists( $sPath.'file1.txt' );
        $this->assertFileExists( $sPath.'a/file2.txt' );

        Tree::removeSection($s);

        $this->assertFileNotExists( $sPath );
        $this->assertFileNotExists( $sPath.'a' );
        $this->assertFileNotExists( $sPath.'file1.txt' );
        $this->assertFileNotExists( $sPath.'a/file2.txt' );

    }

    /**
     * Проверка удаления из поиска для раздела
     * @covers skewer\models\TreeSection::onSectionDelete
     * @group now
     */
    public function testDeleteForSearch() {

        // добавляем раздел
        $s = Tree::addSection(
           \Yii::$app->sections->topMenu(),
            'sect',
           \Yii::$app->sections->tplNew(),
            'test_sect',
            Visible::VISIBLE
        );

        // заводим его в поисковом индексе
        $p = new Parameters();
        $p->group = '.';
        $p->name = 'staticContent';
        $p->show_val = 'asd';
        $p->save();

        $s->save();

        $this->assertNotEmpty(Search\Api::get( 'Page', $s->id ),
            'нет поисковой записи');

        // добавляем тестовую запись в поисковый индекс
        $oRow = SearchIndex::getNewRow();
        $oRow->search_title = 'asd';
        $oRow->status = 1;
        $oRow->use_in_search = 1;
        $oRow->search_text = 'asd';
        $oRow->object_id = 123;
        $oRow->class_name = 'test_tree_del';
        $oRow->section_id = $s->id;
        $oRow->language = \Yii::$app->language;
        $oRow->href = '/qwe/';
        $oRow->save();

        $this->assertNotEmpty(Search\Api::get('test_tree_del', 123),
            'поисковая запись не добавлена');

        // удаляем раздел
        Tree::removeSection($s);

        // раздела в поисковом индексе быть не должно
        $this->assertEmpty(Search\Api::get( 'Page', $s->id ),
            'поисковая запись не удалена');

        // ... и тестовой запись тоже
        $this->assertEmpty(Search\Api::get('test_tree_del', 123),
            'тестовая поисковая запись не удалена');
    }

    public function testDelete() {

        $s1 = Tree::addSection(\Yii::$app->sections->topMenu(), 'sect' );
        $s2 = Tree::addSection( $s1->id, 'sect' );

        $this->assertNotEmpty( TreeSection::findOne($s1->id) );
        $this->assertNotEmpty( TreeSection::findOne($s2->id) );

        $s1->delete();

        $this->assertEmpty( TreeSection::findOne($s1->id) );
        $this->assertEmpty( TreeSection::findOne($s2->id) );

    }

}