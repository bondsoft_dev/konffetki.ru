<?php

use skewer\build\Component\Search\Prototype;

class PrototypeTest extends PHPUnit_Framework_TestCase {

    public function stripProvider() {

        return [
            [
                '<p>qwe</p>',
                'qwe'
            ],
            [
                '<style>123</style>qwe',
                'qwe'
            ],
            [
                '<script>alert(123);</script>qwe',
                'qwe'
            ],
            [
                '<script>alert(123);</script><p>qwe<p>',
                'qwe'
            ],
            [
<<<IN
<script>
     alert(123);
     alert(123);
     alert(123);
</script>
hello
<style>
    .p {
        color: red;
    }
</style>
<script>
     alert(123);
     alert(123);
     alert(123);
</script>
<div>
    <p>qwe<p>
</div>
IN
                     ,
<<<OUT

hello

    qwe

OUT
            ],
        ];

    }

    /**
     * @dataProvider stripProvider
     * @param string $in
     * @param string $out
     */
    public function testStripTags( $in, $out ) {

        $o = new TestSearchEngine;

        $this->assertSame( $out, $o->getStrip( $in ) );

    }

}

class TestSearchEngine extends Prototype {

    /**
     * отдает имя идентификатора ресурса для работы с поисковым индексом
     * @return string
     */
    public function getName() {
        return 'TestSearchEngine';
    }

    /**
     * @inheritdoc
     */
    protected function update(\skewer\build\Component\Search\Row $oSearchRow) {
    }

    /**
     * @inheritdoc
     */
    public function restore() {
    }

    public function getStrip( $sText ) {
        return $this->stripTags($sText);
    }

}
