<?php

namespace tests\build\Component\Import;


use skewer\build\Component\Catalog\Card;
use skewer\build\Component\Catalog\Generator;
use skewer\build\Component\Import\Api;
use skewer\build\Component\Import\ar\ImportTemplate;;
use skewer\build\Component\Import\Field\Title;
use skewer\build\Component\Import\Field\Unique;
use \skewer\build\Component\Import\Field\Prototype;
use skewer\build\Component\Import\Field\Value;
use skewer\build\Component\Import\Task;
use \skewer\build\Component\QueueManager;
use \skewer\build\Component\Catalog;

class FieldsTest extends \PHPUnit_Framework_TestCase {

    /**
     * Вызвать private метод класса в PHP
     * @param $object
     * @param $method
     * @param $args
     * @return mixed
     */
    private function callPrivateMethod($object, $method, $args = [])
    {
        $classReflection = new \ReflectionClass(get_class($object));
        $methodReflection = $classReflection->getMethod($method);
        $methodReflection->setAccessible(true);
        $result = $methodReflection->invokeArgs($object, $args);
        $methodReflection->setAccessible(false);
        return $result;
    }


    const card = 1;

    /** @var Task */
    private $task = false;

    /**
     * @var Catalog\GoodsRow
     */
    private $oRow = false;

    protected function setUp() {

        $base = Generator::genBaseCard();

        $this->oRow = Catalog\GoodsRow::create( Card::DEF_BASE_CARD );
        $this->oRow->save();

        parent::setUp();
    }


    protected function tearDown() {
        if ( $oCard = Card::get( Card::DEF_BASE_CARD ) )
            $oCard->delete();
    }


    /**
     * @param $sType
     * @param $sName
     * @param $importFields
     * @param array $aParams
     * @return mixed
     * @covers \skewer\build\Component\Import\Field\Prototype::__construct
     * @covers \skewer\build\Component\Import\Field\Prototype::initParams
     */
    private function getField( $sType, $sName, $importFields, $aParams = [] ){

        $sClassName = 'skewer\\build\\Component\\Import\\Field\\' . $sType;

        $aData = [];
        foreach( $aParams as $k=>$v ){
            $aData['fields'][$sName]['params'][$k] = $v;
        }

        if (class_exists( $sClassName )){

            $iTpl = ImportTemplate::getNewRow([
                'card' => Card::DEF_BASE_CARD,
                'type' => Api::Type_File,
                'settings' => json_encode($aData)
            ])->save();

            $iTask = QueueManager\Api::addTask([
                'class' => '\skewer\build\Component\Import\Task',
                'priority' => QueueManager\Task::priorityHigh,
                'resource_use' => Task::weightLow,
                'title' => \Yii::t( 'import', 'task_title', 'test'),
                'parameters' => ['tpl' => (int)$iTpl]
            ]);

            $oTask = QueueManager\Api::getTaskById( $iTask );

            $oTask->init([
                'tpl' => $iTpl
            ]);

            $this->task = $oTask;

            /** @var Prototype $oField */
            $oField = new $sClassName( explode( ',', $importFields), $sName, $oTask );

            $this->assertEquals( $oField->getName(), $sName );

            /** Проверка параметров */
            foreach( $aParams as $k => $v ){
                $this->assertAttributeEquals( $v, $k, $oField );
            }



            return $oField;
        }
    }


    /**
     * @covers \skewer\build\Component\Import\Field\Prototype::loadData
     * @covers \skewer\build\Component\Import\Field\Prototype::dropDown
     */
    public function testField(){

        /** @var Prototype $oField */
        $oField = $this->getField( 'Active', 'active_field', '1,6,3' );

        $oField->loadData([
            '1' => 'test1',
            '2' => 'test2',
            '3' => 'test3',
            '4' => 'test4',
            '5' => 'test5',
            '6' => 'test6',
        ]);

        $this->assertAttributeEquals( ['1' => 'test1', '6' => 'test6', '3' => 'test3'], 'values', $oField );

        $oField->dropDown();
        $this->assertAttributeEquals( [], 'values', $oField );

    }


    /**
     * Находим товар
     * @covers \skewer\build\Component\Import\Field\Unique::beforeSave
     */
    public function testUniqueField(){
        /** @var Unique $oField */
        $oField = $this->getField( 'Unique', 'article', '1', ['create' => false] );

        $row = $this->callPrivateMethod( $oField, 'getGoodsRow', [] );
        $this->assertFalse( $row );

        $oField->init();
        $row = $this->callPrivateMethod( $oField, 'getGoodsRow', [] );
        $this->assertFalse( $row );

        $art = '123456skewer';
        $oRow = Catalog\GoodsRow::create('base_card');
        $oRow->setData(['article' => $art, 'title' => 'skewer']);
        $oRow->save();

        $oField->loadData(['1' => $art]);

        $row = $this->callPrivateMethod( $oField, 'getGoodsRow', [] );
        $this->assertFalse( $row );

        $oField->beforeSave();

        $row = $this->callPrivateMethod( $oField, 'getGoodsRow', [] );
        /** @var $row Catalog\GoodsRow */
        $this->assertTrue( $row instanceof Catalog\GoodsRow );

        $this->assertEquals( $oRow->getRowId(), $row->getRowId() );

        $this->assertEquals( $art, $row->getData()['article'] );

        $this->assertEquals( $oField->getValue(), $art );

        $oField->afterSave();

        $oField->shutdown();

    }


    /**
     * Не находим товар
     * @covers \skewer\build\Component\Import\Field\Unique::beforeSave
     */
    public function testUniqueField2(){
        /** @var Unique $oField */
        $oField = $this->getField( 'Unique', 'article', '1', ['create' => false] );

        $row = $this->callPrivateMethod( $oField, 'getGoodsRow', [] );
        $this->assertFalse( $row );

        $oField->init();
        $row = $this->callPrivateMethod( $oField, 'getGoodsRow', [] );
        $this->assertFalse( $row );

        $art = '123456skewer';
        $oRow = Catalog\GoodsRow::create('base_card');
        $oRow->setData(['article' => $art, 'title' => 'skewer']);
        $oRow->save();

        $oField->loadData(['1' => '123456skewer111']);

        $row = $this->callPrivateMethod( $oField, 'getGoodsRow', [] );
        $this->assertFalse( $row );

        $oField->beforeSave();

        $row = $this->callPrivateMethod( $oField, 'getGoodsRow', [] );
        $this->assertFalse( $row );

        $oField->getValue();

        $this->assertFalse( $row );

        $oField->afterSave();

        $this->assertFalse( $row );

        $oField->shutdown();

        $this->assertFalse( $row );

    }

    /**
     * Не находим товар, но создаем
     * @covers \skewer\build\Component\Import\Field\Unique::beforeSave
     * @covers \skewer\build\Component\Import\Field\Unique::getValue
     */
    public function testUniqueField3(){
        /** @var Unique $oField */
        $oField = $this->getField( 'Unique', 'article', '1', ['create' => true] );

        $row = $this->callPrivateMethod( $oField, 'getGoodsRow', [] );
        $this->assertFalse( $row );

        $oField->init();
        $row = $this->callPrivateMethod( $oField, 'getGoodsRow', [] );
        $this->assertFalse( $row );

        $art = 123456;
        $oRow = Catalog\GoodsRow::create( Card::DEF_BASE_CARD );
        $oRow->setData(['article' => $art, 'title' => 'skewer']);
        $oRow->save();

        $oField->loadData(['1' => 123459]);

        $row = $this->callPrivateMethod( $oField, 'getGoodsRow', [] );
        $this->assertFalse( $row );

        $oField->beforeSave();

        $row = $this->callPrivateMethod( $oField, 'getGoodsRow', [] );
        /** @var $row Catalog\GoodsRow */
        $this->assertTrue( $row instanceof Catalog\GoodsRow );

        $this->assertTrue( $oRow->getRowId() != $row->getRowId() );

        $this->callPrivateMethod( $oField, 'execute', [] );

        $this->assertEquals( '123459', $row->getData()['article'] );

        $this->assertEquals( $oField->getValue(), '123459' );

        $oField->afterSave();

        $oField->shutdown();

    }

    /**
     * Value
     * @covers \skewer\build\Component\Import\Field\Value::getValue
     */
    public function testValueField(){
        /** @var Value $oField */
        $oField = $this->getField( 'Value', 'price', '4' );

        $this->callPrivateMethod( $oField, 'setGoodsRow', [$this->oRow] );

        $price = 100500;

        $this->assertNotEquals($this->oRow->getData()['price'], $price);

        $oField->init();

        $this->assertNotEquals($this->oRow->getData()['price'], $price);

        $oField->loadData(['4' => $price]);

        $this->assertNotEquals($this->oRow->getData()['price'], $price);

        $oField->beforeSave();

        $this->assertNotEquals($this->oRow->getData()['price'], $price);

        $this->callPrivateMethod( $oField, 'execute', [] );

        $this->assertEquals($this->oRow->getData()['price'], $price);

        $oField->afterSave();

        $this->assertEquals($this->oRow->getData()['price'], $price);

        $oField->shutdown();

        $this->assertEquals($this->oRow->getData()['price'], $price);

    }


    /**
     * Title
     * @covers \skewer\build\Component\Import\Field\Title::beforeSave
     * @covers \skewer\build\Component\Import\Field\Title::getValue
     */
    public function testTitleField(){
        /** @var Title $oField */
        $oField = $this->getField( 'Title', 'title', '4' );

        $oConfig = $this->task->getConfig();

        $this->callPrivateMethod( $oField, 'setGoodsRow', [$this->oRow] );

        $title = 'Товарчик';

        $this->assertNotEquals($oConfig->getParam('current_title'), $title);
        $this->assertNotEquals($this->oRow->getData()['title'], $title);

        $oField->init();

        $this->assertNotEquals($oConfig->getParam('current_title'), $title);
        $this->assertNotEquals($this->oRow->getData()['title'], $title);

        $oField->loadData(['4' => $title]);

        $this->assertNotEquals($oConfig->getParam('current_title'), $title);
        $this->assertNotEquals($this->oRow->getData()['title'], $title);

        $oField->beforeSave();

        $this->assertEquals($oConfig->getParam('current_title'), $title);
        $this->assertNotEquals($this->oRow->getData()['title'], $title);

        $this->callPrivateMethod( $oField, 'execute', [] );

        $this->assertEquals($oConfig->getParam('current_title'), $title);
        $this->assertEquals($this->oRow->getData()['title'], $title);

        $oField->afterSave();

        $this->assertEquals($oConfig->getParam('current_title'), $title);
        $this->assertEquals($this->oRow->getData()['title'], $title);

        $oField->shutdown();

        $this->assertEquals($oConfig->getParam('current_title'), $title);
        $this->assertEquals($this->oRow->getData()['title'], $title);

    }


    /**
     * Empty Title
     * @covers \skewer\build\Component\Import\Field\Title::beforeSave
     * @covers \skewer\build\Component\Import\Field\Title::getValue
     */
    public function testTitleField2(){
        /** @var Title $oField */
        $oField = $this->getField( 'Title', 'title', '4' );

        $oConfig = $this->task->getConfig();

        $this->callPrivateMethod( $oField, 'setGoodsRow', [$this->oRow] );

        $title = 'Товарчик';

        $this->assertNotEquals($oConfig->getParam('current_title'), $title);
        $this->assertNotEquals($this->oRow->getData()['title'], $title);

        $oField->init();

        $this->assertNotEquals($oConfig->getParam('current_title'), $title);
        $this->assertNotEquals($this->oRow->getData()['title'], $title);

        $oField->loadData(['4' => '']);

        $this->assertNotEquals($oConfig->getParam('current_title'), $title);
        $this->assertNotEquals($this->oRow->getData()['title'], $title);

        $this->assertAttributeEquals( false, 'skipCurrentRow', $this->task );

        $oField->beforeSave();

        $this->assertAttributeEquals( true, 'skipCurrentRow', $this->task );

        $this->assertNotEquals($oConfig->getParam('current_title'), $title);
        $this->assertNotEquals($this->oRow->getData()['title'], $title);

        $this->callPrivateMethod( $oField, 'execute', [] );

        $this->assertNotEquals($oConfig->getParam('current_title'), $title);
        $this->assertNotEquals($this->oRow->getData()['title'], $title);

        $oField->afterSave();

        $this->assertNotEquals($oConfig->getParam('current_title'), $title);
        $this->assertNotEquals($this->oRow->getData()['title'], $title);

        $oField->shutdown();

        $this->assertNotEquals($oConfig->getParam('current_title'), $title);
        $this->assertNotEquals($this->oRow->getData()['title'], $title);

    }

}