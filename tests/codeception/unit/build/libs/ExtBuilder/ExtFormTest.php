<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 24.10.13
 * Time: 10:11
 */

class ExtFormTest extends PHPUnit_Framework_TestCase {

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() {
        Ext::init();
    }

    /**
     * Tears down the fixture, for example, closes a network connection.
     * This method is called after a test is executed.
     */
    protected function tearDown() {
    }

    /**
     * Отдает данные для построения
     * @covers ExtForm::getDesc4MultiSelect
     */
    public function testGetDesc4MultiSelect() {

        $sName = 'settest';
        $sTitle = 'Title';

        $aIn = array(
            'set' => 'set123',
            'un' => 'unique321'
        );

        $aValues = array( 'un' );

        $aOut = ExtForm::getDesc4MultiSelect( $aIn, $aValues, $sName, $sTitle );

        $this->assertCount( 2, $aOut );

        $aFirst = $aOut[0];

        $this->assertArrayHasKey( 'name', $aFirst );
        $this->assertArrayHasKey( 'view', $aFirst );
        $this->assertArrayHasKey( 'title', $aFirst );
        $this->assertArrayHasKey( 'boxLabel', $aFirst );
        $this->assertArrayHasKey( 'value', $aFirst );

        $this->assertSame( 'settest__check__set', $aFirst['name'] );
        $this->assertSame( 'set123', $aFirst['boxLabel'] );
        $this->assertSame( 'check', $aFirst['view'] );
        $this->assertSame( $sTitle, $aFirst['title'] );
        $this->assertEmpty( $aFirst['value'] );

        $aSecond = $aOut[1];

        $this->assertArrayHasKey( 'name', $aSecond );
        $this->assertArrayHasKey( 'view', $aSecond );
        $this->assertArrayHasKey( 'title', $aSecond );
        $this->assertArrayHasKey( 'boxLabel', $aSecond );
        $this->assertArrayHasKey( 'hideEmptyLabel', $aSecond, 'нет флага скрытия метки' );

        $this->assertSame( 'settest__check__un', $aSecond['name'] );
        $this->assertSame( 'unique321', $aSecond['boxLabel'] );
        $this->assertSame( 'check', $aSecond['view'] );
        $this->assertFalse( $aSecond['title'] );
        $this->assertFalse( $aSecond['hideEmptyLabel'] );
        $this->assertNotEmpty( $aSecond['value'] );

    }

    /**
     * Проверяет данные, которые приходят из формы по набору галочек
     * @covers ExtForm::getMultiSelectValues
     */
    public function testGetMultiSelectValues() {

        $aData = array(
            'name' => 123,
            'title' => '321',
            'test__check__set1' => 1,
            'test__check__set2' => '',
            'test__check__set3' => true,
            'test__check__set4' => 0,
        );

        $sValues = ExtForm::getMultiSelectValues( $aData, 'test' );

        $this->assertCount( 2, $sValues );

        $this->assertContains( 'set1', $sValues );
        $this->assertContains( 'set3', $sValues );


    }

}
 