<?php

namespace test\build\libs\ft\proc\modificator;

use skewer\build\libs\ft;

class Call extends ft\proc\modificator\Prototype {

    /**
     * Набор произведенных действий
     * @var array
     */
    private $aActions = array();

    /**
     * Отдает список произведенных действий
     * @return array
     */
    public function getActionList() {
        return $this->aActions;
    }

    public function beforeAdd() {
        $this->aActions[] = 'beforeAdd';
    }

    public function afterAdd() {
        $this->aActions[] = 'afterAdd';
    }

    public function beforeUpd() {
        $this->aActions[] = 'beforeUpd';
    }

    public function afterUpd() {
        $this->aActions[] = 'afterUpd';
    }

    public function beforeDel() {
        $this->aActions[] = 'beforeDel';
    }

    public function afterDel() {
        $this->aActions[] = 'afterDel';
    }

} 