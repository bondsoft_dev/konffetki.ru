<?php

use skewer\build\libs\ft\TestHelper;

class ModificatorTest extends \PHPUnit_Framework_TestCase {

    /**
     * Sets up the fixture, for example, opens a network connection.
     * This method is called before a test is executed.
     */
    protected function setUp() {

//        require_once(__DIR__.'/../../TestHelper.php');
//
//        TestHelper::init();

    }

    /**
     * Тест выполнения модификаторов в принципе
     */
    public function testModificatorExec() {
//
//        $oQuery = TestHelper::getTable();
//
//        $oModel = $oQuery->getModel();
//        $oModel->getFiled('string')->addModificator('int');
//
//        $oItem = $oQuery->getNewRow();
//        $oItem->string = '123asd';
//        $iNewId = $oItem->save();
//
//        $this->assertSame( 123, $oItem->string, 'не сработал модификатор для текущей сущности' );
//
//        $this->assertNotEmpty( $iNewId, 'запись не добалена' );
//
//        $oItem2 = $oQuery->getRowById( $iNewId );
//
//        $this->assertSame( '123', $oItem2->string, 'не сработал модификатор (данные не добавлены верно)' );
//
//        $oItem2->string = '321jsdklfa';
//
//        $oItem2->save();
//
//        $oItem3 = $oQuery->getRowById( $iNewId );
//
//        $this->assertSame( '321', $oItem3->string, 'не сработал модификатор (данные не обновлены верно)' );
//
    }

//    /**
//     * Тест запуска модификаторов для нескольких AR
//     * @covers skewer\build\libs\ft\ArPrototype::insert
//     * @covers skewer\build\libs\ft\ArPrototype::update
//     * @covers skewer\build\libs\ft\ArPrototype::applyModificators
//     */
//    public function testModificatorForManyRows() {
//
//        require_once('Multi.php');
//
//        $oQuery = TestHelper::getTable();
//
//        $oModel = $oQuery->getModel();
//        $oModel->getFiled('string')->addModificator('test\build\libs\ft\proc\modificator\Multi');
//
//        $oItem1 = $oQuery->getNewRow();
//        $oItem1->string = '123asd';
//
//        $aModList = $oModel->getFiled('string')->getModificatorList( $oItem1 );
//        /** @var test\build\libs\ft\proc\modificator\Multi $oMod */
//        $oMod = $aModList[0];
//        $this->assertInstanceOf( 'test\build\libs\ft\proc\modificator\Multi', $oMod );
//
//        $this->assertEmpty( $oMod->getIds() );
//
//        $iNewId1 = $oItem1->save();
//
//        $oItem2 = $oQuery->getNewRow();
//        $oItem2->string = '123asd';
//        $iNewId2 = $oItem2->save();
//
//        $this->assertNotContains( $iNewId1, $oMod->getIds() );
//        $this->assertNotContains( $iNewId2, $oMod->getIds() );
//
//    }
//
//    /**
//     * Тест запуска всех модификаторов
//     * @covers skewer\build\libs\ft\ArPrototype::insert
//     * @covers skewer\build\libs\ft\ArPrototype::update
//     * @covers skewer\build\libs\ft\ArPrototype::applyModificators
//     */
//    public function testAllModificatorExec() {
//
//        require_once( 'Call.php' );
//
//        $oQuery = TestHelper::getTable();
//
//        $oModel = $oQuery->getModel();
//        $oModel->getFiled('string')->addModificator('test\build\libs\ft\proc\modificator\Call');
//
//        $oItem = $oQuery->getNewRow();
//        $oItem->string = '123asd';
//
//        $aModList = $oModel->getFiled('string')->getModificatorList( $oItem );
//        /** @var test\build\libs\ft\proc\modificator\Call $oMod */
//        $oMod = $aModList[0];
//        $this->assertInstanceOf( 'test\build\libs\ft\proc\modificator\Call', $oMod );
//
//        $this->assertNotContains( 'beforeAdd', $oMod->getActionList() );
//        $this->assertNotContains(  'afterAdd', $oMod->getActionList() );
//        $this->assertNotContains( 'beforeUpd', $oMod->getActionList() );
//        $this->assertNotContains(  'afterUpd', $oMod->getActionList() );
//        $this->assertNotContains( 'beforeDel', $oMod->getActionList() );
//        $this->assertNotContains(  'afterDel', $oMod->getActionList() );
//
//        $oItem->save();
//
//        $this->assertContains(    'beforeAdd', $oMod->getActionList() );
//        $this->assertContains(     'afterAdd', $oMod->getActionList() );
//        $this->assertNotContains( 'beforeUpd', $oMod->getActionList() );
//        $this->assertNotContains(  'afterUpd', $oMod->getActionList() );
//        $this->assertNotContains( 'beforeDel', $oMod->getActionList() );
//        $this->assertNotContains(  'afterDel', $oMod->getActionList() );
//
//        $oItem->save();
//
//        $this->assertContains(    'beforeAdd', $oMod->getActionList() );
//        $this->assertContains(     'afterAdd', $oMod->getActionList() );
//        $this->assertContains(    'beforeUpd', $oMod->getActionList() );
//        $this->assertContains(     'afterUpd', $oMod->getActionList() );
//        $this->assertNotContains( 'beforeDel', $oMod->getActionList() );
//        $this->assertNotContains(  'afterDel', $oMod->getActionList() );
//
//        $oItem->del();
//
//        $this->assertContains(    'beforeAdd', $oMod->getActionList() );
//        $this->assertContains(     'afterAdd', $oMod->getActionList() );
//        $this->assertContains(    'beforeUpd', $oMod->getActionList() );
//        $this->assertContains(     'afterUpd', $oMod->getActionList() );
//        $this->assertContains(    'beforeDel', $oMod->getActionList() );
//        $this->assertContains(     'afterDel', $oMod->getActionList() );
//
//    }

} 