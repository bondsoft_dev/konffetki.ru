<?php

namespace test\build\libs\ft\proc\modificator;

use skewer\build\libs\ft;

/**
 * Для тесто того, что модификатор запускается для нескольких AR, а создается один
 * Class Multi
 * @package test\build\libs\ft\proc\modificator
 */
class Multi extends ft\proc\modificator\Prototype {

    /**
     * Набор идентификаторов
     * @var array
     */
    private $aIds = array();

    /**
     * Отдает список id
     * @return array
     */
    public function getIds() {
        return $this->aIds;
    }

    public function beforeAdd() {
        $this->aIds[] = $this->oRow->getPrimaryKeyValue();
    }

} 