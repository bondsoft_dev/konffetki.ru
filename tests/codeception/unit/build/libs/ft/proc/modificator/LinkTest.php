<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20.03.14
 * Time: 18:38
 */

namespace test\build\libs\ft\proc\modificator;


use skewer\build\libs\ft\TestHelper;

class LinkTest extends \PHPUnit_Framework_TestCase {

    /**
     * @covers skewer\build\libs\ft\proc\modificator\Link::modify
     * @dataProvider providerModify
     */
    public function testModify( $sIn, $sOut ) {

//        require_once (__DIR__.'/../../TestHelper.php');
//
//        TestHelper::init();
//
//        $oRow = TestHelper::getTable()->getNewRow();
//
//        $oRow->getModel()->getFiled('string')->addModificator('Link');
//
//        $oRow->string = $sIn;
//        $oRow->save();
//
//        $this->assertSame( $sOut, $oRow->string );


    }

    public function providerModify () {
        return array(
            array( '[123]', '[123]' ),
            array( 'ya.ru', 'http://ya.ru' ),
            array( 'http://ya.ru', 'http://ya.ru' ),
            array( '/hello/', '/hello/' ),
        );
    }

}
 