<?php

/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 25.08.2015
 * Time: 18:07
 */
class TestHelper {

    /**
     * Вызвать private метод класса в PHP
     * @param object $object
     * @param $method
     * @param $args
     * @return mixed
     */
    public static function callPrivateMethod($object, $method, $args = [])
    {
        $classReflection = new \ReflectionClass(get_class($object));
        $methodReflection = $classReflection->getMethod($method);
        $methodReflection->setAccessible(true);
        $result = $methodReflection->invokeArgs($object, $args);
        $methodReflection->setAccessible(false);
        return $result;
    }

}