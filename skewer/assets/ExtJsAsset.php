<?php
namespace skewer\assets;

use yii\web\AssetBundle;
use skewer\build\Component\Site;

class ExtJsAsset extends AssetBundle
{
    public $sourcePath = '@skewer/build/libs/ExtJS/';
    public $css = [
        'css/ext-all.css',
        'js/ux/css/CheckHeader.css',
    ];
    public $js = [
        'js/fixDate.js',
        'js/ext-all.js',
        'js/ie-detect.js',
    ];
    public $depends = [
//        'yii\web\YiiAsset',
    ];

    public function init() {
        $this->js[] = 'js/ext-'.\Yii::$app->i18n->getTranslateLanguage().'.js';
        parent::init();
    }

}
