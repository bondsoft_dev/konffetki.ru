<?php
namespace skewer\assets;

use yii\web\AssetBundle;
use yii\web\View;

class JeegoocontextAsset extends  AssetBundle{

    public $sourcePath = '@skewer/build/libs/jquery/';
    public $css = [
    ];
    public $js = [
        'jquery.jeegoocontext.min.js',
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public $depends = [
        'skewer\assets\JqueryAsset'
    ];
}