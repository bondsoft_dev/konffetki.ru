<?php
namespace skewer\assets;


use yii\web\AssetBundle;
use yii\web\View;

class DatepickerAsset extends  AssetBundle{

    public $sourcePath = '@skewer/build/libs/datepicker/';
    public $css = [
        'jquery.datepicker.css',
    ];
    public $js = [
        'jquery.datepicker.js',
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public $depends = [
        'skewer\assets\JqueryAsset'
    ];

    public function init(){

        $this->js[] = 'jquery.datepicker-' . \Yii::$app->language . '.js';

        parent::init();

    }
}