<?php
namespace skewer\assets;

use yii\web\AssetBundle;
use yii\web\View;

class DesignAsset extends  AssetBundle{

    public $sourcePath = '@skewer/build/Design/Frame/web/';
    public $css = [
        'css/design/design.css'
    ];
    public $js = [
        'js/design/design.js',
        'js/design/designObj.js',
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public $depends = [
        'skewer\assets\JqueryAsset',
        'skewer\assets\JqueryUiAsset',
        'skewer\assets\JeegoocontextAsset',
    ];
}