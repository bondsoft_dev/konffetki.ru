<?php
namespace skewer\assets;

use yii\web\AssetBundle;

class ExtJsProcessManagerAsset extends AssetBundle
{
    public $sourcePath = '@skewer/build/libs/pmExtJS/';
    public $css = [
        'css/admin.css',
    ];
    public $js = [
    ];
    public $depends = [
        'skewer\assets\ExtJsAsset',
    ];
}
