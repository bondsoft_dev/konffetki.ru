<?php
namespace skewer\assets;

use yii\web\AssetBundle;

class CkEditorAsset extends AssetBundle
{
    public $sourcePath = '@skewer/build/libs/CKEditor/';
    public $css = [
    ];
    public $js = [
        'ckeditor.js',
        'ckInit.js'
    ];
    public $depends = [
    ];
}
