<?php
namespace skewer\assets;


use yii\web\AssetBundle;
use yii\web\View;

class FancyboxAsset extends  AssetBundle{

    public $sourcePath = '@skewer/build/libs/fancyBox/';

    public $css = [
        'jquery.fancybox.css'
    ];

    public $js = [
        'jquery.fancybox.pack.js',
        'jquery.mousewheel-3.0.6.pack.js'
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public $depends = [
        'skewer\assets\JqueryAsset'
    ];

    public function init(){

        $this->js[] = 'fancybox-' . \Yii::$app->language . '.js';

        parent::init();

    }
}