<?php
namespace skewer\assets;

use yii\web\AssetBundle;
use yii\web\View;

class JqueryAsset extends  AssetBundle{
    public $sourcePath = '@skewer/build/libs/jquery/';
    public $css = [
    ];
    public $js = [
        'jquery.js'
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];
}