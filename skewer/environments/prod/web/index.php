<?php

session_start();
// comment out the following two lines when deployed to production
defined('YII_DEBUG') or define('YII_DEBUG', false);
defined('YII_ENV') or define('YII_ENV', 'prod');

require_once(__DIR__.'/../config/constants.generated.php');

defined('RELEASEPATH') OR define('RELEASEPATH', ROOTPATH.'skewer/');

require_once(RELEASEPATH.'/config/constants.php');

require_once(RELEASEPATH . '/../vendor/autoload.php');
require_once(RELEASEPATH . '/../skewer/app/Yii.php');
require_once(RELEASEPATH . '/../skewer/app/Application.php');

$config = require(RELEASEPATH. '/config/web.php');

$app = (new \skewer\app\Application($config))->run();

exit;