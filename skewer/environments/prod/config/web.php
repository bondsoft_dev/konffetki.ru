<?php

$skewerParams = require(__DIR__.'/config.php');

/** @noinspection PhpIncludeInspection */
$config = [
    'id' => 'basic',
    'basePath' => ROOTPATH,
    'bootstrap' => ['log'],
    'language' => 'ru',
    'timeZone'=>'Europe/Moscow',
    'modules' => [
        'rest' => [
            'class' => '\skewer\modules\rest\Module',
        ]
    ],
    'aliases'=>[
        '@skewer' => RELEASEPATH,
    ],
    'controllerNamespace' => 'skewer\controllers',
    'components' => [
        'assetManager' => [
            'converter' => [
                'class' => 'skewer\app\SkewerCssConverter',
                'commands' => [
                    //'less' => ['css', 'lessc {from} {to} --no-color'],
                    //'ts' => ['js', 'tsc --out {to} {from}'],
                    'css' => ['css', '{from}']
                ],
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            'cookieValidationKey' => 'I_FPEadMsImj6M7FPaeT5t2WFAb6p6r8',
            'enableCsrfValidation' => false,
            'parsers' => [
                'application/json' => 'yii\web\JsonParser',
            ],
            'baseUrl'=>''
        ],
        'response' => [
            'charset' => 'UTF-8',
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
        'user' => [
            'identityClass' => 'app\models\User',
            'enableAutoLogin' => true,
        ],
        'errorHandler' => [
            'errorAction' => 'site/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@app/log/error.log',
                    'logVars' => [],
                    'maxFileSize' => 1024 * 2,
                    'maxLogFiles' => 3,
                ],
            ],
        ],
        'db' => require(ROOTPATH . '/config/config.db.php'),
        'i18n' => array(
            'class' => '\skewer\build\Component\I18N\I18N',
            'translations' => array(
                'app*' => [
                    'class' => '\skewer\build\Component\I18N\MessageSource',
                    'sourceLanguage' => 'ru',
                    'forceTranslation' => true,
                    'basePath' => '@webroot/cache/language',
                ],
                '*' => [
                    'class' => '\skewer\build\Component\I18N\MessageSource',
                    'sourceLanguage' => 'ru',
                    'forceTranslation' => true,
                    'basePath' => '@webroot/cache/language',
                ],
            ),
        ),
        'sections' => [
            'class' => '\skewer\build\Component\I18N\DBSections'
        ],
        'urlManager' => [
            'class' => 'yii\web\UrlManager',
            'enableStrictParsing' => false,
            'enablePrettyUrl' => true,
            'showScriptName' => false,
            'routeParam'=>'url',
            'suffix' => '/',
            'rules' => [
                'gii' => 'gii',
                'debug' => 'debug',
                ['class' => 'yii\rest\UrlRule', 'controller' => [
                    'rest/section',
                    'rest/catalog',
                    'rest/news',
                ], 'pluralize'=>false ],

                'ajax/ajax.php' => 'ajax/ajax',
                'ajax/captcha.php' => 'ajax/captcha',
                'ajax/uploader.php' => 'ajax/uploader',

                'cron' => 'cron/index',
                'cron/index.php' => 'cron/index',

                'design' => 'design/index',
                'design/index.php' => 'design/index',
                'design/reset.php' => 'design/reset',

                'gateway' => 'gateway/index',
                'gateway/index.php' => 'gateway/index',

                'local' => 'local/index',
                'local/index.php' => 'local/index',

                'admin' => 'skewer/admin',
                'admin/index.php' => 'skewer/admin',

                'keepalive.php' => 'skewer/keepalive',

                'ajax/robokassa.php' => 'payment/index',
                'ajax/payment.php' => 'payment/index',
                'payment.php' => 'payment/index',

                //['route'=>'skewer/index','pattern'=>'<url:[a-zA-Z0-9-/_\W]*>/page/<page:[\d]*>', 'encodeParams'=>false],
                ['route'=>'site/index','pattern'=>'<url:[a-zA-Z0-9-/_\W\w]*>', 'encodeParams'=>false],
                /*
                'gii' => 'gii',

                '<cmd:\w+>/<action:\w+>'=>'<cmd>/<action>',
                '<cmd:\w+>/<action:\w+>/<state:\w+>'=>'<cmd>/<action>/<state>',
                '<cmd:\w+>/<action:\w+>/<state:\w+>/<sid:\w+>'=>'<cmd>/<action>/<state>/<sid>',
                */
                //'catchAll' => ['gii']
            ]
        ],
    ],
    'params' => $skewerParams,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    //$config['modules']['debug'] = 'yii\debug\Module';

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = 'yii\gii\Module';

    $config['bootstrap'][] = 'gii';
    //$config['modules']['gii'] = 'yii\gii\Module';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
        'allowedIPs' => [
            '192.168.0.*'
        ],
    ];

    //$config['components']['assetManager']['forceCopy'] = true;

    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
        'allowedIPs' => [
            '192.168.0.*'
        ],
    ];
}

// костылек для инсталятора, чтоб поддерживал корявый db конфиг
$db = $config['components']['db'];
if (isset($db['db'])){
    // опачки, старый конфиг
    // ничего страшнего, разложим как надо

    // сначала ударим старье нафиг
    unset($config['components']['db']['db']);

    // а теперь как надо
    $config['components']['db'] =
        [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host='.$db['db']['host'].';dbname='.$db['db']['name'],
            'username' => $db['db']['user'],
            'password' => $db['db']['pass'],
            'charset' => 'utf8',
        ];
}

/** Перекрытия */

/** @noinspection PhpIncludeInspection */
$localconf = require(ROOTPATH.'/config/web.php');

$config = \yii\helpers\ArrayHelper::merge($config, $localconf);

return $config;
