<?= '<?php' ?>


$localConfig = [
    <?php foreach ($config as $key => $value): ?>
        <?php $this->beginContent(__DIR__ . '/arrayView.php', ['key' => $key, 'value' => $value, 'level' => 1]); ?>
        <?php $this->endContent(); ?>
    <?php endforeach ?>
    <?= "\r\n" ?>

//
//    // настройка тестовых доменов
//    'params' => [
//          ...
//          'test_domains' => [
//                'doma.net'
//          ],
//    ],
//


];

return $localConfig;