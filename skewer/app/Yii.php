<?php

/**
 * Yii bootstrap file.
 * Перекрытие Yii для работы с phpstorm
 */

/**
 * Class Yii *
 */
class Yii extends \yii\BaseYii
{

    /**
     * @var \skewer\app\Application the application instance
     */
    public static $app;

    /**
     * Эта штуковина перекрыта для разруливания следующей ситуации:
     * Мультиязычный сайт. Работаем в
     * @inheritdoc
     */
    public static function t($category, $message, $params = [], $language = null)
    {
        if (static::$app !== null) {
            return static::$app->getI18n()->translate($category, $message, $params, $language ?: static::$app->i18n->getTranslateLanguage());
        } else {
            return parent::t($category, $message, $params, $language);
        }
    }

}

spl_autoload_register(['Yii', 'autoload'], true, true);
Yii::$classMap = include(RELEASEPATH . '../vendor/yiisoft/yii2/classes.php');
Yii::$container = new yii\di\Container;
