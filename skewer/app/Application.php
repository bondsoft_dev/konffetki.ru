<?php

namespace skewer\app;

use Auth;
use skAutoloader;
use skewer\build\Component\I18N\SectionsPrototype;
use skLinker;
use skRequest;
use skTwig;
use Yii;
use skewer\build\Component\Redirect;
use skewer\build\Component;
use yii\helpers\FileHelper;

/**
 * #stb_doc вообще нет комментариев
 * 1. Разобать логику работы
 * 2. Описать по каждому методу что он делает
 * 3. Для каждого метода проверить его необходимость здесь
 *
 * @property Component\I18N\I18N $i18n the internationalization application component
 * @property Component\I18N\SectionsPrototype $sections component to a service section
 * @property \skewer\core\Component\Config\BuildRegistry $register registry of installed modules and parts
 * @property Component\Environment $environment набор переменных среды для передачи между модулями
 * @property Component\ProcessList $processList список процессов
 * @property Component\Router\Router $router менеджер для работы с url (временный, потом будет urlManager)
 * @property Component\JsonResponse $jsonResponse менеджер для работы с json посылками (временный)
 *
 * @method Component\I18N\I18N getI18N Returns the internationalization application component
 *
 */
class Application extends \yii\web\Application {

    /**
     * @inheritDoc
     */
    public $layout = false;

    /**
     * @inheritDoc
     */
    public function init() {
        parent::init();
        $this->setViewPath('@skewer/views');
    }

    /**
     * @inheritdoc
     */
    public function handleRequest($request) {

        mb_internal_encoding("UTF-8");

        /** Регистрация skewer-автолоадера */
        $this->skAutoloaderInit();

        /** Инициализация языка */
        $this->languageInit();

        /** Редиректы */
        $this->redirect();

        /** Инициализация линкера */
        $this->skLinkerInit();

        /** Инициализация парсеров */
        $this->parserInit();

        /** Инициализация политик */
        $this->authInit();

        /** Инициализация skRequest */
        $this->skRequestInit();

        /** Инициализация логгера */
        $this->loggerInit();

        return parent::handleRequest($request);
    }


    /**
     * #stb_note #stb_move не в приложении должно быть подключение, а в Page\Main или типа того
     */
    public function coreComponents()
    {
        return array_merge(parent::coreComponents(), [
            'seo'=>[
                'class'=>'skewer\components\seo\Manager'
            ],
        ]);
    }

    /**
     * @return SectionsPrototype $sections component to a service section
     */
    public function getSections(){
        return $this->getComponents('section')[0];
    }

    /**
     * Редиректы
     * #stb_doc корневые редиректы - разобрать и описать логику работы
     */
    private function redirect()
    {
        Redirect\Api::checkRedirect();

        // #stb_note можно взять из вызывавшего метода
        $request = Yii::$app->getRequest();
        $params = $request->getQueryParams();
        $response = $this->getResponse();

        $url = explode("?", $request->getUrl())[0];

        // для файлов с расширениями для роутера нужно убирать суффикс
        preg_match('/^(.*)\.(.*)$/', $url, $return);

        if ($return) {
            Yii::$app->getUrlManager()->suffix = '';
            return;
        } else if (empty($params) && substr($request->getUrl(), -1) != '/') {

            // для других файлов в конце подставляем слеш
            $response->redirect($request->getAbsoluteUrl() . '/')->send();
            exit;
        } else {
            if (!empty($params) && substr($url, -1) != '/') {
                $response->redirect($url . '/')->send();
                exit;
            }
        }
    }

    /**
     * Очищает директорию assets cо скомпилированными файлами клиентской части
     */
    public function clearAssets() {
        // очистим все assets
        FileHelper::removeDirectory(WEBPATH.'assets/');
        FileHelper::createDirectory(WEBPATH.'assets/');
    }

    private function skAutoloaderInit()
    {
        require_once COREPATH . 'classes/skAutoloader.php';
        skAutoloader::registerPath(COREPATH, BUILDPATH);
    }

    private function languageInit()
    {
        /** Язык по-умолчанию */
        if (!is_null(\SysVar::get('language'))) {
            $this->language = \SysVar::get('language');
        }
    }

    private function parserInit()
    {
        skTwig::Load(
            array(),
            \Yii::$app->params['cache']['rootPath'] . 'Twig/',
            \Yii::$app->params['debug']['parser']
        );
    }

    private function skLinkerInit()
    {
        skLinker::init(COREPATH);
    }

    private function authInit()
    {
        Auth::init();
    }

    private function skRequestInit()
    {
        skRequest::init();
    }

    private function loggerInit()
    {
        \skLogger::init(ROOTPATH . 'log/access.log');
    }

}
