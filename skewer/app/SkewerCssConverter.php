<?php


namespace skewer\app;


use yii\base\Component;
use yii\web\AssetConverterInterface;

class SkewerCssConverter extends Component implements AssetConverterInterface {

    /**
     * @var array the commands that are used to perform the asset conversion.
     * The keys are the asset file extension names, and the values are the corresponding
     * target script types (either "css" or "js") and the commands used for the conversion.
     *
     * You may also use a path alias to specify the location of the command:
     *
     * ```php
     * [
     *     'styl' => ['css', '@app/node_modules/bin/stylus < {from} > {to}'],
     * ]
     * ```
     */
    public $commands = [];
    /**
     * @var boolean whether the source asset file should be converted even if its result already exists.
     * You may want to set this to be `true` during the development stage to make sure the converted
     * assets are always up-to-date. Do not set this to true on production servers as it will
     * significantly degrade the performance.
     */
    public $forceConvert = false;

    /**
     * Converts a given asset file into a CSS or JS file.
     * @param string $asset the asset file path, relative to $basePath
     * @param string $basePath the directory the $asset is relative to.
     * @return string the converted asset file path, relative to $basePath.
     */
    public function convert($asset, $basePath) {
        $pos = strrpos($asset, '.');

        if ($pos !== false) {

            $ext = substr($asset, $pos + 1);

            if ($ext=='css'){

                $fileName = substr($asset,0,strlen($asset)-4);
                $newFileName = $fileName . '.compile.css';
                $fullFileName = $basePath . '/' . $newFileName;

                // перестраиваем если файла нет или нужно обновить css
                if ( !is_file($fullFileName) || \Yii::$app->params['debug']['css'] ){

                    $oCSSParser = new \CSSParser();
                    $oDesignManager = new \DesignManager();

                    $oCSSParser->analyzeFile( "$basePath/$asset" );

                    if (\Yii::$app->params['debug']['asset'])
                        $oCSSParser->updateDesignSettings();

                    $oCSSParser->aParams = $oDesignManager->getParams(true);

                    $content = $oCSSParser->parseFile("$basePath/$asset");

                    $h = fopen($fullFileName, 'w');
                    fwrite($h, $content);
                    fclose($h);

                }

                return $newFileName;

            }
        }
        return $asset;
    }
}