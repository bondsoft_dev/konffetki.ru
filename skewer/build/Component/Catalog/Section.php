<?php

namespace skewer\build\Component\Catalog;

use skewer\build\Component\orm\Query;
use skewer\build\Component\Section\Parameters;
use skewer\build\Component\Site;
use skewer\models\Parameters as ParamModel;


/**
 * Класс для работы с каталожными разделами
 * Class Section
 * @package skewer\build\Component\Catalog
 */
class Section {


    /**
     * Список каталожных разделов
     * @return array
     */
    public static function getList() {

        $list = Parameters::getListByModule('CatalogViewer', 'content');

        $oQuery = Query::SelectFrom( 'tree_section' )
            ->where( 'id', $list )
            ->where( 'parent<>?', \Yii::$app->sections->templates() )
            ->asArray()
        ;

        $aList = array();
        $aSearchList = self::getSearchList();
        $aCollectionList = self::getCollectionList();

        while ( $aItem = $oQuery->each() ) {
            if ( $aItem['id'] && $aItem['title'] && !isSet($aSearchList[$aItem['id']]) && !isSet($aCollectionList[$aItem['id']]) )
                $aList[$aItem['id']] = $aItem['title'];
        }

        return $aList;
    }


    /**
     * Список поисковых каталожных разделов
     * @return array
     */
    public static function getSearchList() {

        /**
         * @todo это точно так должно быть?
         */
        $oQuery = Query::SelectFrom( 'tree_section' )
            ->fields( 'tree_section.*' )
            ->join( 'inner', 'parameters', 'parameters', 'tree_section.id=parameters.parent' )
            ->on( 'name', 'searchCard' )
            ->asArray()
        ;

        $aList = array( 0 => '---' );

        while ( $aItem = $oQuery->each() ) {
            if ( $aItem['id'] && $aItem['title'] )
                $aList[$aItem['id']] = $aItem['title'];
        }

        return $aList;
    }


    /**
     * Список каталожных разделов с товарами коллекции
     * @return array
     */
    public static function getCollectionList() {

        /**
         * @todo это точно так должно быть?
         */
        $oQuery = Query::SelectFrom( 'tree_section' )
            ->fields( 'tree_section.*' )
            ->join( 'inner', 'parameters', 'parameters', 'tree_section.id=parameters.parent' )
            ->on( 'name', 'collectionField' )
            ->asArray()
        ;

        $aList = [ 0 => '---' ];

        while ( $aItem = $oQuery->each() ) {
            if ( $aItem['id'] && $aItem['title'] )
                $aList[$aItem['id']] = $aItem['title'];
        }

        return $aList;
    }


    /**
     * Список разделов вывода для товарной позиции
     * @param int $iGoodId Ид товарной позиции
     * @param int $iBaseCard ID базовой карточки
     * @return array|bool
     */
    public static function getList4Goods( $iGoodId, $iBaseCard ) {
        return model\SectionTable::get4Goods( $iGoodId, $iBaseCard );
    }


    /**
     * Привязка товара к разделу
     * @param int $iSectionId Ид раздела
     * @param int $iGoodId Ид товарной позиции
     * @param int $iBaseCard Ид базовой карточки
     * @param int $iExtCard Ид карточки
     * @return bool
     */
    public static function addGoods( $iSectionId, $iGoodId, $iBaseCard, $iExtCard ) {
        return model\SectionTable::link( $iSectionId, $iGoodId, $iBaseCard, $iExtCard );
    }


    /**
     * Отвязка товара от раздела
     * @param int $iSectionId Ид раздела
     * @param int $iGoodId Ид товарной позиции
     * @param int $iBaseCard Ид базовой карточки
     * @return bool
     */
    public static function removeGoods( $iSectionId, $iGoodId, $iBaseCard ) {
        return model\SectionTable::unlink( $iSectionId, $iGoodId, $iBaseCard );
    }


    /**
     * Установка основного раздела для товара
     * @param $iGoodsId
     * @param $iSectionId
     * @return bool
     */
    public static function setMain4Goods( $iGoodsId, $iSectionId ) {
        return model\GoodsTable::setMainSection( $iGoodsId, $iSectionId );
    }


    /**
     * Получение основного раздела для товара
     * @param $iGoodsId
     * @param int $iBaseCard
     * @return bool|mixed
     */
    public static function getMain4Goods( $iGoodsId, $iBaseCard = 1 ) {

        $iSectionId = model\GoodsTable::getMainSection( $iGoodsId );

        if ( !$iSectionId ) {
             if ( $aSectionList = self::getList4Goods( $iGoodsId, $iBaseCard ) ) {
                 $iSectionId = array_shift( $aSectionList );
                 self::setMain4Goods( $iGoodsId, $iSectionId );
             }
        }

        return $iSectionId;
    }


    /**
     * Набор карточек для раздела
     * @param int|array $section
     * @return int[]
     */
    public static function getCardList( $section ) {
        return model\SectionTable::cardList( $section );
    }

    /**
     * Актуализация связей товара с разделами
     * @param int $iGoodsId Ид товарной позиции
     * @param int $iBaseCard Ид базовой карточки товара
     * @param int $iExtCard Ид карточки товара
     * @param int[] $aSectionList Список инедтификаторов разделов
     */
    public static function save4Goods( $iGoodsId, $iBaseCard, $iExtCard, $aSectionList ) {

        $sections = array();
        foreach ( $aSectionList as $iSection ) {
            if ( (int)$iSection )
                $sections[] = (int)$iSection;
        }

        $aCurSectionList = Section::getList4Goods( $iGoodsId, $iBaseCard );

        foreach ( $sections as $iSection ) {

            if ( !$aCurSectionList || !in_array( $iSection, $aCurSectionList ) ) {
                Section::addGoods( $iSection, $iGoodsId, $iBaseCard, $iExtCard );
            }


        }

        if ( $aCurSectionList )
            foreach( $aCurSectionList as $iSection ) {

                if ( !in_array( $iSection, $sections ) ) {
                    Section::removeGoods( $iSection, $iGoodsId, $iBaseCard );
                }

            }
    }

    /**
     * Очистка основного раздела
     * @param int $iSectionId Ид раздела
     * @return bool
     */
    public static function clearSection( $iSectionId ) {
        model\GoodsTable::removeSection( $iSectionId );
        return true;
    }


    /**
     * Получение карточки для нового товара в разеделе $section
     * @param int $section Идентификатор раздела
     * @return string
     */
    public static function getDefCard( $section ) {

        return Parameters::getValByName($section, 'content', 'defCard');

    }


    /**
     * Сохранение карточки для новых товаров в разделе $section
     * @param int $section
     * @param $card
     * @return bool
     */
    public static function setDefCard( $section, $card ) {

        $oParam = Parameters::getByName($section, 'content', 'defCard');

        if ( !$oParam )
            $oParam = Parameters::createParam([
                'parent' => $section,
                'group' => 'content',
                'name' => 'defCard',
            ]);

        $oParam->value = $card;

        return $oParam->save();
    }


    /**
     * Сохранение карточки для новых товаров в разделе $section
     * @param int $section
     * @param $name
     * @param $value
     * @return bool
     */
    public static function setParam( $section, $name, $value ) {

        $oParam = Parameters::getByName( $section, 'content', $name );

        if ( !$oParam )
            $oParam = Parameters::createParam([
                'parent' => $section,
                'group' => 'content',
                'name' => $name,
            ]);

        $oParam->value = $value;

        return $oParam->save();
    }


    /**
     * Получить раздел в котором находится коллекция
     * @param int $card Идентификатор сущности коллекции
     * @return int
     */
    public static function get4Collection( $card ) {

        $out = 0;

        // ищем разделы с коллекциями
        $query = ParamModel::find()->where(['name' => 'collectionField']);
        foreach ( $query->each() as $param ) {

            list( $curCard, $curField ) = explode( ':', $param->value );

            if ( $card == $curCard )
                $out = $param->parent;

            // fixme не решена проблема с одноименными полями в разных карточках
        }

        return $out;
    }


    /**
     * Получить раздел с коллекцией связанной с полем $field
     * @param string $field Название поля карточки
     * @return int
     */
    public static function get4CollectionField( $field ) {

        $out = 0;

        // ищем разделы с коллекциями
        $query = ParamModel::find()->where(['name' => 'collectionField']);
        foreach ( $query->each() as $param ) {

            list( $curCard, $curField ) = explode( ':', $param->value );

            if ( $field == $curField )
                $out = $param->parent;

            // fixme не решена проблема с одноименными полями в разных карточках
        }

        return $out;
    }


} 