<?php

namespace skewer\build\Component\Catalog;

use skewer\build\libs\ft;
use yii\helpers\ArrayHelper;



class Dict{

    /** метка модуля для справочника брендов */
    const BRAND_MODULE = 'CatalogBrand';

    /**
     * Отдает класс для работы с таблицей справочника по имени поля и карточке
     * @param $sFieldName
     * @param $scCardName
     * @return bool|\skewer\build\Component\orm\MagicTable
     */
    public static function getDictTable4Field( $sFieldName, $scCardName ){

        $oModel = ft\Cache::get( $scCardName );

        $oDictField = false;

        foreach ( $oModel->getFileds() as $oField )
            if ($oField->getName() == $sFieldName)
                $oDictField = $oField;

        if ( !$oDictField && $oModel->getType() == Card::TypeExtended ) {

            $oParentModel = ft\Cache::get( $oModel->getParentId() );

            foreach ( $oParentModel->getFileds() as $oField )
                if ($oField->getName() == $sFieldName)
                    $oDictField = $oField;

        }

        if ($oDictField){
            $oRel = $oDictField->getModel()->getOneFieldRelation( $oDictField->getName() );
            if ($oRel){
                return ft\Cache::getMagicTable( $oRel->getEntityName() );
            }
        }

        return false;

    }

}