<?php

namespace skewer\build\Component\Catalog;

use skewer\build\libs\ft;
use skewer\build\Component\Cache;


/**
 * Прототип классов для выборки данных из каталожных сущностей
 * Class SelectorPrototype
 * @package skewer\build\Component\Catalog
 */
abstract class SelectorPrototype {

    /** @var \skewer\build\Component\orm\state\StateSelect Объект запросника. Используется для поиска списка товаров. */
    protected $oQuery = null;

    /** @var string Имя базовой карточки */
    protected $sBaseCard = false;

    /** @var string Имя расширенной карточки */
    protected $sExtCard = false;

    /** @var bool Флаг вывода полей из расширенной карточки */
    protected $bUseExtCard = false;

    /** @var ft\model\Field[] Список полей объекта вывода */
    protected $aCardFields = [];

    /** @var Parser Парсер товаров */
    protected $oParser = null;


    protected $entityType = 0;// todo нужет тип сущности для добавления префикса таблицы


    /** @var bool Флаг сортировки по внутреннему порядку */
    protected $bSorted = false; // todo не вписывается в общую механику



    /**
     * Инициализация полей карточек для выборки
     * @param $card
     * @throws Exception
     * @throws ft\Exception
     */
    protected function selectCard( $card ) {

        $fields = [];

        if ( $oModel = ft\Cache::get( $card ) ) {

            $this->entityType = $oModel->getType();

            $bIsExtCard = false;

            if ( $oModel->getType() == Card::TypeExtended ) {

                $bIsExtCard = true;

                $oParentModel = ft\Cache::get( $oModel->getParentId() );

                $this->sBaseCard = $oParentModel->getName();

                foreach ( $oParentModel->getFileds() as $oField )
                    $fields[ $oField->getName() ] = $oField;

            }

            if ( $bIsExtCard )
                $this->sExtCard = $oModel->getName();
            else
                $this->sBaseCard = $oModel->getName();

            foreach ( $oModel->getFileds() as $oField ) {

                $this->bUseExtCard = $bIsExtCard;

                $fields[ $oField->getName() ] = $oField;

            }

        } else
            throw new Exception( 'Card not found' );

        $this->aCardFields = $fields;
    }


    /**
     * Инициализация парсера
     * @param array $attr Набор атрибутов для ограничения выборки
     */
    protected function initParser( $attr = [] ) {
        $this->oParser = Parser::get( $this->aCardFields, $attr );
    }


    /**
     * Кеширование текущих полей (для вывода в панелях фильтра)
     */
    protected function cacheFields() {
        Cache\Api::set( 'Catalog.Fields', $this->aCardFields );
    }



    /**
     * Добавление условия в выборку
     * @param string $fieldName Имя поля в выборке
     * @param bool|string|array $fieldValue Значение поля в выборке
     * @return $this
     * @throws Exception
     */
    public function condition( $fieldName, $fieldValue = true ) {

        if ( !$this->oQuery )
            throw new Exception( 'Ошибка при задании параметров' );

        $this->oQuery->where( $fieldName, $fieldValue );

        return $this;
    }


    /**
     * Добавление условия на значение поля в выборке
     * @param string $fieldName Имя поля в выборке
     * @param bool|string|array $fieldValue Значение поля в выборке
     * @return bool
     */
    public function fieldCondition( $fieldName, $fieldValue = true ) {

        // если поля не найдено - добавляем без валидации значения
        // todo доработать этот сценарий
        if ( !isSet( $this->aCardFields[$fieldName] ) ) {
            $this->oQuery->where( $fieldName, $fieldValue );
            return true;
        }

        $oField = $this->aCardFields[$fieldName];

        switch ( $oField->getEditorName() ) {
            case 'check':
                $this->oQuery->where( $fieldName, (int)($fieldValue == 1) );
                break;
            case 'money':
                if ( strpos( $fieldValue, '-' ) )
                    $this->oQuery->where( $fieldName . ' BETWEEN ?', explode( '-', $fieldValue ) );
                else
                    $this->oQuery->where( $fieldName, (int)$fieldValue );
                break;
            case 'int':// todo проверить работоспособность
                $this->oQuery->where( $fieldName, (int)$fieldValue );
                break;
            default:
                $this->oQuery->where( $fieldName . ' LIKE ?', '%' . $fieldValue . '%' );
        }

        return $this;
    }


    /**
     * Задание условий на сортировку
     * @param string $sFieldName
     * @param string $sWay
     * @return $this
     */
    public function sort( $sFieldName, $sWay = 'ASC' ) {

        if ( $sFieldName ) {

            if ( !count( $this->aCardFields ) || isSet($this->aCardFields[$sFieldName]) ) {
                $this->oQuery->order( $sFieldName, $sWay );
                $this->bSorted = false;
            }

        }

        return $this;
    }


    /**
     * Сортировка в хаотичном порядке
     * @return $this
     */
    public function sortByRand() {

        $this->oQuery->order( 'RAND()' );

        return $this;
    }


    /**
     * Исключает из выборки товары из списка $list
     * @param int[] $list Список идентификаторов товаров
     * @return $this
     */
    public function withOut( $list ) {
        $this->oQuery->where( 'co_' . $this->sBaseCard . '.id NOT IN ?', $list );
        return $this;
    }


    /**
     * Ограничение при постраничном просмотре
     * @param $iCount
     * @param $iPage
     * @param int $iAllCount
     * @throws Exception
     * @return GoodsSelector
     */
    public function limit( $iCount, $iPage = 1, &$iAllCount = 0 ) {

        if ( !$this->oQuery )
            throw new Exception( 'Ошибка при задании параметров' );

        if ( $iCount )
            $this->oQuery->limit( $iCount, $iCount * ( $iPage - 1 ) );
        else throw new Exception( 'Limit 0 in query' );


        // получение общего кол-ва элементов
        $this->oQuery->setCounterRef( $iAllCount );

        return $this;
    }

}