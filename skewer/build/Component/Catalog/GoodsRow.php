<?php

namespace skewer\build\Component\Catalog;

use skewer\build\Catalog\Goods\Search;
use skewer\build\libs\ft;
use skewer\build\Component\orm\Query;
use skewer\build\Component\orm\ActiveRecord;
use skewer\build\Component\Catalog;
use \yii\base\ModelEvent;
use skewer\build\Component;


/**
 * класс для работы с товаром, состоящим из нескольких карточек
 * @package skewer\build\libs\Catalog\model
 */
class GoodsRow extends GoodsRowPrototype {


    /**
     * Создание нового товара
     * @param $card
     * @return GoodsRow
     * @throws ft\Exception
     */
    public static function create( $card ) {

        $oGoodsRow = new self();

        $sBaseCard = Card::getBaseCard( $card );

        // base row
        $oBaseModel = ft\Cache::get( $sBaseCard );
        $oBaseRow = ActiveRecord::getByFTModel( $oBaseModel );

        $oGoodsRow->setBaseRow( $oBaseRow );
        $oGoodsRow->setBaseCardName( $sBaseCard );

        // ext row
        $oExtModel = ft\Cache::get( $card );
        $oExtRow = ActiveRecord::getByFTModel( $oExtModel );

        $oGoodsRow->setExtRow( $oExtRow );
        $oGoodsRow->setExtCardName( $card );

        return $oGoodsRow;
    }


    /**
     * Получить объект товара по id
     * @param $id
     * @param string $card
     * @return GoodsRow
     * @throws \Exception
     * @throws ft\Exception
     * todo обработка карточки
     */
    public static function get( $id, $card = '' ) {

        $oGoodsRow = new self();

        if ( ! ( $row = Catalog\model\GoodsTable::get( $id ) ) )
            return false;

        $oModel = ft\Cache::get( $row['base_card_name'] );

        if ( !$oModel )
            throw new \Exception('Не найдена модель для карточки.');

        $oBaseRow = Query::SelectFrom( $oModel->getTableName(), $oModel )
            ->where( $oModel->getPrimaryKey(), $id )
            ->getOne();

        $oGoodsRow->setRowId( $id );
        $oGoodsRow->setBaseRow( $oBaseRow );
        $oGoodsRow->setBaseCardName( $row['base_card_name'] );

        // ext row
        $oExtModel = ft\Cache::get( $row['ext_card_name'] );
        $oExtRow = Query::SelectFrom( $oExtModel->getTableName(), $oExtModel )
            ->where( $oBaseRow->primaryKey(), $oBaseRow->getPrimaryKeyValue() )
            ->getOne();

        $oGoodsRow->setExtRow( $oExtRow );
        $oGoodsRow->setExtCardName( $row['ext_card_name'] );

        $oGoodsRow->setMainRowId( $row['parent'] );

        $oGoodsRow->fillLinkFields();

        return $oGoodsRow;
    }


    public static function getByAlias( $alias, $card = '' ) {

        if ( !($oModel = ft\Cache::get( $card )) )
            throw new \Exception('Не найдена модель для карточки.');

        if ( !($oBaseRow = Card::getItemRow( $card, ['alias' => $alias] )) )
            return false;

        return self::get( $oBaseRow->getPrimaryKeyValue(), $card );
    }


    public static function getByFields( $aFields, $card = '' ) {

        if ( !($oModel = ft\Cache::get( $card )) )
            throw new \Exception('Не найдена модель для карточки.');

        if ( !($oBaseRow = Card::getItemRow( $card, $aFields )) )
            return false;

        return self::get( $oBaseRow->getPrimaryKeyValue(), $card );
    }


    /**
     * Изменение значений полей товара
     * @param array $aData
     */
    public function setData( $aData ) {

        // генерация и сохранение алиаса
        if ( $this->oBaseRow->getModel()->hasField( 'alias' ) )
            $this->checkAlias( $aData );


        // перебираем все пришедшие данные
        foreach ( $aData as $sKey => $mVal ) {

            if ( $this->oBaseRow->getModel()->hasField($sKey) )
                $this->oBaseRow->$sKey = $mVal;

            if ( $this->oExtRow and $this->oExtRow->getModel()->hasField($sKey) )
                $this->oExtRow->$sKey = $mVal;

        }

    }


    /**
     * Получение значений полей товара
     * Данные базвовой как есть,
     * @return array
     */
    public function getData() {

        $aData = array();

        if ( $this->hasBaseRow() )
            $aData = $this->getBaseRow()->getData();

        if ( $this->hasExtRow() ) {
            foreach ( $this->getExtRow()->getData() as $sKey => $mVal ) {
                $aData[ $sKey ] = $mVal;
            }
        }

        return $aData;
    }


    /**
     * Список разделов для показа
     * @return array|bool
     */
    public function getViewSection() {

        return Section::getList4Goods( $this->oBaseRow->getPrimaryKeyValue(), $this->getBaseCardId() );
    }


    /**
     * Актуализация связей товара с разделами и возвращает основной раздел
     * @param array $aSectionList
     * @return bool|int
     */
    public function setViewSection( $aSectionList = array() ) {
        Section::save4Goods( $this->getRowId(), $this->getBaseCardId(), $this->getExtCardId(), $aSectionList );
        return $this->getMainSection();
    }


    /**
     * Текущий главный раздел для товара
     * @return bool|int
     */
    public function getMainSection() {
        return Section::getMain4Goods( $this->getRowId(), $this->getBaseCardId() );
    }


    /**
     * Обновление главного раздела для товара
     * @param $iSectionId
     * @return bool
     */
    public function setMainSection( $iSectionId ) {
        return Catalog\model\GoodsTable::setMainSection( $this->getRowId(), $iSectionId );
    }


    /**
     * Сохранение товара
     * @return bool
     */
    public function save() {

        $bNew = !$this->iRowId;

        if ( !parent::save() )
            return false;

        if ( $bNew ) {
            $this->createLink();
        } else {

            $this->setUpdDate();

            if ( $this->isMainRow() ) {
                // для модификаций сохраняем не уникальные поля
                $data = [];
                $values = $this->getData();
                unSet( $values['id'] );
                foreach ( $this->getFields() as $oFtField ) {
                    $sFieldName = $oFtField->getName();
                    if ( !$oFtField->getAttr( 'is_uniq' ) && isSet($values[$sFieldName]) )
                        $data[$sFieldName] = $values[$sFieldName];
                }

                if ( count($data) ) {
                    $list = Catalog\GoodsSelector::getModificationList( $this->getRowId() )->getArray( $count );
                    foreach ( $list as $row ) {
                        $goods = self::get( $row['id'] );
                        $goods->setData( $data );
                        $goods->save();
                        // todo прокинуть сообщение об ошибке $goods->getErrorList()
                    }

                }
            }


        }

        return true;
    }


    /**
     * Удаление товара
     */
    public function delete() {

        if ( !$this->iRowId )
            return false;

        if ( !parent::delete() )
            return false;


        // todo удаление модификаций
        //$oQuery = Catalog\GoodsSelector::getModificationList( $iItemId, $iItemId, true )->asArray();
//        while ( $oAnalogRow = $oQuery->each() ) {
//            $iCurId = $oAnalogRow['id'];
//            if ( $iCurId != $iItemId )
//                self::deleteObject( $iCurId );
//        }

        // todo разрулить галереи
//        $aFields = $oRow->getFields();
//        $aVars = $oRow->getData();
//        foreach( $aFields as $oField ) {
//
//            if ( $oField->getEditorName() == 'gallery' ) {
//
//                if ( $iGalleryId = $aVars[$oField->getName()] )
//                    AlbumsApi::removeAlbum( $iGalleryId );
//            }
//
//        }

        Catalog\model\SemanticTable::remove( $this->iRowId, $this->getBaseCardId() );
        $this->setViewSection( array() );
        $this->removeLink();

        return true;
    }


    private function setUpdDate() {
        Catalog\model\GoodsTable::setChangeDate( $this->iRowId, $this->getBaseCardId() );
    }


    private function createLink() {
        Catalog\model\GoodsTable::add(
            $this->iRowId,
            $this->getBaseCardId(),
            $this->getBaseCardName(),
            $this->getExtCardId(),
            $this->getExtCardName(),
            $this->isMainRow() ? $this->getRowId() : $this->getMainRowId()
        );
    }


    private function removeLink() {
        Catalog\model\GoodsTable::remove( $this->iRowId, $this->getBaseCardId() );
    }


    protected function checkAlias( &$aData ) {

        $sAlias = isSet($aData['alias']) ? $aData['alias'] : $this->getBaseRow()->getVal( 'alias' );
        $sTitle = isSet($aData['title']) ? $aData['title'] : $this->getBaseRow()->getVal( 'title' );

        if ( $this->iRowId ) {
            $sOldTitle = $this->getBaseRow()->getVal( 'title' );
            $sOldAlias = $this->getBaseRow()->getVal( 'alias' );
            // Alias должен создаваться заново при следующих условиях:
            // 1. title изменен
            // 2. alias задан и не изменен при редактировании и не изменен при сохранении
            // 3. старый title при транслитерации совпадает с alias
            if ( $sOldTitle != $sTitle && $sOldAlias == $sAlias ) {
                $oldAlias = \skTranslit::generateAlias( $sOldTitle );
                if ( $sOldAlias == $oldAlias || ( strpos($sOldAlias,$oldAlias)!==false ) )
                    $sAlias = $sTitle;
            }
        }

        if ( !$sAlias )
            $sAlias = $sTitle;

        $sAlias = \skTranslit::generateAlias( $sAlias );

        $aData['alias'] = $this->getUniqAlias( $sAlias );
    }


    protected function getUniqAlias( $sAlias ) {

        $flag = (bool)Query::SelectFrom( $this->getBaseRow()->getModel()->getTableName() )
            ->where( 'alias', $sAlias )
            ->andWhere( 'id!=?', $this->iRowId )
            ->getCount()
        ;

        if ( !$flag )
            return $sAlias;

        preg_match( '/^(\S+)(-\d+)?$/Uis', $sAlias, $res );
        $sTplAlias = isSet( $res[1] ) ? $res[1] : $sAlias;
        $iCnt = isSet( $res[2] ) ? -(int)$res[2] : 0;
        while ( substr( $sTplAlias, -1 ) == '-' )
            $sTplAlias = substr( $sTplAlias, 0 , strlen($sTplAlias) - 1 );

        do {

            $iCnt++;
            $sAlias = $sTplAlias . '-' . $iCnt;

            $flag = (bool)Query::SelectFrom( $this->getBaseRow()->getModel()->getTableName() )
                ->where( 'alias', $sAlias )
                ->andWhere( 'id!=?', $this->iRowId )
                ->getCount()
            ;

        } while ( $flag );


        return $sAlias;
    }


    /**
     * Получение значений для сложно связанных полей
     */
    protected function fillLinkFields() {

        foreach( $this->getFields() as $oField ) {

            $aRel = $oField->getRelationList();

            if ( count($aRel) )
                foreach ( $aRel as $oRel )
                    if ( $oRel->getType() == ft\Relation::MANY_TO_MANY ) {
                        $sField = $oField->getName();
                        $this->setData( [$sField => $oField->getLinkRow( $this->getRowId() )] );
                    }
        }

    }

    /**
     * Уладение всех связей товаров с разделом $iSectionId
     * @param ModelEvent $event
     */
    public static function removeSection( ModelEvent $event ) {
        model\SectionTable::removeSection( $event->sender->id );
        model\GoodsTable::removeSection( $event->sender->id );
    }

    /**
     * Класс для сборки списка автивных поисковых движков
     * @param Component\Search\GetEngineEvent $event
     */
    public static function getSearchEngine( Component\Search\GetEngineEvent $event ) {
        $event->addSearchEngine( Search::className() );
    }

}