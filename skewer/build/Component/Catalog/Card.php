<?php

namespace skewer\build\Component\Catalog;


use skewer\build\Component\Gallery\Format;
use yii\helpers\ArrayHelper;
use skewer\build\libs\ft;


/**
 * API для работы с карточками, словарями
 * Class Card
 * @package skewer\build\Component\Catalog
 * todo перенести методы работы с полями, группами в entity
 */
class Card extends Entity {

    /** дефолтная базовая карточка для каталога */
    const DEF_BASE_CARD = 'base_card';

    /** Имя модуля для карточки */
    const ModuleName = 'Catalog';

    const DEF_GOODS_MODULE = 'Catalog';
    const DEF_COLLECTION_MODULE = 'Collection';


    /**
     * Все справочники каталога
     * @return model\EntityRow[]
     */
    public static function getDictionaries() {
        return model\EntityTable::find()
            ->where( 'type', self::TypeDictionary )
            ->andWhere( 'module', self::DEF_GOODS_MODULE )
            ->getAll();
    }


    /**
     * Все коллекции каталога
     * @return model\EntityRow[]
     */
    public static function getCollections() {
        return model\EntityTable::find()
            ->where( 'type', self::TypeDictionary )
            ->andWhere( 'module', self::DEF_COLLECTION_MODULE )
            ->getAll();
    }

    /**
     * Отдает запись коллекции по имени
     * @param string $sName
     * @return model\EntityRow|bool
     */
    public static function getCollection( $sName ) {
        return model\EntityTable::find()
            ->where( 'type', self::TypeDictionary )
            ->andWhere( 'module', self::DEF_COLLECTION_MODULE )
            ->andWhere('name', $sName)
            ->getOne();
    }

    /**
     * Все товарные карточки
     * @param bool $bWithBase Флаг выборки базовых карточек
     * @return model\EntityRow[]
     */
    public static function getGoodsCards( $bWithBase = false ) {

        $aTypes = [self::TypeExtended];

        if ( $bWithBase )
            $aTypes[] = self::TypeBasic;

        return model\EntityTable::find()
            ->where( 'type', $aTypes )
            ->getAll();
    }


    /**
     * Список справочников каталога
     * @return array
     */
    public static function getDictionaryList() {
        $aOut = [];
        foreach ( self::getDictionaries() as $oEntity )
            $aOut[$oEntity->id] = $oEntity->title;
        return $aOut;
    }


    /**
     * Список коллекций каталога
     * @return array
     */
    public static function getCollectionList() {
        $aOut = [];
        foreach ( self::getCollections() as $oEntity )
            $aOut[$oEntity->id] = $oEntity->title;
        return $aOut;
    }


    /**
     * Список товарных карточек
     * @param bool $bKeyIsName Флаг использования в качестве ключа название
     * @return array
     */
    public static function getGoodsCardList( $bKeyIsName = false ) {
        $aOut = [];
        foreach ( self::getGoodsCards() as $oEntity )
            $aOut[$bKeyIsName ? $oEntity->name : $oEntity->id] = $oEntity->title;
        return $aOut;
    }


    /**
     * Получить title карточки
     * @param string $name Имя карточки
     * @return string
     */
    public static function getTitle( $name ) {

        if ( is_numeric($name) )
            $card = model\EntityTable::findOne(['id' => $name]);
        else
            $card = model\EntityTable::findOne(['name' => $name]);

        return $card ? ArrayHelper::getValue( $card, 'title', '' ) : '';
    }


    /**
     * Получиь техническое имя по id
     * @param int $id Идентификатор карточки
     * @return string
     */
    public static function getName( $id ) {
        $card = model\EntityTable::find()->where( 'id', $id )->fields( 'name' )->asArray()->getOne();
        return isset($card['name']) ? $card['name'] : '';
    }


    /**
     * Имя базовой карточки для $card
     * @param int|string $card Идентификатор карточки
     * @return string
     */
    public static function getBaseCard( $card ) {

        $oExtCard = self::get( $card );

        if ( !$oExtCard )
            return '';
            //fixme throw new Exception( "Не найдена карточка [$sExtCardName]" );

        if ( !$oExtCard->isExtended() )
            return $card;
            //fixme throw new Exception( "Сущность [$sExtCardName] не является расширяющей" );

        $oBaseCard = self::get( $oExtCard->parent );

        if ( !$oBaseCard )
            return '';
            //fixme throw new Exception( "Не найдена карточка c id [".$oExtCard->parent."]" );

        return $oBaseCard->name;
    }



    /**
     * @param $data
     * @return model\EntityRow
     * @throws \Exception
     * used Dictionary\Module::actionDictionaryAdd
     */
    public static function addDictionary( $data ) {

        $oCard = Card::get();

        $oCard->setData( $data );

        $oCard->type = self::TypeDictionary;
        $oCard->module = self::DEF_GOODS_MODULE;
        $oCard->save();

        // добавляем поле название
        self::setField( $oCard->id, 'title', \Yii::t('dict', 'title') );

        // генерим кеш
        $oCard->updCache();

        return $oCard;
    }



    public static function addCollection( $data ) {

        $oCard = Card::get();

        $oCard->setData( $data );

        $oCard->type = self::TypeDictionary;
        $oCard->module = self::DEF_COLLECTION_MODULE;
        $oCard->save();

        $iSeoGroup = self::setGroup( $oCard->id, 'seo', 'SEO параметры' );
        $iControlsGroup = self::setGroup( $oCard->id, 'controls', 'Элементы управления' );

        // добавляем поле название
        self::setField( $oCard->id, 'title', \Yii::t('collections', 'field_title') );
        self::setField( $oCard->id, 'alias', \Yii::t('collections', 'field_alias') );
        $row = self::setField( $oCard->id, 'gallery', \Yii::t('collections', 'field_gallery'), ft\Editor::GALLERY );
        $row->modificator = Format::TYPE_CATALOG4COLLECTION;
        $row->save();
        self::setField( $oCard->id, 'info', \Yii::t('collections', 'field_info'), ft\Editor::WYSWYG );

        self::setField( $oCard->id, 'tag_title', \Yii::t('collections', 'field_tag_title'), ft\Editor::STRING, $iSeoGroup->id );
        self::setField( $oCard->id, 'keywords', \Yii::t('collections', 'field_keywords'), ft\Editor::STRING, $iSeoGroup->id );
        self::setField( $oCard->id, 'description', \Yii::t('collections', 'field_description'), ft\Editor::STRING, $iSeoGroup->id );
        self::setField( $oCard->id, 'sitemap_priority', \Yii::t('collections', 'field_sitemap_priority'), ft\Editor::STRING, $iSeoGroup->id );
        self::setField( $oCard->id, 'sitemap_frequency', \Yii::t('collections', 'field_sitemap_frequency'), ft\Editor::STRING, $iSeoGroup->id );

        self::setField( $oCard->id, 'active', \Yii::t('collections', 'field_active'), ft\Editor::CHECK, $iControlsGroup->id );
        self::setField( $oCard->id, 'on_main', \Yii::t('collections', 'field_on_main'), ft\Editor::CHECK, $iControlsGroup->id );

        // генерим кеш
        $oCard->updCache();

        return $oCard;
    }



    /**
     * Получение объекта поля
     * @param null|int $id
     * @return model\FieldRow
     */
    public static function getField( $id = null ) {

        if ( !$id )
            return model\FieldTable::getNewRow();

        return model\FieldTable::find( $id );
    }


    /**
     * Добавление нового поля для сущности
     * @param int $card Идентификатор карточки
     * @param string $name Техническое имя поля
     * @param string $title Заголовок поля
     * @param string $editor Тип редактора для поля
     * @param int $group Идентификатор группы поля
     * @return model\FieldRow
     */
    public static function setField( $card, $name, $title, $editor = ft\Editor::STRING, $group = 0 ) {

        $oField = model\FieldTable::getNewRow();

        $oField->setData([
            'name' => $name,
            'title' => $title,
            'group' => $group,
            'editor' => $editor,
            'entity' => $card
        ]);

        $oField->save();

        return $oField;
    }


    /**
     * Добавление новой группы полей для сущности
     * @param int $card Идентификатор карточки
     * @param string $name Техническое имя поля
     * @param string $title Заголовок поля
     * @return model\FieldRow
     */
    public static function setGroup( $card, $name, $title ) {

        $oGroup = model\FieldGroupTable::getNewRow();

        $oGroup->setData([
            'entity' => $card,
            'name' => $name,
            'title' => $title,
        ]);

        $oGroup->save();

        return $oGroup;
    }


    /**
     * Получение объекта поля по имени и идетификатору карточки
     * @param int $card идентификатор карточки
     * @param string $name имя поля
     * @return model\FieldRow
     */
    public static function getFieldByName( $card, $name ) {
        return model\FieldTable::findOne( [ 'entity' => $card, 'name' => $name ] );
    }


    /**
     * Получение списка всех полей связанных с коллекциями
     * @return array
     */
    public static function getCollectionFields() {

        $coll = self::getCollectionList();

        $out = [];

        if ( !count($coll) )
            return $out;

        $fields = model\FieldTable::find()->where( 'link_id', array_keys( $coll ) )->getAll();

        foreach ( $fields as $field )
            $out[$field->link_id.':'.$field->name] = $coll[$field->link_id] . ' (' . $field->title . ')';

        return $out;
    }


    /**
     * Получение карточки для поля
     * @param $name
     * @return int
     */
    public static function get4Field( $name ) {

        $query = model\FieldTable::find()->where( ['name' => $name] );

        if ( $row = $query->each() )
            return $row->entity;

        return false;
    }


    /**
     * Получение объекта группы поля
     * @param null|int $id идентификатор группы
     * @return model\FieldGroupRow
     */
    public static function getGroup( $id = null ) {

        if ( !$id )
            return model\FieldGroupTable::getNewRow();

        return model\FieldGroupTable::find( $id );
    }


    /**
     * Получение объекта группы поля по имени и идетификатору карточки
     * @param int $card идентификатор карточки
     * @param string $name имя группы
     * @return model\FieldGroupRow
     */
    public static function getGroupByName( $card, $name ) {
        return model\FieldGroupTable::findOne( [ 'entity' => $card, 'name' => $name ] );
    }


    /**
     * Список групп для карточки
     * @param int $card идентификатор карточки
     * @return array
     * todo не используется (EntityRow->getGroupList())
     */
    public static function getGroupList( $card ) {

        $out = [0 => \Yii::t( 'catalog', 'base_group')];

        $query = model\FieldGroupTable::find()->where(['entity'=>$card]);

        /** @var model\FieldGroupRow $oGroup */
        while ( $oGroup = $query->each() )
            $out[ $oGroup->id ] = $oGroup->title;


        return $out;
    }

}