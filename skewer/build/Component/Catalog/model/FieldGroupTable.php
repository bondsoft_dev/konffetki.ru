<?php

namespace skewer\build\Component\Catalog\model;

use skewer\build\libs\ft;
use skewer\build\Component\orm\TablePrototype;


/**
 * Группы полей сущности
 * Class FieldGroupTable
 * @package skewer\build\Component\Catalog\model
 */
class FieldGroupTable extends TablePrototype {

    /** @var string Имя таблицы */
    protected static $sTableName = 'c_field_group';


    protected static function initModel() {

        ft\Entity::get('c_field_group')
            ->clear()
            ->setNamespace( __NAMESPACE__ )
            ->addField('name','varchar(255)','Системное имя')
            ->addField('title','varchar(255)','Название')
            ->addField('entity','int(11)','entity')
            ->addField('position','int(11)','position')
            ->save()
        ;

    }

    public static function getNewRow( $aData = array() ) {
        return new FieldGroupRow( $aData );
    }

}