<?php

namespace skewer\build\Component\Catalog\model;

use skewer\build\Component\orm\ActiveRecord;


/**
 * Запись атрибута для поля сущности
 * Class FieldAttrRow
 * @package skewer\build\Component\Catalog\model
 */
class FieldAttrRow extends ActiveRecord {

    public $id = 0;
    public $field = 0;
    public $tpl = 0;
    public $value = '';


    /**
     * Отдает имя таблицы
     * @return string
     */
    public function getTableName() {
        return 'c_field_attr';
    }


}