<?php

namespace skewer\build\Component\Catalog\model;

use skewer\build\libs\ft;
use skewer\build\Component\orm\ActiveRecord;
use skewer\build\Component\Catalog;


/**
 * Запись поля сущности
 * Class FieldRow
 * @package skewer\build\Component\Catalog\model
 */
class FieldRow extends ActiveRecord {

    public $id = 0;
    public $entity = 0;
    public $name = '';
    public $title = '';
    public $type = 'int';
    public $size = 0;
    public $link_type = '';
    public $link_id = 0;
    public $group = '';
    public $editor = '';
    public $validator = '';
    public $widget = '';
    public $modificator = '';
    public $def_value = '';
    public $position = 0;


    public function getTableName() {
        return 'c_field';
    }


    /**
     * Удалнеие подчиненных элементов
     * @return bool
     */
    public function preDelete() {

        // удаление атрибутов
        $query = FieldAttrTable::find()->where( 'field', $this->id );
        while ( $oItem = $query->each() )
            $oItem->delete();

        return true;
    }


    public function getGroupTitle() {

        /** @var FieldGroupRow $oGroupRow */
        $oGroupRow = FieldGroupTable::find( $this->group );

        return empty($oGroupRow) ? \Yii::t('catalog', 'base_group') : $oGroupRow->title;
    }


    public function save() {

        if ( !$this->entity )
            return false;

        // name
        $this->checkUniqueName();

        // type
        $this->type = ft\Editor::getTypeForEditor( $this->editor );

        // size
        if ( $this->type == 'varchar' && !$this->size )
            $this->size = '255';

        if ( in_array( $this->type, ['float','date','time'] ) )
            $this->size = '';

        // position
        $this->checkPos();

        // linked fields
        if ( $this->link_id and in_array( $this->editor, [ft\Editor::SELECT,ft\Editor::COLLECTION]) ) {
            $this->link_type = ft\Relation::ONE_TO_MANY;
        } elseif ( $this->link_id and ($this->editor == ft\Editor::MULTISELECT) ) {
            $this->link_type = ft\Relation::MANY_TO_MANY;
        } else {
            $this->link_id = 0;
            $this->link_type = '';
        }

        return parent::save();
    }


    private function checkUniqueName() {

        if ( !$this->name )
            $this->name = $this->title ?: 'f';

        $name = \skTranslit::genSysName( $this->name, 'f' );

        $i = '';
        do {
            $this->name = $name.$i;
            $res = FieldTable::findOne([
                'entity' => $this->entity,
                'name' => $name.$i,
                'id<>?' => $this->id
                ]
            );
            $i++;
        } while ( $res );

        return true;
    }


    private function checkPos() {

        if ( !$this->position ) {
            /** @var FieldRow $oField */
            $oField = FieldTable::find()->order( 'position', 'DESC' )->getOne();
            $this->position = $oField ? $oField->position + 1 : 1;
        }

    }


    /**
     * Атрибуты поля
     * @return array
     * используется при построении интерфейса редатирования товара
     * используется при создании модели сущности
     */
    public function getAttr() {
        
        $aList = array();

        $aTplList = Catalog\Attr::getList();

        foreach ( $aTplList as $aTpl ) {

            /** @var FieldAttrRow $oAttr */
            $oAttr = FieldAttrTable::findOne( ['tpl'=>$aTpl['id'],'field'=>$this->id] );

            // if ( !$oAttr ) continue; todo пропускаем только если шаблон не подходит под сущность и поле

            $aList[ $aTpl['id'] ] = [
                'id' => $aTpl['id'],
                'name' => $aTpl['name'],
                'title' => $aTpl['title'],
                'type' => $aTpl['type'],
                'value' => $oAttr ? $oAttr->value : $aTpl['default'] // todo нужно приведение значений к типу (проблема '0')
            ];
        }

        return $aList;
    }


    /**
     * Установка атрибута для поля
     * @param $iTpl
     * @param string $value
     * @return bool
     */
    public function setAttr( $iTpl, $value = '' ) {

        /** @var FieldAttrRow $oAttr */
        $oAttr = FieldAttrTable::findOne( ['field'=>$this->id, 'tpl'=>$iTpl] );

        if ( !$oAttr ) {

            $oAttr = FieldAttrTable::getNewRow();
            $oAttr->field = $this->id;
            $oAttr->tpl = $iTpl;
            $oAttr->value = $value;

            $bResult = $oAttr->save();

        } else {

            $oAttr->value = $value;

            $bResult = $oAttr->save();
        }

        return (bool)$bResult;
    }


    /**
     * Выборка объектов валидаторов для поля
     * @return mixed
     * todo возможно стоит избавиться от объектов
     */
    public function getValidators() {
        return ValidatorTable::find()->where( 'field', $this->id )->getAll();
    }


    /**
     * Список валидаторов для поля
     * @return array
     */
    public function getValidatorList() {

        $query = ValidatorTable::find()->where( 'field', $this->id );

        $out = [];

        /** @var ValidatorRow $oValidatorRow */
        while ( $oValidatorRow = $query->each() )
            $out[] = $oValidatorRow->name;

        return $out;
    }


    /**
     * Сохранение валидаторов для поля
     * @param $data
     * @throws \Exception
     */
    public function setValidator( $data ) {

        $aSaveValidList = array_intersect( explode( ',', $data ), array_keys( Catalog\Validator::getListWithTitles()) );

        $aCurrentValidList = ValidatorTable::find()->where( 'field', $this->id )->getAll();

        // добавить недостающие
        $aToAdd = array_diff( $aSaveValidList, $aCurrentValidList );
        foreach ( $aToAdd as $sName ) {
            $oValid = new ValidatorRow();
            $oValid->name = $sName;
            $oValid->field = $this->id;
            $oValid->save();
        }

        // удалить лишние
        $aToDel = array_diff( $aCurrentValidList, $aSaveValidList );
        foreach ( $aToDel as $sName )
            ValidatorTable::delete()->where( 'name', $sName )->where( 'field', $this->id )->get();

    }


    /**
     * @return ft\model\Field
     */
    public function getFTObject() {

        // hack - сейчас size не может сожержать два числа
        if ( $this->type == 'decimal' )
            $this->size = '12,2';

        $oFtField =  new ft\model\Field(
            $this->name,
            ft\model\Field::getBaseDesc(
                $this->size ? $this->type.'('.$this->size.')' : $this->type,
                $this->title
            )
        );

        foreach ( $this->getAttr() as $aAttr )
            $oFtField->setAttr( $aAttr['name'], $aAttr['value'] );

        if ( $this->editor )
            $oFtField->setEditor( $this->editor );

        foreach ( $this->getValidators() as $oValidator )
            $oFtField->addValidator( $oValidator->name );

        $oFtField->addWidget( $this->widget );

        // id группы
        $oFtField->setParameter( '__group_id'/*Card::ParamGroupId*/, $this->group );

        // добавление значения по умолчанию
        $oFtField->setDefault( $this->def_value );

        return $oFtField;
    }


    /**
     * Поле является ссылочным на сущность
     * @return bool
     */
    public function isLinked() {
        return $this->editor === ft\Editor::SELECT || $this->editor === ft\Editor::COLLECTION || $this->editor === ft\Editor::MULTISELECT;
    }

}