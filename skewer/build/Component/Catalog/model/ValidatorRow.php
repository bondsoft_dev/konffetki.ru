<?php

namespace skewer\build\Component\Catalog\model;

use skewer\build\Component\orm\ActiveRecord;


/**
 * Запись валидатора на поля сущностей
 * Class ValidatorRow
 * @package skewer\build\Component\Catalog\model
 */
class ValidatorRow extends ActiveRecord {

    public $id = 0;
    public $name = '';
    public $field = 0;

    public function getTableName() {
        return 'c_validator';
    }

    public function __toString() {
        return $this->name;
    }

}