<?php

namespace skewer\build\Component\Catalog\model;

use skewer\build\Component\Catalog\Card;
use skewer\build\Component\orm;

use skewer\build\libs\ft;
use yii\helpers\ArrayHelper;


class EntityRow extends orm\ActiveRecord {

    public $id = 0;
    public $name = '';
    public $title = '';
    public $module = '';
    public $type = 0;
    public $in_base = true;
    public $desc = '';
    public $parent = 0;
    public $cache = '';


    public function getTableName() {
        return 'c_entity';
    }


    public function save() {

        $this->checkUniqueName();// todo перенести в preSave


        return parent::save();
    }


    public function preDelete() {

        $query = FieldGroupTable::find()->where( 'entity', $this->id );
        while ( $row = $query->each() )
            $row->delete();

        $query = FieldTable::find()->where( 'entity', $this->id );
        while ( $row = $query->each() )
            $row->delete();


        // удаление связей со справочником
        if ( $this->type == Card::TypeDictionary ) {

            // список структурно изменненных сущностей
            $aEntity = [];

            // ищем поля по связи -<
            $query = FieldTable::find()->where(['link_id' => $this->id, 'link_type' => '-<']);

            /** @var FieldRow $oField */
            while ( $oField = $query->each() ) {
                $oField->link_type = '';
                $oField->link_id = 0;
                $oField->editor = 'string';
                $oField->save();

                $aEntity[ $oField->entity ] = 1;

            }

            // ищем поля по связи ><
            $query = FieldTable::find()->where(['link_id' => $this->id, 'link_type' => '><']);

            /** @var FieldRow $oField */
            while ( $oField = $query->each() ) {
                $oField->link_type = '';
                $oField->link_id = 0;
                $oField->editor = 'string';
                $oField->save();

                $aEntity[ $oField->entity ] = 1;

                // todo удалить таблицу связей
            }

            // сброс кеша для измененных сущностей
            foreach ( $aEntity as $id => $val ) {
                /** @var EntityRow $card */
                $card = EntityTable::find( $id );
                $card->updCache();
            }

        }

        if ( $this->type == Card::TypeBasic ) {
            GoodsTable::removeCard( $this->id );
            SectionTable::removeCard( $this->id );
        }

        if ( $this->type == Card::TypeExtended ) {

            if ( $oBaseCard = EntityTable::find( $this->parent ) )
                EntityTable::removeRowBaseByCard( $oBaseCard->name, $this->name );

            GoodsTable::removeCard( 0, $this->id );
            SectionTable::removeCard( 0, $this->id );
        }

        // todo удаление подчиненных карточек

        return true;
    }


    public function delete() {

        parent::delete();

        // удаление таблицы с данными
        $oModel = $this->getModelFromCache();

        $sTableName = $oModel->getTableName();

        // Удаление таблицы
        orm\Query::dropTable( $sTableName );

        return true;
    }


    /**
     * Получение объекта модели из кеша
     * @return ft\Model
     */
    public function getModelFromCache() {

        if ( !$this->cache ) {
            $this->save();
            $this->updCache();
        }

        $oJson = new ft\converter\Json();

        return $oJson->dataToFtModel( $this->cache );

    }


    /**
     * Получение списка полей карточки
     * @return FieldRow[]
     */
    public function getFields() {
        return FieldTable::find()->where( 'entity', $this->id )->order( 'position' )->getAll();
    }


    /**
     * Получение списка групп карточки
     * @return FieldGroupRow[]
     */
    public function getGroups() {
        return FieldGroupTable::find()->where( 'entity', $this->id )->order( 'position' )->getAll();
    }


    public function getGroupList( $bFullList = false ) {

        $aOut = [0 => \Yii::t('catalog' ,'base_group')];

        /** @var FieldGroupRow $oGroup */
        $query = FieldGroupTable::find()->where( 'entity', $this->id );

        if ( $bFullList )
            $query->orWhere( 'entity', $this->parent );

        while ( $oGroup = $query->each() )
            $aOut[ $oGroup->id ] = $oGroup->title;

        return $aOut;
    }


    public function updCache() {

        $sPreffix = ArrayHelper::getValue( ['cd_', 'co_', 'ce_'], $this->type,'co_' );

        // заголовок новой модели
        $oFtEntity =
            ft\Entity::get( $this->name )
                ->clear()
                ->setTablePrefix( $sPreffix )
                ->setTableType( ft\DBTable::TypeInnoDb )
                ->setParentId( $this->parent )
                ->setType( $this->type )
        ;

        $query = FieldTable::find()
            ->where( 'entity', $this->id )
            ->order( 'position' );

        /** @var FieldRow $oField */
        while ( $oField = $query->each() ) {

            // добавляем поле
            $oFtEntity->addFieldObject( $oField->getFTObject() );

            // обработка полей со связями
            if ( $oField->link_id ) {

                /** @var EntityRow $oRelationEntity */
                $oRelationEntity = EntityTable::find( $oField->link_id );

                if ( !$oRelationEntity )
                    throw new \Exception( sprintf( 'Не найдена связанная сущность при сохранении поля "%s" (%s)', $oField->title, $oField->name));

                $oFtEntity->addRelation( $oField->link_type, $oRelationEntity->name, $oField->name, 'id', 'id' );
            }

            // Добавление индекса для системных полей
            if ( in_array($oField->name, EntityTable::getSystemFieldNames()) ) {
                $oFtEntity->selectField( $oField->name );
                $oFtEntity->addIndex( 'index', $oField->name );
            }

        }

        // пересоздаем модель
        $oFtEntity->addDefaultProcessorSet()
            ->save()
            ->build()
        ;

        $oConverter = new ft\converter\Json();

        $this->cache = $oConverter->ftModelToData( $oFtEntity->getModel() );

        $this->save();

        // todo сброс кеша из ft\Cache
    }


    private function checkUniqueName() {

        if ( !$this->name )
            $this->name = $this->title;

        $name = \skTranslit::genSysName( $this->name, 'card' );

        if ( strlen($name) > 60 )
            $name = substr($name, 0, 57);

        $i = '';
        do {
            $this->name = $name.$i;
            $res = EntityTable::findOne( ['name' => $this->name, 'id<>?' => $this->id] );
            $i++;
        } while ( $res );

        return true;
    }


    /**
     * Отдает флаг того, что сущность является расширяющей
     * @return bool
     */
    public function isExtended() {
        return (int)$this->type === Card::TypeExtended;
    }


    /**
     * Отдает флаг того, что сущность является базовой
     * @return bool
     */
    public function isBasic() {
        return (int)$this->type === Card::TypeBasic;
    }


    /**
     * Отдает флаг того, что сущность является базовой
     * @return bool
     */
    public function isSimple() {
        return (int)$this->type === Card::TypeDictionary;
    }


}