<?php

namespace skewer\build\Component\Catalog\model;

use skewer\build\Component\orm\Query;
use skewer\build\libs\ft;
use skewer\build\Component\orm;


class GoodsTable extends orm\TablePrototype {

    /** @var string Имя таблицы */
    protected static $sTableName = 'c_goods';


    protected static function initModel() {

        ft\Entity::get( 'c_goods' )
            ->clear( false )
            ->setNamespace( __NAMESPACE__ )
            ->addField( 'base_id', 'int', 'ID товарной позиции' )
            ->addField( 'base_card_id', 'int', 'ID базовой карточки' )
            ->addField( 'base_card_name', 'varchar(255)', 'Название базовой карточки' )
            ->addField( 'ext_card_id', 'int', 'ID расширенной карточки' )
            ->addField( 'ext_card_name', 'varchar(255)', 'Название расширенной карточки' )
            ->addField( 'parent', 'int', 'Главный товар' )
            ->addField( 'section', 'int', 'Главный раздел' )
            ->addField( '__add_date', 'date', 'Дата добавления' )
            ->addField( '__upd_date', 'date', 'Дата редактирования' )
            ->addDefaultProcessorSet()
            ->selectFields( ['base_id', 'base_card_id'] )
            ->addIndex( 'unique', 'prim' )
            ->save()
        ;

    }


    /**
     * Получение записи о товаре
     * @param $iGoodsId
     * @return array|bool|orm\ActiveRecord
     * fixme должна передаваться базовая карточка во входных параметрах
     */
    public static function get( $iGoodsId ) {

        return Query::SelectFrom( self::$sTableName )
            ->where( 'base_id', $iGoodsId )
            ->getOne();
    }


    public static function add( $iGoodId, $iGoodsCard, $sGoodsCard, $iExGoodsCard, $sExGoodsCard, $iGoodsParent = 0, $iSectionId = 0 ) {

        if ( !$iGoodsParent )
            $iGoodsParent = $iGoodId;

        return (bool)Query::InsertInto( self::$sTableName )
            ->set( 'base_id', $iGoodId )
            ->set( 'base_card_id', $iGoodsCard )
            ->set( 'base_card_name', $sGoodsCard )
            ->set( 'ext_card_id', $iExGoodsCard )
            ->set( 'ext_card_name', $sExGoodsCard )
            ->set( 'parent', $iGoodsParent )
            ->set( 'section', $iSectionId )
            ->set( '__add_date', date('Y-m-d H:i:s') )
            ->set( '__upd_date', date('Y-m-d H:i:s') )
            ->get();
    }


    public static function remove( $iGoodId, $iGoodsCard ) {

        return (bool)Query::DeleteFrom( self::$sTableName )
            ->where( 'base_id', $iGoodId )
            ->where( 'base_card_id', $iGoodsCard )
            ->get();
    }


    public static function setChangeDate( $iGoodId, $iGoodsCard ) {

        return (bool)Query::UpdateFrom( self::$sTableName )
            ->set( '__upd_date', date('Y-m-d H:i:s') )
            ->where( 'base_id', $iGoodId )
            ->where( 'base_card_id', $iGoodsCard )
            ->get();
    }


    /**
     * Для списка товаров определяет кол-во дочерних
     * @param array|int $list
     * @return array
     * fixme нет проверки на карточку
     * todo входная переменная может быть число
     */
    public static function getChildCount( $list ) {

        $query = Query::SelectFrom( self::$sTableName )
            ->fields( 'parent, count(base_id) as cnt', true )
            ->where( 'parent', $list )
            ->andWhere( 'parent!=base_id')
            ->groupBy( 'parent' )
            ->asArray()
        ;

        $res = array();

        while ( $row = $query->each() )
            $res[$row['parent']] = (int)$row['cnt'];

        return $res;
    }


    /**
     * @param $iGoodsId
     * @return bool
     * @throws \Exception
     * fixme нет проверки на карточку
     */
    public static function getMainSection( $iGoodsId ) {

        $query = Query::SelectFrom( self::$sTableName )
            ->where( 'base_id', $iGoodsId )
            ->asArray()
        ;

        if ( $row = $query->getOne() )
            return $row['section'];
        
        return false;
    }


    /**
     * @param $iGoodsId
     * @param $iSectionId
     * @return bool
     * fixme добавить добавление базовой карточки
     */
    public static function setMainSection( $iGoodsId, $iSectionId ) {

        $query = Query::UpdateFrom( self::$sTableName )
            ->set( 'section', $iSectionId )
            ->where( 'parent', $iGoodsId )
            ->orWhere( 'base_id', $iGoodsId )
            ->get();

        return true;
    }


    /**
     * Для товаров с таким главным разделом - выставить занчение 0
     * Потом они должны быть восстановлены
     * @param int $iSectionId
     * @return bool
     */
    public static function removeSection( $iSectionId ) {

        // todo нужно обойти все разделы и задать новый id
        $query = Query::UpdateFrom( self::$sTableName )
            ->set( 'section', 0 )
            ->where( 'section', $iSectionId )
            ->get();

        return true;
    }


    /**
     * Удаление всех товаров по карточке
     * @param $iBaseCard
     * @param int $iExtCard
     * @return bool
     */
    public static function removeCard( $iBaseCard, $iExtCard = 0 ) {

        $query = Query::DeleteFrom( self::$sTableName );

        if ( $iBaseCard )
            $query->where( 'base_card_id', $iBaseCard );

        if ( $iExtCard )
            $query->where( 'ext_card_id', $iExtCard );

        $query->get();

        return true;
    }

}