<?php

namespace skewer\build\Component\Catalog\model;

use skewer\build\Component\orm\Query;
use skewer\build\libs\ft;
use skewer\build\Component\orm;


class SemanticTable extends orm\TablePrototype {

    /** @var string Имя таблицы */
    protected static $sTableName = 'cl_semantic';

    protected static function initModel() {

        ft\Entity::get( 'cl_semantic' )
            ->clear( false )
            ->setNamespace( __NAMESPACE__ )
            ->addField( 'parent_id', 'int', 'ID родительского товара' )
            ->addField( 'parent_card', 'int','ID базовой карочки родительского товара' )
            ->addField( 'child_id', 'int', 'ID товара' )
            ->addField( 'child_card', 'int','ID базовой карочки товара' )
            ->addField( 'semantic', 'int', 'Тип связи' )
            ->addField( 'priority', 'int', 'Позиция (вес для сотировки)' )
            ->addDefaultProcessorSet()
            ->selectFields( ['parent_id', 'parent_card', 'child_id', 'child_card', 'semantic'] )
            ->addIndex( 'unique' )
            ->save()
        ;

    }


    /**
     * Добавление связи
     * @param $iSemantic
     * @param $iGoodId
     * @param $iGoodsCard
     * @param $iParentId
     * @param $iParentCard
     * @return bool
     */
    public static function link( $iSemantic, $iGoodId, $iGoodsCard, $iParentId, $iParentCard ) {

        return (bool)Query::InsertInto( self::$sTableName )
            ->set( 'semantic', $iSemantic )
            ->set( 'parent_id', $iParentId )
            ->set( 'parent_card', $iParentCard )
            ->set( 'child_id', $iGoodId )
            ->set( 'child_card', $iGoodsCard )
            ->setInc( 'priority', array(
                'parent_id' => $iParentId,
                'parent_card' => $iParentCard,
                'child_id' => $iGoodId,
                'child_card' => $iGoodsCard
            ))
            ->get();
    }


    /**
     * Удаление связи
     * @param $iSemantic
     * @param $iGoodId
     * @param $iGoodsCard
     * @param $iParentId
     * @param $iParentCard
     * @return bool
     */
    public static function unlink( $iSemantic, $iGoodId, $iGoodsCard, $iParentId, $iParentCard ) {

        return (bool)Query::DeleteFrom( self::$sTableName )
            ->where( 'semantic', $iSemantic )
            ->where( 'parent_id', $iParentId )
            ->where( 'parent_card', $iParentCard )
            ->where( 'child_id', $iGoodId )
            ->where( 'child_card', $iGoodsCard )
            ->get();
    }


    /**
     * Удаление всех связей товара
     * @param $iGoodsId
     * @param $iGoodsCard
     * @return bool
     */
    public static function remove( $iGoodsId, $iGoodsCard ) {

        Query::DeleteFrom( self::$sTableName )
            ->where( 'child_id', $iGoodsId )
            ->where( 'child_card', $iGoodsCard )
            ->get();

        Query::DeleteFrom( self::$sTableName )
            ->where( 'parent_id', $iGoodsId )
            ->where( 'parent_card', $iGoodsCard )
            ->get();

        return true;
    }
}