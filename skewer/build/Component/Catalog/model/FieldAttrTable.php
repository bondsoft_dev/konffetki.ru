<?php

namespace skewer\build\Component\Catalog\model;

use skewer\build\libs\ft;
use skewer\build\Component\orm\TablePrototype;


/**
 * Атрибуты полей сущности
 * Class FieldAttrTable
 * @package skewer\build\Component\Catalog\model
 */
class FieldAttrTable extends TablePrototype {

    /** @var string Имя таблицы */
    protected static $sTableName = 'c_field_attr';

    protected static function initModel() {

        ft\Entity::get('c_field_attr')
            ->clear()
            ->setNamespace( __NAMESPACE__ )
            ->addField( 'field', 'int', 'Поле' )
            ->addField( 'tpl', 'int', 'Шаблон' )
            ->addField( 'value', 'varchar(255)', 'Название' )
            ->addDefaultProcessorSet()
            ->save()
        ;

    }

    public static function getNewRow( $aData = array() ) {
        return new FieldAttrRow( $aData );
    }

}