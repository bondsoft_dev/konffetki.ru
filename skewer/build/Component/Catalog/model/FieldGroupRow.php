<?php

namespace skewer\build\Component\Catalog\model;

use skewer\build\Component\orm\ActiveRecord;


/**
 * Запись для группы полей сущности
 * Class FieldGroupRow
 * @package skewer\build\Component\Catalog\model
 */
class FieldGroupRow extends ActiveRecord {

    public $id = 0;
    public $entity = 0;
    public $name = '';
    public $title = '';
    public $position = 0;


    public function getTableName() {
        return 'c_field_group';
    }

    public function __toString() {
        return $this->title;
    }


    /**
     * Получение списка полей для группы
     * @return FieldRow[]
     */
    public function getFields() {
        return FieldTable::find()
            ->where( 'entity', $this->entity )
            ->where( 'group', $this->id )
            ->order('position')
            ->getAll();
    }


    public function save() {

        if ( !$this->entity )
            return false;

        $this->checkUniqueName();
        $this->checkPos();

        return parent::save();
    }


    public function preDelete() {

        FieldTable::update()
            ->set( 'group', 0 )
            ->where( 'group', $this->id )
            ->where( 'entity', $this->entity )
            ->get();

    }

    private function checkUniqueName() {

        if ( !$this->name )
            $this->name = $this->title ?: 'field_group';

        $name = \skTranslit::genSysName( $this->name, 'field_group' );

        $i = '';
        do {
            $this->name = $name.$i;
            $res = FieldGroupTable::findOne([
                'entity' => $this->entity,
                'name' => $name.$i,
                'id<>?' => $this->id
            ]);
            $i++;
        } while ( $res );

        return true;
    }


    private function checkPos() {

        if ( !$this->position ) {

            /** @var FieldGroupRow $oGroup */
            $oGroup = FieldGroupTable::find()
                ->where( 'entity', $this->entity )
                ->order( 'position', 'DESC' )
                ->getOne();

            $this->position = $oGroup ? $oGroup->position + 1 : 1;
        }

    }

}