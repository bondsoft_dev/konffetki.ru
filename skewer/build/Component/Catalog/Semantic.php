<?php

namespace skewer\build\Component\Catalog;

use skewer\build\libs\ft;


class Semantic {

    const TYPE_INCLUDE = 1;
    const TYPE_ANALOG = 2;
    const TYPE_RELATED = 3;

} 