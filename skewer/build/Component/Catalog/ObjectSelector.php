<?php

namespace skewer\build\Component\Catalog;

use skewer\build\Component\orm\Query;
use skewer\build\libs\ft;


/**
 * Класс для выборки значений для простых сущностей
 * Class ObjectSelector
 * @package skewer\build\Component\Catalog
 */
class ObjectSelector extends SelectorPrototype {



    /**
     * Получение объекта
     * @param int|string $card Идентификатор сущности
     * @param int $id Идентификатор объекта
     * @return array
     * @throws \Exception
     * @throws ft\Exception
     */
    public static function get( $id, $card ) {

        $oModel = ft\Cache::get( $card );

        if ( !$oModel )
            throw new \Exception('Не найдена модель для карточки.');

        $query = Query::SelectFrom( $oModel->getTableName(), $oModel );

        if ( is_numeric($id) ) {
            $query->where( 'id', $id );
        } else {
            $query->where( 'alias', $id );
        }

        $row = $query->asArray()->getOne();

        if ( !$row )
            return false;

        return Parser::get( $oModel->getFileds() )->object( $row );
    }


    /**
     * Получить позицию следующую за заданной
     * @param int $id Ид объекта
     * @param int|string $card Карточка вывода
     * @param array $filter Фильтр для сортировки
     * @return GoodsRow
     */
    public static function getPrev( $id, $card, $filter = [] ) {

        $oModel = ft\Cache::get( $card );
        if ( !$oModel )
            return false;

        $row = Query::SelectFrom( $oModel->getTableName() )
            ->where( 'id < ?', $id )
            ->where( 'active', 1 )
            ->order( 'id', 'DESC' )
            ->asArray()
            ->getOne();

        if ( !$row )
            $row = Query::SelectFrom( $oModel->getTableName() )
                ->where( 'id > ?', $id )
                ->where( 'active', 1 )
                ->order( 'id', 'DESC' )
                ->asArray()
                ->getOne();

        if ( !$row )
            return false;

        return Parser::get( $oModel->getFileds() )->object( $row );
    }


    /**
     * Получить позицию идущую перед заданным
     * @param int $id Ид объекта
     * @param int|string $card Карточка вывода
     * @param array $filter Фильтр для сортировки
     * @return GoodsRow
     */
    public static function getNext( $id, $card, $filter = [] ) {

        $oModel = ft\Cache::get( $card );
        if ( !$oModel )
            return false;

        $row = Query::SelectFrom( $oModel->getTableName() )
            ->where( 'id > ?', $id )
            ->where( 'active', 1 )
            ->order( 'id', 'ASC' )
            ->asArray()
            ->getOne();

        if ( !$row )
            $row = Query::SelectFrom( $oModel->getTableName() )
                ->where( 'id < ?', $id )
                ->where( 'active', 1 )
                ->order( 'id', 'ASC' )
                ->asArray()
                ->getOne();

        if ( !$row )
            return false;

        return Parser::get( $oModel->getFileds() )->object( $row );
    }


    /**
     * Список коллекций
     * @param string $card Карточка коллекции
     * @param int $field
     * @return GoodsSelector
     */
    public static function getCollections( $card, $field = 0 ) {

        $oGoods = new self();

        $oGoods->selectCard( $card );
        $oGoods->initParser();

        $oGoods->oQuery = Query::SelectFrom( 'cd_' . $oGoods->sBaseCard );

        $oGoods->oQuery->asArray();

        $oGoods->bSorted = false;

        return $oGoods;
    }



    public function parse() {

        $list = $this->oQuery->getAll();

        $aItems = [];
        foreach ( $list as $row ) {
            $aItems[] = $this->oParser->object( $row );
        }

        return $aItems;
    }


}