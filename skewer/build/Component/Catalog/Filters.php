<?php

namespace skewer\build\Component\Catalog;

use skewer\build\Component\Cache;
use skewer\build\Component\orm\Query;
use skewer\build\libs\ft;
use Yii;
use yii\helpers\ArrayHelper;


/**
 * API для работы с кастомными фильтрами в каталоге
 * Class Filters
 * @package skewer\build\libs\Catalog
 * todo перевести на фабрику полей из catalog/fields
 */
class Filters {

    const TYPE_INPUT = 'input';
    const TYPE_CHECK = 'check';
    const TYPE_NUM_SLIDER = 'num_slider';
    const TYPE_CHECK_GROUP = 'check_group';
    const TYPE_SELECT = 'select';
    const TYPE_MULTI_CHECK_GROUP = 'multi_check_group';
    const TYPE_COLLECTION = 'collection';

    protected $section = 0;

    protected $bUseExtCard = false;

    protected $sBaseCard = '';

    protected $sExtCard = '';

    protected $fields = [];

    protected $data = [];

    protected $shadowParams = false;


    public static function get4Section( $section = 0 ) {

        try {

            $oSelf = new self();

            $oSelf->section = $section;

            $oSelf->shadowParams = (bool)\SysVar::get('catalog.shadow_param_filter');

            $oSelf->initFields();

            $oSelf->initData();

            $oSelf->initFilter();

            return $oSelf->parse();

        } catch ( \Exception $e ) {

            return [];
        }

    }


    public static function get4Card( $card ) {

        try {

            $oSelf = new self();

            $oSelf->shadowParams = (bool)\SysVar::get('catalog.shadow_param_filter');

            $oSelf->initFields( $card );

            $oSelf->initData();

            $oSelf->initFilter();

            return $oSelf->parse();

        } catch ( \Exception $e ) {

            return [];
        }

    }


    /**
     * Инициализация полей для разела
     * @param string $card
     * @throws \Exception
     * @throws ft\Exception
     */
    private function initFields( $card = '' ) {

        if ( $this->section )
            $aCardList = Section::getCardList( $this->section );
        else
            $aCardList = [ $card ];


        if ( count($aCardList) == 1 ) {

            $this->bUseExtCard = true;

            $iCard = array_shift( $aCardList );

            $oModel = ft\Cache::get( $iCard );

            $this->sExtCard = $oModel->getName();

            $oParModel = ft\Cache::get( $oModel->getParentId() );

            $this->sBaseCard = $oParModel->getName();

            foreach ( $oParModel->getFileds() as $oField )
                if ( $oField->getAttr( 'show_in_filter' ) )
                    $this->fields[ $oField->getName() ] = $oField;

        } else {

            if ( !$aCardList )
                throw new \Exception('card not found');

            $iCard = 1; // todo найти базовую карточку
            $oModel = ft\Cache::get( $iCard );
            $this->sBaseCard = $oModel->getName();
        }

        foreach ( $oModel->getFileds() as $oField )
            if ( $oField->getAttr( 'show_in_filter' ) )
                $this->fields[ $oField->getName() ] = $oField;

    }


    private function initData() {

        $this->data = Yii::$app->request->get();

        foreach ( $this->data as $field => $data )
            if ( !isSet( $this->fields[$field] ) )
                unset( $this->data[$field] );

    }


    /**
     * Сбор данных для парсинга формы
     * @return array
     */
    public function parse() {

        $aOut = [];

        if ( !count( $this->fields ) )
            return $aOut;

        foreach ( $this->fields as $oField ) {

            $sWidget = $this->getWidget( $oField );

            switch ( $sWidget ) {

                case self::TYPE_INPUT:
                    $aOut[] = $this->parseInput( $oField );
                    break;

                case self::TYPE_CHECK:
                    $aOut[] = $this->parseCheck( $oField );
                    break;

                case self::TYPE_NUM_SLIDER:
                    $aOut[] = $this->parseSlider( $oField );
                    break;

                case self::TYPE_CHECK_GROUP:
                    if ( $field = $this->parseCheckGroup( $oField ) )
                        $aOut[] = $field;
                    break;

                case self::TYPE_SELECT:
                    if ( $field = $this->parseSelect( $oField ) )
                        $aOut[] = $field;
                    break;

                case self::TYPE_MULTI_CHECK_GROUP:
                    if ( $field = $this->parseMultiCheckGroup( $oField ) )
                        $aOut[] = $field;
                    break;

                case self::TYPE_COLLECTION:
                    if ( $field = $this->parseCheckGroup( $oField ) )
                        $aOut[] = $field;
                    break;
            }

        }

        return $aOut;
    }


    /**
     * Определение типа виджита по полю
     * @param ft\model\Field $oField
     * @return string
     */
    private function getWidget( $oField ) {// fixme ref

        $sWidget = $oField->getWidgetName() ? $oField->getWidgetName() : $oField->getEditorName();

        if ( in_array( $oField->getEditorName(), [ 'string', 'wyswyg', 'text' ] ) )
            $sWidget = self::TYPE_INPUT;
        elseif ( in_array( $oField->getEditorName(), [ 'check' ] ) )
            $sWidget = self::TYPE_CHECK;
        elseif ( in_array( $oField->getEditorName(), [ 'int', 'float', 'money', 'numb' ] ) )
            $sWidget = self::TYPE_NUM_SLIDER;
        elseif ( in_array( $oField->getEditorName(), [ ft\Editor::SELECT ] ) && !$oField->getWidgetName() )
            $sWidget = self::TYPE_CHECK_GROUP;
        elseif ( in_array( $oField->getEditorName(), [ ft\Editor::MULTISELECT ] ) && !$oField->getWidgetName() )
            $sWidget = self::TYPE_MULTI_CHECK_GROUP;

        return $sWidget;
    }


    private function parseInput( ft\model\Field $oField ) {

        return array(
            'title' => $oField->getTitle(),
            'name' => $oField->getName(),
            'type' => self::TYPE_INPUT,
            'value' => ArrayHelper::getValue( $this->data, $oField->getName(), '' )
        );

    }


    private function parseCheck( ft\model\Field $oField ) {

        $value = ArrayHelper::getValue( $this->data, $oField->getName(), 'none' );

        $bDisable = false;
        if ( $this->shadowParams && ($value == 'none') ) {

            $sTable = $oField->getModel()->getTableName();
            $sField = $oField->getName();
            $aConditions = Cache\Api::get( 'Catalog.Conditions' );

            $query = $this->getQuery( true );

            $query
                ->fields( "count($sField) AS cnt", true )
                ->join( 'inner', 'c_goods', '', $sTable . '.id=base_id' )
                ->on( 'base_id=parent' )
                ->where( 'active', 1 )
                ->where( $sField, 1 )
                ->asArray()
            ;

            if ( $aConditions )
                foreach ( $aConditions as $field => $val )
                    $query->where( $val[0], $val[1] );

            $row = $query->getOne();

            $bDisable = !$row || ($row['cnt'] == 0);

        }

        return array(
            'title' => $oField->getTitle(),
            'name' => $oField->getName(),
            'type' => self::TYPE_CHECK,
            'disable' => $bDisable,
            'check' => $value !== 'none'
        );

    }


    private function parseSlider( ft\model\Field $oField ) {

        $sTable = $oField->getModel()->getTableName();
        $sField = $oField->getName();

        $query = $this->getQuery( true );

        $query
            ->fields( "max($sTable.{$sField}) as vmax, min($sTable.{$sField}) as vmin", true )
            ->join( 'inner', 'c_goods', '', $sTable . '.id=base_id' )
            ->on( 'base_id=parent' )
            ->where( 'active', 1 )
        ;

        $row = $query->asArray()->get();

        $out = ['min' => 0, 'max' => 1000];

        if ( $row ) {
            $out['min'] = floor( $row[0]['vmin'] );
            $out['max'] = ceil( $row[0]['vmax'] );
        }

        return array(
            'title' => $oField->getTitle(),
            'name' => $oField->getName(),
            'type' => self::TYPE_NUM_SLIDER,
            'def_value_min' => $out['min'],
            'def_value_max' => $out['max'],
            'value_min' => ArrayHelper::getValue( $this->data, $sField.'.min', $out['min']),
            'value_max' => ArrayHelper::getValue( $this->data, $sField.'.max', $out['max'])
        );

    }


    private function parseCheckGroup( ft\model\Field $oField ) {

        $value = ArrayHelper::getValue( $this->data, $oField->getName(), [] );

        $items = $this->getDictField( $oField );

        if ( !$items )
            return false;

        if ( $items && count( $items ) )
            foreach ( $items as &$item ) {
                if ( self::equal( $item['id'], $value ) )
                    $item['check'] = true;
            }

        return array(
            'title' => $oField->getTitle(),
            'name' => $oField->getName(),
            'type' => self::TYPE_CHECK_GROUP,
            'items' => $items
        );

    }


    private function parseMultiCheckGroup( ft\model\Field $oField ) {

        $value = ArrayHelper::getValue( $this->data, $oField->getName(), [] );

        $items = $this->getMultiDictField( $oField );

        if ( !$items )
            return false;

        if ( $items && count( $items ) )
            foreach ( $items as &$item ) {
                if ( self::equal( $item['id'], $value ) )
                    $item['check'] = true;
            }

        return array(
            'title' => $oField->getTitle(),
            'name' => $oField->getName(),
            'type' => self::TYPE_CHECK_GROUP,
            'items' => $items
        );

    }


    private function parseSelect( ft\model\Field $oField ) {

        $value = ArrayHelper::getValue( $this->data, $oField->getName(), 0 );

        $items = $this->getDictField( $oField );

        if ( !$items )
            return false;

        if ( $items && count( $items ) )
            foreach ( $items as &$item ) {
                if ( self::equal( $item['id'], $value ) )
                    $item['check'] = true;
            }

        return array(
            'title' => $oField->getTitle(),
            'name' => $oField->getName(),
            'type' => self::TYPE_SELECT,
            'items' => $items
        );

    }


    /**
     * Получение значений справочника для поля по фильтру
     * @param ft\model\Field $oField
     * @return array|mixed
     */
    private function getDictField( $oField ) {

        $sField = $oField->getName();
        $aConditions = Cache\Api::get( 'Catalog.Conditions' );
        unSet( $aConditions[$sField] );

        $oRel = $oField->getModel()->getOneFieldRelation( $sField );

        if ( !$oRel )
            return [];

        // find real id
        $arr = [];

        $query = $this->getQuery( true );
        $query
            ->fields( "DISTINCT $sField AS fld", true )
            ->where( 'active', 1 )
            ->asArray()
        ;

        while ( $row = $query->each() )
            $arr[] = $row['fld'];

        if ( !$arr )
            return [];

        // find shadow id
        $clear_arr = [];
        if ( $this->shadowParams ) {

            $query = $this->getQuery( true );

            $query
                ->fields( "DISTINCT $sField AS fld", true )
                ->where( 'active', 1 )
                ->asArray()
            ;

            if ( $aConditions )
                foreach ( $aConditions as $field => $value )
                    $query->where( $value[0], $value[1] );

            while ( $row = $query->each() )
                $clear_arr[] = $row['fld'];

        }

        // get list
        $oTable = ft\Cache::getMagicTable( $oRel->getEntityName() );
        $aItems = $oTable->find()->where( 'id', $arr )->asArray()->getAll();

        if ( $this->shadowParams )
            foreach ( $aItems as &$aItem )
                $aItem['disable'] = !in_array( $aItem['id'], $clear_arr );

        return $aItems;
    }



    /**
     * Получение значений справочника для поля по фильтру
     * @param ft\model\Field $oField
     * @return array|mixed
     */
    private function getMultiDictField( $oField ) {

        $sField = $oField->getName();
        $aConditions = Cache\Api::get( 'Catalog.Conditions' );
        unSet( $aConditions[$sField] );

        $oRel = $oField->getModel()->getOneFieldRelation( $sField );

        if ( !$oRel )
            return [];

        // find real id
        $arr = [];

        $query = $this->getQuery( true )
            ->join( 'inner', $oField->getLinkTableName(), '', 'co_' . $this->sBaseCard . '.id=' . ft\Relation::INNER_FIELD )
            ->fields( "DISTINCT " . ft\Relation::EXTERNAL_FIELD . " AS fld", true )
            ->where( 'active', 1 )
            ->asArray()
        ;

        while ( $row = $query->each() )
            $arr[] = $row['fld'];

        if ( !$arr )
            return [];

        // find shadow id
        $clear_arr = [];

        // todo реализовать "чистые" поля
//        if ( $this->shadowParams ) {
//
//            $query = $this->getQuery( true );
//
//            $query
//                ->fields( "DISTINCT $sField AS fld", true )
//                ->where( 'active', 1 )
//                ->asArray()
//            ;
//
//            if ( $aConditions )
//                foreach ( $aConditions as $field => $value )
//                    $query->where( $value[0], $value[1] );
//
//            while ( $row = $query->each() )
//                $clear_arr[] = $row['fld'];
//
//        }

        // get list
        $oTable = ft\Cache::getMagicTable( $oRel->getEntityName() );
        $aItems = $oTable->find()->where( 'id', $arr )->asArray()->getAll();

//        if ( $this->shadowParams )
//            foreach ( $aItems as &$aItem )
//                $aItem['disable'] = !in_array( $aItem['id'], $clear_arr );

        return $aItems;
    }




    private function initFilter() {

        $bFilterUsed = false;

        $aFilter = [];

        /** @var $oField ft\model\Field */
        foreach ( $this->fields as $oField ) {

            if ( $oField->getAttr( 'show_in_filter' ) ) {

                $sType = $oField->getEditorName();
                $sName = $oField->getName();

                if ( $sType == self::TYPE_CHECK ) {

                    if ( ArrayHelper::getValue( $this->data, $sName, 0 ) ) {
                        $aFilter[$sName] = [$sName, 1];
                        $bFilterUsed = true;
                    }

                } elseif( $sType == self::TYPE_SELECT ) {

                    $value = ArrayHelper::getValue( $this->data, $sName );

                    if ( $value ) {

                        if ( is_array($value) )
                            $aFilter[$sName] = ["$sName IN ?", $value];
                        else
                            $aFilter[$sName] = [$sName, $value];

                        $bFilterUsed = true;
                    }

                } elseif( $sType == ft\Editor::MULTISELECT ) {

                    $value = ArrayHelper::getValue( $this->data, $sName );

                    if ( $value ) {
                        $aFilter[$sName] = [$sName, $value];
                        $bFilterUsed = true;
                    }

                } elseif( $sType == ft\Editor::COLLECTION ) {

                    $value = ArrayHelper::getValue( $this->data, $sName );

                    if ( $value ) {
                        $aFilter[$sName] = [$sName, $value];
                        $bFilterUsed = true;
                    }

                } elseif( in_array( $sType, array( 'string', 'wyswyg' ) ) ) {

                    $value = ArrayHelper::getValue( $this->data, $sName, '' );
                    if ( $value ) {
                        $aFilter[$sName] = ["$sName LIKE ?", "%$value%"];
                        $bFilterUsed = true;
                    }

                } elseif( in_array( $sType, array( 'float', 'int', 'money' ) ) ) { // число в диапазоне

                    $value = ArrayHelper::getValue( $this->data, $sName, 0 );
                    if ( $value ) {
                        $aFilter[$sName] = ["$sName BETWEEN ?", [$value['min'], $value['max']]];
                        $bFilterUsed = true;
                    }

                }

            }

        }

        Cache\Api::set( 'Catalog.Conditions', $aFilter );

        return $bFilterUsed;
    }


    private static function equal( $val, $tpl ) {
        if ( is_array( $tpl ) )
            return in_array( $val, $tpl );
        else
            return ($val == $tpl);
    }


    /**
     * Формирует объект запросника
     * @param bool $bWithSection
     * @return \skewer\build\Component\orm\state\StateSelect
     */
    private function getQuery( $bWithSection = false ) {

        $query = Query::SelectFrom( 'co_' . $this->sBaseCard );

        if ( $this->bUseExtCard )
            $query->join( 'inner', 'ce_' . $this->sExtCard, 'ce_' . $this->sExtCard, 'co_' . $this->sBaseCard . '.id=ce_' . $this->sExtCard . '.id' );

        if ( $bWithSection && $this->section )
            $query
                ->join( 'inner', 'cl_section', '', 'co_' . $this->sBaseCard . '.id=goods_id' )
                ->on( 'section_id', $this->section )
            ;

        return $query;
    }


}