<?php

namespace skewer\build\Component\Catalog;

use skewer\build\Component\Catalog;
use skewer\build\Component\orm\Query;
use skewer\build\Component\Cache;
use skewer\build\libs\ft;
use yii\helpers\ArrayHelper;


/**
 * Класс для выборки товарных позиций для вывода на клиентской части
 * Class GoodsSelector
 * @package skewer\build\libs\Catalog
 */
class GoodsSelector extends SelectorPrototype {


    /** @var bool Флаг фильтрации по полям */
    private $bApplyAttrFilter = false;

    /** @var bool Флаг факта использования фильтрации */
    private $bFilterUsed = false;

    /** @var int Хранит ссылку на переменную со значением общего кол-ва элементов в выборке */
    private $iFullCoun;

    /** @var bool Флаг необходимости использовать группировку для товаров */
    private $useGrouping = false;


    public static function get( $id, $baseCard ) {

        if ( is_numeric($id) ) {
            $oGoodsRow = GoodsRow::get( $id, $baseCard );
        } else {
            $oGoodsRow = GoodsRow::getByAlias( $id, $baseCard );
        }

        if (!$oGoodsRow){
            return false;
        }

        $goods_row = Catalog\model\GoodsTable::get( $oGoodsRow->getRowId() );

        return Parser::get( $oGoodsRow->getFields(), ['active','show_in_detail'] )
            ->goods( $goods_row, $oGoodsRow->getData() )
        ;

    }


    /**
     * Получить товар следующий за заданным
     * @param int $iObjectId Ид объекта
     * @param int $iSectionId Ид раздела
     * @param array $aFilter Фильтр для сортировки
     * @return GoodsRow
     */
    public static function getPrev( $iObjectId, $iSectionId, $aFilter = array() ) {

        if ( !($row = Catalog\model\SectionTable::getPrev( $iObjectId, $iSectionId )) )
            return false;

        if ( !($oGoodsRow = GoodsRow::get( $row['goods_id'], $row['goods_card'] )) )
            return false;

        $goods_row = Catalog\model\GoodsTable::get( $oGoodsRow->getRowId() );

        return Parser::get( $oGoodsRow->getFields() )
            ->goods( $goods_row, $oGoodsRow->getData(), true )
            ;

    }


    /**
     * Получить товар идущий перед заданным
     * @param int $iObjectId Ид объекта
     * @param int $iSectionId Ид раздела
     * @param array $aFilter Фильтр для сортировки
     * @return GoodsRow
     */
    public static function getNext( $iObjectId, $iSectionId, $aFilter = array() ) {

        if ( !($row = Catalog\model\SectionTable::getNext( $iObjectId, $iSectionId )) )
            return false;

        if ( !($oGoodsRow = GoodsRow::get( $row['goods_id'], $row['goods_card'] )) )
            return false;

        $goods_row = Catalog\model\GoodsTable::get( $oGoodsRow->getRowId() );

        return Parser::get( $oGoodsRow->getFields() )
            ->goods( $goods_row, $oGoodsRow->getData(), true )
            ;

    }


    /**
     * Список товарных позиций для коллекции
     * @param $brand
     * @param $field
     * @param $value
     * @return GoodsSelector
     */
    public static function getList4Collection( $brand, $field, $value ) {

        $oGoods = new self();

        $oGoods->selectCard( Card::get4Field( $field ) );
        $oGoods->initParser( ['active','show_in_list'] );
        $oGoods->cacheFields();


        // выборка по таблицам
        $sFieldLine = 'co_' . $oGoods->sBaseCard . '.*, base_id';

        $oGoods->oQuery = Query::SelectFrom( 'co_' . $oGoods->sBaseCard )
            ->join( 'inner', 'c_goods', '', 'co_' . $oGoods->sBaseCard . '.id=base_id' )
            ->on( 'base_id=parent' )
        ;

        if ( $oGoods->bUseExtCard ) {
            $oGoods->oQuery->join( 'inner', 'ce_' . $oGoods->sExtCard, 'ext_card', 'ext_card.id=base_id' );
            $sFieldLine .= ', ext_card.*';
        }


        if ( isSet( $oGoods->aCardFields[ $field ] ) )
            $oGoods->oQuery->where( $field, $value );
        else
            $oGoods->oQuery->where( 'id', 0 );


        $oGoods->oQuery->fields( $sFieldLine )->asArray();

        $oGoods->bSorted = false;

        return $oGoods;

    }


    /**
     * Список товарных позиций для разделов
     * @param int|array $section Ид или список разедлов
     * @return GoodsSelector
     */
    public static function getList4Section( $section ) {

        $oGoods = new self();

        $oGoods->selectCard( self::card4Section( $section ) );
        $oGoods->initParser( ['active','show_in_list'] );
        $oGoods->cacheFields();

        // выборка по таблицам
        $sFieldLine = 'co_' . $oGoods->sBaseCard . '.*, base_id';

        $oGoods->oQuery = Query::SelectFrom( 'co_' . $oGoods->sBaseCard )
            ->join( 'inner', 'c_goods', '', 'co_' . $oGoods->sBaseCard . '.id=base_id' )
            ->on( 'base_id=parent' )
            ->join( 'inner', 'cl_section', '', 'co_' . $oGoods->sBaseCard . '.id=goods_id' )
            ->on( 'section_id', $section )
        ;

        if ( $oGoods->bUseExtCard ) {
            $oGoods->oQuery->join( 'inner', 'ce_' . $oGoods->sExtCard, 'ext_card', 'ext_card.id=goods_id' );//todo goods_id ?? mb base_id
            $sFieldLine .= ', ext_card.*';
        }

        // отсекаем дубли одного товара в нескольких разделах
        if ( is_array($section) && count($section) > 1 )
            $oGoods->useGrouping = true;

        $oGoods->oQuery->fields( $sFieldLine )->asArray();

        $oGoods->bSorted = true;

        return $oGoods;
    }


    /**
     * Выборка позиций для вывода общего списка по базовой карточки
     * @param int|string $card Базовая или расширенная карточка
     * @return GoodsSelector
     */
    public static function getList( $card ) {

        $oGoods = new self();

        $oGoods->selectCard( $card );
        $oGoods->initParser( ['active','show_in_list'] );
        $oGoods->cacheFields();

        // выборка по таблицам
        $sFieldLine = 'co_' . $oGoods->sBaseCard . '.*, base_id';

        $oGoods->oQuery = Query::SelectFrom( 'co_' . $oGoods->sBaseCard )
            ->join( 'inner', 'c_goods', '', 'co_' . $oGoods->sBaseCard . '.id=base_id' )
            ->on( 'base_id=parent' )
        ;

        if ( $oGoods->bUseExtCard ) {
            $oGoods->oQuery->join( 'inner', 'ce_' . $oGoods->sExtCard, 'ext_card', 'ext_card.id=base_id' );
            $sFieldLine .= ', ext_card.*';
        }

        $oGoods->oQuery->fields( $sFieldLine )->asArray();

        return $oGoods;
    }


    /**
     * Список сопутствующих товаров
     * @param $iObjectId
     * @return GoodsSelector
     */
    public static function getRelatedList( $iObjectId ) {

        $oGoods = new self();

        $row = Catalog\model\GoodsTable::get( $iObjectId );

        $oGoods->selectCard( $row['base_card_name'] );
        $oGoods->initParser( ['active','show_in_list'] );

        $oGoods->oQuery = Query::SelectFrom( 'co_' . $oGoods->sBaseCard, $oGoods->sBaseCard )
            ->fields( 'co_' . $oGoods->sBaseCard . '.*' )
            ->join( 'inner', 'c_goods', '', 'co_' . $oGoods->sBaseCard . '.id=base_id' )
            ->on( 'base_id=parent' )
            ->join( 'inner', 'cl_semantic', '', 'co_' . $oGoods->sBaseCard . '.id=parent_id' )
            ->on( 'semantic=?', Semantic::TYPE_RELATED )
            ->on( 'child_id=?', $iObjectId )
            ->asArray()
        ;

        $oGoods->bSorted = true;

        return $oGoods;

    }



    /**
     * Самопальные сопутствующие товары
     * */
    public static function getSimilarToProducts($currentSectionId) {

        $goodsIds = array();
        $goodsAllIds = array();
        $sectionIds = array();
        $arProducts = false;
        
        
        //Разделы из которых нельзя грузить сопуствующие товары
        $disableSectionIDs = array(312, 337, 313, 325, 346, 310, 324, 342, 347, 348, 349);
        if(!in_array($currentSectionId, $disableSectionIDs)){
            $disableSectionIDs[] = $currentSectionId;
        }
        
        //получаем разделы кроме раздела текущего товара
        $querySelect = (new \yii\db\Query())->select('section_id')->distinct()->from('cl_section')->where(['not in', 'section_id', $disableSectionIDs])->orderBy(['section_id'=>SORT_ASC])->limit(10);
        $sectionRows = $querySelect->all();
        

        foreach ($sectionRows as $sectionKey => $sectionRow){
            
            
            $sectionIds[] = $sectionRow['section_id'];
            $goodsRows = false;
            
            //Получаем по 2 товара (id товаров) из каждого раздела
            $goodsQuery = (new \yii\db\Query())->select('goods_id')->from('cl_section')->where('section_id=' . $sectionRow['section_id'])->limit(3);
            $goodsRows = $goodsQuery->all();
            

            if($goodsRows){
                foreach ($goodsRows as $goodKey => $goodValue){
                    //id товаров по разделам
                    $goodsIds[$sectionRow['section_id']]['goods'][] = $goodValue['goods_id'];
                    //все id товаров
                    $goodsAllIds[] = $goodValue['goods_id'];
                }
            }
        }
        
        //Получаем информацию по разделам (в частности alias_path)
        $sectionsInfo = (new \yii\db\Query())->select('id, alias_path')->from('tree_section')->where('id IN (' . implode(",", $sectionIds) . ')')->orderBy(['id'=>SORT_ASC]);
        $sectionsInfoRows = $sectionsInfo->all();
        foreach ($sectionsInfoRows as $secInfoKey => $secInfoValue){
            if($goodsIds[$secInfoValue['id']]) {
                $goodsIds[$secInfoValue['id']]['path'] = $secInfoValue['alias_path'];
            }
        }
        
        //Получаем список единиц измерения
        $arMeasure = array();
        $measureList = (new \yii\db\Query())->select('id, performance')->from('cd_edinica_izmereniya')->orderBy(['id'=>SORT_ASC]);
        $measureListRows = $measureList->all();
        foreach ($measureListRows as $mlKey => $mlValue){
            $arMeasure[$mlValue['id']] = $mlValue['performance'];
        }
        
        //Получаем информацию по товарам
        $arGoodsInfo = array();
        $goodsInfo = (new \yii\db\Query())->select('id, title, alias, gallery, announce, price, measure, residual')->from('co_base_card')->where('id IN ('.  implode(",", $goodsAllIds) .') AND buy = 1 AND active = 1');
        $goodsInfoRows = $goodsInfo->all();
        $gallaryIds = array();
        
        foreach ($goodsInfoRows as $infoRowKey => $infoRowValue){
            
            if($infoRowValue['measure']==6){
                $price = round(($infoRowValue['price'] * 1.0) /10);
            } else {
                $price = round($infoRowValue['price']);
            }
            
            $arGoodsInfo[$infoRowValue['id']] = array(
                    'id' => $infoRowValue['id'],
                    'title' => str_replace('"', '', $infoRowValue['title']),
                    'alias' => $infoRowValue['alias'],
                    'gallery' => $infoRowValue['gallery'],
                    'announce' => $infoRowValue['announce'],
                    'price' => $price,
                    'residual' => $infoRowValue['residual'], //остаток на складе
                    'measure' => $arMeasure[$infoRowValue['measure']]
            );
            
            if($infoRowValue['gallery']){
                $gallaryIds[] = $infoRowValue['gallery'];
            }
            
        }
        
        //Получаем все картинки
        $arImgInfo = array();
        $imgInfo = (new \yii\db\Query())->select('album_id, images_data')->from('photogallery_photos')->where('album_id IN ('. implode(",", $gallaryIds) .')');
        $imgInfoRows = $imgInfo->all();
        foreach ($imgInfoRows as $imgInfoKey => $imgInfoValue){
            
            $arImgInfo[$imgInfoValue['album_id']] = json_decode($imgInfoValue['images_data'], true);
        }
        

        
        //Рассовываем картинки по товарам
        foreach ($arGoodsInfo as $goodId => $goodValue){

            if($arImgInfo[$goodValue['gallery']]){
                $arGoodsInfo[$goodId]['gallery'] = $arImgInfo[$goodValue['gallery']];
            }
        }
        

        //Рассовываем товары по категориями
        foreach ($goodsIds as $secId => $secValue){
            foreach ($secValue['goods'] as $gKey => $gId){
                if($arGoodsInfo[$gId] && is_array($arGoodsInfo[$gId]['gallery']) && $arGoodsInfo[$gId]['residual']) {
                    $arGoodsInfo[$gId]['url'] = $secValue['path'] . $arGoodsInfo[$gId]['alias'] . '/';
                    //$goodsIds[$secId]['goods'][$gKey] = $arGoodsInfo[$gId];
                    $arProducts[] = $arGoodsInfo[$gId];
                }
            }
        }
        
        
        //echo '<pre>'; print_r($arProducts[0]); echo '</pre>';
        
        //Смешиваем массив и возвращаем 6 его элементов
        shuffle($arProducts);
        $arProducts = array_slice($arProducts, 0, 6);

        return $arProducts;
    } 




    /**
     * Список товаров в комплекте
     * @param $iObjectId
     * @return GoodsSelector
     */
    public static function getIncludedList( $iObjectId ) {

        $oGoods = new self();

        $row = Catalog\model\GoodsTable::get( $iObjectId );

        $oGoods->selectCard( $row['base_card_name'] );
        $oGoods->initParser( ['active','show_in_list'] );

        $oGoods->oQuery = Query::SelectFrom( 'co_' . $oGoods->sBaseCard, $oGoods->sBaseCard )
            ->fields( 'co_' . $oGoods->sBaseCard . '.*' )
            ->join( 'inner', 'c_goods', '', 'co_' . $oGoods->sBaseCard . '.id=base_id' )
            ->on( 'base_id=parent' )
            ->join( 'inner', 'cl_semantic', '', 'co_' . $oGoods->sBaseCard . '.id=parent_id' )
            ->on( 'semantic=?', Semantic::TYPE_INCLUDE )
            ->on( 'child_id=?', $iObjectId )
            ->asArray()
        ;

        $oGoods->bSorted = true;

        return $oGoods;

    }


    /**
     * Получение списка аналогов для товара
     * @param $iObjectId
     * @param int $iCurObjId
     * @return GoodsSelector
     */
    public static function getModificationList( $iObjectId, $iCurObjId = 0 ) {

        if ( !$iCurObjId )
            $iCurObjId = $iObjectId;

        $row = Catalog\model\GoodsTable::get( $iCurObjId );

        $oGoods = new self();

        $oGoods->selectCard( $row['ext_card_name'] );
        $oGoods->initParser( ['active','show_in_list'] );

        $oGoods->oQuery = Query::SelectFrom( 'co_' . $oGoods->sBaseCard, $oGoods->sBaseCard )
            ->fields( 'co_' . $oGoods->sBaseCard . '.*,ext_card.*' )
            ->join( 'inner', 'c_goods', '', 'co_' . $oGoods->sBaseCard . '.id=base_id' )
            ->on( 'parent=?', (int)$iObjectId )
            ->on( 'base_id<>?', (int)$iCurObjId )
            ->join( 'inner', 'ce_' . $oGoods->sExtCard, 'ext_card', 'ext_card.id=base_id' )
            ->asArray()
        ;

        $oGoods->bSorted = false;

        return $oGoods;

    }


    /**
     * Фильтрация списка каталожных позиций
     * @return bool
     */
    public function applyFilter() {

        $this->bApplyAttrFilter = true;

        $aFilter = Cache\Api::get( 'Catalog.Conditions' );

        if ( $aFilter )
            foreach ( $aFilter as $field => $conditions ) {

                $oField = $this->aCardFields[$field];
                $oRel = $oField->getModel()->getOneFieldRelation( $field );

                if ( $oRel and $oRel->getType() == ft\Relation::MANY_TO_MANY ) {

                    $this->oQuery
                        ->join( 'inner', $oField->getLinkTableName(), $oField->getLinkTableName(), 'co_' . $this->sBaseCard . '.id=`'.$oField->getLinkTableName().'`.' . ft\Relation::INNER_FIELD )
                        ->on( sprintf('%s`.`%s', $oField->getLinkTableName(), ft\Relation::EXTERNAL_FIELD), $conditions[1] )
                    ;

                    $this->useGrouping = true;

                } else
                    $this->oQuery->andWhere( $conditions[0], $conditions[1] );

                $this->bFilterUsed = true;
            }

        return $this->bFilterUsed;
    }



    /**
     * Парсинг набора товаров
     * @return array
     * Использовать только для малых выборок!!! (2-20 позиций)
     */
    public function parse() {

        // todo опитизация на галерею
        // для не ModeFull нужно получать только миниатюру первой фотографии в галереи
        // выбор галереи из кеша или базы но одним запросом как справочники

        // установка базовой сортировки если не задано поле
        if ( $this->bSorted )
            $this->oQuery->order( 'priority' );

        if ( $this->useGrouping )
            $this->oQuery->groupBy( 'co_' . $this->sBaseCard . '.id' );

        $list = $this->oQuery->getAll();

        $aItems = [];
        foreach ( $list as $row ) {
            $goods_row = Catalog\model\GoodsTable::get( $row['id'] ); // fixme одним запросом выборка по товарам из c_goods
            $aItems[] = $this->oParser->goods( $goods_row, $row );
        }

        return $aItems;
    }


    /**
     * Итератор выборки с парсером
     * @param string $sParseMode
     * @return bool|array
     * используется в YandexExport
     */
    public function parseEach() {

        // установка базовой сортировки если не задано поле
        if ( !$this->oQuery->getBEachIterator() ) {
            if ( $this->bSorted )
                $this->oQuery->order( 'priority' );
        }

        if ( !($row = $this->oQuery->each()) )
            return false;

        $goods_row = Catalog\model\GoodsTable::get( $row['id'] );
        return $this->oParser->goods( $goods_row, $row );
    }


    public function getArray( &$count ) {

        // установка базовой сортировки если не задано поле
        if ( $this->bSorted )
            $this->oQuery->order( 'priority' );

        return $this->oQuery->setCounterRef( $count )->asArray()->getAll();
    }

    /**
     * Возвращает объект запросника
     * @return \skewer\build\Component\orm\state\StateSelect
     * fixme используется в FullCustomList ( ->getWithoutIncluded ->getWithoutRelated )
     */
    public function getQuery() {

        // установка базовой сортировки если не задано поле
        if ( $this->bSorted )
            $this->oQuery->order( 'priority' );

        return $this->oQuery;
    }


    /**
     * Возвращает карточку для построения списка товаров для разделов
     * @param int $section идентификатор раздела с товарами
     * @return int
     * todo это функция в апи разделов должна быть
     */
    private static function card4Section( $section ) {

        // получаем список всех карточек товаров для раздела
        $cards = Section::getCardList( $section );

        if ( count( $cards ) == 1 ) {
            // если одна, то берем ее (вывод по одной расширенной карточке)
            $card = array_shift( $cards );
        } elseif ( count( $cards ) > 1 ) {
            // если больше одной, то берем базовую (вывод только по базовой)
            $card = Card::getBaseCard( $cards[0] );
        } else {
            // карточки нет - либо ошибка, либо раздел пуст - выводим по дефолтной базовой
            // todo проработать этот сценарий
            $card = Card::DEF_BASE_CARD;
        }

        return $card;
    }


    /**
     * Задание условий на сортировку поля товара
     * @param string $sFieldName
     * @param string $sWay
     * @return $this
     * fixme ref
     */
    public function sort( $sFieldName, $sWay = 'ASC' ) {

        // todo разобраться с типами
        // нужна ли проверка на существование поля здесь ?
        //        if ( $sFieldName && count($this->aCardFields) ) {
        //            if ( isSet( $this->aCardFields[$sFieldName] ) ) {
        //                // todo ...
        //                $sDataType = $this->aCardFields[$sFieldName]->getDatatype();
        //                if ( $sDataType == 'int' || $sDataType == 'float' ) {
        //                    $sWay = ( $sWay == 'ASC' ) ? 'ASC_NUMERIC' : 'DESC_NUMERIC';
        //                }
        //            } else {
        //                throw new Exception( 'Ошибка: поле для сортировки "' . $sFieldName . '" не найдено' );
        //            }
        //        }


        if ( !$sFieldName )
            return $this;

        // fixme
        if ( !count( $this->aCardFields ) ) {
            $this->oQuery->order( $sFieldName, $sWay );
            $this->bSorted = false;
            return $this;
        }

        if ( !($field = ArrayHelper::getValue( $this->aCardFields, $sFieldName, false)) )
            return $this;

        // fixme Attr::SHOW_IN_LIST Attr::SHOW_IN_SORTPANEL
        if ( !$field->getAttr( 'show_in_list' ) || !$field->getAttr( 'show_in_sortpanel' ) )
            return $this;

        // hack не обрабатываем пока связи ><
        if ( $rel = $field->getFirstRelation() )
            if ( $rel->getType() == ft\Relation::MANY_TO_MANY )
                return $this;


        $this->oQuery->order( $sFieldName, $sWay );
        $this->bSorted = false;

        return $this;
    }


}
