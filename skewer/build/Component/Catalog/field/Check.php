<?php

namespace skewer\build\Component\Catalog\field;


class Check extends Prototype {

    protected function build( $value, $rowId ) {
        return [
            'value' => $value,
            'html' => ( $value == '1' ) ? \Yii::t('page', 'yes') : \Yii::t('page', 'no')
        ];
    }
}