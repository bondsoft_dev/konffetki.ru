<?php

namespace skewer\build\Component\Catalog\field;

use yii\helpers\ArrayHelper;


class Select extends Prototype {


    protected function build( $value, $rowId ) {

        $item = $this->getSubDataValue( $value );

        return [
            'value' => $value,
            'item' => $item,
            'html' => ArrayHelper::getValue( $item, 'title', '' )
        ];
    }

}