<?php

namespace skewer\build\Component\Catalog\field;


class Money extends Prototype {

    protected function build( $value, $rowId ) {
        $out = ($value == (int)$value) ? (int)$value : $value;
        // todo здесь если потребуется поменять тип разделителя
        return [
            'value' => $out,
            'value_full' => $value,
            'html' => $out
        ];
    }
}