<?php

namespace skewer\build\Component\Catalog\field;


class String extends Prototype {

    protected function build( $value, $rowId ) {
        return [
            'value' => $value,
            'html' => $value
        ];
    }
}