<?php

namespace skewer\build\Component\Catalog\field;

use skewer\build\Component\Catalog\Section;
use yii\helpers\ArrayHelper;


class Collection extends Prototype {

    protected $subSection = 0;


    protected function build( $value, $rowId ) {

        $item = $this->getSubDataValue( $value );
        $itemTitle = ArrayHelper::getValue( $item, 'title', '' );
        $itemAlias = ArrayHelper::getValue( $item, 'alias', '' );

        $section = $this->subSection;

        $href = 'CatalogViewer?' . ( $itemAlias ? 'goods-alias=' . $itemAlias : 'item=' . $value );

        $out = $section ? ' <a href="[' . $section . '][' . $href . ']">' . $itemTitle . '</a>' : $itemTitle;

        return [
            'value' => $value,
            'item' => $item,
            'html' => $out
        ];
    }


    protected function load() {

        // todo перенести это в кэш
        // fixme не решена проблема с одноименными полями в разных карточках

        $this->subSection = Section::get4CollectionField( $this->name );

    }

}