<?php

namespace skewer\build\Component\Catalog\field;

use yii\helpers\ArrayHelper;


class Multiselect extends Prototype {


//    // получение данных для поля со сложными связями. подумать над переносом в другое место
//    $oRel = $oField->getModel()->getOneFieldRelation( $oField->getName() );
//    if ( $oRel and $oRel->getType() == ft\Relation::MANY_TO_MANY ) {
//        $aFieldData[$sFieldName] = $oField->getLinkRow( $out['id'] );
//    }


    protected function build( $value, $rowId ) {


        $value = $this->ftField->getLinkRow( $rowId );

        if ( !is_array( $value ) )
            $value = explode( ',', $value );

        $items = [];
        $out = '';

        foreach( $value as $val )
            if ( $value ) {
                $item = $this->getSubDataValue( $val );
                $items[] = $item;
                $out .= ( $out ? ', ' : '' ) . ArrayHelper::getValue( $item, 'title', '');
            }

        return [
            'value' => $value,
            'item' => $items,
            'html' => $out
        ];
    }
}