<?php

namespace skewer\build\Component\Catalog\field;


class Date extends Prototype {

    protected function build( $value, $rowId ) {
        return [
            'value' => ( $value == '0000-00-00' ) ? '' : $value,
            'html' => $value ? date( 'd.m.Y', strtotime( $value ) ) : ''
        ];
    }
}