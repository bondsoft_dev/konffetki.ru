<?php

namespace skewer\build\Component\Catalog\field;

use skewer\build\Component\Gallery\Photo;


class Gallery extends Prototype {

    protected function build( $value, $rowId ) {

        $images = Photo::getFromAlbum( $value, true );

//        $row['gallery'] = ['images' => $images];
//        $row['first_img'] = $images[0]; // first image

        return [
            'value' => $value,
            'gallery' => $images ? ['images' => $images] : [],
            'first_img' => $images ? $images[0] : false,// fixme
            'html' => $value
        ];
    }
}