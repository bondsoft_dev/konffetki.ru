<?php

namespace skewer\build\Component\Catalog\field;


class Time extends Prototype {

    protected function build( $value, $rowId ) {

        $out = '';

        if ( $value ) {
            list( $sH, $sM, $sS ) = explode( ':', $value );
            $out = $sH . ':' . $sM;
        }

        return [
            'value' => $value,
            'html' => $out
        ];
    }
}