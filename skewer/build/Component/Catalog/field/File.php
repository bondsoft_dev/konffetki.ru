<?php

namespace skewer\build\Component\Catalog\field;


class File extends Prototype {

    protected function build( $value, $rowId ) {

        $fileName = substr( $value, strrpos( $value, '/' ) + 1 );
        $fileLink = $value;
        $out = $value ? '<a href="' . $fileLink . '">' . $fileName . '</a>' : '';

        return [
            'file_name' => $fileName,
            'file_link' => $fileLink,
            'value' => $value,
            'html' => $out
        ];
    }
}