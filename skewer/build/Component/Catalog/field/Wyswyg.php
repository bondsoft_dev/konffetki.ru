<?php

namespace skewer\build\Component\Catalog\field;


class Wyswyg extends Prototype {

    protected function build( $value, $rowId ) {
        return [
            'value' => $value,
            'html' => $value
        ];
    }
}