<?php

namespace skewer\build\Component\Catalog\field;

use skewer\build\libs\ft;
use yii\helpers\ArrayHelper;


/**
 * Прототип для типизированного объекта парсинга поля сущности
 * Class Prototype
 * @package skewer\build\Component\Catalog\field
 */
abstract class Prototype {

    protected $type = '';

    protected $name = '';

    protected $title = '';

    protected $card = '';

    protected $attr = [];


    protected $subEntity = '';

    protected $subData = []; // todo перенести в Cache


    protected $ftField = null;


    public function getName() {
        return $this->name;
    }


    /**
     * Создание поля для парсера на основе ft-поля модели
     * @param ft\model\Field $field
     * @return Prototype
     */
    public static function init( $field ) {

        $class = __NAMESPACE__ . '\\' . ucfirst( $field->getEditorName() );

        $obj = class_exists( $class ) ? new $class() : new String();

        $obj->ftField = $field;
        $obj->type = $field->getEditorName();
        $obj->name = $field->getName();
        $obj->title = $field->getTitle();
        $obj->card = $field->getModel()->getName();
        $obj->attr = $field->getAttrs();

        $obj->load();
        $obj->loadSubData();

        return $obj;
    }


    protected function load() {

    }


    protected function loadSubData() {

        $oModel = ft\Cache::get( $this->card );

        if ( !$oModel )
            return;

        if ( $oRel = $oModel->getOneFieldRelation( $this->name ) ) {

            $this->subEntity = $oRel->getEntityName();

            //if ( !isSet( $this->dict[$this->subEntity]) )

            $query = ft\Cache::getMagicTable( $this->subEntity )->find()->asArray();

            while ( $row = $query->each() )
                $this->subData[ $row['id'] ] = $row;

        }
    }


    protected function getSubDataValue( $key ) {
        return ArrayHelper::getValue( $this->subData, $key, false );
    }


    /**
     * Классовая функция преобразования значения
     * @param string $value значение поля из записи
     * @param int $rowId идентификатор записи
     * @return mixed
     */
    abstract protected function build( $value, $rowId );


    /**
     * Функция преобразования значения
     * @param string $value значение поля из записи
     * @param int $rowId идентификатор записи
     * @return mixed
     */
    public function parse( $value, $rowId ) {

        $out = $this->build( $value, $rowId );

        $out['attrs'] = $this->attr;
        $out['name'] = $this->name;
        $out['card'] = $this->card;
        $out['title'] = $this->title;
        $out['type'] = $this->type;

        // ед измерения
        if ( isSet($out['attrs']['measure']) && $out['attrs']['measure'] && $out['html'] ) {
            $out['measure'] = $out['attrs']['measure'];
            $out['html'] .= ' ' . $out['attrs']['measure'];
        }

        return $out;
    }
}