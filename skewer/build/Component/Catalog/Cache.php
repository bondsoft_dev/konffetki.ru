<?php

namespace skewer\build\Component\Catalog;

use skewer\build\libs\ft;

// todo пока замена и прокся для ft\Cache с доработкой под каталог
class Cache {

    /**
     * @param $card
     * @return ft\Model
     */
    public static function get( $card ) {

        return ft\Cache( $card );
    }

}