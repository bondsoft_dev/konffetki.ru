<?php

namespace skewer\build\Component\Catalog;


use skewer\build\libs\ft;
use yii\helpers\ArrayHelper;


/**
 * Парсер. Переводит товар из формата ActiveRecord в ассоциативный массив для вывода
 * Class Parser
 * @package skewer\build\Page\CatalogViewer\Parser
 */
class Parser {

    /** @var field\Prototype[] набор полей для формирования вывода */
    private $fields = [];


    /**
     * Инициализация парсера
     * @param ft\model\Field[] $fields набор полей для вывода
     * @param array $attr набор атрибутов, ограничивающий набор полей
     * @return Parser
     */
    public static function get( $fields, $attr = [] ) {

        $obj = new self();

        foreach ( $fields as $ftField ) {

            $fl = true;

            foreach ( $attr as $name )
                if ( !$ftField->getAttr( $name ) )
                    $fl = false;

            if ( !$fl )
                continue;

            $obj->fields[] = field\Prototype::init( $ftField );
        }

        return $obj;
    }


    /**
     * Парсим кортеж данных как товарную позицию
     * @param array $goodsData данные о товарной позиции
     * @param array $fieldsData значение полей товарной позиции
     * @param bool $onlyHeader
     * @return array
     */
    public function goods( $goodsData, $fieldsData, $onlyHeader = false ) {

        // заполняем заголовок - товарные данные
        $out = [
            'id' => ArrayHelper::getValue($fieldsData, 'id', 0),
            'title' => ArrayHelper::getValue($fieldsData, 'title', ''),
            'alias' => ArrayHelper::getValue($fieldsData, 'alias', ''),
            'active' => (bool)ArrayHelper::getValue($fieldsData, 'active', false),
            'card' => ArrayHelper::getValue($goodsData, 'ext_card_name', ''),
            'base_card_id' => (int)ArrayHelper::getValue($goodsData, 'base_card_id', 0),
            'main_obj_id' => ArrayHelper::getValue($goodsData, 'parent', ''),
            'main_secton' => ArrayHelper::getValue($goodsData, 'section', 0),
        ];

        $out['url'] = self::buildUrl( $out['main_secton'], $out['id'], $out['alias'] );
        $out['canonical_url'] = \Site::httpDomain() . $out['url'];

        // если нужен только заголовое, то дальше не обрабатываем поля
        if ( $onlyHeader ) return $out;


        // список доступных полей для товара
        $out['fields'] = [];
        foreach ( $this->fields as $field ) {
            $value = ArrayHelper::getValue( $fieldsData, $field->getName(), '' );
            $out['fields'][$field->getName()] = $field->parse( $value, $out['id'] );
        }



        // список полей по группам todo реализовать функционал когда возникнет необходимость
        $out['groups'] = [];


        // блок данных для перекрытия SEO метатегов
        $out['seo'] = [
            'title' => ArrayHelper::getValue( $out, 'fields.tag_title.value', '' ),
            'description' => ArrayHelper::getValue( $out, 'fields.description.value', '' ),
            'keywords' => ArrayHelper::getValue( $out, 'fields.keywords.value', '' ),
        ];

        return $out;
    }


    /**
     * Парсим кортеж данных как сущность
     * @param array $data данные полей сущности
     * @return array
     */
    public function object( $data ) {

        $out = [
            'id' => ArrayHelper::getValue( $data, 'id', 0 ),
            'title' => ArrayHelper::getValue( $data, 'title', '' ),
            'alias' => ArrayHelper::getValue( $data, 'alias', '' ),
            'active' => (bool)ArrayHelper::getValue( $data, 'active', false ),
        ];

        $out['fields'] = [];

        foreach ( $this->fields as $field ) {

            $out['fields'][$field->getName()] = $field->parse( $data[$field->getName()], $out['id'] );
        }

        return $out;
    }


    //fixme priv
    public static function buildUrl( $iMainSection, $iGoods = 0, $sGoodsAlias = '' ) {
        return ( !empty( $sGoodsAlias ) ) ?
            \Yii::$app->router->rewriteURL( sprintf('[%s][CatalogViewer?goods-alias=%s]', $iMainSection, $sGoodsAlias ) ):
            \Yii::$app->router->rewriteURL( sprintf('[%s][CatalogViewer?item=%s]', $iMainSection, $iGoods ) );
    }

} 