<?php

namespace skewer\build\Component\Catalog;


class Attr {

    const SHOW_IN_LIST = 1;
    const SHOW_IN_DETAIL = 3;
    const SHOW_IN_SORTPANEL = 5;
    const ACTIVE = 6;
    const MEASURE = 7;
    const SHOW_IN_PARAMS = 8;
    const SHOW_IN_TAB = 9;
    const SHOW_IN_FILTER = 10;
    const IS_UNIQ = 11;
    const SHOW_IN_TABLE = 12;

    const SHOW_IN_CART = 13;


    public static function getList() {// todo решить вопрос нужны ли тут title
        return [
            [ 'id'=>1, 'name'=>'show_in_list', 'title'=>\Yii::t( 'catalog', 'attr_show_in_list'), 'type'=>'check', 'default'=>1 ],
            [ 'id'=>3, 'name'=>'show_in_detail', 'title'=>\Yii::t( 'catalog', 'attr_show_in_detail'), 'type'=>'check', 'default'=>1 ],
            [ 'id'=>5, 'name'=>'show_in_sortpanel', 'title'=>\Yii::t( 'catalog', 'attr_show_in_sortpanel'), 'type'=>'check', 'default'=>0 ],
            [ 'id'=>6, 'name'=>'active', 'title'=>\Yii::t( 'catalog', 'attr_active'), 'type'=>'check', 'default'=>1 ],
            [ 'id'=>7, 'name'=>'measure', 'title'=>\Yii::t( 'catalog', 'attr_measure'), 'type'=>'string', 'default'=>'' ],
            [ 'id'=>8, 'name'=>'show_in_params', 'title'=>\Yii::t( 'catalog', 'attr_show_in_params'), 'type'=>'check', 'default'=>0 ],
            [ 'id'=>9, 'name'=>'show_in_tab', 'title'=>\Yii::t( 'catalog', 'attr_show_in_tab'), 'type'=>'check', 'default'=>0 ],
            [ 'id'=>10, 'name'=>'show_in_filter', 'title'=>\Yii::t( 'catalog', 'attr_show_in_filter'), 'type'=>'check', 'default'=>0 ],
            [ 'id'=>11, 'name'=>'is_uniq', 'title'=>\Yii::t( 'catalog', 'attr_is_uniq'), 'type'=>'check', 'default'=>0 ],
            [ 'id'=>12, 'name'=>'show_in_table', 'title'=>\Yii::t( 'catalog', 'attr_show_in_table'), 'type'=>'check', 'default'=>0 ],
//            [ 'id'=>13, 'name'=>'show_in_cart', 'title'=>\Yii::t( 'catalog', 'attr_show_in_cart'), 'type'=>'check', 'default'=>0 ],
        ];
    }


}