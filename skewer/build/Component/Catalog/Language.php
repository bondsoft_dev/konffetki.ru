<?php

$aLang = array();

$aLang['ru']['base_group'] = 'Базовая группа';

$aLang['ru']['title'] = 'Название';

$aLang['ru']['validator_set'] = 'Задан';
$aLang['ru']['validator_unique'] = 'Уникальное поле';

$aLang['ru']['attr_show_in_list'] = 'Выводить в списке';
$aLang['ru']['attr_show_in_detail'] = 'Выводить в детальной';
$aLang['ru']['attr_show_in_filter'] = 'Использовать как фильтр';
$aLang['ru']['attr_active'] = 'Активность';
$aLang['ru']['attr_measure'] = 'Единицы измерения';
$aLang['ru']['attr_show_in_params'] = 'Выводить в параметрах';
$aLang['ru']['attr_show_in_tab'] = 'Выводить в табах';
$aLang['ru']['attr_show_in_sortpanel'] = 'Выводить в сортировке';
$aLang['ru']['attr_is_uniq'] = 'Уникальное поле (двухуровневый каталог)';
$aLang['ru']['attr_show_in_table'] = 'Выводить в шаблоне Таблица';
$aLang['ru']['attr_show_in_cart'] = 'Выводить в корзине';

/******************/

$aLang['en']['base_group'] = 'Base group';

$aLang['en']['title'] = 'Title';

$aLang['en']['validator_set'] = 'Required';
$aLang['en']['validator_unique'] = 'Unique';

$aLang['en']['attr_show_in_list'] = 'Show in list';
$aLang['en']['attr_show_in_detail'] = 'Show in detail';
$aLang['en']['attr_show_in_filter'] = 'Use as filter';
$aLang['en']['attr_active'] = 'Active';
$aLang['en']['attr_measure'] = 'Measure';
$aLang['en']['attr_show_in_params'] = 'Show in parameters';
$aLang['en']['attr_show_in_tab'] = 'Show as tab';
$aLang['en']['attr_show_in_sortpanel'] = 'Show in sort';
$aLang['en']['attr_is_uniq'] = 'Unique field (two-level catalog)';
$aLang['en']['attr_show_in_table'] = 'Show in the Table template';
$aLang['en']['attr_show_in_cart'] = 'Show in the Cart';

return $aLang;