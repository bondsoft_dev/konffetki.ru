<?php

namespace skewer\build\Component\Catalog;


use yii\base\Component;

/**
 * Класс для генерации каталожного окружения по умолчанию
 * Class Generator
 * @package skewer\build\Component\Catalog
 * используется при инсталяции каталога и в тестах
 */
class Generator extends Component {


    public static function genBaseCard() {

        $base = self::createBaseCard();

        $iSeoGroup = self::createGroup( $base->id, 'seo', \Yii::t('data/catalog', 'group_seo_title', [], \Yii::$app->language ) );
        $iControlsGroup = self::createGroup( $base->id, 'controls', \Yii::t('data/catalog', 'group_controls_title', [], \Yii::$app->language ) );

        foreach ( self::getBaseFields( $iSeoGroup, $iControlsGroup ) as $aField ) {
            self::createField( $base->id, $aField );
        }

        $base->updCache();
        return $base->save();
    }


    /**
     * @param string $name
     * @param string $title
     * @return model\EntityRow
     * todo зарефакторить когда появится возможность добавление нескольких базовых
     */
    public static function createBaseCard( $name = Card::DEF_BASE_CARD, $title = '' ) {

        if (!$title)
            $title = \Yii::t('data/catalog', 'cart_base_title', [], \Yii::$app->language );

        $oCard = Card::get();
        $oCard->setData([
            'name' => $name,
            'title' => $title,
            'type' => Card::TypeBasic,
            'module' => Card::ModuleName
        ]);
        $oCard->save();
        return $oCard;
    }


    /**
     * @param $card
     * @param $name
     * @param $title
     * @return model\EntityRow
     */
    public static function createExtCard( $card, $name, $title = '' ) {
        $oCard = Card::get();
        $oCard->setData([
            'name' => $name,
            'title' => $title,
            'parent' => $card,
            'type' => Card::TypeExtended,
            'module' => Card::ModuleName
        ]);
        $oCard->save();
        return $oCard;
    }


    public static function createGroup( $card, $name, $title = '' ) {
        $oGroup = Card::getGroup();
        $oGroup->setData([
            'entity' => $card,
            'name' => $name,
            'title' => $title,
        ]);
        return $oGroup->save();
    }


    public static function createField( $card, $data ) {

        $oField = Card::getField();
        $oField->setData( $data );
        $oField->entity = $card;
        $oField->save();

        if ( isSet( $data['attr'] ) )
            foreach ( $data['attr'] as $attr => $val )
                $oField->setAttr( $attr, $val );

        if ( isSet( $data['validator'] ) )
            $oField->setValidator( $data['validator'] );

        return $oField->id;
    }


    protected static function getBaseFields( $iSeoGroup = 1, $iControlsGroup = 2 ) {
        return [
            [
                'name' => 'title',
                'title' => $title = \Yii::t('data/catalog', 'field_title_title', [], \Yii::$app->language ),
                'group' => 0,
                'editor' => 'string',
                'attr' => [Attr::SHOW_IN_SORTPANEL => 1],
                'validator' => 'set'
            ],[
                'name' => 'article',
                'title' => $title = \Yii::t('data/catalog', 'field_article_title', [], \Yii::$app->language ),
                'group' => 0,
                'attr' => [Attr::SHOW_IN_TABLE => 1],
                'editor' => 'string',
            ],[
                'name' => 'alias',
                'title' => $title = \Yii::t('data/catalog', 'field_alias_title', [], \Yii::$app->language ),
                'group' => 0,
                'editor' => 'string',
                'validator' => 'unique'
            ],[
                'name' => 'gallery',
                'title' => \Yii::t('data/catalog', 'field_gallery_title', [], \Yii::$app->language ),
                'group' => 0,
                'editor' => 'gallery',
            ],[
                'name' => 'announce',
                'title' => \Yii::t('data/catalog', 'field_announce_title', [], \Yii::$app->language ),
                'group' => 0,
                'editor' => 'wyswyg',
            ],[
                'name' => 'obj_description',
                'title' => \Yii::t('data/catalog', 'field_obj_description_title', [], \Yii::$app->language ),
                'group' => 0,
                'editor' => 'wyswyg',
                'attr' => [
                    Attr::SHOW_IN_LIST => 0,
                    Attr::SHOW_IN_TAB => 1
                ],
            ],[
                'name' => 'price',
                'title' => \Yii::t('data/catalog', 'field_price_title', [], \Yii::$app->language ),
                'group' => 0,
                'editor' => 'money',
                'attr' => [
                    Attr::SHOW_IN_SORTPANEL => 1,
                    Attr::SHOW_IN_TABLE => 1,
                    Attr::MEASURE => \Yii::t('data/catalog', 'field_price_measure', [], \Yii::$app->language )
                ],
            ],[
                'name' => 'old_price',
                'title' => \Yii::t('data/catalog', 'field_old_price_title', [], \Yii::$app->language ),
                'group' => 0,
                'editor' => 'string',
                'attr' => [Attr::ACTIVE => 0]
            ],[
                'name' => 'measure',
                'title' => \Yii::t('data/catalog', 'field_measure_title', [], \Yii::$app->language ),
                'group' => 0,
                'editor' => 'string',
            ],[
                'name' => 'tag_title',
                'title' => \Yii::t('data/catalog', 'field_tag_title_title', [], \Yii::$app->language ),
                'group' => $iSeoGroup,
                'editor' => 'string',
                'attr' => [Attr::SHOW_IN_LIST => 0],
            ],[
                'name' => 'keywords',
                'title' => \Yii::t('data/catalog', 'field_keywords_title', [], \Yii::$app->language ),
                'group' => $iSeoGroup,
                'editor' => 'string',
                'attr' => [Attr::SHOW_IN_LIST => 0],
            ],[
                'name' => 'description',
                'title' => \Yii::t('data/catalog', 'field_description_title', [], \Yii::$app->language ),
                'group' => $iSeoGroup,
                'editor' => 'string',
                'attr_' => [Attr::SHOW_IN_LIST => 0],
            ],[
                'name' => 'sitemap_priority',
                'title' => \Yii::t('data/catalog', 'field_sitemap_priority_title', [], \Yii::$app->language ),
                'group' => $iSeoGroup,
                'editor' => 'string',
                'def_value' => '0.9',
                'attr' => [Attr::SHOW_IN_LIST => 0],
            ],[
                'name' => 'sitemap_frequency',
                'title' => \Yii::t('data/catalog', 'field_sitemap_frequency_title', [], \Yii::$app->language ),
                'group' => $iSeoGroup,
                'editor' => 'string',
                'def_value' => 'daily',
                'attr' => [Attr::SHOW_IN_LIST => 0],
            ],[
                'name' => 'active',
                'title' => \Yii::t('data/catalog', 'field_active_title', [], \Yii::$app->language ),
                'group' => $iControlsGroup,
                'editor' => 'check',
                'def_value' => 1,
            ],[
                'name' => 'buy',
                'title' => \Yii::t('data/catalog', 'field_buy_title', [], \Yii::$app->language ),
                'group' => $iControlsGroup,
                'editor' => 'check',
                'def_value' => 1,
            ],[
                'name' => 'fastbuy',
                'title' => \Yii::t('data/catalog', 'field_fastbuy_title', [], \Yii::$app->language ),
                'group' => $iControlsGroup,
                'editor' => 'check',
                'def_value' => 1,
                'attr' => [Attr::ACTIVE => 0],
            ],[
                'name' => 'on_main',
                'title' => \Yii::t('data/catalog', 'field_on_main_title', [], \Yii::$app->language ),
                'group' => $iControlsGroup,
                'editor' => 'check',
                'attr' => [Attr::SHOW_IN_LIST => 0],
            ],[
                'name' => 'hit',
                'title' => \Yii::t('data/catalog', 'field_hit_title', [], \Yii::$app->language ),
                'group' => $iControlsGroup,
                'editor' => 'check',
            ],[
                'name' => 'new',
                'title' => \Yii::t('data/catalog', 'field_new_title', [], \Yii::$app->language ),
                'group' => $iControlsGroup,
                'editor' => 'check',
            ],[
                'name' => 'discount',
                'title' => \Yii::t('data/catalog', 'field_discount_title', [], \Yii::$app->language ),
                'group' => $iControlsGroup,
                'editor' => 'check',
            ]
        ];
    }


}