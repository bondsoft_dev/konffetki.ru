<?php

namespace skewer\build\Component\Catalog;

use skewer\build\Catalog\Goods\Search;
use skewer\build\Component\Section\Parameters;
use skewer\build\libs\ft;
use skewer\build\Component\orm\Query;
use yii\base\ModelEvent;
use yii\db\AfterSaveEvent;


class Api {

    /**
     * Ищем товар по полю
     * @param string $sFieldName
     * @param $mValue
     * @param $mCard
     * @return bool|GoodsRow
     */
    public static function getByField( $sFieldName, $mValue, $mCard ){

        if ( $oModel = ft\Cache::get( $mCard ) ) {

            $sCard = '';

            $sBaseCard = $oModel->getName();

            /** ищем поле в карточке */
            if ($oModel->hasField($sFieldName)){
                if ($oModel->getType() == Card::TypeExtended ){
                    $sCard = $oModel->getTableName();
                }else{
                    $sCard = $oModel->getTableName();
                }
            }


            if (!$sCard && $oModel->getType() == Card::TypeExtended){
                $oParentModel = ft\Cache::get( $oModel->getParentId() );
                if ($oParentModel->hasField($sFieldName))
                    $sCard = $oParentModel->getTableName();
            }

            if (!$sCard)
                return false;

            /** Запрос на поиск товара */
            $row = Query::SelectFrom( $sCard )
                ->fields( $sCard . '.id')
                ->join( 'inner', 'c_goods', '', $sCard . '.id=base_id' )
                ->on( 'base_id=parent' )
                ->where( $sFieldName, $mValue )
                ->getOne()
            ;

            if ($row)
                return GoodsRow::get( $row['id'], $sBaseCard );

        }
        return false;

    }


    /**
     * Создание товара
     * @param $mCard
     * @return GoodsRow
     */
    public static function createGoodsRow( $mCard ){
        return GoodsRow::create( $mCard );
    }


    /**
     * Выставление значений поля в 0 для всех товаров в базовой карточке
     * @param string $sFieldName
     */
    public static function disableAll( $sFieldName = 'active' ) {

        $sCard = 'base_card'; //Card::getNameById( 1 ); // fixme base_card
        Query::UpdateFrom( 'co_' . $sCard )
            ->set( $sFieldName, 0)
            ->get();

    }


    /**
     * Выставление значений поля в 0 для всех товаров карточки
     * @param $mCard
     * @param string $sFieldName
     */
    public static function disableByCard( $mCard, $sFieldName = 'active' ) {

        if ( $oModel = ft\Cache::get( $mCard ) ) {

            $sCard = '';
            $extCard = $oModel->getName();

            /** ищем поле в карточке */
            if ($oModel->hasField($sFieldName)){
                if ($oModel->getType() == Card::TypeExtended ){
                    $sCard = $oModel->getTableName();
                }else{
                    $sCard = $oModel->getTableName();
                }
            }

            if ($oModel->getType() == Card::TypeExtended ){
                $oParentModel = ft\Cache::get( $oModel->getParentId() );
                $typeCard = 'ext_card_name';
                if (!$sCard)
                    if ($oParentModel->hasField($sFieldName))
                        $sCard = $oParentModel->getTableName();
            }else{
                $typeCard = 'base_card_name';
            }

            if ($sCard){
                //Обновляем
                $sQuery = '
                    UPDATE `' . $sCard . '`
                    SET `' . $sFieldName . '`=0
                    WHERE `id` IN (SELECT `base_id` FROM `c_goods` WHERE `' . $typeCard .'` = :card)
                ;';

                Query::SQL( $sQuery, ['card' => $extCard]);
            }
        }

    }


    /**
     * Запросник для выборки товаров с пустым значением поля $sFieldName
     * @param $sFieldName
     * @return \skewer\build\Component\orm\state\StateSelect
     */
    public static function selectAllFromField( $sFieldName ) {

        $sCard = 'base_card';// Card::getNameById( 1 ); // fixme base_card
        return Query::SelectFrom( 'co_' . $sCard )
            ->fields('id')
            ->where( $sFieldName, 0)
            ->asArray();

    }


    /**
     * Запросник для выборки товаров карточки $mCard с пустым значением поля $sFieldName
     * @param $sFieldName
     * @param $mCard
     * @return \skewer\build\Component\orm\state\StateSelect
     */
    public static function selectFromField( $sFieldName, $mCard ) {

        if ( $oModel = ft\Cache::get( $mCard ) ) {

            $sExtCard = '';
            $bUseExtCard = false;

            if ($oModel->getType() == Card::TypeExtended ){
                $oParentModel = ft\Cache::get( $oModel->getParentId() );
                $sBaseCard = $oParentModel->getTableName();
                $sExtCard = $oModel->getTableName();
                $bUseExtCard = true;
            }else{
                $sBaseCard = $oModel->getTableName();
            }


            // выборка по таблицам
            $sFieldLine = 'DISTINCT ' . $sBaseCard . '.*, base_id';

            $oQuery = Query::SelectFrom( $sBaseCard )
                ->join( 'inner', 'c_goods', '', $sBaseCard . '.id=base_id' )
                ->on( 'base_id=parent' )
            ;

            if ( $bUseExtCard ) {
                $oQuery->join( 'inner', $sExtCard, 'ext_card', 'ext_card.id=base_id' );
                $sFieldLine .= ', ext_card.*';
            }

            $oQuery->fields( $sFieldLine );

            return $oQuery->where( $sFieldName, 0)->asArray();
        }
    }

    /**
     * Удаление товара
     * @param $id
     * @return bool
     */
    public static function deleteGoods( $id  ) {

        $oSearch = new Search();
        $oSearch->deleteByObjectId( $id );

        /** @var  $GoodsRow */
        $GoodsRow = GoodsRow::get( $id );

        if ($GoodsRow)
            return $GoodsRow->delete();
        return false;
    }


    /**
     * Список полей сео-группы
     * @return array
     */
    public static function getSeoFields(){
        $oSeoGroup = Card::getGroupByName( 1, 'seo' );
        if ($oSeoGroup){
            $aFields = $oSeoGroup->getFields();
            if ($aFields){
                $aRes = [];
                foreach( $aFields as $aField ){
                    /** @var $aField \skewer\build\Component\Catalog\model\FieldRow */
                    $aRes[] = $aField->name;
                }
                return $aRes;
            }
        }
        return [];
    }


    public static function getFieldsByCard( $card, $attr = [] ) {

    }


    /**
     * хак: при создании каталожного раздела подменяем ему раздел с формой для заказа
     * @param AfterSaveEvent $event
     */
    public static function addSection( AfterSaveEvent $event ){

        if (Parameters::getValByName($event->sender->id, 'content', 'objectAdm', true) == 'Catalog'){

            $sLanguage = Parameters::getLanguage($event->sender->id);
            Parameters::setParams($event->sender->id, 'content', 'buyFormSection',
                \Yii::$app->sections->getValue('orderForm', $sLanguage),
                '', \Yii::t('catalog', 'template_catalog_order_form', [], $sLanguage)
            );
        }

    }
}