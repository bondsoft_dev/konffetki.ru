<?php

namespace skewer\build\Component\QueueManager\ar;


use skewer\build\Component\orm\ActiveRecord;
use skewer\build\libs\ft;

class TaskRow extends ActiveRecord{

    public $id = 0;
    public $global_id = 0;
    public $title = '';
    public $class = '';
    public $parameters = '';
    public $priority = 0;
    public $resource_use = 0;
    public $target_area = 0;
    public $upd_time = '';
    public $mutex = 0;
    public $status = 0;
    public $parent = 0;
    public $md5 = '';

    function __construct() {
        $this->setTableName( 'task' );
        $this->setPrimaryKey( 'id' );
    }

    public function save(){

        $this->upd_time = date('Y-m-d H:i:s');

        return parent::save();
    }

}