<?php

namespace skewer\build\Component\QueueManager;


/**
 * Прототип задачи
 */
abstract class Task{

    /** Новая */
    const stNew = 1;

    /** В работе */
    const stProcess = 2;

    /** Завершена */
    const stComplete = 3;

    /** Ошибка */
    const stError = 4;

    /** Повисшая */
    const stTimeout = 5;

    /** Заморожена */
    const stFrozen = 6;

    /** В ожидании */
    const stWait = 7;

    /** Инициализирована */
    const stInit = 8;

    /** Закрыта */
    const stClose = 9;

    /** Прервана */
    const stInterapt = 10;

    /** Приоритеты */
    const priorityLow         = 1;
    const priorityNormal      = 2;
    const priorityHigh        = 3;
    const priorityCritical    = 4;

    /** Ресурсоемкость */
    const weightLow           = 3;
    const weightNormal        = 4;
    const weightHigh          = 7;
    const weightCritic        = 9;

    /** @var int Статус */
    private $status = 0;

    /** @var int Id */
    private $id = 0;

    /**
     * Метод первичной инициализации
     */
    public function init(){

    }


    /**
     * Метод - восстановления данных
     */
    public function recovery(){

    }


    /**
     * Метод, вызываемый перед выполнением
     */
    public function beforeExecute(){

    }


    /**
     * Выполнение задачи
     */
    abstract public function execute();


    /**
     * Метод, вызываемый после выполнения
     */
    public function afterExecute(){

    }


    /**
     * Метод, вызываемый при резервации для последующего продолжения работы
     */
    public function reservation(){

    }


    /**
     * Метод, вызываемый в случае ошибок при работе задачи
     */
    public function error(){

    }


    /**
     * Метод, вызываемый по завершении задачи
     */
    public function complete(){

    }


    /**
     * Возвращает статус задачи
     * @return int
     */
    final public function getStatus(){
        return $this->status;
    }


    /**
     * Установка статуса задачи
     * @param $status
     * @param $bSave
     */
    final public function setStatus( $status, $bSave = true ){
        $this->status = $status;

        if ($bSave){
            Api::updateStatusTask( $this );
        }
    }

    /**
     * Сохранение id
     * @param int $id
     */
    final public function setId($id){
        $this->id = $id;
    }

    /**
     * Получение id
     * @return int
     */
    final public function getId(){
        return $this->id;
    }


    /**
     * Сохранение параметров
     * @param array $aParams
     */
    final protected function setParams( $aParams = [] ){
        Api::saveTask( $this, $aParams );
    }

}