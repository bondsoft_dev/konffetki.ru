<?php

namespace skewer\build\Component\I18N;


use skewer\build\Component\I18N\models\ServiceSections;
use skewer\build\Component\Section\Page;
use yii\helpers\ArrayHelper;

/**
 * Класс для работы с системными разделами через базу данных
 * Class DBSections
 * @package skewer\build\Component\Language
 */
class DBSections extends SectionsPrototype {

    /** @var []|null Хранилище данных */
    protected $storage = null;

    /**
     * @inheritdoc
     */
    public function getValue( $sName, $sLanguage = '' ){

        if (!$sLanguage)
            $sLanguage = \Yii::$app->language;

        if (!isset($this->storage[$sLanguage]) || is_null($this->storage[$sLanguage]))
            $this->getData($sLanguage);

        return (isset($this->storage[$sLanguage][$sName]))?$this->storage[$sLanguage][$sName]:false;
    }


    /**
     * Выборка данных по текущему языку во внутреннее хранилище
     * @param string $sLanguage
     */
    private function getData( $sLanguage ){

        $aList = ServiceSections::find()
            ->select(['name', 'value'])
            ->where(['language' => $sLanguage])
            ->asArray()
            ->all();

        $this->storage[$sLanguage] = [];
        foreach ( $aList as $aRow )
            $this->storage[$sLanguage][$aRow['name']] = (int)$aRow['value'];

    }

    /**
     * Очистка внутреннего хранилища
     * @deprecated хак для тестов, не использовать
     */
    public function clearData(){
        $this->storage = null;
    }

    /**
     * Список системных разделов для текущего языка
     * @param $sLanguage
     * @return array
     */
    public function getListByLanguage( $sLanguage ){

        if (!$sLanguage)
            $sLanguage = \Yii::$app->language;

        if (!isset($this->storage[$sLanguage]) || is_null($this->storage[$sLanguage]))
            $this->getData($sLanguage);

        return (isset($this->storage[$sLanguage]))?$this->storage[$sLanguage]:false;
    }

    /**
     * @inheritdoc
     */
    public function setSection( $sName, $sTitle, $iValue, $sLanguage ){

        $oSection = ServiceSections::findOne(['name' => $sName, 'language' => $sLanguage]);

        if (!$oSection)
            $oSection = new ServiceSections();

        $oSection->name = $sName;
        $oSection->title = $sTitle;
        $oSection->value = (int)$iValue;
        $oSection->language = $sLanguage;

        if ($oSection->save()) {
            unset($this->storage[$sLanguage]);
            return $oSection->id;
        }

        return false;

    }

    /**
     * @inheritDoc
     */
    public function removeByLanguage($sLanguage)
    {
        ServiceSections::deleteAll(['language' => $sLanguage]);
    }

    /**
     * @inheritDoc
     */
    public function getByValue($iValue)
    {
        return ArrayHelper::getColumn(ServiceSections::findAll(['value' => $iValue]), 'name');
    }

    /**
     * @inheritDoc
     */
    public function getValues($sName)
    {
        return ArrayHelper::map(ServiceSections::findAll(['name' => $sName]), 'language', 'value');
    }

    /**
     * @inheritDoc
     */
    public function getDenySections()
    {
        return array_unique(ArrayHelper::map(ServiceSections::findAll(['name' => ['root', 'library', 'templates', Page::LANG_ROOT]]), 'value', 'value'));
    }


}