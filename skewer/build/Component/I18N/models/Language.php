<?php

namespace skewer\build\Component\I18N\models;

use Yii;
use yii\base\ModelEvent;
use yii\db\ActiveRecord;
use yii\db\BaseActiveRecord;
use yii\db\StaleObjectException;

/**
 * This is the model class for table "language".
 *
 * @property integer $id
 * @property string $name
 * @property string $title
 * @property string $icon
 * @property string $src_lang
 * @property integer $active
 * @property integer $admin
 */
class Language extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'language';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'title'], 'required'],
            [['active', 'admin'], 'integer'],
            [['name', 'src_lang'], 'string', 'max' => 30],
            [['title'], 'string', 'max' => 64],
            [['icon'], 'string', 'max' => 255],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'title' => 'Title',
            'icon' => 'Icon',
            'src_lang' => 'Src Lang',
            'active' => 'Active',
            'admin' => 'Admin'
        ];
    }

    /**
     * @inheritDoc
     */
    public function beforeDelete()
    {
        if ($this->active){
            $this->addError('active', \Yii::t('languages', 'error_lang_is_active'));
            return false;
        }
        return parent::beforeDelete();
    }


    public function delete() {

        /**
         * @todo удаляем языковые значения!
         */

        return parent::delete();
    }

}
