<?php

namespace skewer\build\Component\I18N\models;


use Yii;

/**
 * This is the model class for table "language_values".
 *
 * @property string $value
 * @property string $language
 * @property integer $override
 * @property integer $status
 * @property string $category
 * @property string $message
 * @property string $data
 */
class LanguageValues extends \yii\db\ActiveRecord
{

    const statusNotTranslated = 0;
    const statusTranslated = 1;
    const statusInProcess = 2;

    /** флаг перекрытия метки "не перекрыт" */
    const overrideNo = 0;

    /** флаг перекрытия метки "перекрыт" */
    const overrideYes = 1;

    public static function getStatusList(){
        return [
            self::statusNotTranslated => \Yii::t('languages', 'status_0'),
            self::statusTranslated => \Yii::t('languages', 'status_1'),
            self::statusInProcess => \Yii::t('languages', 'status_2')
        ];
    }

    /**
     * @return array массив меток системных языков
     */
    public static function getSystemLanguage(){
        return ['ru','en'];
    }

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'language_values';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['language', 'category', 'message'], 'required'],
            [['language'], 'unique', 'targetAttribute' => ['language', 'category', 'message']],
            [['value'], 'string'],
            [['override', 'status', 'data'], 'integer'],
            [['message'], 'string', 'max' => 100],
            [['language'], 'string', 'max' => 30],
            [['category'], 'string', 'max' => 32]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'value' => 'Value',
            'language' => 'Language',
            'override' => 'Override',
            'status' => 'Status',
            'category' => 'category',
            'message' => 'message',
            'data' => 'data',
        ];
    }

    /**
     * @inheritDoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        \Yii::$app->getI18n()->clearCacheByCategory($this->category);
        parent::afterSave($insert, $changedAttributes);
    }

    /**
     * @inheritDoc
     */
    public function afterDelete()
    {
        \Yii::$app->getI18n()->clearCacheByCategory($this->category);
        parent::afterDelete();
    }


}
