<?php

namespace skewer\build\Component\I18N\models;

use Yii;

/**
 * Модель для параметров модулей
 *
 * @property integer $id
 * @property string $module
 * @property string $name
 * @property string $value
 * @property string $language
 */
class Params extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'modules_params';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['module', 'name', 'language'], 'required'],
            [['module'], 'unique', 'targetAttribute' => ['module', 'name', 'language']],
            [['module', 'name'], 'string', 'max' => 64],
            [['value'], 'string'],
            [['language'], 'string', 'max' => 16]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'module' => 'Module',
            'name' => 'Name',
            'value' => 'Value',
            'language' => 'Language',
        ];
    }
}
