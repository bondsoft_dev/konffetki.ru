<?php

namespace skewer\build\Component\I18N\models;

use Yii;

/**
 * Модель для системных разделов
 * This is the model class for table "ServiceSections".
 *
 * @property integer $id
 * @property string $name имя. Уникальность по полям $name и $language
 * @property integer $value значение
 * @property string $language язык
 * @property string $title описание
 */
class ServiceSections extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'ServiceSections';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'language', 'title'], 'required'],
            [['name', 'language'], 'unique', 'targetAttribute' => ['name', 'language']],
            [['value'], 'integer'],
            [['name'], 'string', 'max' => 128],
            [['language'], 'string', 'max' => 10],
            [['title'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'name' => 'Name',
            'value' => 'Value',
            'language' => 'Language',
            'title' => 'Title',
        ];
    }

}