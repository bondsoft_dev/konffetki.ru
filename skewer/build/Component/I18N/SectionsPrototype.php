<?php


namespace skewer\build\Component\I18N;


use skewer\build\Component\Section\Page;
use yii\base\Component;

/**
 * Прототип класса для работы с системными разделами.
 * Class SectionsPrototype
 * @package skewer\build\Component\I18N
 */
abstract class SectionsPrototype extends Component
{

    /**
     * Возвращает значение системного раздела
     * @param $sName
     * @param string $sLanguage, по умолчанию - текущий
     * @return mixed
     */
    abstract public function getValue( $sName, $sLanguage = '' );


    /**
     * Возвращает значения системного раздела для всех языков
     * @param $sName
     * @return array language => value
     */
    abstract public function getValues( $sName );


    /**
     * Установка записи системного раздела
     * @param $sName
     * @param $sTitle
     * @param $iValue
     * @param $sLanguage
     * @return bool|int
     */
    abstract function setSection( $sName, $sTitle, $iValue, $sLanguage );

    /**
     * Список системных разделов для текущего языка
     * @param $sLanguage
     * @return array
     */
    abstract function getListByLanguage( $sLanguage );

    /**
     * Удаляет все значения для языка
     * @param $sLanguage
     */
    public function removeByLanguage( $sLanguage ){

    }

    /**
     * Список ключей для значения
     * @param $iValue
     * @return array 'name'
     */
    abstract public function getByValue( $iValue );

    /**
     * Отдает id главной страницы (def 78)
     * @return int
     */
    public function main() {
        /**
         * @todo lang вот тут возможно нужно отдавать стартовый раздел! для LandingPage
         */
        return $this->getValue('main');
    }

    /**
     * Отдает id корневого раздела (def 3)
     * @return int
     */
    public function root() {
        return $this->getValue('root');
    }

    /**
     * Отдает id корневого раздела языковой версии (def 3)
     * @return int
     */
    public function languageRoot() {
        return $this->getValue(Page::LANG_ROOT);
    }

    /**
     * Отдает id раздела 404 ошибки (def 137)
     * @return int
     */
    public function page404() {
        return $this->getValue('404');
    }

    /**
     * Отдает id основного шаблона сборки (def 7)
     * @return int
     */
    public function tplNew() {
        return $this->getValue('tplNew');
    }

    /**
     * Отдает id раздела 'верхнее меню' (def 69)
     * @return int
     */
    public function topMenu() {
        return $this->getValue('topMenu');
    }

    /**
     * Отдает id раздела 'левое меню' (def 70)
     * @return int
     */
    public function leftMenu() {
        return $this->getValue('leftMenu');
    }

    /**
     * Отдает id раздела 'Служебные разделы' (def 120)
     * @return int
     */
    public function tools() {
        return $this->getValue('tools');
    }

    /**
     * Отдает id раздела 'Сервисное меню' (def 243)
     * @return int
     */
    public function serviceMenu() {
        return $this->getValue('serviceMenu');
    }

    /**
     * Отдает id раздела раздела авторизации (def 274)
     * @return int
     */
    public function auth() {
        return $this->getValue('auth');
    }

    /**
     * Отдает id корневого раздела с шаблонами (def 2)
     * @return int
     */
    public function templates() {
        return $this->getValue('templates');
    }

    /**
     * Отдает id корневого раздела с библиотеками (def 1)
     * @return int
     */
    public function library() {
        return $this->getValue('library');
    }

    /**
     * Отдает id шаблона lp
     * @return int
     */
    public function landingPageTpl(){
        return $this->getValue('landingPageTpl');
    }

    /**
     * Список разделов, запрещенных для показа
     * @return array
     */
    public function getDenySections(){
        return array_unique(array_merge(
            $this->getValues('root'),
            $this->getValues(Page::LANG_ROOT),
            $this->getValues('library'),
            $this->getValues('templates')
        ));
    }

}