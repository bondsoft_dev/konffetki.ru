<?php

namespace skewer\build\Component\I18N\command\AddBranch;


use skewer\build\Component\Section\Tree;
use skewer\models\TreeSection;

/**
 * Проставление ссылок копируемым разделам
 * Class LinkSection
 * @package skewer\build\Component\I18N\command\AddBranch
 */
class LinkSection extends Prototype
{

    protected $copyList = [];

    /**
     * @inheritDoc
     */
    protected function init()
    {
        parent::init();

        $this->listenTo(CopySections::COPY_SECTIONS, 'setCopySections');
    }


    public function setCopySections( $aParams ){
        $this->copyList = $aParams;
    }


    /**
     * @inheritDoc
     */
    function execute()
    {

        if (!$this->copyList)
            return;

        $aSections = Tree::getCachedSection();

        foreach( $aSections as $sKey => $aSection ){

            if (!isset($this->copyList[$sKey])) {
                continue;
            }

            /** Копируем ссылки */

            if ($aSection['link'] != ''){
                if (preg_match('/^\[\d+\]$/', $aSection['link'])){
                    $linkId = str_replace(['[', ']'], '', $aSection['link']);

                    if (isset($this->copyList[$linkId])){
                        /** @var TreeSection $oSection */
                        $oSection = TreeSection::findOne(['id' => $this->copyList[$sKey]]);
                        if ($oSection){
                            $oSection->link = '[' . $this->copyList[$linkId] . ']';
                            $oSection->save();
                        }

                    }
                }
            }

        }

    }

    /**
     * @inheritDoc
     */
    function rollback()
    {

    }


}