<?php

namespace skewer\build\Component\I18N\command\AddBranch;


use skewer\build\Component\Section\Parameters;
use yii\helpers\ArrayHelper;

/**
 * Копирование разводок категорий
 * Class CategoryViewer
 * @package skewer\build\Component\I18N\command\AddBranch
 */
class CategoryViewer extends Prototype
{

    protected $copyList = [];

    /**
     * @inheritDoc
     */
    protected function init()
    {
        parent::init();

        $this->listenTo(CopySections::COPY_SECTIONS, 'setCopySections');
    }


    public function setCopySections( $aParams ){
        $this->copyList = $aParams;
    }


    /**
     * @inheritDoc
     */
    function execute()
    {

        if (!$this->copyList)
            return;

        /**
         * Обойдем новые разделы и проставим правильную разводку категорий
         */

        $aParams = Parameters::getList()->group('CategoryViewer')->asArray()->get();
        $aParams = ArrayHelper::map($aParams, 'name', 'value', 'parent');

        foreach($this->copyList as $iSource => $iSection){

            if (isset($aParams[$iSection])){
                if ($aParams[$iSection]['category_from'] && isset($this->copyList[$aParams[$iSection]['category_from']])){
                    Parameters::setParams($iSection, 'CategoryViewer', 'category_from', $this->copyList[$aParams[$iSection]['category_from']]);
                }
            }

        }

    }

    /**
     * @inheritDoc
     */
    function rollback()
    {

    }


}