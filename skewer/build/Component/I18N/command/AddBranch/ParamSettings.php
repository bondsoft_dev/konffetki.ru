<?php

namespace skewer\build\Component\I18N\command\AddBranch;


use skewer\build\Component\Section\Page;
use skewer\build\Adm\ParamSettings\Api;
use skewer\build\Component\Section\Parameters;
use skewer\build\Component\Site\Type;

/**
 * Копирование параметров из модуля "Настройка параметров"
 * Class ParamSettings
 * @package skewer\build\Component\I18N\command\AddBranch
 */
class ParamSettings extends Prototype
{

    /** @var int Раздел-источник */
    public $sourceSection = 0;

    /** @var bool Флаг использования перевода */
    public $useTranslate = true;

    /** @var bool Флаг копирования */
    public $copy = true;

    /**
     * @inheritDoc
     */
    protected function init()
    {
        parent::init();

        if (!$this->sourceSection){
            $this->sourceSection = \Yii::$app->sections->getValue(Page::LANG_ROOT, $this->getSourceLanguageName());
        }
    }


    /**
     * @inheritDoc
     */
    function execute()
    {

        $oParamApi = new Api();

        $aParams = $oParamApi->getParams();

        $bCatalogSite = Type::hasCatalogModule();

        foreach($aParams as $aParam) {

            if (!$bCatalogSite && in_array($aParam['group'], ['CatalogFilter', 'catalog', 'catalog2', 'catalog3'])) {
                continue;
            }

            if (!isset($aParam['section'])) {
                $aParam['section'] = 'all';
            }

            if ($aParam['section']){
                $oParam = Parameters::getByName($this->sourceSection, $aParam['group'], $aParam['name']);
                if ($oParam){

                    if (!$this->copy){
                        /** Перенос */
                        $oNewParam = Parameters::getByName($this->getRootSection(), $aParam['group'], $aParam['name']);
                        if (!$oNewParam)
                            $oNewParam = $oParam;

                        $oNewParam->parent = $this->getRootSection();
                        if ($this->useTranslate){
                            $oNewParam->title = $this->getTitle($oNewParam['title']);
                        }
                        $oNewParam->access_level = 0;
                        $oNewParam->save();
                    }else{
                        /** Вообще копирование здесь не произойдет, так как все уже будет скопировано в других командах */
                        $oParam = Parameters::copyToSection($oParam, $this->getRootSection());
                        if (!$oParam)
                            $oParam = Parameters::getByName($this->getRootSection(), $aParam['group'], $aParam['name']);

                        if ($this->useTranslate){
                            $oParam->title = $this->getTitle($aParam['title']);
                        }

                        $oParam->access_level = 0;
                        $oParam->save();
                    }

                }
            }
        }

    }

    /**
     * @inheritDoc
     */
    function rollback()
    {

    }

    /**
     * Обработка заголовка
     * @param string $title
     * @return string
     */
    private function getTitle($title)
    {
        if (strpos($title, '.') !== false) {
            list($sCategory, $sTitle) = explode('.', $title, 2);
            return \Yii::t($sCategory, $sTitle, [], $this->getLanguageName());
        }
        return $title;
    }


}