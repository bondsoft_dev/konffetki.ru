<?php

namespace skewer\build\Component\I18N\command\AddBranch;


/**
 * Активация языка
 * Class ActivateLanguage
 * @package skewer\build\Component\I18N\command\AddBranch
 */
class ActivateLanguage extends Prototype
{
    /**
     * @inheritDoc
     */
    function execute()
    {
        $this->getLanguage()->active = 1;
        $this->getLanguage()->save();
    }

    /**
     * @inheritDoc
     */
    function rollback()
    {
        $this->getLanguage()->active = 0;
        $this->getLanguage()->save();
    }


}