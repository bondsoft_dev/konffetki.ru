<?php

namespace skewer\build\Component\I18N\command\AddBranch;


use skewer\build\Component\Section\Parameters;
use skewer\build\Component\Section\Params\Type;


/**
 * Команда, отмечающая параметры в шаблоне, которые будут тянуться из языковых веток
 * Class LanguageParams
 * @package skewer\build\Component\I18N\command\AddBranch
 */
class LanguageParams extends Prototype
{

    /**
     * Список параметров настроек сайта, которые должны быть уникальные для каждой языковой ветки
     * @var array
     */
    protected $parameters = [
        'copyright',
        'contacts',
        'headtext1',
        'headtext2',
        'headtext3',
        'headtext4',
        'headtext5',
        'copyright_dev',
        'site_name',
        'footertext4'
    ];

    /**
     * @inheritDoc
     */
    function execute()
    {

        /** @var \skewer\models\Parameters[] $aParams */
        $aParams = \skewer\models\Parameters::find()
            ->where(['group' => Parameters::settings, 'parent' => \Yii::$app->sections->root(), 'name' => $this->parameters])
            ->all();
        ;

        if ($aParams){
            foreach( $aParams as $oParam ){
                Parameters::setParams(\Yii::$app->sections->tplNew(), Parameters::settings, $oParam->name,
                    $oParam->value, $oParam->show_val, $oParam->title, Type::paramLanguage);

                $oNewParam = Parameters::copyToSection($oParam, $this->getRootSection());
                if (!$oNewParam) {
                    $oNewParam = Parameters::getByName($this->getRootSection(), Parameters::settings, $oParam->name);
                    $oNewParam->value = $oParam->value;
                    $oNewParam->show_val = $oParam->show_val;
                    $oNewParam->title = $oParam->title;
                    $oNewParam->access_level = $oParam->access_level;
                    $oNewParam->save();
                }

            }
        }

        /** Порядок полей */
        $this->setOrderField();

    }

    /**
     * @inheritDoc
     */
    function rollback()
    {

    }

    private function setOrderField()
    {
        $aOrderFields = [
            'site_name',
            'copyright',
            'contacts',
            'headtext1',
            'headtext2',
            'headtext3',
            'headtext4',
            'headtext5'
        ];

        /**
         * Сделаем человеческий порядок
         */
        $oParam = Parameters::getByName($this->getRootSection(), Parameters::settings, 'field_order');
        if (!$oParam) {
            $oParam = Parameters::createParam([
                'parent' => $this->getRootSection(),
                'group' => Parameters::settings,
                'name' => 'field_order',
            ]);
        }

        if ($oParam) {
            $val = $oParam->show_val;
            $val = trim($val);
            $val = preg_replace('/\x0a+|\x0d+/Uims', '', $val);
            $aFields = explode(';', $val);

            if ($aFields) {

                foreach($aOrderFields as $sField){
                    if (($key = array_search('.:'.$sField, $aFields)) !== false)
                        unset($aFields[$key]);
                }

            }

            foreach(array_reverse($aOrderFields) as $sField)
                array_unshift($aFields, '.:'.$sField);

            $oParam->show_val = implode(";\r\n", $aFields);
            $oParam->save();
        }
    }

}