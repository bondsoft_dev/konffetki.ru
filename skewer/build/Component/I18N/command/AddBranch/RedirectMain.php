<?php

namespace skewer\build\Component\I18N\command\AddBranch;


use skewer\build\Component\Section\Page;
use skewer\models\TreeSection;

/**
 * @todo после доработок в tree возможно уже не требуется
 * Создание редиректа со страницы LANG_ROOT на главную
 * Class RedirectMain
 * @package skewer\build\Component\I18N\command\AddBranch
 */
class RedirectMain extends Prototype
{
    /**
     * @inheritDoc
     */
    function execute()
    {

        $main = \Yii::$app->sections->getValue( 'main',  $this->getLanguageName() );
        $langRoot = \Yii::$app->sections->getValue( Page::LANG_ROOT,  $this->getLanguageName() );

        if ($main && $langRoot) {
            $oSection = TreeSection::findOne(['id' => $langRoot]);
            if ($oSection) {
                $oSection->link = sprintf('[%d]', $main);
                $oSection->save();
            }
        }

    }

    /**
     * @inheritDoc
     */
    function rollback()
    {

    }

}