<?php

namespace skewer\build\Component\I18N\command\AddBranch;


use skewer\build\Component\Section\Parameters;
use skewer\build\Component\Section\Params\ListSelector;
use skewer\build\Component\Section\Tree;
use skewer\build\Component\Section\Visible;
use skewer\models\TreeSection;
use \skewer\build\Component\Site;

/**
 * Команда создания главного раздела для языковой версии
 * Class CreateRootSection
 * @package skewer\build\Component\I18N\command\AddBranch
 */
class CreateRootSection extends Prototype
{

    /**
     * Флаг, означающий, что нужно все текущие разделы переместить в новый
     * @var bool
     */
    public $allInSection = false;

    /**
     * Флаг запрета удаления раздела в случае откатов
     * @var bool
     */
    public $noDelete = false;

    /**
     * Выполнение команды
     * @throws \Exception
     */
    function execute()
    {

        /**
         * @todo кажется не работает
         */
        if (!is_null(Tree::getSectionByAlias( $this->getLanguageName(), \Yii::$app->sections->root() )))
            throw new \Exception(\Yii::t('languages', 'error_alias_is_used'));

        $oParent = $this->createRootSection();

        if (!$oParent)
            throw new \Exception(\Yii::t('languages', 'error_root_section_create'));

        if ($this->allInSection)
            $this->copyAllToRootSection($oParent->id);

    }

    /**
     * Создаем корневой раздел
     * @param string $sLanguage Язык
     * @param string $sAlias Псевдоним
     * @param bool $bHideFromPath Скрыть раздел из пути
     * @return bool|TreeSection
     * @throws \Exception
     */
    private function createRootSection(){

        $oParent = Tree::addSection(\Yii::$app->sections->root(), mb_convert_case($this->getLanguageName(), MB_CASE_UPPER), \Yii::$app->sections->tplNew(),
            $this->getLanguageName(), $this->allInSection?Visible::HIDDEN_FROM_PATH:Visible::VISIBLE);

        if (!$oParent)
            throw new \Exception(\Yii::t('languages', 'error_root_section_create'));

        $this->notify(self::LANGUAGE_ROOT_CREATE, [$this->getLanguageName(), $oParent->id]);

        \Yii::$app->sections->setSection('lang_root', \Yii::t('app', 'lang_root'), $oParent->id, $this->getLanguageName());

        /** Установим ему язык */
        Parameters::setParams($oParent->id, Parameters::settings, Parameters::language, $this->getLanguageName());

        return $oParent;
    }


    /**
     * Откат команды
     */
    function rollback()
    {

        if ($this->noDelete)
            return;

        $iSection = $this->getRootSection();

        if ($iSection)
            Tree::removeSection($iSection);

    }

    /**
     * @param $id
     */
    protected function copyAllToRootSection($id)
    {
        $aSections = Tree::getSubSections(\Yii::$app->sections->root(), true, true);

        $iPos = array_search($id, $aSections);
        if ($iPos !== false)
            unset($aSections[$iPos]);

        /**
         * @todo можно ли просто так переносить? или же это потребует еще и перестроение чего-либо?
         */
        if ($aSections) {
            TreeSection::updateAll(
                ['parent' => $id], ['id' => $aSections]
            );
        }
    }

}