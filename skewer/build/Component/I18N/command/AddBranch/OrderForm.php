<?php

namespace skewer\build\Component\I18N\command\AddBranch;


use skewer\build\Component\Command\Exception;
use skewer\build\Component\Forms\FieldRow;
use skewer\build\Component\Forms\FieldTable;
use skewer\build\Component\Forms\Row;
use skewer\build\Component\Forms\Table;
use skewer\build\Component\orm\Query;
use skewer\build\Component\Section\Parameters;
use yii\helpers\ArrayHelper;

/**
 * Форма заказа
 * Class OrderForm
 * @package skewer\build\Component\I18N\command\AddBranch
 */
class OrderForm extends Prototype
{
    private $iForm = 0;

    private $defaultFields = [
        'naimenovanie-tovara' => 'goods_name',
        'person' => 'person',
        'phone' => 'phone',
        'email' => 'email',
        'text' => 'text',
    ];

    protected $copyList = [];

    /**
     * @inheritDoc
     */
    protected function init()
    {
        parent::init();

        $this->listenTo(CopySections::COPY_SECTIONS, 'setCopySections');
    }


    public function setCopySections( $aParams ){
        $this->copyList = $aParams;
    }


    /**
     * @inheritDoc
     */
    function execute()
    {

        $iSourceSection = \Yii::$app->sections->getValue('orderForm', $this->getSourceLanguageName());

        if (!$iSourceSection)
            return;

        $oSourceForm = Table::get4Section($iSourceSection);

        if (!$oSourceForm)
            return;

        $iSection = \Yii::$app->sections->getValue('orderForm', $this->getLanguageName());

        if (!$iSection)
            return;

        $oForm = $this->copyForm($oSourceForm);
        $this->copyAddData($oSourceForm);
        $this->copyFields($oSourceForm);
        $this->copyLinks($oSourceForm);
        /** Сохранение еще раз после добавления полей, так как по ним строится таблица заказов */
        $oForm->save();
        $this->setFormSection($iSection);
        $this->setParams($iSection);

    }

    /**
     * @inheritDoc
     */
    function rollback()
    {
        /**
         * Удалять форму нельзя - мало ли где используется?
         * Раздел удалять не будем - если что-то не так, он удалиться в Action CopySections
         */
        return;
    }

    /**
     * Копирование формы
     * @param Row $oSourceForm
     * @throws Exception
     * @return Row
     */
    protected function copyForm(Row $oSourceForm)
    {
        $oNewForm = new Row();

        $aData = $oSourceForm->getData();
        unset($aData['form_id']);
        $oNewForm->setData($aData);

        $sTitle = \Yii::t('data/order', 'order_form_title', [], $this->getLanguageName());
        if ($sTitle != 'order_form_title'){
            $oNewForm->form_title = $sTitle;
        }

        $oNewForm->setUniqName();

        $this->iForm = $oNewForm->save();

        if (!$this->iForm)
            throw new Exception('Form save error: ' . $oNewForm->getError() );

        return $oNewForm;
    }

    /**
     * Копированиек полей
     * @param Row $oSourceForm
     */
    protected function copyFields(Row $oSourceForm)
    {
        $aFields = FieldTable::find()
            ->where('form_id', $oSourceForm->getId())
            ->getAll();

        /** @var FieldRow $oFieldRow */
        foreach ($aFields as $oFieldRow) {

            $oFieldRow->param_id = 0;
            $oFieldRow->form_id = $this->iForm;

            if (isset($this->defaultFields[$oFieldRow->param_name])){
                $sTitle = \Yii::t('date/order', 'order_form_field_' . $oFieldRow->param_name, [], $this->getLanguageName());
                if ($sTitle != 'order_form_field_' . $oFieldRow->param_name){
                    $oFieldRow->param_title = $sTitle;
                }
            }

            $oFieldRow->save();
        }
    }

    /**
     * Копирование дополнительных данных формы
     * @param Row $oSourceForm
     */
    protected function copyAddData(Row $oSourceForm)
    {
        $aAddData = $oSourceForm->getAddData();

        if (!$aAddData)
            $aAddData = [];

        $answer_title = \Yii::t('data/order', 'order_form_add_answer_title', [], $this->getLanguageName());
        if ($answer_title != 'order_form_add_answer_title')
            $aAddData['answer_title'] = $answer_title;

        $answer_body = \Yii::t('data/order', 'order_form_add_answer_body', [], $this->getLanguageName());
        if ($answer_body != 'order_form_add_answer_body')
            $aAddData['answer_body'] = $answer_body;

        $agreed_title = \Yii::t('data/order', 'order_form_add_agreed_title', [], $this->getLanguageName());
        if ($agreed_title != 'order_form_add_agreed_title')
            $aAddData['agreed_title'] = $agreed_title;

        $agreed_text = \Yii::t('data/order', 'order_form_add_agreed_text', [], $this->getLanguageName());
        if ($agreed_text != 'order_form_add_agreed_text')
            $aAddData['agreed_text'] = $agreed_text;

        Query::InsertInto('forms_add_data')
            ->set('form_id', $this->iForm)
            ->set('answer_title', ArrayHelper::getValue($aAddData, 'answer_title', ''))
            ->set('answer_body', ArrayHelper::getValue($aAddData, 'answer_body', ''))
            ->set('agreed_title', ArrayHelper::getValue($aAddData, 'agreed_title', ''))
            ->set('agreed_text', ArrayHelper::getValue($aAddData, 'agreed_text', ''))
            ->get();
    }

    /**
     * Копирование связей с полями
     * @param Row $oSourceForm
     */
    protected function copyLinks(Row $oSourceForm)
    {
        $aLinks = Query::SelectFrom('forms_links')
            ->where('form_id', $oSourceForm->getId())
            ->asArray()
            ->getAll();

        foreach ($aLinks as $aLink) {
            Query::InsertInto('forms_links')
                ->set('form_id', $this->iForm)
                ->set('form_field', $aLink['form_field'])
                ->set('card_field', $aLink['card_field'])
                ->get();
        }
    }

    /**
     * Связь раздела заказа с формой
     * @param $iSection
     * @return bool
     */
    protected function setFormSection($iSection)
    {
        return Query::InsertInto('forms_section')
            ->set('form_id', $this->iForm)
            ->set('section_id', $iSection)
            ->get();
    }

    /**
     * Установка раздела с формой заказа для разделов новой языковой ветки
     * @param int $iSection
     */
    protected function setParams($iSection)
    {
        $aParams = Parameters::getList(array_values($this->copyList))
            ->group('content')
            ->name('buyFormSection')
            ->get();
        if ($aParams)
            foreach ($aParams as $oParam) {
                $oParam->value = $iSection;
                $oParam->save();
            }

        /** Главная */
        $aParams = Parameters::getList(\Yii::$app->sections->getValue('main', $this->getLanguageName()))
            ->name('buyFormSection')
            ->group(['catalog', 'catalog2', 'catalog3'])
            ->get();

        if ($aParams)
            foreach ($aParams as $oParam) {
                $oParam->value = $iSection;
                $oParam->save();
            }
    }


}