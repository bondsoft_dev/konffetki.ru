<?php

namespace skewer\build\Component\I18N\command\AddBranch;


use skewer\build\Component\I18N\models\Params;

/**
 * Копирование данных модулей
 * Class ModuleParams
 * @package skewer\build\Component\I18N\command\AddBranch
 */
class ModuleParams extends Prototype
{
    /**
     * @inheritDoc
     */
    function execute()
    {
        /** @var []\Params $aParams */
        $aParams = Params::findAll(['language' => $this->getSourceLanguageName()]);

        /** @todo можно сократить запросы */
        if ($aParams){
            foreach( $aParams as $oParam ){

                $oNewParam = Params::findOne([
                    'module' => $oParam->module,
                    'name' => $oParam->name,
                    'language' => $this->getLanguageName()
                ]);

                if (!$oNewParam){
                    $oNewParam = new Params();
                    $oNewParam->module = $oParam->module;
                    $oNewParam->name = $oParam->name;
                    $oNewParam->language = $this->getLanguageName();
                }

                $value = \Yii::t('data/' . $oNewParam->module, $oNewParam->name, [], $this->getLanguageName());

                $oNewParam->value = ($value == $oNewParam->name)?$oParam->value:$value;
                $oNewParam->language = $this->getLanguageName();
                $oNewParam->save();
            }
        }
    }

    /**
     * @inheritDoc
     */
    function rollback()
    {
        Params::deleteAll(['language' => $this->getLanguageName()]);
    }

}