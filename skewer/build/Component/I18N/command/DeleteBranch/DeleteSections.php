<?php

namespace skewer\build\Component\I18N\command\DeleteBranch;


use skewer\build\Component\Section\Page;
use skewer\build\Component\Section\Tree;

/**
 * Удаление разделов
 * Class DeleteSections
 * @package skewer\build\Component\I18N\command\DeleteBranch
 */
class DeleteSections extends Prototype
{
    /**
     * @inheritDoc
     */
    function execute()
    {
        $iRootSection = \Yii::$app->sections->getValue(Page::LANG_ROOT, $this->getLanguageName());

        if ($iRootSection){
            Tree::removeSection($iRootSection);
        }
    }

    /**
     * @inheritDoc
     */
    function rollback()
    {

    }


}