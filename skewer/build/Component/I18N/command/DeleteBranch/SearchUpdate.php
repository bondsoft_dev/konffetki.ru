<?php

namespace skewer\build\Component\I18N\command\DeleteBranch;


use skewer\build\Component\Search\SearchIndex;
use skewer\build\Component\SEO\Service;

/**
 * Перестроение поиска
 * Class SearchUpdate
 * @package skewer\build\Component\I18N\command\DeleteBranch
 */
class SearchUpdate extends Prototype
{
    /**
     * @inheritDoc
     */
    function execute()
    {
        Service::rebuildSearchIndex();
        SearchIndex::update()->set('status',0)->where('status',1)->get();
        Service::updateSearchIndex();
        Service::updateSiteMap();
    }

    /**
     * @inheritDoc
     */
    function rollback()
    {

    }

}