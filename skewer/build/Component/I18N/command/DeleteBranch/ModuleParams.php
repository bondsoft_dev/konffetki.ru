<?php

namespace skewer\build\Component\I18N\command\DeleteBranch;


use skewer\build\Component\I18N\models\Params;

/**
 * Копирование данных модулей
 * Class ModuleParams
 * @package skewer\build\Component\I18N\command\DeleteBranch
 */
class ModuleParams extends Prototype
{
    /**
     * @inheritDoc
     */
    function execute()
    {
        Params::deleteAll(['language' => $this->getLanguageName()]);
    }

    /**
     * @inheritDoc
     */
    function rollback()
    {

    }

}