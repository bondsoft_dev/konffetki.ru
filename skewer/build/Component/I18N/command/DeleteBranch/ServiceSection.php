<?php

namespace skewer\build\Component\I18N\command\DeleteBranch;


/**
 * Копирование сервисных разделов
 * Основная часть будет скопирована при копировании разделов. Здесь будет только то, что не создает новый раздел, например, root (3)
 * Class ServiceSection
 * @package skewer\build\Component\I18N\command\DeleteBranch
 */
class ServiceSection extends Prototype
{
    /**
     * @inheritDoc
     */
    function execute()
    {
        \Yii::$app->sections->removeByLanguage( $this->getLanguageName() );
    }

    /**
     * @inheritDoc
     */
    function rollback()
    {

    }


}