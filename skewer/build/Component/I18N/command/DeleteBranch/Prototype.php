<?php

namespace skewer\build\Component\I18N\command\DeleteBranch;


use skewer\build\Component\I18N\models\Language;

/**
 * Прототип команды для установки языковой версии.
 * Class Prototype
 * @package skewer\build\Component\I18N\command\DeleteBranch
 */
abstract class Prototype extends \skewer\build\Component\Command\Action
{
    /**
     * @var Language Текущий язык
     */
    private $language = null;

    function __construct(Language $language)
    {
        $this->language = $language;
    }

    /**
     * Инициализация
     * Добавление слушателей событий
     */
    protected function init()
    {

    }

    /**
     * Текущий язык
     * @return Language
     */
    public function getLanguage()
    {
        return $this->language;
    }

    /**
     * Текущий язык
     * @return string
     */
    public function getLanguageName()
    {
        return $this->language->name;
    }

}