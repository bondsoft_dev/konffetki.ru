<?php

namespace skewer\build\Component\I18N\command\DeleteBranch;


use skewer\build\Component\orm\Query;

/**
 * Статусы заказа
 * Class OrderStatus
 * @package skewer\build\Component\I18N\command\AddBranch
 */
class OrderStatus extends Prototype
{
    /**
     * @inheritDoc
     */
    function execute()
    {

        Query::DeleteFrom('orders_status_lang')
            ->where('language', $this->getLanguageName())
            ->get();

    }

    /**
     * @inheritDoc
     */
    function rollback()
    {

    }


}