<?php

namespace skewer\build\Component\I18N\command\DeleteBranch;


/**
 * Деактивация языка
 * Class DeactivateLanguage
 * @package skewer\build\Component\I18N\command\DeleteBranch
 */
class DeactivateLanguage extends Prototype
{
    /**
     * @inheritDoc
     */
    function execute()
    {
        $this->getLanguage()->active = 0;
        $this->getLanguage()->save();
    }

    /**
     * @inheritDoc
     */
    function rollback()
    {
        $this->getLanguage()->active = 1;
        $this->getLanguage()->save();
    }


}