<?php

namespace skewer\build\Component\I18N\command\SwitchLanguage;


use skewer\build\Adm\Order\model\Status;
use skewer\build\Component\orm\Query;

/**
 * Перепись параметров модулей
 * Class OrderStatus
 * @package skewer\build\Component\I18N\command\SwitchLanguage
 */
class OrderStatus extends Prototype
{
    /**
     * @inheritDoc
     */
    function execute()
    {

        Query::DeleteFrom('orders_status_lang')->where('language', $this->getNewLanguage())->get();

        /**
         * Скопировать переводы статусов
         */
        $aStatusList = Status::find()->multilingual()->all();

        if ($aStatusList){
            /** @var Status $oStatus */
            foreach ($aStatusList as $oStatus) {

                $sValue = \Yii::t('data/order', 'status_'.$oStatus->name, [], $this->getNewLanguage());
                if ($sValue == 'status_'.$oStatus->name)
                    $sValue = $oStatus->getLangAttribute('title_' . $this->getOldLanguage());

                $sValue = (string)$sValue;

                Query::InsertInto('orders_status_lang')
                    ->set('status_id', $oStatus->id)
                    ->set('language', $this->getNewLanguage())
                    ->set('title', $sValue)
                    ->set('active', (int)$oStatus->getLangAttribute('active_'.$this->getOldLanguage()))
                    ->get();

            }

        }

        Query::DeleteFrom('orders_status_lang')->where('language', $this->getOldLanguage())->get();

    }

    /**
     * @inheritDoc
     */
    function rollback()
    {

        Query::DeleteFrom('orders_status_lang')->where('language', $this->getNewLanguage())->get();

    }


}