<?php

namespace skewer\build\Component\I18N\command\SwitchLanguage;


use skewer\build\Component\Command\Action;
use skewer\build\Component\Search\SearchIndex;
use skewer\build\Component\SEO\Service;

/**
 * Сброс поиска
 * Class Search
 * @package skewer\build\Component\I18N\command\SwitchLanguage
 */
class Search extends Action
{
    /**
     * @inheritDoc
     */
    protected function init()
    {

    }

    /**
     * @inheritDoc
     */
    function execute()
    {
        Service::rebuildSearchIndex();
        SearchIndex::update()->set('status',0)->where('status',1)->get();
        Service::updateSearchIndex();
        Service::updateSiteMap();
    }

    /**
     * @inheritDoc
     */
    function rollback()
    {

    }


}