<?php

namespace skewer\build\Component\I18N\command\SwitchLanguage;
use skewer\build\Component\I18N\Languages;
use skewer\build\Component\Section\Parameters;


/**
 * Установка языка
 * Class SwichLanguage
 * @package skewer\build\Component\I18N\command\SwitchLanguage
 */
class SwichLanguage extends Prototype
{
    /**
     * @inheritDoc
     */
    function execute()
    {
        \SysVar::set('language', $this->getNewLanguage());
        Languages::setActive($this->getNewLanguage(), 1);
        Languages::setActive($this->getOldLanguage(), 0);
        Parameters::setParams(\Yii::$app->sections->getValue('root', $this->getNewLanguage()), Parameters::settings, 'language', $this->getNewLanguage());
        \Yii::$app->language = $this->getNewLanguage();
    }

    /**
     * @inheritDoc
     */
    function rollback()
    {
        \SysVar::set('language', $this->getOldLanguage());
        Languages::setActive($this->getNewLanguage(), 0);
        Languages::setActive($this->getOldLanguage(), 1);
        Parameters::setParams(\Yii::$app->sections->getValue('root', $this->getOldLanguage()), Parameters::settings, 'language', $this->getOldLanguage());
        \Yii::$app->language = $this->getOldLanguage();
    }

}