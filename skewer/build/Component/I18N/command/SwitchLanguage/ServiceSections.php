<?php

namespace skewer\build\Component\I18N\command\SwitchLanguage;


/**
 * Перепись языка для системных разделов
 * Class ServiceSections
 * @package skewer\build\Component\I18N\command\SwitchLanguage
 */
class ServiceSections extends Prototype
{
    /**
     * @inheritDoc
     */
    function execute()
    {
        \skewer\build\Component\I18N\models\ServiceSections::updateAll(['language' => $this->getNewLanguage()], ['language' => $this->getOldLanguage()]);
    }

    /**
     * @inheritDoc
     */
    function rollback()
    {
        \skewer\build\Component\I18N\models\ServiceSections::updateAll(['language' => $this->getOldLanguage()], ['language' => $this->getNewLanguage()]);
    }


}