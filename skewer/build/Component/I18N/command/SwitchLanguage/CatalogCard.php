<?php

namespace skewer\build\Component\I18N\command\SwitchLanguage;
use skewer\build\Component\Catalog\Attr;
use skewer\build\Component\Catalog\Card;
use skewer\build\Component\Catalog\model\FieldGroupRow;
use skewer\build\Component\Catalog\model\FieldGroupTable;


/**
 * Перевод карточки товаров
 * Class CatalogCard
 * @package skewer\build\Component\I18N\command\SwitchLanguage
 */
class CatalogCard extends Prototype
{

    private $aCards = [
        Card::DEF_BASE_CARD => 'cart_base_title',
        'group1' => 'cart_ext_title',
    ];
    /**
     * @inheritDoc
     */
    function execute()
    {

        $aCards = Card::getGoodsCards(true);

        if ($aCards){
            foreach($aCards as $oCard){

                /** Смена имени карточки */
                if (isset($this->aCards[$oCard->name])){
                    $oCard->title = \Yii::t('data/catalog', $this->aCards[$oCard->name], [], $this->getNewLanguage());
                    $oCard->save();
                }

                /** Названия групп */
                $aGroups = $this->getGroupList($oCard->id);
                if ($aGroups){
                    foreach($aGroups as $oGroup){
                        $sTitle = \Yii::t('data/catalog', 'group_' . $oGroup->name . '_title', [], $this->getNewLanguage());
                        if ($sTitle != 'group_' . $oGroup->name . '_title') {
                            $oGroup->title = $sTitle;
                            $oGroup->save();
                        }
                    }
                }

                /** Названия полей */
                foreach($oCard->getFields() as $oField){
                    $sTitle = \Yii::t('data/catalog', 'field_' . $oField->name . '_title', [], $this->getNewLanguage());
                    if ($sTitle != 'field_' . $oField->name . '_title') {
                        $oField->title = $sTitle;
                        $oField->save();
                    }

                    /** Для цены нужно сменить  */
                    if ($oField->name == 'price'){
                        $oField->setAttr(Attr::MEASURE, \Yii::t('data/catalog', 'field_price_measure', [], $this->getNewLanguage()));
                    }
                }

                Card::build( $oCard->id );

            }
        }

    }

    /**
     * @inheritDoc
     */
    function rollback()
    {

    }

    /**
     * @param $card
     * @return FieldGroupRow[]
     */
    private function getGroupList($card)
    {
        return FieldGroupTable::find()->where(['entity' => $card])->get();
    }


}