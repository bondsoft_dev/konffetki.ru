<?php

namespace skewer\build\Component\I18N\command\SwitchLanguage;


/**
 * Прототип команды для смены языка
 * Class Prototype
 * @package skewer\build\Component\I18N\command\SwitchLanguage
 */
abstract class Prototype extends \skewer\build\Component\Command\Action
{
    /**
     * @var string Новый язык
     */
    private $newLanguage = '';

    /**
     * @var string Новый язык
     */
    private $oldLanguage = '';

    /**
     * @param $oldLanguage
     * @param $newLanguage
     */
    function __construct($oldLanguage, $newLanguage)
    {
        $this->oldLanguage = $oldLanguage;
        $this->newLanguage = $newLanguage;
    }

    /**
     * Инициализация
     * Добавление слушателей событий
     */
    protected function init()
    {

    }

    /**
     * Новый язык
     * @return string
     */
    public function getNewLanguage()
    {
        return $this->newLanguage;
    }

    /**
     * Старый язык
     * @return string
     */
    public function getOldLanguage()
    {
        return $this->oldLanguage;
    }

}