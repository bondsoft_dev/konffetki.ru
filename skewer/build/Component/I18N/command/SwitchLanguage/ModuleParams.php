<?php

namespace skewer\build\Component\I18N\command\SwitchLanguage;


use skewer\build\Component\I18N\models\Params;

/**
 * Перепись параметров модулей
 * Class ModuleParams
 * @package skewer\build\Component\I18N\command\SwitchLanguage
 */
class ModuleParams extends Prototype
{
    /**
     * @inheritDoc
     */
    function execute()
    {

        $aParams = Params::findAll(['language' => $this->getOldLanguage()]);

        if ($aParams)
            foreach($aParams as $oParam){

                $oNewParam = Params::findOne([
                    'module' => $oParam->module,
                    'name' => $oParam->name,
                    'language' => $this->getNewLanguage()
                ]);

                if (!$oNewParam){
                    $oNewParam = new Params();
                    $oNewParam->module = $oParam->module;
                    $oNewParam->name = $oParam->name;
                    $oNewParam->language = $this->getNewLanguage();
                }

                $value = \Yii::t('data/' . $oNewParam->module, $oNewParam->name, [], $this->getNewLanguage());

                $oNewParam->value = ($value == $oNewParam->name)?$oParam->value:$value;
                $oNewParam->save();
            }


        /**
         * @todo lang Удалять ли параметры старого языка?
         */
    }

    /**
     * @inheritDoc
     */
    function rollback()
    {
        Params::deleteAll(['language' => $this->getNewLanguage()]);
    }


}