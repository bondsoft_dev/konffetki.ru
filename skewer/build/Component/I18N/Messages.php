<?php


namespace skewer\build\Component\I18N;


use skewer\build\Component\I18N\models\LanguageValues;
use yii\base\Event;
use yii\i18n\MissingTranslationEvent;


/**
 * Класс для работы с языковыми значениями
 * Class Messages
 * @package skewer\build\Component\I18N
 */
class Messages
{
    protected static $CacheSEOLabels = [];

    /**
     * @param $sCategory
     * @param $sMessage
     * @param $sLanguage
     */
    public static function delete($sCategory, $sMessage, $sLanguage){

        LanguageValues::deleteAll([
            'category' => $sCategory,
            'message' =>  $sMessage,
            'language' => $sLanguage,
        ]);

    }

    /**
     * Добавляет значения
     * @param array $aValues Массив значений
     * @param string $sLang Префикс языка
     * @param string $sCategory имя категории
     * @param bool $bData - флаг того, что данные являются предустановленным контентом
     */
    public static function setValues($aValues, $sLang, $sCategory, $bData = false) {

        if(!count($aValues))
            return;

        $aRows = models\LanguageValues::find()
            ->where(['language' => $sLang, 'category' => $sCategory])
            ->all();

        $aRows = \yii\helpers\ArrayHelper::index($aRows, 'message');

        foreach ($aValues as $sName => $sValue) {

            $oRow = (isset($aRows[$sName]))?$aRows[$sName]:new models\LanguageValues();

            // есди стоит флаг ручной модификации - не перезаписываем метку
            if ($oRow->override != LanguageValues::overrideNo)
                continue;

            // Не трогаем то, что не изменено. Сокращает количество запросов
            if (!$oRow->getIsNewRecord() && $oRow->value == $sValue) {
                continue;
            }

            $oRow->setAttributes([
                'value' => $sValue,
                'language' => $sLang,
                'category' => $sCategory,
                'message' => $sName,
                'status' => LanguageValues::statusTranslated,
                'override' => LanguageValues::overrideNo,
                'data' => (int)$bData
            ]);

            if ($oRow->save()){

                /**
                 * Хак для обновления категорий несколько раз за один запуск
                 */

                Event::on(
                    get_class(\Yii::$app->getI18N()->getMessageSource($sCategory)),
                    MessageSource::EVENT_MISSING_TRANSLATION,
                    function( MissingTranslationEvent $event ) use ($oRow) {

                        if (
                            $oRow->language == $event->language &&
                            (($oRow->data)?'data/':'').$oRow->category == $event->category &&
                            $oRow->message == $event->message
                        ) {
                            $event->translatedMessage = $oRow->value;
                        }
                    }
                );
            }

        }

    }

    /**
     * Выборка по категории
     * @param $sCategory
     * @param $sLanguage
     * @param $bData
     * @return array|models\LanguageValues
     */
    public static function getByCategory($sCategory, $sLanguage, $bData = false){
        return models\LanguageValues::find()
            ->select(['message', 'value'])
            ->where(['language' => $sLanguage, 'category' => $sCategory, 'data' => $bData])
            ->asArray()
            ->all();
    }


    /**
     * @fixme избавиться от этого!
     * @deprecated
     * Отдает метки для сео шаблонов с кешированием
     * @return array
     */
    public static function getSEOLabels() {

        if ( !self::$CacheSEOLabels ){
            $aData = models\LanguageValues::find()
                ->select(['message', 'value'])
                ->where(['category' => 'SEO'])
                ->asArray()
                ->all();

            foreach( $aData as $aVal ){
                self::$CacheSEOLabels[$aVal['message']][] = $aVal['value'];
            }
        }

        return self::$CacheSEOLabels;
    }


    /**
     * @todo переписать
     *  Отдает списко записей по заданному языку
     * @param array $aFilter фильтр - возможный состав и формат:<br>
     *      name => ['like', '<val>'] <br>
     *      override => '<val>' <br>
     *      lang => '<val>'
     * @param bool $abAsArray
     * @return models\LanguageValues[]
     * @throws \Exception
     */
    public static function getFiltered( $aFilter, $abAsArray=false ) {

        $oQuery = models\LanguageValues::find();

        // фильтр по языку
        if ( isset($aFilter['language']) )
            $oQuery->where( ['language' => $aFilter['language']] );

        // фильтр по флагу перекрытия
        if ( isset($aFilter['override']) )
            $oQuery->andWhere( ['override' => $aFilter['override']] );

        // фильтр по категории
        if ( isset($aFilter['category']) )
            $oQuery->andWhere( ['category' => $aFilter['category']] );


        if (isset($aFilter['status'])){
            $oQuery->andWhere( ['status' => $aFilter['status']]);
        }
        if (isset($aFilter['data'])){
            $oQuery->andWhere( ['data' => (int)$aFilter['data']]);
        }

        // фильтр по имени параметра
        if ( isset($aFilter['like']) ) {
            $aLike = [
                'or',
                ['like', 'message', $aFilter['like']],
                ['like', 'category', $aFilter['like']],
            ];
            if (isset($aFilter['like_values']) )
                $aLike[] = ['like', 'value', $aFilter['like']];

            $oQuery->andWhere($aLike);
        }

        if ( $abAsArray )
            $oQuery->asArray();

        return $oQuery->all();

    }


    /**
     * Отдает набор значений по фильтру в виде массива
     * @param array $aFilter формат как в self::getFiltered
     * @return array
     */
    public static function getFilteredSimple( $aFilter ) {
        $aOut = array();
        foreach ( self::getFiltered($aFilter, true) as $aRow )
            $aOut[$aRow['category'] .'.'. $aRow['message']] = $aRow;
        return $aOut;
    }


    /**
     * Отдает запись или выкидывает исключение
     * @param $sCategory
     * @param $sMessage
     * @param $sLang
     * @return null| models\LanguageValues
     * @throws \Exception
     */
    public static function getOrExcept( $sCategory, $sMessage, $sLang ) {
        $oRow = models\LanguageValues::findOne(['category' => $sCategory, 'message' => $sMessage, 'language' => $sLang]);
        if ( !$oRow )
            throw new \Exception( "Языковая запись [$sCategory:$sMessage:$sLang] не найдена" ); //@todo перевод
        return $oRow;
    }

    /**
     * Получение запись сообщения по категории, имени и языку
     * @param $sCategory
     * @param $sMessage
     * @param $sLanguage
     * @return null|static
     */
    public static function getByName($sCategory, $sMessage, $sLanguage){
        return models\LanguageValues::findOne(['category' => $sCategory, 'message' => $sMessage, 'language' => $sLanguage]);
    }
}