<?php

namespace skewer\build\Component\Router;


/**
 * Прототип Vendor`a правил маршрутизации для модулей
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project Skewer
 * @package kernel
 */
interface RoutingInterface {
    /**
     * Возвращает паттерны разбора URL
     * @static
     * @return bool | array
     */
    public static function getRoutePatterns();
}
