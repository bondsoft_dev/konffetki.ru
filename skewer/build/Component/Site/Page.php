<?php

namespace skewer\build\Component\Site;

/**
 * Класс для работы с текущей открытой страницей
 * Предоставляет единый интерфейс для работы с модулями на странице
 * todo #events в идеале это все должно работать на событийной модели,
 *          но пока её нет сделаем есдиную точку доступа и прамые вызовы
 */

class Page {

    /**
     * Задает новый заголовок для страницы
     * @param string $sTitle новый заголовок
     * @return bool если был заменен, то true
     */
    public static function setTitle( $sTitle ) {

        // находим модуль вывода заголовка
        $oProcessTitle = \Yii::$app->processList->getProcess('out.title', psAll);

        // если он на странице есть
        if ( $oProcessTitle instanceof \skProcess ) {

            // задаем переменную с названием
            \Yii::$app->environment->set('title4section', $sTitle);

            // ставим статус на перезагрузку, если он уже отработал
            $oProcessTitle->setStatus( psNew );

            return true;

        }

        return false;

    }

    /**
     * Добавляем элемент в "хлебные крошки".
     * Но только один. Повторный вызов функции вызовет перекрытие предыдущего значения
     * @param string $sTitle текст элемента
     * @param string $sHref ссылка (если нужна)
     * @return bool
     */
    public static function setAddPathItem( $sTitle, $sHref='' ) {
        return self::setAddPathItemData( [
            'title' => $sTitle,
            'link' => $sHref
        ] );
    }

    /**
     * Добавляем элемент в "хлебные крошки".
     * Но только один. Повторный вызов функции вызовет перекрытие предыдущего значения
     * Принимает массив. Функция добавлена на случай, если потребуется собирать
     * сложный элемент. Для большинства случаев подойдет setAddPathItem
     * @param [] $aData данные для добавления
     * @return bool
     */
    public static function setAddPathItemData( $aData ) {

        $oProcessPathLine = \Yii::$app->processList->getProcess( 'out.pathLine', psAll );

        if ( $oProcessPathLine instanceof \skProcess ) {

            $oProcessPathLine->setStatus( psNew );

            \Yii::$app->environment->set(
                'pathline_additem',
                array(
                    'id' => isSet($aData['id']) ? $aData['id'] : 0,
                    'title' => isSet($aData['title']) ? $aData['title'] : 'title',
                    'alias_path' => isSet($aData['alias']) ? $aData['alias'] : '',
                    'link' => isSet($aData['link']) ? $aData['link'] : ''
                )
            );

            return true;

        }

        return false;

    }

    /**
     * Отдает true если главный модуль уже отработал
     * @return bool
     */
    public static function rootModuleComplete() {
        $oPage = self::getRootModule();
        return $oPage->isComplete();
    }

    /**
     * Отдает корневой модуль на страницу (обычно это Page\Main)
     * @return \skProcess
     */
    public static function getRootModule() {
        return \Yii::$app->processList->getProcess('out', psAll);
    }

    /**
     * Отдает главный модуль на страницу (каталог / новостная / ...)
     * #stab_fix rename getMainModuleProcess
     * @return \skProcess|null
     */
    public static function getMainModule() {
        $oProcess = \Yii::$app->processList->getProcess('out.content', psAll);
        return ($oProcess instanceof \skProcess) ? $oProcess : null;
    }

    /**
     * Очищает статический контент для страницы
     * Адекватно сработает только после полной отработки модуля, поэтому
     * используется вместе с методом mainModuleComplete()
     *
     * todo придумать механизм, чтобы не надо было ждать
     * из серии есть - сбросить, нет - поставить флаг, что строить не нужно
     * (может прямо в шаблоне проверять)
     */
    public static function clearStaticContent() {
        $oPage = self::getRootModule();
        $oPage->setData('staticContent', array());
    }

    /**
     * Очищает статический контент №2 для страницы
     * Адекватно сработает только после полной отработки модуля, поэтому
     * используется вместе с методом mainModuleComplete()
     */
    public static function clearStaticContent2() {
        $oPage = self::getRootModule();
        $oPage->setData('staticContent2', array());
    }

    /**
     * Заставляет модуль SEO перезагрузить содержание
     * Вызовом этого метода помечены все места перестроения SEO
     * todo для мест вызва можно подумать над специализированной функциональностью
     *      для перестроения меток
     * @return bool
     */
    public static function reloadSEO() {
        /** @noinspection PhpUndefinedMethodInspection */
        $page = \Yii::$app->processList->getProcess('out.SEOMetatags', psAll);
        if($page instanceof \skProcess) $page->setStatus(psNew);
    }

}