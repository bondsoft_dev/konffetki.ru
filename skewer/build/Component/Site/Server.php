<?php

namespace skewer\build\Component\Site;

/**
 * Класс для работы с серверными данными
 */
class Server {

    /**
     * Отдает true если сервер, на котором работает данный сайт - nginx
     * Пока это только заготовка метода
     * @return bool
     */
    public static function isNginx() {
        if ( !isset($_SERVER['SERVER_SOFTWARE']) )
            return false;
        return (strpos($_SERVER['SERVER_SOFTWARE'],'nginx')!==false);
    }

    /**
     * Отдает true если сервер, на котором работает данный сайт - apache
     * Пока это только заготовка метода
     * @return bool
     */
    public static function isApache() {
        return !self::isNginx();
    }

}