<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 02.07.2014
 * Time: 14:12
 */

namespace skewer\build\Component\Site;

class Type {

    /** тип сайта "Информационный" */
    const info = 'info';

    /** тип сайта "Каталог" */
    const catalog = 'catalog';

    /** тип сайта "Интернет магазин" */
    const shop = 'shop';

    /**
     * Отдает тип версии сайта
     * @throws \Exception
     * @return string
     */
    public static function getAlias() {

        $sType = \SysVar::get( 'syte_type' );

        if ( !$sType )
            throw new \Exception( 'Site type is not setted' );

        return $sType;

    }

    /**
     * Сообщает является ли сайт "Информационным"
     * @return bool
     */
    public static function isInfo() {
        return self::getAlias() === self::info;
    }


    public static function hasCatalogModule(){
        return !self::isInfo();
    }

    /**
     * Сообщает является ли сайт "Каталог"
     * @return bool
     */
    public static function isCatalog() {
        return self::getAlias() === self::catalog;
    }

    /**
     * Сообщает является ли сайт "Интернет магазин"
     * @return bool
     */
    public static function isShop() {
        return self::getAlias() === self::shop;
    }

} 