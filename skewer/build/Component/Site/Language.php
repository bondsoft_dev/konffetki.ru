<?php

/**
 * @todo переместить?
 */

$aLang = [];

$aLang['ru']['main'] = 'Главная';
$aLang['ru']['root'] = 'Раздел настроек';
$aLang['ru']['404'] = '404';
$aLang['ru']['topMenu'] = 'Верхнее меню';
$aLang['ru']['leftMenu'] = 'Левое меню';
$aLang['ru']['tools'] = 'Служебные разделы';
$aLang['ru']['serviceMenu'] = 'Сервисное меню';
$aLang['ru']['auth'] = 'Авторизация';
$aLang['ru']['lang_root'] = 'Главный раздел языка';

$aLang['ru']['landingPageTpl'] = 'Шаблон Landing Page';
$aLang['ru']['root'] = 'Id корневого раздела для админки';
$aLang['ru']['templates'] = 'Шаблоны';
$aLang['ru']['library'] = 'Библиотеки';
$aLang['ru']['tplNew'] = 'Шаблон нового раздела';

$aLang['ru']['profile'] = 'Личный кабинет';
$aLang['ru']['cart'] = 'Корзина';
$aLang['ru']['search'] = 'Поиск';
$aLang['ru']['sitemap'] = 'Карта сайта';
$aLang['ru']['subscribe'] = 'Рассылка';
$aLang['ru']['payment_success'] = 'Успешная оплата';
$aLang['ru']['payment_fail'] = 'Отказ от платежа';
$aLang['ru']['orderForm'] = 'Форма заказа';
$aLang['ru']['subscribe'] = 'Рассылка';

$aLang['ru']['site_label'] = 'название сайта';
$aLang['ru']['site_label_description'] = 'название сайта';
$aLang['ru']['url_label'] = 'адрес сайта';
$aLang['ru']['url_label_description'] = 'URL адрес сайта';

/******************/

$aLang['en']['main'] = 'Main';
$aLang['en']['root'] = 'Settings';
$aLang['en']['404'] = '404';
$aLang['en']['topMenu'] = 'Top menu';
$aLang['en']['leftMenu'] = 'Left menu';
$aLang['en']['tools'] = 'Tools';
$aLang['en']['serviceMenu'] = 'Service menu';
$aLang['en']['auth'] = 'Auth';
$aLang['en']['lang_root'] = 'Language root';
$aLang['en']['subscribe'] = 'Subscribe';

$aLang['en']['landingPageTpl'] = 'Template Landing Page';
$aLang['en']['root'] = 'Id root partition to the admin';
$aLang['en']['templates'] = 'templates';
$aLang['en']['library'] = 'library';
$aLang['en']['tplNew'] = 'Template the new partition';

$aLang['en']['profile'] = 'My account';
$aLang['en']['cart'] = 'Cart';
$aLang['en']['search'] = 'Search';
$aLang['en']['sitemap'] = 'Site Map';
$aLang['en']['subscribe'] = 'Subscribe';
$aLang['en']['payment_success'] = 'Successful Payment';
$aLang['en']['payment_fail'] = 'Waiver of payment';
$aLang['en']['orderForm'] = 'Order Form';
$aLang['en']['subscribe'] = 'Subscribe';

$aLang['en']['site_label'] = 'site name';
$aLang['en']['site_label_description'] = 'site name';
$aLang['en']['url_label'] = 'url';
$aLang['en']['url_label_description'] = 'url';

return $aLang;