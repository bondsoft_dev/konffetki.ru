<?php

namespace skewer\build\Component\Search;


use skewer\build\Component\orm\Query;
use skewer\build\Component\Section\Tree;

class Selector{

    /**
     * Имя модуля для каталожного поиска
     */
    const catalogClassName = 'CatalogViewer';

    /**
     * Текст для поиска
     * @var string
     */
    private $sSearchText = '';

    /**
     * Страница выборки
     * @var int
     */
    private $iPage = 0;

    /**
     * Размер выборки
     * @var int
     */
    private $iLimit = 0;

    /**
     * Тип поиска
     * @var int
     */
    private $iSearchType = 0;

    /**
     * Тип области для поиска
     * @var int
     */
    private $iType = 0;

    /**
     * Раздел для поиска
     * @var int
     */
    private $iSection = 0;

    /**
     * Поиск и в подразделах
     * @var bool
     */
    private $bInSubsection = false;

    /**
     * Исключенные разделы
     * @var array
     */
    private $aDenySection = array();

    private function __construct(){}

    private function __clone(){}

    /**
     * Создание экземпляра класса для выборки
     * @return static
     */
    public static function create(){
        return new static();
    }

    /**
     * Задаем текст для поиска
     * @param $sSearchText
     * @return $this
     */
    public function searchText( $sSearchText ){

        $this->sSearchText = $sSearchText;

        return $this;
    }

    /**
     * Задание пределов выборки
     * @param $iLimit
     * @param $iPage
     * @return $this
     */
    public function limit( $iLimit, $iPage ){

        $this->iPage = $iPage;
        $this->iLimit = $iLimit;

        return $this;
    }

    /**
     * Тип поиска
     * @param $iSearchType
     * @return $this
     */
    public function searchType( $iSearchType ){

        $this->iSearchType = $iSearchType;

        return $this;
    }

    /**
     * Тип области для поиска
     * @param $iType
     * @return $this
     */
    public function type( $iType ){

        $this->iType = $iType;

        return $this;
    }

    /**
     * Раздел для поиска
     * @param $iSection
     * @return $this
     */
    public function section( $iSection ){

        $this->iSection = $iSection;

        return $this;
    }

    /**
     * Исключенные разделы
     * @param $aSection
     * @return $this
     */
    public function denySection( $aSection ){

        $this->aDenySection = $aSection;

        return $this;
    }

    /**
     * Поиск и в подразделах
     * @param $bSubsections
     * @return $this
     */
    public function subsections( $bSubsections ){

        $this->bInSubsection = (bool) $bSubsections;

        return $this;
    }

    /**
     * Изменяем поисковую строку с использованием словоформ
     * @param $sSearchText
     * @param $iSearchType
     * @return string
     */
    private static function rebuildSearchString( $sSearchText, $iSearchType ){

        $iSearchType = Type::getValid( $iSearchType );

        if ( Type::useStemmer( $iSearchType ) )
            $sSearchText = Stemmer::rebuildString( $sSearchText );

        return $sSearchText;
    }

    /**
     * Выполнение поиска
     * @throws \Exception
     * @return array
     */
    public function find(){

        $sSearchText = static::rebuildSearchString( $this->sSearchText, $this->iSearchType );

        $aData = array(
            'start' => ( $this -> iPage - 1) * $this->iLimit,
            'record_count' => (int) $this->iLimit
        );

        /** Заменим - на _ */
        $sSearchText = preg_replace( '/([\pL\d_+])-([\pL\d_+])/ui', '$1_$2', $sSearchText );

        switch( $this->iSearchType ){

            case Type::anyWord:
            case Type::allWords:

                $aTextParts = explode(' ', $sSearchText);
                $aTextParts = array_diff($aTextParts, array(''));

                foreach( $aTextParts as &$sPart ){

                    $sPart = trim($sPart);

                    if ( $this->iSearchType == Type::anyWord )
                        $sPart = $sPart.'*';
                    if ( $this->iSearchType == Type::allWords )
                        $sPart = '+'.$sPart.'*';
                }

                $aData['search_text'] = implode(' ', $aTextParts);
                break;

            case Type::exact:

                $sSearchText = preg_replace( '/([^\pL\d_]|^)([\pL\d_]{2,3})([^\pL\d_]|$)/ui', '$1$2__$3', $sSearchText );
                $aData['search_text'] = '"'.$sSearchText.'"';

                break;

            default:
                throw new \Exception( "Unknown search type [$this->iSearchType]" );

        }

        $sClassNameCondition = '';
        $sOrder = '';
        switch ($this->iType){
            case Type::inCatalog:
                $sClassNameCondition = "`class_name` = '" . self::catalogClassName . "' AND ";
                $sOrder = 'MATCH (`search_title`) AGAINST (:search_text IN BOOLEAN MODE) DESC,';
                break;
            case Type::inInfo:
                $sClassNameCondition = "`class_name` != '" . self::catalogClassName . "' AND ";
                break;
        }

        $searchSection = 0;
        if ( $this->iSection ){

            $searchSection = $this->iSection;

            if ( $this->bInSubsection ){
                $searchSection = $this->getSubSections( $this->iSection );
                $searchSection[] = $this->iSection;
            }

            if (!is_array($searchSection)){
                $searchSection = array( (int)$searchSection );
            }else{
                $searchSection = array_map( create_function( '$a', 'return (int)$a;'), $searchSection);
            }
        }

        $aData['language'] = \Yii::$app->language;

        $sQuery = "
            SELECT
            SQL_CALC_FOUND_ROWS
                `search_title`,
                `search_text`,
                `text`,
                `href`,
                `class_name`,
                `section_id`,
                `object_id`,
                MATCH (`search_title`, `search_text`) AGAINST (:search_text IN BOOLEAN MODE) AS `rel`
            FROM `search_index`
            WHERE `status`=1 AND `use_in_search`=1 AND 
            `language` = :language AND
            ".
            $sClassNameCondition .
            ($searchSection ? ' `section_id` IN (' . implode( ',', $searchSection ) . ') AND ' : '').
            (count($this->aDenySection) ? '`section_id` NOT IN ('.implode(',',$this->aDenySection).') AND ' : '').
            "MATCH (`search_title`, `search_text`) AGAINST (:search_text IN BOOLEAN MODE) > 0
        ORDER BY
        " . $sOrder . "
            `rel` DESC
            LIMIT :start, :record_count;";

        $rResult = Query::SQL( $sQuery, $aData );

        $aItems = array();
        while( $aRow = $rResult->fetchArray() ){
            $aItems['items'][] = $aRow;
        }

        $oQuery = Query::SQL("SELECT FOUND_ROWS() as rows;");

        $aItems['count'] = ( $iCount = $oQuery->getValue( 'rows' ) ) ? $iCount : 0;

        return $aItems;

    }

    /**
     * Возвращает список всех подразделов (включая сам раздел)
     * @param $parent
     * @return array
     */
    private function getSubSections( $parent ){

        if (!is_array($parent)){
            $parent = array($parent);
        }

        $aResult = array();

        do{

            $aItems = Tree::getSubSections( $parent, true, true );
            $aResult = array_merge( $aResult, $aItems );
            $parent = $aItems;

        } while( $aItems );

        return $aResult;
    }
}