<?php

namespace skewer\build\Component\Search;

use skewer\build\Component\orm;

/**
 * AR класс для работы с записями поискового индекса
 */
class Row extends orm\ActiveRecord {

    function __construct() {
        $this->setTableName( 'search_index' );
        $this->setPrimaryKey( 'id' );
    }

    /** @var int id записи */
    public $id = 0;

    /** @var string заголовок */
    public $search_text = '';

    /** @var string Текст */
    public $text = '';

    /** @var string текст для поиска */
    public $search_title = '';

    /**
     * @var int статус
     * 0 - новая запись (стоит в очереди на обработку)
     * 1 - обработана
     */
    public $status = 0;

    /** @var bool флаг использования записи при поиске */
    public $use_in_search = false;

    /** @var string ссылка */
    public $href = '';

    /** @var string имя класса */
    public $class_name = '';

    /** @var int id объекта для класса */
    public $object_id = 0;

    /** @var string Язык */
    public $language = '';

    /** @var int id раздела для вывода */
    public $section_id = 0;

    /** @var string дата модификации */
    public $modify_date = '';

    public function save() {
        $this->modify_date = date('Y-m-d H:i:s');

        $this->text = $this->search_text;

        /** Заменим - на _ */
        $this->search_text = preg_replace( '/([\pL\d_+])-([\pL\d_+])/ui', '$1_$2', $this->text );

        /**
         * все 2-3 буквенные слова дополняем __
         * С кириллицей выходит только так.
         */
        $this->search_text = preg_replace( '/([^\pL\d_]|^)([\pL\d_]{2,3})([^\pL\d_]|$)/ui', '$1$2__$3', $this->search_text );

        return parent::save();
    }

    /**
     * @inheritDoc
     */
    public function delete() {
        $res = parent::delete();
        if ( $res )
            $this->id = 0;
        return $res;
    }

} 