<?php

namespace skewer\build\Component\Search;

use skewer\build\Component\orm;
use skewer\build\libs\ft;
use skewer\build\Component\Site;
use skewer\build\Component\Catalog;


/**
 * API для работы с поисковым индексом
 * method static Api find
 *
 * @method Row getOne
 */
class Api{

    /** событие по сбору активных поисковых движков */
    const EVENT_GET_ENGINE = 'event_get_engine';

    /** @var null|Prototype[] список поисковых движков */
    private static $aList = null;

    /**
     * Отдает запись поискового индекса по классу и id
     * @param string $sClassName имя класса
     * @param int $iObjectId id объекта
     * @return Row|null
     */
    public static function get( $sClassName, $iObjectId ) {

        $oRow = SearchIndex::find()
            ->where( 'class_name', $sClassName )
            ->where( 'object_id', $iObjectId )
            ->getOne()
        ;

        return $oRow ? $oRow : null;

    }

    /**
     * Отдает одну запись по ссылке, если найдет
     * @param $sHref
     * @return Row|null
     */
    public static function getByHref( $sHref ) {

        $oRow = SearchIndex::find()
            ->where( 'href', $sHref )
            ->getOne()
        ;

        return $oRow ? $oRow : null;

    }

    /**
     * Удаление всех записей для раздела
     * @static
     * @param int $iSectionId Идентификатор раздела
     * @return int количество удаленных записей
     */
    public static function removeFromIndexBySection( $iSectionId ){

        return SearchIndex::delete()
            ->where('section_id', $iSectionId)
            ->get();

    }

    /**
     * отдает набор пар 'имя идентификатора' => 'класс с namespace', участвующих в индексе в порядке приоритетов
     * @return \Closure[] array
     */
    public static function getResourceList(){

        if ( is_null(self::$aList) ) {
            $oEvent = new GetEngineEvent();
            \Yii::$app->trigger(self::EVENT_GET_ENGINE, $oEvent);
            self::$aList = $oEvent->getList();
        }

        return self::$aList;

    }

    /**
     * Поиск Search модуля по названию класса
     * @param string $sName псевдоним движка
     * @return Prototype|null объект поискового движка или null
     */
    public static function getSearch($sName){

        $list =  self::getResourceList();

        if (isset($list[$sName])){
            /** @var Prototype $s */
            $s = new $list[$sName]();
            $s->provideName( $sName );
            return $s;
        }

        return false;
    }

}