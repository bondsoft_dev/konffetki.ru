<?php

namespace skewer\build\Component\Search;

use skewer\build\Component\SEO\Service;
use skewer\build\Component\SEO;
use yii\helpers\HtmlPurifier;

/**
 * Класс прототип для поисковых механизмов контентных сущностей
 */
abstract class Prototype {

    /**
     * @var string псевдоним поискового движка,
     *      который передается основным обработчиком при итерации
     *      поисковых записей.
     *      В большинстве случаев совпадает с ответом метода getName()
     */
    protected $sIncomingName = '';

    /**
     * отдает имя идентификатора ресурса для работы с поисковым индексом
     * @return string
     */
    abstract public function getName();

    /**
     * Отдает название модуля
     * @return string
     */
    public function getModuleTitle() {
        if ( !\Yii::$app->register->moduleExists($this->getName(), \Layer::PAGE) )
            return '-';
        return \Yii::$app->register->getModuleConfig($this->getName(), \Layer::PAGE)->getTitle();
    }

    /**
     * обновляет конкретную запись поисковой таблицы
     * * устанавливает status в 1
     * * обновляет поисковый текст
     * * обновляет заголовок
     * * обновляет url
     * * задает раздел
     * * выставляет остальные параметры на свое усмотрение
     * * сохраняет запись. если вернет false, то посиковая запись будет стерта из базы
     * @param $oSearchRow
     * @return boolean если возвращает false - запись будет удалена вышестоящим методом
     */
    abstract protected function update(Row $oSearchRow);

    /**
     *  воссоздает полный список пустых записей для сущности, отдает количество добавленных
     * @return int
     */
    abstract public function restore();

    /**
     * сбрасывает статус для всех записей с идентификатором модуля, отдает количество измененных
     * @return int
     */
    public function resetAll(){
        return SearchIndex::update()->set('status',0)->where('class_name',$this->getName())->where('status',1)->getAffectedRows();
    }

    /**
     * сбрасывает по id объекта в 0
     * @param int $id
     * @return int
     */
    public function resetToId($id){
        return SearchIndex::update()->set('status',0)->where('class_name',$this->getName())->where('object_id',$id)->get();
    }

    /**
     * удаляет все записи из поискового индекса, отдает количество измененных
     * @return int
     */
    public function deleteAll(){
        $res = SearchIndex::delete()->where('class_name',$this->getName())->getAffectedRows();
        Service::updateSiteMap();
        return $res;
    }

    /**
     * отдает число неактивных записей
     * @return int
     */
    public function getInactiveCount(){
        return SearchIndex::find()->where('class_name',$this->getName())->where('status',0)->getCount();
    }

    /**
     * обновляет/добавляет запись в поисковой таблице по id конкретной записи ресурса
     * выбирает / создает если нет поисковую запись по id и идентификатору ресурса
     * @param $iId
     * @param bool $bAddSitemapTask нужно ли ставить задачу на обновление sitemap?
     * @return bool
     */
    public function updateByObjectId($iId, $bAddSitemapTask = true){

        // найти запись
        $oSearchRow = SearchIndex::find()
            ->where('class_name',$this->getName())
            ->where('object_id',$iId)
            ->getOne();

        // ... или создать новую
        if (!$oSearchRow){
            $oSearchRow = SearchIndex::getNewRow();
            $oSearchRow->class_name = $this->getName();
            $oSearchRow->object_id = (int)$iId;
            $oSearchRow->save();
        }

        // выполнить конкретное действие по обновлению
        $res = $this->update($oSearchRow);

        // если метод вернул false, а запись есть в базе (задан id) - убрать из использования
        if ( $res === false and $oSearchRow->id ) {
            $oSearchRow->use_in_search = false; // не использовать при поиске
            $oSearchRow->status = 1;            // обработана
            $oSearchRow->save();
        }

        // если стоит флаг - поставить задачу на обновление sitemap
        if ($bAddSitemapTask)
            SEO\Api::setUpdateSitemapFlag();

        return $res;

    }

    /**
     * удаляет запись по id и идентификатору ресурса
     * @param $iId
     * @return bool
     */
    public function deleteByObjectId($iId){
        $res = (boolean) SearchIndex::delete()->where('class_name',$this->getName())->where('object_id',$iId)->get();
        Service::updateSiteMap();
        return $res;
    }

    /**
     * Returns the fully qualified name of this class.
     * @return string the fully qualified name of this class.
     */
    public static function className() {
        return get_called_class();
    }

    /**
     * Задает имя под которым поисковая записб зарегистрирована в таблице
     * @param string $sName
     */
    public function provideName( $sName ) {
        $this->sIncomingName = $sName;
    }

    /**
     * Расширенная функция удаления тегов
     * Умеет стирать также теги типа style, script
     * @param $sText
     * @return string
     */
    protected function stripTags($sText) {
        return strip_tags( HtmlPurifier::process( $sText ) );
    }

}