<?php

namespace skewer\build\Component\Search;

/**
 * Класс для поддержки словоформ в поиске
 */
class Stemmer {

    /**
     * Пересобирает строку поискового запроса для работы использования в поисковом запросе
     * @param string $sSearchText исходная строка для поиска
     * @return string
     */
    public static function rebuildString($sSearchText) {

        require_once COREPATH.'libs/Stemmer/Lingua_Stem_Ru.php';

        $oStemmer = new \Lingua_Stem_Ru();

        $aSearch = explode(' ', $sSearchText);
        $aSearch = array_diff($aSearch, array(''));

        foreach($aSearch as &$sSearch){

            $sSearch = $oStemmer->stem_word($sSearch);
        }

        return implode(' ', $aSearch);

    }
}