<?php

namespace skewer\build\Component\SiteTester;


class Api {

	/**
	 * Режимы работы
	 */
	const MODE_PROD = 'prod';
	const MODE_DEV  = 'dev';

	/**
	 * Статусы
	 */
	const MESSAGE_TYPE_WARNING = 'warning';
	const MESSAGE_TYPE_ERROR = 'error';
	const MESSAGE_TYPE_INFO = 'info';

	const SESSION = 'sitetester';

	/**
	 * @return string
	 * @throws \GatewayException
	 */

	public static function getSiteMode() {
		return (\SysVar::isProductionServer()) ? self::MODE_PROD : self::MODE_DEV;
	}

	/**
	 * @return array
	 */
	function getProdTestList() {

		return array(
			Tests\Robots::getName(),
			Tests\DisplayErrors::getName(),
		//	Tests\Chmod::getName(),
			Tests\Chown::getName(),
		);
	}

	/**
	 * @return array
	 */
	function getDevTestList() {

		return array(
			Tests\Robots::getName(),
			Tests\DisplayErrors::getName(),
		//	Tests\Chmod::getName(),
			Tests\Chown::getName(),
		);
	}

	public static function getStatus($name) {
		return  (isset($_SESSION[self::SESSION][$name])) ? $_SESSION[self::SESSION][$name]['status'] : 'undefined';
	}

	public static function getInfo($name) {
		return  (isset($_SESSION[self::SESSION][$name])) ? $_SESSION[self::SESSION][$name] : false;
	}


}