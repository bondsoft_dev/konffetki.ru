<?php

namespace skewer\build\Component\SiteTester;

/**
 * Ошибка в процессе выполнения простого линейного набора действий
 */
class TesterException extends \Exception {}
