<?php

namespace skewer\build\Component\SiteTester\Tests;

use skewer\build\Component\SiteTester\TestPrototype;
use skewer\build\Component\SiteTester\Api;

class DisplayErrors extends TestPrototype {

    public static $name = 'Checking PHP error output';
    
    var $indexFiles = array(

        'web/index.php'

    );

    const regEx = '(ini_set\(\'display_errors\',\s*\S{1,10})';

    const ON = 1;
    const OFF = 0;

    var $labels = array(
        0 => 'enabled',
        1 => 'disabled'
    );


    function execute() {


        $status = (Api::getSiteMode() == 'prod') ? static::OFF : static::ON;
        $this->setStatusOk();

        foreach($this->indexFiles as $file) {
            $match = [];
            $find = preg_match(static::regEx, file_get_contents(ROOTPATH.$file), $match);
            if ($find) {
                foreach ($match as $row) {
                    if ($this->getData($row) != $status) $this->setStatusWarning('['.$file.'] Errors are '. $this->labels[$status]);
                    else $this->addMessageInfo('['.$file.'] Errors are '. $this->labels[!$status]);
                }
            }
        }
    }

    function getData($str) {

        $str = substr($str, 0, strlen($str)-2);
        $var = explode(',', $str);
        $flag = trim($var[1]);

        if ($flag == 'false') return 0;
        elseif ($flag == 'true') return 1;
        else return (int)$flag;


    }




}