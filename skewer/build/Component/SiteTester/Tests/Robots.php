<?php

namespace skewer\build\Component\SiteTester\Tests;

use skewer\build\Component\SiteTester\TestPrototype;
use skewer\build\Component\SiteTester\Api;

class Robots extends TestPrototype {

	public static $name = 'Checking robots.txt file';

	var $disallowRules = array(
		'dev' => array(
			"/"
		),
		'prod' => array(
			'/search/',
			'/admin/',
			'*/?objectId',
			'/files/rss/feed.rss',
			'*/files/rss/feed.rss/'
		)
	);

	function execute() {

		$mode = Api::getSiteMode();

		if (file_exists(ROOTPATH.'web/robots.txt'))  {

			$handle = fopen(ROOTPATH.'web/robots.txt', "r");
			if ($handle) {
				while (($row = fgets($handle, 4096)) !== false) {
					$rule = $this->getRule($row);
					if ($rule) {
						if (in_array($rule, $this->disallowRules[$mode])) $this->setStatusOk('[OK] Disallow: '.$rule);
						else $this->setStatusError('[ERROR] Disallow: '.$rule);
					}
				}
			} else $this->setStatusFail('robots.txt file does not exist');
		} else $this->setStatusFail('robots.txt file does not exist');


	}



	private function getRule($str) {

		$row = explode(':', $str);
		return ($row[0] == 'Disallow') ? trim($row[1]) : false;

	}

}