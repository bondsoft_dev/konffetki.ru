<?php

namespace skewer\build\Component\ReachGoal;

use skewer\build\Component\orm;
use skewer\build\libs\ft;

/**
 * Запросник для списка языков
 * @method static bool|TargetRow|orm\state\StateSelect find($id = null)
 */
class Target extends orm\TablePrototype {

    /** @var string Имя таблицы */
    protected static $sTableName = 'reach_goal_target';

    /** имя поля "идентификатор" */
    const id = 'id';

    /** имя поля "системное имя" */
    const name = 'name';

    /** имя поля "название" */
    const title = 'title';

    protected static function initModel() {

        ft\Entity::get( static::$sTableName )
            ->clear()
            ->setTablePrefix('')
            ->setNamespace( __NAMESPACE__ )
            ->addField( self::name, 'varchar(30)', 'ReachGoal.field_name' )
                ->addIndex( ft\Index::unique )
            ->addField( self::title, 'varchar(64)', 'ReachGoal.field_title' )
            ->addDefaultProcessorSet()
            ->addColumnSet( 'list', array( self::name, self::title) )
            ->save()
        ;

    }

    /**
     * Отдает новую запись языка
     * @param array $aData
     * @return TargetRow
     */
    public static function getNewRow( $aData = array() ) {
        return new TargetRow( $aData );
    }

    /*************************************************************************/

    /**
     * Отдает полный список записей
     * @return TargetRow[]
     */
    public static function getAll() {
        return self::find()
            ->order( self::id )
            ->getAll()
        ;
    }

    /**
     * Отдает запись по имени языка
     * @param string $sName
     * @return TargetRow|bool
     */
    public static function getByName( $sName ) {
        return self::find()
            ->where( self::name, $sName )
            ->getOne()
        ;
    }

    /**
     * Отдает список всех языков в виде одноуровнего массива
     * ключ - name, значение - название
     * @param bool $bAddEnpty флаг добавления пустой записи
     * @return array
     */
    public static function getAllAsSimpleArray( $bAddEnpty = false ) {
        $aOut = array();
        if ( $bAddEnpty )
            $aOut[''] = '---';
        foreach ( self::getAll() as $oRow )
            $aOut[ $oRow->name ] = sprintf( '(%s) %s', $oRow->name, $oRow->title);
        return $aOut;
    }

}