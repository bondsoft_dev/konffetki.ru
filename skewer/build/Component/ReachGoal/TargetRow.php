<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 07.04.14
 * Time: 12:19
 */

namespace skewer\build\Component\ReachGoal;

use skewer\build\Component\orm\ActiveRecord;

class TargetRow extends ActiveRecord {

    /** @var int идентификатор */
    public $id = 0;

    /** @var string имя */
    public $name = '';

    /** @var string название */
    public $title = '';

    public function getTableName() {
        return 'reach_goal_target';
    }

}