<?php

namespace skewer\build\Component\Redirect;


use skewer\build\Tool\Redirect301\Mapper;


/**
 * Api для работы с перенаправлениями на сайте (301 Redirect)
 */
class Api {

    /**
     * Отдает имя файла с конфигурацией редиректов
     * @return string
     */
    public static function getFileName() {
        return ROOTPATH.'/config/redirect.php';
    }

    /**
     * Перестраивает файл 301 редиректов
     * Либо стирает его, если данных для перенаправления нет
     */
    public static function rebuildRedirectFile() {

        // путь до файла с редиректами
        $sConfigPath = self::getFileName();

        $aItems = Mapper::getItems();

        // если есть данные
        if ( $aItems['count'] ) {

            // собрать контент
            $template = __DIR__.'/template/redirect.php';
            $aData = [
                'aItems' => $aItems['items']
            ];

            $sContent = \Yii::$app->getView()->renderFile($template,$aData);

            // записать в файл
            $handle = fopen($sConfigPath, 'w+');
            fwrite($handle, $sContent);
            fclose($handle);

        } else {

            // нет данных - стереть файл, если он есть
            if ( is_file($sConfigPath) )
                unlink($sConfigPath);

        }

    }

    /**
     * Функция проверки необходимости выполнения редиректа
     */
    public static function checkRedirect() {

        $sConfigPath = self::getFileName();

        if ( !file_exists($sConfigPath) )
            return;

        /** @noinspection PhpIncludeInspection */
        $redirect = require $sConfigPath;

        if (isset($redirect[$_SERVER['REQUEST_URI']])){
            header("HTTP/1.1 301 Moved Permanently");
            header("Location: ".$redirect[$_SERVER['REQUEST_URI']]);
            exit;
        }

    }

}