<?php

namespace skewer\build\Component\Gallery;

/**
 * @todo Api для работы с изображениями галереи
 * Class Photo
 * @package skewer\build\Component\Gallery
 */
class Photo {

    /**
     * Имя директории для загрузки изображенийв разделе
     * @var string
     */
    public static $sModuleDirName = 'Gallery';

    /**
     * Название директории для хранения thumbnails
     * @var string
     */
    protected static $sThumbnailDirectory = 'thumbnails';

    /**
     * Ширина thumbnails для списка изображений в альбоме
     * @var int
     */
    protected static $iThumbnailWidth  = 0;

    /**
     * Высота thumbnails для списка изображений в альбоме
     * @var int
     */
    protected static $iThumbnailHeight = 150;

    /**
     * Возвращает список изображений для альбома/альбомов $mAlbumId. Если данные не найдены вернется false;
     * @static
     * @param int|array $mAlbumId Альбом или массив альбомов, которые в случае с $bWithoutHidden = true будут проверяться на видимость
     * @param bool $bWithoutHidden Указатель на необходимость выборки без учёта видимости
     * @param int $count Число выбираемых изображений. null - все
     * @param bool $bAlbumVisible Только видимые альбомы. Опция работает только с установленным параметром $bWithoutHidden
     * @return bool|models\Photos []
     */
    public static function getFromAlbum($mAlbumId, $bWithoutHidden = false, $count = null, $bAlbumVisible = true) {

        if ( !$mAlbumId OR (!is_array($mAlbumId) AND (!$mAlbumId = (int)$mAlbumId)) ) return false;

        if ($bWithoutHidden AND $bAlbumVisible) { // Оставить только видимые альбомы
            $mAlbumId = models\Albums::find()->select('id')->where(['id' => $mAlbumId, 'visible' => 1])->asArray()->all();
            if ($mAlbumId)
                $mAlbumId = \yii\helpers\ArrayHelper::getColumn($mAlbumId, 'id');
            else
                return false;
        }

        $query = models\Photos::find();
        $query->where(['album_id' => $mAlbumId]);
        if ($bWithoutHidden) $query->andWhere(['visible' => 1]);

        /** @var models\Photos [] $aItems */
        if (!$aItems = $query->orderBy('priority DESC')->limit($count)->all())
            return false;

        foreach ($aItems as &$aItem) // todo Можно поместить в метод AR afterFind, но где гарантия что при выборке всегда бужут использоваться объекты, а не ->asArray() ?
            $aItem['images_data'] = $aItem->getPictures();

        return $aItems;
    }

    /**
     * Возвращает данные записи изображения $iImageId
     * @static
     * @param int $iImageId Id изображения
     * @return bool|models\Photos Возвращает массив данных изображения либо false в случае отсутствия записи.
     */
    public static function getImage($iImageId) {
        /** @var models\Photos $Image */
        if ( (!$iImageId = (int)$iImageId) OR
             (!$Image = models\Photos::findOne($iImageId))) return false;
        return $Image;
    }

    /**
     * Добавляет либо обновляет данные изображения в зависимости от набора параметров
     * @static
     * @param array $aData Данные
     * @param int $iImageId Id Обновляемого изображения
     * @throws \Exception Сообщение об ошибки валидации полей
     * @return bool|int|\Exception id созданной записи или \Exception / false
     */
    public static function setImage(array $aData, $iImageId = 0) {

        if ($iImageId) { // Изменение изображения с валидацией полей
            if (!$oPhoto = models\Photos::findOne($iImageId)) throw new \Exception(\Yii::t('gallery', 'general_field_empty'));
        } else {// Вставка нового изображения
            $oPhoto = new models\Photos();
            if (isset($aData['album_id']) AND $aData['album_id'])
                $aData['priority'] = models\Photos::find()
                        ->where(['album_id' => $aData['album_id']])
                        ->max('priority') +1;
        }

        $oPhoto->setAttributes($aData);
        if ($oPhoto->save(true)) return $oPhoto->id;

        if ($oPhoto->hasErrors()) { // Если возникла ошибка валидации, то выбросить исключение
            $sFirstError = \yii\helpers\ArrayHelper::getColumn($oPhoto->errors, '0', false)[0];
            throw new \Exception($sFirstError);
        }

        return false;
    }

    /**
     * Возвращает количество видимых Изображений в альбоме $iAlbumId
     * @static
     * @param int $iAlbumId Id Альбома
     * @param bool $onlyVisible Только видимые?
     * @return int
     */
    public static function getCountByAlbum($iAlbumId, $onlyVisible = true) {
        return models\Photos::find()
            ->where(['album_id' => $iAlbumId] + (($onlyVisible) ? ['visible' => 1] : []) )
            ->count('id');
    }

    /**
     * Удаляет изображение $iImageId из альбома
     * @static
     * @param int $iImageId Id удаляемого изображения
     * @param bool|string $mError Переменная, возвращающая сообщение об ошибке случае неудачи
     * @return bool Возвращает true в случае удачного удаление записи и файлов либо false и сообщение об
     * ошибке в параметре $sError.
     * @throws \Exception
     */
    public static function removeImage($iImageId, &$mError = '') {

        try {

            if (!$iImageId = (int)$iImageId) throw new \Exception(\Yii::t('gallery', 'photos_error_notfound'));

            /* Запросить запись */
            /** @var models\Photos $Image */
            if (!$Image = models\Photos::findOne($iImageId))
                throw new \Exception(\Yii::t('gallery', 'photos_error_notfound'));

            /* Удалить исходник */
            \skFiles::remove(WEBPATH.$Image['source']);

            /* Удалить thumbnail */
            \skFiles::remove(WEBPATH.$Image['thumbnail']);

            /* Удалить ресайзы */
            foreach ($Image->getPictures() as $pic)
                \skFiles::remove(WEBPATH.$pic['file']);

            /** @var models\Photos $oPhoto */
            $oPhoto = models\Photos::findOne($iImageId);
            if ( $oPhoto )
                $oPhoto->delete();

            /* Пересчитать зависимости */

        } catch (\Exception $e) {
            $mError = $e->getMessage();
            return false;
        }

        return true;
    }

    /**
     * Изменение видимости изображения
     * @param $iImageId
     * @return bool
     */
    public static function toggleActivePhoto($iImageId){

        $oPhoto = models\Photos::findOne($iImageId);
        if ( !$oPhoto )
            return false;

        $oPhoto->visible = (int)!$oPhoto->visible;

        return $oPhoto->save();

    }

    /**
     * Обрабатывает изображение $sImagePath согласно профилю настроек $iProfileId для раздела $iSectionId.
     * @static
     * @param string $sImageFile Абсолютный путь к исходному файлу изображения
     * @param integer|array $mProfileId Id профиля настроек, согласно которому будет обработано изображение
     * @param integer $iSectionId Id раздела, в который будут сохранены файлы созданных изображений
     * @param bool $bProtected Указатель на необходимость сохранять файлы в закрытую для доступа директорию
     * @param bool $bCreateAllFormat Флаг определяет создавать ли миниатюры всех доступных форматов (false - только пришедших в $mProfileId)
     * @param bool|string $mError Переменная, в которую будет возвращен текст ошибки либо false
     * @param int $cropHeight Параметр обрезки по высоте (по старому берётся из \skewer\build\Adm\Gallery\Api::cropHeight)
     * @throws \Exception
     * @return bool|array Возвращает массив с описанием созданных изображений либо false
     */
    public static function processImage($sImageFile, $mProfileId, $iSectionId, $bProtected = false, $bCreateAllFormat = true, &$mError = false, $cropHeight = 400) {

        try {
            if (is_array($mProfileId)) {
                $iProfileId = $mProfileId['iProfileId'];
                $aCropData = $mProfileId['crop'];
            } else {
                $iProfileId = (int)$mProfileId;
                $aCropData = array();
            }

            $prefix = '';
            if ($iProfileId == 7) $prefix = 'catalog/';

            /* Обработали ошибки входных данных */
            if (!(int)$iProfileId)         throw new \Exception(\Yii::t('gallery', 'profile_error_imgprocc'));
            if (!(int)$iSectionId)         throw new \Exception(\Yii::t('gallery', 'section_error_imgprocc'));
            if (!file_exists($sImageFile)) throw new \Exception(\Yii::t('gallery', 'photos_error_imgprocc'));

            /* Путь к корневой директории галереи в текущем разделе */
            $sImagePath = \skFiles::getFilePath( $prefix.$iSectionId, strtolower(self::$sModuleDirName), $bProtected);

            /* Чтение настроек профиля */
            $aFormats = Format::getByProfile($iProfileId);

            if (!count($aFormats))          throw new \Exception(\Yii::t('gallery', 'format_error_imgprocc'));

            /* Загрузка исходного изображения для дальнейшей обработки */
            $aValues = \skewer\build\Adm\Files\Api::getLanguage4Image();
            \skImage::loadErrorMessages($aValues);
            $oImage = new \skImage();

            $aOut = false;
            if (!$oImage->load($sImageFile)) throw new \Exception(\Yii::t('gallery', 'photos_error_imgload'));
            $oImage->saveToBuffer();

            /* Создание thumbnail для системы администрирования */
            $bCreatedThumbnail = false;
            if (isSet($aFormats)) {
                foreach ($aFormats as $aFormat)
                    if ($aFormat['name'] == 'preview' AND isSet($aCropData['preview'])) {

                        $aCrop = $aCropData[$aFormat['name']]; // кроп данные

                        // коэффициент масштабирования миниатюры для вырезки
                        $fKoef = 1;
                        if ( $cropHeight AND $oImage->getSrcHeight() > $cropHeight )
                            $fKoef = $oImage->getSrcHeight() / $cropHeight;

                        // обрезка
                        $oImage->cropImage(
                            self::$iThumbnailWidth, self::$iThumbnailHeight,
                            $aCrop['x']*$fKoef, $aCrop['y']*$fKoef,
                            $aCrop['width']*$fKoef, $aCrop['height']*$fKoef,
                            false, true
                        );

                        $bCreatedThumbnail = true;
                    }
            }

            if ($bCreateAllFormat OR $bCreatedThumbnail) {

                if( !$bCreatedThumbnail )
                    $oImage->resize(self::$iThumbnailWidth, self::$iThumbnailHeight);

                $sSavedFile = \skFiles::generateUniqFileName($sImagePath.self::$sThumbnailDirectory.DIRECTORY_SEPARATOR, $sImageFile);
                $sSavedFile = str_replace(\skFiles::getRootUploadPath($bProtected), '', $sSavedFile);
                $sDir = \skFiles::createFolderPath(dirname($sSavedFile), $bProtected);
                if (!$sDir) throw new \Exception(\Yii::t('gallery', 'photos_error_thumbnail'));
                $sThumbnailPath = $sDir.DIRECTORY_SEPARATOR.basename($sSavedFile);
                $aOut['thumbnail'] = \skFiles::getWebPath($sThumbnailPath, false);
                $oImage->save($sThumbnailPath); // Сохранить измененное thumbnail
            }

            /* Обработка изображения по каждому из форматов профиля */
            foreach ($aFormats as $aFormat) {

                $oImage->loadFromBuffer();

                if (isset($aCropData[$aFormat['name']])) {
                    $aCrop = $aCropData[$aFormat['name']]; // кроп данные

                    // коэффициент масштабирования миниатюры для вырезки
                    if ($oImage->getSrcHeight() > $cropHeight)
                        $fKoef = $oImage->getSrcHeight() / $cropHeight;
                    else
                        $fKoef = 1;

                    $oImage->cropImage( // обрезка
                        $aFormat['width'], $aFormat['height'],
                        $aCrop['x']*$fKoef, $aCrop['y']*$fKoef,
                        $aCrop['width']*$fKoef, $aCrop['height']*$fKoef,
                        $aFormat['resize_on_larger_side'], $aFormat['scale_and_crop']
                    );

                } else {
                    if (!$bCreateAllFormat) continue;
                    // обычное изменение размера
                    $oImage->resize($aFormat['width'], $aFormat['height'],$aFormat['resize_on_larger_side'],$aFormat['scale_and_crop']);
                }

                /** not arbeiten */
                if ($aFormat['use_watermark'])
                    $oImage->applyWatermark($aFormat['watermark'], $aFormat['watermark_align']);

                $sSavedFile = \skFiles::generateUniqFileName($sImagePath.$aFormat['name'].DIRECTORY_SEPARATOR, basename($sImageFile));
                $sSavedFile = str_replace(\skFiles::getRootUploadPath($bProtected), '', $sSavedFile);
                $sDir = \skFiles::createFolderPath(dirname($sSavedFile), $bProtected);
                if (!$sDir) throw new \Exception(\Yii::t('gallery', 'directory_error_imgprocc'));
                $sDir = rtrim($sDir, DIRECTORY_SEPARATOR); // Защита от второго слеша
                $sNewFilePath = $sDir.DIRECTORY_SEPARATOR.basename($sSavedFile);

                /* Сохранить измененное изображение */
                if (!$oImage->save($sNewFilePath)) throw new \Exception(\Yii::t('gallery', 'photos_error_imgsave'));
                list($iWidth, $iHeight) = $oImage->getSize();
                $aImage = [
                    'file'   => \skFiles::getWebPath($sNewFilePath, false),
                    'name'   => $aFormat['name'],
                    'width'  => $iWidth,
                    'height' => $iHeight
                ];
                $aOut[$aFormat['name']] = $aImage;
            }// each format

            $oImage->clear();

        } catch (\Exception $e) {
            $mError = $e->getMessage();
            return false;
        }
        return $aOut;
    }// func

    /**
     * Сортирует изображения
     * @param int $iItemId id перемещаемого объекта
     * @param int $iTargetId id объекта, относительно которого идет перемещение
     * @param string $sOrderType направление переноса
     * @param string $sSortField колонка для сортировки
     * @return bool
     */
    public static function sortImages($iItemId, $iTargetId, $sOrderType='before', $sSortField = 'priority') {

        if ( (!$Image = self::getImage($iItemId)) OR // перемещаемое изображение
             (!$TargetImage = self::getImage($iTargetId)) OR // "относительное" изображение
             ($Image['album_id'] != $TargetImage['album_id']) )// должны быть в одном альбоме
                return false;

        // выбираем напрвление сдвига
        if ($Image[$sSortField] > $TargetImage[$sSortField]) {
            $iStartPos = $TargetImage[$sSortField];
            if ($sOrderType=='after') $iStartPos--;
            $iEndPos = $Image[$sSortField];
            $iNewPos = $sOrderType=='after' ? $TargetImage[$sSortField] : $TargetImage[$sSortField] + 1;
            $sSign = '1';
        } else {
            $iStartPos = $Image[$sSortField];
            $iEndPos = $TargetImage[$sSortField];
            if ($sOrderType=='before') $iEndPos++;
            $iNewPos = $sOrderType=='before' ? $TargetImage[$sSortField] : $TargetImage[$sSortField] - 1;
            $sSign = '-1';
        }

        return ( models\Photos::updateAllCounters([$sSortField => $sSign], [ 'AND', ['album_id' => $Image['album_id']], ['>', $sSortField, $iStartPos], ['<', $sSortField, $iEndPos] ]) +
                 models\Photos::updateAll([$sSortField => $iNewPos], ['id' => $Image['id']]) ) >= 0;
    }

    /**
     * Получить несколько последних фотографий, имеющих исходное изображение, из всех фотогалерей
     * (метод используется в сервисе чистки исходных изображений)
     * @param int $iLastDays Число прошедших дней
     * @param int $iLimit Ограничение на количество
     * @return array
     */
    public static function getOlderPhotoWithSourse($iLastDays = 7, $iLimit = 100) {
        return models\Photos::findBySql("SELECT * FROM `".models\Photos::tableName()."` WHERE `source` != '' AND `creation_date` < '".date('Y-m-d H:i:s', strtotime("-$iLastDays days"))."' LIMIT 0, $iLimit")->asArray()->all();
    }


    /** todo Метод пока не используется!
    /**
     * Добавление фотки в альбом
     * @param $sPhoto
     * @param $iAlbumId
     * @param $crop
     * @param $iSection
     * @param $iProfileId
     * @throws \Exception
     * @return int|bool
     */
    public static function addPhotoInAlbum ($sPhoto, $iAlbumId, $crop, $iSection, $iProfileId) {

        $sTitle = '';
        $sAltTitle = '';
        $sDescription = '';

        // построим пути
        $sImagePath = substr($sPhoto, strrpos(DIRECTORY_SEPARATOR, $sPhoto) + 1);
        $sImageFullPath = $sPhoto;

        $mProfileId = array(
            'crop' => $crop,
            'iProfileId' => $iProfileId
        );
        $aNewImage = self::processImage($sImageFullPath, $mProfileId, $iSection, false, true, $sError);
        if (!$aNewImage OR $sError) throw new \Exception($sError);
        $sThumbnail = (isSet($aNewImage['thumbnail'])) ? $aNewImage['thumbnail'] : '';
        unSet($aNewImage['thumbnail']);
        /* Сохранение сущности в БД */

        return self::setImage([
            'title'       => $sTitle,
            'alt_title'   => $sAltTitle,
            'source'      => $sImagePath,
            'visible'     => 1,
            'album_id'    => $iAlbumId,
            'thumbnail'   => $sThumbnail,
            'description' => $sDescription,
            'images_data' => json_encode($aNewImage)
        ]);
    }

    /** todo Метод пока не используется!
    /**
     * Удаление фотографий альбома
     * @param $iAlbumId
     * @throws \Exception
     */
    public static function removeFromAlbum($iAlbumId) {
        /* Выбрать изображения к нему */
        if ($Images = self::getFromAlbum($iAlbumId)) {
            /* Удалить изображения */
            foreach ($Images as $Image) {
                $mError = false;
                if (!self::removeImage($Image['id'], $mError))
                    throw new \Exception($mError);
            }// each image in current album
        }
    }
}