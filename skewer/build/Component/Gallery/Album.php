<?php

namespace skewer\build\Component\Gallery;

use skewer\build\Component\Gallery\models\Photos;
use skewer\build\Adm\Gallery\Search;
use Yii;
use \yii\base\ModelEvent;
use yii\helpers\ArrayHelper;
use yii\db\AfterSaveEvent;
use yii\helpers\FileHelper;

/**
 * @todo Апи для работы с альбомами.
 * Class Album
 * @package skewer\build\Component\Gallery
 */
class Album {

    /**
     * Возвращает данные альбома $iAlbumId
     * @param int $iAlbumId Id запрашиваемого альбома
     * @return bool|models\Albums
     */
    public static function getById($iAlbumId) {
        /** @var models\Albums $oAlbum */
        if ( (!$iAlbumId = (int)$iAlbumId) OR
            (!$oAlbum = models\Albums::findOne($iAlbumId)) ) return false;
        return $oAlbum;
    }

    /**
     * Возвращает данные альбома $sAlbumAlias
     * @param string $sAlbumAlias Alias запрашиваемого альбома
     * @param int $iSectionId
     * @return array
     */
    public static function getByAlias($sAlbumAlias, $iSectionId = 0) {

        if (!$sAlbumAlias) return false;
        return models\Albums::find()
            ->where( ['alias' => $sAlbumAlias] + ($iSectionId ? ['section_id' => $iSectionId] : []) )
            ->asArray()->one();
    }

    /**
     * Добавляет либо обновляет данные альбома. Если указан $iAlbumId,
     * то происходит обновление записи.
     * @param  array $aData Массив данных
     * @param int $iAlbumId Id обновляемого альбома
     * @throws \Exception Сообщение об ошибки валидации полей
     * @return bool|int|\Exception id созданной записи или \Exception / false
     */
    public static function setAlbum(array $aData, $iAlbumId = 0) {

        if ($iAlbumId) { // Изменение альбома с валидацией полей
            if (!$oAlbum = self::getById($iAlbumId)) throw new \Exception(\Yii::t('gallery', 'general_field_empty'));
        } else {// Вставка нового альбома
            $oAlbum = new models\Albums();
            if (isset($aData['section_id']) AND $aData['section_id'])
                $aData['priority'] = models\Albums::find()
                        ->where(['section_id' => $aData['section_id']])
                        ->max('priority') +1;
        }

        $oAlbum->setAttributes($aData);
        if ($oAlbum->save(true)) return $oAlbum->id;

        if ($oAlbum->hasErrors()) { // Если возникла ошибка валидации, то выбросить исключение
            $sFirstError = \yii\helpers\ArrayHelper::getColumn($oAlbum->errors, '0', false)[0];
            throw new \Exception($sFirstError);
        }

        return false;
    }

    /**
     * Возвращает массив видимых (активных) альбомов из раздела $iSectionId
     * @param int $iSectionId Id раздела
     * @param bool $bWithoutHidden Без скрытых альбомов?
     * @return array Возвращает массив найденных альбомов
     */
    public static function getBySection($iSectionId, $bWithoutHidden = true) {

        if (!$iSectionId = (int)$iSectionId)
            return [];

        $oQuery = models\Albums::find()
            ->where(['section_id' => $iSectionId]);

        if ($bWithoutHidden)
            $oQuery->andWhere(['visible' => 1]);

        return $oQuery->orderBy('priority DESC')
            ->asArray()->all();
    }


    /**
     * Добавляет к альбомам кол-во фоток и превьюшку
     * @param array $aAlbums
     * @param bool $active - Только активные
     * @return array $aAlbums
     */
    public static function setCountsAndPreview( array $aAlbums, $active = false){

        if (!$aAlbums)
            return [];

        /** Получим кол-во фоток в альбомах */
        $oQuery = Photos::find()
            ->select(['album_id', 'COUNT(*) AS cnt'])
            ->groupBy(['album_id'])
            ->asArray()
            ->where(['album_id' => ArrayHelper::map($aAlbums, 'id', 'id')]);

        if ($active)
            $oQuery->andWhere(['visible' => 1]);

        $aCounts = ArrayHelper::map( $oQuery->all(), 'album_id', 'cnt' );

        foreach($aAlbums as &$aAlbum){
            $aAlbum['album_count'] = (isset($aCounts[$aAlbum['id']])) ? $aCounts[$aAlbum['id']] : 0;
        }

        // Выбрать последнюю активную превьюшку
        foreach ($aAlbums as &$aAlb) {
            if (!($aAlb['album_count']))
                $aAlb['album_img'] = false;
            else{
                $thumbnail = models\Photos::find()
                    ->select('thumbnail as album_img')
                    ->orderBy(['priority' => SORT_DESC])
                    ->where(['album_id' => $aAlb['id'], 'visible' => 1])
                    ->asArray()
                    ->one();
                if (isset($thumbnail['album_img'])){
                    $aAlb['album_img'] = $thumbnail['album_img'];
                }
            }

        }

        return array_values($aAlbums);

    }

    /**
     * Возвращает Id профиля настроек по $iAlbumId
     * @static
     * @param int $iAlbumId Id альбома
     * @return int|bool
     */
    public static function getProfileId($iAlbumId) {
        /** @var models\Albums $oAlbum */
        if ( (!$iAlbumId = (int)$iAlbumId) OR
            (!$oAlbum = models\Albums::findOne($iAlbumId)) ) return false;
        return $oAlbum->profile_id;
    }

    /**
     * Отдает шаблонный набор значений для добавления новой записи
     * @param array $aData Данные для заполнения нового альбома
     * @return array
     */
    public static function getAlbumBlankValues(array $aData = []) {
        $oAlbum = new models\Albums();
        if ($aData) $oAlbum->setAttributes($aData);
        return $oAlbum->getAttributes();
    }

    /**
     * Удаляет альбом $iAlbumId. Возвращает true, если удаление прошло успешно либо false вслучае ошибки.
     * Текст ошибки сохраняется в параметр $mError
     * @param int $iAlbumId Id удаляемого альбома
     * @param bool|string $mError Содержит текст ошибки либо false
     * @throws \Exception
     * @return bool
     */
    public static function removeAlbum($iAlbumId, &$mError = false) {

        try {
            /* Выбрать альбом */
            if ( (!$iAlbumId = (int)$iAlbumId) OR
                 (!$aAlbum = models\Albums::findOne($iAlbumId)) ) throw new \Exception(\Yii::t('gallery', 'error_notfound'));

            /* Выбрать изображения к нему */
            if ($aImages = Photo::getFromAlbum($iAlbumId)) {
                foreach($aImages as $aImage) { // Удалить изображения
                    $mError = false;
                    if (!Photo::removeImage($aImage['id'], $mError))
                        throw new \Exception($mError);
                }
            }

            /** @var models\Albums $aAlbum */
            $aAlbum->delete();

            /*if (\skewer\build\Component\Site\Type::hasCatalogModule()) {
                // todo это больше не работает так
                //$Goods = \skewer\build\libs\Catalog\Goods::getByGalleryId($iAlbumId);
                //$aData = $Goods ? $Goods->getData() : array();
                $aData = array();
            }else{
                $aData = array();
            }
            // Удаление корневой папки todo удаление не только каталожных
            if (isset($aData['gallery'])) {
                $sDirPath = FILEPATH . 'catalog/' . $aData['gallery'] . '/';
                if (file_exists($sDirPath))
                    \skFiles::delDirectoryRec($sDirPath);
            }*/

            /* Пересчитать зависимости */
            /** @todo дописать после введения сортировки */

        } catch (\Exception $e) {
            $mError = $e->getMessage();
            return false;
        }

        return true;
    }// func

    /**
     * Удаляет альбомы и изображения из раздела $iSectionId
     * @param ModelEvent $event
     */
    public static function removeSection(ModelEvent $event) {

        if ($aAlbums = self::getBySection($event->sender->id, false))
            foreach($aAlbums as $aAlbum) {
                $mError = false;
                self::removeAlbum($aAlbum['id'], $mError);
            }
    }

    /**
     * Обновляет поиск при изменении раздела $iSectionId
     * @param AfterSaveEvent $event
     */
    public static function updateSection(AfterSaveEvent $event) {

        if ($event->sender->group != 'content' || $event->sender->name != 'openAlbum')
            return;

        $aAlbums = self::getBySection($event->sender->parent, true);

        if (!$aAlbums)
            return;

        $oSearch = new Search();

        if ((int)$event->sender->value){
            foreach($aAlbums as $aAlbum) {
                $oSearch->deleteByObjectId($aAlbum['id']);
            }
        }else{
            foreach($aAlbums as $aAlbum) {
                $oSearch->updateByObjectId($aAlbum['id']);
            }
        }
    }

    /**
     * Сортирует альбомы
     * @param int $iItemId id перемещаемого объекта
     * @param int $iTargetId id объекта, относительно которого идет перемещение
     * @param string $sOrderType направление переноса
     * @param string $sSortField колонка для сортировки
     * @return bool
     */
    public static function sortAlbums($iItemId, $iTargetId, $sOrderType='before', $sSortField = 'priority') {

        if ( (!$Album = self::getById($iItemId)) OR // перемещаемый объект
            (!$TargetAlbum = self::getById($iTargetId)) OR // "относительный" объект
            ($Album['section_id'] != $TargetAlbum['section_id']) ) // должны быть в одной секции
            return false;

        // выбираем напрвление сдвига
        if ($Album[$sSortField] > $TargetAlbum[$sSortField]) {
            $iStartPos = $TargetAlbum[$sSortField];
            if ($sOrderType=='after') $iStartPos--;
            $iEndPos = $Album[$sSortField];
            $iNewPos = $sOrderType=='after' ? $TargetAlbum[$sSortField] : $TargetAlbum[$sSortField] + 1;
            $sSign = '1';
        } else {
            $iStartPos = $Album[$sSortField];
            $iEndPos = $TargetAlbum[$sSortField];
            if ($sOrderType=='before') $iEndPos++;
            $iNewPos = $sOrderType=='before' ? $TargetAlbum[$sSortField] : $TargetAlbum[$sSortField] - 1;
            $sSign = '-1';
        }

        return ( models\Albums::updateAllCounters([$sSortField => $sSign], [ 'AND', ['section_id' => $Album['section_id']], ['>', $sSortField, $iStartPos], ['<', $sSortField, $iEndPos] ]) +
            models\Albums::updateAll([$sSortField => $iNewPos], ['id' => $Album['id']]) ) >= 0;
    }

    /**
     * Возвращает первое активное изображение в альбоме
     * @param int $iAlbumId id альбома
     * @return string|bool
     */
    public static function getFirstActiveImage($iAlbumId) {
        if ($aPhoto = Photo::getFromAlbum($iAlbumId, false, 1)) {
            if (isset($aPhoto[0]['images_data']['preview']['file']))
                return $aPhoto[0]['images_data']['preview']['file'];
        }
        return false;
    }

    /**
     * Изменение видимости альбома
     * @param int $iAlbumId id альбома
     * @return bool
     */
    public static function toggleActiveAlbum($iAlbumId){

        $oAlbum = models\Albums::findOne($iAlbumId);
        if ( !$oAlbum )
            return false;

        $oAlbum->visible = (int)!$oAlbum->visible;

        return $oAlbum->save();

    }

    /**
     * копируем каталожную!!!! галерею. Другая галерея не скопируется
     * @param $iAlbumId
     * @return bool|int
     */
    public static function copyAlbum($iAlbumId) {

        if (!$iAlbumId = (int)$iAlbumId)
            return false;

        /** @var models\Albums $oAlbum */
        if (!$oAlbum = models\Albums::findOne($iAlbumId))
            return false;

        /** @todo Как классифицировать каталожный альбом??? */
        // если галерея не каталожная, то не копируем
        if ($oAlbum->section_id != 0 || $oAlbum->owner != 'entity')
            return false;

        $oNewAlbum = new models\Albums();
        $oNewAlbum->setAttributes($oAlbum->getAttributes());

        if (!$oNewAlbum->save())
            return false;

        // записываем в базу
        /** @var models\Photos [] $aImages */
        $aImages = models\Photos::findAll(['album_id' => $iAlbumId]);

        foreach($aImages as $oImg) {
            /** @todo str_replace ? */
            $oNewImg = new models\Photos();
            $oNewImg->setAttributes($oImg->getAttributes());

            $oNewImg->images_data = str_replace('/catalog\/'.$iAlbumId.'\/', '/catalog\/'.$oNewAlbum->id.'\/', $oNewImg->images_data);
            $oNewImg->thumbnail = str_replace('/catalog/'.$iAlbumId.'/', '/catalog/'.$oNewAlbum->id.'/', $oNewImg->thumbnail);
            $oNewImg->source = str_replace('/catalog/'.$iAlbumId.'/', '/catalog/'.$oNewAlbum->id.'/', $oNewImg->source);

            $oNewImg->album_id = $oNewAlbum->id;

            $oNewImg->save();
        }

        // копируем файлоы
        FileHelper::copyDirectory(FILEPATH.'catalog/'.$iAlbumId, FILEPATH.'catalog/'.$oNewAlbum->id, ['dirMode' => 0755]);
        return $oNewAlbum->id;
    }

    /**
     * Создание нового альбома для каталога
     * @return bool|int
     */
    public static function create4Catalog() {
        $aDataProfile = array(
            'owner' => 'entity',
            'profile_id' => Format::TYPE_CATALOG,
            'section_id' => 0
        );
        return self::setAlbum($aDataProfile);
    }

}