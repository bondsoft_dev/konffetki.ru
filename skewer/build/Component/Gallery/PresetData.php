<?php

$aLanguage = array();

$aLanguage['ru']['profile_catalog_name'] = 'Изображения для каталога';
$aLanguage['ru']['format_catalog_mini_name'] = 'Иконка';
$aLanguage['ru']['format_catalog_small_name'] = 'Миниатюра';
$aLanguage['ru']['format_catalog_medium_name'] = 'Большая миниатюра';
$aLanguage['ru']['format_catalog_big_name'] = 'Детальный просмотр';

$aLanguage['ru']['profile_collection_name'] = 'Изображения для коллекций каталог';
$aLanguage['ru']['format_collection_min_name'] = 'для краткого описания и главной';
$aLanguage['ru']['format_collection_med_name'] = 'для подробного описания';
$aLanguage['ru']['format_collection_max_name'] = 'полноразмерное изображение';

$aLanguage['en']['profile_catalog_name'] = 'The images catalog';
$aLanguage['en']['format_catalog_mini_name'] = 'Icon';
$aLanguage['en']['format_catalog_small_name'] = 'Thumbnail';
$aLanguage['en']['format_catalog_medium_name'] = 'Large thumbnail';
$aLanguage['en']['format_catalog_big_name'] = 'Detailed view';

$aLanguage['en']['profile_collection_name'] = 'images for the collectionsг';
$aLanguage['en']['format_collection_min_name'] = 'for a brief description and the main';
$aLanguage['en']['format_collection_med_name'] = 'for a detailed description';
$aLanguage['en']['format_collection_max_name'] = 'larger image';

return $aLanguage;