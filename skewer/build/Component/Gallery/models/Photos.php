<?php

namespace skewer\build\Component\Gallery\models;

use skewer\build\Adm\Gallery\Search;
use Yii;

/**
 * This is the model class for table "photogallery_photos".
 *
 * @property integer $id
 * @property integer $album_id
 * @property string $title
 * @property string $alt_title
 * @property string $description
 * @property string $creation_date
 * @property integer $visible
 * @property integer $priority
 * @property string $images_data
 * @property string $thumbnail
 * @property string $source
 *
 * @method static Photos|null findOne($condition)
 */
class Photos extends \yii\db\ActiveRecord
{

    /** @var [] Массив с картинками */
    public $pictures = [];

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'photogallery_photos';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['album_id'], 'required', 'message' => \Yii::t('gallery' ,'general_field_empty')], // Служебные поля

            [['album_id', 'visible', 'priority'], 'integer'],
            [['creation_date'], 'safe'],
            [['images_data', 'thumbnail', 'source'], 'string'],
            [['title'], 'string', 'max' => 100],
            [['alt_title'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => \Yii::t('gallery', 'images_id'),
            'album_id' => \Yii::t('gallery', 'images_album_id'),
            'title' => \Yii::t('gallery', 'images_title'),
            'alt_title' => \Yii::t('gallery', 'images_alt_title'),
            'description' => \Yii::t('gallery', 'images_description'),
            'creation_date' => \Yii::t('gallery', 'images_creation_date'),
            'visible' => \Yii::t('gallery', 'images_visible'),
            'priority' => \Yii::t('gallery', 'images_priority'),
            'images_data' => \Yii::t('gallery', 'images_images_data'),
            'thumbnail' => \Yii::t('gallery', 'images_thumbnail'),
            'source' => \Yii::t('gallery', 'images_source'),
        ];
    }

    /**
     * Возвращает массив с картинками по форматам
     * @return array
     */
    public function getPictures() {
        if (is_array($this->images_data))
            return $this->images_data;
        return json_decode($this->images_data, true);
    }

    /** Сохранить в JSON перед записью в базу */
    public function beforeSave($insert) {

// @todo не отработает
//        if (is_array($this->images_data))
//            $this->images_data = json_encode($this->images_data);

        return parent::beforeSave($insert);
    }

    /**
     * @inheritDoc
     */
    public function afterSave($insert, $changedAttributes) {

        parent::afterSave($insert, $changedAttributes);

        $oSearch = new Search();
        $oSearch->updateByObjectId($this->album_id);

    }

    /**
     * @inheritDoc
     */
    public function afterDelete() {

        parent::afterDelete();

        $oSearch = new Search();
        $oSearch->updateByObjectId($this->album_id);

    }

    // Пока не используется
    /** Заполнить массив изображений по форматам */
    public function afterFind() { // todo Хороший и удобный в будущем приём, но где гарантия, что всегда будет выполнятся этот метод, а не ->asArray()?
                                  // todo Плюс не всегда он нужен, например, в Photo::setImage(), Photo::sortImages() это лишняя операция
        // $this->pictures = json_decode($this->images_data, true);

        return parent::afterFind();
    }
}