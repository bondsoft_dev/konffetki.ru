<?php

namespace skewer\build\Component\Gallery\models;

use Yii;
use yii\base\ModelEvent;
use skewer\build\Component;
use skewer\build\Adm\Gallery\Search;

/**
 * This is the model class for table "photogallery_albums".
 *
 * @property integer $id
 * @property integer $section_id
 * @property string $title
 * @property string $alias
 * @property string $description
 * @property string $creation_date
 * @property integer $visible
 * @property integer $priority
 * @property string $owner
 * @property integer $profile_id
 *
 * @method static Photos|null findOne($condition)
 */
class Albums extends \yii\db\ActiveRecord
{

    function __construct() {
        parent::__construct();
        $this->title = \Yii::t('gallery', 'title_default');
        $this->visible = 1;
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'photogallery_albums';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['section_id', 'profile_id'], 'required', 'message' => \Yii::t('gallery', 'general_field_empty')], // Служебные поля

            [['section_id', 'visible', 'priority', 'profile_id'], 'integer'],
            [['creation_date'], 'safe'],
            [['title', 'owner'], 'string', 'max' => 100],
            [['alias'], 'string', 'max' => 255],
            [['description'], 'string', 'max' => 512]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => \Yii::t('gallery', 'id'),
            'section_id' => \Yii::t('gallery', 'section_id'),
            'title' => \Yii::t('gallery', 'title'),
            'alias' => \Yii::t('gallery', 'alias'),
            'description' => \Yii::t('gallery', 'description'),
            'creation_date' => \Yii::t('gallery', 'creation_date'),
            'visible' => \Yii::t('gallery', 'visible'),
            'priority' => \Yii::t('gallery', 'priority'),
            'owner' => \Yii::t('gallery', 'owner'),
            'profile_id' => \Yii::t('gallery', 'profile_id'),
        ];
    }

    /**
     * Удаляет альбомы и изображения из раздела $iSectionId
     * @param ModelEvent $event
     */
    public static function removeSection(ModelEvent $event) {
        if ($aAlbums = Component\Gallery\Album::getBySection($event->sender->id, false)) {
            foreach ($aAlbums as $aAlbum) {
                $mError = false;
                Component\Gallery\Album::removeAlbum($aAlbum['id'], $mError);
            }
        }
    }

    /**
     * Класс для сборки списка автивных поисковых движков
     * @param Component\Search\GetEngineEvent $event
     */
    public static function getSearchEngine( Component\Search\GetEngineEvent $event ) {
        $event->addSearchEngine( Search::className() );
    }

    /**
     * @inheritDoc
     */
    public function afterSave($insert, $changedAttributes) {

        parent::afterSave($insert, $changedAttributes);

        $oSearch = new Search();
        $oSearch->updateByObjectId($this->id);

    }

    /**
     * @inheritDoc
     */
    public function afterDelete() {

        parent::afterDelete();

        $oSearch = new Search();
        $oSearch->deleteByObjectId($this->id);

    }

}