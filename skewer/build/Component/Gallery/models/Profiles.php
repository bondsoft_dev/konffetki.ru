<?php

namespace skewer\build\Component\Gallery\models;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * This is the model class for table "photogallery_profiles".
 *
 * @property integer $id
 * @property string $title
 * @property string $name
 * @property integer $active
 */
class Profiles extends \yii\db\ActiveRecord
{

    function __construct() {
        $this->title = \Yii::t('gallery' ,'profile_title_default');
        $this->active = 1;
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'photogallery_profiles';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['title'], 'required', 'message' => \Yii::t('gallery' ,'field_empty').' «'.\Yii::t('gallery' ,'profiles_title').'»!'],

            [['active'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['name'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'Profile ID',
            'title' => 'Title',
            'name' => 'Name',
            'active' => 'Active',
        ];
    }
}