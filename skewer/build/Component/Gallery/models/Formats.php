<?php

namespace skewer\build\Component\Gallery\models;

use Yii;

/**
 * @todo возможно нужно сделать форматы для редактора с флагом, указывающим, что не надо применять ресайз, а только водяные знаки
 */

/**
 * This is the model class for table "photogallery_formats".
 *
 * @property integer $id
 * @property integer $profile_id
 * @property string $title
 * @property string $name
 * @property integer $width
 * @property integer $height
 * @property integer $resize_on_larger_side
 * @property integer $scale_and_crop
 * @property integer $use_watermark
 * @property string $watermark
 * @property integer $watermark_align
 * @property integer $active
 * @property integer $priority
 */
class Formats extends \yii\db\ActiveRecord
{

    function __construct() {
        $this->title = \Yii::t('gallery', 'format_title_default');
        $this->active = 1;
        $this->resize_on_larger_side = 1;
        $this->scale_and_crop = 1;
        $this->watermark_align = alignWatermarkBottomRight;
        $this->use_watermark = 0;
    }

    /**
     * @inheritdoc
     */
    public static function tableName() {
        return 'photogallery_formats';
    }

    /**
     * @inheritdoc
     */
    public function rules() {
        return [
            [['profile_id', 'priority'], 'required', 'message' => \Yii::t('gallery', 'general_field_empty')], // Служебные поля

            [['title'], 'required', 'message' => \Yii::t('gallery', 'field_empty').' «'.\Yii::t('gallery', 'formats_title').'»!'],
            [['name'],  'required', 'message' => \Yii::t('gallery', 'field_empty').' «'.\Yii::t('gallery', 'formats_name').'»!'],
            [['width'], 'required', 'message' => \Yii::t('gallery', 'field_empty').' «'.\Yii::t('gallery', 'formats_width').'»!'],
            [['height'],'required', 'message' => \Yii::t('gallery', 'field_empty').' «'.\Yii::t('gallery', 'formats_height').'»!'],

            [['profile_id', 'width', 'height', 'resize_on_larger_side', 'scale_and_crop', 'use_watermark', 'watermark_align', 'active', 'priority'], 'integer'],
            [['title', 'name'], 'string', 'max' => 100], // todo Как подставить своё сообщение о превышении числа допустимых символов в поле
            [['watermark'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels() {
        return [
            'id' => 'Format ID',
            'profile_id' => 'Profile ID',
            'title' => 'Title',
            'name' => 'Name',
            'width' => 'Width',
            'height' => 'Height',
            'resize_on_larger_side' => 'Resize On Larger Side',
            'scale_and_crop' => 'Scale And Crop',
            'use_watermark' => 'Use Watermark',
            'watermark' => 'Watermark',
            'watermark_align' => 'Watermark Align',
            'active' => 'Active',
            'priority' => 'Priority',
        ];
    }

    public function beforeSave($insert) {
        if (!$this->use_watermark) $this->use_watermark = 0; // Защита от NULL-значения

        return parent::beforeSave($insert);
    }
}