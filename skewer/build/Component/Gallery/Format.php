<?php

namespace skewer\build\Component\Gallery;

/**
 * @todo  Api для работы с форматами. Уйти от цифровых констант, обращаться к форматам по тех. имени
 * Class Format
 * @package skewer\build\Component\Gallery
 */
class Format {

    const TYPE_CATALOG4COLLECTION = 8;
    const TYPE_CATALOG = 7;
    const TYPE_SECTION = 6;

    /**
     * Возвращает формат по id
     * @param int $iFormatId Id формата
     * @return bool|models\Formats Возвращает формат или false
     */
    public static function getById($iFormatId) {
        if (!$iFormatId = (int)$iFormatId) return false;
        /** @var models\Formats $oFormat */
        if ( $oFormat = models\Formats::findOne($iFormatId) ) return $oFormat;
        return false;
    }

    /**
     * Возвращает набор форматов по id профиля, равному $iProfileId
     * @param int $iProfileId
     * @return array Возвращает массив элеметов
     */
    public static function getByProfile($iProfileId) {

        if ( (!$iProfileId = (int)$iProfileId) OR
             ($aFormats = models\Formats::find()
                ->where(['profile_id' => $iProfileId])
                ->orderBy('priority')
                ->asArray()
                ->all()
             ) ) return $aFormats;
        return [];
    }

    /**
     * Возвращает формат галереи по имени
     * @param string $sFormatName
     * @return bool|models\Formats Возвращает массив найденных элеметов либо false
     */
    public static function getByName($sFormatName) {

        if ( (!$sFormatName) OR
             (!$aFormat = models\Formats::find()
                 ->where(['name' => $sFormatName])
                 ->asArray()->one()) ) return false;
        return [$aFormat];
    }

    /**
     * Получить новый формат
     * @param array $aData Данные для заполнения нового формата
     * @return array
     */
    public static function getFormatBlankValues(array $aData = []) {
        $oFormat = new models\Formats();
        if ($aData) $oFormat->setAttributes($aData);
        return $oFormat->getAttributes();
    }

    /**
     * Добавляет либо обновляет данные формата
     * @param array $aData Данные формата
     * @param int $iFormatId id Формата
     * @throws \Exception Сообщение об ошибки валидации полей
     * @return bool|int|\Exception id созданной записи или \Exception / false
     */
    public static function setFormat(array $aData, $iFormatId = 0) {

        if ($iFormatId) { // Изменение формата с валидацией полей
            if (!$oFormat = models\Formats::findOne($iFormatId)) throw new \Exception(\Yii::t('gallery', 'general_field_empty'));
        } else {// Вставка нового формата
            $oFormat = new models\Formats();
            if (isset($aData['profile_id']) AND $aData['profile_id'])
                $aData['priority'] = models\Formats::find()
                        ->where(['profile_id' => $aData['profile_id']])
                        ->max('priority') +1;
        }

        $oFormat->setAttributes($aData);
        if ($oFormat->save(true)) return $oFormat->id;

        if ($oFormat->hasErrors()) { // Если возникла ошибка валидации, то выбросить исключение
            $sFirstError = \yii\helpers\ArrayHelper::getColumn($oFormat->errors, '0', false)[0];
            throw new \Exception($sFirstError);
        }

        return false;
    }

    /**
     * Сортирует форматы
     * @param int $iItemId id перемещаемого объекта
     * @param int $iTargetId id объекта, относительно которого идет перемещение
     * @param string $sOrderType направление переноса
     * @param string $sSortField колонка для сортировки
     * @return bool
     */
    public static function sortFormats($iItemId, $iTargetId, $sOrderType='before', $sSortField = 'priority') {

        if ( (!$Format = self::getById($iItemId)) OR // перемещаемый объект
            (!$TargetFormat = self::getById($iTargetId)) OR // "относительный" объект
            ($Format['profile_id'] != $TargetFormat['profile_id']) ) // должны быть в одном профиле
            return false;

        // выбираем напрвление сдвига
        if ($Format[$sSortField] > $TargetFormat[$sSortField]) {
            $iStartPos = $TargetFormat[$sSortField];
            if ($sOrderType=='before') $iStartPos--;
            $iEndPos = $Format[$sSortField];
            $iNewPos = $sOrderType=='before' ? $TargetFormat[$sSortField] : $TargetFormat[$sSortField] + 1;
            $sSign = '1';
        } else {
            $iStartPos = $Format[$sSortField];
            $iEndPos = $TargetFormat[$sSortField];
            if ($sOrderType=='after') $iEndPos++;
            $iNewPos = $sOrderType=='after' ? $TargetFormat[$sSortField] : $TargetFormat[$sSortField] - 1;
            $sSign = '-1';
        }

        return ( models\Formats::updateAllCounters([$sSortField => $sSign], [ 'AND', ['profile_id' => $Format['profile_id']], ['>', $sSortField, $iStartPos], ['<', $sSortField, $iEndPos] ]) +
                 models\Formats::updateAll([$sSortField => $iNewPos], ['id' => $Format['id']]) ) >= 0;
    }

    /**
     * Проверить разрешённые к изменению из под учётки Adm поля
     * @param array $aData Проверяемые поля
     * @return bool
     */
    public static function editParamsForAdmin (array &$aData) {
        $aFields = ['title','name','width','height','active'];
        foreach ($aData as $sKey => &$aItem)
            if (in_array($sKey, $aFields)) $aItem['view'] = 'show';
        return true;
    }

    /**
     * Удаляет формат
     * @param int $iFormatId id Формата
     * @return bool
     */
    public static function removeFormat($iFormatId) {

        if (!$iFormatId = (int)$iFormatId) return false;
        return models\Formats::deleteAll(['id' => $iFormatId]) >= 0;
    }











    /** todo Метод пока не используется!
    /**
     * Получение данных по кроппингу для каталожных галерей
     */
    public static function getCrop4Catalog() {
        $aFormats = self::getByProfile(self::TYPE_CATALOG);

        foreach ($aFormats as $iKey => $aFormat)
            if (!$aFormat['active']) unset($aFormats[$iKey]);
        unset($aFormats['thumbnail']);

        $aCrop = array();
        if (count($aFormats))
            foreach ($aFormats as $aFormat) {
                $aCrop[$aFormat['name']] = array('x' => 0, 'y' => 0, 'width' => 0, 'height' => 0);
            }
        return $aCrop;
    }
}