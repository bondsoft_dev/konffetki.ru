<?php

namespace skewer\build\Component\Gallery;

use Yii;

/**
 * @todo Апи для работы с профилями галереи.
 * Class Profile
 * @package skewer\build\Component\Gallery
 */
class Profile {

    /**
     * Возвращает профайл по id
     * @param int $iProfileId Id Профиля
     * @return bool|array Возвращает профиль или false
     */
    public static function getById($iProfileId) {

        if ( (!$iProfileId = (int)$iProfileId) OR
             (!$aProfile = models\Profiles::find()
                 ->where(['id' => $iProfileId])
                 ->asArray()->one()) ) return false;
        return $aProfile;
    }

    /**
     * Возвращает Id первого активного профиля
     * @return int|bool Возвращает id профиля или false
     */
    public static function getFirstActiveProfileId() {
        /** @var models\Profiles $oProfile */
        if ( $oProfile = models\Profiles::findOne(['active' => 1]) ) return $oProfile->id;
        return false;
    }

    /**
     * Получить список всех профилей
     * @return models\Profiles []
     */
    public static function getAll() {
        /** @var models\Profiles [] $aProfiles */
        return models\Profiles::find()->all();
    }

    /**
     * Получить новый профиль
     * @param array $aData Данные для заполнения нового профиля
     * @return array
     */
    public static function getProfileBlankValues(array $aData = []) {
        $oProfile = new models\Profiles();
        if ($aData) $oProfile->setAttributes($aData);
        return $oProfile->getAttributes();
    }

    /**
     * Добавляет либо обновляет данные профиля
     * @param array $aData Данные провиля
     * @param int $iProfileId id Профиля
     * @throws \Exception Сообщение об ошибки валидации полей
     * @return bool|int|\Exception id созданной записи или \Exception / false
     */
    public static function setProfile(array $aData, $iProfileId = 0) {

        if ($iProfileId) { // Изменение профиля с валидацией полей
            if (!$oProfile = models\Profiles::findOne($iProfileId)) throw new \Exception(\Yii::t('gallery', 'general_field_empty'));
        } else // Вставка нового профиля
            $oProfile = new models\Profiles();

        $oProfile->setAttributes($aData);
        if (isset($aData['id']) AND (int)$aData['id']) $oProfile->id = (int)$aData['id']; // Вставка с заданным id используется в \skewer\build\Page\CatalogViewer\Install::addGalleryProfiles
        if ($oProfile->save(true)) return $oProfile->id;

        if ($oProfile->hasErrors()) { // Если возникла ошибка валидации, то выбросить исключение
            $sFirstError = \yii\helpers\ArrayHelper::getColumn($oProfile->errors, '0', false)[0];
            throw new \Exception($sFirstError);
        }

        return false;
    }

    /**
     * Удаляет профиль
     * @param int $iProfileId id Профиля
     * @return bool
     */
    public static function removeProfile($iProfileId) {
        if (!$iProfileId = (int)$iProfileId) return false;

        // Удаление связанных форматов
        if ($aFormats = Format::getByProfile($iProfileId))
            foreach ($aFormats as $aFormat)
                Format::removeFormat($aFormat['id']);

        return models\Profiles::deleteAll(['id' => $iProfileId]) >= 0;
    }
}