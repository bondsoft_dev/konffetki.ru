<?php

$aLanguage = array();

$aLanguage['ru']['field_empty'] = 'Не задано поле';
$aLanguage['ru']['general_field_empty'] = 'Ошибка заполнения служебных полей!';

// Albums
$aLanguage['ru']['title_default'] = 'Новый альбом';
$aLanguage['ru']['error_notfound'] = 'Ошибка: Альбом не найден!';
$aLanguage['ru']['section_error_imgprocc'] = 'Ошибка обработки изображения: Секция не найдена!';
$aLanguage['ru']['directory_error_imgprocc'] = 'Ошибка обработки изображения: Директория не создана!';

// Photos
$aLanguage['ru']['photos_error_notfound'] = 'Ошибка: Изображение не существует!';
$aLanguage['ru']['photos_error_imgprocc'] = 'Ошибка обработки изображения: Изображение не найдено!';
$aLanguage['ru']['photos_error_imgload'] = 'Ошибка обработки изображения: Изображение не загружено!';
$aLanguage['ru']['photos_error_thumbnail'] = 'Ошибка обработки изображения: Миниатюра не загружена!';
$aLanguage['ru']['photos_error_imgsave'] = 'Ошибка обработки изображения: Изображение не сохранено!';
$aLanguage['ru']['photos_error_imgempty'] = 'Изображение не определено!';
$aLanguage['ru']['photos_error_imgname'] = 'Имя изображения не задано!';

// Profiles
$aLanguage['ru']['profile_title_default'] = 'Новый профиль';
$aLanguage['ru']['profile_error_imgprocc'] = 'Ошибка обработки изображения: Профиль не найден!';

// Formats
$aLanguage['ru']['format_title_default'] = 'Новый формат';
$aLanguage['ru']['format_error_imgprocc'] = 'Ошибка обработки изображения: Формат не найден!';
$aLanguage['ru']['format_error_load'] = 'Формат не загружен!';

//
$aLanguage['ru']['gallery_browser_title'] = 'Редактирование галереи';


// ****************************************************** ENGLISH ******************************************************

$aLanguage['en']['field_empty'] = 'Not filled field';
$aLanguage['en']['general_field_empty'] = 'Error in filling of general fields!';

// Albums
$aLanguage['en']['title_default'] = 'New gallery';
$aLanguage['en']['error_notfound'] = 'Error: Album not found!';
$aLanguage['en']['section_error_imgprocc'] = 'Image processing error: Section is not exists!';
$aLanguage['en']['directory_error_imgprocc'] = 'Image processing error: Directory is not created!';

// Photos
$aLanguage['en']['photos_error_notfound'] = 'Error: Image nto found!';
$aLanguage['en']['photos_error_imgprocc'] = 'Image processing error: Image is not exists!';
$aLanguage['en']['photos_error_imgload'] = 'Image processing error: Image not loaded!';
$aLanguage['en']['photos_error_thumbnail'] = 'Image processing error: Thumbnail for image is not created!';
$aLanguage['en']['photos_error_imgsave'] = 'Image processing error: Image do not saved!';
$aLanguage['en']['photos_error_imgempty'] = 'Image is empty!';
$aLanguage['en']['photos_error_imgname'] = 'Name is empty!';

// Profiles
$aLanguage['en']['profile_title_default'] = 'New profile';
$aLanguage['en']['profile_error_imgprocc'] = 'Image processing error: Profile is not exists!';

// Formats
$aLanguage['en']['format_title_default'] = 'New format';
$aLanguage['en']['format_error_imgprocc'] = 'Image processing error: Formats is not exists!';
$aLanguage['en']['format_error_load'] = 'Format not load!';

//
$aLanguage['en']['gallery_browser_title'] = 'Editing gallery';

return $aLanguage;