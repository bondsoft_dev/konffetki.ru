<?php

namespace skewer\build\Component\SEO;

use skewer\build\Component\orm;


class DataRow extends orm\ActiveRecord {

    public $id = 'NULL';
    public $group = '';
    public $row_id = 0;
    public $title = '';
    public $keywords = '';
    public $description = '';
    public $frequency = '';
    public $priority = '';
    public $none_index = false;
    public $add_meta = '';

    function __construct() {
        $this->setTableName( 'seo_data' );
    }


    /**
     * Расширение массива данных для страницы
     * @param array $arr Целевой массив - хранитель данных
     * @param string $sGroup Имя группы параметров ( имя шаблона )
     */
    public function setParams( &$arr, $sGroup ) {
        $arr[$sGroup]['title'] = $this->title;
        $arr[$sGroup]['keywords'] = $this->keywords;
        $arr[$sGroup]['description'] = $this->description;
        $arr[$sGroup]['none_index'] = $this->none_index;
        $arr[$sGroup]['add_meta'] = $this->add_meta;
    }

} 