<?php

namespace skewer\build\Component\SEO;


use skewer\build\Component\orm;
use skewer\build\libs\ft;
use skewer\build\Component\Site;


class Template extends orm\TablePrototype {

    protected static $sTableName = 'seo_templates';

    protected static function initModel() {

        ft\Entity::get( self::$sTableName )
            ->clear()
            ->setTablePrefix('')
            ->setNamespace( __NAMESPACE__ )
            ->addField( 'sid', 'varchar(255)', 'sid' )
            ->addField( 'name', 'varchar(255)', 'name' )
            ->addField( 'title', 'text', 'title' )
            ->addField( 'description', 'text', 'description' )
            ->addField( 'keywords', 'text', 'keywords' )
            ->addField( 'info', 'text', 'info' )
            ->save()
            ->build()
        ;
    }


    public static function getNewRow($aData = array()) {

        $oRow = new TemplateRow();

        if ($aData)
            $oRow->setData($aData);

        return $oRow;
    }


    /**
     * Получение шаблона по имени
     * @param string $sName Имя шаблона
     * @return TemplateRow
     */
    public static function getByName( $sName ) {

        return self::find()->where( 'sid', $sName )->getOne();
    }


    /**
     * Описание используемых меток для замены
     * @return string
     */
    public static function getLabelsInfo() {

        $aLabels = array(
            'label_news_title_upper' => 'label_news_title_upper_description',
            'label_news_title_lower' => 'label_news_title_lower_description',
            'label_gallery_title_upper' => 'label_gallery_title_upper_description',
            'label_gallery_title_lower' => 'label_gallery_title_lower_description',
            'label_article_title_upper' => 'label_article_title_upper_description',
            'label_article_title_lower' => 'label_article_title_lower_description',
            'label_faq_title_upper' => 'label_faq_title_upper_description',
            'label_faq_title_lower' => 'label_faq_title_lower_description',
            'label_page_title_upper' => 'label_page_title_upper_description',
            'label_page_title_lower' => 'label_page_title_lower_description',
            'label_path_to_main_upper' => 'label_path_to_main_upper_description',
            'label_path_to_main_lower' => 'label_path_to_main_lower_description',
            'label_path_to_page_upper' => 'label_path_to_page_upper_description',
            'label_path_to_page_lower' => 'label_path_to_page_lower_description',
            'label_site_name' => 'label_site_name_description',
        );

        if ( Site\Type::hasCatalogModule() ) {
            $aLabels['label_catalog_title_upper'] = 'label_catalog_title_upper_description';
            $aLabels['label_catalog_title_lower'] = 'label_catalog_title_lower_description';

            $aLabels['label_collection_title_upper'] = 'label_collection_title_upper_description';
            $aLabels['label_collection_title_lower'] = 'label_collection_title_lower_description';
        }

        $aItems = array();
        foreach ( $aLabels as $sLabel => $sDesc ) {
            $aItems[] = sprintf( '[%s] - %s', \Yii::t('SEO', $sLabel), \Yii::t('SEO', $sDesc) );
        }

        return implode( '<br>', $aItems );
    }
} 