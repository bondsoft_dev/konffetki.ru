<?php


namespace skewer\build\Component\SEO;

use skewer\build\Component\Catalog\GoodsRow;
use skewer\build\Component\orm\Query;
use skewer\build\Component\QueueManager\Task;
use skewer\build\Component\Search;

/**
 * Задача на обновление карты сайта
 */
class SitemapTask extends Task{

    private $sTemplateDir = '';

    /** @var int Сдвиг */
    private $shift = 0;

    private $sMainDomain = '';

    const LIMIT = 5000;

    /**
     * @inheritdoc
     */
    public function init(){

        $dir = WEBPATH.'sitemap_files/';

        if (!is_dir($dir))
            @mkdir($dir);

        /** чистка старых сайтмапов */
        if (file_exists($dir)) {
            $aList = glob( $dir . '/*' );
            if ( is_array($aList) ) {
                foreach ( $aList as $file )
                    if($file !== '/home/p336200/www/konffetki.ru/web/sitemap_files//sitemap.custom.xml')
                        unlink( $file );
            }
        }

    }


    /**
     * @inheritdoc
     */
    public function recovery(){

        $argc = func_get_args();

        $this->shift = (isset($argc[0]['shift'])) ? $argc[0]['shift'] : 0;
    }


    /**
     * @inheritdoc
     */
    public function beforeExecute(){

        //fixme а почему он там?
        $this->sTemplateDir = BUILDPATH.'Tool/SEOTemplates/templates/';

        $this->sMainDomain = \Site::httpDomain();

        $aConfigPaths = \Yii::$app->params['parser']['default']['paths'];
        if ( !is_array($aConfigPaths) ) $aConfigPaths = [];
        $aConfigPaths[] = $this->sTemplateDir;
        \skTwig::setPath( $aConfigPaths );

    }


    /**
     * @inheritdoc
     */
    public function execute(){

        $dir = WEBPATH.'sitemap_files/';

        $aItems = $this->getPageList($this->shift);

        // не все обошли?
        if (count($aItems) == self::LIMIT){
            $this->shift += self::LIMIT;
        }else{
            /** чтобы хвост не переписывал предыдущий файл */
            $this->shift += count($aItems);
        }

        if (count($aItems)==0){
            $this->setStatus(static::stComplete);
            return true;
        }
        // -- parse

        \skTwig::assign('items', $aItems);

        $out = \skTwig::render('sitemap.twig');

        // -- save - rewrite file

        $title = "sitemap.$this->shift.xml";

        $filename = $dir.$title;

        /** @todo проверка на существование и выставить права */
        if (!$handle = fopen($filename, 'w+'))   return false;

        if (fwrite($handle, $out) === FALSE)    return false;

        fclose($handle);


        if (count($aItems) <= self::LIMIT){
            $this->setStatus(static::stComplete);
            return true;
        }

        return true;
    }


    /**
     * @inheritdoc
     */
    public function afterExecute(){

        $dir = WEBPATH.'sitemap_files/';

        // забацаем главный сайтмап
        $aFiles = array();
        $oDateTime = new \DateTime();
        $currentTime = $oDateTime->format(\DateTime::W3C);

        foreach(glob($dir . '*') as $file){
            $aFiles[] = array(
                'url' => $this->sMainDomain.'/sitemap_files/'.basename($file),
                'modify_date' => $currentTime
            );
        }
        //$aFiles[] = array(
        //    'url' => $this->sMainDomain.'/sitemap_files/sitemap.custom.xml',
        //    'modify_date' => $currentTime
        //);
        \skTwig::assign('files', $aFiles);
        $out = \skTwig::render('main_sitemap.twig');

        $filename = WEBPATH."sitemap.xml";
        if (!$handle = fopen($filename, 'w+'))   return false;
        if (fwrite($handle, $out) === FALSE)    return false;
        fclose($handle);

    }


    /**
     * @inheritdoc
     */
    public function reservation(){

        $this->setParams( ['shift' => $this->shift] );

    }


    /**
     * Отдает набор записей для составления карты сайта
     * @param int $shift
     * @throws \Exception todo разобраться и переписать
     * @return array
     *
     */
    private function getPageList($shift = 0){

        $oQuery = Search\SearchIndex::find()->where('status',1)->andWhere('use_in_search', 1);

        if($aDenySections = \Auth::getDenySectionByUserId())
            $oQuery->where( 'section_id NOT IN ?', $aDenySections );

        /** @var Search\Row[] $aRowList */
        $oQuery
            ->limit(self::LIMIT, $shift)
            ->order('modify_date', 'DESC')
            ->asArray();

        $aPages = [];
        
        while ( $aRow = $oQuery->each() ) {

            $oDateTime = \DateTime::createFromFormat('Y-m-d H:i:s',$aRow['modify_date']);
            $aRow['modify_date'] = $oDateTime->format(\DateTime::W3C);
            $aRow['frequency'] = 'always';
            $aRow['priority'] = 1;
            $aRow['url'] = $this->sMainDomain . $aRow['href'];

            $flag = true;

            if( isset($aRow['class_name']) ){

                switch ($aRow['class_name']){

                    /**
                     * @fixme наверное это можно оптимизировать
                     * Убрать, после выноса из карточки сео-полей
                     */
                    case 'CatalogViewer':

                        $oGoodsRow = GoodsRow::get( $aRow['object_id'], 1 );

                        if ( !$oGoodsRow )
                            continue;

                        $aFields = $oGoodsRow->getData();

                        if (isset($aFields['sitemap_frequency'])){
                            $aRow['frequency'] = $aFields['sitemap_frequency'];
                            $flag = false;
                        }
                        if (isset($aFields['sitemap_priority'])){
                            $aRow['priority'] = $aFields['sitemap_priority'];
                            $flag = false;
                        }

                        break;

                    case 'Page':
                        break;

                    default:

                        $oResult = Query::SQL(
                            "SELECT * FROM `seo_data` WHERE `row_id`=:item AND `group`=:group",
                            array(
                                'item' => $aRow['object_id'],
                                'group' => $aRow['class_name']
                            )
                        );

                        if ( $aAddRow = $oResult->fetchArray() ) {
                            if ( $aAddRow['frequency'] ) $aRow['frequency'] = $aAddRow['frequency'];
                            if ( $aAddRow['priority'] ) $aRow['priority'] = $aAddRow['priority'];
                            $flag = false;
                        }

                        break;
                }

            }

            if ( $flag ) {
                $oResult = Query::SQL(
                    "SELECT * FROM `seo_data` WHERE `row_id`=:item AND `group`=:group",
                    [
                        'item' => $aRow['section_id'],
                        'group' => 'section'
                    ]
                );
                if ( $aAddRow = $oResult->fetchArray() ) {
                    if ( $aAddRow['frequency'] ) $aRow['frequency'] = $aAddRow['frequency'];
                    if ( $aAddRow['priority'] ) $aRow['priority'] = $aAddRow['priority'];
                }

            }

            $aPages[] = $aRow;

        }

        return $aPages;
    }

}