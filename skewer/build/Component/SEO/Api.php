<?php

namespace skewer\build\Component\SEO;


use skewer\build\Component\Site;
use skewer\build\Component\UI;
use skewer\core\Component\Config;


/**
 * Класс для работы с SEO данными для всех типов объектов
 * Class Api
 * @package skewer\build\Component\SEO
 * Основные поля: заголовок, описание, ключевые слова, частота обновления, приоритет
 */
class Api {

    /** префикс имен полей */
    const fieldPrefix = 'seodata';

    const TPL_CATALOG_COLLECTION_ELEMENT = 'catalogElementCollection';
    const TPL_CATALOG_COLLECTION = 'catalogCollection';
    const TPL_CATALOG = 'catalogDetail';
    const TPL_CATALOG2LAYER = 'catalogDetail2Layer';

    const TPL_GALLERY = 'galleryDetail';
    const TPL_NEWS = 'newsDetail';
    const TPL_FAQ = 'faqDetail';
    const TPL_ARTICLES = 'articlesDetail';

    /**
     * Отдает данные для заданных группы и id
     * @param string $sGroupName имя группы
     * @param int $iRowId id целевой строки
     * @return DataRow
     */
    public static function get( $sGroupName, $iRowId ) {

        $oRow = Data::find()
            ->where( 'group', $sGroupName )
            ->where( 'row_id', $iRowId )
            ->getOne();

        return $oRow;
    }

    /**
     * @param string $sGroupName имя группы
     * @param int $iRowId id целевой строки
     * @param array $aData данные для сохранения
     * @return int
     */
    public static function set( $sGroupName, $iRowId, $aData ) {

        // запросить существующую запись
        $oDataRow = self::get( $sGroupName, $iRowId );

        // флаг наличия актуальных данных
        $bHasData = self::hasData( $aData );// todo !!!!!!!!!

        if ( $bHasData ) { // если есть данные - сохранить/обновить

            if ( !$oDataRow )
                $oDataRow = Data::getNewRow( array(
                    'group' => $sGroupName,
                    'row_id' => (int)$iRowId,
                ));

            $oDataRow->setData( $aData );
            return $oDataRow->save();

        } elseif ( $oDataRow ) { // если запись есть, а данные пустые

            $oDataRow->delete();
        }

        return 0;
    }

    /**
     * Удаляет запись
     * @static
     * @param string $sGroupName имя группы
     * @param int $iRowId id целевой строки
     * @return bool
     */
    public static function del( $sGroupName, $iRowId ) {

        $oRow = Data::find()
            ->where( 'group', $sGroupName )
            ->where( 'row_id', $iRowId )
            ->getOne();

        return $oRow ? $oRow->delete() : false;
    }

    /**
     * Отдает флаг наличия данных
     * @static
     * @param array $aData
     * @return bool
     */
    protected static function hasData( $aData ) {

        // набор значимых полей
        $aFields = self::getDataFields();

        // пытаемся найти значиения в пришедшем массиве
        foreach ( $aFields as $sName )
            if ( isset($aData[$sName]) and $aData[$sName] )
                return true;

        // если не нашли
        return false;

    }


    /**
     * Отдает набор полей для редактирования в JS интерфейсе
     * @static
     * @param string $sGroupName имя группы
     * @param int $iRowId id целевой строки
     * @return array
     */
    public static function getJSFields( $sGroupName, $iRowId ) {

        // запрос данных
        $oRow = self::get( $sGroupName, $iRowId );

        // инициализация полей
        \Ext::init();

        $oForm = UI\StateBuilder::newEdit();

        $oForm
            ->fieldSelect( 'frequency', \Yii::t('SEO', 'frequency'), self::getFrequencyList() )
            ->field( 'priority', \Yii::t('SEO', 'priority'), 'i', 'float', ['minValue' =>  0, 'maxValue' => 1, 'step' => 0.1] )
            ->fieldString( 'title', \Yii::t('SEO', 'meta_title') )
            ->fieldText( 'description', \Yii::t('SEO', 'meta_description'), ['height' => 60] )
            ->fieldText( 'keywords', \Yii::t('SEO', 'meta_keywords'), ['height' => 60] )
            ->fieldCheck( 'none_index', \Yii::t('SEO', 'none_index') )
            ->fieldText( 'add_meta', \Yii::t('SEO', 'add_meta'), ['height' => 60] )
            ->setValue( $oRow ? $oRow : [] )
        ;

        // набор исходных параметров
        $aSrcFields = $oForm->getForm()->getInterfaceArray();

        // преобразование набора полей
        $aItems = array();
        foreach ( $aSrcFields['items'] as $aField ) {
            $aField['name'] = sprintf(
                '%s_%s_%s',
                self::fieldPrefix,
                $sGroupName,
                $aField['name']
            );
            $aItems[] = $aField;
        }

        return array(
            'name' => 'seo_'.$sGroupName,
            'collapsed' => !$oRow,
            'type' => 'specific',
            'view' => 'specific',
            'extendLibName' => 'SEOFields',
            'layerName' => 'Adm',
            'title' => \Yii::t('SEO', 'group_title'),
            'group_name' => $sGroupName,
            'row_id' => $iRowId,
            'data' => $oRow ? $oRow->getData() : array(),
            'fieldConfig' => $aItems
        );

    }


    /**
     * Сохранение данных, пришедших из админского интерфейса
     * @static
     * @param string $sGroupName имя группы
     * @param int $iRowId id целевой строки
     * @param array $aData массив на сохранение
     * @return bool
     */
    public static function saveJSData( $sGroupName, $iRowId, $aData ) {

        // данные для
        $aSaveData = array();

        // префикс полей
        $sPrefix = sprintf( '%s_%s_', self::fieldPrefix, $sGroupName );

        // набор полей для сохранения
        $aAllowFields = self::getDataFields();

        foreach ( $aAllowFields as $sName ) {

            // полное имя (с префиксом)
            $sFullName = $sPrefix.$sName;

            // проверить наличие поля
            if ( !array_key_exists( $sFullName, $aData ) )
                continue;

            // добавить в массив на сохранение
            $aSaveData[$sName] = $aData[$sFullName];

        }

        // если нечего сохранять
        if ( !$aSaveData )
            return false;

        return self::set( $sGroupName, $iRowId, $aSaveData );

    }


    /**
     * Добавляет набор SEO полей к форме
     * @static
     * @param \ExtForm $oForm
     * @param string $sGroupName имя группы
     * @param int $iRowId id целевой строки
     */
    public static function appendExtForm( \ExtForm $oForm, $sGroupName, $iRowId ) {

        // описание спец поля
        $aField = self::getJSFields( $sGroupName, $iRowId );

        // созадние объекта
        $oField = \Ext::makeFieldObject( $aField );

        // добавление объекта поля к списку
        $oForm->addField( $oField );

        // добавление дополнительной JS библиотеки
        $oForm->addLibClass( 'SEOFields', 'Adm', 'SEO' );

    }


    /**
     * Отдает набор имен полей с данными
     * @static
     * @return string[]
     */
    private static function getDataFields() {
        return array(
            'frequency',
            'priority',
            'title',
            'keywords',
            'description',
            'none_index',
            'add_meta',
        );
    }


    /**
     * Отдает список доступных значений поля "частота обновления"
     * @static
     * @return array
     */
    protected static function getFrequencyList() {
        return array(
            '' => \Yii::t('SEO', 'not_defined'),
            'never' => \Yii::t('SEO', 'never'),
            'daily' => \Yii::t('SEO', 'daily'),
            'weekly' => \Yii::t('SEO', 'weekly'),
            'always' => \Yii::t('SEO', 'always'),
        );
    }

    /**
     * Список шаблонов для robots.txt
     * @return array
     */
    public static function getRobotsPattern(){

        $aModules = \Yii::$app->register->getModuleList(\Layer::PAGE);

        $aResult = array('allow' => array(), 'disallow' => array());
        foreach($aModules as $sModuleName){
            $sClassName = '\\skewer\\build\\Page\\'.$sModuleName.'\\Robots';

            /** @var \skRobotsInterface $sClassName */
            if(!class_exists($sClassName)) continue;
            if (!in_array("skRobotsInterface", class_implements($sClassName))){
                continue;
            }

            $aAllow = $sClassName::getRobotsAllowPatterns();
            if (is_array($aAllow)){
                $aResult['allow'] = array_merge($aResult['allow'], $aAllow);
            }

            $aDisallow = $sClassName::getRobotsDisallowPatterns();
            if (is_array($aDisallow)){
                $aResult['disallow'] = array_merge($aResult['disallow'], $aDisallow);
            }
        }

        $aResult['allow'] = array_unique($aResult['allow']);
        $aResult['disallow'] = array_unique($aResult['disallow']);

        return $aResult;
    }

    /**
     * Устанавливает флаг обновления sitemap
     * Сам процесс обновления будет запущен в самом конце работы скрипта,
     * чтобы избежать повторного выполнения
     *
     * @deprecated заменить на использование skewer\behaviors\Seo как в skewer\models\News
     */
    public static function setUpdateSitemapFlag() {

        \Yii::$app->seo;
        \Yii::$app->trigger('CHANGE_CONTENT');

    }

}
