<?php


namespace skewer\build\Component\SEO;


use skewer\build\Component\orm\state\StateSelect;
use skewer\build\Component\QueueManager\Task;
use skewer\build\Component\Search;
use skewer\build\Component\QueueManager as QM;

/**
 * Задача на обновление поискового индекса
 */
class SearchTask extends Task{

    /** @var StateSelect */
    private $oQuery = null;

    private $iCount = 0;

    private $iLimit = 500;

    /**
     * @inheritdoc
     */
    public function init(){
        \SysVar::set('Search.updated', 0);
    }


    /**
     * @inheritdoc
     */
    public function recovery(){
        $this->iCount = \SysVar::get('Search.updated');
    }

    /**
     * @inheritdoc
     */
    public function beforeExecute(){

        $this->oQuery = Search\SearchIndex::find()->where('status', 0);

    }


    /**
     * @inheritdoc
     */
    public function execute(){

        /**
         * Делаем искуственные ограничения, yii валится на тестовом из-за логера
         */
        if (!$this->iLimit){
            $this->setStatus(static::stInterapt);
            return false;
        }
        $this->iLimit--;

        /** @var Search\Row $oRow */
        $oRow = $this->oQuery->each(); //прочитали одну запись поисковой таблицы

        if (!$oRow){
            $this->setStatus( static::stComplete );
            return false;
        }

        //Получили поисковый класс для записи
        $oSearch =  Search\Api::getSearch($oRow->class_name);

        if ( !$oSearch )
            return false;

        //обновим запись в поиске
        if ($oSearch->updateByObjectId($oRow->object_id, false))
            $this->iCount++;

        return true;

    }


    /**
     * @inheritdoc
     */
    public function afterExecute(){
        \SysVar::set('Search.updated', $this->iCount);
    }

    /**
     * Метод, вызываемый по завершении задачи
     */
    public function complete()
    {
        /**
         * Цепляем задачу на сайтмап, если ее нет
         */
        QM\Api::addTask([
            'class' => '\skewer\build\Component\SEO\SitemapTask',
            'priority' => QM\Task::priorityHigh,
            'title' => 'sitemap update'
        ]);
    }


}