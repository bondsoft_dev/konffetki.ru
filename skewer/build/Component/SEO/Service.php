<?php

namespace skewer\build\Component\SEO;


use skewer\build\Component\QueueManager as QM;
use skewer\build\Component\Search;
use skewer\models\TreeSection;
use yii\helpers\ArrayHelper;


/**
 * Сервис для работы с СЕО компонентами
 * Class Service
 * @package skewer\build\Component\SEO
 */
class Service extends \ServicePrototype {

    /**
     * Очищаем поисковый индекс
     * todo перенести в поиск
     */
    public static function rebuildSearchIndex(){
        Search\SearchIndex::delete()->get();

        $aResourseList = Search\Api::getResourceList();

        foreach($aResourseList as $name => $item){
            /** @var Search\Prototype $oEngine */
            $oEngine = new $item();
            $oEngine->provideName( $name );
            $oEngine->restore();
        }
    }



    /**
     * Обновляет поисковый индекс
     * @param $iTask
     * @throws \Exception
     * @return int
     */
    public static function makeSearchIndex( $iTask = 0 ){

        $oManager = QM\Manager::getInstance();

        $oTask = false;
        if (!$iTask)
            $oTask = QM\Api::getTaskByClassName('\skewer\build\Component\SEO\SearchTask');

        if (!$oTask){
            /** Добавим новую задачу */
            $iTask = QM\Api::addTask([
                'class' => '\skewer\build\Component\SEO\SearchTask',
                'priority' => QM\Task::priorityHigh,
                'title' => 'search index update'
            ]);

            $oTask = QM\Api::getTaskById( $iTask );

        }

        if (!$oTask){
            throw new \Exception('Task not found');
        }

        $oManager->executeTask( $oTask );

        return (in_array( $oTask->getStatus(), [QM\Task::stFrozen, QM\Task::stWait]))?$iTask:0;

    }

    /**
     * Постановка задачи на обновление search index
     * @static
     * @return bool
     */
    public static function updateSearchIndex() {

        return QM\Api::addTask([
            'class' => '\skewer\build\Component\SEO\SearchTask',
            'priority' => QM\Task::priorityHigh,
            'title' => 'search index update'
        ]);

    }


    /**
     * Постановка задачи на обновление sitemap.xml
     * @static
     * @return bool
     */
    public static function updateSiteMap() {

        QM\Api::addTask([
            'class' => '\skewer\build\Component\SEO\SitemapTask',
            'priority' => QM\Task::priorityHigh,
            'title' => 'sitemap update'
        ]);

    }


    /**
     * Обновляет карту сайта
     * @param $iTask
     * @throws \Exception
     * @return array
     */
    public static function makeSiteMap( $iTask = false){

        $oManager = QM\Manager::getInstance();

        $oTask = false;

        if (!$iTask){

            $oTask = QM\Api::getTaskByClassName('\skewer\build\Component\SEO\SitemapTask');

            if (!$oTask){
                /** Добавим новую задачу */
                $iTask = QM\Api::addTask([
                    'class' => '\skewer\build\Component\SEO\SitemapTask',
                    'priority' => QM\Task::priorityHigh,
                    'title' => 'sitemap update'
                ]);

                $oTask = QM\Api::getTaskById( $iTask );
            }
        }

        if (!$oTask){
            throw new \Exception('Task not found');
        }

        $oManager->executeTask( $oTask );

        return ['status' => $oTask->getStatus(), 'id' => $iTask];

    }


    public static function setNewDomainToSiteMap(){

        self::updateSiteMap();

        return true;
    }


    public static function updateRobotsTxt($sDomain){

        //$sTemplateDir = 'skewer/build/common/templates/';

        // набор предустановленных в конфиге путей для парсинга
        $aConfigPaths = \Yii::$app->params['parser']['default']['paths'];
        if ( is_array($aConfigPaths) && isset($aConfigPaths[0])){
            $aConfigPaths = $aConfigPaths[0];
        }

        // -- get data

        $bExistDomain = false;
        if( $sDomain ) $bExistDomain = true;
        if( !\SysVar::isProductionServer() ) $bExistDomain = false;

        $aData = array();
        $aData['domain_exist'] = $bExistDomain;
        $aData['site_url'] = $sDomain;

        $aData['pattern'] = Api::getRobotsPattern();

        $aData['system_service'] = self::getSystemPaths();

        $out = \skParser::parseTwig('robots.twig', $aData, $aConfigPaths);

        // -- save - rewrite file

        $filename = WEBPATH."robots.txt";

        if (file_exists($filename) && !is_writable($filename))
            throw new \Exception('Can\'t write robots.txt');

        if (!$handle = fopen($filename, 'w+'))   return false;

        if (fwrite($handle, $out) === FALSE)    return false;

        fclose($handle);

        return true;

    }

    /**
     * Пути системных разделов
     * @return array
     */
    private static function getSystemPaths()
    {
        $aPaths = [];
        $aServices = [];
        foreach(['search', 'card', 'auth', 'profile'] as $key)
            $aServices[$key] = \Yii::$app->sections->getValues($key);

        if ($aServices)
            $aPaths = TreeSection::find()
                ->where(['id' => $aServices])
                ->asArray()
                ->all();

        return ArrayHelper::map($aPaths, 'alias_path', 'alias_path');
    }

}
