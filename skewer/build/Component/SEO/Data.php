<?php

namespace skewer\build\Component\SEO;

use skewer\build\Component\orm;
use skewer\build\libs\ft;


class Data extends orm\TablePrototype {

    protected static $sTableName = 'seo_data';

    protected static function initModel() {

        ft\Entity::get( self::$sTableName )
            ->clear()
            ->setTablePrefix('')
            ->setNamespace( __NAMESPACE__ )
            ->addField( 'group', 'varchar(16)', 'group_data' )
            ->addField( 'row_id', 'int(11)', 'row_id' )
            ->addField( 'title', 'text', 'meta_title' )
            ->addField( 'keywords', 'text', 'meta_keywords' )
            ->addField( 'description', 'text', 'meta_description' )
            ->addField( 'frequency', 'varchar(16)', 'frequency' )
            ->addField( 'priority', 'float', 'priority' )
            ->addField( 'none_index', 'int(1)', 'none_index' )
            ->addField( 'add_meta', 'text', 'add_meta' )
            ->save()
            //->build()
        ;
    }


    public static function getNewRow($aData = array()) {

        $oRow = new DataRow();

        if ($aData)
            $oRow->setData($aData);

        return $oRow;
    }
} 