<?php

namespace skewer\build\Component\SEO;


use skewer\build\Component\I18N\Messages;
use skewer\build\Component\orm;

class TemplateRow extends orm\ActiveRecord {

    public $id = 'NULL';
    public $sid = '';
    public $name = '';
    public $title = '';
    public $description = '';
    public $keywords = '';
    public $info = '';

    function __construct() {
        $this->setTableName( 'seo_templates' );
    }


    /**
     * @todo @fixme ПЕРЕПИСАТЬ
     * Парсинг поля $sField по параметрам $aData
     * @param string $sField Имя поля
     * @param array $aData Метки для замены
     * @return mixed
     */
    public function parseTpl( $sField, $aData = array() ) {

        $sOut = $this->$sField;

        $list = Messages::getSEOLabels();

        if ( $aData ) {
            foreach ( $aData as $key => $val ) {

                if ( preg_match('/^[A-z_\.]+$/', $key) ) {
                    if ( isSet($list[$key]) && $list[$key] ) {
                        $aLabels = $list[$key];
                        foreach ( $aLabels as &$sLabel )
                            $sLabel = '[' . $sLabel . ']';
                    } else {
                        $aLabels = [ '[' . $key . ']' ];
                    }
                } else {
                    $aLabels = [ '[' . $key . ']' ];
                }


                $sOut = str_replace( $aLabels, $val, $sOut );
            }
        }

        return $sOut;
    }
}