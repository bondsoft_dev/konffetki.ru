<?php

namespace skewer\build\Component\Import;


use skewer\build\Component\Import\ar\Log;
use skewer\build\Component\Section\Tree;

use skewer\build\Component\Catalog\Card;
use skewer\build\Component\Import\ar\ImportTemplate;
use skewer\build\libs\ft;
use skewer\build\Component\QueueManager as QM;
use yii\helpers\ArrayHelper;

/**
 * Апи для импорта
 */
class Api{

    /** Загружаемый файл */
    const Type_File = 1;

    /** Файл по пути */
    const Type_Path = 2;

    /** Удаленный файл */
    const Type_Url = 3;

    /** Провайдер csv */
    const ptCSV = 1;

    /** Провайдер XLS */
    const ptXLS = 2;

    /** Провайдер простой XML */
    const ptXMLSimple = 3;

    /** Провайдер CommerceML импорт товаров */
    const ptCommerceMLImport = 4;

    /** Провайдер CommerceML обновление цен */
    const ptCommerceMLPrice = 5;

    /** Тип поля - значение */
    const ftValue = 'Value';

    /** Тип поля - название */
    const ftTitle = 'Title';

    /** Тип поля - уникальное (артикул) */
    const ftUnique = 'Unique';

    /** Тип поля - раздел */
    const ftSection = 'Section';

    /** Тип поля - галерея */
    const ftGallery = 'Gallery';

    /** Тип поля - справочник */
    const ftDict = 'Dict';

    /** Тип поля - активность */
    const ftActive = 'Active';

    /** Кодировка utf-8 */
    const utf = 'utf-8';

    /** Кодировка windows-1251 */
    const windows = 'windows-1251';

    /**
     * Список типов полей
     * @return array
     */
    public static function getFieldTypeList(){

        return [
            0 => \Yii::t( 'import', 'ft_none'),
            static::ftValue => \Yii::t( 'import', 'ft_value'),
            static::ftTitle => \Yii::t( 'import', 'ft_title'),
            static::ftUnique => \Yii::t( 'import', 'ft_unique'),
            static::ftSection => \Yii::t( 'import', 'ft_section'),
            static::ftGallery => \Yii::t( 'import', 'ft_gallery'),
            static::ftDict => \Yii::t( 'import', 'ft_dict'),
            static::ftActive => \Yii::t( 'import', 'ft_active')
        ];

    }

    /**
     * Список провайдеров данных
     * @return array
     */
    public static function getProviderTypeList(){
        return [
            static::ptCSV => \Yii::t( 'import', 'provider_type_csv' ),
            static::ptXLS => \Yii::t( 'import', 'provider_type_xls' ),
            static::ptXMLSimple => \Yii::t( 'import', 'provider_type_xml' ),
            static::ptCommerceMLImport => \Yii::t( 'import', 'provider_type_commerceml_import' ),
            static::ptCommerceMLPrice => \Yii::t( 'import', 'provider_type_commerceml_price' )
        ];
    }

    /**
     * Получаем провайдер данных
     * @param Config $oConfig
     * @throws Exception
     * @return Provider\Prototype
     */
    public static function getProvider( Config $oConfig ){

        switch ( $oConfig->getParam('provider_type') ){

            case static::ptCSV:
                $oProvider = new Provider\Csv( $oConfig );
                break;

            case static::ptXLS:
                $oProvider = new Provider\Xls( $oConfig );
                break;

            case static::ptXMLSimple:
                $oProvider = new Provider\XmlSimple( $oConfig );
                break;

            case static::ptCommerceMLImport:
                $oProvider = new Provider\CommerceMLImport( $oConfig );
                break;

            case static::ptCommerceMLPrice:
                $oProvider = new Provider\CommerceMLPrice( $oConfig );
                break;

            default:
                throw new Exception(\Yii::t( 'import', 'error_invalid_provider_type'));
                break;

        }

        return $oProvider;

    }

    /**
     * Список шаблонов
     * @return mixed
     */
    public static function getTemplateList(){

        return ImportTemplate::find()->getAll();

    }

    /**
     * Получаем
     * @param $id
     * @return ar\ImportTemplateRow
     */
    public static function getTemplate( $id = null ){

        if ($id)
            $oTpl = ImportTemplate::find( $id );
        else
            $oTpl = ImportTemplate::getNewRow();

        return $oTpl;
    }

    /**
     * Список типов источника
     * @return array
     */
    public static function getTypeList(){
        return [
            static::Type_File => \Yii::t( 'import', 'type_file' ),
            static::Type_Path => \Yii::t( 'import', 'type_path' ),
            static::Type_Url => \Yii::t( 'import', 'type_url' )
        ];

    }

    /**
     * Список карточек
     * @return array
     */
    public static function getCardList(){
        $aList = array();
        foreach ( Card::getGoodsCards( false ) as $oEntity )
            $aList[$oEntity->name] = sprintf( '%s (%s)', $oEntity->title, $oEntity->name );

        return $aList;
    }

    /**
     * Список полей карточки
     * @param int $sCardName
     * @return array
     */
    public static function getFieldList( $sCardName = 1 ){

        $aCardFields = [];

        if ( $oModel = ft\Cache::get( $sCardName ) ) {

            if ( $oModel->getType() == Card::TypeExtended ) {

                $oParentModel = ft\Cache::get( $oModel->getParentId() );

                foreach ( $oParentModel->getFileds() as $oField )
                    $aCardFields[ $oField->getName() ] = $oField->getTitle();

            }

            foreach ( $oModel->getFileds() as $oField ) {

                $aCardFields[ $oField->getName() ] = $oField->getTitle();

            }

        }

        return $aCardFields;
    }

    /**
     * Проверка на кодировку
     * @param $string
     * @return null
     */
    public static function detect_encoding($string) {
        $list = [self::utf, self::windows];

        foreach ($list as $item) {
            $sample = @iconv($item, $item, $string);
            if (md5($sample) == md5($string))
                return $item;
        }
        return null;
    }

    /**
     * Конвертация
     * @param mixed
     * @return mixed
     */
    public static function decode( $mData ){
        if (is_array($mData)){
            return array_map( function( $string) {
                return @iconv( self::windows, self::utf, $string );
            }, $mData);
        }
        return @iconv( self::windows, self::utf, $mData );
    }

    /**
     * Список возможных кодировок
     * @return array
     */
    public static function getCodingList(){
        return [
             self::utf => 'utf-8',
             self::windows => 'windows-1251'
        ];
    }

    /**
     * Запуск импорта. Возвращает статус задачи
     * @param int $iTask задача
     * @param int $iTpl шаблон
     * @return array
     * @throws Exception
     * @throws \Exception
     */
    public static function runImport( $iTask, $iTpl ){

        if (!$iTpl && !$iTask)
            throw new Exception(\Yii::t( 'import', 'error_tpl_not_fount'));

        $oManager = QM\Manager::getInstance();

        if (!$iTask){

            /** @var ar\ImportTemplateRow $oTpl */
            $oTpl = ar\ImportTemplate::find($iTpl);
            if (!$oTpl)
                throw new \Exception(\Yii::t( 'import', 'error_tpl_not_fount'));

            $iTask = QM\Api::addTask([
                'class' => '\skewer\build\Component\Import\Task',
                'priority' => QM\Task::priorityHigh,
                'resource_use' => Task::weightHigh,
                'title' => \Yii::t( 'import', 'task_title', $oTpl->title),
                'parameters' => ['tpl' => (int)$iTpl]
            ]);

        }

        $oTask = QM\Api::getTaskById( $iTask );

        if (!$oTask){
            throw new \Exception(\Yii::t( 'import', 'error_run'));
        }

        $oManager->executeTask( $oTask );

        return ['status' => $oTask->getStatus(), 'id' => $iTask];

    }

    /**
     * Получение логов для шаблона
     * @param $id
     * @return array
     */
    public static function getLogs( $id ){
        $aItems = [];

        $aParams = Log::find()->where('tpl', $id)->where('name', 'start')->order('value', 'DESC')->asArray()->getAll();
        if (!$aParams)
            return [];
        $aParamsStatus = Log::find()->where('task IN ?', ArrayHelper::map($aParams, 'task', 'task'))->where('name', 'status')->asArray()->getAll();
        $aParamsStatus = ArrayHelper::map($aParamsStatus, 'task', 'value');

        foreach( $aParams as $aParam ){
            $aItems[$aParam['task']]['id_log'] = $aParam['task'];
            $aItems[$aParam['task']]['start'] = $aParam['value'];
            $aItems[$aParam['task']]['status'] = isset($aParamsStatus[$aParam['task']])?$aParamsStatus[$aParam['task']]:'';
        }

        return $aItems;

    }

    /**
     * Подробный лог по задаче
     * @param $id
     * @return array
     */
    public static function getLog( $id ){

        $aItems = [];

        $aParams = Log::find()->where('task', $id)->asArray()->getAll();

        foreach( $aParams as $aParam ){

            if ($aParam['list'])
                $aItems[$aParam['name']][] = $aParam['value'];
            else
                $aItems[$aParam['name']] = $aParam['value'];

        }

        return $aItems;

    }


    /**
     * Удаление лога
     * @param $id
     */
    public static function deleteLog( $id ){
        Log::delete()->where('task', $id)->get();
    }


    /**
     * Удаление логов шаблона
     * @param $iTpl
     */
    public static function deleteLog4Template( $iTpl ){
        if ($iTpl)
            Log::delete()->where('tpl', $iTpl)->get();
    }


    /**
     * Скачивание файла из удаленного источника
     * @param string $source
     * @return bool|string
     * @throws \Exception
     */
    public static function uploadFile( $source = '' ){

        if (!$source)
            return '';

        /** Ищем папку библиотек */
        $iLib = Tree::getSectionByAlias('Tool_Import', \Yii::$app->sections->library());
        if (!$iLib)
            throw new \Exception( \Yii::t( 'import', 'error_not_lib_dir'));


        $sFilePath = $iLib;
        if (!\skFiles::checkFilePath( $iLib, '', true ))
            if(!$sFilePath = \skFiles::createFolderPath($sFilePath, true))
                throw new \Exception( \Yii::t( 'import', 'error_not_create_dir', $sFilePath) );

        /** Генерируем уникальное имя файлу */
        $sFileName = \skFiles::generateUniqFileName( \skFiles::getFilePath( $iLib, '', true ), $source );

        $file_headers = @get_headers($source);
        if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found' || ($file_headers[0] == 'HTTP/1.1 302 Found' && $file_headers[7] == 'HTTP/1.1 404 Not Found')){
            throw new \Exception( \Yii::t( 'import', 'error_file_not_found', $source) );
        }
        
        /** Скачиваем файл */
        $bSave = file_put_contents( $sFileName, file_get_contents( $source ));
        if (!$bSave)
            throw new \Exception( \Yii::t( 'import', 'error_not_save_file', $sFileName) );

        return $sFileName;

    }


}