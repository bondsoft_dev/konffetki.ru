<?php

namespace skewer\build\Component\Import\ar;


use skewer\build\Component\orm;

class LogRow extends orm\ActiveRecord {

    public $id = 0;
    public $tpl = '';
    public $task = '';
    public $name = '';
    public $value = '';
    public $list = 0;
    public $saved = 0;

    function __construct() {
        $this->setTableName( 'import_logs' );
        $this->setPrimaryKey( 'id' );
    }

} 