<?php

namespace skewer\build\Component\Import\ar;


use skewer\build\Component\orm;

class ImportTemplateRow extends orm\ActiveRecord {

    public $id = 0;
    public $title = '';
    public $card = '';
    public $coding = 'utf-8';
    public $type = 0;
    public $source = '';
    public $provider_type = '';
    public $settings = '';

    function __construct() {
        $this->setTableName( 'import_template' );
        $this->setPrimaryKey( 'id' );
    }

} 