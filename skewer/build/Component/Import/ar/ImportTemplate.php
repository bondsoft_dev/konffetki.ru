<?php

namespace skewer\build\Component\Import\ar;

use skewer\build\Component\orm;
use skewer\build\libs\ft;

class ImportTemplate extends orm\TablePrototype{

    protected static $sTableName = 'import_template';
    protected static $sKeyField = 'id';

    protected static function initModel() {

        ft\Entity::get(self::$sTableName)
            ->clear(false)
            ->setPrimaryKey(self::$sKeyField)
            ->setTablePrefix('')
            ->setNamespace(__NAMESPACE__)
            ->addField('title', 'varchar(255)', 'title')
            ->addField('card', 'varchar(255)', 'card')
            ->addField('coding', 'varchar(25)', 'coding')
            ->addField('type', 'int(11)', 'type')
            ->addField('source', 'varchar(512)', 'source')
            ->addField('provider_type', 'int(11)', 'provider_type')
            ->addField('settings', 'text', 'settings')

            ->addColumnSet( 'required', ['title', 'card', 'provider_type', 'type'] )

            ->save()
            ->build();
    }

    public static function getNewRow($aData = array()) {
        $oRow = new ImportTemplateRow();

        if ($aData)
            $oRow->setData($aData);

        return $oRow;
    }

} 