<?php

namespace skewer\build\Component\Import\ar;


use skewer\build\Component\orm;
use skewer\build\libs\ft;

class Log extends orm\TablePrototype{

    protected static $sTableName = 'import_logs';
    protected static $sKeyField = 'id';

    protected static function initModel() {

        ft\Entity::get(self::$sTableName)
            ->clear(false)
            ->setPrimaryKey(self::$sKeyField)
            ->setTablePrefix('')
            ->setNamespace(__NAMESPACE__)
            ->addField('tpl', 'int(11)', 'tpl')
            ->addField('task', 'int(11)', 'task')
            ->addField('name', 'varchar(255)', 'name')
            ->addField('value', 'text', 'value')
            ->addField('list', 'int(1)', 'list')
            ->addField('saved', 'int(1)', 'saved')
            ->save()
            ->build()
        ;
    }


    /**
     * @param array $aData
     * @return LogRow
     */
    public static function getNewRow($aData = array()) {
        $oRow = new LogRow();

        if ($aData)
            $oRow->setData($aData);

        return $oRow;
    }



}