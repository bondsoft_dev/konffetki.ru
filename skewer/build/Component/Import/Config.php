<?php

namespace skewer\build\Component\Import;


use skewer\build\Component\Import\ar\ImportTemplateRow;
use yii\helpers\ArrayHelper;

/**
 * Класс для работы с конфигурацией импорта
 */
class Config{

    /**
     * Хранилище данных
     * @var array
     */
    private $storage = [];

    /**
     * @param ImportTemplateRow $oTemplate
     * @throws \Exception
     */
    public function __construct( ImportTemplateRow $oTemplate = null ){

        if ($oTemplate){
            $aData = $oTemplate->getData();
            unset($aData['settings']);

            $this->storage = json_decode( $oTemplate->settings, true );

            if (!$this->storage){
                $this->storage = [];
            }

            $this->storage = array_merge( $this->storage, $aData );

            if ( json_last_error() ){
                throw new \Exception ('no valid data');
            }
        }else{
            $this->storage = [];
        }

    }


    /**
     * Установка данных
     * @param array $aData
     */
    public function setData( $aData = [] ){

        $this->storage = $aData;

    }

    /**
     * Возвращает значение параметра по имени
     * @param string $sParamName Путь к параметру
     * @param mixed $mDefault Значение по умолчанию
     * @return mixed
     */
    public function getParam( $sParamName, $mDefault = '' ){

        return ArrayHelper::getValue( $this->storage, $sParamName, $mDefault );

    }

    /**
     * Запись значения в конфиг
     * @param $sParamName
     * @param $mValue
     */
    public function setParam( $sParamName, $mValue ){
        $this->storage[$sParamName] = $mValue;
    }

    /**
     * Возвращаем данные конфига в виде json
     * @return string
     */
    public function getJsonData(){
        return json_encode( $this->storage );
    }


    /**
     * @return array
     */
    public function getData(){

        return $this->storage;

    }


    /**
     * Установка соответствий полей
     * @param array $aData
     *      [
     *          'field_11' => 5,
     *          'type_11' => 3,
     *      ]
     */
    public function setFields( $aData = [] ){

        foreach ($aData as $sKey => $sVal){
            if (preg_match( '/field_(\w+)/', $sKey )){
                $sKey = str_replace( 'field_', '', $sKey );
                if (!isset($aData['type_'.$sKey]))
                    continue;
                $this->storage['fields'][$sKey]['name'] = $sKey;
                $this->storage['fields'][$sKey]['importFields'] = $sVal;
                $this->storage['fields'][$sKey]['type'] = $aData['type_'.$sKey];
            }
        }
    }


    /**
     * Установка настроек полей
     * @param array $aData
     */
    public function setFieldsParam( $aData = [] ){

        foreach( $aData as $sKey => $sValue ){
            if (preg_match( '/^params_([^:]+):(.+)$/', $sKey, $aMatch)){
                if (isset($this->storage['fields'][$aMatch[1]]))
                    $this->storage['fields'][$aMatch[1]]['params'][$aMatch[2]] = $sValue;
            }
        }
    }


    /**
     * Чистка данных по полям
     */
    public function clearFields(){
        unset($this->storage['fields']);
    }

}