<?php

namespace skewer\build\Component\Import\Field;


use skewer\build\Component\Catalog;

/**
 * Обработчик уникального поля (артикул)
 */
class Unique extends Prototype {

    /** @var bool Создавать новые */
    protected $create = true;


    protected static $parameters = [
        'create' => [
            'name' => 'create',
            'title' => 'field_unique_create',
            'datatype' => 'i',
            'viewtype' => 'check',
            'default' => 1
        ]
    ];


    /**
     * @inheritdoc
     */
    public function getValue(){
        return $this->values;
    }


    /**
     * @inheritdoc
     */
    public function beforeSave() {

        if ($this->values){
            $this->values = array_shift( $this->values );

            $oGoodsRow =  Catalog\Api::getByField( $this->fieldName, $this->values, $this->getCard() );

            $this->config->setParam('new', false);
            if (!$oGoodsRow && $this->create){
                $oGoodsRow = Catalog\Api::createGoodsRow( $this->getCard() );
                $this->config->setParam('new', true);
            }

            if ($oGoodsRow)
                $this->setGoodsRow($oGoodsRow);

        }

    }

}