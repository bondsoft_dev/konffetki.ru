<?php

namespace skewer\build\Component\Import\Field;

use skewer\build\Component\Catalog\GoodsRow;
use skewer\build\Component\Import;
use skewer\build\Component\Import\Config;

/**
 * Прототип поля обработчика данных для импорта
 */
abstract class Prototype {

    /** @var string имя поля в карточке товаров */
    protected $fieldName = '';

    /**
     * набор идентификаторов полей
     * Может содержать как цифры, так и строки в зависимости от провайдера данных
     * @var string[]
     */
    protected $importFieldNames;

    /**
     * данные, пришедшие из выгрузки в виде массива
     * В качестве индексов может содержать как цифры,
     * так и строки в зависимости от провайдера данных
     * @var []
     */
    protected $values = [];

    protected static $parameters = [

    ];

    /** @var Config Конфиг */
    protected $config = null;

    /** @var Import\Task $oTask ссылка на задачу импорта */
    private $task = null;

    /** @var Import\Logger */
    protected $logger = null;

    /**
     * @param array $fields
     * @param string $sFieldName
     * @param Import\Task $oTask
     */
    public function __construct( $fields, $sFieldName, Import\Task $oTask ) {
        $this->importFieldNames = $fields;
        $this->fieldName = $sFieldName;
        $this->task = $oTask;
        $this->config = $this->task->getConfig();
        $this->logger = $this->task->getLogger();
        $this->initParams();
    }


    /**
     * Инициализация объекта
     */
    public function init() {

    }


    /**
     * Имя поля
     * @return string
     */
    final public function getName(){
        return $this->fieldName;
    }


    /**
     * Установка данных
     * @param [] $data
     */
    final public function loadData($data) {
        foreach ( $this->importFieldNames as $sName )
            if ( isset($data[$sName]) )
                $this->values[$sName] = $data[$sName];
    }


    /**
     * Операции, выполняемые до сохранения товара
     */
    public function beforeSave() {

    }


    /**
     * Сохранение
     */
    final public function execute(){

        $sField = $this->fieldName;

        //@fixme ахтунг!
        $this->getGoodsRow()->setData( [$sField => $this->getValue()] );
    }


    /**
     * Отдает значение на сохранение в запись товара
     * @return mixed
     */
    abstract public function getValue();


    /**
     * Функция, вызываемая после сохранения записи
     */
    public function afterSave() {

    }


    /**
     * Функция очистки внутренних данных
     */
    final public function dropDown() {
        $this->values = [];
    }


    /**
     * Завершение работы
     */
    public function shutdown(){

    }


    /**
     * Пропуск строки
     */
    final protected function skipCurrentRow( $bSkip ){
        $this->task->skipCurrentRow( $bSkip );
    }


    /**
     * Получение товара
     * @return GoodsRow
     */
    final protected function getGoodsRow(){
        return $this->task->goodsRow;
    }


    /**
     * Задание товара
     * @param GoodsRow $oGoodsRow
     */
    final protected function setGoodsRow( GoodsRow $oGoodsRow){
        $this->task->goodsRow = $oGoodsRow;
    }


    /**
     * Статус импорта
     * @return mixed
     */
    final protected function getImportStatus(){
        return $this->config->getParam('importStatus');
    }


    /**
     * Получаем карточку
     * @return mixed
     */
    final protected function getCard(){
        return $this->config->getParam('card');
    }


    /**
     * Получение id текущей задачи импорта
     * @return int
     */
    final protected function getTaskId(){
        return $this->task->getId();
    }


    /**
     * Инициализация параметров
     */
    private function initParams(){

        $aParams = $this->config->getParam('fields.'.$this->fieldName.'.params');
        foreach( static::getParameters() as $key => $val){
            if (isset($this->$key)){
                $this->$key = (isset($aParams[$key]))?$aParams[$key]:$val['default'];
            }

        }
    }


    /**
     * Параметры для редактирования
     */
    public static function getParameters(){
        return static::$parameters;
    }

}