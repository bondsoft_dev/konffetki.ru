<?php

namespace skewer\build\Component\Import\Field;

use skewer\build\Component\Gallery\Album;
use skewer\build\Component\Gallery\Format;
use skewer\build\Component\Gallery\Photo;
use skewer\build\Component\Import;

/**
 * Обработчик поля типа галерея
 */
class Gallery extends Prototype {

    /** @var string Разделитель фоток */
    protected $delimiter = ',';

    /** @var bool Пересоздавать фото в альбоме */
    protected $recreate = false;

    /** @var bool Искать по названию */
    protected $find = false;

    /** @var [] кроп */
    private $crop = [];

    /** @var int Профиль */
    private $profile = 0;

    /** @var array Изображения */
    private $photos = [];

    /** @var int Альбом */
    private $album = 0;

    /** @var array Разрешенные форматы файлов */
    private $allowFormatFile = [];

    protected static $parameters = [
        'delimiter' => [
            'title' => 'field_gallery_delimiter',
            'datatype' => 's',
            'viewtype' => 'string',
            'default' => ','
        ],
        'find' => [
            'title' => 'field_gallery_find',
            'datatype' => 'i',
            'viewtype' => 'check',
            'default' => '0'
        ],
        'recreate' => [
            'title' => 'field_gallery_recreate',
            'datatype' => 's',
            'viewtype' => 'check',
            'default' => '0'
        ]
    ];

    public function init(){

        $this->profile = Format::TYPE_CATALOG;

        $this->crop = Format::getCrop4Catalog();

        $this->allowFormatFile = \Yii::$app->params['upload']['allow']['images'];
    }


    /**
     * @inheritdoc
     */
    public function beforeSave(){
        $this->photos = [];
    }


    /**
     * Отдает значение на сохранение в запись товара
     * @return mixed
     */
    public function getValue(){

        $this->album = $this->getGoodsRow()->getData()[$this->fieldName];

        //если не нужна перезапись и фотки уже есть
        if (!$this->recreate && Photo::getCountByAlbum( $this->album ))
            return $this->album;

        $sVal = implode( $this->delimiter, $this->values );

        if ($sVal){

            //создадим альбом
            if (!$this->album)
                $this->album = Album::create4Catalog();

            //собираем фотки
            if ($this->find){
                //ищем по названию
                $aFiles = glob(ROOTPATH . 'import/' . $sVal . '.*');
                if ($aFiles)
                    //проверим на форматы
                    foreach( $aFiles as $sFileName ){
                        $ext = strtolower(substr($sFileName, strrpos($sFileName, '.') + 1));
                        if (in_array($ext, $this->allowFormatFile))
                            $this->photos[] = $sFileName;
                    }
            }else{
                $this->photos = explode( $this->delimiter, $sVal );
                $this->photos = array_map(function( $s ){
                        //где-то этот путь должен быть сохранен
                        return ROOTPATH . 'import/' . $s;
                    },
                $this->photos);
            }

        }

        return $this->album;

    }


    /**
     * @inheritdoc
     */
    public function afterSave(){

        $oGoodsRow = $this->getGoodsRow();
        if (!$oGoodsRow)
            return;

        /** Добавляем фотки здесь, так как при добавлении товар уже должен быть создан */
        if ($this->photos){
            //чистим старые
            Photo::removeFromAlbum( $this->album );
            foreach( $this->photos as $sPhoto ){
                try{
                    $bAbb = Photo::addPhotoInAlbum( $sPhoto, $this->album, $this->crop, $oGoodsRow->getRowId(), $this->profile );
                    if ($bAbb){
                        $this->logger->incParam( 'add_photo' );
                    }
                }catch ( \Exception $e){
                    //
                }
            }
        }

    }

}