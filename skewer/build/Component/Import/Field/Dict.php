<?php

namespace skewer\build\Component\Import\Field;

use skewer\build\Component\Import;
use skewer\build\Component\Catalog;

/**
 * Обработчик поля типа справочник
 */
class Dict extends Prototype {

    /** @var bool Создавать новые */
    protected $create = false;

    protected static $parameters = [
        'create' => [
            'title' => 'field_dict_create',
            'datatype' => 'i',
            'viewtype' => 'check',
            'default' => 0
        ]
    ];


    /** @var \skewer\build\Component\orm\MagicTable */
    private $table = false;

    /**
     * @inheritdoc
     */
    public function init(){
        $this->getDict();

        if (!$this->table)
            throw new Import\Exception( \Yii::t( 'import', 'error_dict_not_found', $this->fieldName) );
    }


    /**
     * Отдает значение на сохранение в запись товара
     * @return mixed
     */
    public function getValue(){

        $val = implode( ',', $this->values );

        if ($this->table){

            //ищем в справочнике
            $aElement = $this->table->find()->where('title', $val)->asArray()->getOne();
            if ($aElement)
                return $aElement['id'];

            //создадим, если надо
            if ($this->create && $val != ''){
                $newEl = $this->table->getNewRow(['title' => $val]);
                if ($newEl->save()){
                    return $newEl->getPrimaryKeyValue();
                }
            }
        }

        return '';

    }


    /**
     * Получение таблицы справочника
     */
    private function getDict(){
        $this->table = Catalog\Dict::getDictTable4Field( $this->fieldName, $this->getCard() );

    }

}