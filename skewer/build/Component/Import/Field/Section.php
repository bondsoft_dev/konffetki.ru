<?php

namespace skewer\build\Component\Import\Field;


use skewer\build\Component\Import;
use skewer\build\Component\Section\Api as SectionApi;
use skewer\build\Component\Section\Tree;
use skewer\build\Component\Section\Parameters;
use yii\helpers\ArrayHelper;

/**
 * Обработчик разделов
 */
class Section extends Prototype {

    /**
     * Разделитель разделов
     * @var string
     */
    protected $delimiter = ';';

    /**
     * Разделитель пути
     * @var string
     */
    protected $delimiter_path = '/';

    /**
     * Корневой раздел
     * @var int
     */
    protected $baseId = 70;

    /**
     * Создавать новые разделы
     * @var bool
     */
    protected $create = false;

    /**
     * Шаблон для создания разделов
     * @var int
     */
    protected $template = 254;

    /**
     * Список разделов
     * @var array
     */
    private $sections = [];

    protected static $parameters = [
        'delimiter' => [
            'title' => 'field_section_delimiter',
            'datatype' => 's',
            'viewtype' => 'string',
            'default' => ';'
        ],
        'delimiter_path' => [
            'title' => 'field_section_delimiter_path',
            'datatype' => 's',
            'viewtype' => 'string',
            'default' => '/'
        ],
        'baseId' => [
            'title' => 'field_section_base_id',
            'datatype' => 'i',
            'viewtype' => 'select',
            'default' => '70',
            'method' => 'getSectionList'
        ],
        'create' => [
            'title' => 'field_section_create',
            'datatype' => 'i',
            'viewtype' => 'check',
            'default' => 0
        ],
        'template' => [
            'title' => 'field_section_template',
            'datatype' => 'i',
            'viewtype' => 'select',
            'default' => '254', // не всегда это так!
            'method' => 'getTemplatesList'
        ]
    ];

    /**
     * @inheritdoc
     */
    public function beforeSave(){

        $this->sections = [];

        //собираем все значения алиасов разделов
        $aAliasList = [];
        if ($this->values){
            foreach( $this->values as $sVal ){
                $aAliasList = array_merge( $aAliasList, explode( $this->delimiter, $sVal ) );
            }
        }

        if ($aAliasList){
            foreach( $aAliasList as $sAlias ){
                //ищем раздел
                $iSection = SectionApi::getIdByAlias( $sAlias, $this->baseId );

                if ( !$iSection && $this->create ){
                    $iSection = $this->createSection( $sAlias, $this->baseId, $this->template );
                }

                if ($iSection){
                    $this->sections[] = $iSection;
                }else{
                    $this->logger->setListParam('no_section', $sAlias);
                }
            }
        }

        //Пропускаем, если разделов нет
        if (!$this->sections){
            $this->skipCurrentRow( true );
        }

    }

    /**
     * @inheritdoc
     */
    public function getValue(){
        //pass
    }

    /**
     * @inheritdoc
     */
    public function afterSave(){
        // Основной раздел должен проставиться сам
        $oGoodsRow = $this->getGoodsRow();
        $this->sections = array_unique($this->sections);
        if ($oGoodsRow)
            $oGoodsRow->setViewSection( $this->sections );
    }


    /**
     * Список разделов для выбора базового
     * @return array
     */
    public static function getSectionList(){

//        $oTree = new \Tree();
//        $aSections = $oTree->getSectionsInLine( \Yii::$app->sections->root() );
        $aSections = Tree::getSectionList( \Yii::$app->sections->root() );

        return ArrayHelper::map( $aSections, 'id', 'title');

    }


    /**
     * Список шаблонов каталожных разделов
     * @return array
     */
    public static function getTemplatesList(){

        $aSections = Tree::getSectionByParent( \Yii::$app->sections->templates() );
        $aSections = ArrayHelper::map( $aSections, 'id', 'id' );

        $aParams = Parameters::getList( $aSections )
            ->group( 'content' )
            ->name(Parameters::object)
            ->rec()
            ->asArray()
            ->get();

        foreach($aParams as $k=>$aParam){
            if ($aParam['value'] != 'CatalogViewer')
                unset($aParams[$k]);
        }

        if ($aParams){
            $aParams = ArrayHelper::map( $aParams, 'parent', 'parent' );
            $aSections = array_intersect( $aSections, $aParams );
        }

        return Tree::getSectionsTitle( $aSections );

    }


    /**
     * @todo тесты на эту функцию!
     * Создание раздела по пути от заданного
     * @param $sAlias
     * @param int $iBaseId
     * @param int $iTemplate
     * @return int|false
     *  функция лежит здесь, а не в апи, так как нужно запоминать сколько и какие разделы созданы
     */
    private function createSection( $sAlias, $iBaseId = 0, $iTemplate = 0 ){

        if (!$sAlias)
            return false;

        //разбираем путь на части
        $aAlias = explode( $this->delimiter_path, $sAlias);
        $sPath = '';
        $iSection = $iBaseId;

        //идем по вложенности
        foreach( $aAlias as $alias){
            $sPath .= '/'.$alias ;
            $sPath = trim($sPath, '/');

            $iParent = SectionApi::getIdByAlias( $sPath, $iBaseId );
            //создаем раздел, если его нет
            if ( !$iParent ){
                $iSection = SectionApi::addSection( $iSection, $alias, $iTemplate );
                if ($iSection){

                    Parameters::setParams($iSection, 'content', 'defCard', $this->getCard());

                    $this->logger->incParam('create_section');
                    $this->logger->setListParam('create_section_list', $sPath);
                }
            }else{
                $iSection = $iParent;
            }
        }

        if ($iSection == $iBaseId)
            return false;

        return $iSection;
    }


}