<?php

namespace skewer\build\Component\Import\Field;

use skewer\build\Component\Import;
use skewer\build\Component\Catalog;
use skewer\build\Component\QueueManager;

/**
 * Обработчик поля типа значение
 */
class Active extends Prototype {

    /** Не снимать активность */
    const hideNone = 0;

    /** Снять активность у всех */
    const hideAll = 1;

    /** Снять активность у всех внутри карточки */
    const hideFromCard = 2;

    /** По значению */
    const value = 0;

    /** Всем в выгрузке */
    const all = 1;

    /** Не удалять */
    const deleteNone = 0;

    /** Удалять все */
    const deleteAll = 1;

    /** Удалять все внутри карточки */
    const deleteFromCard = 2;

    /** @var int Скрывать */
    protected $hide = 0;

    /** @var int Значение */
    protected $fromValue = 0;

    /** @var int Удаление */
    protected $delete = 0;

    /** @var array */
    protected static $parameters = [
        'hide' => [
            'title' => 'field_active_hide',
            'datatype' => 'i',
            'viewtype' => 'select',
            'default' => 0,
            'method' => 'getHideList'
        ],
        'fromValue' => [
            'title' => 'field_active_from_value',
            'datatype' => 'i',
            'viewtype' => 'select',
            'default' => 0,
            'method' => 'getFromValueList'
        ],
        'delete' => [
            'title' => 'field_active_delete',
            'datatype' => 'i',
            'viewtype' => 'select',
            'default' => 0,
            'method' => 'getDeleteList'
        ],
    ];

    /**
     * @inheritdoc
     */
    public function init(){

        /** Начало импорта */
        if ($this->getImportStatus() == Import\Task::importStart){
            switch( $this->hide ){
                case self::hideAll:
                    /** Скрываем все */
                    Catalog\Api::disableAll( $this->fieldName );
                    break;

                case self::hideFromCard:
                    /** Скрываем все внутри карточки */
                    Catalog\Api::disableByCard( $this->getCard(), $this->fieldName );
                    break;
            }
        }

    }


    /**
     * Отдает значение на сохранение в запись товара
     * @return mixed
     */
    public function getValue(){

        if ($this->fromValue == self::all)
            return 1;
        else
            return (bool)implode( ',', $this->values );
    }


    /**
     * @inheritdoc
     */
    public function shutdown(){

        /** Конец импорта */
        if ($this->getImportStatus() == Import\Task::importFinish){
            switch ($this->delete){

                case self::deleteAll:
                case self::deleteFromCard:

                    $card = '';
                    if ( $this->delete == self::deleteFromCard )
                        $card = $this->getCard();

                    // Ставим задачу на удаление
                    QueueManager\Api::addTask([
                        'class' => '\skewer\build\Component\Import\DeleteTask',
                        'priority' => QueueManager\Task::priorityHigh,
                        'title' => 'Удаление товаров после импорта по шаблону ' . $this->config->getParam('id'),
                        'parent' => $this->getTaskId(),
                        'parameters' => ['field_name' => $this->fieldName, 'card' => $card, 'parentTask' => $this->getTaskId(), 'tpl' => $this->config->getParam('id')]
                    ]);

                    break;
            }
        }
    }


    /**
     * Список вариантов скрытия
     * @return array
     */
    public static function getHideList(){
        return [
           self::hideNone => \Yii::t( 'import', 'field_active_hide_none'),
           self::hideAll => \Yii::t( 'import', 'field_active_hide_all'),
           self::hideFromCard => \Yii::t( 'import', 'field_active_hide_from_card')
        ];
    }


    /**
     * Список вариантов выставления значения
     * @return array
     */
    public static function getFromValueList(){
        return [
            self::value => \Yii::t( 'import', 'field_active_value'),
            self::all => \Yii::t( 'import', 'field_active_all'),
        ];
    }


    /**
     * Список вариантов удаления
     * @return array
     */
    public static function getDeleteList(){
        return [
            self::deleteNone => \Yii::t( 'import', 'field_active_delete_none'),
            self::deleteAll => \Yii::t( 'import', 'field_active_delete_all'),
            self::deleteFromCard => \Yii::t( 'import', 'field_active_delete_from_card')
        ];
    }

}