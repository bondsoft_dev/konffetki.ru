<?php

namespace skewer\build\Component\Import\Provider;


use skewer\build\Component\Import;

/**
 * Провайдер для Xml простого типа
 * Class XmlSimple
 */
class XmlSimple extends Prototype{

    /** @var XmlReader xml-reader */
    protected $reader = null;

    /** @var string XPath Путь к узлу товара */
    protected $XPath = '';

    /** @var int Счетчик прочитанных узлов */
    protected $row = 0;

    protected $parameters = [
        'XPath' => [
            'title' => 'field_xml_simple_xpath',
            'datatype' => 's',
            'viewtype' => 'select',
            'default' => '',
            'method' => 'getXPathList'
        ]
    ];


    /**
     * @inheritdoc
     * @return array
     */
    public function getAllowedExtension(){
        return ['xml'];
    }


    /**
     * @inheritdoc
     */
    public function init(){
        try{
            $this->reader = new XmlReader( $this->file, $this->codding != Import\Api::utf );
        }
        catch( \Exception $e ){
            $this->fail( \Yii::t( 'import', 'error_xml_read'));
        }
    }


    /**
     * @inheritdoc
     */
    public function beforeExecute(){

        if (!$this->XPath)
            $this->fail(\Yii::t( 'import', 'error_not_found_xpath'));
        
        $this->row = (int)$this->getConfigVal('row', 0);

    }


    /**
     * @inheritdoc
     */
    public function getRow(){

        /** Читаем узел в массив */
        try{
            $aData = $this->reader->getNode4XPathInLine( $this->XPath, $this->row );
        }
        catch( \Exception $e ){
            $this->fail(\Yii::t( 'import', 'error_not_valid_xpath'));
        }

        $this->row++;
        $this->setConfigVal( 'row', $this->row );

        /** Нет данных */
        if (!$aData)
            return false;

        return $aData;
    }


    /**
     * @inheritdoc
     */
    public function getExample(){

        //@todo не понял, почему сам добавляется </plaintext>
        try{
            if (!$this->XPath){
                /** Текстовый кусок */
                return '<plaintext>' . $this->encode($this->reader->getExampleText());
            }else{
                /** Пример товара */
                return '<plaintext>' . $this->reader->getFirstElement4XPathToText($this->XPath);
            }
        }
        catch( \Exception $e ){
            $this->fail(\Yii::t( 'import', 'error_not_valid_xpath'));
        }
        return [];
    }


    /**
     * @inheritdoc
     */
    public function getInfoRow(){

        if (!$this->XPath)
            $this->fail(\Yii::t( 'import', 'error_not_found_xpath'));

        $aRes = [];
        try{
            $aItems = $this->reader->getElementsListFull($this->XPath, false);
        }
        catch( \Exception $e ){
            $this->fail(\Yii::t( 'import', 'error_not_valid_xpath'));
        }

        foreach($aItems as $key=>$aItem){
            $app = ($aItem['attr'])?\Yii::t( 'import', 'xml_attr'):'';
            $aRes[$key] = $key . $app . ':' . $aItem['value'];
        }

        return $aRes;
    }

    /**
     * @inheritdoc
     */
    public function getPureString(){
        return $this->reader->getFirstText();
    }


    /**
     * Список узлов
     * @return array
     */
    public function getXPathList(){
        return $this->reader->getXPathList();
    }

}