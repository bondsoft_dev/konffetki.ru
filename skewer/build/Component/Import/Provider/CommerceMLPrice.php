<?php


namespace skewer\build\Component\Import\Provider;


use skewer\build\Component\Import;

/**
 * Провайдер данных CommerceML для обновления цен
 * http://v8.1c.ru/edi/edi_stnd/131/offers.xml
 * Class CommerceML
 */
class CommerceMLPrice extends CommerceMLPrototype{

    /** @var string Путь к узлу товара */
    protected $goodXPath = '//КоммерческаяИнформация/ПакетПредложений/Предложения/Предложение';

    /** @var string Имя узла для списка полей */
    protected $fieldNodes = 'Цены';

    /** @var string Имя узла для поля */
    protected $fieldNode = 'Цена';

    /** @var string Имя узла для имени поля */
    protected $fieldName = 'ИдТипаЦены';

    /** @var string Имя узла для значения поля */
    protected $fieldValue = 'ЦенаЗаЕдиницу';

    /** @var string Имя узла для единицу измерения */
    protected $fieldType = 'Единица';

    /** @var string Имя узла для склада */
    protected $fieldStorage = 'Склад';

    /** @var string Имя узла для количества на складе */
    protected $fieldCount = 'Склад/КоличествоНаСкладе';


    /** @var array Массив типов цен */
    private $priceTypes = [];

    /** @var string Путь к типу цены */
    private $priceTypesXPath = '//КоммерческаяИнформация/ПакетПредложений/ТипыЦен/ТипЦены';

    /**
     * @inheritdoc
     */
    public function init(){
        parent::init();

        $this->priceTypes = $this->getConfigVal('priceTypes', []);

        if (!$this->priceTypes){
            /** Получим список типов цен из файла */
            $this->getPriceTypes();
            $this->setConfigVal('priceTypes', $this->priceTypes);
        }

    }


    /**
     * Формируем список типов цен
     * @return void
     */
    private function getPriceTypes(){

        $i = 0;
        while( $aPrices = $this->reader->getNode4XPathInLine( $this->priceTypesXPath, $i )){
            $i++;
            $this->priceTypes[$aPrices['Ид']] = $aPrices['Наименование'];
        }

    }


    /**
     * @inheritdoc
     */
    protected function makeGoodArray( $aNode = [], $showName = false ){
        $aItem = [];

        if (!$aNode)
            return [];

        foreach( $aNode as $key => $value){
            if ( !is_array($value) ){
                /** Обычные поля */
                $aItem[$key] = ($showName) ? $key . ':' . $value : $value;
            }else{
                /** Поля из списка */
                if ($key == $this->fieldNodes){
                    foreach( $value as $fieldKey => $field ){
                        if ( $fieldKey == $this->fieldNode ){
                            if (isset($field[$this->fieldName]) && isset($field[$this->fieldValue])){
                                if (isset($this->priceTypes[$field[$this->fieldName]]))
                                    $aItem['field_' . $field[$this->fieldName]] = ($showName) ? \Yii::t( 'import', 'price') . ' (' . $this->priceTypes[$field[$this->fieldName]] . '): ' . $field[$this->fieldValue] : $field[$this->fieldValue];
                            }
                            if (isset($field[$this->fieldType])){
                                $aItem[$this->fieldType] = ($showName) ? $this->fieldType. ':' .$field[$this->fieldType]: $field[$this->fieldType];
                            }

                        }
                    }
                }

                if ($key == $this->fieldStorage){
                    $aItem[$this->fieldCount] = ($showName) ? $this->fieldCount. ':' .$value[$this->fieldCount]: $value[$this->fieldCount];
                }
            }

        }

        return $aItem;

    }

}