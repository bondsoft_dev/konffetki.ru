<?php

namespace skewer\build\Component;

use skContext;
use skProcess;
use Exception;

/**
 * Класс для работы с деревом процессов
 */
class ProcessList
{

    /**
     * Массив путей по меткам к процессам
     * #stab_note найти разницу между этим и $aProcessList
     * @var skProcess[]
     */
    public $aProcessesPaths = NULL;

    /**
     * Флаг готовности дерева процессов
     * @var bool
     */
    private $bProcessTreeComplete = false;

    /**
     * Массив процессов, которые надо обработать
     * @var array
     */
    protected $aProcessList = array();

    /**
     * Возвращает процесс по пути из меток от корневого процесса page
     *
     * #stab_fix в идеале не должен отдавать не статусы, а процессы,
     * если нужно обработчик может сам проверить статус
     * @param string $sPath Путь от корневого процесса до искомого
     * @param integer $iStatus Статус искомого процесса
     * @return bool|\skProcess
     */
    public function getProcess($sPath, $iStatus = psComplete)
    {
        if (isSet($this->aProcessesPaths[$sPath]))
        {

            if ($iStatus == psAll)
                return $this->aProcessesPaths[$sPath];

            if ( $this->aProcessesPaths[$sPath]->getStatus() === $iStatus )
                return $this->aProcessesPaths[$sPath];

        }

        return $this->bProcessTreeComplete ? psNotFound : psWait;

    }

    /**
     * Восстанавливает список путей к процессам
     * @param skProcess[] $aProcessList Список процессов
     */
    public function recoverProcessPaths(&$aProcessList=null)
    {
        if ( is_null($aProcessList) )
            $aProcessList = &$this->aProcessList;

        foreach ($aProcessList as $oChildProcess)
        {
            // восстанавливаем массив с путями

            $this->aProcessesPaths[$oChildProcess->getLabelPath()] = $oChildProcess;

            if (count($oChildProcess->processes))
                $this->recoverProcessPaths($oChildProcess->processes);
        }
        // #stab_check не понятно почему оно стало complete
        $this->bProcessTreeComplete = true;

    }

    /**
     * Запускает на выполнение дерево процессов
     * @throws Exception
     * @return bool|int
     */
    public function executeProcessList()
    {

        $i = 0;

        $this->bProcessTreeComplete = true;


        do {

            if (++$i > 100)
                throw new Exception('Infinite loop in Processor::executeProcessList');

            $bComplete = true;

            foreach ($this->aProcessesPaths as $oProcess)
            {

                /* @var $oProcess skProcess */
                switch ($oProcess->getStatus())
                {

                    case psNew:
                    case psWait:

                        $sClassName = $oProcess->getModuleClass();
                        \Yii::beginProfile($sClassName, 'sk\Module');

                        $iStatus = $oProcess->execute();

                        \Yii::endProfile($sClassName, 'sk\Module');

                        switch ($iStatus)
                        {

                            case psComplete:

                                if (count($oProcess->processes))
                                {

                                    foreach ($oProcess->processes as $oChildProcess)
                                    {
                                        if ($oChildProcess->getStatus() == psNew)
                                            $bComplete = false; // появились новые процессы
                                    }

                                }
                                break;

                            case psError:
                            case psExit:
                            case psReset:

                                return $iStatus;
                                break;

                            case psWait:
                                $bComplete = false;// Текущий процесс не отработал
                                break;

                        }

                    break;

                }

            }

        } while (!$bComplete);

        return psComplete;

    }

    /**
     * Добавляет новый процесс в очередь выполнения.
     * @param skContext $oContext Контекст создаваемого процесса
     * @return skProcess|null
     */
    public function addProcess(skContext $oContext)
    {
        // #stab_check возможно стоит выбросить исключение
        // Место занято - выходим
        if (isset($this->aProcessList[$oContext->sLabel]))
            return null;

        $oContext->sURL = \Yii::$app->router->getURLTail();
        $oContext->sLabelPath = $oContext->sLabel;
        $oContext->aGet = \Yii::$app->router->aGet; // @todo попытка открыть доступ к GET параметрам в модулях #stab_check пересадить на стандартный

        $this->resetProcessTreeComplete();

        return  $this->aProcessList[$oContext->sLabel] = new skProcess($oContext);

    }

    /**
     * Добавляет процесс в метку с заданным именем
     * Метод использовать с осторожностью
     * #stab_check проверить целесообразность применения в CmsPrototype
     * @param string $sLabel
     * @param skProcess $oProcess
     * @return skProcess #stab_check можно void
     */
    public function setProcessToLabel($sLabel, skProcess $oProcess)
    {
        return $this->aProcessList[$sLabel] = $oProcess;
    }

    /**
     * Регистрирует процесс в списке процессов
     * @param string $sLabelPath Путь по меткам вызова до регистрируемого процесса
     * @param skProcess $oProcess Ссылка на процесс
     */
    public function registerProcessPath($sLabelPath, skProcess $oProcess)
    {
        $this->aProcessesPaths[$sLabelPath] = $oProcess;
    }

    /**
     * Отменяет регистрацию процесса по пути
     * #stab_check ? проверки / исключения
     * @param $sLabelPath
     */
    public function unregisterProcessPath($sLabelPath)
    {
        unSet($this->aProcessesPaths[$sLabelPath]);
    }

    /**
     * Сбрасывет флаг того, что дерево процессов отработало
     */
    public function resetProcessTreeComplete()
    {
        $this->bProcessTreeComplete = false;
    }

    /**
     * Удаляет процесс из обработки
     * #stab_check возможно стоит закрыть unregisterProcessPath
     * @param string $sLabelPath
     * @return bool
     */
    public function removeProcess($sLabelPath)
    {

        #stab_fix ввести проверку наличия процесса по имени и работать с ней

        if ($oProcess = $this->getProcess($sLabelPath))
        {

            $this->resetProcessTreeComplete();

            // удаляем детей
            if (($oProcess instanceof skProcess))
            {
                if (($oParentProcess = $oProcess->getParentProcess()) instanceof skProcess)
                {
                    $oParentProcess->removeChildProcess($oProcess->getLabel());
                    return true;
                } else
                {
                    foreach ($oProcess->processes as $sChildLabel => $oChildProcess)
                        $oProcess->removeChildProcess($sChildLabel);

                    $this->unregisterProcessPath($sLabelPath);
                    unSet($oProcess);
                }
            }

            $this->unregisterProcessPath($sLabelPath);
            $this->aProcessList[$sLabelPath] = null;
            unSet($this->aProcessList[$sLabelPath]);

            return true;
        }

        return false;

    }

}