<?php

namespace skewer\build\Component\Forms;


use skewer\build\Component\orm;
use skewer\build\libs\ft;


class FieldTable extends orm\TablePrototype {

    protected static $sTableName = 'forms_parameters';

    protected static $sKeyField = 'param_id';

    public static $aTypes = array(
        '1' => 'input',
        '2' => 'textarea',
        '5' => 'checkbox',
        '6' => 'file',
        '8' => 'radio',
        '9' => 'select',
        '11' => 'delimiter',
        '13' => 'calendar',
        '25' => 'hidden',
        '26' => 'password'
    );


    public static $aTypes4ExtJS = array(
        '1' => 'string',
        '2' => 'text',
        '5' => 'check',
        '6' => 'string',
        '8' => 'radio',
        '9' => 'select',
        '11' => 'hide',
        '13' => 'date',
        '25' => 'hide',
        '26' => 'string'
    );


    protected static function initModel() {

        ft\Entity::get( 'forms_parameters' )
            ->clear( false )
            ->setPrimaryKey( self::$sKeyField )
            ->setTablePrefix('')
            ->setNamespace( __NAMESPACE__ )
            ->addField( 'form_id', 'int(11)', 'forms.form_id' )
            ->addField( 'param_section_id', 'int(11)', 'forms.param_section_id' )
            ->addField( 'param_name', 'varchar(255)', 'forms.param_name' )
            ->addField( 'param_title', 'varchar(255)', 'forms.param_title' )
            ->addField( 'param_description', 'text', 'forms.param_description' )
            ->addField( 'param_type', 'varchar(255)', 'forms.param_type' )
            ->addField( 'param_required', 'int(1)', 'forms.param_required' )
            ->addField( 'param_default', 'text', 'forms.param_default' )
            ->addField( 'param_maxlength', 'int(11)', 'forms.param_maxlength' )
            ->addField( 'param_validation_type', 'varchar(255)', 'forms.param_validation_type' )
            ->addField( 'param_depend', 'varchar(255)', 'forms.param_depend' )
            ->addField( 'param_man_params', 'varchar(255)', 'forms.param_man_params' )
            ->addField( 'param_priority', 'varchar(255)', 'forms.param_priority' )
            ->addField( 'label_position', 'varchar(5)', 'forms.label_position' )
            ->addField( 'new_line', 'int(1)', 'forms.new_line' )
            ->addField( 'width_factor', 'int(11)', 'forms.width_factor' )
            ->save()
        ;

    }


    public static function getNewRow( $aData = array() ) {

        $oRow = new FieldRow();

        if ( $aData )
            $oRow->setData( $aData );

        return $oRow;
    }


    /**
     *
     * @param FieldRow $oField
     * @return array
     * todo review
     */
    public static function parse( $oField ) {

        switch ( $oField->param_type ) {

            // Radio Type
            case '8':

                $aRows = $oField->parseDefaultAsList();
                $aItems = array();
                $iKey = 0;

                if ( is_array($aRows) && count($aRows) )
                    foreach ( $aRows as $sKey => $sValue ) {

                        $aItem = array(
                            'value' => trim( $sKey ),
                            'title' => $sValue,
                            'checked' => !$iKey ? 'checked': '',
                            'param_id' => $oField->param_name . $iKey,
                            'param_name'=> $oField->param_name
                        );

                        $iKey++;
                        $aItems[] = $aItem;
                    }

                return $aItems;
                break;

            //select
            case '9':

                if ( preg_match( '/^([a-zA-Z0-9\\\\]+)\.(\w+)\(\)$/', $oField->param_default, $aItem ) ) {

                    $sClassName = isSet($aItem[1]) ? $aItem[1]: 'none';
                    $sMethodName = isSet($aItem[2]) ? $aItem[2]: 'none';

                    $oCurClass = new \ReflectionClass( $sClassName );

                    if( !($oCurClass instanceof \ReflectionClass) )
                        throw new \Exception("Не создан класс [$sClassName]");

                    if( $oCurClass->getParentClass()->name != 'ServicePrototype')
                        throw new \Exception('Попытка запуска неразрешенного класса');

                    $oCurObj = new $sClassName();

                    if( !method_exists( $oCurObj, $sMethodName ) )
                        throw new \Exception('Попытка запуска несуществующего метода');

                    $aRows = call_user_func_array( array($oCurObj,$sMethodName), array() );

                } else {
                    $aRows = $oField->parseDefaultAsList();
                }
                $aItems = array();
                $iKey = 0;

                if ( is_array($aRows) && count($aRows) )
                    foreach ( $aRows as $sKey => $sValue ) {

                        $aItem = array(
                            'value' => trim( $sKey ),
                            'title' => $sValue,
                            'selected' => !$iKey ? 'selected' : ''
                        );

                        $iKey++;
                        $aItems[] = $aItem;
                    }

//                array_unshift(
//                    $aItems,
//                    array(
//                        'value' => '',
//                        'title' => $oField->param_description,
//                        'selected'=> 'selected'
//                    )
//                );

                return $aItems;
                break;
        }

        return $oField->param_default;
    }

    /**
     * Возвращает размер максимально допустимого размера для загрузки файлов через формы
     * @return int
     */
    public static function getUploadMaxSize(){
        $iMaxUploadSizeIni = \skFiles::getMaxUploadSize()/1024/1024;
        $iMaxUploadSizeConf = \Yii::$app->params['upload']['form']['maxsize'];

        if ($iMaxUploadSizeIni && $iMaxUploadSizeConf){
            $iMaxUploadSizeConf = $iMaxUploadSizeConf/1024/1024;
            return min($iMaxUploadSizeIni, $iMaxUploadSizeConf);
        }else{
            return ($iMaxUploadSizeConf)?$iMaxUploadSizeConf:$iMaxUploadSizeIni;
        }
    }


    /**
     * Виджет для вывода текста для типа формы
     * @param orm\ActiveRecord $oItem
     * @param $sField
     * @return string
     */
    public static function getTypeTitle( $oItem, $sField ) {

        return \Yii::t( 'forms' ,'type_' . self::$aTypes[$oItem->getVal( $sField )] );
    }


    public static function getTypeList() {

        $aTypes = array();

        foreach ( self::$aTypes as $sKey => $sTypeName )
            $aTypes[ $sKey ] = \Yii::t( 'forms' ,'type_' . $sTypeName );

        return $aTypes;
    }

    public static function getLabelPositionList() {
        return array(
            'left' => \Yii::t( 'forms', 'position_left'),
            'top' => \Yii::t( 'forms', 'position_top'),
            'right' => \Yii::t( 'forms', 'position_right'),
            'none' => \Yii::t( 'forms', 'position_none')
        );
    }


    public static function getWidthFactorList() {
        return array(
            '1' => 'x1',
            '2' => 'x2',
            '3' => 'x3',
            '4' => 'x4'
        );
    }



    public static function getValidatorList() {

        $aValidators = array(
            'text' => \Yii::t( 'forms' ,'validation_text' ),
            'number' => \Yii::t( 'forms' ,'validation_number' ),
            'digits' => \Yii::t( 'forms' ,'validation_digits' ),
            'url' => \Yii::t( 'forms' ,'validation_url' ),
            'date' => \Yii::t( 'forms' ,'validation_date' ),
            'email' => \Yii::t( 'forms' ,'validation_email' )
        );
        return $aValidators;//\sk\Yii::t( 'forms' ,'validation_' . self::$aTypes[$oItem->getVal( $sField )] );
    }


    public static function sort( $aItemId, $aTargetId, $sOrderType='before' ) {

        $oItem = self::find( $aItemId );
        $oTarget = self::find( $aTargetId );

        if ( !$oItem || !$oTarget )
            return false;

        $sSortField = 'param_priority';

        // должны быть в одной форме
        if( $oItem->getVal('form_id') != $oTarget->getVal('form_id') )
            return false;

        $iItemPos = $oItem->getVal($sSortField);
        $iTargetPos = $oTarget->getVal($sSortField);

        // выбираем напрвление сдвига
        if ( $iItemPos > $iTargetPos ) {

            $iStartPos = $iTargetPos;
            if( $sOrderType=='before' ) $iStartPos--;
            $iEndPos = $iItemPos;
            $iNewPos = $sOrderType=='before' ? $iTargetPos : $iTargetPos + 1;
            self::shiftPosition( $oItem->getVal('form_id') , $iStartPos, $iEndPos, '+' );
            self::changePosition( $oItem->getVal('param_id') ,$iNewPos );

        } else {

            $iStartPos = $iItemPos;
            $iEndPos = $iTargetPos;
            if( $sOrderType=='after' ) $iEndPos++;
            $iNewPos = $sOrderType=='after' ? $iTargetPos : $iTargetPos - 1;
            self::shiftPosition( $oItem->getVal('form_id') , $iStartPos, $iEndPos, '-' );
            self::changePosition( $oItem->getVal('param_id') ,$iNewPos );

        }

        return true;

    }


    private static function shiftPosition ( $iFormId,$iStartPos,$iEndPos,$sSign = '+' ) {

        orm\Query::UpdateFrom( static::$sTableName )
            ->set( 'param_priority=param_priority'.$sSign.'?', 1 )
            ->where( 'form_id', (int)$iFormId )
            ->where( 'param_priority>?', (int)$iStartPos )
            ->where( 'param_priority<?', (int)$iEndPos )
            ->get();
    }


    private static function changePosition ( $iParamId, $iPos ) {

        orm\Query::UpdateFrom( static::$sTableName )
            ->set( 'param_priority', (int)$iPos )
            ->where( 'param_id', (int)$iParamId )
            ->get();
    }


} 