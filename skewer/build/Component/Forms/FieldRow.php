<?php

namespace skewer\build\Component\Forms;


use skewer\build\Component\orm;


class FieldRow extends orm\ActiveRecord {

    public $param_id = 'NULL';
    public $form_id = '';
    public $param_section_id = '';
    public $param_name = '';
    public $param_title = '';
    public $param_description = '';
    public $param_type = '';
    public $param_required = '';
    public $param_default = '';
    public $param_maxlength = '';
    public $param_validation_type = '';
    public $param_depend = '';
    public $param_man_params = '';
    public $param_priority = '';
    public $label_position = 'left';
    public $new_line = 1;
    public $width_factor = 1;

    function __construct() {
        $this->setTableName( 'forms_parameters' );
        $this->setPrimaryKey( 'param_id' );
        $this->param_title = \Yii::t( 'forms', 'new_param');
        $this->param_type = 1;
        $this->param_validation_type = 'text';
        $this->param_maxlength = 255;
    }

    public function getType() {
        return isSet( FieldTable::$aTypes[$this->param_type] ) ? FieldTable::$aTypes[$this->param_type] : '';
    }

    public function getType4ExtJS() {
        return isSet( FieldTable::$aTypes4ExtJS[$this->param_type] ) ? FieldTable::$aTypes4ExtJS[$this->param_type] : 'string';
    }

    public function getViewItems() {
        return FieldTable::parse( $this );
    }

    public function getClassVal() {
        return isSet($this->label_class) ? $this->label_class : '1';
    }

    /**
     * Возвращает максимальный размер загружаемого файла для поля в мегабайтах
     * @return int
     */
    public function getMaxFileSize(){
        if (!$this->param_maxlength){
            $iMaxUploadSize = FieldTable::getUploadMaxSize();
        }else{
            $iMaxUploadSize = (FieldTable::getUploadMaxSize())?min(FieldTable::getUploadMaxSize(), $this->param_maxlength):$this->param_maxlength;
        }

        return $iMaxUploadSize;
    }

    /**
     * Приведение имени поля к уникальному значению в рамках формы
     */
    private function setUniqueName() {

        $this->param_name = \skFiles::makeURLValidName( $this->param_name, false );

        $iNum = 0;

        $bFlag = true;

        do {

            $sCurAlias = $this->param_name . ( $iNum ? $iNum : '' );

            $iCount = FieldTable::find()
                ->where( 'param_name', $sCurAlias )
                ->where( 'form_id', $this->form_id )
                ->where( 'param_id<>?', $this->param_id )
                ->getCount();

            if ( !$iCount ) {

                $this->param_name = $sCurAlias;

                $bFlag = false;

            }

            $iNum++;

            if ( $iNum > 300 ) $bFlag = false;

        } while( $bFlag );

    }


    private function setPosition() {

        if ( !$this->param_priority ) {

            // todo реализовать max() дла запросов
            $aItems = FieldTable::find()
                ->where( 'form_id', (int)$this->form_id )
                ->getAll();

            $iPos = 0;

            /* @var $oFieldRow FieldRow */
            foreach ( $aItems as $oFieldRow )
                $iPos = ( $iPos < $oFieldRow->param_priority ) ? (int)$oFieldRow->param_priority : $iPos;

            $this->param_priority = $iPos + 1;
        }

    }


    public function preSave() {

        if ( !$this->param_maxlength )
            $this->param_maxlength = 255;

        if ( !$this->param_name )
            $this->param_name = $this->param_title;

        $this->setUniqueName();

        $this->setPosition();

    }


    public function validate( $sValue ) {

        if ( is_array($sValue) ) return false;

        $iMin = 1;
        $iMax = $this->param_maxlength;

        if ( empty($sValue) AND !$this->param_required ) return true;// todo WTF

        switch ( $this->param_validation_type ) {

            case 'text':
                if (mb_strlen($sValue,'UTF-8') >= $iMin AND mb_strlen($sValue,'UTF-8') <= $iMax) return true;
                break;

            case 'digits':
                if (preg_match("/^\d{".$iMin.','.$iMax."}$/",$sValue)) return true;
                break;

            case 'number':
                if (preg_match("/^[\-]?\d+[\.]?\d*$/",$sValue)) return true;
                break;

            case 'email':
                if ( \skValidator::isEmail($sValue) ) return true;
                break;

            case 'url':
                if (filter_var($sValue, FILTER_VALIDATE_URL) !== false)
                    if (mb_strlen($sValue,'UTF-8') >= $iMin AND mb_strlen($sValue,'UTF-8') <= $iMax) return true;
                break;

            case 'phone':
                if (mb_strlen($sValue,'UTF-8') >= $iMin AND mb_strlen($sValue,'UTF-8') <= $iMax) return true; # сейчас только как текст
                break;

            case 'date':
                if (mb_strlen($sValue,'UTF-8') >= $iMin AND mb_strlen($sValue,'UTF-8') <= $iMax) return true; # сейчас только как текст
                break;

            case 'creditcard':
                if (mb_strlen($sValue,'UTF-8') >= $iMin AND mb_strlen($sValue,'UTF-8') <= $iMax) return true; # сейчас только как текст
                break;

            case 'login':
                if (preg_match("/^[\da-z\_\-]{".$iMin.','.$iMax."}$/i",$sValue)) return true;
                break;

            case 'md5':
                if (preg_match("/^[\da-z]{32}$/i",$sValue)) return true;
                break;

            default:
                return false;

        }// switch


        return false;
    }


    /**
     * Формерует массив для списка из значений по умолчанию
     * @return array
     */
    public function parseDefaultAsList() {

        $aRows = explode( ';', trim( preg_replace( '/\x0a+|\x0d+/Uims', '', $this->param_default ) ) );
        $aRows = array_diff( $aRows, array('') );
        $aItems = array();

        foreach ( $aRows as $sRow ) {

            if ( strpos( $sRow, ':' ) )
                list( $sValue, $sTitle ) = explode( ':', $sRow );
            else
                $sValue = $sTitle = $sRow;

            $aItems[ $sValue ] = $sTitle ;
        }

        return $aItems;
    }
} 