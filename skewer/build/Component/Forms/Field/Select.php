<?php

namespace skewer\build\Component\Forms\Field;


class Select extends Prototype {

    public $type = 'select';

    protected $sTpl = 'select.twig';
} 