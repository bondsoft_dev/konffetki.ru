<?php

namespace skewer\build\Component\Forms\Field;


class Calendar extends Select  {

    public $clsSpec = 'form__date';

    protected $sTpl = 'calendar.twig';

} 