<?php

namespace skewer\build\Component\Forms\Field;


class Captcha extends Prototype {

    public $type = 'captcha';
    public $clsSpec = 'form__captha';

    protected $sTpl = 'captcha.twig';


    public function getRandVal() {
        return rand(1000,9999);
    }
}