<?php

namespace skewer\build\Component\Forms\Field;


class Checkbox extends Prototype {

    public $type = 'checkbox';
    public $clsSpec = 'form__checkbox';

    protected $sTpl = 'checkbox.twig';

} 
