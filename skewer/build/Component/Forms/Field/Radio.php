<?php

namespace skewer\build\Component\Forms\Field;


class Radio extends Select {

    public $type = 'radio';
    public $clsSpec = 'form__radio';

    protected $sTpl = 'radio.twig';

} 