<?php

namespace skewer\build\Component\Forms\Field;


use skewer\build\Component\orm\FormRecord;


class Prototype {

    public $type = '';
    public $name = '';
    public $title = '';
    public $value = '';
    public $desc = '';

    public $items = array();
    public $error = '';

    public $clsLabelPos = 'left';
    public $clsWidth = '1';
    public $clsSpec = '';


    protected $sTpl = 'input.twig';

    protected $aRules = array();


    public function __construct( $sName, $sTitle = '', $mValue = '' ) {
        $this->name = $sName;
        $this->title = $sTitle;
        $this->value = $mValue;
    }

    public function setParams( $oRecord, $aParam = array() ) {

        if ( isSet( $aParam['items'] ) && $aParam['items'] ) {

            $this->items = $aParam['items'];
        }


        if ( isSet( $aParam['method'] ) && $aParam['method'] ) {

            $sMethod = $aParam['method'];

            $this->items = $oRecord->$sMethod();
        }

        //$this->items = $aItems;
    }

    public function getItems() {
        return $this->items;
    }

    public function setError( $sMsg = '' ) {
        $this->error = $sMsg;
    }

    public function getError() {
        return $this->error;
    }


    public function parse() {

        \skTwig::setPath( array( BUILDPATH. 'Page/Forms/templates/fields/' ) );
        \skTwig::assign( 'oField', $this );

        $out = \skTwig::render( $this->sTpl );

        return $out;
    }


    protected function applyRule( FormRecord $oFormRecord, $sRuleName, $aCurRule = array() ) {

        $err = false;

        $sFieldName = $this->name;

        if ( $sRuleName == 'required' ) {

            $sRequiredField = isSet( $aCurRule['requiredField'] ) ? $aCurRule['requiredField'] : '';
            $sValue = isSet( $aCurRule['value'] ) ? $aCurRule['value'] : '';

            if ($sRequiredField){
                if (isset($oFormRecord->$sRequiredField) && $oFormRecord->$sRequiredField == $sValue ){
                    if ( $oFormRecord->$sRequiredField && !(bool)$oFormRecord->$sFieldName )
                        $err = isSet( $aCurRule['msg'] ) ? $aCurRule['msg'] : "Empty field [$sFieldName]";
                }

            }else{
                if ( !(bool)$oFormRecord->$sFieldName )
                    $err = isSet( $aCurRule['msg'] ) ? $aCurRule['msg'] : "Empty field [$sFieldName]";
            }

        } elseif ( $sRuleName == 'minlength' ) {

            $iMinLength = isSet( $aCurRule['length'] ) ? $aCurRule['length'] : 0;
            if (mb_strlen((string)$oFormRecord->$sFieldName) < $iMinLength){
                $err = isSet( $aCurRule['msg'] ) ? $aCurRule['msg'] : "Minimum length of ". $iMinLength ." characters";
            };
        }
        elseif ( $sRuleName == 'email' ) {

            if ( !filter_var( $oFormRecord->$sFieldName, FILTER_VALIDATE_EMAIL ) )
                $err = isSet( $aCurRule['msg'] ) ? $aCurRule['msg'] : "Incorrect email";

        } elseif( $sRuleName == 'compare' ) {

            $sCompField = isSet( $aCurRule['compareField'] ) ? $aCurRule['compareField'] : '';

            if ( !$sCompField || !isSet( $oFormRecord->$sCompField ) )
                return "error: field [$sCompField] not found";

            if ( $oFormRecord->$sFieldName != $oFormRecord->$sCompField )
                $err = isSet( $aCurRule['msg'] ) ? $aCurRule['msg'] : "[$sCompField] field is not equal to [$sFieldName]";

        } elseif( $sRuleName == 'check' ) {

            $sMethod = isSet( $aCurRule['method'] ) ? $aCurRule['method'] : '';

            if ( !$sMethod || !method_exists( $oFormRecord, $sMethod ) )
                return "error: method [$sMethod] not found";

            if ( !( $res = $oFormRecord->$sMethod() ) )
                $err = isSet( $aCurRule['msg'] ) ? $aCurRule['msg'] : "Field [$sFieldName] filled correctly";

        } elseif( $sRuleName == 'captcha' ) {

            if( !$oFormRecord->$sFieldName || !\Captcha::check( $oFormRecord->$sFieldName, $oFormRecord->getHash(), true ) )
                $err = isSet( $aCurRule['msg'] ) ? $aCurRule['msg'] : \Yii::t( 'forms', 'ans_captcha_error');

        } else {
            $err = 'Unknown rule';
        }

        return $err;
    }


    public function validate( FormRecord $oRecord, $aRules ) {

        if ( count( $aRules ) )
            foreach ( $aRules as $aCurRule ) {

                if ( count( $aCurRule ) < 2 )
                    break;

                $aFieldList = array_shift( $aCurRule );

                if ( in_array( $this->name, $aFieldList ) ) {

                    $sRuleName = array_shift( $aCurRule );

                    if ( $sErrMsg = $this->applyRule( $oRecord, $sRuleName, $aCurRule ) ) {
                        return $sErrMsg;
                    }

                }

            }

        return false;
    }


} 