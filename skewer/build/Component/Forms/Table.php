<?php

namespace skewer\build\Component\Forms;


use skewer\build\Component\orm;
use skewer\build\libs\ft;


class Table extends orm\TablePrototype {

    protected static $sTableName = 'forms';

    protected static $sKeyField = 'form_id';


    protected static function initModel() {

        ft\Entity::get( 'forms' )
            ->clear( false )
            ->setPrimaryKey( self::$sKeyField )
            ->setTablePrefix('')
            ->setNamespace( __NAMESPACE__ )
            ->addField( 'form_name', 'varchar(255)', 'forms.form_name' )
            ->addField( 'form_title', 'varchar(255)', 'forms.form_title' )
            ->addField( 'form_handler_type', 'varchar(255)', 'forms.form_handler_type' )
            ->addField( 'form_handler_value', 'varchar(255)', 'forms.form_handler_value' )
            ->addField( 'active', 'int(1)', 'forms.form_active' )
                ->setDefaultVal( 1 )
            ->addField( 'form_captcha', 'int(1)', 'forms.form_captcha' )
            ->addField( 'form_is_template', 'int(1)', 'forms.form_is_template' )
            ->addField( 'form_answer', 'int(1)', 'forms.form_answer' )
            ->addField( 'form_redirect', 'varchar(255)', 'forms.form_redirect' )
            ->addField( 'form_agreed', 'int(1)', 'forms.form_agreed' )
            ->addField( 'form_target', 'varchar(255)', 'forms.form_target' )
            ->addField( 'form_send_crm', 'int(1)', 'forms.form_send_crm' )
            ->save()
        ;

        ft\Entity::get( 'forms_add_data' )
            ->clear( false )
            ->setPrimaryKey( self::$sKeyField )
            ->setTablePrefix('')
            ->setNamespace( __NAMESPACE__ )
            ->addField( 'answer_title', 'varchar(255)', 'forms.answer_title' )
            ->addField( 'answer_body', 'text', 'forms.answer_body' )
            ->addField( 'agreed_title', 'varchar(255)', 'forms.field_agreed_title' )
            ->addField( 'agreed_text', 'text', 'forms.field_agreed_text' )
            ->save()
        ;


        ft\Entity::get( 'forms_section' )
            ->clear( false )
            ->setTablePrefix('')
            ->setNamespace( __NAMESPACE__ )
            ->addField( 'form_id', 'int' )
            ->addField( 'section_id', 'int' )
            ->save()
        ;
    }


    public static function getNewRow( $aData = array() ) {

        $oRow = new Row();
        //ActiveRecord::init( static::$sTableName, static::getModel()->getAllFieldNames(), array() );

        if ( $aData )
            $oRow->setData( $aData );

        return $oRow;
    }


    /**
     * Получение записи формы для раздела
     * @param $iSectionId
     * @return Row
     */
    public static function get4Section( $iSectionId ) {

        $oFormRow = static::find()
            ->join( 'inner', 'forms_section', 'fs', 'fs.form_id=forms.form_id' )
            ->on( 'section_id', $iSectionId )
            ->getOne()
        ;

        return $oFormRow;
    }


    /**
     * Привязка формы к разделу
     * @param int $iFormId ид формы
     * @param int $iSectionId ид раздела
     * @return bool
     */
    public static function link2Section( $iFormId, $iSectionId ) {

        orm\Query::DeleteFrom( 'forms_section' )
            ->where( 'section_id', $iSectionId )
            ->get();

        orm\Query::InsertInto( 'forms_section' )
            ->set( 'form_id', $iFormId )
            ->set( 'section_id', $iSectionId )
            ->get();

        return true;
    }


    /**
     * Получение записи формы по имени
     * @param $sFormName
     * @return Row
     */
    public static function getByName( $sFormName ) {

        $oFormRow = static::find()
            ->where( 'form_name', $sFormName )
            ->getOne()
        ;

        return $oFormRow;
    }

    /**
     * Получение записи формы по id
     * @param int $iFormId
     * @return Row
     */
    public static function getById( $iFormId ) {

        return static::find()
            ->where( self::$sKeyField, $iFormId )
            ->getOne()
        ;

    }


    /**
     * Сборка сущности формы
     * @param $iFormId
     * @param array $aData
     * @return Entity
     * @throws \Exception
     */
    public static function build( $iFormId, $aData = array() ) {

        $oFormRow = static::find( $iFormId );

        if ( !( $oFormRow  instanceof Row ) )
            throw new \Exception( "Form not found [id=$iFormId]" );

        $oForm = new Entity( $oFormRow, $aData );

        return $oForm;
    }


    /**
     * Виджет для вывода текста для типа формы
     * @param orm\ActiveRecord $oItem
     * @param $sField
     * @return string
     */
    public static function getTypeTitle( $oItem, $sField ) {

        switch ( $oItem->getVal( $sField ) ) {

            case 'toMail':
                $out = \Yii::t( 'forms', 'send_to_mail');
                break;
            case 'toMethod':
                $out = \Yii::t( 'forms', 'send_to_method');
                break;
            case 'toBase':
                $out = \Yii::t( 'forms', 'send_to_base');
                break;
            default:
                $out = $oItem->getVal( $sField );
        }
        return $out;
    }


    public static function getTypeList() {
        return array(
            'toMail' => \Yii::t( 'forms', 'send_to_mail'),
            'toBase' => \Yii::t( 'forms', 'send_to_base'),
            'toMethod' => \Yii::t( 'forms', 'send_to_method')
        );
    }


} 