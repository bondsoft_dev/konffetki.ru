<?php

namespace skewer\build\Component\orm;

use skewer\build\Component\Forms\Field as FormFields;


/**
 * Прототип для сущностей типа FormRecord
 * Class FormRecord
 * @package skewer\build\Component\orm
 */
abstract class FormRecord extends ActiveRecord {

    /** @var FormFields\Prototype[] Список объектов полей вывода */
    //private $aFieldList = array();

    abstract public function rules();

    abstract public function getLabels();

    public function getEditors() {
        return array();
    }

    public function save() {
        return true;
    }


    /**
     * Проверка значения полей на валидность
     * @return bool
     */
    public function isValid() {

        $aRules = $this->rules();

        $aFields = $this->getFields();

        foreach ( $aFields as $sFieldName => $oField ) {

            if ( $sErrMsg = $oField->validate( $this, $aRules ) ) {
                $this->setFieldError( $sFieldName, $sErrMsg );
                return false;
            }

        }

        return true;
    }


    /**
     * Создать новое поле для формы
     * @param string $sEditorType Тип поля
     * @param string $sFieldName Имя поля
     * @param string $sTitle Надпись
     * @param string $sValue Значение
     * @return FormFields\Prototype
     */
    protected function getNewField( $sEditorType, $sFieldName, $sTitle = '', $sValue = '' ) {

        $sClassName = 'skewer\\build\\Component\\Forms\\Field\\' . ucfirst( $sEditorType );

        //\skLogger::dump($sClassName, class_exists( $sClassName) );

        $oField = new $sClassName( $sFieldName, $sTitle, $sValue );

        return $oField;
    }


    /**
     * Возвращает набор объектов вывода для полей формы
     * @return FormFields\Prototype[]
     */
    public function getFields() {

        $aData = $this->getData();
        $aLabels = $this->getLabels();
        $aErrors = $this->getErrorList();
        $aEditors = $this->getEditors();
        $aRules = $this->rules();

        $aOut = array();

        foreach ( $aData as $sFieldName => $sValue ) {

            // find editor type
            $aParam = array();
            if ( isSet( $aEditors[$sFieldName] ) ) {

                if ( is_array( $aEditors[$sFieldName] ) ) {
                    $aParam = $aEditors[$sFieldName];
                    $sEditorType = array_shift( $aParam );
                } else
                    $sEditorType = $aEditors[$sFieldName];

            } else {
                $sEditorType = 'input';
            }

            $sTitle = isSet( $aLabels[$sFieldName] ) ? $aLabels[$sFieldName] : $sFieldName;

            $oField = $this->getNewField( $sEditorType, $sFieldName, $sTitle, $sValue );

            // error
            $oField->setParams( $this, $aParam );
            if ( isSet( $aErrors[$sFieldName] ) )
                $oField->setError( $aErrors[$sFieldName] );

            // required
            foreach ( $aRules as $aRule )
                if ( count($aRule) > 1 && $aRule[1] == 'required' && in_array( $sFieldName, $aRule[0] ) )
                    $oField->required = 1;

            // add field
            $aOut[$sFieldName] = $oField;
        }

        return $aOut;
    }


    /**
     * Получение хеш-имени для формы
     * @return string
     */
    public function getHash() {
        $sClassName = get_class($this);
        if ( strrpos( $sClassName, '\\' ) !== false )
            $sClassName = substr( $sClassName, strrpos( $sClassName, '\\' )+1 );
        return $sClassName;
    }


    /**
     * Заполнение полей по массивы данных
     * @param array $aData
     * @return bool
     */
    public function load( $aData = array() ) {

        if ( empty( $aData ) )
            return false;

        if ( !isSet( $aData['hash'] ) || $aData['hash']!=$this->getHash() )
            return false;

        parent::load( $aData );

        return true;
    }

    /**
     * Вывод формы
     * @param string $sTpl Шаблон для парсинга
     * @param string $sDir Текущая директория
     * @param array $aParams Дополнительные параметры для шаблона
     * @return string
     */
    public function getForm( $sTpl = '', $sDir = '', $aParams = array() ) {

        if ( $sTpl ) {

            \skTwig::setPath( array( $sDir . '/templates/' ) );
        } else {

            $sTpl = 'common.twig';
            \skTwig::setPath( array( BUILDPATH. 'Page/Forms/templates/' ) );
        }

        /* Получить список хелперов для шаблонов */
        $aParserHelpers = \skParser::getParserHelpers();
        if ( count( $aParserHelpers ) )
            foreach ( $aParserHelpers as $sHelperName => $oHelperObject )
                \skTwig::assign( $sHelperName, $oHelperObject );

        \skTwig::assign( 'oForm', $this );

        if ( count( $aParams ) )
            foreach( $aParams as $sKey => $mVal )
                \skTwig::assign( $sKey, $mVal );

        \skTwig::assign( 'formHash', $this->getHash() );

        return \skTwig::render( $sTpl );
    }

} 