<?php

namespace skewer\build\Component\orm;

use skewer\build\libs\ft;


/**
 * Class MagicTable
 * @package skewer\build\Component\orm
 */
class MagicTable {

    /** @var ft\Model Модель */
    protected $oModel = null;


    protected function setModel( $oModel ) {
        $this->oModel = $oModel;
    }


    /**
     * Генератор объекта для модели
     * @param $oModel
     * @return MagicTable
     */
    public static function init( $oModel ) {

        $oTable = new self();

        $oTable->setModel( $oModel );

        return $oTable;
    }


    public function getTableName() {

        return $this->oModel->getTableName();
    }

    /**
     * Отдает имя сущности
     * @return string
     */
    public function getName() {
        return $this->oModel->getName();
    }


    public function getKeyField() {
        return $this->oModel->getPrimaryKey();
    }


    /**
     * Поиск записей
     * @param null $id
     * @return bool|ActiveRecord|\skewer\build\Component\orm\state\StateSelect
     */
    public function find( $id = null ) {

        if ( !is_null( $id ) ) {
            $oRow = static::getNewRow();
            $res = Query::SelectFrom( $this->getTableName(), $oRow )->where( $this->getKeyField(), $id )->getOne();
        } else {
            $oRow = static::getNewRow();
            $res = Query::SelectFrom( $this->getTableName(), $oRow );
        }

        return $res;
    }


    /**
     * Удаление записей
     * @param null $id
     * @return state\StateDelete
     */
    public function delete( $id = null ) {

        if ( !is_null( $id ) ) {
            $res = Query::DeleteFrom( $this->getTableName() )->where( $this->getKeyField(), $id )->get();
        } else {
            $res = Query::DeleteFrom( $this->getTableName() );
        }

        return $res;
    }


    /**
     * Удаление записей по набору полей
     * @param array $set
     * @param array $fields
     * @return bool
     */
    public function update( $set = [], $fields = [] ) {

        $query = Query::UpdateFrom( $this->getTableName() );

        foreach ( $set as $field => $value )
            $query->set( $field, $value );

        if ( count($fields) )
            foreach ( $fields as $field => $value )
                $query->where( $field, $value );

        return $query->get();
    }


    /**
     * Получить экземлять записи для таблицы
     * @param array $aData
     * @return ActiveRecord
     */
    public function getNewRow( $aData = array() ) {

        // todo брать из перенемнной?
        $oRow = ActiveRecord::getByFTModel( $this->oModel );

        if ( $aData )
            $oRow->setData( $aData );

        return $oRow;
    }


} 