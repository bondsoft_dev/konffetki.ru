<?php

namespace skewer\build\Component\orm\service;

use skewer\build\Component\orm;
use yii\db\DataReader;

/**
 * Адаптер для прямых запросов к базе данных и получения результатов
 * Class DataBaseAdapter
 * @package skewer\build\Component\orm\service
 */
class DataBaseAdapter {

    /** @var DataReader Команда PDO */
    private $oCmd = null;

    private $sError = '';

    function __construct() {

    }


    public function getError() {
        return $this->sError;
    }

    public function applyQuery( $sQuery, $aData = array() ) {

        $this->sError = '';

        $i = 1;
        while ( ($pos = strpos( $sQuery, '?')) !== false ) {
            $label = 'vl' . $i;
            $sQuery = substr( $sQuery, 0, $pos ) . ':' .$label . substr( $sQuery, $pos + 1 );
            $aData[':'.$label] = array_shift( $aData );
            $i++;
        }

        $newData = [];
        foreach($aData as $k=>$item){
            if ($k[0]!=':') $k = ':'.$k;
            $newData[$k] = $item;
        }

        $aData = $newData;

        $this->oCmd = \Yii::$app->db->createCommand($sQuery,$aData)->query();
        return $this;
    }


    public function rowsCount() {
        return $this->oCmd->count();
    }


    /**
     * @deprecated использовать запросник Yii
     * @param int $iMode режим работы выборки \PDO::FETCH_ASSOC \ \PDO::FETCH_NUM
     * @return mixed|null
     */
    public function fetchArray($iMode = \PDO::FETCH_ASSOC) {
        if ($iMode)
            $this->oCmd->setFetchMode($iMode);
        return $this->oCmd->read();
    }


    /**
     * @deprecated использовать запросник Yii
     * @return int
     */
    public function affectedRows() {
        return $this->oCmd->rowCount;
    }

    /**
     * @deprecated использовать запросник Yii
     * @return string
     */
    public function lastId() {
        return \Yii::$app->db->lastInsertID;
    }

    /**
     * @deprecated использовать запросник Yii
     * @param $sField
     * @return null
     */
    public function getValue( $sField ) {

        $result = $this->oCmd->read();
        if (!$result) return null;

        if (isset($result[$sField])) return $result[$sField];

        return null;
    }


} 