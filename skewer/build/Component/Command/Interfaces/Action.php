<?php
/**
 * Интерфейс работы подчиненного элемента прототипа "Команда" (Command)
 */

namespace skewer\build\Component\Command\Interfaces;

interface Action {

    /**
     * Выполнение команды
     * @throws \Exception
     */
    function execute();

    /**
     * Откат команды
     */
    function rollback();

}
