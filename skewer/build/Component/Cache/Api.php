<?php

namespace skewer\build\Component\Cache;


class Api {

    private static $useMemCached = false;

    protected static function useMemCached() {
        return static::$useMemCached;
    }

    public static function set( $sName, $mValue ) {
        Storage::set( $sName, $mValue );
    }


    public static function get( $sName ) {
        return Storage::get( $sName );
    }


} 