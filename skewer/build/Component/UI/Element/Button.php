<?php

namespace skewer\build\Component\UI\Element;

/**
 * Класс для описания кнопок интерфейса
 * @package skewer\build\Component\UI
 */
class Button extends ButtonPrototype {

    /** @var string подтверждение */
    protected $sConfirm = '';

    /** @var bool флаг проверки наличия изменений в форме */
    protected $bUseDirtyChecker = true;

    /**
     * Запрос подтверждения
     * @return string
     */
    public function getConfirm() {
        return $this->sConfirm;
    }

    /**
     * Установка подтверждения
     * @param string $sConfirm
     * @return \ExtDockedPrototype
     */
    public function setConfirm( $sConfirm ) {
        $this->sConfirm = $sConfirm;
        return $this;
    }

    /*
     * Проверка наличия изменений в форме
     */

    /**
     * Отдает статус наличия проверки изменений в форме
     * @return bool
     */
    public function getDirtyChecker() {
        return $this->bUseDirtyChecker;
    }

    /**
     * Устанавливает флаг проверки изменений в форме
     * @param bool $bVal значение
     * @return \ExtDockedPrototype
     */
    public function setDirtyChecker( $bVal=true ) {
        $this->bUseDirtyChecker = (bool)$bVal;
        return $this;
    }

    /**
     * Снимает флаг проверки изменений в форме
     * @return \ExtDockedPrototype
     */
    public function unsetDirtyChecker() {
        $this->bUseDirtyChecker = false;
        return $this;
    }

}