ModuleConstructor
=================

Класс для автоматизированного построения модулей

```php
class Module extends Adm\Tree\ModulePrototype {

    /**
     * @var string - Имя модуля
     */
    protected $sTabName = 'Новости';

    // число элементов на страницу
    public $iOnPage = 20;

    // текущий номер страницы ( с 0, а приходит с 1 )
    public $iPage = 0;

    public $iSectionId;

    /**
     * Метод, выполняемый перед action меодом
     * @throws \ModuleAdminErrorException
     * @return bool
     */
    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt('page');

        // id текущего раздела
        $this->iSectionId = $this->getInt('sectionId');

        // проверить права доступа
        if ( !\CurrentAdmin::canRead($this->iSectionId) )
            throw new \ModuleAdminErrorException( 'accessDenied' );

    }


    /**
     * Возвращает имя текущей каталожной карточки
     * @return string
     */
    private function getCardName() {
        return 'News';
    }


    public function execute() {

        $this->bUseBuilder = true;

        $sCatalogCardName = $this->getCardName();
        $this->oBuilder = new ModuleConstructor( $this, $sCatalogCardName );

        return $this->findAction();
    }

    public function actionInit() {
        return $this->actionList();
    }

    public function actionList() {

        // установка заголовка
        $this->setPanelName( 'Список новостей' );

        // построение интерфейса
        $this->oBuilder->getListState()
            ->setFields( 'name,title' )
            ->addButton( 'new', '', 'Создать новость',  'icon-add' )
            ->addRowButton( '_upd','icon-edit','edit','edit_form' )
            ->addRowButton( '_del','icon-delete','del','delete' )
        ;

        return $this->oBuilder->build();
    }


    public function actionShow() {}


    public function actionEdit() {

        // установка заголовка
        $this->setPanelName( 'Редактор новости' );

        // построение интерфейса
        $this->oBuilder->getEditState()
            ->setFields( '' )
            ->addButton( 'upd', '', 'Сохранить',  'icon-save' )
            ->addButton( 'list', '', 'Назад',  'icon-cancel' )
            ->addButton( 'del', '', 'Удалить', 'icon-delete' )
        ;

        return $this->oBuilder->build();
    }


    public function actionNew() {

        // установка заголовка
        $this->setPanelName( 'Создание новости' );

        // построение интерфейса
        $this->oBuilder->getNewState()
            ->setDefault()
        ;

        return $this->oBuilder->build();
    }


    public function actionUpd() {

        $this->oBuilder->getUpdState()
            ->addValidator('name','set')
            ->addValidator('name','unique')
        ;

        if ( $this->oBuilder->update() === true ) {
            return $this->actionList();
        } else {
            return $this->actionEdit();
        }
    }


    public function actionAdd() {

        $this->oBuilder->getAddState();

        if ( $this->oBuilder->update() === true ) {
            return $this->actionList();
        } else {
            return $this->actionNew();
        }
    }


    public function actionDel() {

        $this->oBuilder->getDelState();

        $this->oBuilder->update();

        return $this->actionList();
    }

}
```