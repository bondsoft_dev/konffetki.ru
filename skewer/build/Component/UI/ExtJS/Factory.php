<?php

namespace skewer\build\Component\UI\ExtJS;

use skewer\build\Component\UI;

/**
 * Фабрика интерфейсных объектов для ExtJS
 * Class Factory
 * @package skewer\build\Component\UI\ExtJS
 */
class Factory implements UI\FactoryInterface {

    /**
     * Отдает объект для построения спискового интерфейса
     * @return UI\State\ListInterface
     */
    function getList() {
        return new \ExtList();
    }

    /**
     * Отдает объект для построения интерфейса редактирования
     * @return UI\State\EditInterface
     */
    function getEdit() {
        return new \ExtForm();
    }

    /**
     * Отдает объект для построения интерфейса отображения данных
     * @return UI\State\ShowInterface
     */
    function getShow() {
        return new \ExtShow();
    }
}