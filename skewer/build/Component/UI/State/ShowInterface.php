<?php

namespace skewer\build\Component\UI\State;
use skewer\build\libs\ft;

/**
 * Интерфейс для построения формы отображения данных
 * Class ListInterface
 * @package skewer\build\Component\UI
 */
interface ShowInterface extends StateInterface {

    /**
     * Устанавливает значения для вывода
     * @param array|ft\ArPrototype $aValues набор пар имя поля - значение
     */
    public function setValues($aValues);

}