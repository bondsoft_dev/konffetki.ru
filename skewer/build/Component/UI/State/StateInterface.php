<?php

namespace skewer\build\Component\UI\State;

use skewer\build\libs\ft;
use skewer\build\Component\UI;

/**
 * Определение обязательных методов состояния пользовательского интерфейса
 * который может отображать данные
 * @package skewer\build\Component\UI
 */
interface StateInterface extends BaseInterface {

    /**
     * Задает набор полей по FT модели
     * @param ft\Model $oModel описание модели
     * @param string $mColSet набор колонок для вывода
     * @return void
     */
    public function setFieldsByFtModel( ft\Model $oModel, $mColSet='' );

}