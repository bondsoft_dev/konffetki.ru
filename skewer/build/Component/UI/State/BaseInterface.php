<?php

namespace skewer\build\Component\UI\State;

use skewer\build\libs\ft;
use skewer\build\Component\UI;
use skewer\build\Cms;

/**
 * Базовая часть интерфейсов для всех состояний
 */
interface BaseInterface {

    /**
     * Добавляет кнопку в интерфейс
     * @param UI\Element\Button $oButton
     * @return void
     */
    public function addButton( UI\Element\Button $oButton );

    /**
     * Установить название компонента
     * @param string $sTitle
     */
    public function setTitle( $sTitle );

    /**
     * Меняет заголовок основной панели
     * @param string $sNewTitle
     */
    public function setPanelTitle($sNewTitle);

    /**
     * Задает инициализационный  массив для атопостроителя интерфейсов
     * @param Cms\Frame\ModulePrototype $oModule - ссылка на вызвавший объект
     */
    public function setInterfaceData( Cms\Frame\ModulePrototype $oModule );

    /**
     * Задает массив со служебными данными для проброса
     * Этот массив вернется с посылкой
     * @param array $aData - массив данных
     * @return void
     */
    public function setServiceData( $aData );

}