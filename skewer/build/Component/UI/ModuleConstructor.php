<?php

namespace skewer\build\Component\UI;

use skewer\build\Adm;
use skewer\build\Component\UI\Action;
use skewer\core\Component\Config\Exception;
use skewer\build\libs\ft as ft;
use skewer\build\Cms;

/**
 * Сборщик интерфейсных состояний для модуля
 * @class ModuleConstructor
 * @project Skewer
 * @package skewer\build\Component\ModuleConstructor
 *
 * @author kolesnikov, $Author: $
 * @version $Revision:  $
 * @date $Date: $
 * @deprecated
 * todo переосмыслить и переписать
 */
class ModuleConstructor {

    private $sDefAlias = '';

    private $sAlias = '';

    /** @var Cms\Tabs\ModulePrototype  */
    private $oModule = null;

    /** @var Action\Prototype  */
    private $oState = null;

    /** @var ft\QueryPrototype  */
    private $oQuery = null;

    /** @var array Список ошибок */
    protected $aErrorList = array();

    /**
     * @param Cms\Tabs\ModulePrototype $oModule Модуль интерфейса
     * @param string $sDefAlias Сущность для вывода по умолчанию
     * @param string $sModuleNamespace Модуль в котором лежит модель
     * @deprecated
     */
    function __construct( $oModule = null, $sDefAlias = '', $sModuleNamespace = '' ) {

        $this->sDefAlias = $sDefAlias;

        $this->sAlias = $oModule->getStr('alias', $sDefAlias);

        $this->oModule = $oModule;

        $this->oQuery = ft\Cache::getModelTable( $this->sAlias, $sModuleNamespace );
    }

    /**
     * Отдает объект для работы с запросами
     * @return ft\QueryPrototype
     */
    public function getQuery () {
        return $this->oQuery;
    }

    /**
     * Возвращает имя метода для текущего состояния
     * @param $sCmd
     * @return string
     */
    public function getActionName( $sCmd = 'list' ) {

        if ( $sCmd == 'init' )
            $sCmd = 'list';

        return sprintf(
            'action%s%s',
            $this->sAlias ? ucfirst( $this->sAlias ) : '',
            ($sCmd && $sCmd!='initTab' ) ? ucfirst( $sCmd ) : 'List'
        );
    }

    /**
     * Возвращает имя метода для обычного модуля
     * @param $sCmd
     * @return string
     */
    public function getSimpleActionName( $sCmd ) {
        return sprintf(
            'action%s',
            $sCmd ? ucfirst( $sCmd ) : ''
        );
    }

    /**
     * Отдает id записи
     * @return int
     */
    private function getItemId() {
        $aData = $this->oModule->get('data');
        return isSet($aData[$this->oQuery->primaryKey()]) ? $aData[$this->oQuery->primaryKey()] : 0;
    }

    /**
     * Отдает сборщик интерфейсного состояния "список"
     * @return Action\ExtList
     */
    public function getListState() {
        $this->oState = new Action\ExtList( $this->sAlias );
        $this->oState->setQuery( $this->oQuery );
        return $this->oState;
    }

    /**
     * Отдает сборщик интерфейсного состояния "форма отображнения"
     * @return Action\ExtShow
     */
    public function getShowState() {
        $this->oState = new Action\ExtShow( $this->sAlias, $this->getItemId() );
        $this->oState->setQuery( $this->oQuery );
        return $this->oState;
    }

    /**
     * Отдает сборщик интерфейсного состояния "форма добавления"
     * @return Action\ExtNew
     */
    public function getNewState() {
        $this->oState = new Action\ExtNew( $this->sAlias );
        $this->oState->setQuery( $this->oQuery );
        return $this->oState;
    }

    /**
     * Отдает сборщик интерфейсного состояния "форма для редактирования"
     * @return Action\ExtEdit
     */
    public function getEditState() {
        $this->oState = new Action\ExtEdit( $this->sAlias, $this->getItemId() );
        $this->oState->setQuery( $this->oQuery );
        return $this->oState;
    }

    /**
     * Отдает сборщик интерфейсного состояния "добавление"
     * @return Action\ExtAdd
     */
    public function getAddState() {
        $this->oState = new Action\ExtAdd( $this->sAlias, $this->getItemId() );
        $this->oState->setQuery( $this->oQuery );
        return $this->oState;
    }

    /**
     * Отдает сборщик интерфейсного состояния "обновление"
     * @return Action\ExtUpd
     */
    public function getUpdState() {
        $this->oState = new Action\ExtUpd( $this->sAlias, $this->getItemId() );
        $this->oState->setQuery( $this->oQuery );
        return $this->oState;
    }

    /**
     * Отдает сборщик интерфейсного состояния "удаление"
     * @return Action\ExtDel
     */
    public function getDelState() {
        $this->oState = new Action\ExtDel( $this->sAlias, $this->getItemId() );
        $this->oState->setQuery( $this->oQuery );
        return $this->oState;
    }


    /**
     * Собирает интерфейс по установленнам данным
     * @throws \skewer\core\Component\Config\Exception
     * @return int
     */
    public function build() {

        if ( ! ($this->oState instanceof Action\Prototype) )
            throw new Exception('Не задано состояние для отображения');

        $this->oState->buildState( $this->oModule, $this->oQuery, $this->aErrorList );

        return psComplete;
    }

    /**
     * Обработка состояния движения данных
     * @throws \skewer\core\Component\Config\Exception
     * @return bool
     */
    public function update() {

        if ( ! ($this->oState instanceof Action\Prototype) )
            throw new Exception('Не задано состояние для отображения');

        $bResult = $this->oState->buildState( $this->oModule, $this->oQuery );

        // сохраняем ошибки, которые произошли в текущем состоянии
        if ( !$bResult )
            $this->setError( $this->oState->getErrorList() );

        return $bResult;
    }

    /**
     * Отдает список ошибок
     * @return array
     */
    public function getError() {
        return $this->aErrorList;
    }


    /**
     * Добавляет новые сообщения об ошибках
     * @param array $aErrorList
     */
    public function setError( $aErrorList = array() ) {
        $this->aErrorList = array_merge($this->aErrorList, $aErrorList);
    }

}