<?php

namespace skewer\build\Component\UI\Form;
use skewer\build\libs\ft;

/**
 * Class MultiCheck
 * @package skewer\build\Component\UI\Form
 */
class MultiCheck extends Select {

    public function __construct($sName, $sTitle = '', $aParams = array()) {
        parent::__construct($sName, $sTitle, $aParams);
        $this->setEditor( ft\Editor::MULTICHECK );
    }

    /**
     * Задает значение
     * @param string|string[] $mValue
     */
    public function setValue( $mValue ) {

        // привести к массиву
        if ( !is_array($mValue) )
            $mValue = explode( ',', $mValue );

        parent::setValue( $mValue );

    }

}