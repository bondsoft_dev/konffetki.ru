<?php

namespace skewer\build\Component\UI\Form;

use skewer\build\libs\ft;
use yii\web\ServerErrorHttpException;

/**
 * Класс для создания поля - выпадающего списка
 * Class Select
 * @package skewer\build\Component\UI\Form
 */
class Select extends Field {

    /**
     * Набор элементов
     * @var array
     */
    protected $aItems = array();

    /**
     * @param $sName
     * @param string $sTitle
     * @param array $aParams
     * @throws ServerErrorHttpException
     */
    public function __construct( $sName, $sTitle='', $aParams=array() ) {
        if ( !is_array($aParams) )
            throw new ServerErrorHttpException( 'Набор параметров должен передаваться массивом' );
        parent::__construct( $sName, $sTitle, ft\Editor::SELECT, $aParams );
    }

    public static function makeByFt( ft\model\Field $oFtField ) {
        return new static(
            $oFtField->getName(),
            $oFtField->getTitle()
        );
    }

    /**
     * Задает набор элементов
     * @param array $aItems
     */
    public function setItems( $aItems ) {
        $this->aItems = $aItems;
    }

    /**
     * Отдает список выходных параметров
     * @return array
     */
    public function getOutParamList() {

        $aBaseParams = parent::getOutParamList();
        $aSelectParams = \ExtForm::getDesc4SelectFromArray( $this->aItems );

        $aOut = array_merge( $aBaseParams, $aSelectParams );
        return $aOut;

    }

} 