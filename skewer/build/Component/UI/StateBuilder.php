<?php

namespace skewer\build\Component\UI;

use skewer\build\Component\Catalog\Card;
use skewer\build\Component\Catalog\model\FieldRow;
use skewer\build\Component\Gallery\Format;
use skewer\build\libs\ft\Editor;
use yii\helpers\ArrayHelper;
use yii\web\ServerErrorHttpException;


/**
 * Сборщик интерфейсных моделей
 */
class StateBuilder {

    /** @var \ExtList  */
    private $oForm = null;

    private $aModel = array();

    private $aWidgetList = array();

    /**
     * Конструктор
     * @param null $oInterface
     */
    function __construct( $oInterface = null ) {

        if ( is_null($oInterface) )
            $this->oForm = $oInterface;
    }

    /**
     * Возвращает текущую модель
     * @return array
     */
    public function getModel() {
        return $this->aModel;
    }

    /**
     * Отдает интерфейс формы
     * @return \ExtList|\ExtForm
     */
    public function getForm() {
        return $this->oForm;
    }

    /**
     * Очищает модель
     * @return $this
     * @deprecated
     */
    public function clear() {
        $this->aModel = array();
        return $this;
    }


    /**
     * Создание нового списки
     * @return StateBuilder
     */
    public static function newList() {

        $oState = new self();
        $oState->aModel = [];
        $oState->oForm = new \ExtList();

        return $oState;
    }


    /**
     * Создание новой формы редактирования
     * @return StateBuilder
     * todo rename createForm
     */
    public static function newEdit() {

        $oState = new self();
        $oState->aModel = [];
        $oState->oForm = new \ExtForm();

        return $oState;
    }


    /**
     * Создаем новую форму типа список
     * @return $this
     * @deprecated лучше использовать self::newList()
     */
    function createFormList() {
        $this->clear();
        $this->oForm = new \ExtList();
        return $this;
    }


    /**
     * Создаем новую форму редактор
     * @return $this
     * @deprecated лучше использовать self::newEdit()
     */
    function createFormEdit() {
        $this->clear();
        $this->oForm = new \ExtForm();
        return $this;
    }

    /**
     * Устанавливает флаг использования спец директории для изображений модуля
     * @param int $iId
     */
    public function useSpecSectionForImages( $iId=0 ) { // fixme
        /** @var \ExtForm $form */
        $form = $this->oForm;
        $form->useSpecSectionForImages($iId);
    }


    /**
     * Добавление поля в форму
     * @param string $sName Имя поля
     * @param string $sTitle Заголовок поля
     * @param string $sDataType Тип в базе данных
     * @param string $sViewType Тип при выводе
     * @param array $aAddParams Доп. параметры при выводе
     * @return $this
     * @deprecated use function ->field<type>()
     */
    public function addField( $sName, $sTitle, $sDataType, $sViewType, $aAddParams = array() ) {

        $this->aModel[$sName] = array(
            "name" => $sName,
            "type" => $sDataType,
            "view" => $sViewType,
            "title" => $sTitle,
            "default" => "",
        );

        if ( count($aAddParams) )
            foreach ( $aAddParams as $sKey => $aVal )
                $this->aModel[$sName][$sKey] = $aVal;

        return $this;
    }


    /**
     * Добавление поля в форму, если выполняется условие $bCondition
     * @param string $sName Имя поля
     * @param string $sTitle Заголовок поля
     * @param string $sDataType Тип в базе данных
     * @param string $sViewType Тип при выводе
     * @param array $aAddParams Доп. параметры при выводе
     * @param bool $bCondition Флаг добавления поля в форму
     * @return $this
     * @deprecated
     * @todo каким методом пользоваться, если этот deprecated?
     */
    public function addFieldIf( $sName, $sTitle, $sDataType, $sViewType, $aAddParams = array(), $bCondition = true ) {

        if ( $bCondition )
            $this->addField( $sName, $sTitle, $sDataType, $sViewType, $aAddParams );

        return $this;
    }


    public function addSpecField( $sName, $sTitle, $sClass, $mValue ) {

        $this->getForm()->addLibClass( $sClass );

        $this->aModel[$sName] = array(
            "name" => $sName,
            "view" => 'specific',
            "title" => $sTitle,
            "extendLibName" => $sClass,
            "value" => $mValue
        );

        return $this;
    }


    /**
     * Добавление поля в форму
     * @param string $sName Имя поля
     * @param string $sTitle Заголовок поля
     * @param string $sDataType Тип в базе данных
     * @param string $sViewType Тип при выводе
     * @param string $sValue Значение
     * @param array $aAddParams Доп. параметры при выводе
     * @return $this
     * @deprecated use ->fieldWithValue()
     */
    public function addFieldWithValue( $sName, $sTitle, $sDataType, $sViewType, $sValue, $aAddParams = array() ) {

        $this->aModel[$sName] = array(
            "name" => $sName,
            "type" => $sDataType,
            "view" => $sViewType,
            "title" => $sTitle,
            "value" => $sValue
        );

        if ( count($aAddParams) )
            foreach ( $aAddParams as $sKey => $aVal )
                $this->aModel[$sName][$sKey] = $aVal;

        return $this;
    }

    /**
     * Добавить выпадающее поле
     * @param $sName
     * @param $sTitle
     * @param $sDataType
     * @param array $aData
     * @param array $aAddParams
     * @return $this
     */
    public function addSelectField( $sName, $sTitle, $sDataType, $aData = array(), $aAddParams = array() ) {

        $this->aModel[$sName] = array_merge(
            array(
                "name" => $sName,
                "type" => $sDataType,
                "title" => $sTitle,
                "default" => "",
            ),
            \ExtForm::getDesc4SelectFromArray( $aData )
        );

        if ( count($aAddParams) )
            foreach ( $aAddParams as $sKey => $aVal )
                $this->aModel[$sName][$sKey] = $aVal;

        return $this;
    }

    /**
     * Добавляет поле - множество галочек
     * @param string $sName
     * @param string $sTitle
     * @param array $aData
     * @param array $aValues
     * @param array $aAddParams
     * @return $this
     * @deprecated use ->fieldMultiSelect()
     */
    public function addMultiSelectField( $sName, $sTitle, $aData, $aValues, $aAddParams = array() ) {

        $oField = new Form\MultiCheck(
            $sName,
            $sTitle
        );

        $oField->setItems( $aData );
        $oField->setValue( $aValues );

        foreach ( $aAddParams as $sPName => $sPVal )
            $oField->setOutParam( $sPName, $sPVal );

        $aParams = array_merge( array(
            'name' => $oField->getName(),
            'title' => $oField->getTitle(),
            'view' => $oField->getEditor()
        ), $oField->getOutParamList() );

        // создаем объект из массивов
        $oIfaceField = \ExtFT::createFieldByUi( $aParams, $oField );

        $this->aModel[$sName] = $oIfaceField;

        return $this;

    }

    private function setField( $name, $fields = [], $params = [] ) {

        $this->aModel[$name] = $fields;

        if ( is_array($params) && $params )
            foreach ( $params as $key => $val ) {

                while ( ($pos = strrpos($key, '.')) !== false ) {
                    $curKey = substr( $key, $pos + 1 );
                    $newVal = [$curKey => $val];
                    $val = $newVal;
                    $key = substr($key, 0, $pos);
                }
                $this->aModel[$name][$key] = $val;

            }

    }

    public function field( $name, $title, $tp = 'i', $editor = 'string', $params = [] ) {

        $this->setField(
            $name,
            [
                "name" => $name,
                "type" => $tp,
                "view" => $editor,
                "title" => $title,
                "default" => "",
            ],
            $params
        );

        return $this;
    }


    public function fieldByEntity( FieldRow $field, $params = [] ) {

        // для галереи передаем дефолтный формат
        if ( $field->editor == Editor::GALLERY )
            $params['gallery_format'] = $field->modificator ? : Format::TYPE_CATALOG;

        // если есть группа, передаем ее имя
        if ( $field->group ) {
            $group = Card::getGroup( $field->group );
            $params['groupTitle'] = ArrayHelper::getValue($group, 'title', '');
        }

        $this->field( $field->name, $field->title, 's', $field->editor, $params );
        return $this;
    }


    public function fieldHide( $name, $title, $tp = 'i', $params = [] ) {

        $this->setField(
            $name,
            [
                "name" => $name,
                "type" => $tp,
                "view" => 'hide',
                "title" => $title,
                "default" => "",
            ],
            $params
        );

        return $this;
    }

    public function fieldShow( $name, $title, $tp = 's', $params = [] ) {

        $this->setField(
            $name,
            [
                "name" => $name,
                "type" => $tp,
                "view" => 'show',
                "title" => $title,
                "default" => "",
            ],
            $params
        );

        return $this;
    }


    public function fieldString( $name, $title, $params = [] ) {

        $this->setField(
            $name,
            [
                "name" => $name,
                "type" => 's',
                "view" => 'string',
                "title" => $title,
                "default" => "",
            ],
            $params
        );

        return $this;
    }


    public function fieldText( $name, $title, $params = [] ) {

        $this->setField(
            $name,
            [
                "name" => $name,
                "type" => 's',
                "view" => 'text',
                "title" => $title,
                "default" => "",
            ],
            $params
        );

        return $this;
    }


    public function fieldCheck( $name, $title, $params = [] ) {

        $this->setField(
            $name,
            [
                "name" => $name,
                "type" => 'i',
                "view" => 'check',
                "title" => $title,
                "default" => "",
            ],
            $params
        );

        return $this;
    }


    public function fieldNumber( $name, $title, $params = [] ) {

        $this->setField(
            $name,
            [
                "name" => $name,
                "type" => 'i',
                "view" => 'string',
                "title" => $title,
                "default" => "",
            ],
            $params
        );

        return $this;
    }


    public function fieldInt( $name, $title, $params = [] ) {

        $this->setField(
            $name,
            [
                "name" => $name,
                "type" => 'i',
                "view" => 'int',
                "title" => $title,
                "default" => "",
            ],
            $params
        );

        return $this;
    }



    public function fieldSelect( $name, $title, $data, $params = [] ) {

        $this->setField(
            $name,
            array_merge(
                [
                    "name" => $name,
                    "type" => 's',
                    "title" => $title,
                    "default" => "",
                ],
                \ExtForm::getDesc4SelectFromArray( $data )
            ),
            $params
        );

        return $this;
    }

    public function fieldMultiSelect( $name, $title, $data, $values, $params = [] ) {

        $oField = new Form\MultiCheck( $name, $title );

        $oField->setItems( $data );
        $oField->setValue( $values );

        foreach ( $params as $sPName => $sPVal )
            $oField->setOutParam( $sPName, $sPVal );

        $aParams = array_merge(
            [
                'name' => $oField->getName(),
                'title' => $oField->getTitle(),
                'view' => $oField->getEditor()
            ],
            $oField->getOutParamList()
        );

        $this->aModel[$name] = \ExtFT::createFieldByUi( $aParams, $oField );

        return $this;

    }


    public function fieldWithValue( $name, $title, $editor, $value, $params = [] ) {

        $tp = 's'; // todo

        $this->setField(
            $name,
            [
                "name" => $name,
                "type" => $tp,
                "view" => $editor,
                "title" => $title,
                "value" => $value,
            ],
            $params
        );

        return $this;
    }


    /**
     * запроксирован метод построения фильтра
     * @param $sName
     * @param $aValues
     * @param bool $mValue
     * @param string $sTitle
     * @param array $aParams
     * @return $this
     */
    public function addFilterSelect( $sName, $aValues, $mValue=false, $sTitle='', $aParams=array() ) {
        $this->oForm->addFilterSelect( $sName, $aValues, $mValue, $sTitle, $aParams );
        return $this;
    }


    /**
     * запроксирован метод построения фильтра
     * @param $sName
     * @param string/array $aValue массив из 2 элементов со значениями (могут содержать false)
     * @param string $sTitle
     * @param array $aParams
     * @return $this
     */
    public function addFilterDate($sName, $aValue, $sTitle='', $aParams=array() ) {
        $this->oForm->addFilterDate( $sName, $aValue, $sTitle, $aParams );
        return $this;
    }

    /**
     * запроксирован метод построения фильтра
     * @param string $sName
     * @param string $sValue
     * @param string $sTitle
     * @return $this
     */
    public function addFilterText( $sName, $sValue, $sTitle='' ) {
        $this->oForm->addFilterText( $sName, $sValue, $sTitle );
        return $this;
    }

    /**
     * запроксирован метод построения фильтра
     * @param bool $bCondition Флаг добавления поля фильтра в вывод
     * @param string $sName
     * @param array $aValues
     * @param bool $mValue
     * @param string $sTitle
     * @param array $aParams
     * @return $this
     */
    public function addFilterSelectIf( $bCondition, $sName, $aValues, $mValue=false, $sTitle='', $aParams=array() ) {
        if ( $bCondition )
            $this->oForm->addFilterSelect( $sName, $aValues, $mValue, $sTitle, $aParams );
        return $this;
    }


    /**
     * запроксирован метод построения фильтра
     * @param bool $bCondition Флаг добавления поля фильтра в вывод
     * @param string $sName
     * @param string $sValue
     * @param string $sTitle
     * @return $this
     */
    public function addFilterTextIf( $bCondition, $sName, $sValue, $sTitle='' ) {
        if ( $bCondition )
            $this->oForm->addFilterText( $sName, $sValue, $sTitle );
        return $this;
    }


    /**
     * Добавление кнопки в панель фильтров
     * @param string $sAction имя метода в php
     * @param string $sTitle надпись на кнопке
     * @param string $sConfirm текст подтверждения, если требуется
     * @param array $aParams дополнительные параметры
     * @return $this
     */
    public function addFilterButton( $sAction, $sTitle, $sConfirm='', $aParams=array() ) {
        $this->oForm->addFilterButton( $sAction, $sTitle, $sConfirm, $aParams );
        return $this;
    }


    /**
     * Добавление состояния обработки
     * @param $sAction
     * @return $this
     */
    public function addFilterAction( $sAction ) {
        $this->oForm->setPageLoadActionName( $sAction );
        return $this;
    }

    /**
     * Установка полей для формы
     * @return $this
     * @deprecated метод перенесен внутрь setValue
     */
    public function setFields() {

//        if ( $sFieldList != '*' ) {
//            $aFieldList = explode(',',$sFieldList);
//            foreach ( $aModel as $sFieldName => $aField )
//                if ( !in_array( $sFieldName, $aFieldList ) )
//                    unSet( $aModel[$sFieldName] );
//        }

        $this->oForm->setFields( $this->aModel );

        // очистка
        $this->aModel = array();

        return $this;
    }


    /**
     * Установка значений для формы
     * @param $aItems
     * @param int $iOnPage Кол-во на странице
     * @param int $iPage Страница
     * @param int $iTotal Всего записей
     * @return $this
     * @throws ServerErrorHttpException
     */
    public function setValue( $aItems, $iOnPage = 0, $iPage = 0, $iTotal = 0 ) {

        // если есть недобавленные поля
        if ( $this->aModel ) {
            $this->oForm->setFields( $this->aModel );
            $this->aModel = array();
        }

        $aFieldsTypes = array();
        $aFields = $this->oForm->getFields(true);
        foreach($aFields as $sKey => $aField){
            if ($aField->getType() == 'i')
                $aFieldsTypes[] = $sKey;
        }

        $aList = array();
        if ( ( $this->oForm instanceof \ExtList ) && count($aItems) > 0 ) {

            foreach ( $aItems  as $oItem ) {

                if ( count($this->aWidgetList) )
                    foreach ( $this->aWidgetList as $aWidget ) {
                        $sField = $aWidget['field'];
                        if ( class_exists( $aWidget['class'] ) ) {
                            if ( is_object( $oItem ) )
                                $oItem->$sField = call_user_func_array( array($aWidget['class'],$aWidget['method']), array($oItem, $sField) );
                            elseif ( is_array( $oItem ) )
                                $oItem[$sField] = call_user_func_array( array($aWidget['class'],$aWidget['method']), array($oItem, $sField) );
                        }

                    }

                foreach($aFieldsTypes as $sFieldName){
                    if ( is_object( $oItem ) ){
                        $oItem->$sFieldName = (int)$oItem->$sFieldName;
                    }else{
                        $oItem[$sFieldName] = (int)$oItem[$sFieldName];
                    }
                }

                $aList[] = $oItem;
            }
        } else {

            $aList = $aItems;
        }


        $this->oForm->setValues( $aList );

        if ( $iOnPage ) {
            // число записей на страницу
            $this->oForm->setOnPage( $iOnPage );
            $this->oForm->setPageNum( $iPage );

            // TODO #paginator нельзя использовать count($aItems)
            if ($iTotal)
                $this->oForm->setTotal( $iTotal );
            else
            $this->oForm->setTotal( count($aItems) );
        }

        return $this;
    }


    /**
     * Установка значений для простой формы с одной записью
     * @param $aItem
     * @return $this
     */
    public function setSimpleValue( $aItem ) {
        $this->oForm->setValues( $aItem );
        return $this;
    }

    /**
     * Установка кнопки в интерфейсе
     * @param $sTitle
     * @param $sAction
     * @param string $sIconCls
     * @param string $sState
     * @param array $aAddParams
     * @return $this
     * @deprecated use ->button()
     */
    public function addButton( $sTitle, $sAction, $sIconCls = '', $sState = 'init', $aAddParams = array() ) {

        $aButton = array (
            'text' => $sTitle,
            'iconCls' => $sIconCls,
            'state' => $sState,
            'action' => $sAction,
        );

        if ( count($aAddParams) )
            foreach ( $aAddParams as $sKey => $aVal )
                $aButton[$sKey] = $aVal;

        // отмена подтверждения об отмене сохранения
        if ( $sTitle == \Yii::t('adm','save') )
            $aButton['unsetFormDirtyBlocker'] = true;

        $this->oForm->addDockedItem( $aButton );

        return $this;
    }


    public function button( $title, $action, $icon = '', $state = 'init', $params = [] ) {
        return $this->addButton( $title, $action, $icon, $state, $params );
    }

    public function buttonConfirm( $sTitle, $sAction, $sIconCls = '', $sTextConfirm = '', $aAddParams = array() ) {

        $aButton = array (
            'text' => $sTitle,
            'iconCls' => $sIconCls,
            'state' => 'allow_do',
            'action' => $sAction,
            'actionText' => $sTextConfirm,
            'unsetFormDirtyBlocker' => true
        );

        if ( count($aAddParams) )
            foreach ( $aAddParams as $sKey => $aVal )
                $aButton[$sKey] = $aVal;

        $this->oForm->addDockedItem( $aButton );

        return $this;
    }

    public function buttonCancel( $sTitle=null, $sAction='init', $sIconCls = 'icon-cancel', $aAddParams = array() ) {

        if ( is_null($sTitle) )
            $sTitle = \Yii::t('adm','cancel');

        $aButton = array (
            'text' => $sTitle,
            'iconCls' => $sIconCls,
            'state' => 'cancel',
            'action' => $sAction,
        );

        if ( count($aAddParams) )
            foreach ( $aAddParams as $sKey => $aVal )
                $aButton[$sKey] = $aVal;

        $this->oForm->addDockedItem( $aButton );

        return $this;
    }

    /**
     * Добавление разделителя в панеле кнопок
     * @param string $sLabel
     * @return $this
     */
    public function buttonSeparator( $sLabel = '-' ) {
        $this->oForm->addDockedItem( $sLabel );
        return $this;
    }


    /**
     * Кнопка редактировать в строке
     * @param string $sAction Состояние в PHP
     * @param string $sState Состояние в JS
     * @return $this
     */
    public function buttonRowUpdate( $sAction='show', $sState='edit_form' ) {

        $this->oForm->addRowBtnArray([
            'tooltip' =>\Yii::t('adm','upd'),
            'iconCls' => 'icon-edit',
            'action' => $sAction,
            'state' => $sState
        ]);

        return $this;
    }


    /**
     * Кнопка удалить в строке
     * @param string $sAction Состояние в PHP
     * @param string $sState Состояние в JS
     * @param string $sText actionText
     * @return $this
     */
    public function buttonRowDelete( $sAction='delete', $sState='delete', $sText = '' ) {

        $this->oForm->addRowBtnArray([
            'tooltip' => \Yii::t('adm','del'),
            'iconCls' => 'icon-delete',
            'action' => $sAction,
            'state' => $sState,
            'actionText' => $sText
        ]);

        return $this;
    }


    /**
     * Установка кнопки в интерфейсе для работы с несколькими записями
     * @param $sTitle
     * @param $sAction
     * @param string $sIconCls
     * @param string $sState
     * @param array $aAddParams
     * @return $this
     */
    public function addButtonMultiple( $sTitle, $sAction, $sIconCls = '', $sState = 'init', $aAddParams = array() ) {

        $aAddParams['multiple'] = true;

        return $this->addButton( $sTitle, $sAction, $sIconCls, $sState, $aAddParams);
    }

    /**
     * Установка кнопки в интерфейсе для удаления нескольких записей
     * @param $sAction
     * @param string $sState
     * @param $sTitle
     * @param array $aAddParams
     * @return $this
     */
    public function addButtonDeleteMultiple( $sAction = 'delete', $sState = 'delete', $sTitle= '', $aAddParams = array() ) {

        $aAddParams['multiple'] = true;
        if (!$sTitle){
            $sTitle = \Yii::t('adm','del');
        }

        return $this->addButton( $sTitle, $sAction, 'icon-delete', $sState, $aAddParams);
    }


    /**
     * Установка кнопки в интерфейсе для удаления нескольких записей
     * @param $sAction
     * @param string $sState
     * @param $sTitle
     * @param array $aAddParams
     * @return $this
     */
    public function addButtonAddMultiple( $sAction = 'add', $sState = 'add', $sTitle= '', $aAddParams = array() ) {

        $aAddParams['multiple'] = true;
        if (!$sTitle){
            $sTitle = \Yii::t('adm', 'add');
        }

        return $this->addButton( $sTitle, $sAction, 'icon-add', $sState, $aAddParams);
    }


    /**
     * Добавление кнопки в интерфейс при выполнении условия
     * @param $bCondition
     * @param $sTitle
     * @param $sAction
     * @param string $sIconCls
     * @param string $sState
     * @param array $aAddParams
     * @return $this
     */
    public function addButtonIf( $bCondition, $sTitle, $sAction, $sIconCls = '', $sState = 'init', $aAddParams = array() ) {

        if ( $bCondition )
            $this->addButton( $sTitle, $sAction, $sIconCls, $sState, $aAddParams );

        return $this;
    }


    /**
     * @param $sTitle
     * @param $sAction
     * @param string $sIconCls
     * @param string $sState
     * @param string $sTextConfirm
     * @param array $aAddParams
     * @return $this
     * @deprecated use ->buttonConfirm()
     */
    public function addButtonConfirm( $sTitle, $sAction, $sIconCls = '', $sState = 'allow_do', $sTextConfirm = '', $aAddParams = array() ) {

        $aButton = array (
            'text' => $sTitle,
            'iconCls' => $sIconCls,
            'state' => $sState,
            'action' => $sAction,
            'actionText' => $sTextConfirm,
            'unsetFormDirtyBlocker' => true
        );

        if ( count($aAddParams) )
            foreach ( $aAddParams as $sKey => $aVal )
                $aButton[$sKey] = $aVal;

        $this->oForm->addDockedItem( $aButton );

        return $this;
    }


    public function addButtonCancel($sAction,$sIcon= 'icon-cancel',$sState = 'init',$aAddParams = array() ){
        return $this->addButton(\Yii::t('adm','cancel'),$sAction,$sIcon,$sState,$aAddParams);
    }

    /*
     * кнопка сохранения
     */
    public function addButtonSave($sAction,$sIcon = 'icon-save',$sState = 'init',$aAddParams = array()){
        return $this->addButton(\Yii::t('adm','save'),$sAction,$sIcon,$sState,$aAddParams);
    }


    /**
     * Установка расширенной кнопки в интерфейсе
     * @param $oButton
     * @return $this
     */
    public function addExtButton( $oButton ) {

        $this->oForm->addExtButton( $oButton );
        return $this;
    }


    /**
     * Добавление разделителя в панеле кнопок
     * @param string $sLabel
     * @return $this
     * @deprecated use ->buttonSeparator()
     */
    public function addBtnSeparator( $sLabel = '-' ) {
        $this->oForm->addDockedItem( $sLabel );
        return $this;
    }


    /**
     * Кнопка редактировать в строке
     * @param string $sAction Состояние в PHP
     * @param string $sState Состояние в JS
     * @return $this
     * @deprecated use ->buttonRowUpdate()
     */
    public function addRowButtonUpdate( $sAction='show', $sState='edit_form' ) {

        $aButton = array (
            'tooltip' =>\Yii::t('adm','upd'),
            'iconCls' => 'icon-edit',
            'action' => $sAction,
            'state' => $sState
        );

        $this->oForm->addRowBtnArray($aButton);

        return $this;
    }


    /**
     * Кнопка удалить в строке
     * @param string $sAction Состояние в PHP
     * @param string $sState Состояние в JS
     * @param string $sText actionText
     * @return $this
     * @deprecated use ->buttonRowDelete()
     */
    public function addRowButtonDelete( $sAction='delete', $sState='delete', $sText = '' ) {

        $aButton = array(
            'tooltip' => \Yii::t('adm','del'),
            'iconCls' => 'icon-delete',
            'action' => $sAction,
            'state' => $sState,
            'actionText' => $sText
        );

        $this->oForm->addRowBtnArray($aButton);

        return $this;
    }

    /**
     * @param $sName
     * @param string $sLayerName слой
     * @param string $sModuleName имя модуля
     * @param array $aParams дополнительные параметры для передачи
     * @return $this
     */
    public function addRowCustomBtn( $sName, $sLayerName='', $sModuleName='', $aParams = array() ) {

        $this->oForm->addLibClass($sName, $sLayerName, $sModuleName);

        $aParams['customBtnName'] = $sName;
        $aParams['customLayer'] = $sLayerName;

        $this->oForm->addRowBtnArray( $aParams );

        return $this;
    }


    public function addRowButtonConfirm($sTitle, $sIcon, $sAction, $sState = 'allow_do',$sTextConfirm){
        $aButton = array(
            'tooltip' => $sTitle,
            'iconCls' => $sIcon,
            'action' => $sAction,
            'state' => $sState,
            'actionText' => $sTextConfirm,
            'unsetFormDirtyBlocker' => true
        );
        $this->oForm->addRowBtnArray($aButton);
        return $this;
    }

    public function addRowButton($sTitle, $sIcon, $sAction, $sState = ''){
        $aButton = array(
            'tooltip' => $sTitle,
            'iconCls' => $sIcon,
            'action' => $sAction,
            'state' => $sState
        );
        $this->oForm->addRowBtnArray($aButton);
        return $this;
    }

    /**
     * Кнопка удалить в строке
     * @param string $sAction Состояние в PHP
     * @param string $sState Состояние в JS
     * @return $this
     */
    public function addRowButtonAdd( $sAction='add', $sState='add' ) {

        $aButton = array(
            'tooltip' => \Yii::t('adm','add'),
            'iconCls' => 'icon-add',
            'action' => $sAction,
            'state' => $sState
        );

        $this->oForm->addRowBtnArray($aButton);

        return $this;
    }

    /**
     * Добавить заголовок с текстом
     * @param string $sText
     * @return $this
     */
    public function addHeadText( $sText = '' ) {
        $this->oForm->setAddText($sText);
        return $this;
    }

    /**
     * Добавление нового преобразователя на поле
     * todo widgetSet
     */
    public function addWidget( $sFieldName, $sClassName, $sMethod ) {
        $this->aWidgetList[] = array(
            'field' => $sFieldName,
            'class' => $sClassName,
            'method' => $sMethod
        );
        return $this;
    }

    /**
     * Задает имя поля для группировки
     * @param $sFieldName
     * @return $this
     * todo groupSet
     */
    public function setGroups( $sFieldName ) {
        $this->oForm->setGroupField( $sFieldName );
        return $this;
    }


    /**
     * Включение DnD в списке
     * @param $sActionName
     * @return $this
     */
    public function enableDragAndDrop( $sActionName ) {
        $this->oForm->enableDragAndDrop($sActionName);
        return $this;
    }

    /**
     * Включение галочек выбора для операций над множеством записей списке
     * @return $this
     */
    public function showCheckboxSelection() {
        $this->oForm->showCheckboxSelection();
        return $this;
    }

    /**
     * Выключение галочек выбора для операций над множеством записей списке
     * @return $this
     */
    public function hideCheckboxSelection() {
        $this->oForm->hideCheckboxSelection();
        return $this;
    }

    /**
     * Добавление редактируемых полей в списке
     * @param array $aFieldList Список полей
     * @param string $sAction Метод обработки
     * @return $this
     * @throws
     */
    public function setEditableFields( $aFieldList, $sAction ) {

        $fields = $this->oForm->getFields();

        if (empty($fields)){
            throw new \Exception("setEditableFields вызван раньше setFields");
        }

        $this->oForm->setEditableFields( $aFieldList, $sAction );
        return $this;
    }

    /**
     * Флаг слежения за изменениями в форме
     * @param $bTrackChange
     */
    public function setTrackChanges( $bTrackChange ){
        if ($this->oForm instanceof \ExtForm)
            $this->oForm->setTrackChanges( $bTrackChange );
    }


    /**
     * Добавление библиотеки
     * @param $sName
     * @param string $sLayerName
     * @param string $sModuleName
     */
    public function addLib( $sName, $sLayerName = '', $sModuleName = '' ){
        $this->oForm->addLibClass($sName, $sLayerName, $sModuleName);
    }


    /**
     * Добавить поле сортировки
     * Первичная сортировка при отображении
     * @param $sFieldName
     * @param string $sDirection
     */
    public function addSorter( $sFieldName, $sDirection='ASC' ) {
        $this->oForm->addSorter( $sFieldName, $sDirection );
    }

}