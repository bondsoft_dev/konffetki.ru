<?php

namespace skewer\build\Component\UI\Action;

use skewer\build\libs\ft;
use skewer\build\Component\UI;
use skewer\build\Cms;

/**
 * Прототип для сборщика интерфейсного состояния
 * @class Prototype
 * @project Skewer
 * @package skewer\build\Component\ModuleConstructor\State
 *
 * @author kolesnikov, $Author: $
 * @version $Revision:  $
 * @date $Date: $
 */
abstract class Prototype {

    protected $aFields = array();

    protected $aButtons = array();

    protected $sAlias = '';

    protected $iItemId = 0;

    /** @var ft\QueryPrototype Апи на таблицу */
    protected $oQuery = null;

    /** @var UI\FactoryInterface */
    protected $oInterface;

    /** @var array Список ошибок */
    protected $aErrorList = array();

    function __construct( $sAlias, $iItemId = 0 ) {
        $this->sAlias = $sAlias;
        $this->iItemId = $iItemId;
        $this->initInterface();
    }

    /**
     * Инициализация типа интерфейса
     */
    private function initInterface() {
        // пока жесткое прописывание типа интерфейса
        $this->oInterface = new UI\ExtJS\Factory();
    }

    /**
     * @return array
     */
    public function getErrorList() {
        return $this->aErrorList;
    }

    /**
     * Задать апи для работы с таблицей
     * @param ft\QueryPrototype $oQuery
     */
    public function setQuery( ft\QueryPrototype $oQuery ) {
        $this->oQuery = $oQuery;
    }

    /**
     * Задание набора полей для вывода
     * @param $mFields
     * @return $this
     */
    public function setFields( $mFields ) {

        if ( is_array($mFields) ) {
            $this->aFields = $mFields;
        } elseif( $mFields !== '' ) {
            $this->aFields = explode( ',', $mFields );
        } else {
            $this->aFields = array();
        }

        return $this;
    }

    /**
     * Набор полей для построителя
     * @return string
     */
    protected function getFields() {

        return (is_array( $this->aFields ) && count( $this->aFields )) ? implode( ',', $this->aFields ) : '';
    }

    /**
     * Добавление кнопки управления
     * @param string $sAction Состояние
     * @param string $sAlias Модель
     * @param string $sTitle Надпись
     * @param string $sIcon Иконка
     * @param string $sCondition Условие вывода
     * @return $this
     */
    public function addButton( $sAction, $sAlias, $sTitle, $sIcon, $sCondition = '' ) {

        $aButton = array(
            'action' => $sAction,
            'title' => $sTitle,
            'icon' => $sIcon,
            'alias' => $sAlias
        );

        if ( $sCondition )
            $aButton['condition'] = $sCondition;

        $this->aButtons[] = $aButton;

        return $this;
    }

    /**
     * Вывод кнопок в интерфейсе
     * @param UI\State\StateInterface $oInterface
     * @param Cms\Tabs\ModulePrototype $oModule
     */
    protected function setButtons( UI\State\StateInterface $oInterface, $oModule ) {

        $aEnvPathData = $oModule->get('env_path');

        foreach ( $this->aButtons as $aButton ) {

            // проверка условий на вывод
            if ( isSet($aButton['condition']) ) {

                // есть предыдущее состояние для перехода
                if ( $aButton['condition'] == 'existParent' && ( !is_array($aEnvPathData) || !count($aEnvPathData) ) )
                    continue;
            }

            if ( isSet($aButton['alias']) && $aButton['alias'] == 'child' ) {

                $aEnvPathData[] = array(
                    'alias' => $this->sAlias,
                    'id' => $this->iItemId
                );

                $aButton['env_path'] = $aEnvPathData;
// todo получение дочерних модулей
//                foreach ( $this->oInterfaceConfig->getChildsAlias() as $sChildName ) {
//
//                    $aButton['alias'] = $sChildName;
//
//                    $this->createButton( $aButton, $oInterface );
//                }

            } elseif ( isSet($aButton['alias']) && $aButton['alias'] == 'parent' ) {

                $aButton['env_path'] = array_slice( $aEnvPathData, 0, count($aEnvPathData)-1 );
                $aButton['alias'] = $aEnvPathData[count($aEnvPathData)-1]['alias'];
                $aButton['data']['id'] = $aEnvPathData[count($aEnvPathData)-1]['id'];

                $this->createButton( $aButton, $oInterface );

            } else {

                if ( isSet($aButton['alias']) && $aButton['alias'] && $aButton['alias'] != $this->sAlias )
                    $aEnvPathData[] = array(
                        'alias' => $this->sAlias,
                        'id' => $this->iItemId
                    );


                $aButton['env_path'] = $aEnvPathData;

                $this->createButton( $aButton, $oInterface );
            }
        }

    }


    /**
     * Собирает объект кнопки по параметрам
     * @param array|UI\Element\Button $mButton
     * @param $oInterface
     * @return \ExtDockedPrototype
     */
    private function createButton( $mButton, UI\State\StateInterface $oInterface ) {

        if ( is_array($mButton) ) {

            $oButton = new UI\Element\Button();
            $oButton->setPhpAction( isSet($mButton['action']) ? $mButton['action'] : '' );
            $oButton->setTitle( isSet($mButton['title']) ? $mButton['title'] : '' );
            $oButton->setIcon( isSet($mButton['icon']) ? $mButton['icon'] : '' );
            $oButton->unsetDirtyChecker();

            if ( isSet($mButton['alias']) && $mButton['alias'] )
                $oButton->setAddParam( 'alias', $mButton['alias'] );
            else
                $oButton->setAddParam( 'alias', $this->sAlias );

            if ( isSet($mButton['data']) && is_array($mButton['data']) )
                $oButton->setAddParam( 'data', $mButton['data'] );

            if ( isSet($mButton['env_path']) )
                $oButton->setAddParam( 'env_path', $mButton['env_path'] );

            $oInterface->addButton( $oButton );

        } else {
            $oInterface->addButton( $mButton );
        }
    }


    public function setDefault() {
        return $this;
    }

    abstract public function buildState( $oModule, ft\QueryPrototype $oQuery, $aError = array() );


    /**
     * Замена шаблонных значений
     * @param $mData
     * @param Cms\Tabs\ModulePrototype $oModule
     */
    protected function replaceParam ( &$mData, $oModule ) {

        if ( is_array($mData) ) {

            if ( count($mData) )
                foreach( $mData as &$mItem ) {
                    $this->replaceParam( $mItem, $oModule );
                }

        } else {

            if ( strpos($mData,':parent.') === 0 ) {

                $sFieldName = substr($mData, 8);

                // поиск во входном окружении
                $aEnvPathArr = $oModule->get('env_path');
                if ( $aEnvPath = end( $aEnvPathArr ) )
                    if ( isSet($aEnvPath[$sFieldName]) )
                        $mData = $aEnvPath[$sFieldName];

            } elseif ( strpos($mData,':') === 0 ) {

                $sFieldName = substr($mData, 1);

                // поиск поля в модуле
                if ( isSet($oModule->$sFieldName) )
                    $mData = $oModule->$sFieldName;

                // поиск поля во входных данных
                if ( $sValue = $oModule->get($sFieldName) )
                    $mData = $sValue;

            }

        }
    }

}