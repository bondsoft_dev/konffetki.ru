<?php

namespace skewer\build\Component\UI\Action;

use skewer\build\libs\ft;

/**
 * Прототип для состояний, которые имеют часть по обновлению данных
 * @package skewer\build\Component\ModuleConstructor\State
 */
abstract class EditStatePrototype extends Prototype {


    /**
     * Добавление в ft-модель модификатора для поля $sFieldName
     * @param string $sFieldName Имя поля
     * @param string $sModifName Имя модификатора
     * @param array $aParams Дополнительные параметры
     * @return $this
     * @throws \skewer\build\libs\ft\exception\Model
     */
    public function addModificator ( $sFieldName, $sModifName, $aParams = array() ) {

        $oField = $this->oQuery->getModel()->getFiled( $sFieldName );
        if ( !$oField )
            throw new ft\exception\Model( "Не найдено поле [$sFieldName]" );

        $oField->addModificator( $sModifName, $aParams );

        return $this;
    }


    /**
     * Добавление в ft-модель валидатора для поля $sFieldName
     * @param string $sFieldName Имя поля
     * @param string $sValidType Тип поля
     * @return $this
     * @throws \skewer\build\libs\ft\exception\Model
     */
    public function addValidator( $sFieldName, $sValidType ) {

        $oField = $this->oQuery->getModel()->getFiled( $sFieldName );
        if ( !$oField )
            throw new ft\exception\Model( "Не найдено поле [$sFieldName]" );

        $oField->addValidator( $sValidType );

        return $this;
    }

}