<?php

namespace skewer\build\Component\UI\Action;

use skewer\build\libs\ft as ft;
use skewer\build\Cms;

/**
 * Сборщик интерфейсного состояния типа NEW
 * @class ExtNew
 * @extends Prototype
 * @project Skewer
 * @package skewer\build\Component\UI\Action
 *
 * @author kolesnikov, $Author: $
 * @version $Revision:  $
 * @date $Date: $
 */
class ExtNew extends ShowStatePrototype {

    /** @var array Массив замен */
    private $aReplaceData = array();

    /**
     * @return ExtNew
     */
    public function setDefault() {
        return $this->setFields( '' )
            ->addButton( 'add', '', 'Сохранить',  'icon-save' )
            ->addButton( 'list', '', 'Назад',  'icon-cancel' );
    }

    /**
     * Построение состояние интерфейса
     * @param Cms\Tabs\ModulePrototype $oModule
     * @param ft\QueryPrototype $oQuery
     * @param array $aError
     */
    public function buildState( $oModule, ft\QueryPrototype $oQuery, $aError = array()  ) {

        /**
         * Сборка данных
         */

        $aData = $oModule->get('data');

        // Оъект записи
        $oItem = $oQuery->getNewRow();

        // Если поля были отредактированы - заменяем
        if ( is_array($aError) && count($aError) )
            foreach ( $aData as $sFieldName => $sFieldValue ) {
                if ( property_exists( $oItem, $sFieldName ) )
                    $oItem->$sFieldName = $sFieldValue;
            }

        // замена меток
        $this->replaceParam( $this->aReplaceData, $oModule );

        // Обновление значения полей
        foreach ( $this->aReplaceData as $sFieldName => $sFieldValue ) {
            if ( property_exists( $oItem, $sFieldName ) )
                $oItem->$sFieldName = $sFieldValue;
        }

        /**
         * Сборка интерфейса
         */

        $oModel = $oQuery->getModel();
        $oInterface = $this->oInterface->getEdit();

        foreach ( $aError as $sFieldName => $sErrorText ) {
            $oField = $oModel->getFiled( $sFieldName );
            if ( !$oField )
                continue;
            $oField->setParameter('form_error',$sErrorText);
        }

        // задание набора полей для интерфейса
        $oInterface->setFieldsByFtModel(
            $oModel,
            $this->getFields()
        );

        // контент
        $oInterface->setValues( $oItem );

        // набор кнопок в панели слева
        $this->setButtons( $oInterface, $oModule );

        // стоим
        $oModule->setInterface( $oInterface );

    }

    /**
     * Подстановка значений в поле
     * @param $sFieldName
     * @param string $sTarget
     * @return $this
     */
    public function setField( $sFieldName, $sTarget = '' ) {

        $this->aReplaceData[$sFieldName] = $sTarget;

        return $this;
    }

}