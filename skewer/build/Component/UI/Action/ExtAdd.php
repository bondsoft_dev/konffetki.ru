<?php

namespace skewer\build\Component\UI\Action;

use skewer\build\libs\ft as ft;
use skewer\build\Cms;

/**
 * Сборщик интерфейсного состояния типа ADD
 * @class ExtAdd
 * @extends Prototype
 * @project Skewer
 * @package skewer\build\Component\UI\Action
 *
 * @author kolesnikov, $Author: $
 * @version $Revision:  $
 * @date $Date: $
 */
class ExtAdd extends EditStatePrototype {

    /** @var array Массив замен */
    private $aReplaceData = array();

    public function setDefault() {
        return $this;
    }

    /**
     * Построение состояние интерфейса
     * @param Cms\Tabs\ModulePrototype $oModule
     * @param ft\QueryPrototype $oQuery
     * @param array $aError
     * @return bool
     */
    public function buildState( $oModule, ft\QueryPrototype $oQuery, $aError = array()  ) {// \AdminToolTabModulePrototype

        // пришедшие поля из форм
        $aData = $oModule->get('data');

        // дополняем полями из замены
        if ( count($this->aReplaceData) )
            foreach ( $this->aReplaceData as $sFieldName=>$sValue ) {
                $aData[$sFieldName] = $sValue;
            }

        /** @var ft\ArPrototype $oItem Новый объект AR для сущности */
        $oItem = $oQuery->getNewRow();

        // замена меток
        $this->replaceParam( $aData, $oModule );

        // Обновление значения полей
        foreach ( $aData as $sFieldName => $sFieldValue ) {
            if ( property_exists( $oItem, $sFieldName ) )
                $oItem->$sFieldName = $sFieldValue;
        }

        $iResult = $oItem->save();

        // сохраняем текст ошибки
        if ( !$iResult ) {
            $this->aErrorList = $oItem->getErrorList();
            return false;
        }

        return true;
    }

    /**
     * Подстановка значений в поле
     * @param $sFieldName
     * @param string $sTarget
     * @return $this
     */
    public function setField( $sFieldName, $sTarget = '' ) {

        $this->aReplaceData[$sFieldName] = $sTarget;

        return $this;
    }
}