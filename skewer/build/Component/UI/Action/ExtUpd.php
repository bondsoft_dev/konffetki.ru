<?php

namespace skewer\build\Component\UI\Action;

use skewer\build\libs\ft as ft;
use skewer\build\Cms;

/**
 * Сборщик интерфейсного состояния типа UPD
 * @class ExtUpd
 * @extends Prototype
 * @project Skewer
 * @package skewer\build\Component\ModuleConstructor\State
 *
 * @author kolesnikov, $Author: $
 * @version $Revision:  $
 * @date $Date: $
 */
class ExtUpd extends EditStatePrototype {


    public function setDefault() {
        return $this;
    }

    /**
     * Построение состояние интерфейса
     * @param Cms\Tabs\ModulePrototype $oModule
     * @param ft\QueryPrototype $oQuery
     * @param array $aError
     * @throws \skewer\build\libs\ft\Exception
     * @return mixed
     */
    public function buildState( $oModule, ft\QueryPrototype $oQuery, $aError = array()  ) {

        $sPKey = $oQuery->primaryKey();

        $iItemId = $this->iItemId;
        $aData = $oModule->get('data');

        if ( !$iItemId )
            throw new ft\Exception("Попытка редактирования несуществующей записи!");

        // Оъект записи
        $oItem = $oQuery
            ->where( $sPKey, $iItemId )
            ->selectOne();

        if ( is_null($oItem) )
            throw new ft\Exception("Попытка редактирования несуществующей записи!");

        // Обновление значения полей
        foreach ( $aData as $sFieldName => $sFieldValue ) {
            if ( property_exists( $oItem, $sFieldName ) )
                $oItem->$sFieldName = $sFieldValue;
        }

        $iResult = $oItem->save();

        // сохраняем текст ошибки
        if ( !$iResult ) {
            $this->aErrorList = $oItem->getErrorList();
            return false;
        }

        return true;
    }
}