<?php

namespace skewer\build\Component\UI\Action;

use skewer\build\libs\ft as ft;
use skewer\build\Cms;

/**
 * Сборщик интерфейсного состояния типа EDIT
 * @class ExtEdit
 * @extends Prototype
 * @project Skewer
 * @package skewer\build\Component\UI\Action
 *
 * @author kolesnikov, $Author: $
 * @version $Revision:  $
 * @date $Date: $
 */
class ExtEdit extends ShowStatePrototype {


    public function setDefault() {
        return $this->setFields( '' )
            ->addButton( 'upd', '', 'Сохранить',  'icon-save' )
            ->addButton( 'list', '', 'Назад',  'icon-cancel' )
            ->addButton( 'del', '', 'Удалить', 'icon-delete' );
    }

    /**
     * Построение состояние интерфейса
     * @param Cms\Tabs\ModulePrototype $oModule
     * @param ft\QueryPrototype $oQuery
     * @param array $aError
     * @throws \skewer\build\libs\ft\Exception
     */
    public function buildState( $oModule, ft\QueryPrototype $oQuery, $aError = array()  ) {// \AdminToolTabModulePrototype

        /**
         * Сборка данных
         */

        $sPKey = $oQuery->primaryKey();
        $aData = $oModule->get('data');

        if ( !$this->iItemId )
            throw new ft\Exception("Не задано значение первичного ключа для выборки");

        /** @var ft\ArPrototype $oItem Оъект записи */
        $oItem = $oQuery
            ->where( $sPKey, $this->iItemId )
            ->selectOne();

        if ( is_null($oItem) )
            throw new ft\Exception("Не найдена запись с $sPKey={$this->iItemId}");

        // Если поля были отредактированы - заменяем
        if ( is_array($aError) && count($aError) )
            foreach ( $aData as $sFieldName => $sFieldValue ) {
                if ( property_exists( $oItem, $sFieldName ) )
                    $oItem->$sFieldName = $sFieldValue;
            }

        /**
         * Сборка интерфейса
         */

        $oModel = $oQuery->getModel();
        $oInterface = $this->oInterface->getEdit();

        foreach ( $aError as $sFieldName => $sErrorText ) {
            $oField = $oModel->getFiled( $sFieldName );
            if ( !$oField )
                continue;
            $oField->setParameter('form_error',$sErrorText);
        }

        // задание набора полей для интерфейса
        $oInterface->setFieldsByFtModel(
            $oModel,
            $this->getFields()
        );

        // todo HACK - не могу пока сделать календарь на стандартном компоненте
        // todo нужно найти место где обрабатывать editor в сборщике
        foreach ( $oModel->getFileds() as $oField ) {
            $sFieldName = $oField->getName();
            if ( $oField->getEditorName() == 'date' )
                $oItem->$sFieldName = date('d.m.Y', strtotime($oItem->$sFieldName));
        }

        // контент
        $oInterface->setValues( $oItem );

        // набор кнопок в панели слева
        $this->setButtons( $oInterface, $oModule );

        // стоим
        $oModule->setInterface( $oInterface );

    }

}