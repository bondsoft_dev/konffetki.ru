<?php

namespace skewer\build\Component\UI\Action;

use skewer\build\libs\ft as ft;
use skewer\build\Component\UI;
use skewer\build\Cms;

/**
 * Сборщик интерфейсного состояния типа SHOW
 * @class ExtShow
 * @extends Prototype
 * @project Skewer
 * @package skewer\build\Component\UI\Action
 *
 * @author kolesnikov, $Author: $
 * @version $Revision:  $
 * @date $Date: $
 */
class ExtShow extends ShowStatePrototype {


    public function setDefault() {
        return $this->setFields( 'name,title,module,desc,in_base' )
            ->addButton( 'list', '', 'Назад', 'icon-cancel' )
            ->addButton( 'edit', '', 'Редактировать', 'icon-edit' );
    }

    /**
     * Построение состояние интерфейса
     * @param Cms\Tabs\ModulePrototype $oModule
     * @param ft\QueryPrototype $oQuery
     * @param array $aError
     * @throws \skewer\build\libs\ft\Exception
     */
    public function buildState( $oModule, ft\QueryPrototype $oQuery, $aError = array()  ) {

        /**
         * Сборка данных
         */

        $sPKey = $this->oQuery->primaryKey();

        // получение идентификатора записи
        $iItemId = $this->iItemId;

        if ( !$iItemId )
            throw new ft\Exception("Error. Row with $sPKey=$iItemId not found!");

        $oItem = $oQuery
            ->where( $sPKey, $iItemId )
            ->selectOne();

        /**
         * Сборка интерфейса
         */

        $oInterface = $this->oInterface->getShow();

        // задание набора полей для интерфейса
        $oInterface->setFieldsByFtModel(
            $oQuery->getModel(),
            $this->getFields()
        );

        // контент
        $oInterface->setValues( $oItem );

        // набор кнопок в панели слева
        $this->setButtons( $oInterface, $oModule );

        // стоим
        $oModule->setInterface( $oInterface );
    }

}