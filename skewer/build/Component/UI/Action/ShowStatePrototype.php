<?php

namespace skewer\build\Component\UI\Action;

use skewer\build\libs\ft;

/**
 * Прототип для состояний, которые имеют визуальную часть
 * @package skewer\build\Component\UI\Action
 */
abstract class ShowStatePrototype extends Prototype {

    /**
     * Задает редактор для поля
     * @param string $sField
     * @param string $sEditorType
     * @param array $aParams
     * @return $this
     * @throws ft\exception\Model
     */
    public function setEditor( $sField, $sEditorType, $aParams = array() ) {

        $oField = $this->oQuery->getModel()->getFiled( $sField );
        if ( !$oField )
            throw new ft\exception\Model( "Не найдено поле [$sField]" );

        $oField->setEditor( $sEditorType, $aParams );

        return $this;
    }


    /**
     * Задает виджет для поля
     * @param $sFieldName
     * @param $sWidgetName
     * @param array $aParams
     * @throws ft\exception\Model
     * @return $this
     */
    public function addWidget( $sFieldName, $sWidgetName, $aParams=array() ) {

        // todo интерфейсные виджеты сохнаряются в эту модель и не доступны из Row напрямую
        $oField = $this->oQuery->getModel()->getFiled( $sFieldName );
        if ( !$oField )
            throw new ft\exception\Model( "Не найдено поле [$sFieldName]" );

        $oField->addWidget( $sWidgetName, $aParams );

        return $this;

    }

}