<?php

namespace skewer\build\Component\UI\Action;


use skewer\build\libs\ft as ft;
use skewer\build\Component\UI;
use skewer\build\Cms;

/**
 * Сборщик интерфейсного состояния типа LIST
 * @class ExtList
 * @extends Prototype
 * @project Skewer
 * @package skewer\build\Component\UI\Action
 *
 * @author kolesnikov, $Author: $
 * @version $Revision:  $
 * @date $Date: $
 */
class ExtList extends ShowStatePrototype {

    /** @var UI\Element\RowButton[] Набор кнопок в строках */
    public $aRowButtons = array();

    /** @var array Фильтр для выборки */
    private $aFilter = array();


    public function setDefault() {

        return $this->setFields( '' )
            ->addRowButton( \Yii::t('adm','upd'),'icon-edit','edit','edit_form' )
            ->addRowButton( \Yii::t('adm','del'),'icon-delete','del','delete' )
            ->addButton( 'new', '', 'Создать',  'icon-add' )
            ->addButton( 'edit', 'parent', 'Назад', 'icon-cancel', 'existParent' );

    }

    /**
     * Построение состояние интерфейса
     * @param Cms\Tabs\ModulePrototype $oModule
     * @param ft\QueryPrototype $oQuery
     * @param array $aError
     */
    public function buildState( $oModule, ft\QueryPrototype $oQuery, $aError = array()  ) {

        $this->parse( $oModule );

        // задать переменную для получения общего количества
        $oQuery->setCounterRef( $iCount );

        // получить список
        $aItems = $this->applyFilter( $oQuery )->select();

        // создание объекта построения интерфейса
        $oInterface = $this->oInterface->getList();

        // задание набора полей для интерфейса
        $oInterface->setFieldsByFtModel(
            $oQuery->getModel(),
            $this->getFields()
        );

        // задать значения
        $oInterface->setValues( $aItems );

        // число записей на страницу
        $oInterface->setOnPage( $oModule->iOnPage );
        $oInterface->setPageNum( $oModule->iPage );
        $oInterface->setTotal( $iCount );

        // кнопки в строках
        $this->setRowButtons( $oInterface, $oModule );

        // кнопки управления
        $this->setButtons( $oInterface, $oModule );

        // сформировать интерфейс
        $oModule->setInterface( $oInterface );
    }

    /**
     * Добавление кнопки в строку
     * @param string $sTooltip подпись
     * @param string $sIconCls иконка
     * @param string $sAction состояние в модуле
     * @param string $sState состояние в форме
     * @param string $sAlias объект состояния
     * @return $this
     */
    public function addRowButton( $sTooltip, $sIconCls, $sAction, $sState, $sAlias = '' ) { // todo переход в другую сущность

        $oRowButton = new UI\Element\RowButton();
        $oRowButton->setTitle( $sTooltip );
        $oRowButton->setIcon( $sIconCls );
        $oRowButton->setPhpAction( $sAction );
        $oRowButton->setJsState( $sState );

        if ( $sAlias )
            $oRowButton->setAddParam( 'alias', $sAlias );

        $this->aRowButtons[] = $oRowButton;

        return $this;
    }

    /**
     * @param UI\State\ListInterface $oInterface
     * @param Cms\Tabs\ModulePrototype $oModule
     */
    protected function setRowButtons( $oInterface, $oModule ) {

        foreach ( $this->aRowButtons as $oButton ) {

            $aEnvPathData = $oModule->get('env_path');

            if ( $oButton->getAddParam('alias') != $this->sAlias && !is_null($oButton->getAddParam('alias')) ) // todo разобраться почему nul приходит
                $this->sAlias = $oButton->getAddParam('alias');
//                $aEnvPathData[] = array(
//                    'alias' => $this->sAlias,
//                    'id' => $this->iItemId
//                );

            $oButton->setAddParam( 'alias', $this->sAlias );
            $oButton->setAddParam( 'env_path', $aEnvPathData );

            $oInterface->addRowBtn( $oButton );
        }

    }

    /**
     * Добавить условие на выборку
     * @param string $sField Название поля
     * @param string $sOperation Операция стравнения
     * @param null|string $mValue Значение
     * @return $this
     */
    public function condition( $sField, $sOperation, $mValue = null ) {

        if ( is_null( $mValue ) ) {
            $mValue = $sOperation;
            $sOperation = '=';
        }

        $this->aFilter['condition'][] = array(
            'field' => $sField,
            'operation' => $sOperation,
            'value' => $mValue
        );

        return $this;
    }

    /**
     * Добавить сортировку по полю
     * @param string $sField Название поля
     * @param string $sWay Направление сортировки
     * @return $this
     */
    public function order( $sField, $sWay = 'ASC' ) {

        if ( in_array( $sWay, array('ASC', 'DESC') ) )
            $sWay = 'ASC';

        $this->aFilter['order'][] = array(
            'field' => $sField,
            'way' => $sWay
        );

        return $this;
    }

    /**
     * Формирование запроса для выборки с фильтром
     * @param ft\QueryPrototype $oQuery
     * @return ft\QueryPrototype
     */
    protected function applyFilter( $oQuery ) {

        // WHERE
        if ( isSet($this->aFilter['condition']) ) {

            foreach ( $this->aFilter['condition'] as $aFilterItem ) {

                if ( isSet($aFilterItem['operator']) ) {
                    $sOperator = $aFilterItem['operator'];
                } else {
                    $sOperator = '=';
                }

                $oQuery->where( $aFilterItem['field'], $sOperator, $aFilterItem['value'] );

            }

        }

        // ORDER BY
        if ( isSet($this->aFilter['order']) ) {

            foreach ( $this->aFilter['order'] as $aFilterItem )
                $oQuery->order( $aFilterItem['field'], $aFilterItem['way'] );

        }

        // LIMIT
        if ( isSet($this->aFilter['limit']) ) {

            $oQuery->limit( $this->aFilter['limit']['count'], $this->aFilter['limit']['page']*$this->aFilter['limit']['count'] );
        }

        return $oQuery;
    }

    /**
     * Препарсинг данных перед построением состояния
     * @param Cms\Tabs\ModulePrototype $oModule
     * @return $this
     */
    public function parse( $oModule ) {

        if ( count($this->aFilter) )
            foreach ( $this->aFilter as &$aBlock )
                foreach ( $aBlock as &$mItem )
                    $this->replaceParam( $mItem, $oModule );


        return $this;
    }


}