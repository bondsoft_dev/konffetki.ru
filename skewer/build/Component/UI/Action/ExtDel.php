<?php

namespace skewer\build\Component\UI\Action;

use skewer\build\libs\ft as ft;
use skewer\build\Cms;

/**
 * Сборщик интерфейсного состояния типа DEL
 * @class ExtDel
 * @extends Prototype
 * @project Skewer
 * @package skewer\build\Component\UI\Action
 *
 * @author kolesnikov, $Author: $
 * @version $Revision:  $
 * @date $Date: $
 */
class ExtDel extends EditStatePrototype {

    public function setDefault() {
        return $this;
    }

    /**
     * Построение состояние интерфейса
     * @param Cms\Tabs\ModulePrototype $oModule
     * @param ft\QueryPrototype $oQuery
     * @param array $aError
     * @throws \skewer\build\libs\ft\Exception
     */
    public function buildState( $oModule, ft\QueryPrototype $oQuery, $aError = array()  ) {// \AdminToolTabModulePrototype

        $sPKey = $oQuery->primaryKey();

        $iItemId = $this->iItemId;

        if ( !$iItemId )
            throw new ft\Exception('Попытка удаления несуществующей записи!');

        // Оъект записи
        $oItem = $oQuery
            ->where( $sPKey, $iItemId )
            ->selectOne();


        $oItem->del();

    }

}