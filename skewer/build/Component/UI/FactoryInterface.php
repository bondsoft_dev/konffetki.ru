<?php

namespace skewer\build\Component\UI;

use skewer\build\Component\UI;

/**
 * Фабрика интерфейсов
 * Class Factory
 * @package skewer\build\Component\UI
 */
interface FactoryInterface {

    /**
     * Отдает объект для построения спискового интерфейса
     * @return UI\State\ListInterface
     */
    function getList();

    /**
     * Отдает объект для построения интерфейса редактирования
     * @return UI\State\EditInterface
     */
    function getEdit();

    /**
     * Отдает объект для построения интерфейса отображения данных
     * @return UI\State\ShowInterface
     */
    function getShow();

}
