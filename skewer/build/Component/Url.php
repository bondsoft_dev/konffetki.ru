<?php

namespace skewer\build\Component;

/**
 * Класс компонент для работы с url
 * Класс временный для вынесения функционала из процессоров.
 * #stab_fix В дальнейшем должен быть заменен на доработанный urlManager
 */
class Url
{

    /**
     * #stab_del можно заменить на данные из \Yii::$app->request
     * Остаток URL после получения Id раздела
     * @var string
     */
    public $sURL = '';

    /**
     * #stab_del можно заменить на данные из \Yii::$app->request
     * массив GET-параметров
     * @var array
     */
    public $aGet = array();


}