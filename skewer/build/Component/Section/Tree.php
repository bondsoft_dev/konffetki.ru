<?php

namespace skewer\build\Component\Section;


use skewer\build\Component\orm\Query;
use skewer\models\TreeSection;
use skewer\build\Component\Catalog;
use skewer\build\Component\Search;
use skewer\build\Component\Site;
use yii\helpers\ArrayHelper;
use skewer\build\Component\Gallery;
use skewer\models;

class Tree {

    const typeSection = 0; /** тип - раздел */
    const typeDirectory = 1; /** тип - папка */

    const policyUser = 'user';
    const policyAdmin = 'admin';

    /** id для подстановки в набор шаблонов */
    const tplDirId = -1;

    protected static $cache = [];
    protected static $AvailableSectionCache = [];

    protected static $copyes = [];


    /**
     * Получение кешированного списка разделов
     * @return array
     * todo доработать кеш через поле и оформить отдельным объектом
     * todo добавить выборку только разрешенных политикой разделов
     * использовать только для клиенской части
     */
    public static function getCachedSection() {

        if ( !self::$cache ) {

            $sections = TreeSection::find();

            foreach ( $sections->each() as $section )
                self::$cache[$section->id] = $section->getAttributes();

        }

        return self::$cache;
    }


    /**
     * Получение объекта раздела
     * @param int|string $id Идентификатор
     * @param bool $bAsArray Флаг вывода результатов как массив, а не объект
     * @return TreeSection|array
     */
    public static function getSection( $id, $bAsArray = false ) {

        if ( is_numeric( $id ) )
            $section = TreeSection::findOne(['id' => $id]);
        else
            $section = TreeSection::findOne(['alias' => $id]);

        return $bAsArray ? $section->getAttributes() : $section;
    }


    /**
     * Получение набора объектов разделов
     * @param int[] $list Набор идентификаторов
     * @param bool $bAsArray Флаг вывода результатов как массив, а не объект
     * @param bool $bKeepOrder Использовать оригинальный порядок
     * @return array
     * fixme $bKeepOrder - wtf??
     * todo нет проверки на политики
     */
    public static function getSections( $list, $bAsArray = false, $bKeepOrder = false ) {

        $out = [];
        $sections = TreeSection::findAll(['id' => $list]);

        foreach ( $sections as $section )
            $out[$section->id] = $bAsArray ? $section->getAttributes() : $section;

        if ( $bKeepOrder ) {
            $realOut = [];
            foreach ( $list as $key )
                if ( isSet($out[$key]) )
                    $realOut[] = $out[$key];
            return $realOut;
        }

        return $out;
    }


    /**
     * @param $path
     * @param string $tail
     * @return int
     * fixme Доработать функцию (переехало из tree->getIdByPath)
     */
    public static function getSectionByPath( $path, &$tail = '', $denySections = [] ) {

        if ( !$path )
            return 0;

        $curPath = $path;
        $id = 0;
        $i = 0;

        do {

            $oQuery = TreeSection::find()
                ->where([
                    'alias_path' => $curPath,
                    'visible' => [
                        Visible::HIDDEN_FROM_MENU,
                        Visible::VISIBLE,
                        Visible::HIDDEN_NO_INDEX,
                    ]]
            );

            if ($denySections)
                $oQuery->andWhere(['NOT IN', 'id', $denySections]);

            $row = $oQuery->one();

            if ( $row ) {
                $id = $row->id;
            } else {
                $curPath = ( strlen($curPath) >= 2 ) ? substr( $curPath, 0, strrpos($curPath,'/',-2)+1 ) : '/';
            }

        } while ( !$id and $curPath and $curPath!=='/' and ++$i<10 );// fixme static::iLevelLimit == 10

        $tail = ($curPath===$path) ? '' : substr( $path, strlen($curPath) );

        return $id;

    }


    /**
     * @param $id
     * @return array
     * todo пересмотреть метод и его применение
     */
    public static function getSectionByParent( $id ) {

        $out = [];

        $list = TreeSection::findAll( ['parent' => $id] );

        if ( $list )
            foreach ( $list as $section )
                $out[] = $section->getAttributes();

        return $out;
    }


    public static function getSectionByAlias( $alias, $parent ) { //@todo смысл в parent, если alias уникален?
        $row = TreeSection::findOne( ['alias' => $alias, 'parent' => $parent] );
        return $row ? $row->id : null;
    }



    public static function getSectionAlias( $id ) {
        $row = TreeSection::findOne( ['id' => $id] );
        return $row ? $row->alias : '';
    }


    /**
     * Значение поля alias_path для раздела $id
     * @param int $id Идентификатор раздела
     * @param bool $bUseCache Флаг использования кешированных данных
     * @return string
     */
    public static function getSectionAliasPath( $id, $bUseCache = false ) {

        if ( $bUseCache ) {
            $sections = self::getCachedSection();
            if ( isSet( $sections[$id]['alias_path'] ) )
                return $sections[$id]['alias_path'];
        }

        $row = TreeSection::findOne( ['id' => $id] );
        return $row ? $row->alias_path : '';
    }


    /**
     * Значение поля alias_path для раздела $id
     * @param int $id Идентификатор раздела
     * @param bool $bUseCache Флаг использования кешированных данных
     * @return string
     */
    public static function getSectionTitle( $id, $bUseCache = false ) {

        if ( $bUseCache ) {
            $sections = self::getCachedSection();
            if ( isSet( $sections[$id]['title'] ) )
                return $sections[$id]['title'];
        }

        $row = TreeSection::findOne( ['id' => $id] );
        return $row ? $row->title : '';
    }


    public static function getSectionParent( $id, $bUseCache = false ) {

        if ( $bUseCache ) {
            $sections = self::getCachedSection();
            if ( isSet( $sections[$id]['parent'] ) )
                return $sections[$id]['parent'];
        }

        $row = TreeSection::findOne( ['id' => $id] );
        return $row ? (int)$row->parent : 0;
    }

    /**
     * @param $iParent
     * @return array
     */
    public static function getAllSubsection($iParent){

        $aSections = [];
        $aParents = [$iParent];

        do {
            $aNewParents = [];
            $bWhile = false;
            foreach (self::getCachedSection() as $key => $value) {
                if (isset($value['parent']) && in_array($value['parent'], $aParents)) {
                    $bWhile = true;
                    $aSections[] = $key;
                    $aNewParents[] = $key;
                }
            }
            $aParents = $aNewParents;
        }
        while ($bWhile);

        return $aSections;

    }


    /**
     * @param $id
     * @param bool $bAsArray
     * @param bool $bOnlyKeys
     * @return array|TreeSection[]
     */
    public static function getSubSections( $id, $bAsArray = false, $bOnlyKeys = false ) {

        $list = TreeSection::find()->where(['parent' => $id])->orderBy( 'position' )->all();

        if ( !$bAsArray )
            return $list;

        $out = [];
        /** @var TreeSection $section */
        foreach ( $list as $section )
            if ( $bOnlyKeys )
                $out[] = $section->id;
            else
                $out[$section->id] = $section->getAttributes();

        return $out;
    }


    /**
     * Получение набора родительских разделов
     * @param int $id Идентификатор
     * @param int $stop Идентификатор стоп раздела
     * @param bool $bUseCache
     * @return array
     */
    public static function getSectionParents( $id, $stop = -1, $bUseCache = true ) {

        $out = [];

        $curId = $id; // текущий обрабатываемый раздел

        do {

            // запрос родительского раздела
            $curId = self::getSectionParent( $curId, $bUseCache ); // fixme избавиться от рекурсивного обращения

            // выходим если достигли стоп-вершины
            if ($curId == $stop) break;

            // дополнение выходного массива
            if ( $curId )
                $out[] = $curId;

        } while ( $curId );

        return $out;
    }


    /**
     * Получение заголовка или списка заголовков страниц
     * @param int|int[] $id Иденификатор или список идентификаторов
     * @param bool $bWithSubs Флаг вывода подразделов
     * @return array|string
     */
    public static function getSectionsTitle( $id, $bWithSubs = false ) {

        if ( $bWithSubs ) {

            $out = [];
            $items = self::getSectionList( $id );

            foreach ( $items as $section )
                $out[$section['id']] = $section['title'];
            
            return $out;

        } else if ( is_array( $id ) ) {

            $out = [];
            $items = TreeSection::findAll(['id' => $id]);

            /** @var TreeSection $section */
            foreach ( $items as $section )
                $out[$section->id] = $section->title;

            return $out;

        } else {

            $section = self::getSection( $id );

            return $section ? $section->title : '';
        }

    }


    /**
     * Получение подразделов в виде списка
     * @param int $id Идентификатор раздела корня дерева
     * @param bool|int|string $policy Политика доступа
     * @return array|bool
     * todo заменить $oTree->getSectionsInLine()
     */
    public static function getSectionList( $id, $policy = false ) {

        $fullSections = self::getAvailableSections( $policy, $id );

        if ( isSet( $fullSections['.'] ) ) {
            $out = [$fullSections['.']];
            $add = self::collect( $id, $fullSections, 0, false );
            if ( $add )
                $out = array_merge( $out, $add );
        } else
            $out = self::collect( $id, $fullSections, 0, false );

        return $out;
    }


    /**
     * Получение подразделов в виде дерева
     * @param int $id Идентификатор раздела корня дерева
     * @param bool|int|string $policy Политика доступа
     * @return array|bool
     * todo заменить $oTree->getAllSections()
     */
    public static function getSectionTree( $id, $policy = false ) {

        $fullSections = self::getAvailableSections( $policy );

        return self::collect( $id, $fullSections, 0 );
    }


    /**
     * Получение ветки дерева разделов для публичной части
     * @param int $root Корень ветки
     * @param int $current Текущий (выделенный) раздел
     * @param int $showLvl
     * @return array|bool
     */
    public static function getUserSectionTree( $root, $current = 0, $showLvl = 0 ) {

        $fullSections = self::getAvailableSections( self::policyUser, $current, true );

        $parents = [$current];
        if ( isSet($fullSections['#'][$current]) )
            while ( $current = $fullSections['#'][$current] ) {
                if( !isSet($fullSections['#'][$current]) )
                    break;
                $parents[] = $current;
            }

        $tree = self::collectMarkTree( $root, $fullSections, 0, $parents, $showLvl );

        return $tree;
    }


    /**
     * Получение всех разрешенных политикой разделов
     * ~~~
     * Отдает массив массивов
     *  1. ключ # - int[] - массив int - id разделов, доступных для вывода
     *  2. ключ . - [] - типовой массив с данными текущего раздела
     *  3. ключ int (тиких много) - [][] - массив типовых массивов с резделами,
     *          подчиненными для раздела с id = ключу
     *
     *  Типовой массив имеет структуру
     *     'id' => 278      // int id раздела
     *     'parent' => 277  // int id родительского раздела
     *     'title' => 'FAQ' // str название раздела
     *     'visible' => 1   // int id режима видимости (см класс Visible)
     *     'show' => true   // bool выводить на сайте или нет (true при visible == Visible::VISIBLE )
     *     'link' => ''     // str ссылка с раздела
     * ~~~
     * @param bool $policy Политика доступа
     * @param int $id Выбранный раздел
     * @param bool $bUseCached Кешировать пользовательские выборки
     * @return array
     * @throws \Exception
     */
    protected static function getAvailableSections( $policy = false, $id = 0, $bUseCached = false ) {

        if ( $bUseCached && self::$AvailableSectionCache && $policy == self::policyUser )
            return self::$AvailableSectionCache;

        $out = [];

        $query = TreeSection::find()->orderBy( 'position' );

        if ( $policy ) { // fixme поправить политики
            if ( $policy == self::policyAdmin )
                $sections = \CurrentAdmin::getReadableSections();
            elseif ( $policy == self::policyUser )
                $sections = \CurrentUser::getReadableSections();
            else
                $sections = ArrayHelper::getValue( \Policy::getGroupPolicyData( $policy ), 'read_access', [] );

            $query->where( ['id' => $sections] );
        }


        /** @var TreeSection $section */
        foreach ( $query->each() as $section ) {
            $out[ $section->parent ][] = [
                'id' => $section->id,
                'parent' => $section->parent,
                'title' => $section->title,
                'visible' => $section->visible,
                'show' => in_array($section['visible'], Visible::$aShowInMenu),
                'link' => $section->link,
            ];
            $out['#'][$section->id] = $section->parent;
            if ( $id && $section->id == $id )
                $out[ '.' ] = [
                    'id' => $section->id,
                    'parent' => $section->parent,
                    'title' => $section->title,
                    'visible' => $section->visible,
                    'show' => in_array($section['visible'], Visible::$aShowInMenu),
                    'link' => $section->link,
                ];
        }

        if ( $bUseCached && $policy == self::policyUser )
            self::$AvailableSectionCache = $out;

        return $out;
    }

    /**
     * Сбрасывает текущий кэш
     */
    public static function dropCache() {
        self::$AvailableSectionCache = [];
    }

    protected static function collect( $section, &$list, $lvl = 0, $bTree = true ) {

        if ( $lvl > 5 || !isSet( $list[$section] ) )
            return false;

        $out = [];

        foreach ( $list[$section] as $data ) {

            if ( !isSet( $list[$data['id']] ) ) {

                if ( !$bTree )
                    $data['title'] = str_repeat( '-', $lvl + 1 ) . $data['title'];
                else
                    $data['children'] = []; // fixme рудимент от старого формата

                $out[] = $data;

            } elseif ( $bTree ) {

                $data['children'] = self::collect( $data['id'], $list, $lvl + 1, $bTree );
                $out[] = $data;

            } else {

                $data['title'] = str_repeat( '-', $lvl + 1 ) . $data['title'];
                $out[] = $data;
                $out = array_merge( $out, self::collect( $data['id'], $list, $lvl + 1, $bTree ) );

            }

        }

        return $out;
    }


    protected static function collectMarkTree( $section, &$list, $lvl = 0, $selected = [], $showLvl = 0 ) {

        if ( $lvl > 5 || !isSet( $list[$section] ) )
            return false;

        $out = [];

        foreach ( $list[$section] as $data ) {

            $data['href'] = $data['link'] ?: '['.$data['id'].']';
            $data['items'] = [];

            if ( in_array( $data['id'], $selected ) )
                $data['selected'] = true;

            if ( !isSet( $list[$data['id']] ) ) {

                $out[] = $data;

            } else {

                if ( $showLvl-1 > $lvl || !$showLvl || isSet($data['selected']) )
                    $data['items'] = self::collectMarkTree( $data['id'], $list, $lvl + 1, $selected, $showLvl );

                $out[] = $data;
            }

        }

        return $out;
    }


    /**
     * Добавление нового раздела
     * @param $iParent
     * @param $sTitle
     * @param int $iTemplateId
     * @param string $sAlias
     * @param int $visible
     * @param string $sLink
     * @return bool|TreeSection
     */
    public static function addSection( $iParent, $sTitle, $iTemplateId = 0, $sAlias = '', $visible = Visible::VISIBLE, $sLink = '' ) {

        $section = new TreeSection();
        $section->parent = $iParent;
        $section->alias = $sAlias;
        $section->title = $sTitle;
        $section->visible = $visible;
        $section->link = $sLink;

        if ( !$section->save() ) {
            //var_dump( $section->errors );
            return false;
        }

        if ( !$iTemplateId )
            return $section;

        $section->setTemplate( $iTemplateId );

        return $section;
    }


    /**
     * Копирует раздел в другой.
     * @param TreeSection $oSection
     * @param $iParent
     * @param bool|false $bRec
     * @param array $filter
     * @return array
     */
    public static function copySection( TreeSection $oSection, $iParent, $bRec = false, $filter = [] ){

        self::$copyes = [];

        self::copy($oSection, $iParent, $bRec, $filter);

        return self::$copyes;

    }


    /**
     * @todo рефакторить!!!
     * Копирует раздел в другой.
     * @param TreeSection $oSection
     * @param $iParent
     * @param bool $bRec
     * @param array $filter
     * @return bool
     * @todo тесты на эту функцию
     * Добавить проверки на зацикливание
     */
    public static function copy( TreeSection $oSection, $iParent, $bRec = false, $filter = [] ){

        $sTitle = $oSection->title;

        $aServicesParam = \Yii::$app->sections->getByValue($oSection->id);
        if ($aServicesParam){
            $sParam = $aServicesParam[0];
            $sTitle = \Yii::t('data/app', 'section_' . $sParam, [], Parameters::getLanguage($iParent)); //@todo может как-то запоминать язык?
            if ($sTitle == 'section_' . $sParam)
                $sTitle = $oSection->title;
        }

        $oNewSection = self::addSection($iParent, $sTitle, $oSection->getTemplate(),
            $oSection->alias, $oSection->visible, ''
        );

        if (!$oNewSection) {
            return false;
        }

        /** Запомним - пригодится */
        self::$copyes[$oSection->id] = $oNewSection->id ;

        /**
         * Если копируется раздел, прописанный как сервисный раздел для чего-либо,
         * то нужно сделать новый такой же параметр для языка создаваемого раздела!
         */

        $lang = Parameters::getLanguage($oNewSection->id);

        if ($aServicesParam){
            self::$cache = []; //хак @todo вспомнить зачем
            if ($lang){
                foreach( $aServicesParam as $sParam ){
                    $sTitle = \Yii::t('data/app', 'section_' . $sParam, [], Parameters::getLanguage($iParent));
                    if ($sTitle == 'section_' . $sParam)
                        $sTitle = $oSection->title;
                    \Yii::$app->sections->setSection($sParam, $sTitle, $oNewSection->id, $lang);

                    /** @todo хак на главные разделы*/
                    if ($sParam == 'main')
                        $oNewSection->save();
                }
            }
        }

        /**
         * Копирование параметров
         */
        $aAddParams = Parameters::getList( $oSection->id )
            ->level( Params\ListSelector::alAll )
            ->get();

        // установить все параметры
        foreach ( $aAddParams as $oParam ) {

            /** Меняем подписи для диз режима */
            if ($oParam->group == \skewer\build\Design\Zones\Api::layoutGroupName && $oParam->name == \skewer\build\Design\Zones\Api::layoutTitleName)
                $oParam->value = $oNewSection->title . ' ('. $lang .')';

            Parameters::setParams(
                $oNewSection->id,
                $oParam->group,
                $oParam->name,
                $oParam->value,
                $oParam->show_val,
                $oParam->title,
                $oParam->access_level
            );
        }

        if ($bRec){
            $aSubSections = self::getSubSections($oSection->id);
            if ($aSubSections)
                foreach( $aSubSections as $oSubSection){
                    if (!$filter || array_search($oSubSection->id, $filter) !== false)
                        self::copy($oSubSection, $oNewSection->id, $bRec);
                }
        }

        /**
         * Обновление поиска должно быть после добавления параметров в раздел
         */
        $search = new \skewer\build\Adm\Tree\Search();
        $search->updateByObjectId( $oNewSection->id );

        return true;
    }

    /**
     * Удаляет ветву дерева разделов
     * @param $id
     * @return bool
     * @throws \Exception
     */
    public static function removeSection( $id ) {

        $oSection = is_object($id) ? $id : self::getSection( $id );

        if ( !$oSection )
            return false;

        return (bool)$oSection->delete();

    }

    /**
     * @param int $section
     * @param int $level
     * @param string $path
     * @param int $defaultSection
     * перенес из TreePrototype::updateAliasPathsRec
     * todo без комментариев
     */
    private static function updateAliasPathsRec( $section=0, $level=0, $path='/', $defaultSection=0 ) {

        // рекурсивно перестроить информацию о маршрутах в таблице main.
        // принцип обхода:
        // вершины с level < 0 не обрабатываются (потомки также не обрабатываются)
        // вершины с visible < 0 или alias = *  - пропускаются в пути


        $aData['level']      = $level++;
        $aData['path']       = $path;
        $aData['section']    = $section;
        $sTableName = TreeSection::tableName();

        // обрабатываем нормальные вершины
        $sQuery = "UPDATE `$sTableName` SET `alias_path`=CONCAT(:path,`alias`,'/'), `level`=:level WHERE parent = :section AND visible IN (0,1) AND alias!='';";

        Query::SQL( $sQuery, $aData );

        // обрабатываем вершины без алиасов
        $sQuery = "UPDATE `$sTableName` SET `alias_path`=CONCAT(:path,`id`,'/'), `level`=:level WHERE parent = :section AND visible IN (0,1) AND alias='' ;";

        Query::SQL( $sQuery, $aData );

        // обрабатываем вершины с пропусками
        $sQuery = "UPDATE `$sTableName` SET `alias_path`=:path, `level`=:level WHERE parent = :section AND visible=2;";

        Query::SQL( $sQuery, $aData );

        // обходим дочерние вершины
        $sQuery = "SELECT id, alias_path, parent FROM `$sTableName` WHERE parent = ? AND level > -1;";

        $oResult = Query::SQL( $sQuery, $aData['section'] );

        while ($item = $oResult->fetchArray() ) {

            $double_slash = strpos($item['alias_path'], '//');

            // исправляем пути если алиас со слешем
            if ($double_slash !== false) {

                $item['alias_path'] = substr($item['alias_path'], $double_slash + 1); // было +2

                $sQuery = "UPDATE `$sTableName` SET `alias_path`=? WHERE id=?;";

                Query::SQL( $sQuery, $item['alias_path'], $item['id'] );
            }


            //             исправляем пути если первый алиас со слешем
            //            отключено за ненадобностью
            //            if (strpos($item['alias_path'], '/') === 0) {
            //
            //                $item['alias_path'] = substr($item['alias_path'], 1);
            //                $sQuery = "
            //                  UPDATE
            //                    `[table_name:q]`
            //                  SET
            //                    `alias_path`=[alias_path:s]
            //                  WHERE
            //                    id=[id:i]";
            //            }
            /** >>>>>>>>>>>>>>>>>>>>>>>>> recursion */
            static::updateAliasPathsRec($item['id'], $level, $item['alias_path'], $defaultSection);

        }// while

        // сбрасываем пути вершин с пропусками, чтобы исключить их из адреса
        if ($level == 1) {

            $sQuery = "UPDATE `$sTableName` SET `alias_path`='' WHERE parent = ? AND visible=2;";

            Query::SQL( $sQuery, $aData['section'] ); // $aData['section'] => $section

            // обходим все пути и проверяем на совпадение
            $last_path = '';

            $sQuery = "SELECT id, alias, alias_path, parent FROM `$sTableName` WHERE alias_path!='' AND visible IN (0,1) AND level > -1 ORDER BY alias_path, level;";

            $oResult = Query::SQL( $sQuery );

            while ($item = $oResult->fetchArray() ){

                //$item['table_name'] = static::getTableName();

                if ($last_path == $item['alias_path']) {

                    $item['new_alias_path'] = substr($item['alias_path'], 0, -1) . '-' . $item['id'] . '/';
                    // todo возможно лишний селект тк new_alias_path имеет уникальный суффикс из id раздела

                    $sQuery = "SELECT COUNT(id) as `alias_duplicates` FROM `$sTableName` WHERE alias_path=? AND visible IN (0,1) AND level > -1;";

                    $oSubresult = Query::SQL( $sQuery, $item['new_alias_path'] );

                    $aAliasDuplicates = $oSubresult->fetchArray();

                    if ($aAliasDuplicates['alias_duplicates'] == 0) {

                        $sQuery = "UPDATE `$sTableName` SET `alias` = CONCAT(`alias`,'-',`id`), `alias_path` = ? WHERE id = ?;";

                        Query::SQL( $sQuery, $item['new_alias_path'], $item['id'] );

                    } // если нет повторений
                } // if

                $last_path = $item['alias_path'];
            } // while
            //
            // обходим вершины которые мы пропускали и восстанавливаем им путь (для ссылок)
            $sQuery = "SELECT id FROM `$sTableName` WHERE `visible` = 2 AND `level` > -1;";

            $oResult = Query::SQL( $sQuery );

            while ( $item = $oResult->fetchArray() ) {
                //                $item['new_alias_path'] = $this->get_first_subsection_path($item['id']);
                //
                //                if ($new_path) {
                //                    $q = "UPDATE " . MINC_DBPREFIX . "main SET `alias_path` ='$new_path'
                //                      WHERE id = " . $item['id'];
                //                    mysql_query($q);
                //                } // if
            } // while
        } // if

        // Убираем дубль главной страницы
        if($level==1 and $defaultSection){

            $sQuery = "UPDATE `$sTableName` SET `alias_path`='' WHERE id=?;";

            Query::SQL( $sQuery, $defaultSection );
        }
    }


    public static function shiftPositionByParent($iParentId, $iPositionId) {

        $sQuery = "UPDATE `tree_section` SET `position`=`position`+1 WHERE `parent` = :parent AND `position` >= :position";

        $oResult = Query::SQL( $sQuery, [
            'parent' => $iParentId,
            'position' => $iPositionId
        ]);

        return $oResult->affectedRows();

    }


    /**
     * Отдает набор шаблонов.
     * Дополняет список фиктивными служебными
     * @param int $iTemplateId
     * @param int $iParentTpl
     * @return array
     * todo переехало из Api::getTemplateList используется Tree\Module::getTemplateList и непонятно что тут происходит
     */
    public static function getTplSection( &$iTemplateId, $iParentTpl ) {

        $aOut = [];

        if ( !($oSection = self::getSection( $iParentTpl )) )
            return $aOut;

        $list = $oSection->getSubSections();

        $bFlag = false;

        /** @var TreeSection $section */
        foreach ( $list as $section ) {
            if ( $section->visible == Visible::VISIBLE ) {

                $aOut[] = [
                    'id' => $section->id,
                    'title' => $section->title,
                ];

                if ( $section->id == $iTemplateId )
                    $bFlag = true;

            }
        }

        if ( !$bFlag ) {
            if ( $list ) {
                $section = reset( $list );
                $iTemplateId = $section->id;
            } else {
                array_unshift($aOut, [
                    'id' => (int)$iTemplateId,
                    'title' => (string)($iTemplateId?:'---')
                ]);
            }
        }

        return $aOut;
    }

}