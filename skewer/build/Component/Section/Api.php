<?php

namespace skewer\build\Component\Section;


/**
 * @todo тесты на этот класс
 * Класс для работы с разделами
 * Class Api
 * @package skewer\build\Component\Section
 */
class Api {

    /**
     * Список вложенных разделов в формате alias=>id
     * @var array
     */
    private static $aAliasSubList = [];


    /**
     * По псевдониму и базовому разделу
     * @param $sAlias
     * @param int $iBaseId
     * @return int
     */
    public static function getIdByAlias( $sAlias, $iBaseId = 0 ) {

        $sAlias = mb_strtolower(trim($sAlias), 'utf-8');

        if (!isset(static::$aAliasSubList[$iBaseId])){
            static::$aAliasSubList[$iBaseId] = static::getAliasSubList( $iBaseId );
        }

        return (isset(static::$aAliasSubList[$iBaseId][$sAlias])) ? static::$aAliasSubList[$iBaseId][$sAlias] : 0;

    }


    /**
     * @todo можно же кешировать куда-нибудь?
     * Список вложенных разделов в формате alias=>id
     * @param $id
     * @param string $label
     * @return array
     */
    public static function getAliasSubList( $id, $label = '/' ) {

        $out = array();

        $tree = Tree::getSubSections( $id );

        foreach ( $tree as $item ) {
            $currentLabel = $label . $item->title . "/";
            $out[mb_strtolower(trim($currentLabel, '/'), 'utf-8')] = $item->id;
            $out = array_merge( $out, self::getAliasSubList( $item->id, $currentLabel ) );
        }

        return $out;
    }


    /**
     * Добавление раздела
     * @param $iParent
     * @param $alias
     * @param $iTemplate
     * @return bool|int
     */
    public static function addSection( $iParent, $alias, $iTemplate ){

        /**
         * @todo предполагаю, что это дублирование, и что чего-нибудь здесь не хватает
         */

//        $oTree = new \Tree();
//
//        $iSection = $oTree->addSection( $iParent, $alias, $iTemplate );

        $oSection = Tree::addSection( $iParent, $alias, $iTemplate );

        if ( $oSection ) {

            $alias = mb_strtolower(trim($alias), 'utf-8');

            /** Добавляем во внутренние массивы */
            if (isset(static::$aAliasSubList[$iParent]))
                static::$aAliasSubList[$iParent][$alias] = $oSection->id;

            foreach( static::$aAliasSubList as &$aAliasList ){
                foreach( $aAliasList as $sAlias => $iKey ){
                    if ($iKey == $iParent){
                        $aAliasList[$sAlias.'/'.$alias] = $oSection->id;
                    }
                }
            }
        }

        return $oSection->id;
    }

}