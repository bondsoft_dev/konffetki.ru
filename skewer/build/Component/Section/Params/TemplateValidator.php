<?php

namespace skewer\build\Component\Section\Params;


use skewer\models\Parameters;
use yii\base\InvalidConfigException;

/**
 * Валидатор для шаблонов
 * Class TemplateValidator
 * @package skewer\build\Component\Section\Params
 */
class TemplateValidator extends \yii\validators\Validator{

    protected $parent = 0;

    protected function validateValue($value) {

        if (!$this->parent)
            return;

        if (!$value)
            throw new InvalidConfigException( 'TemplateValidator: Template can not be empty!' );

        if ((int)$value <= 0)
            throw new InvalidConfigException( 'TemplateValidator: Template not valid!' );

        if ( $this->parent == $value )
            throw new InvalidConfigException( 'TemplateValidator: You can not specify the template itself!' );

        $aChildren = \skewer\build\Component\Section\Parameters::getChildrenList( $this->parent );
        if (!$aChildren)
            $aChildren = [];
        
        if (in_array($value, $aChildren))
            throw new InvalidConfigException( 'TemplateValidator: You can not specify a template of his heirs!' );

    }


    public function validateAttributes($model, $attributes = null) {

        if ($model instanceof Parameters){
            $this->parent = $model->parent;
        }

        parent::validateAttributes($model, $attributes);
    }

}