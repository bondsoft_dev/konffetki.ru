<?php

namespace skewer\build\Component\Section\Params;
use skewer\models\Parameters;


/**
 * Класс для выборки списков параметров
 * Class ListSelector
 * @package skewer\build\Component\Section\Params
 */
class ListSelector {

    /** Все уровни доступа */
    const alAll = 0;

    /** Уровни доступа больше нуля */
    const alPos = 1;

    /** Ненуленые уровни */
    const alEdit = 2;

    /** @var int|[] Родительский раздел */
    private $parent = null;

    /** @var bool|string Группа */
    private $group = false;

    /** @var  bool|string Имя */
    private $name = false;

    /** @var bool Флаг рекурсии */
    private $recursive = false;

    /** @var int Флаг на получение только редактируемых */
    private $level = false;

    /** @var array Набор запрашиваемых полей */
    private $fields = [];

    /** @var bool Флаг получения в виде массивов */
    private $asArray = false;

    /** @var bool Флаг группировки */
    private $groups = false;

    /** @var array Поля сортировки */
    private $order = [];


    /**
     * Установка родительского раздела
     * @param mixed $mParent id раздела, или массив из списка id разделов
     * @return ListSelector
     */
    public function parent( $mParent ){

        if (is_array($mParent))
            $this->parent = array_map( create_function( '$a', 'return (int)$a;'), $mParent );
        else
            $this->parent = (int)$mParent;

        return $this;
    }


    /**
     * Установка группы для поиска
     * @param $sGroupName
     * @return ListSelector
     */
    public function group( $sGroupName ){
        $this->group = $sGroupName;
        return $this;
    }


    /**
     * Установка имени для поиска
     * @param $sParamName
     * @return ListSelector
     */
    public function name( $sParamName ){
        $this->name = $sParamName;
        return $this;
    }


    /**
     * @todo тесты на сортировку
     * Добавить поле сортировки
     * @param $sFieldName
     * @param $sType
     * @return ListSelector
     */
    public function addOrder( $sFieldName, $sType = 'ASC' ){
        $this->order[$sFieldName] = ($sType == 'ASC')?SORT_ASC:SORT_DESC;
        return $this;
    }


    /**
     * Установка флага рекурсии
     * Ищет рекурсивно только для одного заданного раздела
     * @param bool $bRec
     * @return ListSelector
     */
    public function rec( $bRec = true ){
        $this->recursive = (bool) $bRec;
        return $this;
    }


    /**
     * Установка флага только редактируемых полей
     * @param int $iLevel
     * @return ListSelector
     */
    public function level( $iLevel = ListSelector::alAll ){
        $this->level = $iLevel;
        return $this;
    }


    /**
     * Установка списка запрашиваемых полей.
     * Поля id, group, name присутствуют всегда. Поле parent присутствует всегда, если заданы разделы
     * @param array $aFields
     * @return ListSelector
     */
    public function fields( array $aFields ){
        $this->fields = array_intersect( $aFields, Parameters::getAttributeList() );
        return $this;
    }


    /**
     * Установка флага возвращать в виде массивов
     * @return ListSelector
     */
    public function asArray(){
        $this->asArray = true;
        return $this;
    }


    /**
     * Установка флага группировки
     * @return ListSelector
     */
    public function groups(){
        $this->groups = true;
        return $this;
    }


    /**
     * Получение списка параметров
     * @return array|Parameters[]
     */
    public function get(){

        /** Запрос списка параметров */
        $oQuery = Parameters::find();

        /** Родительский раздел */
        if (!is_null( $this->parent ))
            $oQuery->where(['parent' => $this->parent]);

        /** Только редактируемые */
        switch ($this->level){
            case static::alEdit:
                $oQuery->andWhere('access_level != 0');
                if (count($this->fields) && !in_array('access_level', $this->fields))
                    $this->fields[] = 'access_level';
                break;
            case static::alPos:
                $oQuery->andWhere('access_level > 0');
                if (count($this->fields) && !in_array('access_level', $this->fields))
                    $this->fields[] = 'access_level';
                break;
        }

        /** Группа */
        if ($this->group)
            $oQuery->andWhere(['group' => $this->group]);

        /** Имя */
        if ($this->name)
            $oQuery->andWhere(['name' => $this->name]);

        /** Поля */
        if (count($this->fields)){
            $this->fields[] = 'group';
            $this->fields[] = 'name';
            $this->fields[] = 'id';

            $oQuery->select(array_unique($this->fields));
        }

        /** В виде массивов */
        if ( $this->asArray )
            $oQuery->asArray();

        $asArray = $this->asArray;

        /** Сортировка */
        if ($this->order){
            $oQuery->orderBy($this->order);
        }

        /**
         * Поиск параметра по группе и имени
         * @param $aData
         * @param $sGroup
         * @param $sName
         * @return Parameters[]|bool
         */
        $fGetByName = function( $aData, $sGroup, $sName ) use ( $asArray ) {
            if (!is_array($aData))
                return false;
            foreach( $aData as $aParam ){

                if ( $asArray ){
                    if ( $aParam['group'] == $sGroup && $aParam['name'] == $sName ){
                        return $aParam;
                    }
                }else{

                    if (!$aParam instanceof Parameters)
                        continue;
                    if ( $aParam->group == $sGroup && $aParam->name == $sName ){
                        return $aParam;
                    }

                }

            }
            return false;
        };

        $aParams = $oQuery->all();

        $bGroups = $this->groups;

        /** Рекурсивно по шаблонам */
        if ( $this->recursive && !is_null($this->parent) && !is_array($this->parent) ){
            /** Поиск шаблона */
            $aParam = $fGetByName( $aParams, \skewer\build\Component\Section\Parameters::settings, \skewer\build\Component\Section\Parameters::template );

            /** @var Parameters $aParam */
            if ($aParam){
                $this->parent = $asArray?$aParam['value']:$aParam->value;
            }
            if (!$aParam)
                $this->parent = \skewer\build\Component\Section\Parameters::getTpl( $this->parent );

            if ($this->parent){
                /** Выборка параметров из шаблона */
                $this->groups = false;
                $aTplParams = self::get();

                foreach($aTplParams as $aParam){
                    if (!$fGetByName( $aParams,
                        $asArray?$aParam['group']:$aParam->group,
                        $asArray?$aParam['name']:$aParam->name ))
                        $aParams[] = $aParam;
                }
            }

        }

        /** Группировка */
        if ($bGroups){
            $aParamList = [];
            foreach( $aParams as $aParam ){
                $aParamList[$asArray?$aParam['group']:$aParam->group][] = $aParam;
            }
            return $aParamList;
        }

        return $aParams;

    }
}