<?php

namespace skewer\build\Component\Section\Params;


/**
 * Класс, содержащий сведения о типах параметров
 * Class Type
 * @package skewer\build\Component\Section\Params
 */
class Type{

    /** Системный параметр */
    const paramSystem = 0;

    /** Строка */
    const paramString = 1;

    /** Текстовое поле */
    const paramText = 2;

    /** Редактор */
    const paramWyswyg = 3;

    /** Картинка */
    const paramImage = 4;

    /** Галочка */
    const paramCheck = 5;

    /** Файл */
    const paramFile = 6;

    /** HTML */
    const paramHTML = 7;

    /** Выпадающий список */
    const paramSelect = 9;

    /** Целое число */
    const paramInt = 10;

    /** Число с плавающей точкой */
    const paramFloat = 11;

    /** Дата */
    const paramData = 15;

    /** Время */
    const paramTime = 16;

    /** Дата и время */
    const paramDataTime = 17;

    /** Наследуемый */
    const paramInherit = 20;

    /** Системный раздел */
    const paramServiceSection = 23;

    /** Языковой параметр */
    const paramLanguage = 24;

    /** Текст HTML */
    const paramTextHTML = 31;

    /** Текст JS */
    const paramTextJS = 32;

    /** Текст Css */
    const paramTextCss = 33;

    /**
     * Список типов полей, использующих расширенное значение
     * @return array
     */
    public static function getShowValFieldList(){
        return[
            self::paramText,
            self::paramWyswyg,
            self::paramHTML,
            self::paramSelect,
            self::paramTextHTML,
            self::paramTextJS,
            self::paramTextCss
        ];
    }

    /**
     * Список типов текстовых полей
     * @return array
     */
    public static function getTextFieldList(){
        return[
            self::paramText,
            self::paramHTML,
            self::paramTextHTML,
            self::paramTextJS,
            self::paramTextCss
        ];
    }

    /**
     * Список параметров для метки.
     * @return array
     */
    public static function getParametersList() {
        return array(

            self::paramSystem => \Yii::t('params', 'type_system'),
            self::paramString => \Yii::t('params', 'type_string'),
            self::paramText => \Yii::t('params', 'type_text'),
            self::paramWyswyg => \Yii::t('params', 'type_wyswyg'),
            self::paramImage => \Yii::t('params', 'type_imagefile'),
            self::paramCheck => \Yii::t('params', 'type_check'),
            self::paramFile => \Yii::t('params', 'type_file'),
            self::paramHTML => \Yii::t('params', 'type_html'),
            self::paramSelect => \Yii::t('params', 'type_select'),
            self::paramInt => \Yii::t('params', 'type_int'),
            self::paramFloat => \Yii::t('params', 'type_float'),
            self::paramData => \Yii::t('params', 'type_date'),
            self::paramTime => \Yii::t('params', 'type_time'),
            self::paramDataTime => \Yii::t('params', 'type_datetime'),
            self::paramInherit => \Yii::t('params', 'type_inherit'),
            self::paramServiceSection => \Yii::t('params', 'type_service_section'),
            self::paramLanguage => \Yii::t('params', 'type_language'),
            self::paramTextHTML => \Yii::t('params', 'type_text_html'),
            self::paramTextJS => \Yii::t('params', 'type_text_js'),
            self::paramTextCss => \Yii::t('params', 'type_text_css'),

            -self::paramString => \Yii::t('params', 'type_string_local'),
            -self::paramText => \Yii::t('params', 'type_text_local'),
            -self::paramWyswyg => \Yii::t('params', 'type_wyswyg_local'),
            -self::paramImage => \Yii::t('params', 'type_imagefile_local'),
            -self::paramCheck => \Yii::t('params', 'type_check_local'),
            -self::paramFile => \Yii::t('params', 'type_file_local'),
            -self::paramHTML => \Yii::t('params', 'type_html_local'),
            -self::paramSelect => \Yii::t('params', 'type_select_local'),
            -self::paramInt => \Yii::t('params', 'type_int_local'),
            -self::paramFloat => \Yii::t('params', 'type_float_local'),
            -self::paramData => \Yii::t('params', 'type_date_local'),
            -self::paramTime => \Yii::t('params', 'type_time_local'),
            -self::paramDataTime => \Yii::t('params', 'type_datetime_local'),
            -self::paramInherit => \Yii::t('params', 'type_inherit_local'), // такое бывает?
            -self::paramServiceSection => \Yii::t('params', 'type_service_section'),
            -self::paramLanguage => \Yii::t('params', 'type_language'),
            -self::paramTextHTML => \Yii::t('params', 'type_text_html_local'),
            -self::paramTextJS => \Yii::t('params', 'type_text_js_local'),
            -self::paramTextCss => \Yii::t('params', 'type_text_css_local'),
        );
    }


    /**
     * @static возвращает массив с описаниями типов по access_level
     * @return array
     */
    public static function getParamTypes(){

        return array(
            self::paramString => array( 'type'=>'string', 'val'=> 'value'),
            self::paramText => array( 'type'=>'text', 'val'=> 'show_val' ),
            self::paramWyswyg => array( 'type'=>'wyswyg', 'val'=> 'show_val' ),
            self::paramImage => array( 'type'=>'file', 'val'=> 'value'),
            self::paramCheck => array( 'type'=>'check', 'val'=> 'value'),
            self::paramFile => array( 'type'=>'file', 'val'=> 'value'),
            self::paramHTML => array( 'type'=>'html', 'val'=> 'show_val' ),
            self::paramSelect => array( 'type'=>'select', 'val'=> 'value'),
            self::paramInt => array( 'type'=>'int', 'val'=> 'value'),
            self::paramFloat => array( 'type'=>'float', 'val'=> 'value'),
            self::paramData => array( 'type'=>'date', 'val'=> 'value'),
            self::paramTime => array( 'type'=>'time', 'val'=> 'value'),
            self::paramDataTime => array( 'type'=>'datetime', 'val'=> 'value'),
            self::paramInherit => array( 'type'=>'inherit', 'val'=> 'value'),
            self::paramServiceSection => array( 'type'=>'service_section', 'val'=> 'value'),
            self::paramLanguage => array( 'type'=>'language', 'val'=> 'value'),
            self::paramTextHTML => array( 'type'=>'text_html', 'val'=> 'show_val'),
            self::paramTextJS => array( 'type'=>'text_js', 'val'=> 'show_val'),
            self::paramTextCss => array( 'type'=>'text_css', 'val'=> 'show_val')
        );
    }


    /**
     * Проверяет, использует ли тип расширеное значение
     * @param $sType
     * @return bool
     */
    public static function hasShowValUseType( $sType ){
        return in_array($sType, ['text', 'wyswyg', 'html', 'text_html', 'text_js', 'text_css']);
    }

}