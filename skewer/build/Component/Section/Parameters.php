<?php

namespace skewer\build\Component\Section;


use skewer\build\Component\Section\Params\ListSelector;
use \skewer\models\Parameters as Params;
use yii\helpers\ArrayHelper;

/**
 * Фасад для работы с параметрами
 * Class Parameters
 * @package skewer\build\Component\Section
 */
class Parameters {

    /** Название параметра-шаблона раздела */
    const template = 'template';

    /** Имя группы настроек раздела */
    const settings = '.';

    /** Метка модуля */
    const object = 'object';

    /** Метка админского модуля */
    const objectAdm = 'objectAdm';

    /** Имя группы */
    const groupName = '.groupTitle';


    /** имя параметра "выводить шаблонами для подразделлов - наследников шаблона текущей страницы" */
    const SubSectonTpl = '__section_sub_tpl';

    /** имя параметра "выводить допустимые родительские разделы" */
    const HideParents = '__section_hide_parents';

    /** имя параметра "Добавлять в родительский раздел" */
    const AddToParent = '__section_add_to_parent';

    /** имя параметра 'Скрыть вкладку "Редактор"' */
    const HideEditor = '__section_hide_editor';

    /** Название параметра языка */
    const language = 'language';


    /**
     * Создает параметр с данными
     * @param array $aData
     * @return Params
     */
    public static function createParam( $aData = [] ){
        $oParam = new Params();
        $oParam->setAttributes( $aData );
        return $oParam;
    }


    /**
     * Копирует параметр $oParam в раздел $iNewSection, возвращает новый параметр или false.
     * Не заменяет существующие параметры!
     * @param Params $oParam
     * @param $iNewSection
     * @param $value
     * @return Params|bool
     */
    public static function copyToSection( \skewer\models\Parameters $oParam, $iNewSection, $value = null ){

        $oNewParam = self::createParam($oParam->getAttributes( null, ['id', 'parent']) );
        $oNewParam->parent = $iNewSection;
        if (!is_null($value))
            $oNewParam->value = $value;
        return ($oNewParam->save()) ? $oNewParam : false;

    }


    /**
     * Получение параметра по id
     * @param $id
     * @return null|Params
     */
    public static function getById( $id ){
        $id = (int)$id;
        return Params::findOne($id);
    }


    /**
     * Выбор параметра по разделу, имени и группе
     * @param $iParent
     * @param $sGroupName
     * @param $sParamName
     * @param bool $bRec
     * @param array $aSelect - список выбираемых полей
     * @return false|Params
     */
    private static function getParamByName( $iParent, $sGroupName, $sParamName, $bRec = false, $aSelect = [] ){

        $iParent = (int)$iParent;

        $oQuery = Params::find()
            ->where( ['parent' => $iParent, 'name' => $sParamName, 'group' => $sGroupName] );

        if ($aSelect && is_array($aSelect)){
            $oQuery->select( array_intersect( $aSelect, Params::getAttributeList() ) );
        }

        $oParam = $oQuery->one();

        if ( $bRec && is_null($oParam) ){
            $iTpl = self::getTpl( $iParent );
            if ($iTpl)
                $oParam = self::getParamByName( $iTpl, $sGroupName, $sParamName, true, $aSelect );
        }

        if (is_null($oParam))
            $oParam = false;

        return $oParam;
    }


    /**
     * Выбор параметра по разделу, имени и группе
     * @param $iParent
     * @param $sGroupName
     * @param $sParamName
     * @param $bRec
     * @return false|Params
     */
    public static function getByName( $iParent, $sGroupName, $sParamName, $bRec = false ){
        return self::getParamByName( $iParent, $sGroupName, $sParamName, $bRec );
    }


    /**
     * Возвращает значение параметра по разделу, имени и группе или false, если параметр не найден
     * @param $iParent
     * @param $sGroupName
     * @param $sParamName
     * @param $bRec
     * @return string|false
     */
    public static function getValByName( $iParent, $sGroupName, $sParamName, $bRec = false ){
        /** @var Params $oParam */
        $oParam = self::getParamByName( $iParent, $sGroupName, $sParamName, $bRec, ['value'] );
        return $oParam ? $oParam->value : false;
    }


    /**
     * Возвращает текстовое значение параметра по разделу, имени и группе или false, если параметр не найден
     * @param $iParent
     * @param $sGroupName
     * @param $sParamName
     * @param $bRec
     * @return string|false
     */
    public static function getShowValByName( $iParent, $sGroupName, $sParamName, $bRec = false ){
        /** @var Params $oParam */
        $oParam = self::getParamByName( $iParent, $sGroupName, $sParamName, $bRec, ['show_val'] );
        return $oParam ? $oParam->show_val : false;
    }


    /**
     * Возвращает объект для выбоки списка параметров
     * @param int $iParent
     * @return $this|ListSelector
     */
    public static function getList( $iParent = null ){
        if (!is_null( $iParent ))
            return (new ListSelector())->parent( $iParent );
        else
            return new ListSelector();
    }


    /**
     * Список разделов, для которых данные разделы присутствуют в цепочке наследования по шаблонам
     * @param $mParent
     * @return array|bool
     */
    public static function getChildrenList( $mParent ){
        if (!is_array($mParent))
            $mParent = [(int)$mParent];

        $aTplParams = Params::find()
            ->where( ['name' => static::template, 'group' => static::settings, 'value' => $mParent] )
            ->select(['parent'])
            ->asArray()
            ->all();

        $aTplParams = ArrayHelper::map( $aTplParams, 'parent', 'parent' );

        if ($aTplParams){
            $aSubTplParams = self::getChildrenList( array_diff($aTplParams, $mParent) );
            if ($aSubTplParams)
                return array_unique(array_merge($aTplParams, $aSubTplParams));
            return $aTplParams;
        }

        return false;
    }


    /**
     * Список разделов с модулем в группе
     * @param $sModule
     * @param $sGroupName
     * @param $sName
     * @return array
     */
    public static function getListByModule( $sModule, $sGroupName, $sName = Parameters::object ){

        $aParams = Params::find()
            ->select('parent')
            ->where(['group' => $sGroupName, 'name' => $sName, 'value' => $sModule])
            ->asArray()
            ->all();

        $aParams = ArrayHelper::map( $aParams, 'parent', 'parent' );

        if ($aParams){
            $aTplParams = self::getChildrenList( $aParams );
            if ($aTplParams){
                /** Исключаем разделы с перекрытым модулем */
                $aModulesSections = Params::find()
                    ->select('parent')
                    ->where(['group' => $sGroupName, 'name' => $sName])
                    ->asArray()
                    ->all();

                $aModulesSections = ArrayHelper::map( $aModulesSections, 'parent', 'parent' );
                $aParams = array_merge( $aParams, array_diff($aTplParams, $aModulesSections) );
            }
        }

        return $aParams;
    }


    /**
     * Возвращает шаблон раздела
     * @param $iSection
     * @return int|false
     */
    public static function getTpl( $iSection ){

        $oParam = self::getParamByName( $iSection, static::settings, static::template, false, ['value'] );
        return $oParam ? (int)$oParam->value : false;

    }


    /**
     * Цепочка шаблонов, от которого наследуется раздел
     * @param $iSection
     * @return int[] массив id шаблонных разделов|пустой массив, если нет
     */
    public static function getParentTemplates( $iSection ){

        $iTpl = self::getTpl( $iSection );

        if ($iTpl){
            $aParents = self::getParentTemplates( $iTpl );
            if ($aParents){
                $aParents[] = $iTpl;
                return $aParents;
            }else{
                return [$iTpl];
            }

        }

        return [];
    }


    /**
     * Обновляет значения параметров по группе и имени, возвращает количество измененных параметров
     * @param $sParamGroup
     * @param $sParamName
     * @param $value
     * @return int
     */
    public static function updateByName( $sParamGroup, $sParamName, $value ){

        if ( !$sParamName || !$sParamGroup )
            return 0;

        return Params::updateAll(['value' => $value], ['group' => $sParamGroup, 'name' => $sParamName]);

    }


    /**
     * Установка параметра. Ищет параметр по родителю, группе и имени или создает новый
     * и сохраняет его с указанными атрибутами
     * Возвращает id параметра или false
     * @param $iSection
     * @param $sGroup
     * @param $sName
     * @param $sVal
     * @param string $sShowVal
     * @param string $sTitle
     * @param int $iAccessLevel
     * @return int|false
     */
    public static function setParams( $iSection, $sGroup, $sName, $sVal = null , $sShowVal = null, $sTitle = null, $iAccessLevel = null ){
        $oParam = self::getByName( $iSection, $sGroup, $sName);
        if (!$oParam)
            $oParam = self::createParam(['parent' => $iSection, 'group' => $sGroup, 'name' => $sName]);
        if ($oParam){
            if (!is_null($sVal))
                $oParam->value = $sVal;
            if (!is_null($sShowVal))
                $oParam->show_val = $sShowVal;
            if (!is_null($sTitle))
                $oParam->title = $sTitle;
            if (!is_null($iAccessLevel))
                $oParam->access_level = $iAccessLevel;

            if ($oParam->save())
                return $oParam->id;
        }

        return false;
    }


    /**
     * Удаление параметра по родителю имени и группе
     * @param $iParent
     * @param $sGroup
     * @param $sName
     * @return int
     */
    public static function removeByName( $iParent, $sGroup, $sName ){
        return Params::deleteAll([
            'parent' => $iParent,
            'group' => $sGroup,
            'name' => $sName,
        ]);
    }


    /**
     * Удаление разделов по id
     * @param $mId
     * @return int
     */
    public static function removeById( $mId ){
        return Params::deleteAll([
            'id' => $mId
        ]);
    }

    /**
     * Список доступных параметров для объекта
     * @param $sObjectModel
     * @return array
     */
    public static function getParamTplList($sObjectModel){

        $aParamList = get_class_vars($sObjectModel);

        if ($aParamList){
            $aResult = array();
            foreach($aParamList as $sKey=>$item){
                // если строка, число или bool
                if (is_string($item) || is_numeric($item) || is_bool($item))
                    $aResult[$sKey] = $sKey;
            }
            return $aResult;
        }

        return array();
    }


    /**
     * Язык раздела.
     * Ищет в разделе или его родителях
     * @param $iPage
     * @return string
     */
    public static function getLanguage( $iPage ){

        $aParentSections = Tree::getSectionParents($iPage, -1, true);
        array_unshift($aParentSections, $iPage);

        $aLangParams = self::getList($aParentSections)
            ->group(self::settings)
            ->name(self::language)
            ->asArray()
            ->get();

        $aLangParams = ArrayHelper::map($aLangParams, 'parent', 'value');

        foreach( $aParentSections as $iSection ){
            if (isset($aLangParams[$iSection]))
                return $aLangParams[$iSection];
        }

        return '';
    }
} 