<?php
 /**
 * Менеджер проверки целостности и состояния файлов модуля.
 * @author ArmiT
 * @date 03.02.14
 * @project canape
 * @package 
 * @subpackage 
 */

namespace skewer\build\Component\Installer;
use skewer\core\Component\Config as Config;


class IntegrityManager {

    /**
     * @const int MODULE_NAMESPACE Паттерн для сборки пространства имен
     */
    const MODULE_NAMESPACE = 'skewer\\build\\%s\\%s';

    /**
     * @const int MODULE_INSTALL Паттерн сборки имени файла установки
     */
    const MODULE_INSTALL = '%s%sInstall';

    /**
     * @const int MODULE_ASSET Паттерн сборки имени файла
     */
    const MODULE_ASSET = '%s%sAsset';

    /**
     * @const int MODULE_CONFIG Паттерн сборки имени файла конфигурации
     */
    const MODULE_CONFIG = '%s%sConfig.php';

    /**
     * @const int MODULE_LANG Паттерн сборки имени файла словарей
     */
    const MODULE_LANG = '%s%sLanguage.php';

    /**
     * @const int MODULE_PRESETDATA Паттерн сборки имени файла предустановленных данных
     */
    const MODULE_PRESETDATA = '%s%sPresetData.php';

    /**
     * @const int MODULE_MAIN Паттерн сборки имени исполняемого файла модуля
     */
    const MODULE_MAIN = '%s%sModule';

    /**
     * Инициализирует класс проверки целостности данных модулю. Производит проверку на наличие валидных входных данных,
     * наличие файла установки, конфигурации и исполняемого файла модуля.
     * @param $moduleName
     * @param $layer
     * @return \skewer\build\Component\Installer\Module Возвращает экземпляр Класса Installer\Module
     * @throws Exception
     */
    public static function init($moduleName, $layer) {

        if(empty($moduleName))
            throw new Exception('Name for module is empty');

        if(empty($layer))
            throw new Exception('Layer for module is empty');

        $module = new Module();

        $module->moduleName = $moduleName;
        $module->layer = $layer;

        /*
         * Проверить существование исполняемого класса модуля с пространством имен и без
         */

        if(!class_exists($exec = self::getModuleClass($moduleName, $layer, $module->useNamespace))){

            $module->useNamespace = !$module->useNamespace;

            if(!class_exists($exec = self::getModuleClass($moduleName, $layer, $module->useNamespace)))
                throw new Exception('Module executor ['.$layer.'\\'.$moduleName.'] does not exist');
        }

        /*
         * Получить путь до корневой директории модуля
         */
        $modulePath = \Yii::getAlias('@' . str_replace('\\', '/', $exec) . '.php');
        if(!$modulePath)
            throw new Exception('Module executor file['.$exec.'] not found');

        /* отрезаем имя исполняемого фалйла - получаем путь к корневому каталогу модуля в кластере */
        $module->moduleRootDir = dirname($modulePath).DIRECTORY_SEPARATOR;

        // Проверяем наличие файла конфигурации
        if(!file_exists($configFile = self::getConfigFile(
            $moduleName,
            $layer,
            $module->moduleRootDir,
            $module->useNamespace)))
                throw new Exception('Config file ['.$configFile.'] for module ['.$moduleName.'] does not exist');

        $module->configFile = $configFile;
        $module->moduleConfig = self::loadConfigFile($configFile);

        if (file_exists($langFile = self::getLangFile(
            $moduleName,
            $layer,
            $module->moduleRootDir,
            $module->useNamespace))){

            $module->languageFile = $langFile;
        }

        if (file_exists($langFile = self::getPresetDataFile(
            $moduleName,
            $layer,
            $module->moduleRootDir,
            $module->useNamespace))){

            $module->presetDataFile = $langFile;
        }


        /** Имя категории для словарей */
        $module->languageCategory = $module->moduleConfig->getLanguageCategory();

        /*
         * Проверить существование класса установки с пространством имен и без
         */

        if(!class_exists($install = self::getInstallClass($moduleName, $layer, $module->useNamespace)))
                throw new Exception('Installation file for module ['.$moduleName.'] does not exist');

        $module->installClass = $install;

        $module->assetClass = self::getAssetClass($moduleName, $layer, $module->useNamespace);

        $module->installFile;

        $module->alreadyInstalled = \Yii::$app->register->moduleExists($moduleName, $layer);

        return $module;
    }

    /**
     * Возвращает сформированное имя класса установки для модуля $moduleName в слое $layer
     * @param string $moduleName Имя модуля, путь к классу установки которого нужно получить
     * @param string $layer Имя слоя модуля
     * @param bool $withNamespace Если установлена в  true - предполагаемое имя класса формируется с четом пространства имен
     * @return string
     */
    protected static function getInstallClass($moduleName, $layer, $withNamespace = false) {

        return $withNamespace?
            sprintf(self::MODULE_NAMESPACE.'%s', $layer, $moduleName, '\Install'):
            sprintf(self::MODULE_ASSET, $moduleName, $layer);

    }

    /**
     * Возвращает сформированное имя класса ассета для модуля $moduleName в слое $layer
     * @param string $moduleName Имя модуля, путь к классу установки которого нужно получить
     * @param string $layer Имя слоя модуля
     * @param bool $withNamespace Если установлена в  true - предполагаемое имя класса формируется с четом пространства имен
     * @return string
     */
    protected static function getAssetClass($moduleName, $layer, $withNamespace = false) {

        return $withNamespace?
            sprintf(self::MODULE_NAMESPACE.'%s', $layer, $moduleName, '\Asset'):
            sprintf(self::MODULE_INSTALL, $moduleName, $layer);

    }

    /**
     * Возвращает полное имя файла конфигурации модуля
     * @param string $moduleName Имя модуля
     * @param string $layer Имя слоя
     * @param string $moduleRootPath Путь к корневой директории модуля
     * @param bool $withNamespace Если указано true - предполагаемое имя файла формируется с учетом пространства имен
     * @return string
     */
    protected static function getConfigFile($moduleName, $layer, $moduleRootPath, $withNamespace = false) {

        return $withNamespace?
            $moduleRootPath.'Config.php':
            $moduleRootPath.sprintf(self::MODULE_CONFIG, $moduleName, $layer);
    }

    /**
     * Возвращает полное имя файла словарей модуля
     * @param string $moduleName Имя модуля
     * @param string $layer Имя слоя
     * @param string $moduleRootPath Путь к корневой директории модуля
     * @param bool $withNamespace Если указано true - предполагаемое имя файла формируется с учетом пространства имен
     * @return string
     */
    protected static function getLangFile($moduleName, $layer, $moduleRootPath, $withNamespace = false) {

        return $withNamespace?
            $moduleRootPath.'Language.php':
            $moduleRootPath.sprintf(self::MODULE_LANG, $moduleName, $layer);
    }

    /**
     * Возвращает полное имя файла предустановленных данных модуля
     * @param string $moduleName Имя модуля
     * @param string $layer Имя слоя
     * @param string $moduleRootPath Путь к корневой директории модуля
     * @param bool $withNamespace Если указано true - предполагаемое имя файла формируется с учетом пространства имен
     * @return string
     */
    protected static function getPresetDataFile($moduleName, $layer, $moduleRootPath, $withNamespace = false) {

        return $withNamespace?
            $moduleRootPath.'PresetData.php':
            $moduleRootPath.sprintf(self::MODULE_PRESETDATA, $moduleName, $layer);
    }

    /**
     * Проверяет возможность чтения файла конфигурации, расположенного по пути $moduleConfigPath.
     * В случае успешной проверки загружает его и создает экземляр класса Config\ModuleConfig
     * @param $moduleConfigPath
     * @return Config\ModuleConfig Возвращает экземпляр класса хранения настроек модуля
     * @throws Exception В случае ошибки выбрасывает исключение skewer\build\Component\Installer\Exception либо
     * \Exception в зависимости от уровня произошедшей ошибки
     */
    protected static function loadConfigFile($moduleConfigPath) {

        if(!is_readable($moduleConfigPath))
            throw new Exception('Configuration file ['.$moduleConfigPath.'] is not readable');

        $moduleConfig = new Config\ModuleConfig(include($moduleConfigPath));

        return $moduleConfig;
    }

    /**
     * Возвращает предполагаемое имя основного исполняемого класса модуля
     * @param string $moduleName Имя модуля (без указания мнемоник и слоя)
     * @param string $layer Имя слоя
     * @param bool $withNamespace Если указано true - возвращает имя класса с учетом пространства имен
     * @return string
     */
    protected static function getModuleClass($moduleName, $layer, $withNamespace = false) {

        return $withNamespace?
            sprintf(self::MODULE_NAMESPACE.'%s', $layer, $moduleName, '\Module'):
            sprintf(self::MODULE_MAIN, $moduleName, $layer);
    }

}
