<?php
 /**
 * @class RegisterLanguage
 * @author ArmiT
 * @date 24.01.14
 * @project canape
 * @package Component
 * @subpackage Installer
 */

namespace skewer\build\Component\Installer\SystemAction\Install;


use skewer\build\Component\Installer as Installer;
use skewer\build\Component\I18N\Categories;

class RegisterLanguage extends Installer\Action {

    public function init() {}

    public function execute() {

        Categories::updateModuleLanguageValues($this->module);

    }

    public function rollback() {

    }
}
