<?php

namespace skewer\build\Component\Installer\SystemAction\Install;
use skewer\build\Component\Installer as Installer;
use skewer\build\Component\Installer\SystemAction\Reinstall\LangUpdateHelper;
use yii\web\AssetBundle;


class RegisterCss extends Installer\Action {

    public function init() {}

    public function execute() {

        if (!class_exists($this->module->assetClass))
            return;

        /** @var AssetBundle $oAsset */
        $oAsset = new $this->module->assetClass;

        $aCss = $oAsset->css;

        if ($aCss){

            foreach( $aCss as $sCssFile ){

                $oCSSParser = new \CSSParser();

                $oCSSParser->analyzeFile( $this->module->moduleRootDir . "web/$sCssFile" );

                $oCSSParser->updateDesignSettings();

            }

        }

    }

    public function rollback() {

    }
}
