<?php
 /**
 * @class RegisterConfig
 * @author ArmiT
 * @date 24.01.14
 * @project canape
 * @package Component
 * @subpackage Installer
 */

namespace skewer\build\Component\Installer\SystemAction\Install;
use skewer\build\Component\Installer as Installer;
use skewer\core\Component\Config as Config;


class RegisterConfig extends Installer\Action {

    public function init() {}

    public function execute() {

        \ConfigUpdater::buildRegistry()->registerModule( $this->module->moduleConfig );
    }

    public function rollback() {

        \ConfigUpdater::buildRegistry()->removeModule( $this->module->moduleName, $this->module->layer );
    }


} 