<?php
 /**
  * @class UnregisterLanguage
  * @author ArmiT
  * @date 24.01.14
  * @project canape
  * @package Component
  * @subpackage Installer
  */

namespace skewer\build\Component\Installer\SystemAction\Uninstall;


use skewer\build\Component\Installer as Installer;
use skewer\build\Component\I18N\Categories;

class UnregisterLanguage extends Installer\Action {

    public function init() {}

    public function execute() {
        /** @noinspection PhpUndefinedMethodInspection */
        \Yii::$app->getI18n()->clearCache();
    }

    public function rollback() {

        Categories::updateModuleLanguageValues($this->module);
        /** @noinspection PhpUndefinedMethodInspection */
        \Yii::$app->getI18n()->clearCache();
    }
}