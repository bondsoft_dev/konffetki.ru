<?php
 /**
 * @class InstallDependency
 * @author ArmiT
 * @date 24.01.14
 * @project canape
 * @package Component
 * @subpackage Installer
 */

namespace skewer\build\Component\Installer\SystemAction\Uninstall;
use skewer\build\Component\Installer as Installer;


class ExecuteModuleInstructions extends Installer\Action {

    /**
     * Экземпляр класса установки модуля
     * @var null|\skModuleInstall
     */
    protected $installer = null;

    public function init() {

        $installer = $this->module->installClass;

        /** @var \skModuleInstall $moduleInstaller */
        $this->installer = new $installer($this->module->moduleConfig);

    }

    public function execute() {

        if($this->installer->init())
            $this->installer->uninstall();

    }

    public function rollback() {}


} 