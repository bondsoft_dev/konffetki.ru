<?php

namespace skewer\build\Component\Installer\SystemAction\Reinstall;


use skewer\build\Component\Command\Action;
use skewer\build\Component\I18N\models\LanguageValues;
use skewer\build\Component\Installer as Installer;
use skewer\core\Component\Config as Config;

class ClearLanguage extends Action {

    public function init() {}

    public function execute() {

        LanguageValues::deleteAll([
            'override' => LanguageValues::overrideNo,
            'language' => LanguageValues::getSystemLanguage()
        ]);

        \Yii::$app->getI18n()->clearCache();

    }

    public function rollback() {

    }


} 