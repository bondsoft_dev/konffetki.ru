<?php
/**
 * Created by PhpStorm.
 * User: Александр
 * Date: 25.06.2015
 * Time: 14:00
 */

namespace skewer\build\Component\Installer\SystemAction\Reinstall;

use skewer\build\Component\Command as Command;
use skewer\build\Component\I18N\Categories;

/**
 * Преустанавливает языковые значения для заданного компонента из файла
 * Class LanguageFile
 * @package skewer\build\Component\Installer\SystemAction\Reinstall
 */
class ComponentLanguage extends Command\Action {

    /** @var string имя окомпонента */
    private $sName = '';

    /** @var string путь до файла */
    private $sPath = '';

    /** @var bool Флаг предустановленных контентных данных */
    private $isData = false;

    /**
     * @param string $sName имя компонента
     * @param string $sPath путь до файла
     * @param bool $bData Флаг предустановленных контентных данных
     */
    function __construct( $sName, $sPath, $bData = false ) {
        $this->sName = $sName;
        $this->sPath = $sPath;
        $this->isData = $bData;
    }

    /**
     * Инициализация
     * Добавление слушателей событий
     */
    protected function init() {
    }

    /**
     * Выполнение команды
     * @throws \Exception
     */
    function execute() {

        Categories::updateByCategory($this->sName, RELEASEPATH.$this->sPath, $this->isData);

    }

    /**
     * Откат команды
     */
    function rollback() {
    }

}