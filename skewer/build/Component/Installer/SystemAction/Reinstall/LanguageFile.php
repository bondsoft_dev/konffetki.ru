<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 11.04.14
 * Time: 16:41
 */

namespace skewer\build\Component\Installer\SystemAction\Reinstall;


use skewer\build\Component\Command as Command;
use skewer\build\Component\Installer\Module;
use skewer\build\Component\I18N\Categories;

/**
 * Преустанавливает языковые значения для заданного модуля из файла
 * Class LanguageFile
 * @package skewer\build\Component\Installer\SystemAction\Reinstall
 */
class LanguageFile extends Command\Action {

    protected $module = null;

    public function __construct(Module $module) {
        $this->module = $module;
    }

    protected function init() {
    }

    function execute() {

        Categories::updateByCategory($this->module->languageCategory, $this->module->languageFile);
        Categories::updateByCategory($this->module->languageCategory, $this->module->presetDataFile, true);

    }

    function rollback() {
    }

}