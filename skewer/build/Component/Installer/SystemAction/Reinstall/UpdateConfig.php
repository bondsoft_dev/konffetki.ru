<?php
 /**
 * @class RegisterConfig
 * @author ArmiT
 * @date 24.01.14
 * @project canape
 * @package Component
 * @subpackage Installer
 */

namespace skewer\build\Component\Installer\SystemAction\Reinstall;
use skewer\build\Component\Installer as Installer;
use skewer\core\Component\Config as Config;


class UpdateConfig extends Installer\Action {

    protected $backupName = '';

    public function init() {

        $this->backupName = md5(
            $this->module->moduleName.
            $this->module->layer.
            dechex(rand(0, 10000))
        );

    }

    public function execute() {

        \ConfigUpdater::createBackup($this->backupName);
        \ConfigUpdater::buildRegistry()->removeModule( $this->module->moduleName, $this->module->layer );
        \ConfigUpdater::buildRegistry()->registerModule($this->module->moduleConfig);

    }

    public function rollback() {

        \ConfigUpdater::recoverBackup($this->backupName);
    }


} 