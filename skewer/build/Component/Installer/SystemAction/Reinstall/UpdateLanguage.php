<?php
 /**
 * @class RegisterConfig
 * @author ArmiT
 * @date 24.01.14
 * @project canape
 * @package Component
 * @subpackage Installer
 */

namespace skewer\build\Component\Installer\SystemAction\Reinstall;


use skewer\build\Component\Installer as Installer;
use skewer\build\Component\I18N\Categories;
use skewer\core\Component\Config as Config;

class UpdateLanguage extends Installer\Action {

    protected $updateCache = false;

    public function __construct(Installer\Module $module, $updateCache = false) {

        $this->module = $module;
        $this->updateCache = (bool)$updateCache;
    }

    public function init() {}

    public function execute() {

        Categories::updateModuleLanguageValues($this->module);

        if($this->updateCache)
            /** @noinspection PhpUndefinedMethodInspection */
            \Yii::$app->getI18n()->clearCache();

    }

    public function rollback() {

    }


} 