<?php

namespace skewer\build\Component\Installer;
use skewer\build\Component\Command as Command;
use skewer\build\Component\Installer\SystemAction\Install\RegisterCss;


class InstallModule  extends Command\Hub {

    protected $aCommandList = array();

    public function __construct(Module $module) {

        $this->addCommandList(array(
            new SystemAction\Install\RegisterConfig($module),
            new SystemAction\Install\RegisterLanguage($module),
            new SystemAction\Install\ExecuteModuleInstructions($module),
            new RegisterCss($module)
        ));

    }

}
