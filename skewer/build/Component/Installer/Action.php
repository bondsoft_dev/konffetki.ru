<?php
 /**
 * 
 * @author Артем
 * @date 27.01.14
 * @project canape
 * @package 
 * @subpackage 
 */

namespace skewer\build\Component\Installer;
use skewer\build\Component\Command as Command;

abstract class Action extends Command\Action {

    protected $module = null;

    public function __construct(Module $module) {

        $this->module = $module;
    }
} 