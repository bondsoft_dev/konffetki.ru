<?php

namespace skewer\build\Component\Installer;
use skewer\build\Component\Command as Command;


class UninstallModule  extends Command\Hub {

    protected $aCommandList = array();

    public function __construct(Module $module) {

        $this->addCommandList(array(
            new SystemAction\Uninstall\UnregisterConfig($module),
            new SystemAction\Uninstall\UnregisterLanguage($module),
            new SystemAction\Uninstall\ExecuteModuleInstructions($module),
        ));

    }

}
