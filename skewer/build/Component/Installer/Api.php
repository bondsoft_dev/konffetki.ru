<?php
/**
 *
 * @class skewer\build\Component\Installer\Api
 *
 * @author ArmiT
 * @date 24.01.2014
 * @project Skewer
 * @package Component
 * @subpackage Installer
 */

namespace skewer\build\Component\Installer;


use skewer\build\Component\Installer\SystemAction as SysAction;
use skewer\build\Component\Command;
use skewer\build\Component\I18N\Categories;
use skewer\core\Component\Config as Config;

/**
 * @todo рефакторить класс! потому как много команд, делающих схожие действия в непонятном порядке
 * Class Api
 * @package skewer\build\Component\Installer
 */
class Api {

    /**
     * @const int INSTALLED Указывает на то, что модуль установлен. Используется как фильтр
     */
    const INSTALLED = 0x01;

    /**
     * @const int INSTALLED Указывает на то, что модуль не установлен. Используется как фильтр
     */
    const N_INSTALLED = 0x02;

    /**
     * @const int ALL Используется для указания на то, что флаг установки не важен
     */
    const ALL = 0x03;

    /**
     * Для заданных модулей обновляет конфиги в реестре и языковые метки
     * @param Module[] $aList список модулей к установке
     * @throws Exception
     * @return array
     */
    protected function reinstallModuleConfigList( $aList ) {

        try {

            $this->startDiagnosticMode();

            \ConfigUpdater::init();

            \ConfigUpdater::buildRegistry()->clear();

            $oHub = new Command\Hub();

            // обновляем данные для модулей из списка
            foreach($aList as &$installModule) {

                if ( !$installModule instanceof Module )
                    throw new Exception( "Item must be an instance of Installer\\Module" );

                // стереть старый конфиг
                $oHub->addCommand(new SysAction\Uninstall\UnregisterConfig($installModule));

                // записать свежий
                $oHub->addCommand(new SysAction\Install\RegisterConfig($installModule));

                // обновить языковые метки
                $oHub->addCommand(new SysAction\Reinstall\LanguageFile($installModule));

                // Распарсить css файлы
                $oHub->addCommand(new SysAction\Install\RegisterCss($installModule));

            }

            $this->addComponentReinstallComand($oHub);

            $oHub->executeOrExcept();

            \ConfigUpdater::commit();

            \Yii::$app->register->reloadData();

            \Yii::$app->getI18N()->clearCache();

        } catch(\Exception $e) {

            $this->stopDiagnosticMode();
            throw new Exception('В процессе установки модуля произошли ошибки ['.$e->getMessage().']', $e->getCode(), $e);

        }

        $this->stopDiagnosticMode();

        return $aList;

    }

    /**
     * Переустанавливает все зарегистрированные в реестре модули
     */
    public function updateAllModulesConfig() {

        $aList = array();

        $layersList = \Yii::$app->register->getLayerList();
        foreach($layersList as $layer) {

            $modules = $this->getInstalledModules($layer);
            foreach($modules as $module) {

                $aList[] = $module;

            }

        }

        $aOut =  $this->reinstallModuleConfigList( $aList );

        return $aOut;

    }


    /**
     * Устанавливает модуль с именем $moduleName в слое $layer, учитывая зависимости.
     * @param string $moduleName
     * @param string $layer
     * @return array Возвращает массив объектов хранения данных по модулям
     * @throws Exception
     */
    public function install($moduleName, $layer) {

        try {

            $this->startDiagnosticMode();

            \ConfigUpdater::init();

            # получить данные корневого устанавливаемого модуля
            $module = IntegrityManager::init($moduleName, $layer);

            if($module->alreadyInstalled)
                throw new Exception(sprintf('Module [%s:%s] already installed', $module->moduleName, $module->layer));

            # получить дерево зависимостей
            $installTree = $this->getInstallTree($module);

            if(!count($installTree)) throw new Exception('All modules already installed');

            $oHub = new Command\Hub();
            foreach($installTree as &$installModule)
                $oHub->addCommand(new InstallModule($installModule));

            $oHub->executeOrExcept();

            \ConfigUpdater::commit();

        } catch(\Exception $e) {

            $this->stopDiagnosticMode();
            throw new Exception(
                'В процессе установки модуля произошли ошибки ['.$e->getMessage().']',
                $e->getCode(),
                $e
            );


        }

        $this->stopDiagnosticMode();
        return $installTree;
    }

    /**
     * Удаляет модуль с именем $moduleName в слое $layer без учета зависимостей
     * @param string $moduleName Имя модуля без учета мнемоники и слоя
     * @param string $layer Имя слоя
     * @throws Exception В случае Ошибки выбрасывает исключение skewer\build\Component\Installer\Exception
     * @return void
     */
    public function uninstall($moduleName, $layer) {

        try {

            $this->startDiagnosticMode();

            \ConfigUpdater::init();

            # получить данные корневого устанавливаемого модуля
            $module = IntegrityManager::init($moduleName, $layer);

            if(!$module->alreadyInstalled)
                throw new Exception(sprintf('Module [%s:%s]  does not installed', $module->moduleName, $module->layer));

            $oHub = new Command\Hub();
            $oHub->addCommand(new UninstallModule($module));

            $oHub->executeOrExcept();

            \ConfigUpdater::commit();

            $this->stopDiagnosticMode();

        } catch(\Exception $e) {

            $this->stopDiagnosticMode();

            throw new Exception(
                'В процессе Удаления модуля произошли ошибки ['.$e->getMessage().']',
                0,
                $e,
                $e->getFile(),
                $e->getLine()
            );

        }

    }

    /**
     * Обновляет данные модуля в реестре по файлу конфигурации без выполнения операций установки и удаления самого
     * модуля
     * @param Module $module В качестве входного параметра принимает экземпляр класса типа Installer\Module,
     * предварительно заполненный через Installer\IntegrityManager
     * @return void
     */
    protected function updateConfigByModule(Module $module) {

            $this->startDiagnosticMode();
            \ConfigUpdater::init();
            $oHub = new Command\Hub();
            $oHub->addCommand(new SysAction\Reinstall\UpdateConfig($module));
            $oHub->executeOrExcept();
            \ConfigUpdater::commit();
            $this->stopDiagnosticMode();

    }

    /**
     * Обновляет данные модуля в реестре по файлу конфигурации без выполнения операций установки и удаления самого
     * модуля
     * @param string $moduleName Имя модуля без учета слоя и мнемоники
     * @param string $layer Имя слоя
     * @throws Exception
     */
    public function updateConfig($moduleName, $layer) {

        try {

            $module = IntegrityManager::init($moduleName, $layer);
            $this->updateConfigByModule($module);

        } catch(\Exception $e) {

            $this->stopDiagnosticMode();

            throw new Exception(
                'В процессе Обновления файла конфигурации модуля произошли ошибки ['.$e->getMessage().']',
                0,
                $e,
                $e->getFile(),
                $e->getLine()
            );

        }

    }

    /**
     * Обновляет данные всех модулей слоя $layer в реестре по файлу конфигурации без выполнения операций установки
     * и удаления
     * @param string $layer Имя существующего слоя
     * @return bool В случае, если установленных модулей в слое не найдено возвращает false
     * @throws Exception
     */
    protected function updateModuleConfigByLayer($layer) {

        try {

            $modules = $this->getInstalledModules($layer);

            if(!count($modules)) return false;

            foreach($modules as $module)
                    $this->updateConfigByModule($module);



        } catch(\Exception $e) {

            $this->stopDiagnosticMode();

            throw new Exception(
                'В процессе Обновления файлов конфигурации модулей слоя '.$layer.' произошли ошибки ['.$e->getMessage().']',
                0,
                $e,
                $e->getFile(),
                $e->getLine()
            );

        }

        return true;

    }

    /**
     * Обновляет данные всех модулей всех слоев
     */
    public function updateAllConfigs() {

        $layersList = \Yii::$app->register->getLayerList();
        foreach($layersList as $layer)
            $this->updateModuleConfigByLayer($layer);

    }

    /**
     * Обновляет данные словарей для модуля с именем $moduleName в слое $layer
     * @param string $moduleName
     * @param string $layer
     * @param bool $updateCache Флаг, указывающий на необходимость перестройки файла кеша для словарей
     * @throws Exception
     */
    public function updateLanguage($moduleName, $layer, $updateCache = false) {

        try {

            $module = IntegrityManager::init($moduleName, $layer);
            $this->updateLanguageByModule($module, $updateCache);

        } catch(\Exception $e) {

            $this->stopDiagnosticMode();

            throw new Exception(
                'В процессе Обновления словарей модуля произошли ошибки ['.$e->getMessage().']',
                0,
                $e,
                $e->getFile(),
                $e->getLine()
            );

        }

    }

    /**
     * Обновляет данные словарей по данным объекта хранения данных о модуле $module
     * @param Module $module
     * @param bool $updateCache Флаг, указывающий на необходимость перестройки файла кеша для словарей
     */
    protected function updateLanguageByModule(Module $module, $updateCache = false) {

        $this->startDiagnosticMode();
        $oHub = new Command\Hub();
        $oHub->addCommand(new SysAction\Reinstall\UpdateLanguage($module, $updateCache));
        $oHub->executeOrExcept();
        $this->stopDiagnosticMode();

    }

    /**
     * Обновляет данные словарей всех установленных в слое $layer модулей
     * Внимание! Не перестраивает кеш словарей. Данную обработку нужно запускать самостоятельно
     * @param string $layer
     * @return bool
     * @throws Exception В случае возникновения ошибки выбрасывает исключение
     */
    protected function updateLanguageByLayer($layer) {

        try {

            $modules = $this->getInstalledModules($layer);

            if(!count($modules)) return false;

            foreach($modules as $module)
                $this->updateLanguageByModule($module);

        } catch(\Exception $e) {

            $this->stopDiagnosticMode();

            throw new Exception(
                'В процессе Обновления файлов словарей модулей слоя '.$layer.' произошли ошибки ['.$e->getMessage().']',
                0,
                $e,
                $e->getFile(),
                $e->getLine()
            );

        }

        return true;
    }

    /**
     * Чистка языков
     * @throws Exception
     * @throws \Exception
     * @throws null
     */
    public function clearLanguages(){
        $this->startDiagnosticMode();
        $oHub = new Command\Hub();

        $oHub->addCommand(new SysAction\Reinstall\ClearLanguage());

        $oHub->executeOrExcept();
        $this->stopDiagnosticMode();
    }

    /**
     * Обновляет данные словарей для всех установленных в системе модулей
     */
    public function updateComponentLanguages() {

        $this->startDiagnosticMode();
        $oHub = new Command\Hub();

        $this->addComponentReinstallComand($oHub);

        $oHub->executeOrExcept();
        $this->stopDiagnosticMode();

        \Yii::$app->getI18N()->clearCache();
    }

    /**
     * Возвращает массив экземпляров Installer\Module, установленных в слое $layer
     * @param string $layer
     * @throws Exception
     * @throws Config\Exception
     * @return Module[]
     */

    public function getInstalledModules($layer) {

        return $this->getModules($layer, self::INSTALLED);
    }

    /**
     * Возвращает массив экземпляров Installer\Module, установленных в слое $layer
     * @param string $layer
     * @throws Exception
     * @throws Config\Exception
     * @return Module[]
     */
    public function getAvailableModules($layer) {

        return $this->getModules($layer, self::N_INSTALLED);
    }

    /**
     * Отдает список модулей для всех зарегистрированных модулей
     */
    public static function getLayers() {
        return \Yii::$app->register->getLayerList();
    }

    /**
     * Возвращает список доступных к установке либо удалению модулей
     * @param $layer
     * @param int $moduleStatus Фильтр на список модулей. Взможно применение побитовых операций
     * @throws \skewer\build\Component\Installer\Exception
     * @return Module[]
     */
    public static function getModules($layer, $moduleStatus = self::N_INSTALLED) {

        $layers = \Yii::$app->register->getLayerList();

        if(!count($layers))
            throw new Exception('Application does not contain layers');

//        if(!in_array($layer, $layers))
//            throw new Exception('Application does not contain layer with name ['.$layer.']');

        // todo Заменить на соотв. вызов после написания компонента управления путями
        $layerPath = BUILDPATH.$layer.DIRECTORY_SEPARATOR;

        if(!is_dir($layerPath))
            throw new Exception('Real path for layer ['.$layer.':'.$layerPath.'] is not exist');

        $buildModules = \skFiles::getDirectoryContent($layerPath, false, \skFiles::DIRS);

        $out = array();
        if($buildModules)
            foreach($buildModules as $item) {

                // todo убрать замалчивание #36492
                try {

                    $module = IntegrityManager::init($item, $layer);

                    if($module->alreadyInstalled && ($moduleStatus & self::INSTALLED)) {
                        $out[] = $module;
                        continue;
                    }

                    if(!$module->alreadyInstalled && ($moduleStatus & self::N_INSTALLED)) {
                        $out[] = $module;
                        continue;
                    }

                    if($moduleStatus & (self::N_INSTALLED & self::INSTALLED)) {
                        $out[] = $module;
                        continue;
                    }

                } catch(\Exception $e) {

                    continue;
                }
            }

        return $out;
    }

    /**
     * Возвращает список объектов модулей в порядке их установки с учетом дерева зависимостей.
     * @param Module $module Класс описания модуля, Для которого требуется построить дерево зависимостей
     * @param bool $fullTree Если указан, то строиться полное дерево без учета ранее установленных модулей
     * @param array $aParents Список родительских модулей
     * @return Module[]
     *
     * Внимание! Процесс выполнения модульных инструкций необратим с точки зрения компонента Installer.
     * Поэтому, все инструкции должны быть оформлены таким образом, чтобы в случае возникновения ошибки, они могли быть
     * отменены.
     */
    public function getInstallTree(Module $module, $fullTree = false, $aParents = array() ) {

        $aParents[$module->layer][$module->moduleName] = true;

        $dependency = $module->moduleConfig->getDependency();

        $installTree = array();

        foreach($dependency as $item) {
            list($moduleName, $layer) = $item;

            if (isset($aParents[$layer][$moduleName]))
                continue;

            $item = IntegrityManager::init($moduleName, $layer);

            $subDependency = $this->getInstallTree($item, $fullTree, $aParents);

            if(count($subDependency))
                foreach($subDependency as $depItem)
                    if(!in_array($depItem, $installTree)) //Фильтровать ранее добавленные модули
                        $installTree[] = $depItem;

        }

        if($fullTree | !$module->alreadyInstalled) $installTree[] = $module;

        return $installTree;
    }

    /**
     * Запустить диагностический режим. Т.е. остановить все процессоры, кроме системных
     * @throws Exception
     */
    protected function startDiagnosticMode() {
        /* Пытаемся остановить процессоры. В случае если он уже остановлен завершаем работу */
        $hostTools = new \HostTools();
        if(!$hostTools->enableProcessor(false))
            throw new Exception('Application already in diagnostic mode. Try later');
    }

    /**\
     * Остановить диагностический режим. Т.е. запустить все процессоры
     */
    protected function stopDiagnosticMode() {

        $hostTools = new \HostTools();
        $hostTools->enableProcessor(true);
    }

    /**
     * Проверяет установлен ли модуль в системе
     * @param string $moduleName
     * @param string $layer
     * @return bool
     */
    public function isInstalled( $moduleName, $layer ) {
        return \Yii::$app->register->moduleExists( $moduleName, $layer );
    }

    /**
     * @param Command\Hub $oHub
     */
    protected function addComponentReinstallComand(Command\Hub $oHub)
    {
        // обновить вынесенные языковые файлы
        foreach (Categories::$aAddLangList as $sName => $sPath)
            $oHub->addCommand(new SysAction\Reinstall\ComponentLanguage($sName, $sPath));

        // обновить вынесенные файлы данных
        foreach (Categories::$aAddPresetDataList as $sName => $sPath)
            $oHub->addCommand(new SysAction\Reinstall\ComponentLanguage($sName, $sPath, true));
    }

}
