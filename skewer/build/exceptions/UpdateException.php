<?php
/**
 * Обработчик исключений для модулей обновления и установки
 * @class UpdateExeption
 * @extends skException
 * @implements skExeptionInterface
 *
 * @author ArmiT, $Author: sys $
 * @version $Revision: 1916 $
 * @date $Date: 2013-04-25 14:42:53 +0400 (Чт., 25 апр. 2013) $
 * @project Skewer
 * @package Build
 */
class UpdateException extends skException implements skExeptionInterface {

    public function __construct ($message, $code = 0, \Exception $previous = null) {

        parent::__construct($message, $code, $previous);

        skLogger::dump( sprintf(
            "\nUpdateException: %s\n    in [%s:%d]\n\n%s\n",
            $this->getMessage(),
            $this->getFile(),
            $this->getLine(),
            $this->getTraceAsString()
        ));

    }

}// class
