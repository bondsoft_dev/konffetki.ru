<?php

use skewer\build\Tool\LeftList;

/* main */
$aConfig['name']     = 'Import';
$aConfig['title']    = 'Импорт и обновление товаров';
$aConfig['version']  = '2.0';
$aConfig['description']  = 'Админ-интерфейс управления импортом и обновлением товаров';
$aConfig['revision'] = '0001';
$aConfig['useNamespace'] = true;
$aConfig['layer']     = \Layer::TOOL;
$aConfig['group']     = LeftList\Group::CONTENT;

return $aConfig;
