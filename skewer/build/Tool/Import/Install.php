<?php

namespace skewer\build\Tool\Import;


use skewer\build\Component\Import\ar\ImportTemplate;
use skewer\build\Component\Import\ar\Log;
use skewer\build\Component\Section\Visible;
use skewer\build\Component\Section\Tree;

use skewer\build\Component\Site\Type;

/**
 * Class Install
 * @package skewer\build\Tool\Import
 */
class Install extends \skModuleInstall {

    public function init() {
        return true;
    }// func

    public function install() {

        if (!Type::hasCatalogModule())
            $this->fail('Нельзя установить импорт на некаталожный сайт!');

        Log::rebuildTable();
        ImportTemplate::rebuildTable();

        $iSectionId = Tree::getSectionByAlias( 'Tool_Import', \Yii::$app->sections->library() );
        if (!$iSectionId){
            //@todo как создать папку?
            $iSectionId = $this->addSectionByTemplate( \Yii::$app->sections->library(), \Yii::$app->sections->tplNew(), 'Tool_Import', 'Импорт', Visible::VISIBLE);
        }

        if (!$iSectionId)
            $this->fail('Не удалось создать папку для модуля!');

        $aPolicyAdmin = \Policy::getPolicyList(['where_condition' => [
            'alias' => [
                'sign' => '=',
                'value' => 'admin'
            ]
        ]]);

        if ($aPolicyAdmin['items'])
            foreach ($aPolicyAdmin['items'] as $aPolicy){
                \Policy::addModule( $aPolicy['id'], 'Import', 'Импорт и обновление товаров');
            }

    }// func

    public function uninstall() {
        return true;
    }// func

}//class