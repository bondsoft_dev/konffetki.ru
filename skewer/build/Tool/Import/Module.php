<?php

namespace skewer\build\Tool\Import;


use skewer\build\Component\Import\Api;
use skewer\build\Component\Import\ar\ImportTemplate;
use skewer\build\Component\Import\Config;
use skewer\build\Component\Import\Exception;
use skewer\build\Component\Import\Task;
use skewer\build\Component\UI;
use skewer\build\Tool;
use yii\base\UserException;

/**
 * Модуль настройки шаблонов для импорта данных в каталога, запуска импорта и чтения статистики
 * Class Module
 */
class Module extends Tool\LeftList\ModulePrototype {

    /** @var bool Флаг того, что админ sys */
    protected $isSys = false;

    protected function actionInit(){

        $this->isSys = $this->isSys();
        $this->actionList();
    }

    /**
     * id текущего шаблона
     * @return int
     */
    private function getTplId(){

        $iTpl = $this->getInDataValInt( 'id' );
        if (!$iTpl){
            $iTpl = $this->getInnerDataInt( 'tpl_id' );
        }

        $this->setInnerData( 'tpl_id', $iTpl );

        return $iTpl;
    }

    /**
     * Список шаблонов импорта
     */
    protected function actionList(){

        $this->setPanelName(\Yii::t('import', 'tpl_list'));

        $oList = UI\StateBuilder::newList();

        $oList
            ->addField( 'id', 'id', 's', $this->isSys?'string':'hide', [ 'listColumns' => ['flex' => 1] ] )
            ->addField( 'title', \Yii::t('import', 'field_title'), 's', 'string', [ 'listColumns' => ['flex' => 3] ] )
            ->addRowButton( \Yii::t('import', 'run_import'), 'icon-reload', 'runImport')
            ->addRowButtonUpdate('headSettings')
        ;
        if ($this->isSys){
            $oList
                ->addRowButtonDelete('delete')
                ->addButton(\Yii::t('adm','add'), 'add', 'icon-add')
            ;
        }

        $aList = Api::getTemplateList();

        $oList->setValue( $aList );

        if (!is_dir(ROOTPATH.'import/')){
            $oList->addBtnSeparator('->');
            $oList->addButton(\Yii::t('import', 'addUploadFolder'), 'addFolder', 'icon-add');
        }

        $this->setInterface($oList->getForm());

    }

    /**
     * Добавление нового шаблона
     */
    protected function actionAdd(){

        $this->showHeadSettingsForm();

    }

    /**
     * Основные настройки
     * @param null|int $iTpl
     */
    protected function actionHeadSettings( $iTpl = null ){

        if ( !$iTpl )
            $iTpl = $this->getTplId();

        if ($this->isSys)
            $this->showHeadSettingsForm( $iTpl );
        else
            $this->showClientForm( $iTpl );

    }


    /**
     * Форма основных настроек
     * @param null|int $id id шаблона
     */
    private function showHeadSettingsForm( $id = null ){

        $this->setPanelName(\Yii::t('import', 'head_settings_form'));

        $oTemplate = Api::getTemplate( $id );

        $oForm = UI\StateBuilder::newEdit();

        $aData = $oTemplate->getData();

        if ( $oTemplate->type == Api::Type_File ){
            $aData['source_file'] = $aData['source'];
        }else{
            $aData['source_str'] = $aData['source'];
        }

        $sGroup = \Yii::t('import', 'head_settings_form');

        $oForm
            ->addField( 'id', 'ID', 'i', 'hide', ['groupTitle' => $sGroup] )
            ->addField( 'title', \Yii::t( 'import', 'field_title' ), 's', 'string', ['groupTitle' => $sGroup] )
            ->addSelectField( 'card', \Yii::t('import', 'field_card'), 's', Api::getCardList(), ['groupTitle' => $sGroup] )

            ->addSelectField( 'provider_type', \Yii::t('import', 'field_provider_type'), 'i', Api::getProviderTypeList(), ['groupTitle' => $sGroup] )

            ->addSelectField( 'type', \Yii::t( 'import', 'field_type' ), 'i', Api::getTypeList(), ['customField' => 'ImportType', 'groupTitle' => $sGroup] )

            ->addField( 'source_file', \Yii::t( 'import', 'field_source' ), 's', 'file', ['groupTitle' => $sGroup] )
            ->addField( 'source_str', \Yii::t( 'import', 'field_source' ), 's', 'string', ['groupTitle' => $sGroup] )

            ->addSelectField( 'coding', \Yii::t( 'import', 'field_coding' ), 's', Api::getCodingList(), ['groupTitle' => $sGroup] );
        ;

        $oForm->addLib('ImportType');

        $oForm->setValue( $aData );

        if ( $id ){
            $oForm->addButtonSave( 'save' );
            $this->addButton( $oForm, 'headSettings' );
        } else{

            $oForm
                ->addButtonSave( 'save' )
                ->addButtonCancel( 'list' )
            ;
        }

        $oForm->setTrackChanges( false );

        $this->setInterface( $oForm->getForm() );

    }

    /**
     * Форма основных настроек для клиента
     * @param null|int $id id шаблона
     */
    private function showClientForm( $id = null ){

        $this->setPanelName(\Yii::t('import', 'head_settings_form'));

        $oTemplate = Api::getTemplate( $id );

        $oForm = UI\StateBuilder::newEdit();

        $aData = $oTemplate->getData();

        if ( $oTemplate->type == Api::Type_File ){
            $aData['source_file'] = $aData['source'];
        }else{
            $aData['source_str'] = $aData['source'];
        }

        $sGroup = \Yii::t('import', 'head_settings_form');

        $oForm
            ->addField( 'id', 'ID', 'i', 'hide', ['groupTitle' => $sGroup] )
            ->addField( 'title', \Yii::t( 'import', 'field_title' ), 's', 'show', ['groupTitle' => $sGroup] )
        ;

        if ( $oTemplate->type == Api::Type_File ){
            $oForm->addField( 'source_file', \Yii::t( 'import', 'field_source' ), 's', 'file', ['groupTitle' => $sGroup] );
        }else{
            $oForm->addField( 'source_str', \Yii::t( 'import', 'field_source' ), 's', 'string', ['groupTitle' => $sGroup] );
        }

        $oForm->setValue( $aData );

        $oForm->addButtonSave( 'save' )
            ->addBtnSeparator()
            ->addButton( \Yii::t('import', 'run_import'), 'runImport', 'icon-reload' )
            ->addBtnSeparator()
            ->addButton( \Yii::t('import', 'log_list'), 'logList', 'icon-page' )
            ->addBtnSeparator()
            ->addButtonCancel( 'list' );

        $oForm->setTrackChanges( false );

        $this->setInterface( $oForm->getForm() );

    }

    /**
     * Настройки провайдера
     */
    protected function actionProviderSettings(){
        $iTpl = $this->getTplId();
        $this->showProviderSettingsForm( $iTpl );
    }


    private function showProviderSettingsForm( $iTpl ){

        if (!$iTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oTemplate = Api::getTemplate( $iTpl );

        if (!$oTemplate){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $this->setPanelName(\Yii::t('import', 'provider_settings_form'));

        $oForm = UI\StateBuilder::newEdit();

        /** Добавим поля настроек по провайдеру */
        try{
            View::getProviderForm( $oForm, $oTemplate );
        } catch (Exception $e){
            throw new UserException( $e->getMessage() );
        }

        $oForm->addButtonSave( 'saveProviderSettings' );
        $this->addButton( $oForm, 'providerSettings' );

        $oForm->setTrackChanges( false );

        $this->setInterface( $oForm->getForm() );

    }

    /**
     * Настройки импорта
     */
    protected function actionFields(){
        $iTpl = $this->getTplId();
        $this->showFieldsForm( $iTpl );
    }

    /**
     * Форма настроек импорта
     * @param $iTpl
     * @throws UserException
     * @throws UserException
     */
    private function showFieldsForm( $iTpl ){

        if (!$iTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oTemplate = Api::getTemplate( $iTpl );

        if (!$oTemplate){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $this->setPanelName(\Yii::t('import', 'fields_form'));

        $oForm = UI\StateBuilder::newEdit();

        /** Добавим поля настроек по провайдеру */
        try{
            View::getFieldsForm( $oForm, $oTemplate );
        } catch (Exception $e){
            throw new UserException( $e->getMessage() );
        }

        $oForm->addButtonSave( 'saveFields' );
        $this->addButton( $oForm, 'fields' );

        $oForm->setTrackChanges( false );

        $this->setInterface( $oForm->getForm() );
    }


    /**
     * Форма настройки полей
     */
    protected function actionFieldsSettings(){

        $iTpl = $this->getTplId();
        $this->showFieldSettingsForm( $iTpl );

    }


    private function showFieldSettingsForm( $iTpl = null ){

        if (!$iTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oTemplate = Api::getTemplate( $iTpl );

        if (!$oTemplate){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $this->setPanelName(\Yii::t('import', 'fields_settings_form'));

        $oForm = UI\StateBuilder::newEdit();

        /** Добавим поля настроек по провайдеру */
        try{
            View::getFieldsSettingsForm( $oForm, $oTemplate );
        } catch (Exception $e){
            throw new UserException( $e->getMessage(), $e->getCode(), $e );
        }

        $oForm->addButtonSave( 'saveSettingsFields' );
        $this->addButton( $oForm, 'fieldsSettings' );

        $oForm->setTrackChanges( false );

        $this->setInterface( $oForm->getForm() );

    }


    /**
     * Сохранение
     */
    protected function actionSave(){

        $aData = $this->getInData();

        $oTpl = Api::getTemplate( (isset($aData['id'])) ? $aData['id'] : null );

        if ( $this->isSys ) {
            $aRequiredList = ImportTemplate::getModel()->getColumnSet('required');
            foreach ($aRequiredList as $sFieldName) {
                if (!isset($aData[$sFieldName]) || !$aData[$sFieldName])
                    throw new UserException(\Yii::t('import', 'not_defined_field', \Yii::t('import', 'field_' . $sFieldName)));
            }
        }

        $sOldType = $oTpl->provider_type;
        $sOldCard = $oTpl->card;
        $oTpl->setData($aData);

        if ($oTpl->type == Api::Type_File){
            $oTpl->source = (isset($aData['source_file'])) ? $aData['source_file'] : '';
        }else{
            $oTpl->source = (isset($aData['source_str'])) ? $aData['source_str'] : '';
        }

        if (!$oTpl->source){
            throw new UserException( \Yii::t('import', 'not_defined_field', \Yii::t( 'import', 'field_source' )) );
        }

        $id = $oTpl->save();
        if ($id){
            /** Сменился провайдер или карточка - почистим конфиг */
            if ($sOldType != $oTpl->provider_type || $sOldCard != $oTpl->card){
                $oConfig = new Config( $oTpl );
                $oConfig->clearFields();
                $oTpl->settings = json_encode( $oConfig->getData() );
                $oTpl->save();
            }
            $this->actionHeadSettings( $id );
        }else{
            throw new UserException( \Yii::t('import', 'error_no_save') );
        }

    }

    /**
     * Сохранение настроек провайдера
     */
    protected function actionSaveProviderSettings(){

        $aData = $this->getInData();

        if (!isset($aData['id'])){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oTpl = Api::getTemplate( $aData['id'] );

        if (!$oTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oConfig = new Config( $oTpl );
        $oProvider = Api::getProvider( $oConfig );

        $aVars = $oConfig->getData();

        /** Параметры провайдера */
        foreach ($oProvider->getParameters() as $key => $value){
            $aVars[$key] = (isset($aData[$key])) ? $aData[$key] : ((isset($value['default'])) ? $value['default'] : '');
        }

        $oTpl->settings = json_encode( $aVars );
        $oTpl->save();
    }


    /**
     * Сохранение настроек
     */
    protected function actionSaveFields(){

        $aData = $this->getInData();

        if (!isset($aData['id'])){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oTpl = Api::getTemplate( $aData['id'] );

        if (!$oTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oConfig = new Config( $oTpl );
        $oConfig->setFields( $aData );

        $oTpl->settings = $oConfig->getJsonData();

        $oTpl->save();

        /** Проверка на наличие уникального поля */
        $bUnique = false;
        foreach($aData as $sKey => $value){
            if (preg_match( '/type_(\w+)/', $sKey )){
                if ($value === 'Unique'){
                    $bUnique = true;
                    break;
                }
            }
        }

        if (!$bUnique)
            throw new UserException(\Yii::t('import', 'error_unique_field_not_found'));

    }


    /**
     * Сохранение настроек полей
     */
    protected function actionSaveSettingsFields(){

        $aData = $this->getInData();

        if (!isset($aData['id'])){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oTpl = Api::getTemplate( $aData['id'] );

        if (!$oTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oConfig = new Config( $oTpl );
        $oConfig->setFieldsParam( $aData );

        $oTpl->settings = $oConfig->getJsonData();
        $oTpl->save();
    }


    /**
     * Удаление шаблона
     */
    protected function actionDelete(){

        $id = $this->getInDataValInt( 'id' );

        if ($id){
            ImportTemplate::delete( $id );
            Api::deleteLog4Template( $id );
        }

        $this->actionList();

    }

    /**
     * Задача
     */
    protected function actionShowTask(){

        $this->setPanelName(\Yii::t('import', 'task_form'));

        $oForm = UI\StateBuilder::newEdit();

        $command = json_encode([
            'class' => '\skewer\build\Component\Import\Task',
            'parameters' => ['tpl' => $this->getTplId()]
        ]);

        $aData = Tool\Schedule\Mapper::getItem( ['where_condition' => ['command' => ['sign' => '=', 'value' => $command]]] );

        if ($aData){
            $aData['active'] = 1;
            $aData['schedule_id'] = $aData['id'];
        }

        $oForm
            ->addField('schedule_id', 'schedule_id', 'i', 'hide' )
            ->addField('active', \Yii::t('import', 'active_task'), 'i', 'check' )

            ->addField('c_min', \Yii::t('schedule', 'c_min'), 'i', 'int', ['minValue' => 0, 'maxValue' => 59] )
            ->addField('c_hour', \Yii::t('schedule', 'c_hour'), 'i', 'int', ['minValue' => 0, 'maxValue' => 23] )
            ->addField('c_day', \Yii::t('schedule', 'c_day'), 'i', 'int', ['minValue' => 1, 'maxValue' => 31] )
            ->addField('c_month', \Yii::t('schedule', 'c_month'), 'i', 'int', ['minValue' => 1, 'maxValue' => 12] )
            ->addField('c_dow',\Yii::t('schedule', 'c_dow'), 'i', 'int', ['minValue' => 1, 'maxValue' => 7] )
        ;

        $oForm->setValue( $aData );

        $oForm->addButtonSave('saveTask');
        $oForm->addButtonCancel('headSettings');

        $this->setInterface( $oForm->getForm() );

    }

    /**
     * Сохранение задачи
     */
    protected function actionSaveTask(){

        $aData = $this->getInData();

        $command = json_encode([
            'class' => '\skewer\build\Component\Import\Task',
            'parameters' => ['tpl' => (int)$this->getTplId()]
        ]);

        /** @todo Пока тут. Думаю модуль Schedule будет переписан и будет человеческий апи для этих действий !!! */
        if ($aData['active']){

            /** @var \skewer\build\Component\Import\ar\ImportTemplateRow $oTpl */
            $oTpl = ImportTemplate::find($this->getTplId());
            if (!$oTpl)
                throw new \Exception(\Yii::t('import', 'error_tpl_not_fount'));

            //save
            $aData['id'] = $aData['schedule_id'];
            $aData['command'] = $command;
            $aData['name'] ='import_' . $oTpl->id;
            $aData['title'] = \Yii::t( 'import', 'task_title', $oTpl->title);
            $aData['priority'] = Task::priorityHigh;
            $aData['resource_use'] = Task::weightHigh;
            $aData['target_area'] = 3;
            $aData['status'] = Task::stNew;

            if ($aData['c_min'] && ($aData['c_min'] < 0 || $aData['c_min'] > 59))
                throw new UserException( \Yii::t('import', 'field_time_range_error', [\Yii::t('schedule', 'c_min'), 0, 59]) );
            if ($aData['c_hour'] && ($aData['c_hour'] < 0 || $aData['c_hour'] > 23))
                throw new UserException( \Yii::t('import', 'field_time_range_error', [\Yii::t('schedule', 'c_hour'), 0, 23]) );
            if ($aData['c_day'] && ($aData['c_day'] < 1 || $aData['c_day'] > 31))
                throw new UserException( \Yii::t('import', 'field_time_range_error', [\Yii::t('schedule', 'c_day'), 1, 31]) );
            if ($aData['c_month'] && ($aData['c_month'] < 1 || $aData['c_month'] > 12))
                throw new UserException( \Yii::t('import', 'field_time_range_error', [\Yii::t('schedule', 'c_month'), 1, 12]) );
            if ($aData['c_dow'] && ($aData['c_dow'] < 1 || $aData['c_dow'] > 7))
                throw new UserException( \Yii::t('import', 'field_time_range_error', [\Yii::t('schedule', 'c_dow'), 1, 7]) );

            if ($aData['c_min'] == '')
                $aData['c_min'] = null;
            if ($aData['c_hour'] == '')
                $aData['c_hour'] = null;
            if ($aData['c_day'] == '')
                $aData['c_day'] = null;
            if ($aData['c_month'] == '')
                $aData['c_month'] = null;
            if ($aData['c_dow'] == '')
                $aData['c_dow'] = null;

            Tool\Schedule\Mapper::saveItem( $aData );

        }else{
            //remove
            Tool\Schedule\Mapper::delItem( $aData['schedule_id'] );
        }

        $this->actionHeadSettings();
    }

    /**
     * Запуск импорта
     */
    protected function actionRunImport(){

        $aData = $this->getInData();
        if (empty($aData) && $this->get('params')){
            $aData = $this->get('params');
            $aData = $aData[0];
        }

        $taskId = (isset($aData['taskId'])) ? $aData['taskId'] : 0;

        $iTpl = (isset($aData['id'])) ? $aData['id'] : 0;
        if (!$iTpl){
            $iTpl = $this->getInnerDataInt( 'tpl_id' );
        }

        if (!$iTpl && !$taskId){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        if ($iTpl){
            $this->setInnerData( 'tpl_id', $iTpl );
            $this->setInnerData( 'importRun', $iTpl );
        }

        /** Запуск импорта */
        $aRes = Api::runImport( $taskId, $iTpl );

        if ( in_array($aRes['status'], [Task::stFrozen, Task::stWait]) ){
            /** Крутим, ели еще нужно */
            $this->addJSListener( 'runImport', 'runImport' );
            $this->fireJSEvent( 'runImport', ['taskId' => $aRes['id']] );
        }

        $this->showLog( $aRes['id'] );
    }

    /**
     * Список логов
     */
    protected function actionLogList(){

        $this->setPanelName(\Yii::t('import', 'logs_list'));

        $iTpl = $this->getInnerDataInt( 'tpl_id' );
        if (!$iTpl){
            throw new UserException( \Yii::t('import', 'error_tpl_not_fount') );
        }

        $oList = UI\StateBuilder::newList();

        $oList
            ->addField( 'id_log', 'ID', 'i', 'hide' )
            ->addField( 'start', \Yii::t('import', 'log_start'), 's', 'string', [ 'listColumns' => ['flex' => 3] ] )
            ->addField( 'status', \Yii::t('import', 'log_status'), 's', 'string', [ 'listColumns' => ['flex' => 1] ] )
        ;

        $oList->addWidget( 'status', __NAMESPACE__.'\View', 'getStatus' );

        $oList->setValue( Api::getLogs( $iTpl ) );

        $oList
            ->addRowButton( \Yii::t('import', 'log_detail'), 'icon-page', 'detailLog', 'edit_form' )
            ->addRowButtonDelete( 'deleteLog' )
            ->addButton( \Yii::t('import', 'back'), 'headSettings', 'icon-cancel' )
        ;

        $this->setInnerData('importRun', false);

        $this->setInterface( $oList->getForm() );

    }

    /**
     * Детальная лога
     */
    protected function actionDetailLog(){

        $iTpl = $this->getInDataValInt( 'id_log' );

        $this->showLog( $iTpl );

    }


    private function showLog( $id ){
        if (!$id){
            throw new UserException( \Yii::t('import', 'error_task_not_fount') );
        }

        $this->setPanelName(\Yii::t('import', 'log_show'));

        $log = Api::getLog( $id );

        if (isset($log['status'])){
            $log['status'] = View::getStatus($log);
        }

        $oForm = UI\StateBuilder::newEdit();
        $oForm->addField('result', \Yii::t('import', 'log_result'), 'string', 'show', ['labelAlign' => 'top', 'height' => '100%']);

        $baskAction = $this->getInnerData('importRun')?'headSettings':'logList';
        $oForm->addButton( \Yii::t('import', 'back'), $baskAction, 'icon-cancel' );

        $sText = $this->renderTemplate('log_template.twig', ['log' => $log]);
        $oForm->setValue(['result' => $sText]);

        $this->setInterface($oForm->getForm());
    }


    /**
     * Удаление записи лога
     */
    protected function actionDeleteLog(){

        $id = $this->getInDataValInt('id_log');
        if (!$id){
            throw new UserException( \Yii::t('import', 'error_task_not_fount') );
        }

        Api::deleteLog( $id );

        $this->actionLogList();
    }


    /**
     * Добавление папки импорта
     */
    protected function actionAddFolder(){

        if (mkdir(ROOTPATH.'import')){
            /**
             * @todo вот тут должно быть 755, но на наших sk* это не покатит, потом надо исправить
             */
            chmod(ROOTPATH.'import', 0777);
            $this->addMessage(\Yii::t('import', 'folderCreateHeader'), \Yii::t('import', 'folderCreate'));
        }else{
            $this->addMessage(\Yii::t('import', 'folderCreateHeader'), \Yii::t('import', 'folderNonCreate'));
        }

        $this->actionInit();

    }


    /**
     * Определяет, является ли текущий админ sys-ом
     * @return bool
     */
    protected function isSys(){

        $aUserData = \CurrentAdmin::getUserData();
        if (!isset($aUserData['login']))
            return false;

        return $aUserData['login'] == 'sys';

    }


    /**
     * Докидывание кнопок на формы
     * @param UI\StateBuilder $oForm
     * @param $state - текущее состояние
     */
    private function addButton( UI\StateBuilder &$oForm, $state ){

        $oForm
            ->addBtnSeparator()
            ->addButton( \Yii::t('import', 'run_import'), 'runImport', 'icon-reload' )
            ->addBtnSeparator()
            ->addButton( \Yii::t('import', 'head_settings_form'), 'headSettings', 'icon-edit', 'init', ['disabled' => ($state == 'headSettings')] )
            ->addButton( \Yii::t('import', 'provider_settings_form'), 'providerSettings', 'icon-edit', 'init', ['disabled' => ($state == 'providerSettings')] )
            ->addButton( \Yii::t('import', 'fields_form'), 'fields', 'icon-edit', 'init', ['disabled' => ($state == 'fields')] )
            ->addButton( \Yii::t('import', 'fields_settings_form'), 'fieldsSettings', 'icon-edit', 'init', ['disabled' => ($state == 'fieldsSettings')] )
            ->addBtnSeparator()
            ->addButton( \Yii::t('import', 'task_form'), 'showTask', 'icon-configuration' )
            ->addButton( \Yii::t('import', 'log_list'), 'logList', 'icon-page' )
            ->addBtnSeparator()
            ->addButtonCancel( 'list' );
    }
}