<?php

$aLanguage = array();

$aLanguage['ru']['tab_name'] = 'Импорт';

$aLanguage['ru']['tpl_list'] = 'Список шаблонов импорта';
$aLanguage['ru']['task_form'] = 'Настройки задания';
$aLanguage['ru']['logs_list'] = 'Список логов';
$aLanguage['ru']['log_show'] = 'Лог импорта';

$aLanguage['ru']['field_title'] = 'Название';
$aLanguage['ru']['field_card'] = 'Карточка';
$aLanguage['ru']['field_provider_type'] = 'Тип провайдера данных';
$aLanguage['ru']['field_type'] = 'Тип доступа к данным';
$aLanguage['ru']['field_source'] = 'Источник';
$aLanguage['ru']['field_coding'] = 'Кодировка';

$aLanguage['ru']['not_defined_field'] = 'Не задано поле {0}';

$aLanguage['ru']['section'] = 'Раздел';
$aLanguage['ru']['field'] = 'Поле карточки';
$aLanguage['ru']['field_type_card'] = 'Поле выгрузки';
$aLanguage['ru']['field_type'] = 'Тип';

$aLanguage['ru']['run_import'] = 'Запуск';
$aLanguage['ru']['back'] = 'Назад';

$aLanguage['ru']['active_task'] = 'Активность';

$aLanguage['ru']['type_file'] = 'Загружаемый файл';
$aLanguage['ru']['type_path'] = 'Путь к файлу';
$aLanguage['ru']['type_url'] = 'Удаленный источник';

$aLanguage['ru']['provider_type_csv'] = 'CSV - файл с разделителями';
$aLanguage['ru']['provider_type_xls'] = 'XLS - электронные таблицы';
$aLanguage['ru']['provider_type_xml'] = 'XML - простая структура';
$aLanguage['ru']['provider_type_commerceml_import'] = 'CommerceML 2 - импорт товаров';
$aLanguage['ru']['provider_type_commerceml_price'] = 'CommerceML 2 - обновление цен';

$aLanguage['ru']['example'] = 'Пример';
$aLanguage['ru']['xml_attr'] = '(атрибут)';
$aLanguage['ru']['price'] = 'Цена';

$aLanguage['ru']['ft_none'] = 'Нет';
$aLanguage['ru']['ft_value'] = 'Значение';
$aLanguage['ru']['ft_title'] = 'Название товара';
$aLanguage['ru']['ft_unique'] = 'Уникальное поле';
$aLanguage['ru']['ft_section'] = 'Раздел';
$aLanguage['ru']['ft_gallery'] = 'Галерея';
$aLanguage['ru']['ft_dict'] = 'Справочники';
$aLanguage['ru']['ft_active'] = 'Активность';

$aLanguage['ru']['field_csv_delimiter'] = 'Csv разделитель';
$aLanguage['ru']['field_xls_row_count'] = 'Количество читаемых столбцов';
$aLanguage['ru']['field_skip_row'] = 'Пропуск строк';
$aLanguage['ru']['field_xml_simple_xpath'] = 'Путь к узлу товара';

$aLanguage['ru']['field_section_delimiter'] = 'Разделитель разделов';
$aLanguage['ru']['field_section_delimiter_path'] = 'Разделитель пути';
$aLanguage['ru']['field_section_base_id'] = 'Базовый раздел';
$aLanguage['ru']['field_section_create'] = 'Создавать новые разделы';
$aLanguage['ru']['field_section_template'] = 'Шаблон для новых разделов';

$aLanguage['ru']['field_unique_create'] = 'Создавать новые товары';

$aLanguage['ru']['field_dict_create'] = 'Создавать новые элементы';

$aLanguage['ru']['head_settings_form'] = 'Основные настройки';
$aLanguage['ru']['provider_settings_form'] = 'Настройки провайдера';
$aLanguage['ru']['fields_form'] = 'Соответствие полей';
$aLanguage['ru']['fields_settings_form'] = 'Настройки полей';

$aLanguage['ru']['field_gallery_delimiter'] = 'Разделитель изображений';
$aLanguage['ru']['field_gallery_find'] = 'Искать по названию';
$aLanguage['ru']['field_gallery_recreate'] = 'Пересоздавать изображения';

$aLanguage['ru']['field_active_hide'] = 'Снятие значения';
$aLanguage['ru']['field_active_from_value'] = 'Выставление значения';
$aLanguage['ru']['field_active_delete'] = 'Удаление после импорта';

$aLanguage['ru']['field_active_hide_none'] = 'Не снимать';
$aLanguage['ru']['field_active_hide_all'] = 'Снимать у всех товаров';
$aLanguage['ru']['field_active_hide_from_card'] = 'Снимать у товаров внутри карточки';

$aLanguage['ru']['field_active_value'] = 'Выставлять по значению в выгрузке';
$aLanguage['ru']['field_active_all'] = 'Выставлять всем товарам в выгрузке';

$aLanguage['ru']['field_active_delete_none'] = 'Не удалять';
$aLanguage['ru']['field_active_delete_all'] = 'Удалять все без значения';
$aLanguage['ru']['field_active_delete_from_card'] = 'Удалять внутри карточки';

$aLanguage['ru']['task_form'] = 'Настройка задания';
$aLanguage['ru']['task_title'] = 'Импорт по шаблону "{0}"';
$aLanguage['ru']['log_list'] = 'Логи';

$aLanguage['ru']['addUploadFolder'] = 'Добавление папки';

$aLanguage['ru']['log_start'] = 'Начало импорта';
$aLanguage['ru']['log_status'] = 'Статус';
$aLanguage['ru']['log_detail'] = 'Подробнее';
$aLanguage['ru']['log_result'] = 'Результат';

$aLanguage['ru']['status'] = 'Статус';
$aLanguage['ru']['start'] = 'Начало';
$aLanguage['ru']['finish'] = 'Конец';
$aLanguage['ru']['error_list'] = 'Ошибка';
$aLanguage['ru']['added'] = 'Добавлено';
$aLanguage['ru']['update'] = 'Обновлено';
$aLanguage['ru']['skip'] = 'Пропущено';
$aLanguage['ru']['error'] = 'Ошибок';
$aLanguage['ru']['no_section'] = 'Нет раздела';
$aLanguage['ru']['create_section'] = 'Созданы разделы';
$aLanguage['ru']['add_photo'] = 'Загружено фотографий';
$aLanguage['ru']['error_list'] = 'Ошибки';
$aLanguage['ru']['delete'] = 'Удалено';

$aLanguage['ru']['error_tpl_not_fount'] = 'Шаблон поиска не найден!';
$aLanguage['ru']['error_task_not_fount'] = 'Задача не найдена!';
$aLanguage['ru']['error_run'] = 'Не удалось запустить импорт';
$aLanguage['ru']['error_codding'] = 'Неверная кодировка файла!';
$aLanguage['ru']['error_not_file'] = 'Не задан файл!';
$aLanguage['ru']['error_not_exist_file'] = 'Не найден файл!';
$aLanguage['ru']['error_xml_read'] = 'Не удалось прочитать xml файл!';
$aLanguage['ru']['error_no_valid_format_file'] = 'Неверный формат файла!';
$aLanguage['ru']['task in this process'] = 'Задача еще выполняется!';
$aLanguage['ru']['error_unique_field_not_found'] = 'Не задано уникальное поле для импорта!';
$aLanguage['ru']['error_not_found_xpath'] = 'Не задан путь к узлу товара!';
$aLanguage['ru']['error_not_valid_xpath'] = 'Неверный путь к узлу товара!';
$aLanguage['ru']['error_fields_not_found'] = 'Не заданы связи с полями!';
$aLanguage['ru']['error_dict_not_found'] = 'Не найден справочник для поля "{0}"';

$aLanguage['ru']['error_invalid_provider_type'] = 'Неверный тип провайдера данных!';
$aLanguage['ru']['error_not_lib_dir'] = 'Не найдена папка для библиотек!';
$aLanguage['ru']['error_not_create_dir'] = 'Не удалось создать директорию {0}!';
$aLanguage['ru']['error_not_save_file'] = 'Не удалось записать файл {0}!';
$aLanguage['ru']['error_file_not_found'] = 'Не найден файл {0}!';

$aLanguage['ru']['field_time_range_error'] = 'Значение поля {0} не может быть меньше {1} или больше {2}';

$aLanguage['ru']['folderCreateHeader'] = 'Создание директории';
$aLanguage['ru']['folderCreate'] = 'Директория создана';
$aLanguage['ru']['folderNonCreate'] = 'Не удалось создать директорию';


$aLanguage['en']['tab_name'] = 'Import';

$aLanguage['en']['tpl_list'] = 'List of import templates';
$aLanguage['en']['task_form'] = 'Settings job';
$aLanguage['en']['logs_list'] = 'List of logs';
$aLanguage['en']['log_show'] = 'Log imports';

$aLanguage['en']['field_title'] = 'Name';
$aLanguage['en']['field_card'] = 'Card';
$aLanguage['en']['field_provider_type'] = 'Type the data provider';
$aLanguage['en']['field_type'] = 'The type of access to data';
$aLanguage['en']['field_source'] = 'source';
$aLanguage['en']['field_coding'] = 'encoding';

$aLanguage['en']['not_defined_field'] = 'Not defined field {0}';

$aLanguage['en']['section'] = 'section';
$aLanguage['en']['field'] = 'Golf Card';
$aLanguage['en']['field_type_card'] = 'Field of unloading';
$aLanguage['en']['field_type'] = 'type';

$aLanguage['en']['run_import'] = 'Run';
$aLanguage['en']['back'] = 'Back';

$aLanguage['en']['active_task'] = 'Active';

$aLanguage['en']['type_file'] = 'The uploaded file';
$aLanguage['en']['type_path'] = 'path';
$aLanguage['en']['type_url'] = 'remote source';

$aLanguage['en']['provider_type_csv'] = 'CSV - delimited file';
$aLanguage['en']['provider_type_xls'] = 'XLS - spreadsheets';
$aLanguage['en']['provider_type_xml'] = 'XML - simple structure';
$aLanguage['en']['provider_type_commerceml_import'] = 'CommerceML 2 - import of goods';
$aLanguage['en']['provider_type_commerceml_price'] = 'CommerceML 2 - upgrade pricing';

$aLanguage['en']['example'] = 'Example';
$aLanguage['en']['xml_attr'] = '(attribute)';
$aLanguage['en']['price'] = 'Price';

$aLanguage['en']['ft_none'] = 'No';
$aLanguage['en']['ft_value'] = 'value';
$aLanguage['en']['ft_title'] = 'Product name';
$aLanguage['en']['ft_unique'] = 'unique field';
$aLanguage['en']['ft_section'] = 'section';
$aLanguage['en']['ft_gallery'] = 'gallery';
$aLanguage['en']['ft_dict'] = 'Directories';
$aLanguage['en']['ft_active'] = 'Active';

$aLanguage['en']['field_csv_delimiter'] = 'Csv separator';
$aLanguage['en']['field_xls_row_count'] = 'Number of columns read';
$aLanguage['en']['field_skip_row'] = 'pass line';
$aLanguage['en']['field_xml_simple_xpath'] = 'The path to the node goods';

$aLanguage['en']['field_section_delimiter'] = 'separator partitions';
$aLanguage['en']['field_section_delimiter_path'] = 'path separator';
$aLanguage['en']['field_section_base_id'] = 'Basic section';
$aLanguage['en']['field_section_create'] = 'Create a new partition';
$aLanguage['en']['field_section_template'] = 'Template for new partitions';

$aLanguage['en']['field_unique_create'] = 'Creating new products';

$aLanguage['en']['field_dict_create'] = 'Creating new features';

$aLanguage['en']['head_settings_form'] = 'Basic settings';
$aLanguage['en']['provider_settings_form'] = 'Settings provider';
$aLanguage['en']['fields_form'] = 'Match fields';
$aLanguage['en']['fields_settings_form'] = 'Settings fields';

$aLanguage['en']['field_gallery_delimiter'] = 'separator image';
$aLanguage['en']['field_gallery_find'] = 'Search by name';
$aLanguage['en']['field_gallery_recreate'] = 'recreate the image';

$aLanguage['en']['field_active_hide'] = 'Removing values';
$aLanguage['en']['field_active_from_value'] = 'set value';
$aLanguage['en']['field_active_delete'] = 'Delete after import';

$aLanguage['en']['field_active_hide_none'] = 'Do not shoot';
$aLanguage['en']['field_active_hide_all'] = 'To remove all products';
$aLanguage['en']['field_active_hide_from_card'] = 'starred in goods within the cards';

$aLanguage['en']['field_active_value'] = 'To exhibit meaningfully in unloading';
$aLanguage['en']['field_active_all'] = 'To expose all item in unloading';

$aLanguage['en']['field_active_delete_none'] = 'Do not remove';
$aLanguage['en']['field_active_delete_all'] = 'Delete all without value';
$aLanguage['en']['field_active_delete_from_card'] = 'Remove the card inside';

$aLanguage['en']['task_form'] = 'Setting the job';
$aLanguage['en']['task_title'] = 'Import Pattern "{0}"';
$aLanguage['en']['log_list'] = 'logs';

$aLanguage['en']['addUploadFolder'] = 'To add a folder';

$aLanguage['en']['log_start'] = 'Start import';
$aLanguage['en']['log_status'] = 'Status';
$aLanguage['en']['log_detail'] = 'View';
$aLanguage['en']['log_result'] = 'Result';

$aLanguage['en']['status'] = 'Status';
$aLanguage['en']['start'] = 'Home';
$aLanguage['en']['finish'] = 'End';
$aLanguage['en']['error_list'] = 'error';
$aLanguage['en']['added'] = 'Added';
$aLanguage['en']['update'] = 'Updated';
$aLanguage['en']['skip'] = 'Missing';
$aLanguage['en']['error'] = 'Error';
$aLanguage['en']['no_section'] = 'There is no section';
$aLanguage['en']['create_section'] = 'Create Partition';
$aLanguage['en']['add_photo'] = 'Uploaded pictures';
$aLanguage['en']['error_list'] = 'error';
$aLanguage['en']['delete'] = 'Delete';

$aLanguage['en']['error_tpl_not_fount'] = 'Search pattern not found';
$aLanguage['en']['error_task_not_fount'] = 'The problem is not found!';
$aLanguage['en']['error_run'] = 'Unable to start import';
$aLanguage['en']['error_codding'] = 'Not true file encoding!';
$aLanguage['en']['error_not_file'] = 'Not specified file!';
$aLanguage['en']['error_not_exist_file'] = 'File Not Found!';
$aLanguage['en']['error_xml_read'] = 'Failed to read the xml file!';
$aLanguage['en']['error_no_valid_format_file'] = 'Invalid file format!';
$aLanguage['en']['task in this process'] = 'The task is still running!';
$aLanguage['en']['error_unique_field_not_found'] = 'Do not set a unique field for import!';
$aLanguage['en']['error_not_found_xpath'] = 'Do not set the path to the node product!';
$aLanguage['en']['error_not_valid_xpath'] = 'Invalid path to the node product!';
$aLanguage['en']['error_fields_not_found'] = 'Do not asked due to the fields!';
$aLanguage['en']['error_dict_not_found'] = 'Could not find reference for the field "{0}"';

$aLanguage['en']['error_invalid_provider_type'] = 'Wrong type data provider';
$aLanguage['en']['error_not_lib_dir'] = 'Do not locate the folder for the library!';
$aLanguage['en']['error_not_create_dir'] = 'Unable to create directory {0}!';
$aLanguage['en']['error_not_save_file'] = 'Failed to write file {0}!';
$aLanguage['en']['error_file_not_found'] = 'File {0} not found!';

$aLanguage['en']['field_time_range_error'] = 'The value of {0} can not be less or more {1} {2}';

$aLanguage['en']['folderCreateHeader'] = 'Create a directory';
$aLanguage['en']['folderCreate'] = 'Directory created';
$aLanguage['en']['folderNonCreate'] = 'Unable to create directory';


return $aLanguage;