<?php

namespace skewer\build\Tool\Import;


use skewer\build\Component\Import\Api;
use skewer\build\Component\Import\ar\ImportTemplateRow;
use skewer\build\Component\Import\Config;
use skewer\build\Component\QueueManager\Task;
use skewer\build\Component\UI\StateBuilder;
use yii\helpers\ArrayHelper;
use skewer\build\Component\Catalog;

/**
 * Класс для построений форм настройки
 */
class View{

    /**
     * Дополнение формы полями для нужного провайдера данных
     * @param StateBuilder $oForm
     * @param ImportTemplateRow $oTemplate
     */
    public static function getProviderForm( StateBuilder &$oForm, ImportTemplateRow $oTemplate ){

        $oConfig = new Config( $oTemplate );

        /** Если удаленный источник и нет локального файла - скачаем для примера */
        if ( $oConfig->getParam( 'type' ) == Api::Type_Url && (!$oConfig->getParam('file') || !file_exists( $oConfig->getParam('file'))) ){
            $oConfig->setParam( 'file', Api::uploadFile( $oConfig->getParam( 'source' )));
            //  и запомним для будущих примеров
            $oTemplate->settings = json_encode( $oConfig->getData() );
            $oTemplate->save();
        }

        $oProvider = Api::getProvider( $oConfig );

        $aParameters = $oProvider->getParameters();

        $aData = $oConfig->getData();

        /** Создаем поля для редактирования параметров */
        $oForm->addField( 'id', 'ID', 'i', 'hide');
        foreach( $aParameters as $key=>$aVal ){

            switch ($aVal['viewtype']){
                case 'select':
                    $aList = [];
                    if (isset($aVal['method'])){
                        $aList = $oProvider->$aVal['method']();
                    }
                    $oForm->addSelectField( $key, \Yii::t('import', $aVal['title'] ), $aVal['datatype'], $aList );
                    break;

                default:
                    $oForm->addField( $key, \Yii::t('import', $aVal['title'] ), $aVal['datatype'], $aVal['viewtype'] );
                    break;
            }

            /** Значение полей по умолчанию */
            if (!isset($aData[$key]))
                $aData[$key] = $aVal['default'];
        }

        /** Пример данных из файла */
        $sExapmles = $oProvider->getExample();
        if ($sExapmles){
            $oForm->addField('example', \Yii::t('import', 'example' ), 's', 'show', ['labelAlign' => 'top']);
            $aData['example'] = $sExapmles;
        }

        $oForm->setValue( $aData );

    }

    /**
     * Дополнение формы связями полей
     * @param StateBuilder $oForm
     * @param ImportTemplateRow $oTemplate
     */
    public static function getFieldsForm( StateBuilder &$oForm, ImportTemplateRow $oTemplate ){

        $oConfig = new Config( $oTemplate );

        /** Если удаленный источник и нет локального файла - скачаем для примера */
        if ( $oConfig->getParam( 'type' ) == Api::Type_Url && (!$oConfig->getParam('file') || !file_exists( $oConfig->getParam('file'))) ){
            $oConfig->setParam( 'file', Api::uploadFile( $oConfig->getParam( 'source' )));
            //  и запомним для будущих примеров
            $oTemplate->settings = json_encode( $oConfig->getData() );
            $oTemplate->save();
        }

        $oProvider = Api::getProvider( $oConfig );

        $aData = [];
        $aData['id'] = $oTemplate->id;
        $aFields = $oConfig->getParam('fields');
        if (is_array($aFields)){
            foreach($aFields as $aVal){

                if ($aVal['type']){
                    /** чтобы не путать незаданное поле с полем, которому соответствует 0 */
                    if ($aVal['importFields'] !== '')
                        $aData['field_'.$aVal['name']] = $aVal['importFields'];
                    
                    $aData['type_'.$aVal['name']] = $aVal['type'];
                }
            }
        }

        /** Создаем поля для редактирования параметров */
        $oForm->addField( 'id', 'ID', 'i', 'hide');

        /** Пример данных из файла */
        $sExapmles = $oProvider->getExample();
        if ($sExapmles){
            $oForm->addField('example', \Yii::t('import', 'example' ), 's', 'show', ['labelAlign' => 'top']);
            $aData['example'] = $sExapmles;
        }

        /** Соответствие полей */
        $aInfoRow = $oProvider->getInfoRow();
        if ($aInfoRow){
            $aFields = Api::getFieldList( $oTemplate->card );

            /** Убираем сео поля */
            $aSeoFields = Catalog\Api::getSeoFields();
            $aFields = array_diff_key($aFields, ArrayHelper::index($aSeoFields, function($a){ return $a;}));

            unset($aFields['id']);

            /** Хак на раздел */
            $aFields = array_merge( ['section' => \Yii::t( 'import', 'section')], $aFields);

            $j=0;
            foreach( $aFields as $i=>$sValue ){

                $j++;
                $sGroup = (string)($j);

                $oForm->addField( 'show_' . $i, \Yii::t("import", "field"), 's', 'show', ['groupTitle' => $sGroup] );
                $aData['show_' . $i] = $sValue;

                $oForm->addMultiSelectField( 'field_' . $i, \Yii::t("import", "field_type_card"), $aInfoRow, [], ['forceSelection' => false, 'groupTitle' => $sGroup]);

                $oForm->addSelectField( "type_"  . $i, \Yii::t("import", "field_type"), 'i', Api::getFieldTypeList(), ['forceSelection' => false, 'groupTitle' => $sGroup]);
            }
        }

        $oForm->setValue( $aData );

    }


    /**
     * Дополнение формы настройками полей
     * @param StateBuilder $oForm
     * @param ImportTemplateRow $oTemplate
     */
    public static function getFieldsSettingsForm( StateBuilder &$oForm, ImportTemplateRow $oTemplate ){

        $oConfig = new Config( $oTemplate );

        $aData = [];
        $aData['id'] = $oTemplate->id;

        /** Создаем поля для редактирования параметров */
        $oForm->addField( 'id', 'ID', 'i', 'hide');

        $aCardFields = Api::getFieldList( $oTemplate->card );

        /** Убираем сео поля */
        $aSeoFields = Catalog\Api::getSeoFields();
        $aCardFields = array_diff_key($aCardFields, ArrayHelper::index($aSeoFields, function($a){ return $a;}));

        /** Хак на раздел */
        $aCardFields = array_merge( ['section' => \Yii::t( 'import', 'section')], $aCardFields);

        $aFields = $oConfig->getParam('fields');
        if (is_array($aFields)){
            foreach($aFields as $aVal){
                if (!isset($aVal['type']) || !$aVal['type'])
                    continue;

                $sClassName = 'skewer\\build\\Component\\Import\\Field\\' . $aVal['type'];
                if (!class_exists($sClassName))
                    continue;

                /** @noinspection PhpUndefinedMethodInspection */
                $aParams = $sClassName::getParameters();
                if (!$aParams)
                    continue;

                $sGroup = $aCardFields[$aVal['name']];

                foreach($aParams as $k => $value){

                    if ($value['viewtype'] == 'select'){

                        $aList = [];
                        if (isset($value['method'])){
                            $aList = $sClassName::$value['method']();

                            /** Если значения по умолчанию нет в списке, выделяем первый */
                            if (!isset($aList[$value['default']]) && count($aList))
                                $value['default'] = key($aList);
                        }

                        $oForm->addSelectField( 'params_' . $aVal['name'] . ':' . $k, \Yii::t("import", $value['title']), $value['datatype'], $aList, ['groupTitle' => $sGroup] );
                    }else{
                        $oForm->addField( 'params_' . $aVal['name'] . ':' . $k, \Yii::t("import", $value['title']), $value['datatype'], $value['viewtype'], ['groupTitle' => $sGroup] );
                    }

                    $aData['params_' . $aVal['name'] . ':' . $k] = (isset($aVal['params'][$k])) ? $aVal['params'][$k] : $value['default'];
                }

            }
        }

        $oForm->setValue( $aData );

    }


    /**
     * Статус
     * @param $item
     * @return string
     */
    public static function getStatus( $item ){

        $aList = \skewer\build\Component\QueueManager\Api::getStatusList();

        // вместо заморожена, писать в работе
        if ($item['status'] == Task::stFrozen)
            $item['status'] = Task::stProcess;

        return (isset($aList[$item['status']])) ? $aList[$item['status']] : '';

    }

}