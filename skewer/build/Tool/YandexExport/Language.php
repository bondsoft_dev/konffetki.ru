<?php

$aLanguage = array();

$aLanguage ['ru']['tab_name'] = 'Яндекс.Маркет';
$aLanguage ['ru']['settings'] = 'Настройки';

$aLanguage ['ru']['create'] = 'Создать';
$aLanguage ['ru']['refresh'] = 'Обновить';
$aLanguage ['ru']['utils'] = 'Установка галочек';
$aLanguage ['ru']['error_lock'] = 'Ошибка! Файл уже используется!';
$aLanguage ['ru']['section'] = 'Раздел';


$aLanguage ['ru']['shopName'] = 'Имя магазина';
$aLanguage ['ru']['companyName'] = 'Название компании';
$aLanguage ['ru']['title_section'] = 'Раздел доступен для выгрузки в Yandex Market';
$aLanguage ['ru']['localDeliveryCost'] = 'Стоимость доставки по региону';

$aLanguage ['ru']['tpl_adress'] = 'Адрес выгрузки';
$aLanguage ['ru']['tpl_data'] = 'Дата выгрузки';
$aLanguage ['ru']['tpl_count'] = 'Количество товаров в выгрузке';
$aLanguage ['ru']['tpl_list'] = 'Список разделов, попавших в выгрузку:';

$aLanguage ['en']['tab_name'] = 'Yandex.Market';
$aLanguage ['en']['settings'] = 'Settings';
$aLanguage ['en']['shopName'] = 'Shop name';
$aLanguage ['en']['companyName'] = 'Company name';
$aLanguage ['en']['title_section'] = 'Yandex Market';
$aLanguage ['en']['localDeliveryCost'] = 'local delivery cost';
$aLanguage ['en']['create'] = 'Create';
$aLanguage ['en']['refresh'] = 'Refresh';
$aLanguage ['en']['utils'] = 'Installing ticks';

$aLanguage ['en']['section'] = 'Section';
$aLanguage ['en']['error_lock'] = 'Error! File already in use!';

$aLanguage ['en']['tpl_adress'] = 'Address discharge';
$aLanguage ['en']['tpl_data'] = 'Date of discharge';
$aLanguage ['en']['tpl_count'] = 'The amount of goods unloaded';
$aLanguage ['en']['tpl_list'] = 'The list of partitions belonging to a discharge:';

return $aLanguage;