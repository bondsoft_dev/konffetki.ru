<?php

namespace skewer\build\Tool\YandexExport;

use skewer\build\Component\Catalog;
use skewer\build\Component\Site;
use skewer\build\Adm\Params\ar;
use skewer\build\Catalog\CardEditor;

class Install extends \skModuleInstall {

    private $iCardId = 1;

    private $iGroupId = false;

    const FieldGroupName = 'yandex';

    public $aListAttribute = array();

    public function getListAttribute() {

        $this->aListAttribute = [
            'set' => [
                Catalog\Attr::ACTIVE,
                Catalog\Attr::SHOW_IN_LIST,
                Catalog\Attr::SHOW_IN_DETAIL,
            ],
            'not_set' => [
                Catalog\Attr::MEASURE,
                Catalog\Attr::SHOW_IN_SORTPANEL,
                Catalog\Attr::SHOW_IN_PARAMS,
                Catalog\Attr::SHOW_IN_TAB,
                Catalog\Attr::SHOW_IN_FILTER,
                Catalog\Attr::IS_UNIQ,
                Catalog\Attr::SHOW_IN_TABLE,
                Catalog\Attr::SHOW_IN_CART,
            ]
        ];

    }

    public function addParam($name, $title, $editor, $defValue = '') {

        $aData['id'] = 0;
        $aData['title'] = $title;
        $aData['name'] = $name;
        $aData['editor'] = $editor;
        $aData['group'] = $this->iGroupId;
        $aData['entity'] = $this->iCardId;
        $aData['validator'] = "";
        $aData['link_id'] = 0;
        $aData['def_value'] = $defValue;

        foreach ($this->aListAttribute['set'] as $item) {
            $aData['attr_' . $item] = 1;
        }

        foreach ($this->aListAttribute['not_set'] as $item) {
            $aData['attr_' . $item] = '';
        }

        $oField = Catalog\Card::getField();
        $oField->setData( $aData );
        $oField->save();

        return $oField;
    }

    private function setYandex(){
        $this->executeSQLQuery("UPDATE `co_base_card` SET in_yandex =1;");
    }

    public function init() {

        $sBaseCard = Catalog\Card::DEF_BASE_CARD;
        $oCardRow = Catalog\Card::get( $sBaseCard );
        if (!$oCardRow){
            $this->fail( 'Не найдена базовая карточка!' );
        }

        $this->getListAttribute();

        $this->iCardId = $oCardRow->id;

        return true;
    }

    // func

    public function install() {

        $oGroup = Catalog\Card::getGroup();
        $oGroup->setData([
            'entity' => $this->iCardId,
            'name' => static::FieldGroupName,
            'title' => \Yii::t('data/catalog', 'group_yandex_title', [], \Yii::$app->language )
        ]);
        $oGroup->save();

        $this->iGroupId = $oGroup->id;

        $this->addParam('in_yandex', \Yii::t('data/catalog', 'field_in_yandex_title', [], \Yii::$app->language ), 'check', 1);
        $this->addParam('pickup', \Yii::t('data/catalog', 'field_pickup_title', [], \Yii::$app->language ), 'check', 1);
        $this->addParam('available', \Yii::t('data/catalog', 'field_available_title', [], \Yii::$app->language ), 'check', '');
        $this->addParam('store', \Yii::t('data/catalog', 'field_store_title', [], \Yii::$app->language ), 'check', 1);
        $this->addParam('delivery', \Yii::t('data/catalog', 'field_delivery_title', [], \Yii::$app->language ), 'check', 1);
        $this->addParam('warranty', \Yii::t('data/catalog', 'field_warranty_title', [], \Yii::$app->language ), 'check', '');
        $this->addParam('vendor', \Yii::t('data/catalog', 'field_vendor_title', [], \Yii::$app->language ), 'string', '');
        $this->addParam('adult', \Yii::t('data/catalog', 'field_adult_title', [], \Yii::$app->language ), 'check', '');

        Catalog\Card::build( $this->iCardId );

        $this->setYandex();

        $sDir = WEBPATH . 'export';
        if (!is_dir( $sDir )){
            try {
                mkdir( $sDir );
                chmod( $sDir, 0777 );
            } catch ( \Exception $e ) {
                $this->fail($e->getMessage());
                return false;
            }
        }

        return true;
    }

    // func

    public function uninstall() {

        $oGroup = Catalog\Card::getGroupByName( $this->iCardId, static::FieldGroupName);
        if (!$oGroup){
            return true;
        }

        $aFields = $oGroup->getFields();

        if ($aFields){
            /**
             * @var $oField Catalog\model\FieldRow
             */
            foreach ($aFields as $oField){
                $oField->delete();
                // todo перестроение сущности
            }
        }

        $oGroup->delete();

        Catalog\Card::build( $this->iCardId );

        return true;
    }
    // func

}//class
