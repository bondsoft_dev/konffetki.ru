<?php

namespace skewer\build\Tool\YandexExport;


use skewer\build\Adm;
use skewer\build\Component\Catalog\Card;
use skewer\build\Component\Catalog\GoodsRow;
use skewer\build\Component\Catalog\GoodsSelector;
use skewer\build\Component\Section\Tree;
use skewer\build\Component\Section\Parameters;
use skewer\build\Tool;
use skewer\build\Component\UI;
use skewer\build\libs\ExtBuilder;
use skewer\build\Component\Site;
use skewer\build\Adm\GuestBook\ar;
use \skewer\build\Component\Search;
use skewer\build\Adm\Params;
use yii\base\UserException;


class Module extends Adm\Order\Module implements Tool\LeftList\ModuleInterface {

    protected $sFilePath = 'export/store.xml';

    protected $sTmpFilePath = 'export/store_tmp.xml';

    private $iExportLimit = 50;

    /**
     * @inheritDoc
     */
    public function init()
    {
        Tool\LeftList\ModulePrototype::updateLanguage();
        parent::init();
    }

    function getName() {
        return $this->getModuleName();
    }

    /**
     * @todo пересмотреть всю функцию
     * рекурсивно соберем разделы в каталог
     * @param $iSectionId
     * @return array
     */
    function getRecursiveCatalog($iSectionId) {

        $aResult = array();

        $aTree = Tree::getSubSections( $iSectionId );
        foreach ($aTree as $section) {

            $aItems = $this->getRecursiveCatalog( $section->id );

            $rowYandex = Parameters::getByName( $section->id, 'content', 'sectionYandex' );

            $aResult[ $section->id ] = [
                'id' => $section->id,
                'title' => $section->title,
                'url' => Tree::getSectionAlias( $section->id ),
                'in_yandex' => ( $rowYandex && $rowYandex->value == 1 ) ? 1 : 0,
                'items' => $aItems
            ];

        }
        return $aResult;
    }

    /**
     * @todo пересмотреть эту функцию
     */
    function convertTreeToArray($aTree, $parent) {
        $aResult = array();

        foreach ($aTree as $aItem) {

            $oCatalog = Parameters::getByName( $aItem['id'], 'content', Parameters::objectAdm, true );

            if ($oCatalog && $oCatalog->value == 'Catalog'){
                $aResult[$aItem['id']] = ['id' => $aItem['id'], 'title' => $aItem['title'],
                    'parent' => $parent, 'in_yandex' => 1];
            }


            if (isset($aItem['items'])) {
                $aResult = $this->convertTreeToArray($aItem['items'], $aItem['id']) + $aResult;
            }
        }

        return $aResult;
    }


    function actionExport() {

        $aTree = $this->getRecursiveCatalog(\Yii::$app->sections->root());
        $aTree = $this->convertTreeToArray($aTree, \Yii::$app->sections->root());

        $oFormBuilder = UI\StateBuilder::newEdit();

        if (!is_dir(ROOTPATH . 'export')) {
            mkdir(ROOTPATH . 'export');
        }

        $iPage = 1;
        $bNext = false;

        if ($this->get('params')){
            $aData = $this->get('params');
            $aData = $aData[0];
            $iPage = (isset($aData['page']))?$aData['page']:1;
        }

        if ($aTree){

            if (\SysVar::get('YandexExport.file_lock')){
                throw new UserException(\Yii::t('yandexExport', 'error_lock'));
            }

            \SysVar::set('YandexExport.file_lock', 1);

            if ( $iPage > 1 ){
                $rExportFile = fopen(WEBPATH . $this->sTmpFilePath, 'a+');
            }
            else{
                $rExportFile = fopen(WEBPATH . $this->sTmpFilePath, 'w+');
            }

            $query = GoodsSelector::getList4Section( array_keys($aTree) )
                ->condition( 'in_yandex', 1 )
                ->condition( 'active', 1 )
                ->condition( 'price > ?', 0 )
                ->limit( $this->iExportLimit, $iPage )
                ->sort( 'base_id' );

            $aData = array();
            $aData['domain'] = $sDomain = \Site::httpDomain();

            if ( $iPage == 1 ){
                //шапка
                $aData['categories'] = $aTree;
                $aData['date'] = date('Y-m-d H:i');
                $aData['shopName'] = \SysVar::getSafe('YandexExport.shopName', '');
                $aData['companyName'] = \SysVar::getSafe('YandexExport.companyName', '');
                $aData['localDeliveryCost'] = \SysVar::getSafe('YandexExport.localDeliveryCost', '');

                fwrite($rExportFile, \skParser::parseTwig('store_head.twig', $aData, $this->getModuleDir().$this->getTplDirectory()));

                \SysVar::set('YandexExport.count', 0);

            }

            $iCount = 0;
            while ( ( ($aGood = $query->parseEach()) !== false ) ) {
                $iCount++;

                fwrite($rExportFile, \skParser::parseTwig(
                    'store_item.twig',
                    array('item' => $aGood, 'domain' => $sDomain),
                    $this->getModuleDir().$this->getTplDirectory())
                );

            }

            $iPage++;

            if ( $iCount < $this->iExportLimit ){
                fwrite($rExportFile, \skParser::parseTwig('store_footer.twig', array(), $this->getModuleDir().$this->getTplDirectory()));
            }else{
                $bNext = true;
            }

            fclose($rExportFile);

            copy(WEBPATH.$this->sTmpFilePath, WEBPATH.$this->sFilePath);

            \SysVar::set('YandexExport.file_lock', 0);

            $iCount = $iCount + \SysVar::get('YandexExport.count');
            \SysVar::set('YandexExport.count', $iCount);

            $sReportText = $this->renderTemplate('report.twig',array('date'=>date('Y-m-d H:i:s'),'count'=>$iCount,'url'=>\Site::httpDomainSlash().$this->sFilePath,'categories' => $aTree));

            $oFormBuilder->addField('title', 'title', 's', 'text_html', array('height' => '100%'));
            $oFormBuilder->addHeadText($sReportText);

            \SysVar::set('Yandex.report',$sReportText);
        }

        if ($bNext){
            $this->addJSListener( 'reload_export', 'export' );
            $this->fireJSEvent( 'reload_export', array('page' => $iPage) );
        }

        $this->actionInit();
        return psComplete;

    }

    public function actionSettings() {

        $oFormBuilder = new UI\StateBuilder();
        $oFormBuilder = $oFormBuilder::newEdit();

        $oFormBuilder
            ->addField('shopName', \Yii::t('yandexExport', 'shopName'), 's', 'string')
            ->addField('companyName', \Yii::t('yandexExport', 'companyName'), 's', 'string')
            ->addField('localDeliveryCost', \Yii::t('yandexExport', 'localDeliveryCost'), 's', 'string');

        $sShopName = \SysVar::getSafe('YandexExport.shopName', '');
        $sCompanyName = \SysVar::getSafe('YandexExport.companyName', '');
        $sLocalDeliveryCost = \SysVar::getSafe('YandexExport.localDeliveryCost', '');

        $oFormBuilder->setValue(array('shopName' => $sShopName, 'companyName' => $sCompanyName, 'localDeliveryCost'=>$sLocalDeliveryCost));

        $oFormBuilder->addButton(\Yii::t('adm','save'), 'saveSettings', 'icon-save');
        $oFormBuilder->addButton(\Yii::t('adm','back'), 'init', 'icon-cancel');

        $this->setInterface($oFormBuilder->getForm());
    }

    public function actionSaveSettings() {

        $data = $this->getInData();
        $sShopName = $data['shopName'];
        $sCompanyName = $data['companyName'];
        $sLocalDeliveryCost = $data['localDeliveryCost'];

        \SysVar::set('YandexExport.shopName', $sShopName);
        \SysVar::set('YandexExport.companyName', $sCompanyName);
        \SysVar::set('YandexExport.localDeliveryCost', $sLocalDeliveryCost);

        $this->actionSettings();
    }

    public function actionUtils(){
        $oFormBuilder = UI\StateBuilder::newList();

        $aTree = $this->getRecursiveCatalog(\Yii::$app->sections->root());
        $aTree = $this->convertTreeToArray($aTree, \Yii::$app->sections->root());

        $group = Card::getGroupByName(1,'yandex');
        $fields = $group->getFields();

        $oFormBuilder->addField('title', \Yii::t('yandexExport', 'section'),'s','string',array( 'listColumns' => array('flex' => 3) ));

        $aEditableFields = array();
        // собираем чекбоксы
        foreach($fields as $field){
            if ($field->group == $group->id && $field->editor=='check'){
                $aEditableFields[] = $field->name;

                $oFormBuilder->addField($field->name,$field->title,'i','check',array( 'listColumns' => array('flex' => 1) ));

                foreach ($aTree as $k=>$item) {
                    $aTree[$k][$field->name] = $field->def_value;
                }
            }
        }

        $oFormBuilder->addRowButton(\Yii::t('adm','save'),'icon-save','saveParam','add');
        $oFormBuilder->setValue($aTree);

        // todo как сделать чекбоксы?
        $oFormBuilder->setEditableFields($aEditableFields,'saveCheck');

        $oFormBuilder->addButton(\Yii::t('adm','back'), 'init', 'icon-cancel');

        $this->setInterface($oFormBuilder->getForm());
        return psComplete;
    }

    public function actionSaveParam(){
        //fixme странный метод, сомневаюсь что он вообще работает

        $aData = $this->getInData();

        if (!$aData){
            $aParams = $this->get('params');
            if (isset($aParams[0]['data'])){
                $aData = $aParams[0]['data'];
            }
        }

        if (!isset($aData['id'])) return;

        $sectionId = $aData['id'];

        // оставляем только метки от яндекс каталога, удаляем все лишнее
        //fixme так данные получать, обрабатывать и тем более сохранять нельзя (они еще и из виджитированного списка приходят)
        unset($aData['id']);
        unset($aData['title']);
        unset($aData['parent']);

        $iPage = (isset($aData['page']))?$aData['page']:1;

        /**
         * @todo После окончательного допиливания каталога переписать на более быстрые методы
         */
        $query = GoodsSelector::getList4Section( $sectionId )
            ->limit( $this->iExportLimit, $iPage )
        ;

        $iCount = 0;
        while ( $aGoods = $query->parseEach() ) {

            $oGoodsRow = GoodsRow::get( $aGoods['id'] );

            $oGoodsRow->setData( $aData );

            $iCount++;
            $oGoodsRow->save();
        }

        $aData['id'] = $sectionId;

        if ($iCount >= $this->iExportLimit){
            $iPage++;
            $aData['page'] = $iPage;
            $this->addJSListener( 'reload_save_param', 'saveParam' );
            $this->fireJSEvent( 'reload_save_param', array('data' => $aData) );
        }

    }


    public function actionSaveCheck(){
        return psComplete;
    }

    public function actionInit() {
        $oFormBuilder = UI\StateBuilder::newEdit();
        $oFormBuilder
            ->addButton( (!is_file(WEBPATH.$this->sFilePath))?\Yii::t('yandexExport', 'create'):\Yii::t('yandexExport', 'refresh'), 'export', 'icon-add')
            ->addButton(\Yii::t('yandexExport', 'settings'), 'settings', 'icon-edit')
            ->addButton(\Yii::t('yandexExport', 'utils'), 'Utils', 'icon-edit');

        $sReportText = \SysVar::get('Yandex.report');

        if ($sReportText && is_file(WEBPATH.$this->sFilePath)){
            $oFormBuilder->addHeadText($sReportText);
        } else {
            $oFormBuilder->addHeadText("Выгрузка не создана");
        }

        $this->setInterface($oFormBuilder->getForm());
        return psComplete;
    }
}