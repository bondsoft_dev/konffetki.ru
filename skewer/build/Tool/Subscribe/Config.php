<?php

use skewer\build\Tool\LeftList;

$aConfig['name']     = 'Subscribe';
$aConfig['title']    = 'Рассылка';
$aConfig['version']  = '1.1';
$aConfig['description']  = 'Админ-интерфейс управления рассылкой новостей';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::TOOL;
$aConfig['group']     = LeftList\Group::CONTENT;
$aConfig['useNamespace'] = true;

/**
 * @todo что должны делать эти настройки?
 */
$aConfig['mode'] = array(
    'firstState' => 'list', // допустимые состояния: 'user' / 'list'
    'fullButtons' => true,
    'multiTemplates' => true,
    'extFields' => true,
);

return $aConfig;
