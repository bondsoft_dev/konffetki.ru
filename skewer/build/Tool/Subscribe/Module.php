<?php

namespace skewer\build\Tool\Subscribe;


use skewer\build\Component\UI\StateBuilder;
use skewer\build\Page\Subscribe\ar\SubscribeMessage;
use skewer\build\Page\Subscribe\ar\SubscribeMessageRow;
use skewer\build\Page\Subscribe\ar\SubscribePosting;
use skewer\build\Page\Subscribe\ar\SubscribeTemplate;
use skewer\build\Page\Subscribe\ar\SubscribeUser;
use skewer\build\Page\Subscribe\ar\SubscribeUserRow;
use skewer\build\Tool;
use yii\base\UserException;

class Module extends Tool\LeftList\ModulePrototype {

    protected $iOnPage = 20;
    protected $iPage = 0;

    protected function preExecute() {

        $this->iPage = $this->getInt('page');

    }

    public function actionInit(){

        if ( $this->getConfigParam('mode.firstState') == 'user' )
            $this->actionUsers();
        else
            $this->actionList();
    }

    /********************************** USER *********************************/

    public function actionUsers(){
        $oList = StateBuilder::newList();

        $oList->addHeadText('<h1>'.\Yii::t('subscribe', 'list').'</h1>');
        $oList
            ->addField('id', 'ID', 'i', 'hide')
            ->addField('email','E-mail','s','string',array('listColumns'=>array('flex'=>1)))
        ;

        $users = SubscribeUser::find()->getAll();
        $oList->setValue($users);

        $oList->addRowButtonUpdate('editUser');
        $oList->addRowButtonDelete('delUser','allow_do',\Yii::t('subscribe', 'deleteUser'));

        if ( $this->getConfigParam('mode.fullButtons') != false ) {
            $oList->addButton(\Yii::t('subscribe', 'subscribers'),'users','','init');
            $oList->addButton(\Yii::t('subscribe', 'templates'),'templates','','init');
            $oList->addButton(\Yii::t('subscribe', 'subscribe_btn'),'list','','init');
        } else {
            $oList->addButton(\Yii::t('subscribe', 'editTemplate'),'editTemplate','','init');
        }

        $oList->addBtnSeparator();
        $oList->addButton(\Yii::t('subscribe', 'addSubscriber'),'editUser','icon-add','init');

        $this->setInterface($oList->getForm());
    }

    public function actionEditUser() {

        $aData = $this->get('data');
        $iItemId = isSet($aData['id']) ? $aData['id'] : false;

        $oForm = StateBuilder::newEdit();
        $oForm->addHeadText('<h1>'.\Yii::t('subscribe', 'editSubscriber').'</h1>');

        $aItems = array();
        if( $iItemId )
            $aItems = SubscribeUser::find($iItemId);

        $oForm
            ->addField('id', 'ID', 'i', 'hide')
            ->addField('email','E-mail','s','string');
        $oForm->setValue($aItems);
        $oForm->addButtonSave('saveUser','icon-save','save');
        $oForm->addButton(\Yii::t('adm','cancel'),'users','icon-cancel');
        $this->setInterface($oForm->getForm());

        return psComplete;
    }

    public function actionSaveUser() {

        $aData = $this->get('data');

        try{
            // проверка входных данных
            if ( !isset($aData['email']) )
                throw new \Exception('email field not found');

            $sEmail = $aData['email'];

            // валидация поля email
            if ( !$sEmail )
                throw new \Exception(\Yii::t('subscribe', 'email_expected'));

            if ( !filter_var( $sEmail, FILTER_VALIDATE_EMAIL ) )
                throw new \Exception(\Yii::t('subscribe', 'invalid_email'));

            /** @var SubscribeUserRow $row */
            $row = SubscribeUser::load($aData);
            if ($row){
                $row->save();
            }

            $this->addNoticeReport(\Yii::t('subscribe', 'saving_mailing_user'), "email: $sEmail", \skLogger::logUsers, 'skewer\build\Tool\Subscribe\Module');

        } catch ( \Exception $e ) {
            $this->addError($e->getMessage());
        }
        $this->actionUsers();
    }

    public function actionDelUser() {

        $aData = $this->get('data');

        $iItemId = iSset($aData['id']) ? $aData['id'] : false;

        if( $iItemId ){
            SubscribeUser::delete($iItemId);
        }

        $this->actionUsers();
        $this->addNoticeReport(\Yii::t('subscribe', 'deleting_mailing_user'), "User: $aData[email]", \skLogger::logUsers, 'skewer\build\Tool\Subscribe\Module');
    }

    /********************************** TEMPLATES *********************************/

    public function actionTemplates() {

        $oList = StateBuilder::newList();
        $oList->addHeadText('<h1>'.\Yii::t('subscribe', 'subscribeList').'</h1>');

        $oList
            ->addField('id', 'ID', 'i', 'hide')
            ->addField('title',\Yii::t('subscribe', 'title'),'s','string',array('listColumns'=>array('flex'=>1)));

        $aItems = SubscribeTemplate::find()->getAll();
        $oList->setValue($aItems);

        $oList->addRowButtonUpdate('editTemplate');
        $oList->addRowButtonDelete('delTemplate','allow_do',\Yii::t('subscribe', 'deleteTemplate'));

        $oList->addButton(\Yii::t('subscribe', 'subscribers'),'users','','init');
        $oList->addButton(\Yii::t('subscribe', 'templates'),'templates','','init');
        $oList->addButton(\Yii::t('subscribe', 'subscribe_btn'),'list','','init');
        $oList->addBtnSeparator();
        $oList->addButton(\Yii::t('subscribe', 'addTemplate'),'editTemplate','icon-add');


        $this->setInterface($oList->getForm());
    }

    public function actionEditTemplate() {

        $aData = $this->get('data');
        $iItemId = isSet($aData['id']) ? $aData['id'] : false;

        $oForm = StateBuilder::newEdit();
        $oForm->addHeadText('<h1>'.\Yii::t('subscribe', 'editTemplateH1').'</h1>');

        $info = Api::addTextInfoBlock();

        $oForm->addHeadText('<h1>'.\Yii::t('subscribe', 'editTemplateH1').'</h1>'
            .'<div>'.$info['value'].'</div>');

        $oForm
            ->addField('id','ID','i','hide')
            ->addField('title',\Yii::t('subscribe', 'title'),'s','string')
            ->addField('content',\Yii::t('subscribe', 'text'),'s','wyswyg');

        if ($iItemId)
            $row = SubscribeTemplate::find($iItemId);
        else $row = SubscribeTemplate::getNewRow();

        $oForm->setValue($row);

        $oForm->useSpecSectionForImages();
        $oForm->addButtonSave('saveTemplate');

        if ( !$this->getConfigParam('mode.multiTemplates') )
            $oForm->addButton(\Yii::t('adm','cancel'),'users','icon-cancel');
        else
            $oForm->addButton(\Yii::t('adm','cancel'),'templates','icon-cancel');

        $this->setInterface($oForm->getForm());
        $this->addNoticeReport(\Yii::t('subscribe', 'saving_template'), "id $iItemId", \skLogger::logUsers, 'skewer\build\Tool\Subscribe\Module');
        return psComplete;

    }

    public function actionSaveTemplate() {

        $aData = $this->get('data');

        $row = SubscribeTemplate::load($aData);
        if ($row) {
            $row->save();
        }

        $this->addNoticeReport(\Yii::t('subscribe', 'saving_template'), "id $aData[id]", \skLogger::logUsers, 'skewer\build\Tool\Subscribe\Module');
        if ( !$this->getConfigParam('mode.multiTemplates') )
            $this->actionUsers();
        else
            $this->actionTemplates();

    }

    public function actionDelTemplate() {

        $aData = $this->get('data');

        $iItemId = iSset($aData['id']) ? $aData['id'] : false;

        if( $iItemId )
            SubscribeTemplate::delete($iItemId);

        $this->actionTemplates();
        $this->addNoticeReport(\Yii::t('subscribe', 'deleting_template'), "id $iItemId", \skLogger::logUsers, 'skewer\build\Tool\Subscribe\Module');

    }


    /********************************** SUBSCRIBE *********************************/

    public function actionList() {

        $oList = StateBuilder::newList();
        $oList->addHeadText('<h1>'.\Yii::t('subscribe', 'subList').'</h1>');

        $oList
            ->addField('id','ID','i','hide')
            ->addField('title',\Yii::t('subscribe', 'title'),'s','string',array('listColumns'=>array('flex'=>1)))
            ->addField('status',\Yii::t('subscribe', 'status'),'s','string',array('listColumns'=>array('flex'=>1)));

        $aItems = SubscribeMessage::find()->order('id','DESC')->asArray()->getAll();
        foreach($aItems as $iKey=>$aItem){
            $aItems[$iKey]['status'] = Api::getStatusName( $aItem['status'] );
        }
        $oList->setValue($aItems);

        $oList
            ->addRowButtonUpdate('editSubscribe')
            ->addRowButtonDelete('delSubscribe','delete',\Yii::t('subscribe', 'subListText'));

        $oList->addButton(\Yii::t('subscribe', 'subscribers'),'users','','init');
        $oList->addButton(\Yii::t('subscribe', 'templates'),'templates','','init');
        $oList->addButton(\Yii::t('subscribe', 'subscribe_btn'),'list','','init');
        $oList->addBtnSeparator();
        $oList->addButton(\Yii::t('subscribe', 'addSubscribe'),'addSubscribeStep1','icon-add','init');

        $this->setInterface($oList->getForm());

    }

    public function actionAddSubscribeStep1() {
        $oForm = new \ExtForm();

        $oForm->setAddText('<h1>'.\Yii::t('subscribe', 'getTemplate').'</h1>');

        /* установить набор элементов формы */
        $aModel = Api::getChangeTemplateInterface();

        $oForm->setFields($aModel);
        $oForm->setValues(array());

        $oForm->addExtButton( \ExtDocked::create(\Yii::t('subscribe', 'next'))
            ->setIconCls( \ExtDocked::iconNext )
            ->setAction('addSubscribeStep2')
            ->unsetDirtyChecker()
        );

        $oForm->addDockedItem(array(
            'text' => \Yii::t('subscribe', 'cancel'),
            'iconCls' => 'icon-cancel',
            'state' => 'init',
            'action' => 'list',
        ));



        $this->setInterface($oForm);

        return psComplete;

    }

    public function actionAddSubscribeStep2() {

        $aData = $this->get('data');
        $iItemId = isSet($aData['tpl']) ? $aData['tpl'] : false;

        $info = Api::addTextInfoBlock();

        $oForm = StateBuilder::newEdit();
        $oForm->addHeadText('<h1>'.\Yii::t('subscribe', 'editSubscribeText').'</h1>'.
            "<div>".$info['value']."</div>"
        );
        $aStatusData = \ExtForm::getDesc4SelectFromArray(Api::getStatusArr());

        $oForm
            ->addField('id','id','i','hide')
            ->addField('title',\Yii::t('subscribe', 'title'),'s','string')
            ->addField('text',\Yii::t('subscribe', 'text'),'s','wyswyg')
            ->addField('status',\Yii::t('subscribe', 'status'),'s','select',array_merge(array('disabled'=>true),$aStatusData))
            ->addField('template',\Yii::t('subscribe', 'template'),'s','hide')
        ;

        $aItems = array();
        $aTempItems =  SubscribeTemplate::find()->where('id',$iItemId)->asArray()->getOne();

        $aItems['title'] = isSet($aTempItems['title']) ? $aTempItems['title'] : '';
        $aItems['text'] = isSet($aTempItems['content']) ? $aTempItems['content'] : '';
        $aItems['status'] = Api::statusFormation;
        $aItems['template'] = $iItemId;

        $oForm->setValue($aItems);

        $oForm->addButtonSave('saveSubscribe');
        $oForm->addButtonCancel('addSubscribeStep1');
        $oForm->useSpecSectionForImages();
        $this->setInterface($oForm->getForm());

        return psComplete;
    }

    public function actionEditSubscribe() {

        $aData = $this->get('data');
        $iItemId = isSet($aData['id']) ? $aData['id'] : false;
        if( !$iItemId )
            throw new \Exception (\Yii::t('subscribe', 'wrong_subscribe_id'));

        $oForm = StateBuilder::newEdit();

        $info = Api::addTextInfoBlock();
        $oForm->addHeadText('<h1>'.\Yii::t('subscribe', 'editSubscribe').'</h1>'.
            "<div>".$info['value']."</div>");

        $aTempItems = SubscribeMessage::find()->where('id',$iItemId)->asArray()->getOne();
        $aStatusData = \ExtForm::getDesc4SelectFromArray(Api::getStatusArr());

        $bTypeView = true;
        // блокироум редактирование по статусу
        if( !isSet($aTempItems['status']) ||  $aTempItems['status'] != Api::statusFormation ){
            $bTypeView = false;
        }

        $oForm
            ->addField('id','ID','s','hide')
            ->addField('title',\Yii::t('subscribe', 'title'),'s',($bTypeView)?'string':'show')
            ->addField('text',\Yii::t('subscribe', 'text'),'s', ($bTypeView)?'wyswyg':'show')
            ->addField('status',\Yii::t('subscribe', 'status'),'s','select',array_merge(array('disabled'=>true),$aStatusData))
            ->addField('template',\Yii::t('subscribe', 'template'),'s','hide')
        ;

        $aTempItems['title'] = isSet($aTempItems['title']) ? $aTempItems['title'] : '';
        $aTempItems['text'] = isSet($aTempItems['text']) ? $aTempItems['text'] : '';
        $aTempItems['template'] = $iItemId;

        $oForm->setValue($aTempItems);

        if( isSet($aTempItems['status']) && $aTempItems['status'] == Api::statusFormation ) {

            $oForm->addButton(\Yii::t('subscribe', 'send'),'sendSubscribeForm','icon-commit','init');
            $oForm->addBtnSeparator();
            $oForm->addButtonSave( 'saveSubscribe' );
        }
        $oForm->useSpecSectionForImages();
        $oForm->addButtonCancel('list');

        $this->setInterface($oForm->getForm());
        return psComplete;
    }

    public function actionSaveSubscribe() {

        $aData = $this->get('data');

        /** @var SubscribeMessageRow $row  */
        $row = SubscribeMessage::load($aData);
        if ($row){
            $row->status = Api::statusFormation;
            $row->save();
        }

        $this->addNoticeReport(\Yii::t('subscribe', 'saving_mailing'), "id $row->id", \skLogger::logUsers, 'skewer\build\Tool\Subscribe\Module');
        $this->actionList();
    }

    public function actionDelSubscribe() {

        $aData = $this->get('data');
        $iItemId = iSset($aData['id']) ? $aData['id'] : false;

        if( $iItemId ){
            SubscribePosting::delete()->where('id_body',$iItemId)->get();
            SubscribeMessage::delete($iItemId);
        }

        $this->addNoticeReport(\Yii::t('subscribe', 'deleting_mailing'), "id $iItemId", \skLogger::logUsers, 'skewer\build\Tool\Subscribe\Module');
        $this->actionList();
    }

    public function actionSendSubscribeForm() {

        $aData = $this->get('data');
        $iItemId = isSet($aData['id']) ? $aData['id'] : false;
        if( !$iItemId )
            throw new \Exception (\Yii::t('subscribe', 'wrong_subscribe_id'));


        $oForm = StateBuilder::newEdit();
        $oForm->addHeadText('<h1>'.\Yii::t('subscribe', 'sending').'</h1>');
        $oForm
            ->addField('id','ID','s','hide')
            ->addField('title',\Yii::t('subscribe', 'title'),'s','show')
            ->addField('text',\Yii::t('subscribe', 'text'),'s','show')
            ->addField('test_mail',\Yii::t('subscribe', 'test_email_title'),'s','string',array('subtext'=>\Yii::t('subscribe', 'testMail')))
        ;

        $aTempItems = SubscribeMessage::find()->where('id',$iItemId)->asArray()->getOne();
        $aTempItems['title'] = isSet($aTempItems['title']) ? $aTempItems['title'] : '';
        $aTempItems['text'] = isSet($aTempItems['text']) ? $aTempItems['text'] : '';

        $oForm->setValue($aTempItems);

        $oForm->addButton(\Yii::t('subscribe', 'sendSubscribers'),'sendSubscribe','icon-commit','allow_do',array('actionText'=>\Yii::t('subscribe', 'sendSubscribersText')));
        $oForm->addButton(\Yii::t('subscribe', 'testMailText'),'sendToEmailSubscribe','icon-commit','init',array('doNotUseTimeout'=>true));

        $oForm->addButtonCancel('list');

        $oForm->setTrackChanges(false);

        $this->setInterface($oForm->getForm());
        return psComplete;

    }

    public function actionSendSubscribe() {

        try{

            $aData = $this->get('data');

            $iItemId = iSset($aData['id']) ? $aData['id'] : false;

            if( !$iItemId )
                throw new \Exception (\Yii::t('subscribe', 'wrong_message_id'));

            $iSubId = Api::addMailer($iItemId);
            if( !$iSubId )
                throw new \Exception (\Yii::t('subscribe', 'not_added'));
            /** @var SubscribeMessageRow $row */
            $row = SubscribeMessage::find($iItemId);
            if ($row) {
                $row->status = Api::statusWaiting;
                $row->save();
            }

            Api::makeTask( $iSubId );

            $this->addMessage(\Yii::t('subscribe', 'mailing_created'));
            $this->addNoticeReport(\Yii::t('subscribe', 'log_create_mailing'), "", \skLogger::logUsers, 'skewer\build\Tool\Subscribe\Module');

        } catch ( \Exception $e ) {
            $this->addError($e->getMessage());
        }

        $this->actionList();

        return psComplete;

    }

    public function actionSendToEmailSubscribe() {

        $aData = $this->get('data');

        $iItemId = iSset($aData['id']) ? $aData['id'] : false;
        $sTestMail = iSset($aData['test_mail']) ? $aData['test_mail'] : false;

        if( !$iItemId )
            throw new UserException(\Yii::t('subscribe', 'wrong_message_id'));

        if ( !$sTestMail )
            throw new UserException( \Yii::t('subscribe', 'email_expected') );

        if( !Api::sendTestMailer($iItemId, $sTestMail) )
            throw new \Exception (\Yii::t('subscribe', 'test_messages_fail'));

        $this->addMessage(\Yii::t('subscribe', 'test_email_sended'));
        $this->addNoticeReport(\Yii::t('subscribe', 'test_email_sended'), "", \skLogger::logUsers, 'skewer\build\Tool\Subscribe\Module');

        $this->actionList();

        return psComplete;

    }

}//class