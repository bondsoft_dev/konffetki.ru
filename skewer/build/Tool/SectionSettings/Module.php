<?php

namespace skewer\build\Tool\SectionSettings;

use skewer\build\Component\Search;
use skewer\build\Component\Section\Parameters;
use skewer\build\Component\Site\Type;
use skewer\build\Component\UI\StateBuilder;
use skewer\build\Tool;
use skewer\build\Page\CatalogViewer;

/**
 * Модуль Настроек
 * Class Module
 * @package skewer\build\Tool\SectionSettings
 */
class Module extends Tool\LeftList\ModulePrototype {

    protected function actionInit() {

        $oForm = StateBuilder::newEdit();

        $oForm
            ->field( 'menuIcons', \Yii::t('page', 'menuIcons'), 'i', 'check')
            ->field( 'newsDetailLink', \Yii::t('page', 'newsDetailLink'), 'i', 'check')         
        ;

        $oForm->setValue(
            [
                'menuIcons' => \SysVar::get('Menu.ShowIcons'),
                'newsDetailLink' => \SysVar::get('News.showDetailLink'),
            ]
        );

        $oForm->addButtonSave('save');
        $oForm->addButtonCancel('init');

        $this->setInterface($oForm->getForm());
    }

    /**
     * Сохранение
     */
    protected function actionSave(){

        \SysVar::set('Menu.ShowIcons', $this->getInDataVal('menuIcons'));
        \SysVar::set('News.showDetailLink', $this->getInDataVal('newsDetailLink'));

        $this->actionInit();
    }

}