<?php

$aLanguage = array();

$aLanguage['ru']['SectionSettings.Tool.tab_name'] = 'Разделы';
$aLanguage['ru']['menuIcons'] = 'Иконки пунктов меню';
$aLanguage['ru']['newsDetailLink'] = 'Ссылка "Подробнее" в новостях';

$aLanguage['en']['SectionSettings.Tool.tab_name'] = 'Sections';
$aLanguage['en']['menuIcons'] = 'Icons menu';
$aLanguage['en']['newsDetailLink'] = '"Read more" in the news';

return $aLanguage;