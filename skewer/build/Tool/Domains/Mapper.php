<?php
namespace skewer\build\Tool\Domains;
use skewer\build\Component\orm\Query;

/**
 * Класс для работы с доменами для площадки
 */
class Mapper extends \skMapperPrototype {

    /**
     * @var string Имя текущей таблицы, с которой работает маппер
     */
    protected static $sCurrentTable = 'domains';

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'd_id' => 'i:int:Domains.id',
        'domain_id' => 'i:int:Domains.domain_id',
        'domain' => 's:string:Domains.domain',
        'prim' => 'i:int:Domains.prim',
    );

    /**
     * Имя ключевого поля
     * @var string
     */
    protected static $sKeyFieldName = 'd_id';


    public static function delByName($sDomain){

        Query::SQL("DELETE FROM `domains` WHERE domain=?",$sDomain);
        return true;
    }
}
