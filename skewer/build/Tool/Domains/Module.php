<?php
namespace skewer\build\Tool\Domains;
use skewer\build\Tool;
use yii\base\UserException;

class Module extends Tool\LeftList\ModulePrototype {

    // id текущего раздела
    protected $iSectionId = 0;

    /**
     * Первичный обработчик
     * @throws UserException
     * @return bool
     */
    protected function preExecute(){
        
    }

    /**
     * Инициализация
     */
    protected function actionInit() {

        // вывод списка
        $this->actionList();
    }

    /**
     * Вывод списка
     * @param string $sMsg
     * @return int
     */
    protected function actionList($sMsg = ''){
        // объект для построения списка
        $oList = new \ExtList();

        try {

            if($sMsg) $this->addError($sMsg);

            /* Модель данных */
            $aModel = array("domain_id"=> array("name" => "domain_id",
                    "type" => "i",
                    "view" => "hide",
                    "jsView" => "hide",
                    "title" => \Yii::t('domains', 'module_id'),
                    "default" => "",
                    "hidden" => true,
                ),
                "domain"=> array("name" => "domain",
                    "type" => "s",
                    "view" => "select",
                    "title" => \Yii::t('domains', 'module_domain_name'),
                    "default" => "",
                ),
                "prim"=> array("name" => "prim",
                    "type" => "s",
                    "view" => "select",
                    "title" => \Yii::t('domains', 'module_main_domain'),
                    "default" => "",
                ),
            );

            $oList->setFields( $aModel );

            $aItems = Api::getAllDomains();

            $oList->setValues( $aItems['items'] );


               /*
            $oList->addDockedItem(array(
                'text' => 'Добавить домен',
                'iconCls' => 'icon-add',
                'state' => 'init',
                'action' => 'addDomainForm',
            ));
                 */
        } catch(\Exception $e) {
            $this->addError($e->getMessage());
        }

        // вывод данных в интерфейс
        $this->setInterface( $oList );

        return psComplete;
    }




}
