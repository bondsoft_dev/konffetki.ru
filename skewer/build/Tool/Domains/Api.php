<?php
namespace skewer\build\Tool\Domains;
use skewer\build\Component\SEO;


/**
 * API работы с резевным копированием
 *
 */
class Api {

    /**
     * Домен
     * @var string|null
     */
    private static $sDomain = null;

    public static function getAllDomains(){

        $aOut = Mapper::getItems(array());

        return $aOut;

    }

    /**
     * Отдает имя основного домена или false, если он не привязан
     * @return string|bool
     */
    public static function getMainDomain(){

        if (!is_null(static::$sDomain)){
            return static::$sDomain;
        }

        $aFilter = array('select_fields' => array('domain'),
                        'where_condition' => array( 'prim' => array (
                                                                'sign' => '=',
                                                                'value' => '1'
                                                               )));

        $mDomains = Mapper::getItems($aFilter);

        if(  isset($mDomains['items'][0]['domain']) ){
            static::$sDomain = $mDomains['items'][0]['domain'];
        }else{
            static::$sDomain = false;
        }

        return static::$sDomain;
    }

    /**
     * Отдает имя текущего домена.
     * Если привязан основной, но используется сейчас другой, будет возвращен основной
     * @return string
     */
    public static function getCurrentDomain(){
        $sMainDomain = self::getMainDomain();
        return $sMainDomain ? $sMainDomain : $_SERVER['HTTP_HOST'];
    }

    /**
     * получение массива редиректов на основной домен с вторичных
     * @static
     * @return array
     */
    public static function getRedirectItems(){

        $aOut = array();

        $aItems = self::getAllDomains();

        $aPrimItem = false;

        if($aItems['count']){

            foreach($aItems['items'] as $aItem){
                if($aItem['prim'])
                    $aPrimItem = $aItem;
            }

            if($aPrimItem){

                foreach($aItems['items'] as $aItem){
                    if($aItem['d_id'] != $aPrimItem['d_id'])
                        $aOut[] = array('old_url'=>$aItem['domain'], 'new_url'=>$aPrimItem['domain']);
                }


            }

        }


        return $aOut;
    }

    public static function syncDomains($aDomains){

        if(!$aDomains || !is_array($aDomains) || !count($aDomains)){

            $aRealDomain = self::getAllDomains();
            $aRealDomain = isset($aRealDomain['items']) ? $aRealDomain['items'] : array();

            foreach($aRealDomain as $aCurDomain)
                Mapper::delItem($aCurDomain);

            SEO\Service::setNewDomainToSiteMap();

            SEO\Service::updateRobotsTxt(false);
            \skewer\build\Tool\Redirect301\Api::makeHtaccessFile();

            return true;
        }


        $aRealDomain = self::getAllDomains();
        $aRealDomain = isset($aRealDomain['items']) ? $aRealDomain['items'] : array();

        $aUpdDomains = array();
        $aDelDomains = array();

        foreach($aDomains as $aCurDomain){
            $flag = false;
            foreach($aRealDomain as $aCurRealDomain){
                if($aCurDomain['domain'] == $aCurRealDomain['domain']){  // upd
                    $item = $aCurRealDomain;
                    $item['prim'] = $aCurDomain['prim'];
                    $item['domain_id'] = $aCurDomain['domain_id'];
                    $aUpdDomains[] = $item;
                    $flag = true;
                }
            }
            if(!$flag){ // new
                $item = array();
                $item['domain'] = $aCurDomain['domain'];
                $item['prim'] = $aCurDomain['prim'];
                $item['domain_id'] = $aCurDomain['domain_id'];
                $aUpdDomains[] = $item;

            }
        }

        foreach($aRealDomain as $aCurRealDomain){
            $flag = false;
            foreach($aDomains as $aCurDomain){
                if($aCurDomain['domain'] == $aCurRealDomain['domain'])
                    $flag = true;
            }
            if(!$flag || !count($aDomains))
                $aDelDomains[] = $aCurRealDomain['d_id'];
        }



        foreach($aUpdDomains as $aCurDomain){

            Mapper::saveItem($aCurDomain);

        }


        foreach($aDelDomains as $aCurDomain){

            Mapper::delItem($aCurDomain);

        }

        SEO\Service::setNewDomainToSiteMap();

        \skewer\build\Tool\Redirect301\Api::makeHtaccessFile();

        SEO\Service::updateRobotsTxt(self::getCurrentDomain());

        return true;
    }


    public static function addDomain($sDomain, $iPrim=0, $iDomainId = 0){

        $aParam = array('domain_id'=>$iDomainId,'domain'=>$sDomain,'prim'=>$iPrim);

        Mapper::saveItem($aParam);

        return true;
    }


    public static function delDomain($sDomain){

        Mapper::delByName($sDomain);

        return true;
    }


}
