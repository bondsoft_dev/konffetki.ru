<?php

namespace skewer\build\Tool\Messages;


class Api {

    //Типы сообщений
    const MSG_TYPE_DEFAULT = 1;     //По умолчанию
    const MSG_TYPE_EXTRA = 2;       //Экстренное
    const MSG_TYPE_IMPORTANT = 3;   //Важное
    const MSG_TYPE_WARNING = 4;     //Предупреждение

    //Новое сообщение
    const MSG_STATUS_NEW = 1;
    const MSG_STATUS_READ = 0;

    /**
     * Возвращает поля для списка
     * @return array
     */
    private static function getListFields(){
        return array(
            'title',
            'type',
            'arrival_date'
        );
    }

    /**
     * Возвращает модель списка
     * @return array
     */
    public static function getListModel(){
        return Mapper::getFullParamDefList(self::getListFields());
    }

    /**
     * Обновляет сообщение
     * @param $data
     * @return bool|int
     */
    public static function updMessage($data){
        return Mapper::saveItem($data);
    }

    /**
     * Возвращает сообщения
     * @return array
     */
    public static function getMessages(){

        $filter = array();
        $filter['order'] = array(
            'field' => 'arrival_date',
            'way' => 'DESC'
        );
        $messages = Mapper::getItems($filter);
        foreach ($messages['items'] as &$message) {
            $message['type'] = self::getMessageType($message['type']);
            $message['arrival_date'] = self::getDescriptionForDate($message['arrival_date']);

            if ($message['new']) {
                $message['title'] = '<b>'.$message['title'].'</b>';
                $message['type'] = '<b>'.$message['type'].'</b>';
                $message['arrival_date'] = '<b>'.$message['arrival_date'].'</b>';
            }
        }

        return $messages;
    }

    /**
     * Возвращает тип сообщения
     * @param $typeId
     * @return string
     */
    public static function getMessageType($typeId) {

        switch ($typeId) {
            case self::MSG_TYPE_DEFAULT:
                return \Yii::t('messages', 'type_default');
                break;
            case self::MSG_TYPE_EXTRA:
                return \Yii::t('messages', 'type_extra');
                break;
            case self::MSG_TYPE_IMPORTANT:
                return \Yii::t('messages', 'type_important');
                break;
            case self::MSG_TYPE_WARNING:
                return \Yii::t('messages', 'type_warning');
                break;
        }

        return false;
    }

    /**
     * Удаляет сообщениеа
     * @param $msgId
     * @return bool|int
     */
    public static function delMessage($msgId){
        return Mapper::delItem($msgId);
    }

    /**
     * Возвращает сообщение по id
     * @param $msgID
     * @return array
     */
    public static function getMessageById($msgID){

        $message = Mapper::getItem($msgID);
        $message['type'] = self::getMessageType($message['type']);
        $message['arrival_date'] = self::getDescriptionForDate($message['arrival_date']);

        return $message;
    }

    /**
     * Возвращает описание для даты
     * @param $sDate
     * @return string
     */
    public static function getDescriptionForDate($sDate) {

        $oStartDate = new \DateTime($sDate);
        $oEndDate   = new \DateTime(date('Y-m-d H:i:s',time()));
        $oDiff = $oStartDate->diff($oEndDate);

        $sStartDayNum = $oStartDate->format('m-d');
        $sTodayDayNum = $oEndDate->format('m-d');

        switch($oDiff->days) {

            //Меньше одного дня
            case 0:

                if ($oDiff->h < 1) {

                    if ($oDiff->i == 0) {
                        $sOut = \Yii::t('messages', 'field_now');
                    }
                    else $sOut = \Yii::t('messages', 'field_minutes', [$oDiff->i,self::getSuffix($oDiff->i)]);

                }
                elseIf($sStartDayNum == $sTodayDayNum) {
                    $sOut = \Yii::t('messages', 'field_today', $oStartDate->format('H:i:s'));
                }
                else {
                    $sOut = \Yii::t('messages', 'field_longago',
                        [$oStartDate->format('d.m.Y'), $oStartDate->format('H:i')]);
                }
                break;

            default:
                $sOut = \Yii::t('messages', 'field_longago',
                    [$oStartDate->format('d.m.Y'), $oStartDate->format('H:i')]);
                break;
        }
        return $sOut;
    }

    /**
     * Возвращает числительное
     * @param $iNum
     * @return string
     */
    public static function getSuffix($iNum) {

        $iLastNum    = (int) substr($iNum,-1);
        $iPreLastNum = (int) substr(round($iNum/10,0),-1);
        return ($iLastNum == 1)? (($iPreLastNum == 1)? \Yii::t('messages', 'minutes_1'):
            \Yii::t('messages', 'minutes_2')): (($iLastNum ==0 OR $iLastNum > 4) ?
            \Yii::t('messages', 'minutes_1'): ($iPreLastNum == 1)?
            \Yii::t('messages', 'minutes_1'): \Yii::t('messages', 'minutes_3'));
    }

    /**
     * Возвращает текст новых сообщений в зависимости от количества
     * @param $iNum
     * @return string
     */
    public static function getMessagesSuffix($iNum){

        $iLastNum    = (int) substr($iNum,-1);
        $iPreLastNum = (int) substr(round($iNum/10,0),-1);
        return ($iLastNum == 1)? (($iPreLastNum == 1)? \Yii::t('messages', 'messages_1'):
            \Yii::t('messages', 'messages_2')): (($iLastNum ==0 OR $iLastNum > 4)?
            \Yii::t('messages', 'messages_1'): ($iPreLastNum == 1)? \Yii::t('messages', 'messages_1'):
                \Yii::t('messages', 'messages_3'));
    }

    /**
     * Читает сообщение
     * @param $msgId
     * @return bool|int
     */
    public static function setMessageRead($msgId) {

        $data = array();
        $data['id'] = $msgId;
        $data['new'] = 0;
        Mapper::saveItem($data);

        $message = self::getMessageById($msgId);
        $data = array();
        $data['message_id'] = $msgId;
        $data['send_id'] = (isset($message['send_id'])) ? $message['send_id'] : false;

        return ReadMapper::saveItem($data);
    }

    /**
     * Говорит смс, что сообщение прочитано
     * @param $sendings
     */
    public static function setSendingRead($sendings){

        if ( !INCLUSTER )
            return;

        $oClient = \skGateway::createClient();
        $oClient->addMethod('MessagesToolService', 'iAmReadMessage', array($sendings),
            function ($mResult, $mError) use ($sendings) {

            });
        $oClient->doRequest();
    }

    /**
     * Возвращает сообщения, которые прочитаны
     * @return array
     */
    public static function getReadMessages(){
        return ReadMapper::getItems();
    }

    /**
     * Возвращает непрочитанные сообщения
     * @return array
     */
    public static function getUnreadMessages(){

        $filter = array();
        $filter['where_condition']['new'] = array(
            'sign' => '=',
            'value' => '1'
        );

        return Mapper::getItems($filter);
    }

    /**
     * Отчет о доставки отправлен
     * @param $msgId
     * @return bool|int
     */
    public static function setMessageSendRead($msgId){
        return ReadMapper::delItem($msgId);
    }
}
