<?php

namespace skewer\build\Tool\Messages;
/**
 * @class MessagesToolInstall
 */
class Install extends \skModuleInstall {

    public function __construct() {
    }

    public function init() {
        return true;
    }

    public function install() {

        \skewer\build\Component\orm\Query::SQL(
            "CREATE TABLE IF NOT EXISTS `messages` (
                `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
                `title` char(255) NOT NULL,
                `text` text NOT NULL,
                `type` int(1) NOT NULL COMMENT 'type of letter',
                `new` bit(1) NOT NULL DEFAULT b'1',
                `arrival_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
                `send_id` int(11) NOT NULL,
                `send_read` tinyint(1) NOT NULL,
                PRIMARY KEY (`id`),
                KEY `for_new` (`new`)
            ) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=1 ;"
        );

        \skewer\build\Component\orm\Query::SQL(
            "CREATE TABLE IF NOT EXISTS `messages_read` (
                `id` int(11) NOT NULL AUTO_INCREMENT,
                `send_id` int(11) DEFAULT NULL,
                PRIMARY KEY (`id`)
            ) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;"
        );

        \skewer\build\Component\orm\Query::SQL(
            "INSERT INTO `schedule` (
                `id`,`title`,`name`,`command`,`priority`,`resource_use`,`target_area`,`status`,`c_min`,`c_hour`,
                `c_day`,`c_month`,`c_dow`)
            VALUES (30,'Отчет о прочтении','send_read','{\"class\":\"Tool\\Messages\\Service\",\"method\":\"sendRead\",
                \"parameters\":[]}', 2, 4, 3, 1, 30, NULL, NULL, NULL, NULL);"
        );

        \skewer\build\Component\orm\Query::SQL(
            "INSERT INTO `group_policy_module` (`policy_id`,`module_name`,`title`)
             VALUES ((SELECT `id` FROM `group_policy` WHERE `alias`='admin' LIMIT 1), 'Messages','Сообщения');"
        );

        return true;
    }

    public function uninstall() {

        \skewer\build\Component\orm\Query::SQL( "DROP TABLE IF NOT EXISTS `messages`;" );
        \skewer\build\Component\orm\Query::SQL( "DROP TABLE IF NOT EXISTS `messages_read`;" );
        \skewer\build\Component\orm\Query::SQL( "DELETE FROM `schedule` WHERE `name`='send_read';" );

        return true;
    }
}
