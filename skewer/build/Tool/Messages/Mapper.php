<?php

namespace skewer\build\Tool\Messages;

class Mapper extends \skMapperPrototype {

    /** @var string Таблица */
    protected static $sCurrentTable = 'messages';

    /** @var array Список параметров */
    protected static $aParametersList = array(
        'id' => 'i:hide:messages.field_id',
        'title' => 's:str:messages.field_title',
        'text' => 's:str:messages.field_text',
        'type' => 's:str:messages.field_status',
        'new' => 'i:hide:messages.field_new',
        'arrival_date' => 's:str:messages.field_date',
        'send_id' => 'i:hide:messages.field_send_id',
        'send_read' => 'i:hide:Messages.field_sendread'
    );

    /**
     * Возвращает дополнительный набор параметров
     * @return array
     */
    protected static function getAddParamList() {

        return array(
            'title' => array(
                'listColumns' => array( 'flex' => 1 )
            ),
            'arrival_date' => array(
                'listColumns' => array('width' => 150)
            )
        );
    }
}