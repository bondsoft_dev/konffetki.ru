<?php

use skewer\build\Tool\LeftList;

$aConfig['name']     = 'Messages';
$aConfig['title']     = 'Сообщения';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Система сообщений';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::TOOL;
$aConfig['group']     = LeftList\Group::CONTENT;
$aConfig['useNamespace'] = true;

return $aConfig;
