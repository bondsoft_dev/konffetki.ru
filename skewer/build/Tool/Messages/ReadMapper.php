<?php

namespace skewer\build\Tool\Messages;

class ReadMapper extends \skMapperPrototype {

    /** @var string Таблица */
    protected static $sCurrentTable = 'messages_read';

    /** @var array Список параметров */
    protected static $aParametersList = array(
        'id' => 'i:hide:ID',
        'send_id' => 'i:hide:send ID',
    );
}
