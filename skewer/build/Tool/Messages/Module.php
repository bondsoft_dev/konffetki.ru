<?php

namespace skewer\build\Tool\Messages;


use skewer\build\Tool;

class Module extends Tool\LeftList\ModulePrototype {

    /**
     * Перед запуском
     * @return bool|void
     */
    protected function preExecute() {}

    /**
     * Первичное состояние
     * @return void
     */
    protected function actionInit() {
        $this->actionList();
    }

    /**
     * Список сообщений
     * @return void
     */
    protected function actionList() {

        $oList = new \ExtList();

        //Кнопки
        $oList->addRowBtnArray(array(
            'tooltip' => \Yii::t('adm','view'),
            'iconCls' => 'icon-view',
            'action' => 'msgShow',
            'state' => 'edit_form',
        ));
        $oList->addRowBtnDelete('msgDelete');

        //Поля
        $fields = Api::getListModel();
        $oList->setFields($fields);

        //Значения
        $values = Api::getMessages();
        $oList->setValues($values['items']);

        $this->setInterface($oList);
    }

    /**
     * Показывает сообщение
     * @return void
     */
    protected function actionMsgShow() {

        $oForm = new \ExtUserFile('Message');
        $this->addLibClass('MessageView');

        $oForm->addDockedItem(array(
            'text' => \Yii::t('messages', 'back'),
            'action' => 'list',
            'iconCls' => 'icon-cancel'
        ));

        $this->setCmd('load');

        $data = $this->get('data');
        $msgId = (isset($data['id'])) ? $data['id'] : false;
        $message = Api::getMessageById($msgId);
        $body = \skParser::parseTwig('message.twig', $message, BUILDPATH.'Tool/Messages/templates/');

        if ($message['new']) {
            Api::setMessageRead($msgId);
        }

        $this->setData('message', $body);

        $this->fireJSEvent( 'reloadMessageBar' );

        $this->setInterface($oForm);
    }

    /**
     * Удаляет сообщение
     * @return void
     */
    protected function actionMsgDelete(){

        $data = $this->get('data');
        $msgId = (isset($data['id'])) ? $data['id'] : false;

        Api::delMessage($msgId);
        $this->actionList();
    }
}