<?php

namespace skewer\build\Tool\Crm;


use skewer\build\Component\UI;
use skewer\build\Tool;
use yii\helpers\ArrayHelper;


class Module extends Tool\LeftList\ModulePrototype  {

    protected function actionInit() {

        $formBuilder = UI\StateBuilder::newEdit();
        $formBuilder->addField('token', \Yii::t('crm','token'), 's', 'str');
        $formBuilder->addField('email', \Yii::t('crm','email'), 's', 'str');

        $values['token'] = \SysVar::get('crm_token');
        $values['email'] = \SysVar::get('crm_email');
        $formBuilder->setValue($values);

        // добавляем элементы управления
        $formBuilder->addButton( \Yii::t('adm','save'), 'save', 'icon-save' );

        $this->setInterface( $formBuilder->getForm() );

        return psComplete;
    }

    protected function actionSave(){
        try {
            $aData = $this->getInData();
            \SysVar::set('crm_token', ArrayHelper::getValue($aData,'token',''));
            \SysVar::set('crm_email', ArrayHelper::getValue($aData,'email',''));
            $this->actionInit();
        } catch ( \Exception $e ) {
            $this->addError( $e->getMessage() );
        }
    }
}