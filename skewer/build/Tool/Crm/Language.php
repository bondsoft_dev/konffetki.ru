<?php

$aLanguage = array();

$aLanguage['ru']['tab_name'] = 'CRM';
$aLanguage['ru']['token'] = 'Токен';
$aLanguage['ru']['email'] = 'Email';

$aLanguage['en']['tab_name'] = 'CRM';
$aLanguage['en']['token'] = 'Token';
$aLanguage['en']['email'] = 'Email';

return $aLanguage;