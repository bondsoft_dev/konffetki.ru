<?php

namespace skewer\build\Tool\ModulesManager;


use skewer\build\Component\Installer as Installer;
use skewer\build\Component\UI;
use skewer\build\Tool;
use skewer\build\libs\ExtBuilder;

/**
 * Менеджер модулей
 * @class ModulesManager
 * @extends Tool\LeftList\ModulePrototype
 * @project Skewer
 * @package modules
 *
 * @author ArmiT
 */
class Module extends Tool\LeftList\ModulePrototype {

    /**
     * Метки статусов модулей для словарей
     * @var array
     */
    protected $modulesStatus = array(
        Installer\Api::INSTALLED => 'module_status_installed',
        Installer\Api::N_INSTALLED => 'module_status_notinstalled',
    );


    /**
     * Набор выводимых в списке полей
     * @return array
     */
    protected function getFieldList() {
        return array(
            'moduleName' => array(
                \Yii::t('modulesManager', 'field_module_name'),
                3,
            ),
            'definer' => array(
                \Yii::t('modulesManager', 'field_layer').'/'.\Yii::t('modulesManager', 'field_module_name'),
                2,
            ),
            'version' => array(
                \Yii::t('modulesManager', 'field_version'),
                1,
            ),
            'status' => array(
                \Yii::t('modulesManager', 'field_status'),
                2,
            ),
        );
    }

    /**
     * фильтр слоя
     * @var string
     */
    protected $layerFilter = \Layer::PAGE;

    /**
     * Фильтр статуса
     * @var int
     */
    protected $statusFilter = Installer\Api::ALL;

    /**
     * Инстанс инсталлера
     * @var null|Installer\Api
     */
    protected $installer = null;

    protected function preExecute() {

        $this->installer = new Installer\Api();
        $this->layerFilter = Api::checkLayer($this->get('layers_filter', $this->layerFilter), $this->layerFilter);
        $this->statusFilter = (int)$this->get('status_filter', $this->statusFilter);
        $this->statusFilter = !$this->statusFilter? (Installer\Api::INSTALLED | Installer\Api::N_INSTALLED): $this->statusFilter;

    }

    protected function actionInit() {

        $this->actionList();
    }// func

    protected function actionList() {

        $oList = new \ExtList();

        /* Собираем список */
        foreach($this->getFieldList() as $name => $field){

            $oField = new ExtBuilder\Field\String();
            $oField->setName($name);
            $oField->setTitle($field[0]);
            $oField->setAddListDesc(array(
                'flex' => $field[1],
            ));

            //$oField->setValue( $aItem[ $aType['val'] ] );
            $oList->addField( $oField );
        }

        $modules = $this->installer->getModules($this->layerFilter, $this->statusFilter);

        $items = array();

        foreach($modules as $module){
            $items[] = array(
                'moduleName' => $module->moduleConfig->getTitle(),
                'definer' => $module->layer.' / '.$module->moduleName,
                'version' => $module->moduleConfig->getVersion(),
                'status' => $module->alreadyInstalled?
                    \Yii::t('modulesManager', $this->modulesStatus[Installer\Api::INSTALLED]):
                    \Yii::t('modulesManager', $this->modulesStatus[Installer\Api::N_INSTALLED]),
            );
        }
        $oList->setValues($items);

        //$oList->addFilterText( 'search', '', 'Поиск' );


        /* Добавить фильтр по слоям */
        $filter = array();
        foreach(Api::getLayers() as $layer)
            $filter[$layer] = \Yii::t('modulesManager', 'layer_name_'.$layer);

        $oList->addFilterSelect( 'layers_filter', $filter, $this->layerFilter, \Yii::t('modulesManager', 'field_layer'), array(
            'set' => true
        ) );

        /* Добавить фильтр по статусу установки */
        $oList->addFilterSelect( 'status_filter', $this->getStatusList(), $this->statusFilter, \Yii::t('modulesManager', 'field_status') );

//        $oList->setOnPage( 20 );
//        $oList->setPageNum( 4 );
//        $this->getActionMethodName()

        $btn = new UI\Element\RowButton();
        $btn->setTitle(\Yii::t('modulesManager', 'list_view_btn'));
        $btn->setIcon('icon-view');
        $btn->setPhpAction('View');
        $btn->setJsState('edit_form');
        $oList->addRowBtn($btn);

        $this->setInterface( $oList );

    }

    protected function actionView() {

        $oForm = new \ExtForm();

        try {

            $data = $this->get('data');

            list($data['layer'], $data['moduleName']) = explode(' / ', $data['definer']);

            $module = Installer\IntegrityManager::init($data['moduleName'], $data['layer']);

            /* Заголовочная часть */
            $oForm->setAddText(sprintf(
                '<h2>%s (%s/%s %s)</h2>',
                $module->moduleConfig->getTitle(),
                $module->moduleConfig->getLayer(),
                $module->moduleConfig->getName(),
                $module->moduleConfig->getVersion()
            ));

            /* Статус */
            $oField = new ExtBuilder\Field\Show();
            $oField->setName('status');
            $oField->setTitle(\Yii::t('modulesManager', 'field_status'));
            $oField->setValue(
                $module->alreadyInstalled?
                    \Yii::t('modulesManager', 'module_status_installed'):
                    \Yii::t('modulesManager', 'module_status_notinstalled')
            );
            $oForm->addField( $oField );

            /* Имя модуля */
            $oField = new ExtBuilder\Field\Show();
            $oField->setName('moduleName');
            $oField->setTitle('ID');
            $oField->setValue( $module->moduleName );
            $oForm->addField( $oField );

            /*Слой*/
            $oField = new ExtBuilder\Field\Show();
            $oField->setName('layer');
            $oField->setTitle(\Yii::t('modulesManager', 'field_layer'));
            $oField->setValue( $module->layer );
            $oForm->addField( $oField );

            /*Ревизия*/
            $oField = new ExtBuilder\Field\Show();
            $oField->setName('revision');
            $oField->setTitle(\Yii::t('modulesManager', 'field_version'));
            $oField->setValue( $module->moduleConfig->getRevision());
            $oForm->addField( $oField );

            try {

                $dependencyTree = $this->installer->getInstallTree($module, true);
                if(count($dependencyTree))
                    array_splice($dependencyTree, -1, 1); // удалить текущий модуль, дабы не вводить в заблуждение


                if(count($dependencyTree)) {
                    $oField = new ExtBuilder\Field\Show();
                    $oField->setName('delim');
                    $oField->setTitle(\Yii::t('modulesManager', 'deptree_delim'));
                    $oField->setValue('');
                    $oForm->addField( $oField );
                }

                foreach($dependencyTree as $item) {

                    $oField = new ExtBuilder\Field\Show();
                    $oField->setName('dependency_'.$item->moduleName.$item->layer);

                    $oField->setTitle(
                        sprintf(
                            '%s (%s/%s %s)',
                            $item->moduleConfig->getTitle(),
                            $item->moduleConfig->getLayer(),
                            $item->moduleConfig->getName(),
                            $item->moduleConfig->getVersion(),
                            $item->moduleConfig->getDescription()
                        )
                    );
                    $oField->setValue(
                        $item->alreadyInstalled?
                            \Yii::t('modulesManager', 'module_status_installed'):
                            \Yii::t('modulesManager', 'module_status_notinstalled')
                    );

                    $oForm->addField( $oField );
                }

            } catch(\Exception $e) {

                $oField = new ExtBuilder\Field\Show();
                $oField->setName('dependency_attention');

                $oField->setTitle(\Yii::t('modulesManager', 'wrong_dependency_header'));
                $oField->setValue(\Yii::t('modulesManager', 'wrong_dependency' ,$e->getMessage()));
                $oForm->addField( $oField );

            }

            /* к списку */
            $oForm->addExtButton(
                \ExtDocked::create(\Yii::t('adm','cancel'))
                    ->setTitle(\Yii::t('modulesManager', 'tolist_btn'))
                    ->setIconCls( \ExtDocked::iconCancel )
                    ->setAction('List')
                    ->unsetDirtyChecker()
            );


            if($module->alreadyInstalled) {

                $oForm->addExtButton(
                    \ExtDocked::create(\Yii::t('adm','save'))
                        ->setTitle(\Yii::t('modulesManager', 'remove_btn'))
                        ->setIconCls( \ExtDocked::iconDel )
                        ->setAction('remove')
                        ->setAddParamList( array(
                            'moduleName' => $module->moduleName,
                            'layer' => $module->layer,
                        ))
                        ->setConfirm(\Yii::t('modulesManager', 'remove_confirm'))
                );

                $oForm->addExtButton(
                    \ExtDocked::create(\Yii::t('adm','save'))
                        ->setTitle(\Yii::t('modulesManager', 'reinstall_btn'))
                        ->setIconCls( \ExtDocked::iconReinstall )
                        ->setAction('reinstall')
                        ->setAddParamList( array(
                            'moduleName' => $module->moduleName,
                            'layer' => $module->layer,
                        ))
                        ->setConfirm(\Yii::t('modulesManager', 'reinstall_confirm'))
                );

                $oForm->addExtButton(
                    \ExtDocked::create(\Yii::t('adm','save'))
                        ->setTitle(\Yii::t('modulesManager', 'updconf_btn'))
                        ->setIconCls( \ExtDocked::iconConfiguration )
                        ->setAction('updateConfig')
                        ->setAddParamList( array(
                            'moduleName' => $module->moduleName,
                            'layer' => $module->layer,
                        ))
                        ->setConfirm(\Yii::t('modulesManager', 'updconf_confirm'))
                );

                $oForm->addExtButton(
                    \ExtDocked::create(\Yii::t('adm','save'))
                        ->setTitle(\Yii::t('modulesManager', 'upddict_btn'))
                        ->setIconCls( \ExtDocked::iconLanguages )
                        ->setAction('updateDictionary')
                        ->setAddParamList( array(
                            'moduleName' => $module->moduleName,
                            'layer' => $module->layer,
                        ))
                        ->setConfirm(\Yii::t('modulesManager', 'upddict_confirm'))
                );

            } else {

                $oForm->addExtButton(
                    \ExtDocked::create(\Yii::t('adm','save'))
                        ->setTitle(\Yii::t('modulesManager', 'install_btn'))
                        ->setIconCls( \ExtDocked::iconInstall )
                        ->setAction('install')
                        ->setAddParamList( array(
                            'moduleName' => $module->moduleName,
                            'layer' => $module->layer,
                        ))
                        ->setConfirm(\Yii::t('modulesManager', 'install_confirm'))
                );

            }


        } catch(\Exception $e) {

            $this->addError($e->getMessage());
        }

        $this->setInterface( $oForm );

    }

    protected function actionInstall() {

        $moduleName = $this->get('moduleName');
        $layer = $this->get('layer');

        try {

            $this->installer->install($moduleName, $layer);
            $this->addMessage(\Yii::t('modulesManager', 'install_successful', [$layer, $moduleName]));
        } catch(\Exception $e) {
            $this->addError($e->getMessage());
        }

        $this->actionList();
    }

    protected function actionRemove() {

        $moduleName = $this->get('moduleName');
        $layer = $this->get('layer');

        try {

            $this->installer->uninstall($moduleName, $layer);
            $this->addMessage(\Yii::t('modulesManager', 'remove_successful', [$layer, $moduleName]));
        } catch(\Exception $e) {
            $this->addError($e->getMessage());
        }

        $this->actionList();

    }

    protected function actionReinstall() {

        $moduleName = $this->get('moduleName');
        $layer = $this->get('layer');

        try {

            $this->installer->uninstall($moduleName, $layer);
            $this->installer->install($moduleName, $layer);
            $this->addMessage(\Yii::t('modulesManager', 'reinstalled_successful', [$layer, $moduleName]));
        } catch(\Exception $e) {
            $this->addError($e->getMessage());
        }

        $this->actionList();

    }
    protected function actionUpdateConfig() {

        $moduleName = $this->get('moduleName');
        $layer = $this->get('layer');

        try {

            $this->installer->updateConfig($moduleName, $layer);
            $this->addMessage(\Yii::t('modulesManager', 'updconf_successful', [$layer, $moduleName]));
        } catch(\Exception $e) {
            $this->addError($e->getMessage());
        }

        $this->actionList();

    }

    protected function actionUpdateDictionary() {

        $moduleName = $this->get('moduleName');
        $layer = $this->get('layer');

        try {

            $this->installer->updateLanguage($moduleName, $layer, true);
            $this->addMessage(\Yii::t('modulesManager', 'upddict_successful', [$layer, $moduleName]));
        } catch(\Exception $e) {
            $this->addError($e->getMessage());
        }

        $this->actionList();

    }

    /**
     * @return array
     */
    protected function getStatusList()
    {
        $aStatusList = [];
        foreach($this->modulesStatus as $iStatusNum => $sStatus){
            $aStatusList[$iStatusNum] = \Yii::t('modulesManager', $sStatus);
        }
        return $aStatusList;
    }

}