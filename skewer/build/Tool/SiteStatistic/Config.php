<?php

use skewer\build\Tool\LeftList;

/* main */
$aConfig['name']     = 'SiteStatistic';
$aConfig['title']    = 'Статистика сайта';
$aConfig['version']  = '1.000';
$aConfig['description']  = 'Настройка статистики сайта';
$aConfig['revision'] = '0002';
$aConfig['layer']     = \Layer::TOOL;
$aConfig['group']     = LeftList\Group::ADMIN;
$aConfig['useNamespace'] = true;

return $aConfig;
