<?php

namespace skewer\build\Tool\SiteStatistic;

use yii\web\AssetBundle;

class Asset extends AssetBundle {
    public $sourcePath = '@skewer/build/Tool/SiteStatistic/web/';
}