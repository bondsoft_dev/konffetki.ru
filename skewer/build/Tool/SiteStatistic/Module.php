<?php

namespace skewer\build\Tool\SiteStatistic;

use skewer\build\Tool;

/**
 * Модуль для управления статистикой на сайте
 */
class Module extends Tool\LeftList\ModulePrototype {

    // перед выполнением
    protected function preExecute() {

    }

    /**
     * Инициализация
     */
    public function actionInit() {


        /* Строим список изображений */
        $oInterface = new \ExtUserFile( 'SiteStatistic' );

        $oInterface->setModuleLangValues(array(
            'statOn',
            'statOff'
        ));

        $bActive = (bool)\SysVar::get('calc_page_views');

        $this->setData('siteStatus',$bActive);


        if ( $bActive ) {

            $oInterface->addDockedItem(array(
                'text' => \Yii::t('siteStatistic', 'notcollect'),
                'iconCls' => 'icon-stop',
                'action' => 'stop'
            ));


        } else {

            $oInterface->addDockedItem(array(
                'text' => \Yii::t('siteStatistic', 'collect'),
                'iconCls' => 'icon-visible',
                'action' => 'start'
            ));

        }

        $this->setInterface( $oInterface );

    }

    /**
     * Закрывает площадку
     */
    protected function actionStop() {

        \SysVar::set('calc_page_views',0);

        $this->actionInit();

    }

    /**
     * Открывает площадку
     */
    protected function actionStart() {

        \SysVar::set('calc_page_views',1);

        $this->actionInit();

    }

}
