/**
 * Интерфейс статистики
 */
Ext.define('Ext.Tool.SiteStatistic',{

    extend: 'Ext.panel.Panel',

    execute: function( data) {

        var me = this;
        var message;

        if ( data['siteStatus'] )
            message = me.lang.statOn;
        else
            message = me.lang.statOff;

        // вывод текста
        this.add({
            padding: 10,
            border: 0,
            html: message
        });

    }

});
