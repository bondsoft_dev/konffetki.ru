<?php

$aLanguage = array();

$aLanguage['ru']['tab_name'] = 'Статистика сайта';
$aLanguage['ru']['notcollect'] = 'Не собирать статистику';
$aLanguage['ru']['collect'] = 'Собирать статистику';
$aLanguage['ru']['statOn'] = 'Статистика просмотров страниц собирается.';
$aLanguage['ru']['statOff'] = 'Статистика просмотров страниц не собирается.';

$aLanguage['en']['tab_name'] = 'Site statistic';
$aLanguage['en']['notcollect'] = 'Do not collect statistics';
$aLanguage['en']['collect'] = 'Collect statistics';
$aLanguage['en']['statOn'] = 'Statistics page views going.';
$aLanguage['en']['statOff'] = 'Statistics pages is collected.';


return $aLanguage;