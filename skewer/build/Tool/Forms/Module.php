<?php

namespace skewer\build\Tool\Forms;

use skewer\build\Component\Catalog;
use skewer\build\Component\ReachGoal;
use skewer\build\Component\orm\Query;
use skewer\build\Component\UI;
use skewer\build\Component\Forms;
use skewer\build\Tool;
use skewer\build\Page;
use skewer\build\Component\Site;

/**
 * Class Module
 * @package skewer\build\Tool\Forms
 */
class Module extends Tool\LeftList\ModulePrototype {

    public $iSectionId;
    public $iCurrentForm = 0;
    public $enableSettings = 0;

    protected function preExecute() {

        // id текущего раздела
        $this->iCurrentForm = $this->getInt('form_id');

        if ( !$this->iCurrentForm )
            $this->iCurrentForm = $this->getEnvParam('form_id');

        if ( $this->getInt('sectionId') )
            $this->iSectionId = $this->getInt('sectionId');

        if ( !$this->iSectionId )
            $this->iSectionId = $this->getEnvParam('sectionId');

    }


    protected function actionInit() {
        $this->actionList();
    }


    /**
     * Список форм
     */
    protected function actionList() {

        // -- обработка данных
        $this->iCurrentForm = 0;

        $oQuery = Forms\Table::find();
        if ( !\CurrentAdmin::isSystemMode() )
            $oQuery->where( 'form_handler_type<>?', 'toMethod' );
        $aItems = $oQuery->getAll();

        $sHandlerLabel = \CurrentAdmin::isSystemMode() ? \Yii::t('forms', 'form_handler_value') :
            \Yii::t('forms', 'formHandlerEmailTitle') ;


        // -- сборка интерфейса
        $oFormBuilder = UI\StateBuilder::newList();

        $this->setPanelName( \Yii::t('forms', 'form_list') );

        // добавляем поля
        $oFormBuilder
            ->addField( 'form_title', \Yii::t('forms', 'form_title'), 's', 'string',
                        array( 'listColumns' => array('flex' => 1) ) )
            ->addFieldIf(  'form_handler_type', \Yii::t('forms', 'form_handler_type'), 's', 'string',
                            array( 'listColumns' => array('width' => 140) ),
                            \CurrentAdmin::isSystemMode() )
            ->addField( 'form_handler_value', $sHandlerLabel, 's', 'string',
                        array( 'listColumns' => array('width' => 240) ) )
            ->addWidget( 'form_handler_type', 'skewer\build\Component\Forms\Table', 'getTypeTitle' )

            // добавляем данные
            ->setValue( $aItems )

            // элементы управления
            ->addButton( \Yii::t('forms', 'add_new_form'), 'EditForm', 'icon-add' )
            ->addRowButtonUpdate( 'FieldList' )
            ->addRowButton( \Yii::t('adm','clone'), 'icon-clone', 'Clone', '' )
            ->addRowButtonDelete( 'delete' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

    }


    /**
     * Редактирование параметров формы
     * @param array $aSubData
     * @return int
     */
    protected function actionEditForm( $aSubData = array() ) {

        // -- обработка данных
        $aData = $this->getInData();

        if ( is_array($aSubData) && !empty($aSubData) )
            $aData = array_merge($aData, $aSubData);

        $iFormId =  isset( $aData['form_id'] ) ? (int)$aData['form_id'] : 0;
        $this->iCurrentForm = $iFormId ? $iFormId : $this->iCurrentForm;

        $oFormRow = $this->iCurrentForm ? Forms\Table::find( $this->iCurrentForm ) : Forms\Table::getNewRow();

        $sHandlerLabel = \CurrentAdmin::isSystemMode() ? \Yii::t('forms', 'form_handler_value') :
            \Yii::t('forms', 'formHandlerEmailTitle') ;

        $aFormTypeList = Forms\Table::getTypeList();
        if ( ! \CurrentAdmin::isSystemMode() && isSet($aFormTypeList['toMethod']) )
            unSet( $aFormTypeList['toMethod'] );

        // -- сборка интерфейса
        $oFormBuilder = UI\StateBuilder::newEdit();

        // добавляем поля
        $oFormBuilder
            ->addField( 'form_id', 'id', 'i', 'hide' )
            ->addField( 'form_title', \Yii::t('forms', 'form_title'), 's', 'string' )
            ->addFieldIf( 'form_name', \Yii::t('forms', 'form_name'), 's', 'string', array(),
                            \CurrentAdmin::isSystemMode() )
            ->addSelectField( 'form_handler_type', \Yii::t('forms', 'form_handler_type'), 's', $aFormTypeList )
            ->addField( 'form_handler_value', $sHandlerLabel, 's', 'string', array(
                'subtext' => sprintf( '%s <b>%s</b>',
                    \Yii::t('forms', 'form_handler_value_default' ),
                    \Site::getAdminEmail() )
                ) )

            ->addField( 'form_captcha', \Yii::t('forms', 'form_captcha'), 'i', 'check', array( 'margin'=>'15 5 0 0' ) );

        if (\CurrentAdmin::isSystemMode())
            $oFormBuilder->addSelectField( 'form_target', \Yii::t('forms', 'form_target'), 's',ReachGoal\Target::getAllAsSimpleArray(true) );

        $oFormBuilder
            ->addField( 'form_redirect', \Yii::t('forms', 'form_redirect'), 's', 'string', array(
                'subtext' => \Yii::t('forms', 'form_redirect_default') ) )

            ->addField( 'form_send_crm', \Yii::t('forms', 'form_send_crm'), 'i', 'check')
            // устанавливаем значения
            ->setValue( $oFormRow )

            // добавляем элементы управления
            ->addButton( \Yii::t('adm','save'), 'save', 'icon-save', 'save' )
            ->addButton( \Yii::t('adm','back'), $this->iCurrentForm ? 'FieldList' : 'Init', 'icon-cancel' )
        ;

        if ( $this->iCurrentForm ) {
            if( \CurrentAdmin::isSystemMode() )
                $oFormBuilder
                    ->addBtnSeparator( '->' )
                    ->addButton( \Yii::t('adm','del'), 'delete', 'icon-delete' );
        }

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }


    /**
     * Сохранение параметров формы
     */
    protected function actionSave() {

        try {

            $aData = $this->getInData();

            $oFormRow = Forms\Table::getById($this->iCurrentForm);
            if ( !$oFormRow )
                $oFormRow = Forms\Table::getNewRow();
            $oFormRow->setData( $aData );

            $oFormRow->save();

            if ( $oFormRow->getError() )
                throw new \Exception( $oFormRow->getError() );

            $this->iCurrentForm = $oFormRow->getId();

            $this->addMessage( \Yii::t('forms', 'save_data') );
            $this->actionFieldList();

        } catch ( \Exception $e ) {
            $this->addError( $e->getMessage() );
        }

    }


    /**
     * Удаление формы
     */
    protected function actionDelete() {

        try {

            $aData = $this->getInData();

            $iFormId = isSet( $aData['form_id'] ) ? $aData['form_id'] : false;

            if ( !$iFormId )
                throw new \Exception("not found form id=$iFormId");

            $oFormRow = Forms\Table::find( $iFormId );

            if ( !$oFormRow )
                throw new \Exception("not found form id=$iFormId");

            $oFormRow->delete();

            $this->actionList();

        } catch ( \Exception $e ) {

            $this->addError( $e->getMessage() );
        }
    }


    /**
     * Клонирование формы
     */
    protected function actionClone() {

        $aData = $this->getInData();

        $iFormId = isSet( $aData['form_id'] ) ? (int)$aData['form_id'] : 0;

        if ( $iFormId ) {

            $oForm = Forms\Table::getById( $iFormId );

            $oForm->form_id = 0;
            $oForm->form_title .= ' (clone)';

            $oForm->setUniqName();

            $iNewForm = $oForm->save();

            $aFields = Forms\FieldTable::find()
                ->where( 'form_id', $iFormId )
                ->getAll();

            /** @var Forms\FieldRow $oFieldRow */
            foreach ( $aFields as $oFieldRow ) {

                $oFieldRow->param_id = 0;
                $oFieldRow->form_id = $iNewForm;

                $oFieldRow->save();
            }

            $oForm->save();

            $this->addMessage( \Yii::t('forms', 'from_cloned' ) );

        } else {

            $this->addError( 'From not found.' );
        }

        $this->actionList();
    }


    protected function actionFieldList() {

        // -- обработка данных
        $aData = $this->getInData();

        $iFormId = isset( $aData['form_id'] ) ? (int)$aData['form_id'] : 0;
        $this->iCurrentForm = $iFormId ? $iFormId : $this->iCurrentForm;

        /** @var Forms\Row $oFormRow */
        $oFormRow = Forms\Table::find( $this->iCurrentForm );

        $aItems = Forms\FieldTable::find()
            ->where( 'form_id', $this->iCurrentForm )
            ->order( 'param_priority' )
            ->getAll();

        $sHeadText = sprintf(
            '%s "%s"',
            \Yii::t('forms', 'field_list'),
            $this->iCurrentForm ? $oFormRow->form_title : \Yii::t('forms', 'new_form')
        );

        $this->setPanelName( $sHeadText );


        // -- построение интерфейсов
        $oFormBuilder = UI\StateBuilder::newList();

        // добавляем поля
        $oFormBuilder
            ->addField( 'param_title', \Yii::t('forms', 'param_title'), 's', 'string',
                        array( 'listColumns' => array('flex' => 1) ) )
            ->addField( 'param_name', \Yii::t('forms', 'param_name'), 's', 'string',
                        array( 'listColumns' => array('flex' => 1) ) )
            ->addField( 'param_type', \Yii::t('forms', 'param_type'), 's', 'string',
                        array( 'listColumns' => array('flex' => 1) ) )
            ->addField( 'param_required', \Yii::t('forms', 'param_required'), 'i', 'check',
                        array('listColumns' => array('width' => 140) ) )
            ->addWidget( 'param_type', 'skewer\\build\\Component\\Forms\\FieldTable', 'getTypeTitle' )
            //->setEditableFields( array( 'form_active' ), 'save' )
            ->enableDragAndDrop( 'sortFieldList' )

            // добавляем данные
            ->setValue( $aItems )

            // элементы управления
            ->addRowButtonUpdate( 'editField' )
            ->addRowButtonDelete( 'deleteField' )
            ->addButton( \Yii::t('adm','add'), 'EditField', 'icon-add' )
            ->addButton( \Yii::t('adm','back'), 'list', 'icon-cancel' )
            ->addBtnSeparator()
            ->addButton( \Yii::t('forms', 'from_settings'), 'EditForm', 'icon-edit' )

            ->addButtonIf( Site\Type::hasCatalogModule(), \Yii::t('forms', 'elConnectText'), 'linkList', 'icon-link' )

            ->addBtnSeparator()
            ->addButton( \Yii::t('forms', 'answerDetailText'), 'answer', 'icon-edit' )
            ->addButton( \Yii::t('forms', 'license'), 'agreed', 'icon-edit' )
        ;
        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

    }


    protected function actionSortFieldList() {

        $aData = $this->getInData();
        $aDropData = $this->get( 'dropData' );
        $sPosition = $this->get( 'position' );

        if( !isSet($aData['param_id']) || !$aData['param_id'] ||
            !isSet($aDropData['param_id']) || !$aDropData['param_id'] || !$sPosition )
            $this->addError( \Yii::t('forms', 'sortParamError') );

        if( ! Forms\FieldTable::sort( $aData['param_id'], $aDropData['param_id'], $sPosition ) )
            $this->addError( \Yii::t('forms', 'sortParamError') );

    }


    /**
     * Отображение формы
     */
    protected function actionEditField() {

        // -- обработка данных
        $aData = $this->getInData();
        $iFieldId = isset( $aData['param_id'] ) ? (int)$aData['param_id'] : 0;

        $oFieldRow = $iFieldId ? Forms\FieldTable::find( $iFieldId ) : Forms\FieldTable::getNewRow();

        if ( !$oFieldRow )
            throw new \Exception( \Yii::t('forms', 'field_not_found') );


        // -- сборка интерфейса
        $oFormBuilder = UI\StateBuilder::newEdit();

        $this->setPanelName( \Yii::t('forms', 'edit_field') );

        // добавляем поля
        $oFormBuilder
            ->addField( 'param_id', 'id', 'i', 'hide' )
            ->addField( 'param_title', \Yii::t('forms', 'param_title'), 's', 'string' )
            ->addField( 'param_name', \Yii::t('forms', 'param_name'), 's', 'string' )
            ->addField( 'param_description', \Yii::t('forms', 'param_description'), 's', 'text',
                array( 'labelAlign' => 'left' ) )
            ->addSelectField( 'param_type', \Yii::t('forms', 'param_type'), 's',
                Forms\FieldTable::getTypeList() )
            ->addField( 'param_required', \Yii::t('forms', 'param_required'), 'i', 'check' )
            ->addField( 'param_default', \Yii::t('forms', 'param_default'), 's', 'text', array(
                'labelAlign' => 'left',
                'subtext' => \Yii::t('forms', 'defaultDesc') ) )
            ->addField( 'param_maxlength', \Yii::t('forms', 'param_maxlength'), 's', 'string',
                array(
                    'labelAlign' => 'left',
                    'subtext' => sprintf(\Yii::t('forms', 'maxlength_desc'), Forms\FieldTable::getUploadMaxSize()) ))

            ->addSelectField( 'param_validation_type', \Yii::t('forms', 'param_validation_type'), 's',
                Forms\FieldTable::getValidatorList() )
        ;

        if ( \CurrentAdmin::isSystemMode() )
        $oFormBuilder
            ->addSelectField( 'label_position', \Yii::t('forms', 'label_position'), 's',
                Forms\FieldTable::getLabelPositionList(), array( 'margin'=>'15 5 0 0' ) )
            ->addField( 'new_line', \Yii::t('forms', 'new_line'), 'i', 'check' )
            ->addSelectField( 'width_factor', \Yii::t('forms', 'width_factor'), 's',
                Forms\FieldTable::getWidthFactorList() )
            ->addField( 'param_man_params', \Yii::t('forms', 'param_man_params'), 's', 'string', array(
                'subtext' => \Yii::t('forms', 'manParamsDesc')
            ) )
        ;

        // устанавливаем значения
        $oFormBuilder->setValue( $oFieldRow );

        // добавляем элементы управления
        $oFormBuilder->addButton( \Yii::t('adm','save'), 'SaveField', 'icon-save', 'save' );
        $oFormBuilder->addButton( \Yii::t('adm','back'), 'FieldList', 'icon-cancel' );

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }


    /**
     * Сохранение
     */
    protected function actionSaveField() {

        try {

            $aData = $this->getInData();

            $iFieldId = isset( $aData['param_id'] ) ? (int)$aData['param_id'] : 0;

            $oFieldRow = $iFieldId ? Forms\FieldTable::find( $iFieldId ) : Forms\FieldTable::getNewRow();
            $oFieldRow->setData( $aData );
            $oFieldRow->form_id = $this->iCurrentForm;

            $oFieldRow->save();

            if ( $oFieldRow->getError() )
                throw new \Exception( $oFieldRow->getError() );

            /** @var Forms\Row $oFormRow */
            $oFormRow =  Forms\Table::find( $this->iCurrentForm );
            if ( $oFormRow->form_handler_type == 'toBase' )
                $oFormRow->preSave();

            $this->actionFieldList();

        } catch ( \Exception $e ) {
            $this->addError( $e->getMessage() );
        }

    }


    /**
     * Удаляет запись
     */
    protected function actionDeleteField() {

        try {

            $aData = $this->getInData();

            $aData['form_id'] = $this->iCurrentForm;

            $iFieldId = iSset( $aData['param_id'] ) ? $aData['param_id'] : false;

            if ( !$iFieldId )
                throw new \Exception( "not found form field id=$iFieldId" );

            $oFieldRow = Forms\FieldTable::find( $iFieldId );

            if ( !$oFieldRow )
                throw new \Exception( "not found form field id=$iFieldId" );

            $oFieldRow->delete();

            $this->actionFieldList();

        } catch ( \Exception $e ) {

            $this->addError( $e->getMessage() );
        }

    }


    protected function actionAnswer() {

        $sInfoText = \Yii::t('forms', 'head_mail_text', [\Yii::t('app', 'site_label'), \Yii::t('app', 'url_label')]);
        /** @var Forms\Row $oFormRow */
        $oFormRow = Forms\Table::find( $this->iCurrentForm );

        $aAddParams = Query::SelectFrom( 'forms_add_data' )->where( 'form_id', $this->iCurrentForm )->asArray()->getOne();

        $aValues = array(
            'form_answer' => $oFormRow->form_answer,
            'answer_title' => $aAddParams['answer_title'],
            'answer_body' => $aAddParams['answer_body']
        );

        // --
        $oFormBuilder = UI\StateBuilder::newEdit();

        $this->setPanelName( sprintf(
            '%s "%s"',
            \Yii::t('forms', 'answer_head'),
            $oFormRow->form_title
        ));

        // добавляем поля
        $oFormBuilder
            ->addField( 'form_answer', \Yii::t('forms', 'form_answer'), 'i', 'check' )
            ->addField( 'answer_title', \Yii::t('forms', 'answer_title'), 's', 'str' )
            ->addField( 'answer_body', \Yii::t('forms', 'answer_body'), 's', 'wyswyg' )
            ->addFieldWithValue( 'info', \Yii::t('forms', 'replace_label'), 's', 'show', $sInfoText )
        ;

        // устанавливаем значения
        $oFormBuilder->setValue( $aValues );

        // добавляем элементы управления
        $oFormBuilder->addButton( \Yii::t('adm','save'), 'answerSave', 'icon-save', 'save' );
        $oFormBuilder->addButton( \Yii::t('adm','back'), 'FieldList', 'icon-cancel' );

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;

    }


    protected function actionAnswerSave() {

        // запросить данные
        $aData = $this->getInData();

        if ( !$this->iCurrentForm )
            throw new \Exception('не выбрана форма');

        if ( !isSet($aData['form_answer']) OR !isSet($aData['answer_title']) OR !isSet($aData['answer_body']) )
            throw new \Exception('не пришли данные');

        Query::InsertInto( 'forms_add_data' )
            ->set( 'form_id', $this->iCurrentForm )
            ->set( 'answer_title', $aData['answer_title'] )
            ->set( 'answer_body', $aData['answer_body'] )
            ->onDuplicateKeyUpdate()
            ->set( 'answer_title', $aData['answer_title'] )
            ->set( 'answer_body', $aData['answer_body'] )
            ->get();

        /** @var Forms\Row $oFormRow */
        $oFormRow = Forms\Table::find( $this->iCurrentForm );
        $oFormRow->form_answer = $aData['form_answer'];
        $oFormRow->save();

        // вывод списка
        $this->actionFieldList();
    }


    /**
     * Форма редактирования лицензионного соглашения
     */
    protected function actionAgreed() {

        $aAddParams = Query::SelectFrom( 'forms_add_data' )->where( 'form_id', $this->iCurrentForm )->asArray()->getOne();

        /** @var Forms\Row $oFormRow */
        $oFormRow = Forms\Table::find( $this->iCurrentForm );

        $aValues = array(
            'form_agreed' => $oFormRow->form_agreed,
            'agreed_title' => $aAddParams['agreed_title'],
            'agreed_text' => $aAddParams['agreed_text']
        );

            // --
        $oFormBuilder = UI\StateBuilder::newEdit();

        $this->setPanelName( sprintf(
            '%s "%s"',
            \Yii::t('forms', 'agreed_head'),
            $oFormRow->form_title
        ));

        // добавляем поля
        $oFormBuilder
            ->addField( 'form_agreed', \Yii::t('forms', 'form_agreed'), 'i', 'check' )
            ->addField( 'agreed_title', \Yii::t('forms', 'field_agreed_title'), 's', 'str' )
            ->addField( 'agreed_text', \Yii::t('forms', 'field_agreed_text'), 's', 'html' )

            // устанавливаем значения
            ->setValue( $aValues )

            // добавляем элементы управления
            ->addButton( \Yii::t('adm','save'), 'agreedSave', 'icon-save', 'save' )
            ->addButton( \Yii::t('adm','back'), 'FieldList', 'icon-cancel' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }

    /**
     * Сохранение лицензионного соглашения
     * @throws \Exception
     * @return void
     */
    protected function actionAgreedSave() {

        // запросить данные
        $aData = $this->getInData();

        if ( !$this->iCurrentForm )
            throw new \Exception('не выбрана форма');

        if ( !isSet($aData['form_agreed']) OR !isSet($aData['agreed_title']) OR !isSet($aData['agreed_text']) )
            throw new \Exception('не пришли данные');

        Query::InsertInto( 'forms_add_data' )
            ->set( 'form_id', $this->iCurrentForm )
            ->set( 'agreed_title', $aData['agreed_title'] )
            ->set( 'agreed_text', $aData['agreed_text'] )
            ->onDuplicateKeyUpdate()
            ->set( 'agreed_title', $aData['agreed_title'] )
            ->set( 'agreed_text', $aData['agreed_text'] )
            ->get();

        /** @var Forms\Row $oFormRow */
        $oFormRow = Forms\Table::find( $this->iCurrentForm );
        $oFormRow->form_agreed = $aData['form_agreed'];
        $oFormRow->save();

        // вывод списка
        $this->actionFieldList();
    }


    /**
     * Список полей формы связанных с каталогом
     */
    protected function actionLinkList() {

        // -- обработка данных
        if ( !$this->iCurrentForm )
            throw new \Exception('Не найдена форма!');

        $aItems = Query::SelectFrom( 'forms_links' )->where( 'form_id', $this->iCurrentForm )->getAll();


        // -- сборка интерфейса
        $oFormBuilder = UI\StateBuilder::newList();

        // добавляем поля
        $oFormBuilder
            ->addField( 'form_field', \Yii::t('forms', 'form_field'), 's', 'string',
                array( 'listColumns' => array('flex' => 1) ) )
            ->addField( 'card_field', \Yii::t('forms', 'card_field'), 's', 'string',
                array( 'listColumns' => array('flex' => 1) ) )

            // добавляем данные
            ->setValue( $aItems )

            // элементы управления
            ->addButton( \Yii::t('adm','add'), 'addLink', 'icon-add' )
            ->addButton( \Yii::t('adm','back'), 'FieldList', 'icon-cancel' )
            ->addRowButtonDelete( 'delLink' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

    }


    /**
     * Форма добавление связи
     * @throws \Exception
     * @return int
     */
    protected function actionAddLink() {

        // поля формы
        if ( !$this->iCurrentForm )
            throw new \Exception('Не найдена форма!');

        $aRows = Forms\FieldTable::find()->where( 'form_id', $this->iCurrentForm )->getAll();
        $aFormFields = array();
        foreach ( $aRows as $oRow )
            $aFormFields[$oRow->param_name] = $oRow->param_title;

        // поля карточки товара
        $oCard = Catalog\Card::get( Catalog\Card::DEF_BASE_CARD );
        $aRows = $oCard->getFields();
        $aCardFields = array( 'id' => 'id' );
        if ( $aRows )
            foreach ( $aRows as $oRow )
                $aCardFields[$oRow->name] = $oRow->title;


        // --
        $oFormBuilder = UI\StateBuilder::newEdit();

        // добавляем поля
        $oFormBuilder
            ->addSelectField( 'form_field', \Yii::t('forms', 'form_field'), 's', $aFormFields )
            ->addSelectField( 'card_field', \Yii::t('forms', 'card_field'), 's', $aCardFields )

            // устанавливаем значения
            ->setValue( array() )

            // добавляем элементы управления
            ->addButton( \Yii::t('adm','save'), 'saveLink', 'icon-save', 'save' )
            ->addButton( \Yii::t('adm','back'), 'LinkList', 'icon-cancel' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }


    protected function actionSaveLink() {

        $aData = $this->getInData();

        if ( !$this->iCurrentForm )
            throw new \Exception(\Yii::t('forms', 'error_form_not_found'));

        if ( !isSet($aData['form_field']) OR !isSet($aData['card_field']) )
            throw new \Exception(\Yii::t('forms', 'error_form_not_found'));

        Query::InsertInto( 'forms_links' )
            ->set( 'form_id', $this->iCurrentForm )
            ->set( 'form_field', $aData['form_field'] )
            ->set( 'card_field', $aData['card_field'] )
            ->get();

        $this->actionLinkList();
    }


    protected function actionDelLink() {

        $aData = $this->getInData();

        if ( !$this->iCurrentForm )
            throw new \Exception(\Yii::t('forms', 'error_form_not_found'));

        if ( !isSet($aData['link_id']) OR !$aData['link_id'] )
            throw new \Exception(\Yii::t('forms', 'error_form_not_found'));

        Query::DeleteFrom( 'forms_links' )
            ->where( 'link_id', $aData['link_id']  )
            ->where( 'form_id', $this->iCurrentForm )
            ->get();

        $this->actionLinkList();
    }


    /**
     * Установка служебных данных
     * @param UI\State\BaseInterface $oIface
     */
    protected function setServiceData( UI\State\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData( array(
            'section' => $this->iSectionId,
            'form_id' => $this->iCurrentForm,
            'enableSettings' => $this->enableSettings
        ) );

    }

} //class