<?php

namespace skewer\build\Tool\TasksManager;

use skewer\build\Component\UI\StateBuilder;
use skewer\build\Tool;
use skewer\build\Component\QueueManager;

/**
 * Интерфейс для работы с планировщиком задач
 */
class Module extends Tool\LeftList\ModulePrototype {

    protected function preExecute(){

    }

    /**
     * Первичное состояние
     */
    protected function actionInit() {

        // вывод списка
        $this->actionList();

    }

    /**
     * Список пользователей
     */
    protected function actionList() {

        // установка заголовка
        $this->setPanelName( \Yii::t('tasks', 'tasks') );

        // объект для построения списка
        $oList = StateBuilder::newList();

        $oList
            ->addField( 'id', \Yii::t('tasks', 'id'), 'i', 'int')
            ->addField( 'global_id', \Yii::t('tasks', 'global_id'), 'i', 'int')
            ->addField( 'title', \Yii::t('tasks', 'title'), 's', 'string', ['listColumns' => ['flex' => 3]])
/*            ->addField( 'class', \Yii::t('tasks', 'class'), 's', 'string')
            ->addField( 'parameters', \Yii::t('tasks', 'parameters'), 's', 'string')*/
            ->addField( 'priority', \Yii::t('tasks', 'priority'), 's', 'string')
            ->addField( 'resource_use', \Yii::t('tasks', 'resource_use'), 's', 'string')
            ->addField( 'upd_time', \Yii::t('tasks', 'upd_time'), 's', 'string', ['listColumns' => ['flex' => 1]])
            ->addField( 'status', \Yii::t('tasks', 'status'), 's', 'string')
        ;

        $oList->buttonConfirm(\Yii::t('adm','clear'), 'clear', 'icon-stop', \Yii::t('tasks', 'clear_confirm'));

        $aItems = Api::getListItems();

        $oList->setValue( $aItems );

        // вывод данных в интерфейс
        $this->setInterface( $oList->getForm() );

    }

    /**
     * Удалеение х задач из списка
     */
    protected function actionClear() {
        QueueManager\ar\Task::delete()->get();
        $this->actionList();
    }

}
