<?php

namespace skewer\build\Tool\TasksManager;


use skewer\build\Component\QueueManager;

/**
 * API работы с менеджером процессов
 */
class Api {

    /**
     * Список задач
     * @return mixed
     */
    public static function getListItems(){

        $aItems = QueueManager\ar\Task::find()->order('upd_time', 'DESC')->asArray()->getAll();

        $aPriority = QueueManager\Api::getPriorityList();
        $aResourceUse = QueueManager\Api::getResourceUseList();
        $aStatus = QueueManager\Api::getStatusList();

        /** Если закрыта - пишем выполнена */
        $aStatus[QueueManager\Task::stClose] = \Yii::t( 'tasks', 'status_complete');

        foreach( $aItems as &$aItem ){
            $aItem['priority'] = (isset($aPriority[$aItem['priority']]))?$aPriority[$aItem['priority']]:'';
            $aItem['resource_use'] = (isset($aResourceUse[$aItem['resource_use']]))?$aResourceUse[$aItem['resource_use']]:'';
            $aItem['status'] = (isset($aStatus[$aItem['status']]))?$aStatus[$aItem['status']]:'';
        }

        return $aItems;
    }



}
