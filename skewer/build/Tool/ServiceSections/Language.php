<?php

$aLanguage = array();

$aLanguage['ru']['ServiceSections.Tool.tab_name'] = 'Системные разделы';

$aLanguage['ru']['section_name'] = 'Имя';
$aLanguage['ru']['section_title'] = 'Название';
$aLanguage['ru']['section_value'] = 'Раздел';
$aLanguage['ru']['section_language'] = 'Язык';
$aLanguage['ru']['section_lang_filter'] = 'Язык';

$aLanguage['en']['ServiceSections.Tool.tab_name'] = 'Service sections';

$aLanguage['en']['section_name'] = 'Name';
$aLanguage['en']['section_title'] = 'Title';
$aLanguage['en']['section_value'] = 'Section';
$aLanguage['en']['section_language'] = 'Language';
$aLanguage['en']['section_lang_filter'] = 'Language';

return $aLanguage;