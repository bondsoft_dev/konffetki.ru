<?php

use skewer\build\Tool\LeftList;


$aConfig['name']     = 'ServiceSections';
$aConfig['title']    = 'Системные разделы';
$aConfig['version']  = '1.000';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['useNamespace'] = true;
$aConfig['layer']     = \Layer::TOOL;
$aConfig['group']     = LeftList\Group::LANGUAGE;
$aConfig['languageCategory']     = 'languages';


return $aConfig;