<?php

namespace skewer\build\Tool\ServiceSections;


use skewer\build\Component\I18N\Languages;
use skewer\build\Component\I18N\models\ServiceSections;
use skewer\build\Component\UI\StateBuilder;
use skewer\build\Tool;
use yii\helpers\ArrayHelper;

/**
 * Модуль для редактирования системных разделов
 * Class Module
 * @package skewer\build\Tool\ServiceSections
 */
class Module extends Tool\LeftList\ModulePrototype {

    protected $lang_filter = false;

    protected function preExecute() {
        $this->lang_filter = $this->get('lang_filter', false);
    }


    public function actionInit(){

        $this->actionList();

    }

    public function actionList(){

        $oList = StateBuilder::newList();

        $oList
            ->fieldHide('id', 'ID')
            ->fieldString('name', \yii::t('languages', 'section_name'), ['listColumns' => ['flex' => 3]])
            ->fieldString('title', \yii::t('languages', 'section_title'), ['listColumns' => ['flex' => 5]])
            ->fieldNumber('value', \yii::t('languages', 'section_value'))
            ->fieldString('language', \yii::t('languages', 'section_language'))
            ;

        $aLanguages = Languages::getAllActive();
        if (count($aLanguages) > 1){
            $oList->addFilterSelect('lang_filter', ArrayHelper::map($aLanguages, 'name', 'title'), $this->lang_filter, \yii::t('languages', 'section_lang_filter'));
        }

        $oQuery = ServiceSections::find()
            ->asArray()
            ->orderBy(['language' => SORT_ASC]);

        if ($this->lang_filter)
            $oQuery->where(['language' => $this->lang_filter]);

        $oList->setValue( $oQuery->all() );

        $oList->setEditableFields(['value'], 'save');

        $this->setInterface( $oList->getForm() );

    }

    public function actionSave(){

        $aData = $this->getInData();

        if (isset($aData['id'])){
            /** @var ServiceSections $oSection */
            $oSection = ServiceSections::findOne(['id' => $aData['id']]);

            if (!is_null($oSection)){
                $oSection->value = (isset($aData['value']))?(int)$aData['value']:0;
                $oSection->save();
            }
        }

        $this->actionList();

    }

}