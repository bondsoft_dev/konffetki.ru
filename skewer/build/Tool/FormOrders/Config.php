<?php

use skewer\build\Tool\LeftList;

/* main */
$aConfig['name']     = 'FormOrders';
$aConfig['title']    = 'Формы: заказы';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Админ-интерфейс управления заказами, поступившими из форм';
$aConfig['revision'] = '0001';
$aConfig['useNamespace'] = true;
$aConfig['layer']     = Layer::TOOL;
$aConfig['group']     = LeftList\Group::CONTENT;
$aConfig['languageCategory']     = 'forms';

return $aConfig;
