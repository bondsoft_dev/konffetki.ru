<?php

namespace skewer\build\Tool\FormOrders;


use skewer\build\Component\Forms;
use skewer\build\Component\orm\Query;
use skewer\build\Component\UI;
use skewer\build\Adm;
use skewer\build\Tool;
use skewer\build\Adm\Tree;



class Module extends Tool\LeftList\ModulePrototype implements Tree\ModuleInterface {

    public $iCurrentForm = 0;

    /** @var int id раздела */
    protected $pageId = 0;

    /** @var int id формы */
    protected $formId = 0;

    function getPageId() {
        return $this->pageId;
    }

    /**
     * Сообщает используется ли только одна форма для отображения
     * @return bool
     */
    private function useOneForm() {
        return (bool)$this->formId;
    }

    protected function preExecute() {

        $this->iCurrentForm = $this->getInt('form_id');
        if ( !$this->iCurrentForm )
            $this->iCurrentForm = $this->getEnvParam('form_id');
    }


    protected function setServiceData( UI\State\BaseInterface $oIface ) {

        $oIface->setServiceData( array(
            'form_id' => $this->iCurrentForm,
        ) );
    }


    protected function actionInit() {

        if ( $this->useOneForm() ) {
            $this->iCurrentForm = $this->formId;
            $this->actionList();
        } else {
            $this->actionShowForms();
        }

    }

    /**
     * Список форм с заказами
     */
    protected function actionShowForms() {

        // список форм с сохранением в базу
        $aItems = Forms\Table::find()->where( 'form_handler_type', 'toBase' )->getAll();

        $this->iCurrentForm = 0;


        $oFormBuilder = new UI\StateBuilder();

        $oFormBuilder->createFormList();

        $this->setPanelName( \Yii::t( 'forms', 'form_list') );

        // добавляем поля
        $oFormBuilder
            ->addField( 'form_title',  \Yii::t( 'forms', 'form_title'), 's', 'string',
                array( 'listColumns' => array('flex' => 1) ) )
            ->setFields()
            ->addWidget( 'form_handler_type', 'skewer\\build\\Component\\Forms\\Table', 'getTypeTitle' )
        ;

        // добавляем данные
        $oFormBuilder->setValue( $aItems );

        // элементы управления
        $oFormBuilder
            ->addRowButtonUpdate( 'List' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Список заказов
     */
    protected function actionList() {

        // -- input data
        $aData = $this->getInData();
        $this->iCurrentForm = isSet( $aData['form_id'] ) ? $aData['form_id'] : $this->iCurrentForm;
        if ( !$this->iCurrentForm )
           throw new \Exception( 'form not found' );

        /** @var Forms\Row $oFormRow */
        $oFormRow = Forms\Table::find($this->iCurrentForm);

        $sErrorMsg = "";
        if (!$oFormRow){
            $sErrorMsg = \Yii::t('forms', "not_found");
        }

        if ($oFormRow->form_handler_type != "toBase"){
            $sErrorMsg = \Yii::t('forms', "bad_status");
        }

        if ($sErrorMsg){
            $oForm = new \ExtForm();
            $oForm->setTitle("");
            $oForm->setAddText($sErrorMsg);
            $this->setInterface($oForm);
            return;
        }

        $aItems = Query::SelectFrom( 'frm_' . $oFormRow->form_name )->getAll();
        $aFieldList = $oFormRow->getFields();


        // -- build form list
        $oFormBuilder = new UI\StateBuilder();

        $oFormBuilder->createFormList();

        $this->setPanelName(  \Yii::t( 'forms', 'form_list') );

        // добавляем поля
        $oFormBuilder
            ->addField( 'id', 'id', 's', 'string' );

        $iFieldCounter = 0;
        foreach ( $aFieldList as $oFieldRow ) {

            $iFieldCounter++;
            if ( $iFieldCounter > 3 ) break;

            $oFormBuilder->addField( $oFieldRow->param_name, $oFieldRow->param_title, 's', 'string',
                array( 'listColumns' => array('flex' => 1) ) );
        }

        $oFormBuilder
            ->addField( '__add_date',  \Yii::t('forms', 'add_date'), 's', 'string' )
            ->addField( '__status',  \Yii::t('forms', 'status'), 's', 'string' )
            ->addWidget( '__status', 'skewer\\build\\Tool\\FormOrders\\Api', 'getWidget4Status' )
        ;

        // добавляем данные
        $oFormBuilder->setFields()->setValue( $aItems );

        // элементы управления
        $oFormBuilder
            ->addRowButtonUpdate( 'Edit' )
            ->addRowButtonDelete( 'Delete' )
            ->addButton( \Yii::t('adm','add'), 'edit', 'icon-add' );

        if (!$this->useOneForm()) $oFormBuilder->addButton( \Yii::t('adm','back'), 'Init', 'icon-cancel' );


        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }



    protected function actionEdit() {

        // -- обработка данных
        $aData = $this->getInData();
        $iItemId = isSet( $aData['id'] ) ? $aData['id'] : 0;
        if ( !$this->iCurrentForm )
            throw new \Exception( 'item not found' );

        /** @var Forms\Row $oFormRow */
        $oFormRow = Forms\Table::find( $this->iCurrentForm );
        $aFieldList = $oFormRow->getFields();

        if ( $iItemId )
            $aItems = Query::SelectFrom( 'frm_' . $oFormRow->form_name )->where( 'id', $iItemId )->getOne();
        else
            $aItems = array();


        // -- сборка интерфейса
        $oFormBuilder = new UI\StateBuilder();

        // создаем форму
        $oFormBuilder->clear()->createFormEdit();//->addHeadText( $sHeadText );

        // добавляем поля
        $oFormBuilder
            ->addField( 'id', 'id', 'i', 'hide' );


        foreach ( $aFieldList as $oFieldRow ) {

            $sCurType = $oFieldRow->getType4ExtJS();
            switch ( $sCurType ) {
                case 'select':
                    $oFormBuilder->addSelectField( $oFieldRow->param_name, $oFieldRow->param_title, 's', $oFieldRow->parseDefaultAsList() );
                    break;
                case 'radio':
                    $oFormBuilder->addSelectField( $oFieldRow->param_name, $oFieldRow->param_title, 's', $oFieldRow->parseDefaultAsList() );
                    break;
                default:
                    $oFormBuilder->addField( $oFieldRow->param_name, $oFieldRow->param_title, 's', $sCurType);
            }
        }

        $oFormBuilder->addField( '__add_date', \Yii::t('forms', 'add_date'), 's', 'string', array( 'disabled' => true ) );
        $oFormBuilder->addField( '__section', \Yii::t('forms', 'section'), 's', 'string', array( 'disabled' => true ) );
        $oFormBuilder->addSelectField( '__status', \Yii::t('forms', 'status'), 's', Api::getStatusList() );

        // устанавливаем значения
        $oFormBuilder->setFields()->setValue( $aItems );

        // добавляем элементы управления
        $oFormBuilder->addButton( \Yii::t('adm','save'), 'save', 'icon-save', 'save' );
        $oFormBuilder->addButton( \Yii::t('adm','back'), 'list', 'icon-cancel' );

        if ( $this->iCurrentForm ) {
            $oFormBuilder
                ->addBtnSeparator( '->' )
                ->addButton( \Yii::t('adm','del'), 'delete', 'icon-delete' );
        }

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }


    protected function actionDelete() {

        // -- обработка данных
        $aData = $this->getInData();
        $iItemId = isSet( $aData['id'] ) ? $aData['id'] : 0;
        if ( !$iItemId || !$this->iCurrentForm )
            throw new \Exception( 'item not found' );

        /** @var Forms\Row $oFormRow */
        $oFormRow = Forms\Table::find( $this->iCurrentForm );

        Query::DeleteFrom( 'frm_' . $oFormRow->form_name )->where( 'id', $iItemId )->get();

        // вывод списка
        $this->actionList();
    }


    /**
     * Сохранение заказы
     */
    protected function actionSave() {

        // -- обработка данных
        $aData = $this->getInData();
        $iItemId = isSet( $aData['id'] ) ? $aData['id'] : 0;
        if ( !$this->iCurrentForm )
            throw new \Exception( 'item not found' );

        /** @var Forms\Row $oFormRow */
        $oFormRow = Forms\Table::find( $this->iCurrentForm );
        $aFields = $oFormRow->getFields();

        if ( !$iItemId )
            $oQuery = Query::InsertInto( 'frm_' . $oFormRow->form_name )
                ->set( '__status', 'new' )
                ->set( '__add_date', date('Y-m-d') );
        else
            $oQuery = Query::UpdateFrom( 'frm_' . $oFormRow->form_name )
                ->where( 'id', $iItemId )
                ->set( '__status', isSet( $aData['__status'] ) ? $aData['__status'] : 'new' );

        foreach ( $aFields as $oFieldRow ) {
            $sName = $oFieldRow->param_name;
            $oQuery->set( $sName, isSet($aData[$sName]) ? $aData[$sName] : $oFieldRow->param_default );
        }

        $oQuery->get();


        // вывод списка
        $this->actionList();

    }

}