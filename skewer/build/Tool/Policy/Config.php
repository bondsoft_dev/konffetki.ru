<?php

use skewer\build\Tool\LeftList;

$aConfig['name']     = 'Policy';
$aConfig['title']    = 'Политики доступа';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Админ-интерфейс управления политиками доступа';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::TOOL;
$aConfig['group']     = LeftList\Group::ADMIN;
$aConfig['useNamespace'] = true;
$aConfig['languageCategory']     = 'auth';

$aConfig['policy'] = array( array(
    'name'    => 'useControlPanel',
    'title'   => 'Доступ к панели управления',
    'default' => 0
), array(
    'name'    => 'useDesignMode',
    'title'   => 'Доступ в дизайнерский режим',
    'default' => 0
));

return $aConfig;
