<?php

namespace skewer\build\Tool\Policy;

use skewer\build\Component\Section\Tree;
use skewer\build\Component\UI;
use skewer\build\Tool;
use yii\base\UserException;

class Module extends Tool\LeftList\ModulePrototype {

    /* @var Api */
    private $oPolicyApi = null;

    // текущий номер страницы ( с 0, а приходит с 1 )
    protected $iPage = 0;

    /**
     * Имя панели
     * @var string
     */
    protected $sPanelName = '';

    /**
     * Метод, выполняемый перед action меодом
     * @throws UserException
     * @return bool
     */
    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt('page')-1;
        if ( $this->iPage < 0 ) $this->iPage = 0;

    }

    /**
     * Первичное состояние
     */
    protected function actionInit() {

        $this->oPolicyApi = new Api();
        // вывод списка
        $this->actionList();
    }

    /**
     * Список пользователей
     */
    protected function actionList() {

        // установка заголовка панели
        $this->setPanelName(\Yii::t('auth', 'policyList'));

        // объект для построения списка
        $oList = new \ExtList();

        /**
         * Модель данных
         */

        $aFieldFilter = $this->oPolicyApi->getListFields();
        $aListModel = $this->oPolicyApi->getParamList($aFieldFilter);
        // задать модель данных для вывода
        $oList->setFields( $aListModel );

        /**
         * Данные
         */

        // добавление набора данных
        $aFilter = array();

        //if(CurrentAdmin::isSystemMode())
        $aFilter['where_condition']['active'] = array("sign" => "!=", "value" => \Policy::stSys); // не выводим политику сисадмина в списке

        $aItems = \Policy::getPolicyList($aFilter);

        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );

        /**
         * Интерфейс
         */

        // добавление кнопок
        $oList->addRowBtnUpdate();

        // кнопка добавления
        $oList->addBtnAdd('show');

        // вывод данных в интерфейс
        $this->setInterface( $oList );
    }

    /**
     * Отображение формы
     */
    protected function actionShow() {

        // подключить автоматический генератор форм
        $oForm = new \ExtForm();

        // номер записи
        $aData = $this->get('data');
        $iItemId = (is_array($aData) && isset($aData['id'] ) && $aData['id']) ? (int)$aData['id'] : $this->getInt('id');

        // установка заголовка панели
        $this->setPanelName($iItemId?\Yii::t('auth', 'edit'):\Yii::t('auth', 'add'),true);

        // запись
        $aItem = $iItemId ? $this->oPolicyApi->getPolicy($iItemId) : $this->oPolicyApi->getBlankValues();

        if( isSet($aItem['access_level']) && $aItem['access_level'] < 2 )
            $aItem['fulladmin'] = 1;

        // установить набор элементов формы
        $aFieldFilter = $this->oPolicyApi->getDetailFields();

        // добавить поле - набор галочек функциональных политик
        $aFieldFilter['params'] = $oForm->getSpecificItemInitArray('CheckSet', Api::getFuncDataForField( $iItemId ) );

        // добавить поле - набор галочек политики модулей
        $aFieldFilter['params_module'] = $oForm->getSpecificItemInitArray('CheckSet4Module', Api::getModuleDataForField( $iItemId ) );

        /**
         * Скрываем активность для default политики
         */
        $bHide = false;
        if ($iItemId){
            $aDefaultUser = \AuthUsersMapper::getDefaultUserData();
            if (isset($aDefaultUser['group_policy_id']) && $aDefaultUser['group_policy_id'] == $iItemId){
                $bHide = true;
                if (isset($aFieldFilter['active']))
                    $aFieldFilter['active']['view'] = 'hide';
            }
        }

        $oForm->setFields( $aFieldFilter );

        // установить значения для элементов
        $oForm->setValues( $aItem );

        // добавление кнопок
        $oForm->addBtnSave();

        // при редактировании
        if ( $iItemId ) {
            // добавить кнопку редактирования разделов
            $oForm->addDockedItem(array(
                'text' => \Yii::t('auth', 'sections'),
                'iconCls' => 'icon-edit',
                'state' => '',
                'action' => 'sections',
                'addParams' => array(
                    'id' => $iItemId
                )
            ));
        }

        // кнопка отмены
        $oForm->addBtnCancel();

        // при редактировании
        if ( $iItemId && !$bHide) {
            // добавить кнопку удаления
            $oForm->addBtnSeparator('->');
            $oForm->addBtnDelete();
        }

        // вывод данных в интерфейс
        $this->setInterface( $oForm );
    }

    /**
     * Сохранение
     * @throws \Exception
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->get( 'data' );
        $bHasId = isset($aData['id']) && is_numeric($aData['id']) && $aData['id'];
        $iId = 0;

        // есть данные - сохранить
        if ( $aData ) {

            // проверка на превышение прав доступа
            if( \CurrentAdmin::isLimitedRights() || ( isSet($aData['access_level']) && $aData['access_level'] && $aData['access_level'] < \CurrentAdmin::getAccessLevel() ) )
                throw new \Exception(\Yii::t('auth', 'policy_no_rights'));

            // уточнение уровня прав доступа
            if( !$aData['access_level'] ){
                // если права доступа изначально максимальны, то в базе не меняем флаг - чтоб не испортить сисадмина
                unSet($aData['access_level']);
            } elseif( $aData['fulladmin'] == 1 ) {
                $aData['access_level'] = 1;
                // todo сделать чтоб нельзя было себе понизить и возможно проверку чтоб хоть одна политика оставалась с 2
            } else {
                $aData['access_level'] = 2;
            }

            if ( !isset($aData['area']) )
                throw new \Exception(\Yii::t('auth', 'no_area'));

            if ( !\GroupPolicyMapper::hasArea($aData['area']) )
                throw new \Exception(\Yii::t('auth', 'wrong_area'));

            // основная запись
            $iId = \Policy::update( $aData );

            // набор функциональных параметров
            $aData['id'] = $iId;

            Api::saveFuncData( $aData );
            Api::saveModuleData( $aData );
            \Policy::incPolicyVersion();
        }

        // если задан id и даные сохранены
        if ( $bHasId and $iId ) {
            // вывод списка
            $this->addMessage(\Yii::t('auth', 'policy_changed'));
            $this->addModuleNoticeReport(\Yii::t('auth', 'editing_policy'),$aData);
            $this->actionList();
        } else {
            // вывод записи на редактиорование
            $this->addMessage(\Yii::t('auth', 'policy_add'));
            $this->addModuleNoticeReport(\Yii::t('auth', 'policy_adding'),$aData);
            $this->set('id',$iId);
            $this->actionShow();
        }

    }

    /**
     * Удаляет запись
     */
    protected function actionDelete() {

        try{

            // запросить данные
            $aData = $this->get( 'data' );

            // id записи
            $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

            // проверка на возможность удаления политики
            $aGroupPolicy = \Policy::getPolicyHeader( $iItemId );
            if ( !$aGroupPolicy || !isSet($aGroupPolicy['del_block']) || $aGroupPolicy['del_block'] )
                throw new \Exception(\Yii::t('auth', 'delete_no_rights'));

            // удалять можно только дочерние политики
            if( \CurrentAdmin::isLimitedRights() || ( isSet($aData['access_level']) && $aData['access_level'] && $aData['access_level'] < \CurrentAdmin::getAccessLevel() ) )
                throw new \Exception(\Yii::t('auth', 'access_no_rights'));

            // удаление
            if( !($iRes = \Policy::delete( $iItemId )) )
                throw new \Exception(\Yii::t('auth', 'delete_error'));

            $this->addMessage(\Yii::t('auth', 'policy_deleted'));
            $this->addModuleNoticeReport(\Yii::t('auth', 'policy_deleting'),$aData);
        } catch ( \Exception $e ) {
            $this->addError($e->getMessage());
        }

        // вывод списка
        $this->actionList();

    }

    /**
     * Сохранение политики доступа по разделам
     * @throws UserException
     */
    protected function actionSaveSections(){

        // проверка на превышение прав доступа
        if( \CurrentAdmin::isLimitedRights() || ( isSet($aData['access_level']) && $aData['access_level'] && $aData['access_level'] < \CurrentAdmin::getAccessLevel() ) )
            throw new \Exception(\Yii::t('auth', 'access_no_rights'));

        // запросить идентификатор сущности
        $iItemId = $this->getInt('id');

        // запросить данные для сохранения
        $iStartSection = $this->getInt('startSection');
        $aItemsA = $this->get('itemsAllow');
        $aItemsD = $this->get('itemsDeny');

        // проверка пришедщих данных
        if ( !$iItemId )
            throw new UserException( \Yii::t('auth', 'policy_id_expected') );
        if ( !is_array($aItemsA) or !is_array($aItemsD) )
            throw new UserException( \Yii::t('auth', 'wrong_data_format') );

        // запрос данных политики
        $aData = $this->oPolicyApi->getPolicy($iItemId);
        if ( !$aData )
            throw new UserException( \Yii::t('auth', 'policy_not_found') );

        // сформировать массив для сохраненния
        $aSaveData = array(
            'id' => $iItemId,
            'read_enable' => implode(',',$aItemsA),
            'read_disable' => implode(',',$aItemsD),
            'start_section' => $iStartSection,
        );

        // сохранение
        $iRes = \Policy::update( $aSaveData );

        if ( $iRes ) {
            $this->addMessage(\Yii::t('auth', 'policy_updated'));
            $this->addModuleNoticeReport(\Yii::t('auth', 'policy_adding'),$aData);
        } else {
            $this->addMessage(\Yii::t('auth', 'changing_sections_error'));
        }

        $this->actionShow();

    }

    /**
     * Форма редактора доступа к разделам
     * @throws UserException
     */
    protected function actionSections(){

        // проверка на превышение прав доступа
        if( \CurrentAdmin::isLimitedRights() || ( isSet($aData['access_level']) && $aData['access_level'] && $aData['access_level'] < \CurrentAdmin::getAccessLevel() ) )
            throw new \Exception(\Yii::t('auth', 'access_no_rights'));

        // идентификатор политики
        $iItemId = $this->get('id');
        if ( !$iItemId )
            throw new UserException( \Yii::t('auth', 'policy_id_expected') );

        // запрос данных политики
        $aData = $this->oPolicyApi->getPolicy($iItemId);
        if ( !$aData )
            throw new UserException( \Yii::t('auth', 'policy_not_found') );

        // объект для построения списка
        $oIface = new \ExtUserFile( 'ReadSections' );
        $oIface->setModuleLangValues(array('polisyAllow','polisyNone','polisyDeny','polisyMain'));

        // заголовок панели
        $this->setPanelName( \Yii::t('auth', 'policySection').' "'.$aData['title'].'"' );

        // сборка дерева разделов
//        $oTree = new \Tree();
//        $this->setData('items',$oTree->getAllSections(0));
        $this->setData( 'items', Tree::getSectionTree(0) );

        /* Добавляем css файл для */
        $this->addCssFile('policy_tree.css');

        // разрешенные и запрещенные разделы
        $this->setData('startSection',(int)$aData['start_section']);
        $this->setData('itemsAllow',explode(',',$aData['read_enable']));
        $this->setData('itemsDeny',explode(',',$aData['read_disable']));

        // кнопки
        $oIface->addBtnSave('','saveSection',array('id'=>$iItemId));
        $oIface->addBtnCancel('show','',array('id'=>$iItemId));

        // вывод данных в интерфейс
        $this->setInterface( $oIface );

    }

    /**
     * Установка служебных данных
     * @param UI\State\BaseInterface $oIface
     */
    protected function setServiceData( UI\State\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData( array(
            'page' => $this->iPage
        ) );

    }


}
