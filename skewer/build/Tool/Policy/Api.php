<?php

namespace skewer\build\Tool\Policy;


use skewer\core\Component\Config as Config;

class Api {

    public function getListFields(){

        return array('id','title','active');
    }

    public function getDetailFields(){

        return $this->getParamList(array('id','title','alias','area','fulladmin','access_level','active'));
    }

    /**
     * Отдает набор полей с описаниями
     * @param array $aFieldFilter - массив с набором колонок
     * @return array
     */
    public function getParamList( $aFieldFilter ) {

        return \GroupPolicyMapper::getFullParamDefList( $aFieldFilter );

    }

    public static function getPolicy($iPolicyId){

        return \GroupPolicyMapper::getPolicyDetail($iPolicyId);
    }

    /**
     * Отдает шаблонный набор значений для добавления новой записи
     * @return array Политика доступа
     */
    public function getBlankValues() {
        return array(
            'title' => \Yii::t('auth', 'new_policy'),
            'publication_date' => date('d.m.Y',time()),
            'active' => 1,
            'access_level' => 2
        );
    }

    /**
     * Сохранение данных функциональной политики
     * @static
     * @param array $aData - набор данных для сохранения
     * @return bool
     */
    public static function saveFuncData( $aData ) {

        // выйти, если данных недостаточно
        if ( !$aData or !isset($aData['id']) )
            return false;

        // id политики
        $iPolicyId = (int)$aData['id'];
        if ( !$iPolicyId ) return false;

        // все значения функциональных политик
        $aLayerList = \Yii::$app->register->get( Config\Vars::POLICY );

        // перебираем все группы
        foreach ( $aLayerList as $sLayerName => $aModuleList ) {

            foreach ( $aModuleList as $sModuleName => $aModule ) {

                $oModuleConfig = \Yii::$app->register->getModuleConfig( $sModuleName, $sLayerName );

                if ( !$oModuleConfig )
                    continue;

                if ( isset($aModule['items']) ) {

                    // перебираем все элементы
                    foreach ( $aModule['items'] as $aParam ) {

                        /***************************************/

                        // сбор имен переменных
                        $sParamName = $aParam['name'];
                        $sModuleName = $oModuleConfig->getNameFull();

                        // собираем имя переменной
                        $sValName = sprintf('params.%s.%s', $oModuleConfig->getNameWithLayer(), $sParamName);

                        // если есть данные во входном массиве
                        if ( isset( $aData[$sValName] ) ) {

                            // пришедшее значение
                            $mValue = $aData[$sValName];

                            // название параметра
                            $sParamTitle = $aParam['title'];

                            // сохранить значение
                            \Policy::setGroupActionParam( $iPolicyId, $sModuleName, $sParamName, $mValue, $sParamTitle );

                            /**
                             * todo удалить данные, которых нет во входном массиве
                             * если нет во входном массиве, но есть в сохраненных данных
                             * эту ситуацию решили оставить без изменений в записях
                             * пока не устаканится структура и принцип работы
                             */

                        }

                    }

                }

            }

        }

        return true;

    }


    /**
     * Сохранение данных по модулям для политики
     * @static
     * @param array $aData - набор данных для сохранения
     * @return bool
     */
    public static function saveModuleData( $aData ) {

        // выйти, если данных недостаточно
        if ( !$aData or !isset($aData['id']) )
            return false;

        // id политики
        $iPolicyId = (int)$aData['id'];
        if ( !$iPolicyId ) return false;

        // все значения функциональных политик
        $aModuleList = \Yii::$app->register->getModuleList( \Layer::TOOL );

        foreach ( $aModuleList as $sModuleName ) {

            $oModuleConfig = \Yii::$app->register->getModuleConfig( $sModuleName, \Layer::TOOL );

            $sValName = sprintf('params_module.%s', $oModuleConfig->getName());

            if( isSet( $aData[$sValName] ) ) {

                // пришедшее значение
                $mValue = $aData[$sValName];
                $sCurName = $sModuleName . ($oModuleConfig->isUseNamespace() ? '' : 'ToolModule');

                if( $mValue ) {
                    \Policy::addModule( $iPolicyId, $sCurName, $oModuleConfig->getTitle() );
                } else {
                    \Policy::removeModule( $iPolicyId, $sCurName );
                }

            }

        }

        return true;
    }

    /**
     * Возвращает массив для инициализации спец компонента в js, отвечающего
     *      за набор параметров по модулям в рамках политики
     * @param $iPolicyId
     * @return array
     */
    public static function getModuleDataForField( $iPolicyId ) {

        // все значения функциональных политик
        $aModuleList = \Yii::$app->register->getModuleList( \Layer::TOOL );

        // запросить данные для данной политики
        $aSetData = \Policy::getGroupModuleData( $iPolicyId );

        // выходной массив
        $aOut = array(
            'name' => 'params_module',
            'title' => \Yii::t('auth', 'access_options'),
            'value' => array()
        );

        $aItems = array();
        foreach ( $aModuleList as $sModuleName ) {

            $oModuleConfig = \Yii::$app->register->getModuleConfig( $sModuleName, \Layer::TOOL );

            // модули помеченные как "системные" пропускаем
            if ( $oModuleConfig->getVal('isSystem') )
                continue;

            // сбор имен переменных
            $sCurName = $sModuleName . ($oModuleConfig->isUseNamespace() ? '' : 'ToolModule');
            if( isSet($aSetData[$sCurName]) )//$oModuleConfig->getNameFull()
                $sVal = '1';
            else
                $sVal = '';

            // добавляем параметр в список
            $aItems[] = array(
                'name' => $oModuleConfig->getName(),
                'title' => $oModuleConfig->getTitle(),
                'value' => $sVal
            );

        }


        $aOut['items'] = $aItems;

        return $aOut;

    }

    /**
     * Возвращает массив для инициализации спец компонента в js, отвечающего
     *      за набор параметров по подулям в рамках политики
     * @param $iPolicyId
     * @return array
     */
    public static function getFuncDataForField( $iPolicyId ) {

        // все значения функциональных политик
        $aAllData = \Yii::$app->register->get( Config\Vars::POLICY );

        // запросить данные для данной политики
        $aSetData = \Policy::getGroupActionData( $iPolicyId );

        // выходной массив
        $aOut = array(
            'name' => 'params',
            'title' => \Yii::t('auth', 'function_options'),
            'value' => array(
                'groups' => array()
            )
        );

        // ссылка на ветку для удобства
        $aGroupsRef = &$aOut['value']['groups'];

        // перебираем все группы
        foreach ( $aAllData as $sLayerName => $aModuleList ) {


            foreach ( $aModuleList as $sModuleName => $aModule ) {

                $oModuleConfig = \Yii::$app->register->getModuleConfig( $sModuleName, $sLayerName );

                if ( !$oModuleConfig )
                    continue;

                // новая группа
                $aNewGroup = array(
                    'name' => $sModuleName.$sLayerName,
                    'title' => $oModuleConfig->getTitle(),
                    'items' => array()
                );


                if ( isset($aModule['items']) ) {

                    // перебираем все элементы
                    foreach ( $aModule['items'] as $aParam ) {

                        $aParam['title'] =\Yii::t($oModuleConfig->getLanguageCategory(), $aParam['name']);

                        // сбор имен переменных
                        $sParamName = $aParam['name'];
                        $sModuleName = $oModuleConfig->getNameFull();

                        // запрос значения
                        if ( isset($aSetData[$sModuleName]) and
                            isset($aSetData[$sModuleName][$sParamName]) and
                                isset($aSetData[$sModuleName][$sParamName]['value'])
                        )
                        {
                            $aParam['value'] = $aSetData[$sModuleName][$sParamName]['value'];
                        } else {
                            $aParam['value'] = '';
                        }

                        // добавляем параметр в список
                        $aNewGroup['items'][] = $aParam;

                    }
                }

                // добавляем группу
                $aGroupsRef[] = $aNewGroup;

            }

        }

        return $aOut;

    }

    /**
     * Возвращает true если возможна работа с политикой $iPolicyId текущему администратору
     * @param $iPolicyId
     * @return bool
     */
    public static function hasAccessToPolicy( $iPolicyId ) {

        $aItemPolicy = \GroupPolicyMapper::getPolicyDetail($iPolicyId);

        return (isSet($aItemPolicy['access_level']) && $aItemPolicy['access_level'] >= \CurrentAdmin::getAccessLevel());
    }

}
