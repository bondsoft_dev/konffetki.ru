<?php

namespace skewer\build\Tool\Order;


use skewer\build\Component\Site;

class Install extends \skModuleInstall {

    public function init() {
        return true;
    }

    public function install() {

        \SysVar::set( 'syte_type', Site\Type::shop );
        return true;
    }// func

    public function uninstall() {

        if (Site\Type::isShop()){
            $this->fail("Нельзя удалить магазин");
        };

        return true;
    }// func

}//class
