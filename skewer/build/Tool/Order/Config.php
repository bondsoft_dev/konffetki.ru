<?php

use skewer\build\Tool\LeftList;

$aConfig['name']     = 'Order';
$aConfig['title']    = 'Заказы';
$aConfig['version']  = '1.1';
$aConfig['description']  = 'Админ-интерфейс управления модулем заказов';
$aConfig['revision'] = '0002';
$aConfig['useNamespace'] = true;
$aConfig['layer']     = \Layer::TOOL;
$aConfig['group']     = LeftList\Group::ORDER;

$aConfig['dependency'] = array(
    array('Cart', \Layer::PAGE),
    array('MiniCart', \Layer::PAGE),
    array('Auth', \Layer::PAGE),
    array('Auth', \Layer::TOOL),
    array('Auth', \Layer::ADM),
    array('Profile', \Layer::PAGE),
    array('Payments', \Layer::TOOL)
);


return $aConfig;
