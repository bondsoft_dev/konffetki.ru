<?php

namespace skewer\build\Tool\Payments;

use skewer\build\Component\UI;
use skewer\build\Tool;


/**
 * Class Module
 * @package skewer\build\Tool\Payments
 */
class Module extends Tool\LeftList\ModulePrototype  {

    /** @var string Заглушка для пароля */
    private $pwdFake = '--------';

    protected function actionInit(){
        $this->actionList();
    }

    protected function actionList() {

        $oList = UI\StateBuilder::newList();

        $oList
            ->addField( 'title', \Yii::t('payments', 'title'), 's', 'string', array( 'listColumns' => array('flex' => 3) ))
            ->addField( 'value', \Yii::t('payments', 'active'), 'i', 'check', array( 'listColumns' => array('flex' => 1) ))
            ;

        $aItems = Api::getPaymentsList();

        $oList->setValue( $aItems );

        $oList->setEditableFields( array('value'), 'saveActive' );

        $oList->addRowButtonUpdate( 'edit' );

        $this->setInterface( $oList->getForm() );

        return psComplete;
    }

    protected function actionEdit(){
        $sType = $this->getInDataVal( 'type' );

        $aPaymentsFields = Api::getFields4PaymentType( $sType );

        if (!$aPaymentsFields){
            $this->actionList();
        }

        $oForm = UI\StateBuilder::newEdit();

        $aFields = array( 'active' );
        $oForm->addField( 'active', \Yii::t('payments', 'active'), 'i', 'check');

        foreach ($aPaymentsFields as $aField){
            $aFields[] = $aField[0];
            $oForm->addField($aField[0], $aField[1], $aField[2], $aField[3]);
        }

        $aItems = Api::getParams( $sType, $aFields );

        if (!$aItems){
            $aItems = array();
        }

        $oForm->addField( 'type', $sType, 's', 'hide');
        $aItems['type'] = $sType;

        $oForm->setValue( $aItems );

        $oForm->addButtonSave( 'save' );
        $oForm->addButtonCancel( 'list' );

        $this->setInterface( $oForm->getForm() );
    }

    /**
     * Сохранение активности из списка
     * @return int
     */
    protected function actionSaveActive(){
        $aType = $this->getInDataVal( 'type' );

        if (!$aType || !Api::existType($aType)){
            return $this->actionList();
        }

        $oParam = ar\Params::getParam( $aType, 'active');
        $oParam->value = $this->getInDataVal('value');
        $oParam->save();

        return $this->actionList();
    }

    protected function actionSave(){
        $aType = $this->getInDataVal( 'type' );

        if (!$aType || !Api::existType($aType)){
            return $this->actionList();
        }

        /**
         * @todo нехорошо
         */
        $aData = $this->getInData();
        unset($aData['type']);

        if ($aData){
            foreach( $aData as $sKey => $sVal){
                $oParam = ar\Params::getParam( $aType, $sKey);
                $oParam->value = $sVal;
                $oParam->save();
            }
        }
        return $this->actionList();
    }
}