<?php

namespace skewer\build\Tool\Payments;

use PayPal\Rest\ApiContext;
use PayPal\Auth\OAuthTokenCredential;
use PayPal\Api\Payer;
use PayPal\Api\Amount;
use PayPal\Api\Item;
use PayPal\Api\ItemList;
use PayPal\Api\Payment as PaylibPayment;
use PayPal\Api\Transaction;
use PayPal\Exception as PayPalException;
use PayPal\Api\PaymentExecution;
//use PayPal\IPN;
use skewer\build\Adm\Order;

/**
 * Class PaypalPayment
 * @package skewer\build\Tool\Payments
 */
class PaypalPayment extends Payment{

    /**
     * @var null|ApiContext
     */
    private $oApiContext = null;

    /**
     * @var null|Transaction
     */
    private $oTransaction = null;

    /**
     * @var null|Amount
     */
    private $oAmount = null;

    /**
     * @var null|ItemList
     */
    private $oItemList = null;

    /**
     * Код валюты
     * @var string
     */
    private $sCurrency = 'RUB';

    /**
     * @var string
     */
    private $paymentId = '';

    /**
     * clientId
     * @var string
     */
    private $clientId = '';

    /**
     * clientSecret
     * @var string
     */
    private $clientSecret = '';

    /**
     * Сообщение об успешной оплате
     * @var string
     */
    private $sSuccessMsg = '';

    /**
     * @var array Список полей для редактирования
     */
    protected static $aFields = array(
        array( 'clientId', 'PayPal_clientId_field', 's', 'str'),
        array( 'clientSecret', 'PayPal_clientSecret_field', 's', 'pass'),
        array( 'currency', 'PayPal_currency_field', 's', 'str')
    );

    public function init(){

        $this->oApiContext = false;

        $this->sCurrency = \SysVar::get('PayPal.currency');

        $this->oAmount = new Amount();
        $this->oAmount->setCurrency($this->sCurrency);

        $this->oTransaction = new Transaction();

        $this->oItemList = new ItemList();
    }


    private function getContext(){

        try{

            if (!$this->clientId || !$this->clientSecret){
                throw new \Exception('not auth!');
            }

            $oApiContext = new ApiContext(new OAuthTokenCredential(
                $this->clientId, $this->clientSecret));

            if (defined('PayPalSandbox') && PayPalSandbox == 1){
                $oApiContext->setConfig(array( 'mode' => 'sandbox'));
            }else{
                $oApiContext->setConfig(array( 'mode' => 'live'));
            }

        }
        catch(\Exception $e){
            $oApiContext = false;
        }

        return $oApiContext;
    }

    /**
     * Тип агрегатора систем оплат
     * @return mixed
     */
    public function getType(){
        return 'PayPal';
    }

    /**
     * Устанавливаем список товаров в заказе
     */
    private function setItems(){

        $aItems = Order\ar\Goods::find()->where('id_order', $this->orderId)->asArray()->getAll();
        if (count($aItems)){
            $aItemsOrder = array();
            foreach($aItems as $aItem){
                if (!is_array($aItem)){
                    continue;
                }
                $oItem = new Item();
                $oItem->setName((isset($aItem['title']))?$aItem['title']:'');
                $oItem->setCurrency($this->sCurrency);
                $oItem->setPrice((isset($aItem['price']))?$aItem['price']:'');
                $oItem->setQuantity((isset($aItem['count']))?$aItem['count']:'');
                $oItem->setSku((isset($aItem['id_goods']))?$aItem['id_goods']:'');

                $aItemsOrder[] = $oItem;
            }
            if (count($aItemsOrder)){
                $this->oItemList->setItems($aItemsOrder);
            }
        }
    }

    /**
     * Метод проверки результата об оплате, полученного от системы платежей
     * @return mixed
     */
    public function checkResult(){
/*        if (!$this->IPNValidate()){
            $this->sSuccessMsg = 'fail';
        }*/

        $payment_status = \skRequest::getStr('payment_status');
        $txn_id = \skRequest::getStr('txn_id');
        $mc_gross = \skRequest::getStr('mc_gross');
        // $_GET['txn_id']          Ид платежа PayPal
        // $_GET['mc_gross']        Сумма платежа
        // $_GET['mc_currency']     Валюта платежа
        // $_GET['payer_email']     Еmail плательщика
        // $_GET['item_number1']    Ид первого товара
        // $_GET['payment_status']  Статус заказа
        // $_GET['receiver_email']  Email получателя

        $payment_status = strtolower($payment_status);

        switch ($payment_status) {
            // Платеж успешно выполнен, оказываем услугу
            case 'completed':
                /**
                 * @var $oPay ar\PayPalPaymentRow
                 */
                $oPay = ar\PayPalPayments::find()->where('payment', $txn_id)->getOne();

                if (!$oPay){
                    return false;
                }

                if ($oPay->order_id){

                    $this->orderId = $oPay->order_id;

                    /**
                     * проверим сумму
                     */
                    $price = Api::getOrderSum( $oPay->order_id);

                    $this->setSum($price);

                    if ($mc_gross == $price){
                        $oPay->delete();
                        return true;
                    }
                }
                break;
            // Платеж не прошел
            case 'failed':
                break;
            // Платеж отменен продавцом
            case 'denied':
                break;
            // Деньги были возвращены покупателю
            case 'refunded':
                break;
        }

        $this->sSuccessMsg = $payment_status;

        return false;
    }

    /**
     * Проверка валидации в IPN
     * @return bool;
     */
/*    public function IPNValidate(){
        if (defined('PayPalSandbox') && PayPalSandbox == 1){
            $ipn = new IPN\PPIPNMessage(null, array('mode' => 'sandbox'));
        }else{
            $ipn = new IPN\PPIPNMessage(null, array('mode' => 'live'));
        }

        return $ipn->validate();
    }*/

    /**
     * Вывод формы для оплаты
     * @return string
     */
    public function getForm(){

        /**
         * @var ar\PayPalPaymentRow $oPayment
         */
        $oPayment = ar\PayPalPayments::find()->where('order_id', $this->orderId)->getOne();

        $sRes = '';

        if (!$oPayment || !$oPayment->href){
            /** Если первый раз - создадим оплату в пайпале */
            $oPayer = new Payer();
            $oPayer->setPaymentMethod('paypal');

            $payment = new PaylibPayment();
            $payment->setIntent('sale');
            $payment->setPayer($oPayer);

            $total = number_format($this->sum, 2, '.', '');
            $this->oAmount->setTotal($total);

            $this->oTransaction->setAmount($this->oAmount);
            $this->oTransaction->setDescription($this->description);

            $this->setItems();

            $this->oTransaction->setItemList($this->oItemList);
            $payment->setTransactions(array($this->oTransaction));

            $payment->setRedirectUrls([
                "return_url" => $this->getSuccessUrl(),
                "cancel_url" => $this->getCancelUrl()
            ]);

            try{
                $oContext = $this->getContext();
                if (!$oContext){
                    return $sRes;
                }
                $payment->create();
            }
            catch(PayPalException\PPConnectionException $e){
                \skLogger::dump(json_decode($e->getData()));
                return $sRes;
            }
            catch(PayPalException\PayPalConnectionException $e){
                \skLogger::dump(json_decode($e->getData()));
                return $sRes;
            }

            if ($payment->getState() == "created"){

                $oPayment = ar\PayPalPayments::getNewRow();

                $oPayment->order_id = $this->orderId;
                $oPayment->payment = $payment->getId();
                $oPayment->date = date('Y-m-d H:i:s');
                $this->paymentId = $payment->getId();

                if ($payment->getState() == "created"){
                    $links = $payment->getLinks();
                    foreach ($links as $link) {
                        if ($link->getMethod() == 'REDIRECT') {

                            $oForm = new Form();
                            $sHref = $link->getHref();
                            $oForm->setAction( $sHref );
                            $oPayment->href = $sHref;

                            return $this->parseForm( $oForm );
                        }
                    }
                }
                $oPayment->save();
            }
        }else{
            $oForm = new Form();
            $oForm->setAction( $oPayment->href );

            return $this->parseForm( $oForm );

        }

        return $sRes;

    }

    /**
     * @todo Возможно функция не нужна будет, если оплата будет подтверждаться через IPN
     * Выполнение платежа
     * @return bool
     */
    public function execute(){

        /**
         * @var ar\PayPalPaymentRow $oPay
         */
        $oPay = ar\PayPalPayments::find()->where('order_id', $this->orderId)->getOne();

        if (!$oPay){
            return false;
        }

        $paymentId = $oPay->payment;

        $oPayment = PaylibPayment::get($paymentId, $this->oApiContext);

        $iPayerId = \skRequest::getStr('PayerID');

        if ($iPayerId == ''){
            return false;
        }
        $oPaymentExecution= new PaymentExecution();
        $oPaymentExecution->setPayerId($iPayerId);

        try{
            $result = $oPayment->execute($oPaymentExecution, $this->getContext());
            return true;
        }
        catch(PayPalException\PPConnectionException $e){
            //echo '<pre>';print_r(json_decode($e->getData()));exit;
            return false;
        }

        return false;
    }

    /**
     * Сообщение об успешной оплате
     * @return string
     */
    public function getSuccess(){

        return $this->sSuccessMsg;

    }

    /**
     * Сообщение о неуспешной оплате
     * @return string
     */
    public function getFail(){
        return 'fail';
    }

    /**
     * Инициализация параметров
     * @param array $aParams
     */
    public function initParams( $aParams = array() ){
        if ($aParams){
            foreach($aParams as $sKey => $aParam){
                switch ( $sKey ){
                    case 'active':
                        $this->active = $aParam;
                        break;
                    case 'clientId':
                        $this->clientId = $aParam;
                        break;
                    case 'clientSecret':
                        $this->clientSecret = $aParam;
                        break;
                    case 'currency':
                        $this->sCurrency = $aParam;
                        $this->oAmount->setCurrency($this->sCurrency);
                        break;
                }
            }
            $this->bInitParams = true;
        }
    }

}