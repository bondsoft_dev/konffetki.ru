<?php

namespace skewer\build\Tool\Payments;


/**
 * Class PayanywayPayment
 * @package skewer\build\Tool\Payments
 */
class PayanywayPayment extends Payment{

    /** Адреса шлюзов */
    const PRODUCTION_WSDL= 'https://www.moneta.ru/services.wsdl';
    const DEVELOPMENT_WSDL = 'https://demo.moneta.ru/services.wsdl';

    /** url */
    const PRODUCTION_URL = 'https://www.payanyway.ru/assistant.htm';
    const DEVELOPMENT_URL = 'https://demo.moneta.ru/assistant.htm';

    /** @var string url шлюза */
    private $wsdl = '';

    /** @var string url страницы */
    private $url = '';

    /** @var string Логин */
    private $sLogin = '';

    /** @var string Пароль */
    private $sPassword = '';

    /** @var string Номер счета */
    private $iInvoice = '';

    /** @var string Код проверки целостности данных */
    private $sCode = '';

    /** @var string Валюта */
    private $currency = 'RUB';

    /** @var bool Дебаг */
    private $bDebug = false;

    /** @var int */
    private $subscriber = '';

    /**
     * @var array Список полей для редактирования
     */
    protected static $aFields = array(
        array( 'login', 'Payanyway_login_field', 's', 'str'),
        array( 'password', 'Payanyway_password_field', 's', 'pass'),
        array( 'invoice', 'Payanyway_invoice_field', 'i', 'str'),
        array( 'code', 'Payanyway_code_field', 's', 'str'),
    );

    /**
     * Конструктор
     */
    public function init(){

        $this->url = static::PRODUCTION_URL;

        if ( defined('PAYANYWAY_DEBUG') && PAYANYWAY_DEBUG ){
            $this->url = static::DEVELOPMENT_URL;
            $this->wsdl = static::DEVELOPMENT_WSDL;
            $this->bDebug = true;
        }
        else{
            $this->url = static::PRODUCTION_URL;
            $this->wsdl = static::PRODUCTION_WSDL;
        }
    }

    public function getType(){
        return 'payanyway';
    }

    /**
     * Устанавливает логин
     * @param string $sLogin
     */
    public function setLogin($sLogin){
        $this->sLogin = $sLogin;
    }

    /**
     * Возвращает модуль
     * @return string
     */
    public function getLogin(){
        return $this->sLogin;
    }

    /**
     * Установить номер счета
     * @param string $iInvoice
     */
    public function setInvoice($iInvoice)
    {
        $this->iInvoice = $iInvoice;
    }

    /**
     * Номер счета
     * @return string
     */
    public function getInvoice(){
        return $this->iInvoice;
    }

    /**
     * Установить пароль
     * @param string $sPassword
     */
    public function setPassword($sPassword){
        $this->sPassword = $sPassword;
    }

    /**
     * Возвращает пароль
     * @return string
     */
    public function getPassword(){
        return $this->sPassword;
    }

    /**
     * Код проверки
     * @param string $sCode
     */
    public function setCode($sCode){
        $this->sCode = $sCode;
    }

    /**
     * Код проверки
     * @return string
     */
    public function getCode(){
        return $this->sCode;
    }

    /**
     * Установить валюту
     * @param string $currency
     */
    public function setCurrency($currency){
        $this->currency = $currency;
    }

    /**
     * Валюта
     * @return string
     */
    public function getCurrency(){
        return $this->currency;
    }

    /**
     * Построение формы
     * @return string|void
     */
    public function getForm(){

        if (!$this->active){
            return '';
        }

        $this->setDescription(\Yii::t('order', 'order_description', [$this->getOrderId()]));

        $oForm = new Form();
        $oForm->setAction($this->url);

        $aItems = array();
        $aItems['MNT_ID'] = $this->iInvoice;

        $aItems['MNT_TRANSACTION_ID'] = $this->orderId;
        $aItems['MNT_CURRENCY_CODE'] = $this->currency;
        $aItems['MNT_AMOUNT'] = number_format($this->sum, 2, '.', '');
        $aItems['MNT_TEST_MODE'] = (int)$this->bDebug;
        $aItems['MNT_SUBSCRIBER_ID'] = $this->subscriber;
        $aItems['MNT_SIGNATURE'] = $this->createResultSignature();
        $aItems['MNT_DESCRIPTION'] = $this->description;
        $aItems['MNT_TYPE'] = $this->getType();

        $oForm->setFields( $aItems );

        return $this->parseForm( $oForm );
    }

    /**
     * генерация подписи
     * @param $sOperationId
     * @return string
     */
    private function createResultSignature( $sOperationId = '' ){
        return md5(
            $this->iInvoice . $this->orderId . $sOperationId. number_format($this->sum, 2, '.', '') .
            $this->currency . $this->subscriber . (int)$this->bDebug . $this->sCode
        );
    }

    /**
     * проверка оплаты
     * @return bool|mixed
     */
    public function checkResult(){

        $sum = \skRequest::getStr( 'MNT_AMOUNT' );
        $orderId = (int)\skRequest::getStr( 'MNT_TRANSACTION_ID' );
        $sOperationId = (int)\skRequest::getStr( 'MNT_OPERATION_ID' );
        $signature = \skRequest::getStr( 'MNT_SIGNATURE' );


        \skLogger::error("id заказа с системой оплаты " . $orderId);

        if ($sum != Api::getOrderSum( $orderId ))
            return false;

        $this->setOrderId( $orderId );
        $this->setSum( $sum );



        return (strtolower($signature) == $this->createResultSignature( $sOperationId ));

    }

    /**
     * Сообщение об успешной оплате
     * @return string
     */
    public function getSuccess(){
        return 'SUCCESS';
    }

    /**
     * Сообщение о неуспешной оплате
     * @return string
     */
    public function getFail(){
        return 'FAIL';
    }

    /**
     * Инициализация параметров
     * @param array $aParams
     */
    public function initParams( $aParams = array() ){
        if ($aParams){
            foreach($aParams as $sKey => $aParam){
                switch ( $sKey ){
                    case 'active':
                        $this->active = $aParam;
                        break;
                    case 'rk_login':
                        $this->sLogin = $aParam;
                        break;
                    case 'password':
                        $this->sPassword = $aParam;
                        break;
                    case 'invoice':
                        $this->iInvoice = $aParam;
                        break;
                    case 'code':
                        $this->sCode = $aParam;
                        break;

                }
            }
            $this->bInitParams = true;
        }
    }
}