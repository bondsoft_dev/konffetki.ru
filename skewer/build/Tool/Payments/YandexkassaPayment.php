<?php

namespace skewer\build\Tool\Payments;


/**
 * Class YandexkassaPayment
 * @see https://money.yandex.ru/doc.xml?id=526537
 * @package skewer\build\Tool\Payments
 */
class YandexkassaPayment extends Payment{

    /** Продакшен-шлюз */
    const prodUrl = 'https://money.yandex.ru/eshop.xml';

    /** Тестовый шлюз */
    const devUrl = 'https://demomoney.yandex.ru/eshop.xml';

    /** Рубль */
    const valRub = 643;

    /** Тестовая валюта */
    const valTest = 10643;

    /** @var string  */
    private $url = '';

    /** @var int Идентификатор Контрагента */
    private $shopId = 0;

    /** @var int Номер витрины Контрагента */
    private $scid = 0;

    /** @var int Код валюты */
    private $sCurrencyCode = 0;

    /** @var string Пароль магазина */
    private $shopPassword = '';

    /** @var string Способ оплаты */
    private $paymentType = 'AC';

    /** @var  string Текст ответа яндексу */
    private $xml;

    /**
     * @var [] Список полей для редактирования
     */
    protected static $aFields = [
        ['shopId', 'YandexKassa_shopId_field', 'i', 'str'],
        ['scid', 'YandexKassa_scid_field', 'i', 'str'],
        ['shopPassword', 'YandexKassa_shopPassword_field', 's', 'pass']
    ];


    public function init() {

        if ( defined('YANDEXKASSA_DEBUG') && YANDEXKASSA_DEBUG ){
            $this->url = static::devUrl;
            $this->sCurrencyCode = static::valTest;
        } else {
            $this->url = static::prodUrl;
            $this->sCurrencyCode = static::valRub;
        }

        parent::init();
    }


    /**
     * Тип агрегатора систем оплат
     * @return mixed
     */
    public function getType(){
        return 'yandexkassa';
    }


    /**
     * Сумма заказа
     * @return float|string
     */
    public function getSum(){
        return number_format( $this->sum, 2, '.', '' );
    }


    /**
     * Сообщение о неуспешной оплате
     * @return string
     */
    public function getFail() {
        header("Content-type: text/xml; charset=utf-8");
        return $this->xml;
    }

    /**
     * Сообщение об успешной оплате
     * @return string
     */
    public function getSuccess() {
        header("Content-type: text/xml; charset=utf-8");
        return $this->xml;
    }

    /**
     * Метод проверки результата об оплате, полученного от системы платежей
     * @return bool
     */
    public function checkResult(){
        $md5 = \skRequest::getStr( 'md5' );
        $shopId = \skRequest::getStr( 'shopId' );
        $invoiceId = \skRequest::getStr( 'invoiceId' );

        $hash = implode(';', [
            'checkOrder',
            $this->getSum(),
            $this->sCurrencyCode,
            \skRequest::getStr( 'orderSumBankPaycash' ),
            $this->shopId,
            $invoiceId,
            \skRequest::getStr( 'customerNumber' ),
            $this->shopPassword
        ]);

        $aCallbackParams = [
            'action' => 'checkOrder',
            'invoiceId' => $invoiceId
        ];
        if ($shopId != $this->shopId || $hash != $md5){
            /** Не принимаем платеж! */
            $this->sendCode( $aCallbackParams, 100 );
        } else {
            /** Принимаем платеж */
            $this->sendCode( $aCallbackParams, 0 );
        }

    }

    /**
     * Текст для отправки сообщения о принятии платежа
     * @param $callbackParams
     * @param $code
     * @return string
     */
    private function sendCode($callbackParams, $code){

        $this->xml = '<?xml version="1.0" encoding="UTF-8"?>
			<'.$callbackParams['action'].'Response performedDatetime="'.date("c").'" code="'.$code.'" invoiceId="'.$callbackParams['invoiceId'].'" shopId="'.$this->shopId.'"/>';

    }


    /**
     * Вывод формы для оплаты
     * @return string
     */
    public function getForm(){

        if (!$this->active){
            return '';
        }

        $oForm = new Form();
        $oForm->setAction($this->url);
        $oForm->setMethod('POST');

        $aFields = [
            'shopId' => $this->shopId,
            'scid' => $this->scid,
            'sum' => $this->getSum(),
            'orderDetails' => \Yii::t('order', 'order_description', [$this->getOrderId()]),
            'customerNumber' => 1, //Идентификатор плательщика в ИС Контрагента.
            'paymentType' => $this->paymentType,
            'shopSuccessURL' => $this->getSuccessUrl(),
            'shopFailURL' => $this->getCancelUrl()
        ];

        $oForm->setFields( $aFields );

        return $this->parseForm( $oForm );
    }


    /**
     * Инициализация параметров
     * @param [] $aParams
     */
    public function initParams($aParams = []) {
        if ($aParams){
            foreach($aParams as $sKey => $aParam){
                switch ( $sKey ){
                    case 'active':
                        $this->active = $aParam;
                        break;
                    case 'shopId':
                        $this->shopId = $aParam;
                        break;
                    case 'scid':
                        $this->scid = $aParam;
                        break;
                }
            }
            $this->bInitParams = true;
        }
    }

}