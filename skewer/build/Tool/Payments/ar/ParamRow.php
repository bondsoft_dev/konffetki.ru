<?php

namespace skewer\build\Tool\Payments\ar;


use skewer\build\Component\orm;

/**
 * Class Params
 * @package skewer\build\Tool\Payments
 */
class ParamRow extends orm\ActiveRecord {

    public $id = 0;
    public $type = '';
    public $name = '';
    public $value = '';

    function __construct() {
        $this->setTableName( 'payment_parameters' );
        $this->setPrimaryKey( 'id' );
    }

} 