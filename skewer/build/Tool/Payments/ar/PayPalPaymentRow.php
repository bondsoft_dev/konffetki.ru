<?php

namespace skewer\build\Tool\Payments\ar;


use skewer\build\Component\orm;

/**
 * Class Params
 * @package skewer\build\Tool\Payments
 */
class PayPalPaymentRow extends orm\ActiveRecord {

    public $id = 0;
    public $order_id = 0;
    public $payment = '';
    public $href = '';
    public $date = '';

    function __construct() {
        $this->setTableName( 'paypal_payments' );
        $this->setPrimaryKey( 'id' );
    }

} 