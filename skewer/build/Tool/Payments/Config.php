<?php

use skewer\build\Tool\LeftList;

$aConfig['name']     = 'Payments';
$aConfig['title']    = 'Системы оплат';
$aConfig['version']  = '1.1';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['useNamespace'] = true;
$aConfig['layer']     = \Layer::TOOL;
$aConfig['group']     = LeftList\Group::ADMIN;

$aConfig['dependency'] = array(
    array('Payment', \Layer::PAGE)
);

return $aConfig;
