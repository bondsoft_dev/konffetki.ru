<?php

namespace skewer\build\Tool\Payments;


class Install extends \skModuleInstall {

    public function init() {
        return true;
    }

    public function install() {

        ar\Params::rebuildTable();
        ar\PayPalPayments::rebuildTable();
        ar\Params::getParam( 'robokassa', 'active', true);
        ar\Params::getParam( 'payanyway', 'active', true);
        ar\Params::getParam( 'paypal', 'active', true);
        ar\Params::getParam( 'yandexkassa', 'active', true);

        return true;
    }

    public function uninstall() {
        return true;
    }
}
