<?php

namespace skewer\build\Tool\Payments;


/**
 * Class RobokassaPayment
 * @package skewer\build\Tool\Payments
 */
class RobokassaPayment extends Payment{

    /** Адреса шлюзов */
    const PRODUCTION_URL = 'https://merchant.roboxchange.com/Index.aspx';
    const DEVELOPMENT_URL = 'http://test.robokassa.ru/Index.aspx';

    /** @var string Логин */
    private $login = '';

    /** @var string Пароль 1 */
    private $password1 = '';

    /** @var string Пароль 2 */
    private $password2 = '';

    /** @var string Язык интерфейса */
    private $language = 'ru';

    /** @var string Выбранный тип оплаты. Если пусто, то пользователь сам выбирает способ оплаты */
    private $ecurrency;

    /** @var string Кодировка */
    private $encoding = 'utf-8';

    /** @var string URL для отправки формы */
    private $url;

    /**
     * @var array Список полей для редактирования
     */
    protected static $aFields = array(
        array( 'rk_login', 'Robokassa_login_field', 's', 'str'),
        array( 'rk_password1', 'Robokassa_password1_field', 's', 'pass'),
        array( 'rk_password2', 'Robokassa_password2_field', 's', 'pass'),
    );

    public function getType(){
        return 'robokassa';
    }

    /**
     * Конструктор
     */
    public function init(){

        $this->url = static::PRODUCTION_URL;

        if ( defined('ROBOKASSA_DEBUG') && ROBOKASSA_DEBUG )
            $this->url = static::DEVELOPMENT_URL;
        else
            $this->url = static::PRODUCTION_URL;
    }

    /**
     * Устанавливает логин
     * @param string $rk_login
     */
    public function setLogin($rk_login){
        $this->login = $rk_login;
    }

    /**
     * Логин
     * @return string
     */
    public function getLogin(){
        return $this->login;
    }

    /**
     * Устанавливает пароль 1
     * @param string $rk_password1
     */
    public function setPassword1($rk_password1){
        $this->password1 = $rk_password1;
    }

    /**
     * Устанавливает пароль 2
     * @param string $rk_password2
     */
    public function setPassword2($rk_password2){
        $this->password2 = $rk_password2;
    }

    /**
     * Возвращает способ оплаты
     * @return string Способ оплаты
     */
    public function getEcurrency() {
        return $this->ecurrency;
    }

    /**
     * Устанавливает способ оплаты
     * @param string $ecurrency Способ оплаты
     */
    public function setEcurrency($ecurrency) {
        $this->ecurrency = $ecurrency;
    }

    /**
     * Возвращает кодировку
     * @return string Кодировка
     */
    public function getEncoding() {
        return $this->encoding;
    }

    /**
     * Устанавливает кодировку
     * @param string $encoding Кодировка
     */
    public function setEncoding($encoding) {
        $this->encoding = $encoding;
    }

    /**
     * Возвращает язык интерфейса
     * @return string Язык интерфейса
     */
    public function getLanguage() {
        return $this->language;
    }

    /**
     * Устанавливает язык интерфейса
     * @param string $language Язык интерфейса
     */
    public function setLanguage($language) {
        $this->language = $language;
    }

    /**
     * Возвращает URL шлюза
     * @return string URL
     */
    public function getUrl(){
        return $this->url;
    }

    /**
     * Генерирует подпись для оплаты
     * @return string
     */
    public function createPaymentSignature() {
        return md5($this->login.':'.$this->getSum().':'.$this->getOrderId().':'.$this->password1.':shp_type='.$this->getType());
    }

    /**
     * Построение формы
     * @return string|void
     */
    public function getForm(){

        if (!$this->active){
            return '';
        }

        $this->setDescription(\Yii::t('order', 'order_description', [(int)$this->getOrderId()]));

        $oForm = new Form();
        $oForm->setAction($this->url);

        $aItems = array();
        $aItems['MrchLogin'] = $this->login;
        $aItems['OutSum'] = $this->sum;
        $aItems['InvId'] = $this->orderId;
        $aItems['Desc'] = $this->description;
        $aItems['SignatureValue'] = $this->createPaymentSignature();
        $aItems['IncCurrLabel'] = $this->ecurrency;
        $aItems['Culture'] = $this->language;
        $aItems['encoding'] = $this->encoding;

        $aItems['shp_type'] = $this->getType();

        $oForm->setFields( $aItems );

        return $this->parseForm( $oForm );
    }

    /**
     * Генерирует подпись для сравнения после оплаты
     * @param float $sum Сумма заказа
     * @param int $orderId Идентификатор заказа
     * @return string
     */
    public function createResultSignature($sum, $orderId) {
        return md5($sum.':'.$orderId.':'.$this->password1.':shp_type'.$this->getType());
    }

    /**
     * Сравнивает подпись после совершения оплаты
     * @return bool
     */
    public function checkResult() {
        $sum = \skRequest::getStr('OutSum');
        $orderId = (int)\skRequest::getStr('InvId');
        $signature = \skRequest::getStr('SignatureValue');

        if ($sum != Api::getOrderSum( $orderId ))
            return false;

        $this->setOrderId( $orderId );
        $this->setSum( $sum );

        return (strtolower($signature) == $this->createResultSignature($sum, $orderId));
    }

    /**
     * Сообщение об успешной оплате
     * @return string
     */
    public function getSuccess(){
        return 'OK'. $this->getOrderId();
    }

    /**
     * Сообщение о неуспешной оплате
     * @return string
     */
    public function getFail(){
        return 'FAIL';
    }

    /**
     * Инициализация параметров
     * @param array $aParams
     */
    public function initParams( $aParams = array() ){
        if ($aParams){
            foreach($aParams as $sKey => $aParam){
                switch ( $sKey ){
                    case 'active':
                        $this->active = $aParam;
                        break;
                    case 'rk_login':
                        $this->login = $aParam;
                        break;
                    case 'rk_password1':
                        $this->password1 = $aParam;
                        break;
                    case 'rk_password2':
                        $this->password2 = $aParam;
                        break;
                }
            }
            $this->bInitParams = true;
        }
    }

}