<?php

namespace skewer\build\Tool\Payments;


use skewer\build\Adm\Order\Service;
use yii\helpers\ArrayHelper;

/**
 * Class Api
 * @package skewer\build\Tool\Payments
 */
class Api{

    /**
     * @todo Возможно в конфиги?
     * Список оплат в системе
     * @var array
     */
    private static $aList = array(
        'robokassa',
        'payanyway',
        'paypal',
        //'yandexkassa' //@todo #31750 временно убрали
    );

    /**
     * Возвращает полный список систем оплаты
     * @param bool $bActive только активные
     * @return array
     */
    public static function getPaymentsList( $bActive = false ){

        $oQuery = ar\Params::find()->where('type IN ?', static::$aList)->where('name', 'active');

        if ($bActive){
            $oQuery->where('value', 1);
        }
        $aItems = $oQuery->asArray()->getAll();

        if ($aItems){
            foreach( $aItems as &$aItem ){
                $aItem['title'] = \Yii::t( 'payments', $aItem['type']);
            }
        }

        return $aItems;
    }

    /**
     * Возвращает экземпляр класса платежей по типу
     * @param $sPaymentType
     * @return bool|Payment
     */
    public static function make( $sPaymentType ){

        if (!static::existType( $sPaymentType )){
            return false;
        }

        $sClassName =  __NAMESPACE__ .'\\'. mb_convert_case( $sPaymentType, MB_CASE_TITLE ) . 'Payment';

        if (!class_exists($sClassName)){
            return false;
        }

        if (!is_subclass_of($sClassName, __NAMESPACE__ .'\\'.'Payment')){
            return false;
        };

        if (!method_exists($sClassName, 'getInstance')){
            return false;
        }

        /**
         * @var Payment $oPayment
         */
        $oPayment = $sClassName::getInstance();

        if (!$oPayment->isInitParams()){
            $aParams = ar\Params::find()->where('type', $sPaymentType)->asArray()->getAll();

            $aParams = ArrayHelper::map( $aParams, 'name', 'value' );

            $oPayment->initParams( $aParams );
        }

        if ($oPayment->getActive()){
            return $oPayment;
        }

        return false;
    }

    /**
     * Список полей для редактирования для типа оплаты
     * @param $sPaymentType
     * @return array
     */
    public static function getFields4PaymentType( $sPaymentType ){

        if (!static::existType( $sPaymentType )){
            return array();
        }

        $sClassName =  __NAMESPACE__ .'\\'. mb_convert_case( $sPaymentType, MB_CASE_TITLE ) . 'Payment';

        if (!class_exists($sClassName)){
            return array();
        }

        if (!is_subclass_of($sClassName, __NAMESPACE__ .'\\'.'Payment')){
            return array();
        };

        if (!method_exists($sClassName, 'getFields')){
            return array();
        }

        return $sClassName::getFields();
    }

    /**
     * Проверка типа оплаты на существование в системе
     * @param $sType
     * @return bool
     */
    public static function existType( $sType ){
        return (array_search( $sType, static::$aList ) !== false) ? true : false;
    }

    /**
     * Получение значений параметров для типа оплаты
     * @param $sType
     * @param array $aParamNames
     * @return array|bool
     */
    public static function getParams( $sType, $aParamNames = array() ){

        $oQuery = ar\Params::find()->where('type', $sType);
        if (count($aParamNames)){
            $oQuery->where('name IN ?', $aParamNames);
        }
        $aItems = $oQuery->asArray()->getAll();

        if ($aItems){
            return ArrayHelper::map($aItems, 'name', 'value');
        }

        return false;
    }

    /**
     * Название типа оплаты
     * @param array $aItem
     * @return string
     */
    public static function getPaymentsTitle( $aItem ){
        return \Yii::t('Payments', $aItem['payment'] );
    }


    /**
     * Сумма заказа
     * @param $iOrderId
     * @return string
     */
    public static function getOrderSum( $iOrderId ){
        return Service::getTotalPrice( $iOrderId );
    }

}