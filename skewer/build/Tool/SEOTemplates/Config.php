<?php

use skewer\build\Tool\LeftList;


$aConfig['name']     = 'SEOTemplates';
$aConfig['title']    = 'SEO шаблоны';
$aConfig['version']  = '1.000a';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['useNamespace'] = true;
$aConfig['layer']     = \Layer::TOOL;
$aConfig['group']     = LeftList\Group::ADMIN;
$aConfig['languageCategory']     = 'SEO';


return $aConfig;
