<?php

namespace skewer\build\Tool\SEOTemplates;


use skewer\build\Component\Site\Type;
use skewer\build\Component\Catalog\Card;
use skewer\build\Tool;
use skewer\build\Component\UI;
use skewer\build\Component\SEO;


/**
 * Модуль редактирования шаблонов для SEO параметров
 * Class Module
 * @package skewer\build\Tool\SEOTemplates
 */
class Module extends Tool\LeftList\ModulePrototype {

    public function actionInit(){

        $this->actionList();

    }


    /**
     * Список шаблонов
     */
    public function actionList(){

        $aItems = SEO\Template::find()->getAll();


        // создаем форму
        $oFormBuilder = UI\StateBuilder::newList();

        // добавляем поля
        $oFormBuilder
            //->addField( 'id', 'id', 'i', 'hide', array('listColumns' => array('width' => 40)) )
            ->addField( 'sid', \Yii::t('SEO', 'sid'), 's', 'string', array('listColumns' => array('width' => 150)) )
            ->addField( 'name', \Yii::t('SEO', 'name'), 's', 'string', array('listColumns' => array('flex' => 1)) )
            // добавляем данные
            ->setValue( $aItems )
            // элементы управления
            ->addRowButtonUpdate( 'editForm' )
            ->addButtonIf( Type::hasCatalogModule(), \Yii::t('adm','clone'), 'selectCard', 'icon-clone' )
        ;

        if ( \CurrentAdmin::isSystemMode() ) {
            $oFormBuilder
                ->addRowButtonDelete( 'delete' )
            ;
        }

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

    }


    /**
     * ФОрма редактирование seo шаблона
     */
    public function actionEditForm(){

        $aData = $this->getInData();
        $iTplId = isSet( $aData['id'] ) ? $aData['id'] : 0;

        if ( !$iTplId )
            $this->addError( 'SEO Tempate not found!' );

        /** @var SEO\TemplateRow $oTpl */
        $oTpl = SEO\Template::find( $iTplId );
        $oTpl->info = SEO\Template::getLabelsInfo(); // todo убрать из базы данных поле


        $oFormBuilder = UI\StateBuilder::newEdit();

        $this->setPanelName( \Yii::t('SEO', 'editseo'), true );

        // добавляем поля
        $oFormBuilder
            ->addField( 'id', 'id', 'i', 'hide' )
            ->addField( 'sid', \Yii::t('SEO', 'sid'), 's', 'string' )
            ->addField( 'name', \Yii::t('SEO', 'name'), 's', 'string' )
            ->addField( 'title', \Yii::t('SEO', 'title'), 's', 'string' )
            ->addField( 'description', \Yii::t('SEO', 'description'), 's', 'text' )
            ->addField( 'keywords', \Yii::t('SEO', 'keywords'), 's', 'text' )
            ->addField( 'info', \Yii::t('SEO', 'info'), 's', 'show' )

            ->setValue( $oTpl )

            ->addButton( \Yii::t('adm','save'), 'update', 'icon-save' )
            ->addButton( \Yii::t('adm','cancel'), 'list', 'icon-cancel')
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    public function actionSelectCard() {

        $aCardList = Card::getGoodsCards();
        $aData = array();
        foreach( $aCardList as $oCard )
            $aData[$oCard->name] = $oCard->title;


        $oFormBuilder = UI\StateBuilder::newEdit();

        $this->setPanelName( \Yii::t('SEO', 'editseo'), true );

        // добавляем поля
        $oFormBuilder
            ->addSelectField( 'card', \Yii::t('SEO', 'select_card'), 's', $aData )
            ->setValue( array() )
            ->addButton( \Yii::t('adm','save'), 'clone', 'icon-save' )
            ->addButton( \Yii::t('adm','cancel'), 'list', 'icon-cancel')
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    public function actionClone() {

        $aData = $this->getInData();

        $sCard = isSet( $aData['card'] ) ? $aData['card'] : false;

        if ( !$sCard )
            $this->addError( 'Card not found' );

        $oRow = SEO\Template::find()
            ->where( 'sid', SEO\Api::TPL_CATALOG . ':' . $sCard )
            ->getOne();

        if ( !$oRow ) {

            /** @var SEO\TemplateRow $oRow */
            $oRow = SEO\Template::find()
                ->where( 'sid', SEO\Api::TPL_CATALOG )
                ->getOne();

            $oRow->id = 'NULL';
            $oRow->sid = SEO\Api::TPL_CATALOG . ':' . $sCard;
            $oRow->save();

        } else
            $this->addError( 'SEO template already exsist' );

        $this->actionInit();
    }


    public function actionDelete(){

        try {

            $aData = $this->getInData();

            $iTplId = isSet( $aData['id'] ) ? $aData['id'] : 0;

            if ( !$iTplId )
                $this->addError( 'SEO Tempate not found!' );

            $oTpl = SEO\Template::find( $iTplId );

            if ( !$oTpl->delete() )
                throw new \Exception( \Yii::t('SEO', 'template_not_deleted') );
            
            $this->addNoticeReport(
                \Yii::t('SEO', 'template_deleting'),
                "id $iTplId",
                \skLogger::logUsers,
                "skewer\\build\\Tool\\SEOTemplates\\Module"
            );

        } catch( \Exception $e ) {

            $this->addError($e->getMessage());
        }

        $this->actionList();

        return psComplete;
    }


    public function actionUpdate(){

        try {

            $aData = $this->getInData();

            $iTplId = isSet( $aData['id'] ) ? $aData['id'] : 0;

            if ( !$iTplId )
                $this->addError( 'SEO Tempate not found!' );

            $oTpl = SEO\Template::find( $iTplId );

            $oTpl->setData( $aData );

            if ( !$oTpl->save() )
                throw new \Exception( \Yii::t('SEO', 'template_not_saved') );

            $this->addNoticeReport(
                \Yii::t('SEO', 'template_changing'),
                "id $iTplId",
                \skLogger::logUsers,
                "skewer\\build\\Tool\\SEOTemplates\\Module"
            );
            
        } catch( \Exception $e ) {

            $this->addError($e->getMessage());
        }

        $this->actionList();

        return psComplete;
    }



}
