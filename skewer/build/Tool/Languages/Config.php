<?php

use skewer\build\Tool\LeftList;

$aConfig['name']     = 'Languages';
$aConfig['title']    = 'Управление языками';
$aConfig['version']  = '1.000';
$aConfig['description']  = 'Управление языками';
$aConfig['useNamespace']  = true;
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::TOOL;
$aConfig['group']     = LeftList\Group::LANGUAGE;

return $aConfig;
