<?php

namespace skewer\build\Tool\Languages;


use skewer\build\Component\I18N\Categories;
use skewer\build\Component\I18N\Languages;
use skewer\build\Component\I18N\Messages;
use skewer\build\Component\I18N\models;
use skewer\build\Component\UI;
use skewer\build\Tool;
use skewer\build\libs\ExtBuilder;
use yii\base\UserException;
use yii\helpers\ArrayHelper;


/**
 * Модуль для работы с системой управления языками
 */
class Module extends Tool\LeftList\ModulePrototype {

    /** значение фильтра для "Все" */
    const statusFilterAll = -1;

    /**
     * Первичный запуск
     * @return void
     */
    protected function actionInit() {
        $this->actionLanguagesList();
    }

    /** @var null Текущий язык */
    protected $language = null;

    // фильтр по тексту
    protected $sSearchFilter = '';

    // фильтр по категории
    protected $sCategoryFilter = '';

    /** Фильтр данных */
    protected $iDataFilter = 0;

    // фильтр по статусам
    protected $iStatusFilter = self::statusFilterAll;

    private $bHasSrcLang = false;

    protected function preExecute() {

        $this->sTabName = '';

        $this->sSearchFilter = $this->getStr('search');
        $this->sCategoryFilter = $this->getStr('filter_category');
        $this->iStatusFilter = $this->getInt('filter_status', self::statusFilterAll);
        $this->iDataFilter = $this->getInt('filter_data');
        return parent::preExecute();
    }

    /**
     * Установка служебных данных
     * @param UI\State\BaseInterface $oIface
     */
    protected function setServiceData(UI\State\BaseInterface $oIface) {
        $oIface->setServiceData(array(
            'search' => $this->sSearchFilter,
            'language' => $this->language,
            'filter_status' => $this->iStatusFilter,
            'filter_category' => $this->sCategoryFilter,
            'filter_data' => $this->iDataFilter,
        ));
    }

    /**
     * Список языков
     * @return void
     */
    protected function actionLanguagesList() {

        $oList = UI\StateBuilder::newList();

        $oList
            ->fieldString('name', \Yii::t('languages', 'field_lang_name'))
            ->fieldString('title', \Yii::t('languages', 'field_lang_title'), ['listColumns.flex' => 8])
            ->field('active', \Yii::t('languages', 'field_lang_active'), 'i', 'check')
            ->field('admin', \Yii::t('languages', 'field_lang_admin'), 'i', 'check', ['listColumns.flex' => 3])
            ;

        $oList->setValue(Languages::getAll());

        $oList->button(\Yii::t('languages','addLanguage'), 'newLang', 'icon-add');

        if (Languages::getCountNotActiveLanguage())
            $oList->button(\Yii::t('languages','addBranch'), 'addBranch', 'icon-add');

        $oList->addRowButton(\Yii::t('languages', 'edit'), 'icon-edit', 'EditLang');
        $oList->addRowButton(\Yii::t('languages', 'edit_words'), 'icon-page', 'showKeys', 'edit_form');

        if (\CurrentAdmin::isSystemMode())
            $oList->buttonRowDelete('delLang');

        $this->setInnerData('currentLanguage', '');

        $oList->button(\Yii::t('languages','defaultLanguage'), 'defaultLanguage', 'icon-edit');

        $this->setInterface($oList->getForm());

    }


    /**
     * Список ключей
     * @throws UserException
     */
    protected function actionShowKeys() {

        $oList = UI\StateBuilder::newList();
        $language = $this->getInDataVal('name', $this->language);
        $this->language = $language;

        $aStatusList = models\LanguageValues::getStatusList();
        $aStatusList[0] = \Yii::t('languages', 'status_'.models\LanguageValues::statusNotTranslated);

        $oLang = Languages::getByName($language);
        if (!$oLang)
            throw new UserException(\Yii::t('languages', 'error_lang_not_found'));

        $aDataFilter = [
            '1' => \Yii::t('languages', 'no_data_messages'),
            '2' => \Yii::t('languages', 'data_messages'),
        ];

        // текстовый фильтр
        $oList
            ->addFilterText('search', $this->sSearchFilter)
            ->addFilterSelect('filter_category', Categories::getCategoryList(), $this->sCategoryFilter, \Yii::t('languages', 'field_category'))
            ->addFilterSelect('filter_data', $aDataFilter, $this->iDataFilter, \Yii::t('languages', 'filter_data'))
            //->fieldHide('data', \Yii::t('languages', 'filter_data'))
            ->fieldString('category', \Yii::t('languages', 'field_category'), ['listColumns.flex' => 10])
            ->fieldString('message', \Yii::t('languages', 'field_message'), ['listColumns.flex' => 10])
            ->fieldString('value', \Yii::t('languages', 'field_value'), ['listColumns.flex' => 10])
            ->addFilterAction('showKeys')
        ;

        if (!$this->bHasSrcLang) {
            $this->iStatusFilter = self::statusFilterAll;
        }

        // фильтрация @todo переписать!
        $aFilter = ['language' => $oLang->name];
        if ($this->iDataFilter > 0)
            $aFilter['data'] = $this->iDataFilter-1;
        if ($this->sCategoryFilter)
            $aFilter['category'] = $this->sCategoryFilter;
        if ($this->iStatusFilter >= 0 && $this->iStatusFilter != models\LanguageValues::statusNotTranslated)
            $aFilter['status'] = $this->iStatusFilter;
        if ($this->sSearchFilter) {
            $aFilter['like'] = $this->sSearchFilter;
            $aFilter['like_values'] = 1;
        }

        $this->bHasSrcLang = (bool)$oLang->src_lang;

        $aValueList = Messages::getFiltered($aFilter, true);

        $aValueList = ArrayHelper::index($aValueList, function($aValue) {return $aValue['category'].'.'.$aValue['message'];});

        // если есть исходный язык - добавляем колонку с базовым значением
        if ($this->bHasSrcLang) {

            // добавляем фильтр по статусам
            $oList->addFilterSelect('filter_status', models\LanguageValues::getStatusList(), $this->iStatusFilter,
                \Yii::t('languages', 'field_status'), array('default'=>self::statusFilterAll));

            $oList->fieldString('src', \Yii::t('languages', 'field_src'), ['listColumns.flex' => 8]);
            $oList->fieldString('status_text', \Yii::t('languages', 'field_status'));

            $oList->addRowCustomBtn('StatusGroupBtn');

            $aSrcFilter = $aFilter;
            unset($aSrcFilter['status']);
            unset($aSrcFilter['like_values']);
            $aSrcFilter['language'] = $oLang->src_lang;

            $aSrcValList = Messages::getFilteredSimple($aSrcFilter);

            foreach ($aSrcValList as $sKey => $aVal) {

                if ( !array_key_exists( $sKey, $aValueList ) ) {

                    $aValueList[$sKey] = array(
                        'category' => $aVal['category'],
                        'message' => $aVal['message'],
                        'value' => $aVal['value'],
                        'src' => $aVal['value'],
                        'data' => $aVal['data'],
                        'language' => $language,
                        'override' => models\LanguageValues::overrideNo,
                        // статус "не переведен", так как наследуемые значения не переведены
                        'status' => models\LanguageValues::statusNotTranslated,
                    );

                } else {
                    $aValueList[$sKey]['src'] = $aVal['value'];
                }

            }

        }

        $aValueList = array_filter( $aValueList, array($this, 'filterItems') );

        foreach ($aValueList as $sKey => $aRow)
            $aValueList[$sKey]['status_text'] = $aStatusList[$aValueList[$sKey]['status']];

        /** SORT_FLAG_CASE | SORT_STRING для регистронезависимой сортировки */
        ArrayHelper::multisort($aValueList, ['category', 'message'], SORT_ASC, SORT_FLAG_CASE | SORT_STRING);

        $oList->setValue($aValueList);

        $oList->setEditableFields(['value'], 'saveKey');

        $oList->button(\Yii::t('adm','add'), 'addKey', 'icon-add', 'addForm', ['language'=>$language]);
        $oList->addButtonCancel('init');

        // если язык системный, то кнопка Override отображается
        if (in_array($language, models\LanguageValues::getSystemLanguage()))
            $oList->addRowCustomBtn('OverrideBtn');

        $oList->addRowCustomBtn('DelBtn');

        $this->sTabName = \Yii::t('languages', 'messages_for_lang_title', [$oLang->title]);

        $this->setInterface($oList->getForm());

    }


    /**
     * @todo actionSaveKey и actionSave вынести общею часть в 1 метод
     * Сохранение сообщения
     * @throws UserException
     */
    protected function actionSave(){

        $sCategory = $this->getInDataVal( 'category' );
        $sLanguage = $this->getInDataVal( 'language' );
        $sMessage = $this->getInDataVal( 'message' );

        if (!$sCategory)
            throw new UserException(\Yii::t('languages', 'error_fields_not_found', [\Yii::t('languages', 'field_category')]));

        if (!$sMessage)
            throw new UserException(\Yii::t('languages', 'error_fields_not_found', [\Yii::t('languages', 'field_message')]));

        if (!$sLanguage)
            throw new UserException(\Yii::t('languages', 'error_fields_not_found', [\Yii::t('languages', 'field_lang')]));

        $oRow = Messages::getByName($sCategory, $sMessage, $sLanguage);

        if (!$oRow){
            $oRow = new models\LanguageValues();
            $oRow->category = $sCategory;
            $oRow->message = $sMessage;
            $oRow->language = $sLanguage;
        }

        $oRow->setAttributes($this->getInData());

        $iStatus = (int)$oRow->status;
        if ( $iStatus === models\LanguageValues::statusNotTranslated ) {
            $iStatus = models\LanguageValues::statusTranslated;
            $oRow->status = $iStatus;
        }

        $oRow->override = 1;

        $oRow->save();

        $aRow = $oRow->getAttributes();

        // сбросить языковой кэш
        \Yii::$app->getI18n()->clearCache();

        $aStatusList = models\LanguageValues::getStatusList();
        $aRow['status_text'] = isset($aStatusList[$iStatus]) ? $aStatusList[$iStatus] : '--';

        $oListVals = new \ExtListRows();

        $oListVals->setSearchField( ['category', 'message'] );

        $oListVals->addDataRow( $aRow );

        $oListVals->setData( $this );

    }

    /**
     * Фильтрация меток по тексту
     * @param $aItem
     * @return bool
     */
    private function filterItems( $aItem ) {

        $sKey = $aItem['message'];
        $sVal = $aItem['value'];
        $iStatus = (int)$aItem['status'];
        $sSrcVal = isset($aItem['src']) ? $aItem['src'] : '';

        if ($this->iStatusFilter != self::statusFilterAll )
            if ($this->iStatusFilter !== $iStatus) return false;

        if ($this->sSearchFilter) {
            if (!((mb_stripos( $sKey, $this->sSearchFilter )   !== false) or
                  (mb_stripos( $sVal, $this->sSearchFilter )   !== false) or
                  (mb_stripos( $sSrcVal, $this->sSearchFilter) !== false)))
            return false;
        }

        return true;

    }


    /**
     * Форма редактирования данных по языку
     */
    protected function actionEditLang() {
        $sLang = $this->getInDataVal('name', $this->getInnerData('currentLanguage'));
        $oLang = Languages::getByName( $sLang );
        if (!$oLang)
            throw new UserException(\Yii::t('languages', 'error_lang_name_not_found', [$sLang]));

        $this->editLangForm($oLang);
    }


    /**
     * Форма редактирования языка
     * @param models\Language $oLang
     * @param string $save
     */
    private function editLangForm( models\Language $oLang = null, $save = 'updLang' ){

        $oForm = UI\StateBuilder::newEdit();

        if ($oLang->id){
            $oForm->fieldHide('name', \Yii::t('languages', 'field_lang_name'));
            $oForm->field('name_show', \Yii::t('languages', 'field_lang_name'), 's', 'show');
        }else{
            $oForm->fieldString('name', \Yii::t('languages', 'field_lang_name'));
        }

        $oForm
            ->fieldString('title', \Yii::t('languages', 'field_lang_title'))
            ->field('icon', \Yii::t('languages', 'field_lang_icon'), 's', 'imagefile');

        if (!$oLang->id) {
            $oForm->fieldSelect('src_lang', \Yii::t('languages', 'field_lang_src_lang'), ArrayHelper::map(Languages::getAll(), 'name', 'title'));
        }else{
            $oForm->fieldString('src_lang', \Yii::t('languages', 'field_lang_src_lang'), ['disabled' => true]);
        }

        $oForm
            ->field('active', \Yii::t('languages', 'field_lang_active'), 'i', 'check', ['disabled' => true])
            ->field('admin', \Yii::t('languages', 'field_lang_admin'), 'i', 'check')
        ;

        $aData = $oLang->getAttributes();
        $aData['name_show'] = $aData['name'];

        $oForm->setValue($aData);

        $oForm->addButtonSave($save);

        $this->setInnerData('currentLanguage', $oLang->name);
        if ($oLang->id && !$oLang->active) {
            $oForm->button(\Yii::t('languages', 'addBranch'), 'addBranch', 'icon-add');
        }else{
            if (!$oLang->isNewRecord)
                $oForm->buttonConfirm(\Yii::t('languages', 'deleteBranch'), 'deleteBranch', 'icon-delete', \Yii::t('languages', 'deleteBranchConfirm'));
        }

        $oForm->addButtonCancel('init');

        $this->setInterface($oForm->getForm());
    }


    /**
     * Сохранение нового ключа
     * @return void
     */
    protected function actionSaveNewKey() {
        $this->actionSaveKey(false);
        $this->actionShowKeys();
    }

    /**
     * Сохранение ключа
     * @param bool $reload
     * @throws UserException
     */
    protected function actionSaveKey($reload = true) {

        $sCategory = $this->getInDataVal( 'category' );
        $sMessage = $this->getInDataVal( 'message' );
        $sLanguage = $this->getInDataVal( 'language' );

        if (!$sCategory)
            throw new UserException(\Yii::t('languages', 'error_fields_not_found', [\Yii::t('languages', 'field_category')]));

        if (!$sMessage)
            throw new UserException(\Yii::t('languages', 'error_fields_not_found', [\Yii::t('languages', 'field_message')]));

        if (!$sLanguage)
            throw new UserException(\Yii::t('languages', 'error_fields_not_found', [\Yii::t('languages', 'field_lang')]));


        $oRow = Messages::getByName($sCategory, $sMessage, $sLanguage);

        if (!$oRow){
            $oRow = new models\LanguageValues();
        }
        $oRow->setAttributes($this->getInData());

        // если запись уже существует
        if ($this->get("status") == "newRow"){
            $oRow->override = 1;
            $oRow->status = models\LanguageValues::statusTranslated;
            $oRow->save();
            \Yii::$app->getI18n()->clearCache();
        }
        else {

            $oRow->status = models\LanguageValues::statusTranslated;
            $oRow->override = 1;

            if (!$oRow->language) {
                $oRow->language = $this->language;
            }

            $oRow->save();
            \Yii::$app->getI18n()->clearCache();

        }

        if ($reload){

            $aRow = $oRow->getAttributes();

            $oListVals = new \ExtListRows();

            $oListVals->setSearchField( ['category', 'message'] );

            $oListVals->addDataRow( $aRow );

            $oListVals->setData( $this );

        }

    }

    /**
     * Изменяет данные для языка
     */
    protected function actionUpdLang() {

        $name = $this->getInDataVal('name');
        $oLang = Languages::getByName($name);

        if (!$oLang)
            $oLang = new models\Language();

        $oLang->setAttributes($this->getInData());

        if ($oLang->save()) {
            $this->addMessage(\Yii::t('editor', 'message_saved'));
            $this->language = $oLang->name;
        } else {
            $this->addError(\Yii::t('adm', 'error').': <br>'.implode('<br>', ArrayHelper::getColumn($oLang->getErrors(), '0')));
        }

        $this->actionInit();

    }

    /**
     * Добавление языка
     * @return void
     */
    protected function actionNewLang() {

        $this->editLangForm( new models\Language(), 'addLang' );

    }

    /**
     * Удаляет перекрытие
     * @return void
     */
    protected function actionUnsetOverride() {

        $oRow = Messages::getOrExcept(
            $this->getInDataVal('category'),
            $this->getInDataVal('message'),
            $this->getInDataVal('language')
        );
        $oRow->override = 0;
        $oRow->save();

        //$this->actionShowKeys();
        $aRow = $oRow->getAttributes();

        $oListVals = new \ExtListRows();

        $oListVals->setSearchField( ['category', 'message'] );

        $oListVals->addDataRow( $aRow );

        $oListVals->setData( $this );

    }

    /**
     * Добавление нового языка
     * @return bool
     * @throws UserException
     */
    protected function actionAddLang() {

        $sLang = $this->getInDataVal('name');

        if (!$sLang)
            throw new UserException(\Yii::t('languages', 'empty_prefix'));

        if (!ctype_alpha($sLang))
            throw new UserException(\Yii::t('languages', 'only_latin'));

        if (Languages::getByName($sLang))
            throw new UserException(\Yii::t('languages', 'language_exists'));

        $oLang = new models\Language();
        $oLang->setAttributes($this->getInData());

        if (!$oLang->save())
            throw new UserException('Ошибка при сохранении нового языка'); //Перевод?

        $this->actionInit();

    }

    protected function actionDelKey(){

        $sCategory = $this->getInDataVal("category");
        $sMessage = $this->getInDataVal("message");
        $sLanguage = $this->getInDataVal("language");

        Messages::delete($sCategory, $sMessage, $sLanguage);

        $this->actionShowKeys();

    }

    /**
     * Удаляет язык
     * @throws UserException
     */
    protected function actionDelLang() {

        $iTpl = $this->getInDataValInt('id');
        /** @var \skewer\build\Component\I18N\models\Language $oLang */
        $oLang = \skewer\build\Component\I18N\models\Language::findOne(['id' => $iTpl]);
        if (!$oLang)
            throw new UserException(\Yii::t('languages', 'error_lang_not_found'));

        if ($oLang->active)
            throw new UserException(\Yii::t('languages', 'error_lang_is_active'));

        $oLang->delete();

        $this->actionInit();
    }

    /**
     * Добавление нового ключа
     * @return void
     */
    protected function actionAddKey() {

        $language = $this->getStr("language");

        $oParameters = new models\LanguageValues();
        $oParameters->value = '';
        $oParameters->language = $language;
        $oParameters->override = models\LanguageValues::overrideYes;
        $oParameters->status = models\LanguageValues::statusTranslated;

        $oFormBuilder = UI\StateBuilder::newEdit();

        $oFormBuilder
            ->fieldString('category', \Yii::t('languages', 'field_category'))
            ->fieldString('message', \Yii::t('languages', 'field_message'))
            ->fieldString('value', \Yii::t('languages', 'field_value'))
            ->fieldString('language', \Yii::t('languages', 'field_language'))
        ;

        $oFormBuilder->setValue($oParameters);

        $this->setInterface($oFormBuilder->getForm());

        // добавляем элементы управления
        $oFormBuilder->button(\Yii::t('adm','save'), 'saveNewKey', 'icon-save','init', [
            'addParams' => ['status'=>'newRow']
        ]);

        $oFormBuilder->button(\Yii::t('adm','back'), 'showKeys', 'icon-cancel');

        $this->setInterface($oFormBuilder->getForm());
    }

    /**
     * Интерфейс добавления языковой ветки
     */
    protected function actionAddBranch(){

        $oForm = UI\StateBuilder::newEdit();

        $aLanguages = ArrayHelper::map(Languages::getAllNotActive(),
            'name',
            'title'
        );

        $aCopy = ArrayHelper::map(Languages::getAllActive(), 'name', function($aLang){
            return \Yii::t('languages', 'copy_lang', $aLang['title']);
        });

        $oForm
            ->fieldSelect('lang', \Yii::t('languages', 'field_lang'), $aLanguages)
            ->fieldSelect('source', \Yii::t('languages', 'field_source'), $aCopy )
            ->fieldCheck('copy', \Yii::t('languages', 'field_copy') )
        ;

        $aParams = [];

        $current = $this->getInnerData('currentLanguage');

        if ($current)
            $aParams['lang'] = $current;

        $oForm->setValue($aParams);

        $oForm->addButtonSave('saveBranch');
        $oForm->addButtonCancel($current ? 'editLang': 'init');

        $this->setInterface( $oForm->getForm() );
    }

    /**
     * Сохранить языковую ветку
     * @throws \Exception
     * @throws UserException
     */
    public function actionSaveBranch(){

        $aData = $this->getInData();

        if (!isset($aData['lang']))
            throw new UserException(\Yii::t('languages', 'error_empty_lang'));
        if (!isset($aData['source']))
            throw new UserException(\Yii::t('languages', 'error_empty_copy'));

        Api::addBranch($aData['lang'], $aData['source'], $aData['copy']);

        $this->actionInit();

    }

    /**
     * Удалить языковую ветку
     * @throws \Exception
     * @throws UserException
     */
    public function actionDeleteBranch(){

        $sLang = $this->getInnerData('currentLanguage');

        if (!$sLang)
            throw new UserException(\Yii::t('languages', 'error_empty_lang'));

        if ($sLang == \SysVar::get('language'))
            throw new UserException(\Yii::t('languages', 'error_delete_system_lang_branch'));

        Api::deleteBranch($sLang);

        $this->actionInit();

    }

    /**
     * Список языковых веток
     * @todo пока не используется, не знаю, где это можно вывести в этом модуле
     */
    public function actionBranchList(){

        $aLangs = Languages::getAllActive();

        if (!count($aLangs))
            $sMsg = \Yii::t('languages', 'not_threads');
        else{
            $sMsg = \Yii::t('languages', 'list_threads');
            foreach( $aLangs as $language ){
                $sMsg .= '<br>' . $language['title'];
            }
        }

        $oInterface = new \ExtShow();

        $oInterface->setAddText($sMsg);

        if (Languages::getCountNotActiveLanguage())
            $oInterface->addBtnAdd('add');

        $this->setInterface( $oInterface );

    }

    /**
     * Форма установки языка по умолчанию
     */
    public function actionDefaultLanguage(){

        $oForm = UI\StateBuilder::newEdit();

        $aLanguages = Languages::getAll();

        $aAdmLanguages = array_filter($aLanguages, function($lang){
           return $lang['admin'];
        });
        $aAdmLanguages = ArrayHelper::map($aAdmLanguages, 'name', 'title');

        $aActiveLanguages = Languages::getAllActive();
        if (count($aActiveLanguages))
            $aActiveLanguages = $aLanguages ;
        $aActiveLanguages = ArrayHelper::map($aActiveLanguages, 'name', 'title');

        if (count(Languages::getAllActive()) == 1) {
            $oForm->fieldSelect('language', \Yii::t('languages', 'select_language'), $aActiveLanguages);
        }
        $oForm->fieldSelect('admin_language', \Yii::t('languages', 'select_admin_language'), $aAdmLanguages);

        $oForm->setValue([
            'language' => \SysVar::get('language'),
            'admin_language' => \SysVar::get('admin_language'),
        ]);

        $oForm->button(\Yii::t('adm', 'save'), 'preSaveDefault', 'icon-save');
        $oForm->button(\Yii::t('adm', 'cancel'), 'init', 'icon-cancel');

        $this->setInterface($oForm->getForm());

    }

    /**
     * Пре-сохранение
     */
    public function actionPreSaveDefault(){

        $aData = $this->getInData();
        
        $this->setInnerData('data', $aData);
        if (isset($aData['language']) && $aData['language'] != \Yii::$app->language){

            if (count(Languages::getAllActive()) == 1){
                /** Язык один и он меняется */

                $oForm = UI\StateBuilder::newEdit();

                /**
                 * @todo а вот если бы не extJs, все бы сделали в одном интерфейсе в интерактивном режиме
                 */

                $oForm->fieldCheck('moduleParams', \yii::t('languages', 'moduleParams'));
                if ((new \skewer\build\Component\Installer\Api())->isInstalled('Goods', \Layer::CATALOG)) {
                    $oForm->fieldCheck('translateCard', \yii::t('languages', 'translateCard'));
                }

                $oForm->button(\Yii::t('adm', 'save'), 'saveAndReloadDefault', 'icon-save');
                $oForm->button(\Yii::t('adm', 'cancel'), 'defaultLanguage', 'icon-cancel');

                $oForm->setValue([]);

                $this->setInterface($oForm->getForm());
            }
        }else{
            $this->actionSaveDefault();
        }

    }

    /**
     * Сохранение языков по умолчанию
     */
    public function actionSaveDefault(){

        $aData = $this->getInnerData('data');

        if ($aData['admin_language'] != \SysVar::get('admin_language'))
            \Yii::$app->i18n->admin->setLang( $aData['admin_language'] );

        \SysVar::set('admin_language', $aData['admin_language']);

        $this->fireJSEvent('reload');

        $this->actionInit();

    }

    /**
     * Сохранение языков по умолчанию со сменой языка
     * @throws \Exception
     * @throws UserException
     * @throws null
     */
    public function actionSaveAndReloadDefault(){

        $aData = array_merge($this->getInData(), $this->getInnerData('data'));

        if (!isset($aData['language']))
            throw new UserException(\Yii::t('languages', 'error_empty_lang'));

        Api::swichLanguage(\Yii::$app->language, $aData['language'], $aData);

        \SysVar::set('admin_language', $aData['admin_language']);
        \Yii::$app->i18n->admin->setLang($aData['admin_language']);

        /**
         * нужно перезагрузить страницу, иначе в \Yii::$app->language останется старый язык.
         */

        $this->fireJSEvent( 'reload' );

    }

}