<?php

namespace skewer\build\Tool\Languages;


use skewer\build\Component\Command\Hub;
use skewer\build\Component\I18N\command\AddBranch\ActivateLanguage;
use skewer\build\Component\I18N\command\AddBranch\CategoryViewer;
use skewer\build\Component\I18N\command\AddBranch\CopySections;
use skewer\build\Component\I18N\command\AddBranch\CreateRootSection;
use skewer\build\Component\I18N\command\AddBranch\LanguageParams;
use skewer\build\Component\I18N\command\AddBranch\LinkSection;
use skewer\build\Component\I18N\command\AddBranch\ModuleParams;
use skewer\build\Component\I18N\command\AddBranch\OrderForm;
use skewer\build\Component\I18N\command\AddBranch\OrderStatus;
use skewer\build\Component\I18N\command\AddBranch\ParamSettings;
use skewer\build\Component\I18N\command\AddBranch\RedirectMain;
use skewer\build\Component\I18N\command\AddBranch\SearchUpdate;
use skewer\build\Component\I18N\command\AddBranch\ServiceSection;
use skewer\build\Component\I18N\command\DeleteBranch\DeactivateLanguage;
use skewer\build\Component\I18N\command\DeleteBranch\DeleteSections;
use skewer\build\Component\I18N\command\SwitchLanguage;
use skewer\build\Component\I18N\models;
use skewer\build\Component\Section\Page;

use skewer\build\Component\Site\Type;
use \skewer\build\Component\I18N\command\DeleteBranch;

class Api {

    public static function addBranch($sLanguage, $copy, $bCopySection){

        /** @var models\Language $oLanguage */
        $oLanguage = models\Language::findOne(['name' => $sLanguage]);

        if (!$oLanguage)
            throw new \Exception(\Yii::t('languages', 'error_lang_not_found'));

        if ($oLanguage->active)
            throw new \Exception(\Yii::t('languages', 'error_lang_is_used'));

        /** @var models\Language $oSource */
        $oSource = models\Language::findOne(['name' => $copy]);

        if (!$oSource)
            throw new \Exception(\Yii::t('languages', 'error_src_language_not_found'));

        $oHub = new Hub();

        $langRootSrc = \Yii::$app->sections->getValue(Page::LANG_ROOT, $oSource->name);

        if (!$langRootSrc || $langRootSrc == \Yii::$app->sections->root()){
            /** Чистый сайт - добавим языковую ветку на существующие разделы */
            $oCommand = new CreateRootSection($oSource, null);
            $oCommand->allInSection = true;
            $oCommand->noDelete = true;
            $oHub->addCommand($oCommand);

            /** Скопируем параметры из настроек сайта */
            $oHub->addCommand(new LanguageParams($oSource, null));

            /** Перенос параметров */
            $oCommand = new ParamSettings($oSource, null);
            $oCommand->sourceSection = \Yii::$app->sections->root();
            $oCommand->useTranslate = false;
            $oCommand->copy = false;
            $oHub->addCommand($oCommand);
        }

        /** Создание главного  раздела языковой ветки */
        $oHub->addCommand(new CreateRootSection($oLanguage, $oSource));
        /** Копирование ветки разделов */
        $oCommandCopy = new CopySections($oLanguage, $oSource);
        if ($bCopySection)
            $oCommandCopy->bAllCopy = true;
        $oHub->addCommand($oCommandCopy);
        /** Скопируем параметры из настроек сайта */
        $oHub->addCommand(new LanguageParams($oLanguage, $oSource));
        /** Ссылки разделов */
        $oHub->addCommand(new LinkSection($oLanguage, $oSource));
        /** Разводка категорий */
        $oHub->addCommand(new CategoryViewer($oLanguage, $oSource));
        /** Редиректы на главных страницах */
        $oHub->addCommand(new RedirectMain($oLanguage, $oSource));
        /** Параметры модулей */
        $oHub->addCommand(new ModuleParams($oLanguage, $oSource));
        /** Настройка параметров */
        $oHub->addCommand(new ParamSettings($oLanguage, $oSource));

        /** Каталожный сайт */
        if (Type::hasCatalogModule()){
            /** Скопируем форму заказа! */
            $oHub->addCommand(new OrderForm($oLanguage, $oSource));
        }

        if (Type::isShop()){
            /** Переведем статусы заказа! */
            $oHub->addCommand(new OrderStatus($oLanguage, $oSource));
        }

        /** Системные разделы */
        $oHub->addCommand(new ServiceSection($oLanguage, $oSource));
        /** Сброс поиска */
        $oHub->addCommand(new SearchUpdate($oLanguage, $oSource));
        /**
         * это должна быть последняя команда!
         */
        /** Активация языка */
        $oHub->addCommand(new ActivateLanguage($oLanguage, null));

        $oHub->executeOrExcept();

    }


    /**
     * Удаление языковой ветки
     * @param string $sLanguage
     * @throws \Exception
     */
    public static function deleteBranch( $sLanguage ){

        /** @var models\Language $oLanguage */
        $oLanguage = models\Language::findOne(['name' => $sLanguage]);

        if (!$oLanguage)
            throw new \Exception(\Yii::t('languages', 'error_lang_not_found'));

        if (!$oLanguage->active)
            throw new \Exception(\Yii::t('languages', 'error_lang_is_not_used'));

        $oHub = new Hub();

        $oHub->addCommand(new DeleteSections($oLanguage));
        $oHub->addCommand(new DeleteBranch\ModuleParams($oLanguage));
        $oHub->addCommand(new DeleteBranch\ServiceSection($oLanguage));
        $oHub->addCommand(new DeleteBranch\OrderStatus($oLanguage));
        $oHub->addCommand(new DeleteBranch\SearchUpdate($oLanguage));

        $oHub->addCommand(new DeactivateLanguage($oLanguage));

        $oHub->executeOrExcept();

    }

    /**
     * Смена языка
     * @param $sOldLanguage
     * @param $sNewLanguage
     * @param $aParams
     */
    public static function swichLanguage($sOldLanguage, $sNewLanguage, $aParams){

        $oHub = new Hub();

        $oInstaller = new \skewer\build\Component\Installer\Api();

        /** Перепись системных разделов */
        $oHub->addCommand(new SwitchLanguage\ServiceSections($sOldLanguage, $sNewLanguage));

        /** Перепись параметров модулей */
        if (isset($aParams['moduleParams']) && $aParams['moduleParams'])
            $oHub->addCommand(new SwitchLanguage\ModuleParams($sOldLanguage, $sNewLanguage));

        /** Сброс поиска */
        $oHub->addCommand(new SwitchLanguage\Search());

        /** Статусы заказов */
        if ($oInstaller->isInstalled('Order', \Layer::TOOL)){
            $oHub->addCommand(new SwitchLanguage\OrderStatus($sOldLanguage, $sNewLanguage));
        }

        /** Карточка каталога */
        if ($oInstaller->isInstalled('Goods', \Layer::CATALOG)){
            if (isset($aParams['translateCard']) && $aParams['translateCard']){
                $oHub->addCommand(new SwitchLanguage\CatalogCard($sOldLanguage, $sNewLanguage));
            }
        }

        /** Смена языка. Эта команда должна быть последней */
        $oHub->addCommand(new SwitchLanguage\SwichLanguage($sOldLanguage, $sNewLanguage));

        $oHub->executeOrExcept();

    }

} 