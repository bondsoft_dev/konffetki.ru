<?php

namespace skewer\build\Tool\SearchSettings;

use skewer\build\Component\Search;
use skewer\build\Component\Section\Parameters;
use skewer\build\Component\Site\Type;
use skewer\build\Component\UI\StateBuilder;
use skewer\build\Tool;
use skewer\build\Page\CatalogViewer;

/**
 * Модуль Настройки поиска
 * Class Module
 * @package skewer\build\Tool\SearchSettings
 */
class Module extends Tool\LeftList\ModulePrototype {

    protected function actionInit() {

        $oForm = StateBuilder::newEdit();

        $iType = (int)\SysVar::get('Search.default_type');

        $oForm->addSelectField('type', \Yii::t('search', 'title_default_type'), 'i',  Search\Type::getSearchTypeList() );

        if ( Type::hasCatalogModule() ){
            $oForm->addSelectField( 'search_type', \Yii::t('search', 'search_type'), 'i', Search\Type::getTypeList() );
            $oForm->addSelectField( 'tpl_name', \Yii::t('search', 'typeTpl'), 'string', CatalogViewer\State\ListPage::getTemplates()  );
        }

        $oForm
            ->addField( 'showTypeSelect', \Yii::t('search', 'showTypeSelect'), 'i', 'check')
            ->addField( 'showSectionSelect', \Yii::t('search', 'showSectionSelect'), 'i', 'check');

        $oForm->setValue(array(
                'type' => $iType,
                'search_type' => \SysVar::get('Search.search_type'),
                'showSort' => \SysVar::get('Search.showSort'),
                'tpl_name' => \SysVar::get('Search.CatalogListTemplate'),
                'showTypeSelect' => \SysVar::get('Search.showTypeSelect'),
                'showSectionSelect' => \SysVar::get('Search.showSectionSelect'),
            )
        );

        $oForm->addButtonSave('save');
        $oForm->addButtonCancel('init');

        $this->setInterface($oForm->getForm());
    }

    /**
     * Сохранение
     */
    protected function actionSave(){
        \SysVar::set('Search.default_type', $this->getInDataVal('type'));
        if ( Type::hasCatalogModule() ){
            \SysVar::set('Search.search_type', (int)$this->getInDataVal('search_type'));

            \SysVar::set('Search.CatalogListTemplate', $this->getInDataVal('tpl_name'));
            $this->setParams( 'sCatalogListTemplate', $this->getInDataVal('tpl_name') );
            $this->setParams( 'search_type', (int)$this->getInDataVal('search_type') );
        }

        \SysVar::set('Search.showTypeSelect', (bool)$this->getInDataVal('showTypeSelect'));
        \SysVar::set('Search.showSectionSelect', (bool)$this->getInDataVal('showSectionSelect'));

        $this->setParams( 'showTypeSelect', (bool)$this->getInDataVal('showTypeSelect') );
        $this->setParams( 'showSectionSelect', (bool)$this->getInDataVal('showSectionSelect') );

        $this->actionInit();
    }

    /**
     * Устанавливает параметр во все поисковые разделы
     * @param $sParamName
     * @param $value
     */
    private function setParams( $sParamName, $value){

        $aSearchParam = Parameters::getListByModule('Search', 'content');

        if ($aSearchParam){

            foreach( $aSearchParam as $iParam ){
                Parameters::setParams( $iParam, 'content', $sParamName, $value);
            }
        }
    }

} 