<?php

$aLanguage = array();

$aLanguage['ru']['SearchSettings.Tool.tab_name'] = 'Поиск';
$aLanguage['ru']['title_default_type'] = 'Критерий поиск по умолчанию';
$aLanguage['ru']['search_type'] = 'Тип поиска';
$aLanguage['ru']['type_info'] = 'Информационный';
$aLanguage['ru']['type_catalog'] = 'Каталожный';
$aLanguage['ru']['type_all'] = 'Общий';
$aLanguage['ru']['catalogSearch'] = 'Поиск по каталогу';
$aLanguage['ru']['showTypeSelect'] = 'Критерии поиска';
$aLanguage['ru']['showSectionSelect'] = 'Раздел для поиска';
$aLanguage['ru']['typeTpl'] = 'Тип вывода товаров';

$aLanguage['en']['SearchSettings.Tool.tab_name'] = 'Search';
$aLanguage['en']['title_default_type'] = 'Default search criterium';
$aLanguage['en']['search_type'] = 'Search type';
$aLanguage['en']['type_info'] = 'Information';
$aLanguage['en']['type_catalog'] = 'Catalog';
$aLanguage['en']['type_all'] = 'General';
$aLanguage['en']['catalogSearch'] = 'Search the catalog';
$aLanguage['en']['showTypeSelect'] = 'Search criteria';
$aLanguage['en']['showSectionSelect'] = 'Section for search';
$aLanguage['en']['typeTpl'] = 'Items output type';

return $aLanguage;