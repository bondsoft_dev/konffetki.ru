<?php

namespace skewer\build\Tool\Auth;

use skewer\build\Adm;
use skewer\build\Tool;


/**
 * Проекция редактора баннеров для слайдера в панель управления
 * Class Module
 * @package skewer\build\Adm\Auth
 */
class Module extends Adm\Auth\Module implements Tool\LeftList\ModuleInterface {

    /**
     * @inheritDoc
     */
    public function init()
    {
        Tool\LeftList\ModulePrototype::updateLanguage();
        parent::init();
    }

    public function __construct( \skContext $oContext ) {
        parent::__construct( $oContext );

        $oContext->setModuleWebDir('/skewer/build/Adm/Auth');
        $oContext->setModuleDir(RELEASEPATH.'build/Adm/Auth');

        $oContext->setModuleLayer('Adm');


    }

    function getName() {
        return $this->getModuleName();
    }

    /**
     * @todo не используется?
     */
    protected function getWorkMode() {
        return self::tool;
    }

}