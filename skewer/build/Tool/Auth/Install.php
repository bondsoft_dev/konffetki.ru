<?php

namespace skewer\build\Tool\Auth;


use skewer\build\Adm\Auth\ar\Users;
use skewer\build\Component\I18N\Languages;
use skewer\build\Component\I18N\ModulesParams;
use skewer\build\Component\Section\Tree;
use skewer\build\Component\Section\Visible;
use skewer\build\Component\Site;
use yii\helpers\ArrayHelper;

class Install extends \skModuleInstall {

    private $moduleParamKeys = [
        'mail_activate',
        'mail_close_ban',
        'mail_banned',
        'mail_admin_activate',
        'mail_user_activate',
        'mail_reset_password',
        'mail_title_admin_newuser',
        'mail_title_user_newuser',
        'mail_title_reset_password',
        'mail_title_mail_activate',
        'mail_title_mail_close_banned',
        'mail_title_mail_banned',
        'reg_license',
        'mail_title_new_pass',
        'mail_new_pass',
    ];

    private $languages;

    public function init() {

        $this->languages = ArrayHelper::map(Languages::getAllActive(), 'name', 'name');

        return true;
    }// func

    public function install() {

        /** Установка параметров модуля */
        $this->setModuleParams();

        /** Установка параметров в шаблоны */
        $this->setTemplateParams();

        /** Создание разделов авторизации */
        $this->createAuthSections();

        /** перестройка таблиц */
        Users::rebuildTable();

        /** тип активации по умолчанию */
        \SysVar::set( 'auth.activate_status', 2 );

        return true;
    }// func

    public function uninstall() {

        foreach (\Yii::$app->sections->getValues('auth') as $value){
            Tree::removeSection($value);
        }

        ModulesParams::deleteByModule( 'auth' );

        return true;
    }

    protected function setModuleParams()
    {
        foreach ($this->languages as $lang) {
            foreach ($this->moduleParamKeys as $key) {
                ModulesParams::setParams('auth', $key, $lang, \Yii::t('data/auth', $key, [], $lang));
            }
        }
    }

    /**
     * @throws \UpdateException
     */
    protected function setTemplateParams()
    {
        $iNewPageSection = \Yii::$app->sections->tplNew();
        $this->addParameter($iNewPageSection, 'layout', 'left,right', '', 'auth');
        $this->addParameter($iNewPageSection, 'mini_auth', '1', '', 'auth');
        $this->addParameter($iNewPageSection, 'object', 'Auth', '', 'auth');
    }

    protected function createAuthSections()
    {
        $iNewPageSection = \Yii::$app->sections->tplNew();
        foreach (\Yii::$app->sections->getValues('tools') as $sLang => $iSection) {

            $oSection = Tree::addSection($iSection, \Yii::t('data/auth', 'auth_sections_title', [], $sLang), $iNewPageSection, 'auth', Visible::HIDDEN_FROM_MENU);
            $this->setParameter($oSection->id, 'object', 'auth', '');
            $this->setParameter($oSection->id, 'object', 'content', 'Auth');
            $this->setParameter($oSection->id, 'objectAdm', 'content', 'Auth');

            \Yii::$app->sections->setSection('auth', \Yii::t('site', 'auth', [], $sLang), $oSection->id, $sLang);

        }
    }// func

}//class
