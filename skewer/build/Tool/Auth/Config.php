<?php

use skewer\build\Tool\LeftList;

$aConfig['name']     = 'Auth';
$aConfig['title']    = 'Клиенты';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Клиенты';
$aConfig['revision'] = '0001';
$aConfig['useNamespace'] = true;
$aConfig['layer']     = \Layer::TOOL;
$aConfig['group']     = LeftList\Group::ORDER;
$aConfig['languageCategory']     = 'auth';

$aConfig['dependency'] = [
    ['Auth', \Layer::PAGE],
    ['Auth', \Layer::ADM],
    ['Profile', \Layer::PAGE],
];


return $aConfig;
