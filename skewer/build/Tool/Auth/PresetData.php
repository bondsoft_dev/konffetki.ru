<?php

$aLanguage = [];

$aLanguage['ru']['mail_title_admin_newuser'] = 'Регистрация нового пользователя';
$aLanguage['ru']['mail_admin_activate'] = '<p>Уважаемый администратор!</p>

<p>На сайте [адрес сайта] появился новый пользователь.</p>

<p>Чтобы посмотреть его перейдите по <a href="[link]">ссылке</a>.</p>';

$aLanguage['ru']['mail_title_user_newuser'] = 'Создание новой учетной записи';
$aLanguage['ru']['mail_user_activate'] = '<p>Уважаемый пользователь!</p>

<p>Ваш e-mail был указан при регистрации на сайте [адрес сайта].</p>

<p>Для активации аккаунта перейдите по <a href="[link]">ссылке</a>.</p>

<p>Если Вы не регистрировались на сайте [адрес сайта], просто проигнорируйте письмо.</p>

<p>__<br/>
С уважением, администрация сайта [адрес сайта]</p>';

$aLanguage['ru']['mail_title_mail_activate'] = 'Учетная запись активирована';
$aLanguage['ru']['mail_activate'] = '<p>Уважаемый пользователь!</p>

<p>Ваша учетная запись на сайте [адрес сайта] была активирована.</p>

<p>__<br/>
С уважением, администрация сайта [адрес сайта]</p>
';

$aLanguage['ru']['mail_title_new_pass'] = 'Ваш пароль изменен';
$aLanguage['ru']['mail_new_pass'] = 'Ваш пароль изменен';

$aLanguage['ru']['mail_title_reset_password'] = 'Сброс пароля учетной записи';
$aLanguage['ru']['mail_reset_password'] = '<p>Уважаемый пользователь!</p>

<p>Чтобы сбросить пароль, пройдите по этой <a href="[link]">ссылке</a></p>

<p>__</p>

<p>С уважением, администрация сайта [адрес сайта]</p>';

$aLanguage['ru']['mail_title_mail_banned'] = 'Учетная запись заблокирована';
$aLanguage['ru']['mail_banned'] = '<p>Уважаемый пользователь!</p>

<p>Ваша учетная запись на сайте [адрес сайта] была заблокирована.</p>

<p>__<br/>
С уважением, администрация сайта [адрес сайта]</p>
';

$aLanguage['ru']['mail_title_mail_close_banned'] = 'Учетная запись активирована';
$aLanguage['ru']['mail_close_ban'] = '<p>Уважаемый пользователь!</p>

<p>Ваша учетная запись на сайте [адрес сайта] была активирована.</p>

<p>__<br/>
С уважением, администрация сайта [адрес сайта]</p>

<p>&nbsp;</p>
';

$aLanguage['ru']['auth_sections_title'] = 'Авторизация';

$aLanguage['ru']['profile_sections_title'] = 'Личный кабинет';

$aLanguage['ru']['reg_license'] = '<h2>Соглашение об обработке персональных данных</h2>

<p>Данное соглашение об обработке персональных данных разработано в соответствии с законодательством Российской Федерации.<br />
Все граждане, заполнившие сведения, составляющие персональные данные, на сайте или на любой его странице, а также разместившие иную информацию обозначенными действиями, подтверждают свое согласие на:</p>

<ul>
	<li>подписку на электронные почтовые рассылки с информационными и рекламными целями;</li>
	<li>обработку персональных данных;</li>
	<li>передачу персональных данных оператору по их обработке.</li>
</ul>

<p><strong>Под персональными данными Гражданина понимается:</strong></p>

<ul>
	<li>общая информация (Ф.И.О);</li>
	<li>электронная почта (e-mail);</li>
	<li>номер телефона;</li>
	<li>компания и должность.</li>
</ul>

<p>Гражданин, принимая настоящее Соглашение, выражает свою заинтересованность и полное согласие, что обработка его персональных данных может включать в себя следующие действия: сбор, систематизацию, накопление, хранение, уточнение (обновление, изменение), использование, уничтожение.</p>

<p><strong>Гражданин гарантирует:</strong></p>

<ul>
	<li>информация, им предоставленная, является полной, точной и достоверной;</li>
	<li>при предоставлении информации не нарушается действующее законодательство Российской Федерации, законные права и интересы третьих лиц;</li>
	<li>вся предоставленная информация заполнена Гражданином в отношении себя лично.</li>
</ul>
';


/*** EN ***/


$aLanguage['en']['mail_title_admin_newuser'] = 'New User Registration';
$aLanguage['en']['mail_admin_activate'] = '<p> Dear administrator! </p>

<p> The site [site name], a new user. </p>

<p> To see it, go to <a href="[link]"> link </a>. </p> ';

$aLanguage['en']['mail_title_user_newuser'] = 'Create a new account';
$aLanguage['en']['mail_user_activate'] = '<p> Dear user! </p>

<p> Your e-mail has been entered after registration on the site [url]. </p>

<p> To activate your account, go to <a href="[link]"> link </a>. </p>

<p> If you did not register on the website [url], just ignore the letter. </p>

<p> __ <br/>
Sincerely, Administration of the site [url] </p> ';

$aLanguage['en']['mail_title_mail_activate'] = 'The account is enabled';
$aLanguage['en']['mail_activate'] = '<p> Dear user! </p>

<p> Your account online [url] has been activated. </p>

<p> __ <br/>
Sincerely, Administration of the site [url] </p>
';

$aLanguage['en']['mail_title_reset_password'] = 'Reset account password';
$aLanguage['en']['mail_reset_password'] = '<p> Dear user! </p>

<p> To reset your password, follow this <a href="[link]">link</a> </p>

<p> __ </p>

<p> Sincerely, Administration of the site [url] </p> ';

$aLanguage['en']['mail_title_new_pass'] = 'Your password has been changed';
$aLanguage['en']['mail_new_pass'] = 'Your password has been changed';

$aLanguage['en']['mail_title_mail_banned'] = 'The Account is disabled';
$aLanguage['en']['mail_banned'] = '<p> Dear user! </p>

<p> Your online account [url] has been blocked. </p>

<p> __ <br/>
Sincerely, Administration of the site [url] </p>
';

$aLanguage['en']['mail_title_mail_close_banned'] = 'The account is enabled';
$aLanguage['en']['mail_close_ban'] = '<p> Dear user! </p>

<p> Your online account [url] has been activated. </p>

<p> __ <br/>
Sincerely, Administration of the site [url] </p>

';

$aLanguage['en']['auth_sections_title'] = 'Authorization';

$aLanguage['en']['profile_sections_title'] = 'My account';

$aLanguage['en']['reg_license'] = "<h2> The agreement on the processing of personal data</h2>

<p> The agreement on the processing of personal data developed in accordance with the legislation of the Russian Federation. <br />
All citizens who filled the information constituting any personal data on the site or on any of its page and also post other information designated actions confirm their agreement to: </p>

<ul>
<li> subscribe to the electronic mailing list with information and advertising purposes; </li>
<li> the processing of personal data; </li>
<li> the transfer of personal data to the operator's processing. </li>
</ul>

<p> <strong> Under the personal data refers to the Citizen: </strong> </p>

<ul>
<li> general information (full name); </li>
<li> e-mail (e-mail); </li>
<li> phone number; </li>
<li> the company and position. </li>
</ul>

<p> Citizen, taking this Agreement, expressed their interest and complete agreement that the processing of personal data may include the following: the collection, systematization, accumulation, storage, clarification (update, change), use, destruction. </p >

<p> <strong> A citizen guarantees: </strong> </p>

<ul>
<li> the information they provided is complete, accurate and reliable; </li>
<li> When the information does not violate any applicable laws of the Russian Federation, the legitimate rights and interests of third parties; </li>
<li> All information provided is full citizen in respect of myself. </li>
</ul>";

return $aLanguage;