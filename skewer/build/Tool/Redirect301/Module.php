<?php

namespace skewer\build\Tool\Redirect301;


use skewer\build\Tool;
use skewer\build\Component\Redirect;

class Module extends Tool\LeftList\ModulePrototype {

    protected function preExecute() {

    }

    protected function actionInit() {

        $this->actionList();

    }

    /**
     * Список опросов
     */
    protected function actionList() {

        // установка заголовка
        $this->setPanelName( \Yii::t('redirect301', 'urlList') );

        // объект для построения списка
        $oList = new \ExtList();

        $aModel = Api::getListModel(Api::getListFields());

        $aModel['id']['listColumns'] = array('hidden' => true);
        $aModel['old_url']['listColumns'] = array('flex' => 1);
        $aModel['new_url']['listColumns'] = array('flex' => 1);

        // задать модель данных для вывода
        $oList->setFields( $aModel );

        // добавление набора данных
        $aItems = Api::getListItems();

        $oList->setValues( $aItems['items'] );

        $oList->addRowBtnArray(array(
            'tooltip' =>\Yii::t('adm','upd'),
            'iconCls' => 'icon-edit',
            'action' => 'editForm',
            'state' => 'show'
        ));
        $oList->addRowBtnDelete();

        // кнопка добавления
        //$oList->addBtnAdd('add');
        $oList->addDockedItem(array(
            'text' => \Yii::t('redirect301', 'add'),
            'iconCls' => 'icon-add',
            'state' => 'init',
            'action' => 'addForm',
        ));

        // вывод данных в интерфейс
        $this->setInterface( $oList );

    }

    public function actionAddForm(){

        $oForm = new \ExtForm();

        $this->setPanelName(\Yii::t('redirect301', 'newRedirect'),true);

        /* установить набор элементов формы */
        $aItems = Api::getListModel(Api::getListFields());

        $oForm->setFields($aItems);  //setItems
        $oForm->setValues(array());

        $oForm->addBtnSave('add');
        /*
        $oForm->addDockedItem(array(
            'text' => \Yii::t('redirect301', 'save'),
            'iconCls' => 'icon-save',
            'state' => 'allow_do',
            'action' => 'add',
            'actionText' => \Yii::t('redirect301', 'saveText'),
        ));*/

        $oForm->addBtnCancel('list');

        $this->setInterface($oForm);

        return psComplete;

    }

    public function actionAdd(){

        try {

            $aData = $this->get('data');

            //$bRes = DomainManagerApi::linkDomain($iSiteId,$iDomainId);

            $bRes = Mapper::saveItem($aData);

            if(!$bRes) throw new \Exception('Ошибка: правило не добавлено!');

            // перестроить список файл редиректов
            Redirect\Api::rebuildRedirectFile();

        } catch(\Exception $e) {
            $this->addError($e->getMessage());
        }

        // переход к списку
        $this->actionList();

        return psComplete;

    }

    public function actionDelete(){

        try {

            $aData = $this->get('data');

            $bRes = Mapper::delItem($aData['id']);

            if(!$bRes) throw new \Exception('Ошибка: не удалось удалить правило!');

            // перестроить список файл редиректов
            Redirect\Api::rebuildRedirectFile();

        } catch(\Exception $e) {
            $this->addError($e->getMessage());
        }


        // переход к списку
        $this->actionList();

        return psComplete;

    }

    public function actionEditForm(){

        $oForm = new \ExtForm();

        $aData = $this->get('data');

        $this->setPanelName(\Yii::t('redirect301', 'editUrl'),true);

        /* установить набор элементов формы */
        $aItems = Api::getListModel(Api::getListFields());

        $oForm->setFields($aItems);
        $oForm->setValues($aData);

        $oForm->addBtnSave('update');

/*        $oForm->addDockedItem(array(
            'text' => \Yii::t('redirect301', 'save'),
            'iconCls' => 'icon-save',
            'state' => 'allow_do',
            'action' => 'update',
            'actionText' => \Yii::t('redirect301', 'editText'),
        ));*/

        $oForm->addBtnCancel('list');

        $this->setInterface($oForm);

        return psComplete;

    }

    public function actionUpdate(){

        try {

            $aData = $this->get('data');

            $bRes = Mapper::saveItem($aData);

            if(!$bRes) throw new \Exception('Ошибка: шаблон не изменен!');

            // перестроить список файл редиректов
            Redirect\Api::rebuildRedirectFile();

        } catch(\Exception $e) {
            $this->addError($e->getMessage());
        }

        // переход к списку
        $this->actionList();

        return psComplete;

    }

}
