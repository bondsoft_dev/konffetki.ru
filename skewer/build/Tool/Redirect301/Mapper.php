<?php

namespace skewer\build\Tool\Redirect301;

/**
 * Класс для работы с базой адресов для редиректов на сайте
 * todo заенить на yii/AR
 * todo перенести (например в skewer\build\Component\Redirect)
 */
class Mapper extends \skMapperPrototype {

    /** @var string Имя таблицы, с которой работает маппер */
    protected static $sCurrentTable = 'redirect301';

    /** @var array Конфигурация полей таблицы */
    protected static $aParametersList = array(
        'id' => 'i:hide:Redirect301.id',
        'old_url' => 's:str:Redirect301.old_url',
        'new_url' => 's:str:Redirect301.new_url',
    );






}
