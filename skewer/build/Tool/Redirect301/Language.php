<?php

$aLanguage = array();

$aLanguage['ru']['tab_name'] = 'Управление редиректами 301';
$aLanguage['ru']['id'] = 'ID правила';
$aLanguage['ru']['old_url'] = 'Старый адрес';
$aLanguage['ru']['new_url'] = 'Новый адрес';
$aLanguage['ru']['urlList'] = 'Список адресов';
$aLanguage['ru']['add'] = 'Добавить';
$aLanguage['ru']['newRedirect'] = 'Добавление нового редиректа';
$aLanguage['ru']['save'] = 'Сохранить';
$aLanguage['ru']['saveText'] = 'Добавить новое правило?';
$aLanguage['ru']['editText'] = 'Сохранить изменения в правиле?';
$aLanguage['ru']['editUrl'] = 'Редактирование правила';


$aLanguage['en']['tab_name'] = 'Redirect 301';
$aLanguage['en']['id'] = 'ID';
$aLanguage['en']['old_url'] = 'Old URL';
$aLanguage['en']['new_url'] = 'New URL';
$aLanguage['en']['urlList'] = 'URL list';
$aLanguage['en']['add'] = 'Add';
$aLanguage['en']['newRedirect'] = 'Add new redirect';
$aLanguage['en']['save'] = 'Save';
$aLanguage['en']['saveText'] = 'Add a new rule?';
$aLanguage['en']['editText'] = 'Save the changes to the rule?';
$aLanguage['en']['editUrl'] = 'Edit rules';

return $aLanguage;