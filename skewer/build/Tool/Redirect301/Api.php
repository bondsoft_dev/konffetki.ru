<?php

namespace skewer\build\Tool\Redirect301;

use skewer\build\Component\Redirect;

class Api {

    /**
     * Набор полей для списка
     * @static
     * @return array
     */
    public static function getListFields(){
        return array('id','old_url','new_url');
    }


    /**
     * @static
     * @param $aFields
     * @return array
     */
    public static function getListModel($aFields){

        return Mapper::getFullParamDefList($aFields);
    }

    /**
     * @static
     * @return array
     */
    public static function getListItems(){

        $aFilter = array();

        return Mapper::getItems($aFilter);
    }


    /**
     * Функция пересоздает файл .htaccess в корне
     * todo должна находиться не здесь и должна быть полностью пересмотрена
     * раньше использовалась для добавления 301 редиректов напрямую в .htaccess,
     *   теперь это делается через отдельный файл
     * @return bool
     * @throws \UpdateException
     */
    public static function makeHtaccessFile(){

        /* Массив меток, подставляемых в шаблон htaccess */
        $aDomainItems = \skewer\build\Tool\Domains\Api::getRedirectItems();
        $bOpenSite = (bool)\SysVar::get('site_open');

        $oUpHalper = new \skUpdateHelper();
        $aData = array('redirectItems'=>array(),
            'buildVersion'=> BUILDVERSION,
            'buildName'	  => BUILDNAME,
            'buildNumber' => BUILDNUMBER,
            'inCluster'=>INCLUSTER,
            'USECLUSTERBUILD'=>USECLUSTERBUILD,
            'site_open' => $bOpenSite,
        );

        foreach($aDomainItems as $aItem)
            $aData['redirectDomainItems'][] = $aItem;

        /* rewrite htaccess */

        $oUpHalper->updateHtaccess(BUILDPATH.'common/templates/',$aData);

        return true;
    }
}
