<?php

$aLanguage = array();

$aLanguage['ru']['rating'] = '';  //@todo ???
$aLanguage['ru']['mail.title'] = 'Получен новый отзыв на [адрес сайта]';
$aLanguage['ru']['mail.content'] = '<p>​Уважаемый администратор!</p>

<p>​На вашем сайте [адрес сайта] появился новый отзыв.</p>

<p>​Чтобы промодерировать его, зайдите в панель управления сайтом и на вкладке &laquo;Отзывы&raquo; соответствующего раздела одобрите или отклоните этот отзыв.</p>
';

$aLanguage['ru']['mail.onNotif'] = 1;
$aLanguage['ru']['mail.notifTitleNew'] = 'Ваш отзыв на [адрес сайта]';
$aLanguage['ru']['mail.notifContentNew'] = '<p>Спасибо за ваш отзыв. После прохождения модерации он появится на сайте</p>';

$aLanguage['ru']['mail.notifTitleApprove'] = 'Ваш отзыв на [адрес сайта] одобрен';
$aLanguage['ru']['mail.notifContentApprove'] = '<p>​Уважаемый пользователь!</p>

<p>​Ваш отзыв был одобрен и размещен на сайте [адрес сайта].</p>

<p>​__<br/>
С уважением, администрация сайта [адрес сайта]</p>
';

$aLanguage['ru']['mail.notifTitleReject'] = 'Ваш отзыв на [адрес сайта] отклонен';
$aLanguage['ru']['mail.notifContentReject'] = '<p>Уважаемый пользователь!</p>

<p>Ваш отзыв был отклонен и не будет размещен на сайте [адрес сайта].</p>

<p>Приносим свои извинения.</p>

<p>__<br/>
С уважением, администрация сайта [адрес сайта]</p>';

$aLanguage['ru']['mail.catalog.title'] = 'Новый отзыв о товаре на [адрес сайта]';
$aLanguage['ru']['mail.catalog.content'] = '<p>Уважаемый администратор!<br/>
На вашем сайте [адрес сайта] появился новый отзыв о [название товара].<br/>
Чтобы промодерировать его, зайдите в панель управления сайтом.</p>';
$aLanguage['ru']['mail.catalog.onNotif'] = 1;
$aLanguage['ru']['mail.catalog.notifTitleNew'] = 'Отзыв с сайта [название сайта]';
$aLanguage['ru']['mail.catalog.notifContentNew'] = 'Спасибо за ваш отзыв. После прохождения модерации он появится на сайте';
$aLanguage['ru']['mail.catalog.notifTitleApprove'] = 'Ваш отзыв на [адрес сайта] одобрен';
$aLanguage['ru']['mail.catalog.notifContentApprove'] = '<p>Уважаемый пользователь!<br/>
Ваш отзыв был одобрен и размещен на сайте [адрес сайта].<br/>
<br/>
__<br/>
С уважением, администрация сайта [адрес сайта]</p>
';

$aLanguage['ru']['mail.catalog.notifTitleReject'] = 'Ваш отзыв на [адрес сайта] отклонен';
$aLanguage['ru']['mail.catalog.notifContentReject'] = '<p>Уважаемый пользователь!<br/>
Ваш отзыв был отклонен и не будет размещен на сайте [адрес сайта].<br/>
Приносим свои извинения.<br/>
<br/>
__<br/>
С уважением, администрация сайта [адрес сайта]​Извините, но Ваш отзыв был отклонен и не будет размещен на сайте [адрес сайта]</p>
';

/*** EN ***/

$aLanguage['en']['rating'] = ''; // @ todo ???
$aLanguage['en']['mail.title'] = 'sent a new review in the [url]';
$aLanguage['en']['mail.content'] = '<p> Dear administrator! </p>

<p> On your site [url], a new review. </p>

<p> To promoderirovat it, go to the Control Panel and site tab &laquo; Reviews &raquo; the relevant section to approve or reject this opinion. </p>
';

$aLanguage['en']['mail.onNotif'] = 1;
$aLanguage['en']['mail.notifTitleNew'] = 'Your response to [url]';
$aLanguage['en']['mail.notifContentNew'] = '<p> Thank you for your feedback. After passing the moderation it will appear on the site </p>';

$aLanguage['en']['mail.notifTitleApprove'] = 'Your review at [website address] approved';
$aLanguage['en']['mail.notifContentApprove'] = '<p> Dear user! </p>

<p> Your comment has been approved and posted on the website [url]. </p>

<p> __ <br/>
Sincerely, Administration site [sitename] </p>
';

$aLanguage['en']['mail.notifTitleReject'] = 'Your review at [url] rejected';
$aLanguage['en']['mail.notifContentReject'] = '<p> Dear user! </p>

<p> Your comment has been rejected and will not be posted on the website [url]. </p>

<p> We apologize. </p>

<p> __ <br/>
Sincerely, Administration site [url] </p> ';

$aLanguage['en']['mail.catalog.title'] = 'A new review of the product on the [url]';
$aLanguage['en']['mail.catalog.content'] = '<p> Dear administrator! <br/>
On your site [site name], a new review of [order]. <br/>
To promoderirovat it, go to Control Panel website. </p> ';
$aLanguage['en']['mail.catalog.onNotif'] = 1;
$aLanguage['en']['mail.catalog.notifTitleNew'] = 'Review site [site name]';
$aLanguage['en']['mail.catalog.notifContentNew'] = 'Thank you for your feedback. After passing the moderation it will appear on the site ';
$aLanguage['en']['mail.catalog.notifTitleApprove'] = 'Your review at [url] approved';
$aLanguage['en']['mail.catalog.notifContentApprove'] = '<p> Dear user! <br/>
Your review has been approved and posted on the website [url]. <br/>
<br/>
__ <br/>
Sincerely, Administration site [url] </p>
';

$aLanguage['en']['mail.catalog.notifTitleReject'] = 'Your review at [url] rejected';
$aLanguage['en']['mail.catalog.notifContentReject'] = '<p> Dear user! <br/>
Your review has been rejected and will not be posted on the website [url]. <br/>
We apologize. <br/>
<br/>
__ <br/>
Sincerely, Administration site [url] Sorry, Your review has been rejected and will not be posted on the website [url] </p>
';


return $aLanguage;
