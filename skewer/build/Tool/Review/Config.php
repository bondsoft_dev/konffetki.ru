<?php

use skewer\build\Tool\LeftList;

$aConfig['name']     = 'Review';
$aConfig['title']    = 'Отзывы';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Отзывы товаров';
$aConfig['revision'] = '0001';
$aConfig['useNamespace'] = true;
$aConfig['layer']     = \Layer::TOOL;
$aConfig['group']     = LeftList\Group::CONTENT;
$aConfig['languageCategory']     = 'review';

$aConfig['dependency'] = [
    ['GuestBook', \Layer::PAGE],
    ['GuestBook', \Layer::ADM],
];

return $aConfig;
