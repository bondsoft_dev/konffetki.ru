<?php

namespace skewer\build\Tool\Review;

use skewer\build\Adm;
use skewer\build\Component\I18N\Languages;
use skewer\build\Component\orm\state\StateSelect;
use skewer\build\Component\Catalog\GoodsSelector;
use skewer\build\Component\Section\Tree;
use skewer\build\Component\Section\Parameters;
use skewer\build\Tool;
use skewer\build\Component\UI;
use skewer\build\libs\ExtBuilder;
use skewer\build\Component\Site;
use skewer\build\Adm\GuestBook\ar;
use yii\base\UserException;
use yii\helpers\ArrayHelper;
use skewer\build\Component\I18N\ModulesParams;


/**
 * Проекция редактора баннеров для слайдера в панель управления
 * Class Module
 * @package skewer\build\Adm\Order
 */
class Module extends Adm\Tree\ModulePrototype implements Tool\LeftList\ModuleInterface {

    protected $sLanguageFilter = '';

    protected $iStatusFilter;
    /**
     * @var int id показываемого раздела
     */
    protected $iShowSection = 0;

    // число элементов на страницу
    protected $iOnPage = 20;

    // текущий номер страницы ( с 0, а приходит с 1 )
    protected $iPage = 0;

    /** @var array Поля настроек */
    protected $aSettingsKeys =
        [
            'rating',
            'mail.title',
            'mail.content',
            'mail.onNotif',
            'mail.notifTitleNew',
            'mail.notifContentNew',
            'mail.notifTitleApprove',
            'mail.notifContentApprove',
            'mail.notifTitleReject',
            'mail.notifContentReject'
        ];

    /** @var array Поля настроек для каталога */
    protected $aSettingsCatalogKeys =
        [
            'rating',
            'mail.catalog.title',
            'mail.catalog.content',
            'mail.catalog.onNotif',
            'mail.catalog.notifTitleNew',
            'mail.catalog.notifContentNew',
            'mail.catalog.notifTitleApprove',
            'mail.catalog.notifContentApprove',
            'mail.catalog.notifTitleReject',
            'mail.catalog.notifContentReject'
        ];

    /**
     * @inheritDoc
     */
    public function init()
    {
        Tool\LeftList\ModulePrototype::updateLanguage();
        parent::init();
    }

    /**
     * Проверяем установку параметра. Если нет, то не выводим
     * @return bool
     */
    function checkCatalogAccess(){
        return \SysVar::get('catalog.guest_book_show');
    }


    function getName() {
        return $this->getModuleName();
    }

    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt('page');

        $this->iStatusFilter = $this->get('filter_status', false );

        $sLanguage = \Yii::$app->language;
        if ($this->pageId){
            $sLanguage = Parameters::getLanguage($this->pageId);
        }

        $this->sLanguageFilter = $this->get('filter_language', $sLanguage);
    }

    function actionChangeStatus(){
        $id =  $this->getInDataValInt('id');
        $iStatus = $this->getInDataValInt('status');

        /**
         * @var ar\GuestBookRow $row
         */
        $row =  ar\GuestBook::find($id);

        if ($row){
            Api::sendMailToClient( $row, $iStatus );

            $row->status = $iStatus;
            $row->save();

            $oListVals = new \ExtListRows();
            $oListVals->setSearchField( 'id' );
            $aStatulList = static::getStatusList();

            $aData = $row->getData();
            if (isset($aData['on_main'])) $aData['on_main'] = (int) $aData['on_main'];

            $oListVals->addDataRow( array_merge($aData,array('status'=>$row->status),array('status_text'=>$aStatulList[$row->status])));
            $oListVals->setData( $this );

        }
    }

    function actionShow(){

        $oFormBuilder = new UI\StateBuilder();

        $oFormBuilder = $oFormBuilder::newEdit();

        $iId = $this->getInDataValInt('id');
        /**
         * @var ar\GuestBookRow $row
         */
        $row = ar\GuestBook::find($iId);

        if (!$row){
            $row = ar\GuestBook::getNewRow();
            $row->parent_class = '';
            $row->date_time = date('Y-m-d H:i:s');
            $row->parent = $this->get('show_section');
        }

        $oFormBuilder->addField('id', 'ID', 'i', 'hide' )
            ->addField('link',\Yii::t('review', 'field_link'),'s','show')
            ->addField('status_text', \Yii::t('review', 'field_status'), 's', 'show')
            ->addField('status', \Yii::t('review', 'field_status'), 'i', 'hide')
            ->addField('parent', 'parent', 's', 'hide');

        if (ModulesParams::getByName('review', 'rating'))
            $oFormBuilder->addSelectField('rating', \Yii::t('review', 'field_rating'), 'i', [0, 1, 2, 3, 4, 5]);

        if ($this->checkCatalogAccess())
            $oFormBuilder->addField('type', \Yii::t('review', 'field_type'), 's', 'show');

        $oFormBuilder->addField('date_time', \Yii::t('review', 'field_date_time'), 's', 'datetime')
            ->addField('name', \Yii::t('review', 'field_name'), 's', 'string')
            ->addField('email', \Yii::t('review', 'field_email'), 's', 'string')
            ->addField('city', \Yii::t('review', 'field_city'), 's', 'string')
            ->addField('content', \Yii::t('review', 'field_content'), 's', 'wyswyg')

        ;

        $oFormBuilder->addButton(\Yii::t('adm','save'),'save','icon-save','init');
        $oFormBuilder->addButton(\Yii::t('adm','back'),'init','icon-cancel','init');

        $aItem = $row->getData();
        self::parseData($aItem);

        $bShowButtonApprove = false;
        $bShowButtonReject = false;

        switch($row->status){
            case ar\GuestBook::statusNew:
                $bShowButtonApprove = true;
                $bShowButtonReject  = true;
                break;
            case ar\GuestBook::statusApproved:
                $bShowButtonReject  = true;
                break;
            case ar\GuestBook::statusRejected:
                $bShowButtonApprove = true;
                break;
        }

        $oFormBuilder->addBtnSeparator('-');

        if ($bShowButtonApprove)
            $oFormBuilder->addButton(\Yii::t('review', 'approve'),'save','icon-commit','init',
                array(
                    'unsetFormDirtyBlocker'=>true,
                    'addParams'=>array(
                        'data'=>array(
                            'status_new' => ar\GuestBook::statusApproved
                        )
                    )
                )
            );

        if ($bShowButtonReject)
            $oFormBuilder->addButton(\Yii::t('review', 'reject'), 'save','icon-stop','init',
                array(
                    'unsetFormDirtyBlocker'=>true,
                    'addParams'=>array(
                        'data'=>array(
                            'status_new' => ar\GuestBook::statusRejected
                        )
                    )
                )
            );
        // кнопка удаления
        $oFormBuilder->addBtnSeparator('->');
        $oFormBuilder->addButton(\Yii::t('adm','del'),'delete','icon-delete','init');

        /**
         * @TODO так как виджет отключен для ExtForm и через addWidget это не сделать, добавил костыль
         */
        $aItem['status_text'] = $this::getStatusValue($aItem,'');

        $oFormBuilder->setValue($aItem);

        $this->setInterface($oFormBuilder->getForm());

        return psComplete;
    }

    function actionSave(){

        // запросить данные
        $aData = $this->get('data');
        $iId = $this->getInDataValInt('id');
        $iStatus = $this->getInDataValInt('status_new', -1);

        if ($iStatus >= 0){
            $aData['status'] = $iStatus;
        }

        if (!$aData)
            throw new UserException('Empty data');

        if ($iId) {
            /**
             * @var ar\GuestBookRow $oRow
             */
            $oRow = ar\GuestBook::find($iId);

            if (!$oRow)
                throw new UserException("Запись [$iId] не найдена");

            Api::sendMailToClient( $oRow, $aData['status'] );

            $oRow->setData($aData);
        } else {
            $oRow = ar\GuestBook::getNewRow($aData);
            /**
             * @todo почему незаполненная дата прихотит как string 'null' ?
             */
            if ( !$oRow->date_time || $oRow->date_time === 'null')
                $oRow->date_time = date('Y-m-d H:i:s');
        }

        $oRow->save();
        $this->actionInit();
    }

    public static function getStatusList(){

        return array(
            0 => \Yii::t('review', 'field_status_new'),
            1 => \Yii::t('review', 'field_status_approve'),
            2 => \Yii::t('review', 'field_status_reject')
        );
    }


    static function getStatusValue($oItem, $sField){
        $aStatulList = static::getStatusList();

        if (isset($aStatulList[$oItem['status']])){
            return $aStatulList[$oItem['status']];
        } else return '';
    }

    function actionInit(){

        // -- сборка интерфейса
        $oFormBuilder = new UI\StateBuilder();
        // создаем форму

        $oFormBuilder = $oFormBuilder::newList();
        $oFormBuilder->addField('id', 'ID', 'i', 'string',array( 'listColumns' => array('flex' => 1) ) )
            ->addField('name', \Yii::t('review', 'field_name'), 's', 'string',array( 'listColumns' => array('flex' => 3) ))
            ->addField('date_time', \Yii::t('review', 'field_date_time'), 's', 'date',array( 'listColumns' => array('flex' => 4) ))
            //->addField('email', \Yii::t('review', 'field_email'), 's', 'string',array( 'listColumns' => array('flex' => 3) ))
            ->addField('content', \Yii::t('review', 'field_content'), 's', 'string',array( 'listColumns' => array('flex' => 10) ));

        if (!$this->iShowSection && $this->checkCatalogAccess())
            $oFormBuilder->addField('type', \Yii::t('review', 'field_type'), 's', 'string',array( 'listColumns' => array('flex' => 2) ));

        $oFormBuilder->addField('link',\Yii::t('review', 'field_link'),'s','show',array( 'listColumns' => array('flex' => 5) ))
            ->addField('status_text', \Yii::t('review', 'field_status'), 's', 'string',array( 'listColumns' => array('flex' => 2) ))

            ->addRowCustomBtn('ApproveBtn')
            ->addRowCustomBtn('RejectBtn')

            ->addButton(\Yii::t('review', 'settings'),'settings','icon-edit','init')
            ->addWidget( 'status_text', 'skewer\\build\\Tool\\Review\\Module', 'getStatusValue' );

        if (Site\Type::hasCatalogModule() && $this->checkCatalogAccess()){
            $oFormBuilder->addButton(\Yii::t('review', 'settings_catalog'),'settingsCatalog','icon-edit','init');
        };

        $oFormBuilder->addField('on_main',\Yii::t('review', 'field_show_main'),'i','check',array( 'listColumns' => array('flex' => 3) ));

        $oFormBuilder
            ->addFilterSelect('filter_status', self::getStatusList(), $this->iStatusFilter, \Yii::t('review', 'field_status_active'))
            ->addRowButtonUpdate()
            ->addRowButtonDelete()
        ;

        if (get_class($this)=='skewer\build\Adm\GuestBook\Module'){
            $oFormBuilder->addButton(\Yii::t('adm','add'),'show','icon-add','init',array('addParams'=>array('show_section'=>$this->iShowSection)));
        }

        $iCount = 0;
        $aItems = ar\GuestBook::find();

        if ($this->iShowSection){
            $aItems = $aItems
                ->where('parent',$this->iShowSection)
                ->where('parent_class','')
            ;
        }

        if ($this->iStatusFilter!==false){
            $aItems = $aItems->where('status',$this->iStatusFilter);
        }

        /**
         * @var StateSelect $aItems
         */
        $aItems = $aItems
            ->setCounterRef( $iCount )
            ->limit( $this->iOnPage, $this->iPage * $this->iOnPage)
            ->asArray()
            ->getAll();

        foreach($aItems as &$aItem){
            static::parseData($aItem);
        }

        if ($iCount)
            $oFormBuilder->setValue($aItems, $this->iOnPage, $this->iPage, $iCount);
        else
            $oFormBuilder->setValue($aItems);
        $oFormBuilder->setEditableFields(array('on_main'),'saveOnMain');

        $this->setInterface($oFormBuilder->getForm());

        return psComplete;
    }

    protected function actionSaveOnMain(){

        $iId = $this->getInDataValInt( 'id' );
        $oRow = ar\GuestBook::find($iId);
        /**
         * @var ar\GuestBookRow $oRow
         */
        if ( !$oRow )
            throw new UserException( "Запись [$iId] не найдена" );

        $oRow->on_main = $this->getInDataVal( 'on_main');
        $oRow->save();

        $this->actionInit();
    }

    private static function parseData(&$aItem){

        $aItem['on_main'] = (int)$aItem['on_main'];

        if ($aItem['parent_class'] == '' || $aItem['parent_class']=='0'){
            $path = Tree::getSectionAliasPath( $aItem['parent'] );
            $aItem['link'] = "<a target='_blank' href='".$path."'>".$path."</a>";
            $aItem['type'] = \Yii::t('review', 'field_type_section');
        }

        # todo проверить результат слияния
        if ($aItem['parent_class'] == Api::GoodReviews){
            $goods = GoodsSelector::get($aItem['parent'],1);//fixme #GET_CATALOG_GOODS
            if (isset($goods['url']) && $goods['url'])
                $aItem['link'] = "<a target='_blank' href='".$goods['url']."'>".$goods['url']."</a>";
            $aItem['type'] = \Yii::t('review', 'field_type_order');
        }
    }

    /**
     * Удаляет запись
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        // id записи
        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        // удаление
        if ($iItemId)
            Adm\GuestBook\ar\GuestBook::delete($iItemId);

        // вывод списка
        $this->actionInit();

    }

    /**
     * Форма настроек модуля
     */
    protected function actionSettings(){

        $oFormBuilder = new UI\StateBuilder();
        $oFormBuilder = $oFormBuilder::newEdit();

        if (!$this->pageId){
            $aLanguages = Languages::getAllActive();
            $aLanguages = ArrayHelper::map($aLanguages, 'name', 'title');

            if (count($aLanguages) > 1) {
                $oFormBuilder->addFilterSelect('filter_language', $aLanguages, $this->sLanguageFilter, \Yii::t('adm','language'), ['set' => true]);
                $oFormBuilder->addFilterAction('settings');
            }
        }

        $oFormBuilder
            ->field('info', '', 's', 'show', ['hideLabel' => true])

            ->field('rating', \Yii::t('review', 'settings_rating'), 'i', 'check')
            ->fieldString('mail.title', \Yii::t('review', 'settings_admin_letter_title'))
            ->field('mail.content', \Yii::t('review', 'settings_content'), 's', 'wyswyg')
            ->fieldString('mail.notifTitleNew', \Yii::t('review', 'settings_new_subject'))
            ->field('mail.notifContentNew', \Yii::t('review', 'settings_new_content'), 's', 'wyswyg')

            ->field('mail.onNotif', \Yii::t('review', 'settings_enable_notifications'), 'i', 'check')
            ->fieldString('mail.notifTitleApprove', \Yii::t('review', 'settings_approve_subject'))
            ->field('mail.notifContentApprove', \Yii::t('review', 'settings_approve_content'), 's', 'wyswyg')
            ->fieldString('mail.notifTitleReject', \Yii::t('review', 'settings_reject_subject'))
            ->field('mail.notifContentReject', \Yii::t('review', 'settings_reject_content'), 's', 'wyswyg')
        ;

        $aModulesData = ModulesParams::getByModule('review', $this->sLanguageFilter);
        $this->setInnerData('languageFilter', $this->sLanguageFilter);

        $aItems = [];
        $aItems['info'] = \Yii::t('review', 'head_mail_text',  [\Yii::t('app', 'site_label', [], $this->sLanguageFilter),
                \Yii::t('app', 'url_label', [], $this->sLanguageFilter),
                \Yii::t('review', 'label_user', [], $this->sLanguageFilter)
                , $this->sLanguageFilter]
        );

        foreach( $this->aSettingsKeys as  $key ){
            $aItems[$key] = (isset($aModulesData[$key]))?$aModulesData[$key]:'';
        }

        $oFormBuilder
            ->addButtonSave('saveSettings')
            ->addButtonCancel('init')
        ;

        $oFormBuilder->setValue($aItems);

        $this->setInterface($oFormBuilder->getForm());
    }

    /**
     * Форма настроек модуля
     */
    protected function actionSettingsCatalog(){

        $oFormBuilder = new UI\StateBuilder();
        $oFormBuilder = $oFormBuilder::newEdit();

        if (!$this->pageId){
            $aLanguages = Languages::getAllActive();
            $aLanguages = ArrayHelper::map($aLanguages, 'name', 'title');

            if (count($aLanguages) > 1) {
                $oFormBuilder->addFilterSelect('filter_language', $aLanguages, $this->sLanguageFilter, \Yii::t('adm','language'), ['set' => true]);
                $oFormBuilder->addFilterAction('settingsCatalog');
            }
        }

        $oFormBuilder
            ->field('info', '', 's', 'show', ['hideLabel' => true])

            ->field('rating', \Yii::t('review', 'settings_rating'), 'i', 'check')
            ->fieldString('mail.catalog.title', \Yii::t('review', 'settings_catalog_admin_letter_title'))
            ->field('mail.catalog.content', \Yii::t('review', 'settings_catalog_content'), 's', 'wyswyg')
            ->fieldString('mail.catalog.notifTitleNew', \Yii::t('review', 'settings_catalog_new_subject'))
            ->field('mail.catalog.notifContentNew', \Yii::t('review', 'settings_catalog_new_content'), 's', 'wyswyg')

            ->field('mail.catalog.onNotif', \Yii::t('review', 'settings_catalog_enable_notifications'), 'i', 'check')
            ->fieldString('mail.catalog.notifTitleApprove', \Yii::t('review', 'settings_catalog_approve_subject'))
            ->field('mail.catalog.notifContentApprove', \Yii::t('review', 'settings_catalog_approve_content'), 's', 'wyswyg')
            ->fieldString('mail.catalog.notifTitleReject', \Yii::t('review', 'settings_catalog_reject_subject'))
            ->field('mail.catalog.notifContentReject', \Yii::t('review', 'settings_catalog_reject_content'), 's', 'wyswyg')
        ;

        $aModulesData = ModulesParams::getByModule('review', $this->sLanguageFilter);
        $this->setInnerData('languageFilter', $this->sLanguageFilter);

        $aItems = [];
        $aItems['info'] = \Yii::t('review', 'head_mail_text_catalog',  [\Yii::t('app', 'site_label', [], $this->sLanguageFilter),
                \Yii::t('app', 'url_label', [], $this->sLanguageFilter),
                \Yii::t('review', 'label_order', [], $this->sLanguageFilter),
                \Yii::t('review', 'label_user', [], $this->sLanguageFilter),
                $this->sLanguageFilter]
        );

        foreach( $this->aSettingsCatalogKeys as  $key ){
            $aItems[$key] = (isset($aModulesData[$key]))?$aModulesData[$key]:'';
        }
        $oFormBuilder
            ->addButtonSave('saveSettingsCatalog')
            ->addButtonCancel('init')
        ;

        $oFormBuilder->setValue($aItems);

        $this->setInterface($oFormBuilder->getForm());
    }

    /**
     * Сохраняем настройки формы
     */
    protected function actionSaveSettings(){

        $aData = $this->getInData();

        $sLanguage = $this->getInnerData('languageFilter');
        $this->setInnerData('languageFilter', '');

        if ($sLanguage){
            foreach( $aData as $sName => $sValue ){

                if (!in_array($sName, $this->aSettingsKeys))
                    continue;

                ModulesParams::setParams( 'review', $sName, $sLanguage, $sValue);
            }
        }

        $this->actionInit();
    }

    protected function actionSaveSettingsCatalog(){

        $aData = $this->getInData();

        $sLanguage = $this->getInnerData('languageFilter');
        $this->setInnerData('languageFilter', '');

        if ($sLanguage){
            foreach( $aData as $sName => $sValue ){

                if (!in_array($sName, $this->aSettingsCatalogKeys))
                    continue;

                ModulesParams::setParams( 'review', $sName, $sLanguage, $sValue);
            }
        }

        $this->actionInit();
    }

    /**
     * Установка служебных данных
     * @param UI\State\BaseInterface $oIface
     */
    protected function setServiceData( UI\State\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData( array(
            'page' => $this->iPage
        ) );

    }

}