<?php

namespace skewer\build\Tool\Review;


use skewer\build\Adm\GuestBook\ar;
use skewer\build\Component\Catalog\GoodsSelector;
use \skewer\build\Component\I18N\ModulesParams;
use yii\helpers\ArrayHelper;

class Api{

    const GoodReviews = 'GoodsReviews';

    /**
     * Отправка письма админу
     * @param ar\GuestBookRow $rowGuestBook
     * @return bool
     */
    public static function sendMailToAdmin( $rowGuestBook ) {

        $aParams = ModulesParams::getByModule('review');

        // берем заголовок письма из базы
        if ($rowGuestBook->parent_class == static::GoodReviews){
            $sTitle = ArrayHelper::getValue($aParams, 'mail.catalog.title', '');
            $sContent = ArrayHelper::getValue($aParams, 'mail.catalog.content', '');
        } else {
            $sTitle = ArrayHelper::getValue($aParams, 'mail.title' .'');
            $sContent = ArrayHelper::getValue($aParams, 'mail.content', '');
        }

        $aUserLabels = \Yii::$app->getI18n()->getValues('review', 'label_user');
        foreach( $aUserLabels as $sLabel){
            $aParams[$sLabel] = $rowGuestBook->name;
        }
        if ($rowGuestBook->parent_class == static::GoodReviews){
            $goods = GoodsSelector::get($rowGuestBook->parent, 1);
            $aOrderLabels = \Yii::$app->getI18n()->getValues('review', 'label_order');
            foreach( $aOrderLabels as $sLabel){
                $aParams[$sLabel] = $goods['title'];
            }
        }
        $aParams['email'] = $rowGuestBook->email;

        return \Mailer::sendMailAdmin( $sTitle, $sContent, $aParams );
    }

    /**
     * Отправка письма клиенту
     * @param ar\GuestBookRow $rowGuestBook
     * @param $iStatus
     * @return bool
     */
    public static function sendMailToClient( $rowGuestBook, $iStatus = null ) {

        $aParams = ModulesParams::getByModule('review');

        if (is_null($iStatus)){
            $iStatus = $rowGuestBook->status;
        } else {

            if ( $iStatus == $rowGuestBook->status ){
                return false;
            }

            //отправлять уведомление пользователю или нет
            if ( $rowGuestBook->parent_class == static::GoodReviews ){
                $sOnNotif = ArrayHelper::getValue($aParams, 'mail.catalog.onNotif', false);
            } else {
                $sOnNotif = ArrayHelper::getValue($aParams, 'mail.onNotif', false);
            }

            if (!$sOnNotif){
                return false;
            }
        }

        $sTitle = "";
        $sContent = '';
        if ($rowGuestBook->parent_class == static::GoodReviews){

            switch ($iStatus){
                case ar\GuestBook::statusApproved:
                    $sTitle = ArrayHelper::getValue($aParams, 'mail.catalog.notifTitleApprove', '');
                    $sContent = ArrayHelper::getValue($aParams, 'mail.catalog.notifContentApprove', '');
                    break;
                case ar\GuestBook::statusRejected:
                    $sTitle = ArrayHelper::getValue($aParams, 'mail.catalog.notifTitleReject', '');
                    $sContent = ArrayHelper::getValue($aParams, 'mail.catalog.notifContentReject', '');
                    break;
                case ar\GuestBook::statusNew:
                default:
                    $sTitle = ArrayHelper::getValue($aParams, 'mail.catalog.notifTitleNew', '');
                    $sContent = ArrayHelper::getValue($aParams, 'mail.catalog.notifContentNew', '');
                    break;
            }

        } else {

            switch ($iStatus){
                case ar\GuestBook::statusApproved:
                    $sTitle = ArrayHelper::getValue($aParams, 'mail.notifTitleApprove', '');
                    $sContent = ArrayHelper::getValue($aParams, 'mail.notifContentApprove', '');
                    break;
                case ar\GuestBook::statusRejected:
                    $sTitle = ArrayHelper::getValue($aParams, 'mail.notifTitleReject', '');
                    $sContent = ArrayHelper::getValue($aParams, 'mail.notifContentReject', '');
                    break;
                case ar\GuestBook::statusNew:
                default:
                    $sTitle = ArrayHelper::getValue($aParams, 'mail.notifTitleNew', '');
                    $sContent = ArrayHelper::getValue($aParams, 'mail.notifContentNew', '');
                    break;
            }

        }

        $aMailParams = [];

        $aUserLabels = \Yii::$app->getI18n()->getValues('review', 'label_user');

        foreach( $aUserLabels as $sLabel){
            $aMailParams[$sLabel] = $rowGuestBook->name;
        }
        if ( $rowGuestBook->parent_class == static::GoodReviews ){
            $goods = GoodsSelector::get($rowGuestBook->parent, 1);
            $aOrderLabels = \Yii::$app->getI18n()->getValues('review', 'label_order');
            foreach( $aOrderLabels as $sLabel){
                $aMailParams[$sLabel] = $goods['title'];
            }
        }
        $aMailParams['email'] = $rowGuestBook->email;

        return \Mailer::sendMail( $rowGuestBook->email, $sTitle, $sContent, $aMailParams );
    }

}