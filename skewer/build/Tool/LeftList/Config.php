<?php

/* main */
$aConfig['name']     = 'LeftList';
$aConfig['title']    = 'Список';
$aConfig['version']  = '1.000';
$aConfig['description']  = '';
$aConfig['revision'] = '0002';
$aConfig['layer']     = \Layer::TOOL;
$aConfig['isSystem']  = true;
$aConfig['languageCategory']     = 'adm';

return $aConfig;
