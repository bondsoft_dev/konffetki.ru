<?php

use skewer\build\Tool\LeftList;

$aConfig['name']     = 'Slider';
$aConfig['title']    = 'Slider';
$aConfig['version']  = '1.000';
$aConfig['description']  = '';
$aConfig['revision'] = '0002';
$aConfig['useNamespace'] = true;
$aConfig['layer']     = \Layer::TOOL;
$aConfig['group']     = LeftList\Group::CONTENT;

return $aConfig;
