<?php

namespace skewer\build\Tool\Slider;

use skewer\build\Adm;
use skewer\build\Tool;


/**
 * Проекция редактора баннеров для слайдера в панель управления
 * Class Module
 * @package skewer\build\Tool\Slider
 */
class Module extends Adm\Slider\Module implements Tool\LeftList\ModuleInterface {

    /**
     * @inheritDoc
     */
    public function init()
    {
        Tool\LeftList\ModulePrototype::updateLanguage();
        parent::init();
    }

    public function __construct( \skContext $oContext ) {
        parent::__construct( $oContext );
        $oContext->setModuleLayer('Adm');
        $oContext->setModuleWebDir('/skewer/build/Adm/Slider');
    }

    function getName() {
        return $this->getModuleName();
    }

    protected function getWorkMode() {
        return self::tool;
    }

}