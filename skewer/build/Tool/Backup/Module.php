<?php

namespace skewer\build\Tool\Backup;


use skewer\build\Component\Site\Server;
use skewer\build\Tool;
use \skewer\build\Component\UI\StateBuilder;
use skewer\build\Component\QueueManager as QM;

class Module extends Tool\LeftList\ModulePrototype {

    protected function preExecute() {
        \CurrentAdmin::testControlPanelAccess();
    }

    public function createFormIncluster(){
        // объект для построения списка
        $oList = new \ExtList();

        $aModel = Model::getFullParamDefList( Model::getListFields() );

        // задать модель данных для вывода
        $oList->setFields( $aModel );

        // добавление набора данных
        $aItems = Api::getListItems();

        // форматирование элементов
        if(isSet($aItems['items']) AND count($aItems['items']))
            foreach ( $aItems['items'] as $iKey => $aItem ) {
                $aItem['size_sort'] = \skFiles::sizeToSortStr( $aItem['size'] );
                $aItem['size'] = \skFiles::sizeToStr( $aItem['size'] );
                $aItems['items'][$iKey] = $aItem;
            }

        $oList->setValues( $aItems['items'] );

        // для nginx выключим востановление бекапов
        if ( !Server::isNginx() ) {
            $oList->addRowBtnArray(array(
                'tooltip' => \Yii::t('backup', 'restore'),
                'iconCls' => 'icon-recover',
                'action' => 'recoverForm',
                'state' => 'init'
            ));
        }

//        $oList->addRowBtnArray(array(
//            'tooltip' =>'Скачать',
//            'iconCls' => 'icon-save',
//            'action' => 'downloadFile',
//            'state' => 'init'
//        ));

        $oList->addRowBtnArray(array(
            'tooltip' => \Yii::t('adm','del'),
            'iconCls' => 'icon-delete',
            'action'  => 'remove',
            'state'   => 'delete'
        ));


        $oList->addDockedItem(array(
            'text' => \Yii::t('backup', 'date_setup'),
            'iconCls' => 'icon-edit',
            'state' => 'init',
            'action' => 'toolsForm',
        ));

        $oList->addDockedItem(array(
            'text' => \Yii::t('backup', 'createBackup'),
            'iconCls' => 'icon-add',
            'state' => 'allow_do',
            'action' => 'createBackup',
            'actionText' => \Yii::t('backup', 'createBackupText'),
            'doNotUseTimeout' => true,
        ));

        return $oList;
    }

    public function createFormNotcluster(){
        $builder =  StateBuilder::newList();

        if (!is_dir(ROOTPATH.'backup')){
            mkdir(ROOTPATH.'backup');
        }

        $aValues = array();
        $aFiles = Api::getDumpFiles(ROOTPATH.'/backup');
        $builder
            ->addField('filename_text',\Yii::t('backup', 'filename'),'s','string',array( 'listColumns' => array('flex' => 3) ) )
            ->addField('filename',\Yii::t('backup', 'filename'),'s','hide',array( 'listColumns' => array('flex' => 3) ) )
            ->addField('filesize',\Yii::t('backup', 'size'),'s','string',array( 'listColumns' => array('flex' => 1) ) );

        rsort($aFiles);

        foreach($aFiles as $file){
            $aValues[] = array('filename'=>$file['filename'],'filename_text'=>'<a href="/local/?ctrl='.Local::getLocalLabel().'&&fileName='.$file['filename'].'">'.$file['filename'].'</a>','filesize'=>$file['filesize']);
        }

        $builder->setValue($aValues);
        $builder->addRowButtonConfirm(\Yii::t('backup', 'restore'),'icon-clone','restoreBackupDB','allow_do',\Yii::t('backup', 'restoreBackupText'));        

        $builder->addRowButtonDelete('deleteBackupDB');

        $builder->addButton(\Yii::t('adm','add'),'addBackupDB','icon-add');
        return $builder->getForm();
    }

    public function actionDeleteBackupDB(){
        $data = $this->getInData();
        if (isset($data['filename'])) {
            unlink(ROOTPATH.'backup/'.$data['filename']);
        }
        $this->actionInit();
    }

    public function actionRestoreBackupDB(){
        $data = $this->getInData();
        if (isset($data['filename']) && is_file(ROOTPATH.'backup/'.$data['filename'])){
            Api::restoreDbase(ROOTPATH.'backup/'.$data['filename']);
            \CacheUpdater::setUpdFlag();
            $this->addMessage(\Yii::t('backup', 'backupOk'));
        }

        $this->actionInit();
    }

    public function actionAddBackupDB(){

        Api::createDBbackup(ROOTPATH.'backup/'.date('Y-m-d_H-i-s').'.sql');

        $this->actionInit();
    }

    public function ActionInit(){

        if (INCLUSTER)
            $this->setInterface( $this->createFormIncluster() );
        else $this->setInterface( $this->createFormNotcluster() );
    }


    public function actionToolsForm(){

        $aData = Service::getBackupSetting();

        // объект для построения списка
        $oForm = new \ExtForm();

        $aItems = array();

        /* Файл */
        $aItems['bs_enable'] = array(
            'name' => 'bs_enable',
            'title' => \Yii::t('backup', 'useLocalSettings'),
            'view' => 'check',
            'value' => $aData['bs_enable'],
            //'disabled' => true,
        );

        $aItems['bs_day'] = array(
            'name' => 'bs_day',
            'title' => \Yii::t('backup', 'bs_day'),
            'view' => 'int',
            'value' => $aData['bs_day'],
            //'disabled' => true,
        );

        $aItems['bs_week'] = array(
            'name' => 'bs_week',
            'title' => \Yii::t('backup', 'bs_week'),
            'view' => 'int',
            'value' => $aData['bs_week'],
            //'disabled' => true,
        );

        $aItems['bs_month'] = array(
            'name' => 'bs_month',
            'title' => \Yii::t('backup', 'bs_month'),
            'view' => 'int',
            'value' => $aData['bs_month'],
            //'disabled' => true,
        );
                              /*
        $aItems['bs_hour'] = array(
            'name' => 'bs_hour',
            'title' => 'Время запуска, час',
            'view' => 'int',
            'value' => $aData['bs_hour'],
            //'disabled' => true,
        );

        $aItems['bs_min'] = array(
            'name' => 'bs_min',
            'title' => 'Время запуска, мин',
            'view' => 'int',
            'value' => $aData['bs_min'],
            //'disabled' => true,
        );
                            */
        $oForm->setFields($aItems);
        $oForm->setValues(array());

        $oForm->addBtnSave('saveTools');
        
        $oForm->addDockedItem(array(
            'text' => \Yii::t('backup', 'backToList'),
            'iconCls' => 'icon-cancel',
            'state' => 'init',
            'action' => 'init',
        ));

        $this->setInterface($oForm);

        return psComplete;

    }

    public function actionSaveTools(){

        $aData = $this->get('data');

        Api::setBackupSetting($aData);

        $this->actionToolsForm();

        return psComplete;
    }


    public function actionCreateBackup(){

        $iStatus = Api::createNewBackup();

        if ($iStatus == QM\Task::stClose)
            $iStatus = QM\Task::stComplete;
        $aStatus = QM\Api::getStatusList();
        $status = (isset($aStatus[$iStatus]))?$aStatus[$iStatus]:'';

        $this->addMessage( \Yii::t('backup', 'backupStatus').': ' . $status );
        $this->addNoticeReport(\Yii::t('backup', 'addBackupReport'), "", \skLogger::logUsers, "BackupToolModule");

        $this->actionInit();

    }


    public function actionRecoverForm(){

        $aData = $this->get('data');

        try {
            Api::checkBackup($aData['id']);
        }
        catch( \Exception $e ){

            $oForm = new \ExtShow();

            $oForm->setAddText(\Yii::t('backup', 'error_msg', $e->getMessage()));

            $oForm->addBtnCancel('init');
            $oForm->addBtnSeparator('->');

            $this->setInterface($oForm);

            return psComplete;

        }

        $oForm = new \ExtForm();

        $this->setPanelName(\Yii::t('backup', 'restoreMaster'),true);

        /* Id резервной копии */
        $aItems['id'] = array(
            'name' => 'id',
            'title' => '',
            'view' => 'hide',
            'value' => $aData['id'],
            'disabled' => false,
        );

        /* Файл */
        $aItems['file'] = array(
            'name' => 'file',
            'title' => \Yii::t('backup', 'file'),
            'view' => 'str',
            'value' => $aData['backup_file'],
            'disabled' => true,
        );

        /* Дата создания */
        $aItems['creation_date'] = array(
            'name' => 'creation_date',
            'title' => \Yii::t('backup', 'creation_date'),
            'view' => 'str',
            'value' => $aData['date'],
            'disabled' => true,
        );

        /* Делать ли резервную копию перед разворачиванием площадки */
        $aItems['before_backup'] = array(
            'name' => 'before_backup',
            'title' => \Yii::t('backup', 'beforeBackup'),
            'view' => 'check',
            'value' => 1,
        );

        $oForm->setFields($aItems);
        $oForm->setValues(array());

        $oForm->addExtButton( \ExtDockedAddBtn::create()
            ->setTitle( \Yii::t('backup', 'restore') )
            ->setIconCls('icon-recover')
            ->setAction('recover')
            ->unsetDirtyChecker()
            ->setConfirm( \Yii::t('backup', 'restoreBackupText') )
        );
        $oForm->addBtnCancel('init');
        $oForm->addBtnSeparator('->');

        $this->setInterface($oForm);

        return psComplete;


    }


    public function actionRecover(){

        try {

            $aData = $this->get('data');

            if(!isSet($aData['id']) OR !$iBackupId = $aData['id']) throw new \Exception('Recover error: Backup is undefined!');

            $bCreateBeforeBackup = (isSet($aData['before_backup']) AND $aData['before_backup'])? true: false;

            /* Получить данные по резервной копии */
            if($bCreateBeforeBackup) {

                //$mError = false;
                //$sDescription = 'Создано перед восстановлением из резервной копии от '.$aBackupItem['date'];
                //if(!$this->createBackup($aBackupItem['site_id'], 3, $sDescription, $mError)) throw new Exception($mError);
                Api::createNewBackup();

            }

            //$mError = false;
            //if(!$this->recoverBackup($aSiteItem['name'], $aBackupItem['backup_file'], $mError)) throw new Exception($mError);
            Api::recoverBackup(array($iBackupId));

            $this->addMessage(\Yii::t('backup', 'backupOk'));
            $this->addNoticeReport(\Yii::t('backup', 'goodRecover'), "", \skLogger::logUsers, "BackupToolModule");
        } catch (\Exception $e) {
            $this->addError($e->getMessage());
        }

        $this->actionInit();

        return psComplete;
    }


    public function actionRemove(){

        $aData = $this->get('data');

        Api::removeBackup($aData);
        $this->addNoticeReport(\Yii::t('backup', 'deleteBackup'), "", \skLogger::logUsers, "BackupToolModule");
        $this->actionInit();

        return psComplete;

    }

//    public function actionDownloadFile(){
//
//        $aData = $this->get('data');
//
//        $sToken = Service::getDownloadFileToken($aData);
//
//        if(!$sToken) throw new \Exception(\Yii::t('backup', 'loadBackupError'));
//
//        $sLink = str_replace('index','downloadBackup',CLUSTERGATEWAY);
//        $sLink .= '?token='.$sToken;
//
//        $this->setData('link',$sLink);
//
//        // дополнительная библиотека для отображения
//        $this->addLibClass( 'BackupFile' );
//        $oInterface = new \ExtUserFile( 'BackupFile' );
//        $this->setInterface( $oInterface );
//
//        return psComplete;
//    }

}
