<?php

namespace skewer\build\Tool\Backup;

/**
 * Маппер для работы с записями бэкапов
 */
class Model extends \skModelPrototype {

    /**
     * Отдает описание модели
     * @var array
     */
    protected static $aParametersList = array(
        'id'          => 'i:hide:backup.id',
        'site_id'     => 'i:hide:backup.site_id',
        'backup_file' => 's:str:backup.backup_file',
        'date'        => 's:str:backup.date',
        'status'      => 'i:str:backup.status',
        'comments'    => 's:text:backup.comments'
    );

    /**
     * Отдает набор полей для просмотра списка
     */
    public static function getListFields() {
        return array(
            'id',
            'date',
            'mode',
            'size',
            'backup_file',
        );
    }

    /**
     * Отдает дополнительный набор параметров для конфигурации полей
     * @return array
     */
    protected static function getAddParamList(){
        return array(
            'id' => array(
                'listColumns' => array('hidden' => true)
            ),
            'date' => array(
                'listColumns' => array('width' => 120)
            ),
            'mode' => array(
                'title' => \Yii::t('backup', 'mode'),
                'listColumns' => array('width' => 50)
            ),
            'backup_file' => array(
                'listColumns' => array('flex' => 1)
            ),
            'size' => array(
                'title' => \Yii::t('backup', 'size'),
                'listColumns' => array('width' => 80)
            ),
        );
    }

}
