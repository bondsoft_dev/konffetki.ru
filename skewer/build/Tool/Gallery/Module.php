<?php

namespace skewer\build\Tool\Gallery;


use skewer\build\Component\UI;
use skewer\build\Tool;
use skewer\build\Component\Gallery;
use yii\base\UserException;

/**
 * @todo Рефакторить. Избавиться от передачи параметров через кнопки и настроить отлов ошибок
 * Модуль редактирования форматов для галереи
 */
class Module extends Tool\LeftList\ModulePrototype {

    /**
     * Массив полей, выводимых колонками в списке форматов для профиля настроек
     * @var array
     */
    protected $aFormatsListFields = ['id', 'title', 'width', 'height', 'active'];

    /**
     * Id текущего открытого профиля
     * @var int
     */
    protected $iCurrentProfile = 0;

    /**
     * Иницализация
     */
    protected function preExecute() {
        /* Восстанавливаем Id текущего открытого профиля */
        $this->iCurrentProfile = $this->getInt('currentProfile');
    }

    /**
     * Установка служебных данных
     * @param UI\State\BaseInterface $oIface
     */
    protected function setServiceData( UI\State\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData([
            'currentProfile' => $this->iCurrentProfile,
        ]);
    }

    /**
     * Вызывается в случае отсутствия явного обработчика
     * @return int
     */
    protected function actionInit() {
        return $this->actionGetProfilesList();
    }

    /**
     * Обработчик состояния списка профилей
     * @return int
     */
    protected function actionGetProfilesList() {

        $this->setPanelName(\Yii::t('gallery', 'tools_profileImageSettings'),true);
        $this->iCurrentProfile = false;

        $oFormBuilder = UI\StateBuilder::newList();

        $oFormBuilder
            ->fieldString('title', \Yii::t('gallery', 'profiles_title'),['listColumns' => ['flex' => 1]]);

        $aProfiles = Gallery\Profile::getAll();

        $oFormBuilder->setValue($aProfiles);

        $oFormBuilder->buttonRowUpdate('addUpdProfile');
        $oFormBuilder->button(\Yii::t('adm', 'add'), 'AddUpdProfile', 'icon-add');

        if (\CurrentAdmin::isSystemMode())
            $oFormBuilder->buttonRowDelete('delProfile');
        $this->setInterface($oFormBuilder->getForm());
    }// func

    /**
     * Состояние редактирования профиля
     */
    protected function actionAddUpdProfile() {

        // Данные по профилю
        $aData = $this->get('data');

        // Id профиля
        if ($this->iCurrentProfile) // Отрабатывает возврат из списка форматов к профилю
             $iProfileId = $this->iCurrentProfile;
        else $iProfileId = $this->iCurrentProfile = (is_array($aData) AND isset($aData['id'])) ? (int)$aData['id'] : 0;

        $this->setPanelName(\Yii::t('gallery', 'tools_addProfile'),true);

        // -- сборка интерфейса
        $oFormBuilder = UI\StateBuilder::newEdit(); // создаем форму

        // Получаем данные профиля или заготовку под новый профиль
        $aItem = $iProfileId ? Gallery\Profile::getById($iProfileId) : Gallery\Profile::getProfileBlankValues();

        // добавляем поля
        $oFormBuilder
            ->fieldHide('id', 'id')
            ->fieldString('title', \Yii::t('gallery', 'profiles_title'))
            ->field('active', \Yii::t('gallery', 'profiles_active'), 'i', 'check');

        $oFormBuilder->setValue($aItem);

        // добавляем элементы управления
        $oFormBuilder->button(\Yii::t('adm','save'), 'saveProfile', 'icon-save','init');
        $oFormBuilder->button(\Yii::t('adm','back'), 'init', 'icon-cancel');

        if ($iProfileId){
            $oFormBuilder->button(\Yii::t('gallery', 'tools_formats'),'formatsList','icon-edit','init');
        }

        $this->setInterface($oFormBuilder->getForm());
    }// func

    /**
     * Сохраняет профиль
     * @throws \Exception
     */
    protected function actionSaveProfile() {

        /* Сохранение данных с профиля */
        try {
            $aData = $this->get('data');
            if (!count($aData)) throw new \Exception ('Error: Data is not sent!');
            Gallery\Profile::setProfile($aData, $aData['id']);

        } catch (\Exception $e) {
            echo $e; //@todo echo ???
        }

        /* вывод списка */
        $this->actionGetProfilesList();

    }// func

    /**
     * Удаляет выбранный профиль
     * @throws \Exception
     */
    protected function actionDelProfile() {
        /* Данные по профилю */
        $aData = $this->get('data');
        try {
            if (!\CurrentAdmin::isSystemMode())
                throw new \Exception('Нет прав на выполнение действия.');

            if ( !isSet($aData['id']) OR (!$aData['id'] = (int)$aData['id']) ) throw new \Exception('Error: Element is not removed!');
            Gallery\Profile::removeProfile($aData['id']);

        } catch (\Exception $e) {
            $this->addError( $e->getMessage() );
        }
        /*Вывод списка профилей*/
        $this->actionGetProfilesList();

    }// func

    /**
     * Выводит список форматов для профиля
     * @throws \Exception
     */
    protected function actionFormatsList() {

        /* Данные по профилю */
        $aData = $this->get('data');

        $this->setPanelName(\Yii::t('gallery', 'tools_formatsImage'),true);

        try {

            if ($this->iCurrentProfile)
                $iProfileId = $this->iCurrentProfile;
            elseif ( !isSet($aData['id']) OR (!$iProfileId = (int)$aData['id']) ) throw new \Exception('Error: Formats not received!');

            // -- сборка интерфейса
            $oFormBuilder = UI\StateBuilder::newList(); // создаем форму

            // добавляем поля
            $oFormBuilder
                ->fieldString('title', \Yii::t('gallery', 'formats_title'), ['listColumns' => ['flex' => 2]])
                ->fieldString('width', \Yii::t('gallery', 'formats_width'), ['listColumns' => ['flex' => 1]])
                ->fieldString('height', \Yii::t('gallery', 'formats_height'),['listColumns' => ['flex' => 1]])
                ->fieldString('active', \Yii::t('gallery', 'formats_active'), ['listColumns' => ['flex' => 1]]);

            $aItems = Gallery\Format::getByProfile($iProfileId);
            $oFormBuilder->setValue($aItems);

            $oFormBuilder->buttonRowUpdate('addUpdFormat');

            if ( \CurrentAdmin::isSystemMode() )
                $oFormBuilder->buttonRowDelete('delFormat');

            /**
             * @todo передавать параметры через кнопку - это не ок ни разу, пересмотреть это место
             */
            $oFormBuilder->button(\Yii::t('gallery', 'tools_backToProfile'),'addUpdProfile','icon-cancel','init',array('addParams'=>array('data'=>array('profile_id'=>$aData['id']))));
            $oFormBuilder->button(\Yii::t('adm', 'add'),'addUpdFormat','icon-add','init',array('addParams'=>array('data'=>array('profile_id'=>$aData['id']))));
            $oFormBuilder->enableDragAndDrop('sortFormats');

            $this->setInterface($oFormBuilder->getForm());

        } catch (\Exception $e) {
            echo $e; //@todo echo ???
        }
    }// func

    /**
     * Сортировка форматов
     */
    protected function actionSortFormats() {

        $aData = $this->get('data');
        $aDropData = $this->get('dropData');
        $sPosition = $this->get('position');

        if (!isSet($aData['id']) OR !$aData['id'] OR
            !isSet($aDropData['id']) OR !$aDropData['id'] OR !$sPosition)
                $this->addError('Ошибка! Неверно заданы параметры сортировки'); //@todo lang

        if (!Gallery\Format::sortFormats($aData['id'], $aDropData['id'], $sPosition))
                $this->addError('Ошибка! Неверно заданы параметры сортировки');
    }

    /** Вывести список выравнивания водяного знака относительно изображения
     * @return array
    */
    private static function getWatermarkCalibrateList() {
        return [
            alignWatermarkTopLeft => \Yii::t('gallery', 'water_top_left'), 
            alignWatermarkTopRight => \Yii::t('gallery', 'water_top_right'),
            alignWatermarkBottomLeft => \Yii::t('gallery', 'water_bottom_left'),
            alignWatermarkBottomRight => \Yii::t('gallery', 'water_bottom_right'),
            alignWatermarkCenter => \Yii::t('gallery', 'water_center')
        ];
    }

    /**
     * @TODO дорефакторить
     * Состояние добавления/редактирования формата
     */
    protected function actionAddUpdFormat() {

        try {
            $oFormBuilder = UI\StateBuilder::newEdit();

            // Данные по формату
            $aData = $this->get('data');

            $iFormatId  = (is_array($aData) && isset($aData['id'])) ? (int)$aData['id'] : 0;
            $iProfileId = $this->iCurrentProfile;

            $this->setPanelName(\Yii::t('gallery', 'tools_addFormat'),true);
   
            $this->setPanelName(\Yii::t('gallery', 'tools_addFormat'),true);
            if($iFormatId)
                $this->setPanelName(\Yii::t('gallery', 'tools_editFormat'),true);

            // Получаем данные формата или заготовку под новый формат
            $aValues = $iFormatId ? Gallery\Format::getById($iFormatId) : Gallery\Format::getFormatBlankValues(['profile_id' => $iProfileId]);

            // Установить набор элементов формы
            $oFormBuilder
                ->fieldString('title', \Yii::t('gallery', 'formats_title'))
                ->fieldString('name', \Yii::t('gallery', 'formats_name'))
                ->field('width', \Yii::t('gallery', 'formats_width'), 'i', 'int')
                ->field('height', \Yii::t('gallery', 'formats_height'), 'i', 'int')
                ->field('active', \Yii::t('gallery', 'formats_active'), 'i', 'check')
                ->fieldHide('id', '')
                ->field('resize_on_larger_side', \Yii::t('gallery', 'formats_resize_on_larger_side'), 'i', 'check')
                ->field('scale_and_crop', \Yii::t('gallery', 'formats_scale_and_crop'), 'i', 'check')
                ->field('use_watermark', \Yii::t('gallery', 'formats_use_watermark'), 'i', 'check')
                ->field('watermark', \Yii::t('gallery', 'formats_watermark'), 's', 'file')
                ->fieldHide('profile_id', \Yii::t('gallery', 'formats_profile_id'), 'i', 'int')
                ->fieldSelect('watermark_align', \Yii::t('gallery', 'formats_watermark_align'), self::getWatermarkCalibrateList())
                ->fieldHide('position', \Yii::t('gallery', 'formats_position'), 'i');

            // Установить значения для элементов
            $oFormBuilder->setValue($aValues);

            // Добавление кнопок
            $oFormBuilder->addButtonSave('saveFormat');
            $oFormBuilder->addButtonCancel('formatsList');

            if ($iFormatId AND \CurrentAdmin::isSystemMode()) {
                $oFormBuilder->buttonSeparator('->');
                $oFormBuilder->getForm()->addBtnDelete('delFormat');
            }

            // вывод данных в интерфейс
            $this->setInterface( $oFormBuilder->getForm() );

        } catch (\Exception $e) {
            echo $e; //@todo echo ???
        }
    }// func

    /**
     * Сохраняет формат в профиле настроек
     * @throws \Exception
     */
    protected function actionSaveFormat() {

        /* Сохранение данных формата */
        try {

            $aData = $this->get('data');

            if (!count($aData)) throw new \Exception ('Error: Data is not send!');

            $iFormatId = ($aData['id'])? $aData['id']: false;

            /* Привязка к профилю обязательна */
            if ( !isSet($aData['profile_id']) OR (!$aData['profile_id'] = (int)$aData['profile_id']) ) throw new \Exception('Error: Data is not send!');

            // если водяной знак - файл, то проверить, что он png
            $sWatermark = isset($aData['watermark']) ? $aData['watermark'] : '';
            if ($sWatermark) {
                $sPossibleFileName = WEBPATH.$sWatermark;
                if (file_exists($sPossibleFileName)) {
                    $aFile = getimagesize($sPossibleFileName);
                    if ($aFile[2] !== 3)
                        $this->addError( 'В качестве водяного знака может быть использован только файл типа PNG' );
                }
            }

            // Добавляем либо обнавляем формат
            Gallery\Format::setFormat($aData, $iFormatId);

            $this->addMessage( \Yii::t('gallery', 'tools_saveFormatMessage') );

        } catch (\Exception $e) {
            throw new UserException($e->getMessage());
        }

        /* вывод списка */
        $this->actionFormatsList();

    }// func

    /**
     * Удаляет выбранный формат
     * @throws \Exception
     */
    protected function actionDelFormat() {

        /* Данные по формату */
        $aData = $this->get('data');

        try {
            if (!\CurrentAdmin::isSystemMode())
                throw new \Exception('Нет прав на выполнение действия.');

            if (!isSet($aData['id']) OR (!$aData['id'] = (int)$aData['id'])) throw new \Exception('Error: Element(Format) is not removed!');

            /*Удаление формат*/
            Gallery\Format::removeFormat($aData['id']);

        } catch (\Exception $e) {

            $this->addError( $e->getMessage() );
        }

        /*Вывод списка профилей*/
        $this->actionFormatsList();

    }// func

}
