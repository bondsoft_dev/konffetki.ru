<?php

namespace skewer\build\Tool\Utils;


use skewer\build\Component\Installer\Api;
use skewer\build\Component\QueueManager\Task;
use skewer\build\Component\Search\SearchIndex;
use skewer\build\Component\SEO\Service;
use skewer\build\Component\UI\StateBuilder;
use skewer\build\Tool;
use \skewer\build\Tool\Domains;

/**
 * Модуль c системными утилитами
 * Class Module
 * @package skewer\build\Tool\Utils
 */
class Module extends Tool\LeftList\ModulePrototype {

    protected function actionInit( $sText = '' ) {

        $form = StateBuilder::newEdit();

        $form->button(\Yii::t('utils', 'drop_cache_act'),'dropCache',\ExtDocked::iconDel,'dropCache');
        $form->button(\Yii::t('utils', 'renew_act'),'init',\ExtDocked::iconReload,'init');
        $form->button(\Yii::t('utils', 'rebuildLanguages'),'DropLanguages',\ExtDocked::iconReload,'init');
        $form->button(\Yii::t('utils', 'logs'),'Logs',\ExtDocked::iconConfiguration,'Logs');
        $form->button(\Yii::t('utils', 'search'),'Search',\ExtDocked::iconNext,'init');


        if ( $sText )
            $form->addHeadText( $sText );

        $this->setInterface($form->getForm());
    }

    protected function actionDropCache() {

        \CacheUpdater::setUpdFlag();

        $this->actionInit( \Yii::t('utils', 'drop_cache_text') );

    }

    protected function actionDropLanguages() {

        \CacheUpdater::setUpdFlag();

        $installer = new Api();
        $installer->clearLanguages();
        $installer->updateAllModulesConfig();

        $this->actionInit( \Yii::t('utils', 'drop_languages_text') );

    }

    protected function actionLogs( $sText = '' ) {

        $oShow = new \ExtShow;

        if ( $sText )
            $oShow->setAddText( sprintf( '<p>%s</p>', $sText ) );

        $oShow->addExtButton( \ExtDocked::create( \Yii::t('utils', 'view_access') )
            ->setIconCls( \ExtDocked::iconReload )
            ->setState( 'viewAccess' )
            ->setAction( 'viewAccess' )
        );

        $oShow->addExtButton( \ExtDocked::create( \Yii::t('utils', 'view_error') )
            ->setIconCls( \ExtDocked::iconReload )
            ->setState( 'viewError' )
            ->setAction( 'viewError' )
        );


        $oShow->addExtButton( \ExtDocked::create( \Yii::t('utils', 'clear_logs') )
            ->setIconCls( \ExtDocked::iconDel )
            ->setState( 'clearLogs' )
            ->setAction( 'clearLogs' )
        );


        $oShow->addExtButton( \ExtDocked::create( \Yii::t('adm','back') )
            ->setIconCls( \ExtDocked::iconCancel )
            ->setState( 'init' )
            ->setAction( 'init' )
        );

        $this->setInterface($oShow);
    }


    protected function actionViewAccess() {

        $sText = '';

        $sFile = ROOTPATH . 'log/access.log';
        if ( file_exists($sFile) ) {

            $arr = file( $sFile );

            $iStrCount = count($arr);
            if ( $iStrCount ) {
                $iFirstStr = ($iStrCount > 30) ? $iStrCount - 30 : 0;
                for ( $i = $iFirstStr; $i<$iStrCount; $i++ )
                    $sText .= $arr[ $i ] . '<br>';
            }
        }

        $this->actionLogs( $sText );
    }


    protected function actionViewError() {

        $sText = '';

        $sFile = ROOTPATH . 'log/error.log';
        if ( file_exists($sFile) ) {

            $arr = file( $sFile );

            $iStrCount = count($arr);
            if ( $iStrCount ) {
                $iFirstStr = ($iStrCount > 30) ? $iStrCount - 30 : 0;
                for ( $i = $iFirstStr; $i<$iStrCount; $i++ )
                    $sText .= $arr[ $i ] . '<br>';
            }
        }

        $this->actionLogs( $sText );
    }


    protected function actionClearLogs() {

        $sLogFile = ROOTPATH . '/log/access.log';
        $fh = fopen( $sLogFile, 'w' );
        fclose($fh);

        $sLogFile = ROOTPATH . '/log/error.log';
        $fh = fopen( $sLogFile, 'w' );
        fclose($fh);

        $this->actionLogs();
    }

    protected function actionSearch($sHeadText = '', $sText = ''){
        $form = StateBuilder::newEdit();

        $form->button(\Yii::t('utils', 'SearchDropAll'),'searchDropAll',\ExtDocked::iconReinstall);
        $form->button(\Yii::t('utils', 'resetActive'),'resetActive',\ExtDocked::iconReload);
        $form->button(\Yii::t('utils', 'reindex'),'reindex',\ExtDocked::iconReload);
        $form->button(\Yii::t('utils', 'rebuildSitemap'),'RebuildSitemap',\ExtDocked::iconInstall);
        $form->button(\Yii::t('utils', 'rebuildRobots'),'RebuildRobots',\ExtDocked::iconReload);

        $form->button(\Yii::t('adm','back'),'init','icon-back');

        if ( $sHeadText )
            $form->addHeadText( $sHeadText );

        if ( $sText ){
            $form->addField('text', 'text', 's', 'show', array('hideLabel' => 1));
            $form->setValue(array('text' => $sText));
        }

        $this->setInterface($form->getForm());
    }

    /**
     * "Пересобрать заново" - dropAll(), затем restoreAll()
     */
    protected function actionSearchDropAll(){

        Service::rebuildSearchIndex();
        $sText = \Yii::t('utils', 'search_drop_index');

        $this->actionSearch($sText);
    }

    /**
     * "Сбросить автивность записей" - сбросить флаг активности для всех записей
     */
    protected function actionResetActive(){

        $iAffectedRow =  SearchIndex::update()->set('status',0)->where('status',1)->get();
        $sText = \Yii::t('utils', 'record_update') . ": $iAffectedRow";

        $this->actionSearch($sText);
    }

    /**
     * "Переиндексировать неактивные" - запуск переиндексации для записей со статусом 0
     */
    protected function actionReindex(){

        $aData = $this->getInData();
        if ($this->get('params')){
            $aData = $this->get('params');
            $aData = $aData[0];
        }

        $iTask = (isset($aData['idTask']))?$aData['idTask']:0;

        $iTask = Service::makeSearchIndex( $iTask );

        $iRefreshCount = \SysVar::get('Search.updated');
        $sText = \Yii::t('utils', 'record_update') . ": $iRefreshCount";

        if ( $iTask ){
            $this->addJSListener( 'search_reindex', 'reindex' );
            $this->fireJSEvent( 'search_reindex', ['idTask' => $iTask] );

        }

        $this->actionSearch($sText);
    }

    /**
     * @todo проверить на большом объеме
     * Перестроим сайтмап
     */
    protected function actionRebuildSitemap(){

        $aData = $this->getInData();
        if ($this->get('params')){
            $aData = $this->get('params');
            $aData = $aData[0];
        }

        $iTask = (isset($aData['idTask']))?$aData['idTask']:0;

        $aResult = Service::makeSiteMap( $iTask );

        if ($aResult['status'] != Task::stError) {

            $sUrl = \Site::httpDomainSlash()."sitemap.xml";
            $sPath = WEBPATH."sitemap.xml";

            if ( file_exists($sPath) ) {
                $sText = htmlentities(file_get_contents($sPath));
                $sText = str_replace(' ', '&nbsp;', $sText);
                $sText = preg_replace('(http://.*\.xml)', '<a target="_blank" href="$0">$0</a>', $sText);
            } else {
                $sText = '';
            }

            $msg = nl2br(sprintf(
                '%s

                <a target="_blank" href="%s">%s</a>

                %s',
                \Yii::t('utils', 'sitemap_update_msg'),
                $sUrl,
                $sUrl,
                $sText
            ));
        }
        else
            $msg = \Yii::t('utils', 'sitemap_update_error');

        if ( $iTask ){
            $this->addJSListener( 'rebuildSitemap', 'rebuildSitemap' );
            $this->fireJSEvent( 'rebuildSitemap', ['idTask' => $aResult['id']] );
        }

        $this->actionSearch($msg);
    }

    /**
     * Перестроим роботс
     */
    protected function actionRebuildRobots(){

        $res = Service::updateRobotsTxt(Domains\Api::getMainDomain());

        if ($res){
            $msg = \Yii::t('utils', 'robots_update_msg');
        } else $msg = \Yii::t('utils', 'robots_update_error');

        $sRobots = file_get_contents(WEBPATH.'robots.txt');
        $sRobots = nl2br($sRobots);

        $this->actionSearch($msg, $sRobots);
    }

} 