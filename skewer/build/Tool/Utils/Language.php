<?php

$aLanguage = array();

$aLanguage['ru']['tab_name'] = 'Утилиты';

$aLanguage['ru']['drop_cache_act'] = 'Сбросить кэш';
$aLanguage['ru']['drop_cache_text'] = 'Установлен флаг обновления кэша. Данные обновятся автоматически при следующем обращении.';
$aLanguage['ru']['renew_act'] = 'Обновить страницу';
$aLanguage['ru']['clear_logs'] = 'Очистить логи';
$aLanguage['ru']['logs'] = 'Логи';
$aLanguage['ru']['search'] = 'Поиск';
$aLanguage['ru']['reset_search_index'] = 'Сброс поискового индекса';
$aLanguage['ru']['resetActive'] = 'Сброс метки поиского индекса';
$aLanguage['ru']['reindex'] = 'Обновить индекс';
$aLanguage['ru']['rebuildSitemap'] = 'Перестроить sitemap';
$aLanguage['ru']['rebuildRobots'] = 'Перестроить robots.txt';
$aLanguage['ru']['SearchDropAll'] = 'Удалить всё';
$aLanguage['ru']['rebuildLanguages'] = 'Перестроить языковые значения';

$aLanguage['ru']['view_access'] = 'Логи доступа';
$aLanguage['ru']['view_error'] = 'Логи ошибок';
$aLanguage['ru']['search_drop_index'] = 'Поисковый индекс очищен';
$aLanguage['ru']['record_update'] = 'Обновленно записей';
$aLanguage['ru']['new_update_msg'] = 'Запустите скрипт по новой, не все записи обновлены';
$aLanguage['ru']['sitemap_update_error'] = 'Ошибка обновления';
$aLanguage['ru']['sitemap_update_msg'] = 'sitemap обновлен';
$aLanguage['ru']['robots_update_error'] = 'Ошибка обновления';
$aLanguage['ru']['robots_update_msg'] = 'robots.txt обновлен';
$aLanguage['ru']['drop_languages_text'] = 'Языковые значения перестроены';


$aLanguage['en']['tab_name'] = 'Utils';

$aLanguage['en']['drop_cache_act'] = 'Drop cache';
$aLanguage['en']['drop_cache_text'] = 'Cache drop flag was seted';
$aLanguage['en']['renew_act'] = 'Renew page';
$aLanguage['en']['clear_logs'] = 'Clear logs files';
$aLanguage['en']['logs'] = 'Logs';
$aLanguage['en']['reset_search_index'] = 'Reset search index';
$aLanguage['en']['search'] = 'Search';
$aLanguage['en']['resetActive'] = 'reset active';
$aLanguage['en']['reindex'] = 'reindex';
$aLanguage['en']['rebuildSitemap'] = 'rebuild sitemap';
$aLanguage['en']['rebuildRobots'] = 'rebuild robots.txt';
$aLanguage['en']['SearchDropAll'] = 'remove all';
$aLanguage['en']['rebuildLanguages'] = 'Rebuild linguistic values';

$aLanguage['en']['view_access'] = 'Access logs';
$aLanguage['en']['view_error'] = 'Error logs';
$aLanguage['en']['search_drop_index'] = 'Search index cleared';
$aLanguage['en']['record_update'] = 'update records';
$aLanguage['en']['new_update_msg'] = 'Run the script on the new, not all records are updated';
$aLanguage['en']['sitemap_update_error'] = 'update error';
$aLanguage['en']['sitemap_update_msg'] = 'sitemap updated';
$aLanguage['en']['robots_update_error'] = 'update error';
$aLanguage['en']['robots_update_msg'] = 'robots.txt updated';
$aLanguage['en']['drop_languages_text'] = 'Language values rebuilt';

return $aLanguage;