<?php

namespace skewer\build\Tool\Patches\ar;

use skewer\build\Component\orm;
use skewer\build\libs\ft;

class Patches extends orm\TablePrototype{

    protected static $sTableName = 'patches';
    protected static $sKeyField = 'patch_uid';

    protected static function initModel() {

        ft\Entity::get(self::$sTableName)
            ->clear(false)
            ->setPrimaryKey(self::$sKeyField,'varchar(255)')
            ->setTablePrefix('')
            ->setNamespace(__NAMESPACE__)
            ->addField('install_date','timestamp','install_date')
            ->addField('file','varchar(255)','file')
            ->addField('description','varchar(255)','description')

            ->save()
            ->build();
    }

    public static function getNewRow($aData = array()) {
        $oRow = new PatchesRow();

        if ($aData)
            $oRow->setData($aData);

        return $oRow;
    }

} 