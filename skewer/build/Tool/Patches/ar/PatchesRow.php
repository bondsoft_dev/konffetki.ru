<?php

namespace skewer\build\Tool\Patches\ar;
use skewer\build\Component\orm;

class PatchesRow extends orm\ActiveRecord {


    public $patch_uid = '';
    public $install_date = '';
    public $file = '';
    public $description = '';

    function __construct() {
        $this->setTableName( 'patches' );
        $this->setPrimaryKey( 'patch_uid' );
    }

} 