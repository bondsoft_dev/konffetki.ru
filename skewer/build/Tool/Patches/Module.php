<?php
namespace skewer\build\Tool\Patches;


use skewer\build\Component\ReachGoal;
use skewer\build\Component\UI;
use skewer\build\Component\Forms;
use skewer\build\Tool;
use skewer\build\Page;
use yii\base\UserException;


class Module extends Tool\LeftList\ModulePrototype {

    /**
     * Поля в списке
     * @var array
     */
    protected $aListFields = array('patch_uid', 'install_date', 'description');

    protected function actionInit() {
        $this->actionList();
    }

    // func

    protected function actionList() {

        $this->setPanelName(\Yii::t('patches', 'availablepatches'));

        $oFormBuilder = UI\StateBuilder::newList();

        $oFormBuilder
            ->fieldString('patch_uid', \Yii::t('patches', 'patch_uid'))
            ->fieldString('install_date', \Yii::t('patches', 'install_date'), ['listColumns' => ['width' => 150]])
            ->fieldString('description', \Yii::t('patches', 'description'), ['listColumns' => ['flex' => 1]]);
        $aPaths = Api::getLocalList();

        $aItems = [];

        // но нам надо только за посление пол года
        foreach ($aPaths as $aItem) {
            $oDate = \DateTime::createFromFormat("Y-m-d H:i:s",$aItem['install_date']);

            if ($oDate && $oDate->diff(new \DateTime("now"))->m>6){
                break;
            } else $aItems[] = $aItem;
        }

        $oFormBuilder->setValue($aItems);
        $oFormBuilder->addRowButton(\Yii::t('patches', 'install'),'icon-install','installPatchForm','edit_form');

        // вывод данных в интерфейс
        $this->setInterface($oFormBuilder->getForm());
    }

    // func

    protected function actionInstallPatchForm() {

        $oForm = UI\StateBuilder::newEdit();

        try {

            $this->setPanelName(\Yii::t('patches', 'installPatch'), false);
            $aData = $this->get('data');


            if (!isSet($aData['file']) OR
                empty($aData['file']) OR
                !isSet($aData['patch_uid']) OR
                empty($aData['patch_uid'])
            ) throw new UserException(\Yii::t('patches', 'patchError'));

            /* Относительный путь к директории обновления */

            $aVal = array();

            $oForm
                ->fieldHide('patch_file', \Yii::t('patches', 'patch_file'), 's')
                ->field('patch_uid', \Yii::t('patches', 'patch_id'), 's', 'show')
                ->field('status', \Yii::t('patches', 'status'), 's', 'show')
                ->addFieldIf('description', \Yii::t('patches', 'description'), 's', 'show', [], !empty($aData['description']));


            $aVal['patch_file'] = $aData['file'];
            $aVal['patch_uid'] = $aData['patch_uid'];
            $aVal['status'] = ($aData['is_install']) ? \Yii::t('patches', 'installed') . $aData['install_date'] : $aData['install_date'];
            $aVal['description'] = $aData['description'];

            $oForm->setValue($aVal);

            /* Патч не устанавливали - разрешаем ставить */
            if (!$aData['is_install']) {
                $oForm->button(\Yii::t('patches', 'install'), 'installPatch', 'icon-install', 'allow_do', ['actionText' => \Yii::t('patches', 'installText')]);

            }

            $oForm->button(\Yii::t('adm','cancel'), 'List', 'icon-cancel');
            $oForm->buttonSeparator('->');

        } catch (UserException $e) {

            $this->addError($e->getMessage());
        }
        $this->setInterface($oForm->getForm());

        return psComplete;

    }

    // func

    protected function actionInstallPatch() {

        try {

            $aData = $this->get('data');

            if (!isSet($aData['patch_file']) OR empty($aData['patch_file']))
                throw new \UpdateException('Wrong parameters!');

            Api::installPatch($aData['patch_file']);

        } catch (\UpdateException $e) {

            $this->addError($e->getMessage());
        }

        $this->actionList();

    }
    // func
}

// class
