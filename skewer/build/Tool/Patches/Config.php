<?php

use skewer\build\Tool\LeftList;

$aConfig['name']     = 'Patches';
$aConfig['title']    = 'Патчи';
$aConfig['version']  = '1.1';
$aConfig['description']  = 'Патчи';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::TOOL;
$aConfig['group']     = LeftList\Group::SYSTEM;
$aConfig['useNamespace'] = true;

return $aConfig;
