<?php
namespace skewer\build\Tool\Patches;

use yii\web\ServerErrorHttpException;

class Api {

    /**
     * Возвращает список доступных к установке патчей в директории сайта либо false в случае ошибки или отсутствия патчей
     * @static
     * @param string $sRootPatchesDir Путь к корневой директории с патчами
     * @return array|bool
     */
    public static function getAvailablePatches($sRootPatchesDir) {
        if(!is_dir($sRootPatchesDir)) return false;

        $aOut = false;

        $oDir = dir($sRootPatchesDir);

        /** @noinspection PhpUndefinedFieldInspection */
        if($oDir->handle)
            while(false !== ($sFile = $oDir->read())){

                if($sFile == '.' and $sFile == '..') continue;
                if(!is_dir($sRootPatchesDir.$sFile)) continue;

                $sPatchFile = $sFile.DIRECTORY_SEPARATOR.$sFile.'.php';

                if(!file_exists($sRootPatchesDir.$sPatchFile)) continue;

                $aOut[] = $sPatchFile;
            }// h

        $oDir->close();

        return $aOut;
    }// func

    /**
     * Возвращает список примененных патчей на текущей площадке
     * @static
     * @param int $iPage страница постраничного
     * @param int $iOnPage Количество элементов на страницу
     * @return bool|array
     */
    public static function getAppliedPatches($iPage = 0, $iOnPage = 0) {

        $iPage = ($iPage)? --$iPage: $iPage;

        if(!(!$iPage AND !$iOnPage))
            $aFilter['limit'] = array( 'start' => $iPage, 'count' => $iOnPage );

        $aFilter['order'] = array('field' => 'install_date', 'way' => 'DESC');
        //$aFilter['where_condition']['section_id'] = array('sign' => '=', 'value' => (int)$iSectionId);

        $aPatches = Mapper::getItems($aFilter);

        return (isSet($aPatches['items']))? $aPatches['items']: false;

    }// func

    /**
     * Возвращает список доступных и установленных патчей
     * @static
     * @throws \UpdateException
     * @return array
     */
    public static function getLocalList() {

        /* Запросили список доступных к установке патчей */
        $aAvailable = static::getAvailablePatches(PATCHPATH);
        /* Запросили список установленных патчей */
        $aApplied   = static::getAppliedPatches();


        if(!$aAvailable AND !$aApplied) return array();

        $aAppliedUIDs = array();
        foreach($aApplied as $iKey=>$aPatch) {

            $aApplied[$iKey]['is_install'] = true;
            $aAppliedUIDs[] = $aPatch['patch_uid'];
        }// each installed patch

		if(is_array($aAvailable) AND count($aAvailable))
	        foreach($aAvailable as $sPatch) {
	            try {
	
	                $sPatchUID = basename($sPatch);
	
	                if(empty($sPatchUID)) continue;
	                if(in_array($sPatchUID, $aAppliedUIDs)) continue;
	
	                $aNewPatch['patch_uid']     = $sPatchUID;
	                $aNewPatch['install_date']  = 'Не установлен';
	                $aNewPatch['description']   = static::getDescFormFile( $sPatch );
	                $aNewPatch['is_install']    = false;
	                $aNewPatch['file']          = $sPatch;
	
	                array_unshift($aApplied, $aNewPatch);
	
	            } catch(\UpdateException $e) {
	                continue;
	            }
	        }// each available patch

        return $aApplied;
    }// func

    /**
     * Возвращает true, если патч с UID $sPatchUID ранее не устанавливался на данной площадке
     * @param string $sPatchUID
     * @return bool
     */
    public static function checkPatch($sPatchUID) {

        $aFilter['where_condition']['patch_uid'] = array('sign' => '=', 'value' => $sPatchUID);
        $aFilter['limit'] = array( 'start' => 0, 'count' => 1 );

        $aPatches = Mapper::getItems($aFilter);

        return !(bool)$aPatches['count'];
    }

    /**
     * Отдать описание из файла
     * @static
     * @param $sPatch
     * @return string
     */
    public static function getDescFormFile( $sPatch ) {

        // попробовать открыть и прочитать
        $sCont = file_get_contents( PATCHPATH.$sPatch );
        if ( !$sCont ) return '';

        // попробовать достать описание
        if ( preg_match( '/\$sDescription\s*=\s*[\'"]{1}(?<desc>.*)[\'"]{1};/i', $sCont, $aMatch ) ) {
            return $aMatch['desc'];
        }

        return $sCont;

    }


    /**
     * Установка патча.
     * @param $patch_file
     * @throws ServerErrorHttpException
     */
    public static function installPatch( $patch_file ){

        $oInstaller = new \skPatchInstaller(PATCHPATH . $patch_file);

        /* Устанавливаем проверку на то, что патч еще не устанавливался */
        $oInstaller->setChecker('skewer\build\Tool\Patches\Api::checkPatch');

        \ConfigUpdater::init();

        $oInstaller->install();

        //Установка времени последнего обновления
        \Design::setLastUpdatedTime();

        \ConfigUpdater::commit();

        /* Все прошло нормально - пишем о том, что патч поставили */
        Mapper::registerPatch(basename($patch_file), $patch_file, $oInstaller->getDescription());

    }

}// class
