<?php

$aLanguage = array();

$aLanguage['ru']['tab_name'] = 'Патчи';
$aLanguage['ru']['patch_uid'] = 'Патч';
$aLanguage['ru']['install_date'] = 'Дата установки';
$aLanguage['ru']['file'] = 'Файл';
$aLanguage['ru']['description'] = 'Описание';
$aLanguage['ru']['availablepatches'] = 'Доступные патчи';
$aLanguage['ru']['installPatch'] = 'Установка патча';

$aLanguage['ru']['patch_file'] = 'Путь к обновлению';
$aLanguage['ru']['patch_id'] = 'Номер обновления';
$aLanguage['ru']['install'] = 'Установить';
$aLanguage['ru']['installText'] = 'Вы действительно хотите <strong>установить</strong> данный патч?';
$aLanguage['ru']['status'] = 'Статус';
$aLanguage['ru']['installed'] = 'Установлен ';


$aLanguage['ru']['patchError'] = 'Ошибка: Отсутствуют данные о патче!';





$aLanguage['en']['tab_name'] = 'Patches';
$aLanguage['en']['patch_uid'] = 'Patch';
$aLanguage['en']['install_date'] = 'Install date';
$aLanguage['en']['file'] = 'File';
$aLanguage['en']['description'] = 'Description';
$aLanguage['en']['availablepatches'] = 'Available patches';
$aLanguage['en']['installPatch'] = 'Install patch';

$aLanguage['en']['patch_file'] = 'Patch file';
$aLanguage['en']['patch_id'] = 'Patch ID';
$aLanguage['en']['install'] = 'Install';
$aLanguage['en']['installText'] = 'Do you really want to <strong>install</strong> this patch?';
$aLanguage['en']['status'] = 'Status';
$aLanguage['en']['installed'] = 'Installed ';

$aLanguage['en']['patchError'] = 'Error: no find data!';

return $aLanguage;