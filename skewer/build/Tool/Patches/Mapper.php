<?php
namespace skewer\build\Tool\Patches;

use skewer\build\Component\orm\Query;

class Mapper extends \skMapperPrototype {

    /**
     * @var string Имя таблицы, с которой работает маппер
     */
    protected  static $sCurrentTable = 'patches';

    protected static $sKeyFieldName = 'patch_uid';

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'patch_uid'     => 'i:str:patch_uid',
        'install_date'  => 's:date:install_date',
        'file'          => 's:str:file',
        'description'   => 's:text:description',
    );

    public static function registerPatch($sUID, $sFile, $sDescription = '') {

        $sQuery = "
            INSERT INTO
              patches
            SET
              patch_uid=?,
              file=?,
              install_date=NOW(),
              description=?;";

        $res = Query::SQL($sQuery, $sUID, $sFile, $sDescription);

        return $res->lastId();

    }// func

}// class
