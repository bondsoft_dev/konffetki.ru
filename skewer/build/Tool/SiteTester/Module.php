<?php

namespace skewer\build\Tool\SiteTester;


use skewer\build\Tool;
use skewer\build\Component\SiteTester;
use skewer\build\libs\ExtBuilder;
use skewer\build\Component\UI;

/**
 * Модуль для автоматического тестирование сайта
 */
class Module extends Tool\LeftList\ModulePrototype {

    // перед выполнением
    protected function preExecute() {



    }

    protected function getFieldList() {
        return array(
            'testName' => array(
                \Yii::t('siteTester', 'field_test_name'),
                5,
            ),
            'class' => array(
                'class',
                0,
                ),
            'type' => array(
                \Yii::t('siteTester', 'field_test_type'),
                1,
            ),
            'status' => array(
                \Yii::t('siteTester', 'field_test_status'),
                1,
            ),
        );
    }

    protected function getDetailFieldList() {
        return array(
            'time' => array(
                \Yii::t('siteTester', 'field_datetime'),
                1,
            ),
            'status' => array(
                \Yii::t('siteTester', 'field_status'),
                1,
            ),
            'text' => array(
                \Yii::t('siteTester', 'field_message'),
                6,
            )
        );
    }




    protected function actionInit() {

        $this->actionList();
    }// func

    protected function actionList() {

        $oList = new \ExtList();

        foreach($this->getFieldList() as $name => $field){

            $oField = new ExtBuilder\Field\String();

            $oField->setName($name);
            $oField->setTitle($field[0]);
            $oField->setAddListDesc(array(
                'flex' => $field[1],
            ));

            $oList->addField( $oField );
        }

        $oList->setValues($this->getTestLists());

        $oList->addRowBtnUpdate();


        $oList->addExtButton(
            \ExtDocked::create(\Yii::t('siteTester', 'start'))
                ->setTitle(\Yii::t('siteTester', 'start'))
                ->setIconCls( \ExtDocked::iconInstall )
                ->setAction('start')
                ->unsetDirtyChecker()
        );




        $this->setInterface( $oList );


    }

    protected function actionStart() {

        $aTests = array();
        $api = new SiteTester\Api();

        $aTests = (SiteTester\Api::getSiteMode() == 'prod') ? $api->getProdTestList() : $api->getDevTestList();


        foreach($aTests as $test) {
            $instance = new $test();
            $instance->run();
        }


        $this->actionList();

    }


    protected function actionShow() {

        $data = $this->get('data');



        $info = SiteTester\Api::getInfo($data['class']);


        if (!$info) $this->actionList();
        else {

            $oList = new \ExtList();

            foreach($this->getDetailFieldList() as $name => $field){

                $oField = new ExtBuilder\Field\String();

                $oField->setName($name);
                $oField->setTitle($field[0]);
                $oField->setAddListDesc(array(
                    'flex' => $field[1],
                ));

                $oList->addField( $oField );
            }
            $oList->setValues($info['messages']);

            $oList->addExtButton(
                \ExtDocked::create(\Yii::t('siteTester', 'back'))
                    ->setTitle(\Yii::t('siteTester', 'SiteTester.back'))
                    ->setIconCls( \ExtDocked::iconCancel )
                    ->setAction('list')
                    ->unsetDirtyChecker()
            );

            $oList->setAddText('<br><p><b>'.\Yii::t('siteTester', 'field_test_name').'</b>: '. $data['testName'].'</p><p><b>'
                .\Yii::t('siteTester', 'field_test_status').'</b>: <span style="color: '.SiteTester\Status::getColor($info['status']).'">'
                .\Yii::t('siteTester', 'st_'.$info['status']).'</span></p>');

            $this->setInterface( $oList );


        }


    }


    protected function getTestLists() {

        $aOut = array();
        $api = new SiteTester\Api();

        $aProdTests = $api->getProdTestList();
        foreach($aProdTests as $sTest ) {
            $aOut[$sTest] = array(
                'testName' => $sTest::$name,
                'class' => $sTest,
                'type'     => 'prod',
                'status'   => sprintf(
                    '<span style="color: %s">%s</span>',
                    SiteTester\Status::getColor(SiteTester\Api::getStatus($sTest)),
                    \Yii::t('siteTester', 'st_'.SiteTester\Api::getStatus($sTest))
                )
            );
        }



        $aDevTests  = $api->getDevTestList();
        foreach($aDevTests as $sTest ) {

            if (isset($aOut[$sTest])) $aOut[$sTest]['type'] = 'prod/dev';
            else {
                $aOut[$sTest] = array(
                    'testName' => $sTest::$name,
                    'type'     => 'dev',
                    'status'   => sprintf(
                        '<span style="color: %s">%s</span>',
                        SiteTester\Status::getColor(SiteTester\Api::getStatus($sTest)),
                        \Yii::t('siteTester', 'st_'.SiteTester\Api::getStatus($sTest))
                    )
                );
            }

        }

        return $aOut;
    }



    

}
