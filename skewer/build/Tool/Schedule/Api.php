<?php

namespace skewer\build\Tool\Schedule;

use skewer\build\Tool\Schedule\fields\Day;
use skewer\build\Tool\Schedule\fields\DOW;
use skewer\build\Tool\Schedule\fields\Hour;
use skewer\build\Tool\Schedule\fields\Minute;
use skewer\build\Tool\Schedule\fields\Month;
use skewer\build\Tool\Schedule\fields\Priority;
use skewer\build\Tool\Schedule\fields\Resource;
use skewer\build\Tool\Schedule\fields\Status;
use skewer\build\Tool\Schedule\fields\Target;

class Api {

    /**
     * Набор полей для списка
     * @static
     * @return array
     */
    public static function getListFields(){
        return array('id','title','c_min','c_hour','c_day','c_month','c_dow','priority','resource_use','target_area','status');
    }

    /**
     * дополнительный набор параметров
     * @static
     * @return array
     */
    protected static function getAddParamList() {
        return array(
            'id' => array(
                'listColumns' => array(
                    'hidden' => true
                )
            ),
            'title' => array(
                'allowBlank' => false,
                'listColumns' => array('flex' => 1),
            ),

            'priority' => new Priority(),
            'resource_use' => new Resource(),
            'target_area' => new Target(),
            'status' => new Status(),
            'c_min' => new Minute(),
            'c_hour' => new Hour(),
            'c_day' => new Day(),
            'c_month' => new Month(),
            'c_dow' => new DOW(),

        );

    }

    /**
     * Отдает модель данных для списка
     * @return array
     */
    public static function getListModel() {

        // набор полей для списка
        $aFieldFilter = static::getListFields();

        // описания полей
        $aParamList = \Ext::getParamFields(
            Mapper::getFullParamDefList($aFieldFilter),
            static::getAddParamList()
        );

        // описания полей
        return $aParamList;

    }

    /**
     * Отдает модель данных для формы редактирования
     * @return array
     */
    public static function getEditModel() {

        // взять все поля
        $aFieldFilter = Mapper::getParamNames();

        // описания полей
        $aParamList = \Ext::getParamFields(
            Mapper::getParamDefList($aFieldFilter),
            static::getAddParamList()
        );

        return $aParamList;

    }


}
