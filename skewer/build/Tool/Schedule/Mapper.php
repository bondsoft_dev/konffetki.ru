<?php

namespace skewer\build\Tool\Schedule;
use skewer\build\Component\orm\Query;

/**
 * Класс для работы с таблицей расписаний
 */

class Mapper extends \skMapperPrototype {

    /** @var string Имя таблицы, с которой работает маппер */
    protected static $sCurrentTable = 'schedule';

    /** @var array Конфигурация полей таблицы */
    protected static $aParametersList = array(
        'id' => 'i:hide:Schedule.id',
        'title' => 's:str:Schedule.title:Schedule.name',
        'name' => 's:str:Schedule.name',
        'command' => 's:str:Schedule.command',
        'priority' => 'i:int:Schedule.priority',
        'resource_use' => 'i:int:Schedule.resource_use',
        'target_area' => 'i:int:Schedule.target_area',
        'status' => 'i:int:Schedule.status',
        'c_min' => 'i:int:Schedule.c_min',
        'c_hour' => 'i:int:Schedule.c_hour',
        'c_day' => 'i:int:Schedule.c_day',
        'c_month' => 'i:int:Schedule.c_month',
        'c_dow' => 'i:int:Schedule.c_dow',
    );

    /**
     * Отдает все записи
     * @static
     * @return array
     */
    public static function getAllItems(){

        // формирование критериев выборки
        $aFilter = array(
            'order' => array(
                'field' => 'title',
                'way' => 'ASC'
            )
        );

        // запрос данных
        $aItems = static::getItems( $aFilter );

        return $aItems['items'];

    }

    /**
     * получение ид задания в рассписании по имени
     * @static
     * @param $name
     * @return bool
     */
    public static function getIdByName($name){
        $res = Query::SQL("SELECT id FROM `schedule` WHERE `name`=?",$name);

        if ( !$res ) return false;

        if( $aRow = $res->fetchArray() ){
            $sResult = $aRow['id'];
            return $sResult;
        }

        return false;
    }

}
