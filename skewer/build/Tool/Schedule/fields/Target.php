<?php

namespace skewer\build\Tool\Schedule\fields;


class Target extends SelectPrototype {

    /** @var array набор доступных значений */
    protected  $aList = array(
        1 => 'target_site',
        2 => 'target_server',
        3 => 'target_system',
    );

    /** @var mixed стандартное значение */
    protected $mDefault = 1;
} 