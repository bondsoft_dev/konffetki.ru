<?php

namespace skewer\build\Tool\Schedule\fields;

use skewer\build\libs\ExtBuilder\Field\SelectByArray;

class SelectPrototype extends SelectByArray {

    /**
     * @inheritdoc
     * @return array
     */
    function getList()
    {
        return array_map(function($s){ return \Yii::t('tasks', $s);}, parent::getList());
    }
} 