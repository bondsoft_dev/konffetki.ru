<?php

namespace skewer\build\Tool\Schedule\fields;

/**
 * Поле часы
 */
class Hour extends Minute {
    /** @var int максимальное число */
    protected $iMax = 23;
} 