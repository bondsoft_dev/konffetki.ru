<?php

namespace skewer\build\Tool\Schedule\fields;

/**
 * Набор классов для полей
 */

class Priority extends SelectPrototype {

    /** @var array набор доступных значений */
    protected $aList = array(
        1 => 'priority_low',
        2 => 'priority_normal',
        3 => 'priority_high',
        4 => 'priority_critical',
    );

    /** @var mixed стандартное значение */
    protected $mDefault = 2;
} 