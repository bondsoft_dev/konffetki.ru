<?php


namespace skewer\build\Tool\Schedule\fields;

class Status extends SelectPrototype {

    /** @var array набор доступных значений */
    protected $aList = array(
        1 => 'status_active',
        2 => 'status_stopped',
        3 => 'status_paused',
    );

    /** @var mixed стандартное значение */
    protected $mDefault = 1;

} 