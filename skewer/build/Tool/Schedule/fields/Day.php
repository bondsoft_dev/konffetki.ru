<?php


namespace skewer\build\Tool\Schedule\fields;

/**
 * Поле дни
 */
class Day extends Minute {

    /** @var int минимальное число */
    protected $iMin = 1;

    /** @var int максимальное число */
    protected $iMax = 31;
} 