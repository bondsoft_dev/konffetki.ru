<?php

namespace skewer\build\Tool\Schedule\fields;

/**
 * Поле источник
 */
class Resource extends SelectPrototype  {

    /** @var array набор доступных значений */
    protected  $aList = array(
        3 => 'resource_background',
        4 => 'resource_normal',
        7 => 'resource_intensive',
        9 => 'resource_critical',
    );

    /** @var mixed стандартное значение */
    protected $mDefault = 2;
}