<?php


namespace skewer\build\Tool\Schedule\fields;

use skewer\build\libs\ExtBuilder;

/**
 * Поле минуты
 */
class Minute extends ExtBuilder\Field\Prototype {

    /** @var int минимальное число */
    protected $iMin = 0;

    /** @var int максимальное число */
    protected $iMax = 59;

    /**
     * Значение по умолчанию
     * @return int
     */
    public function getDefaultVal() {
        return '';
    }

    /**
     * Проверяет доступность заданного значения
     * @return bool
     */
    function isValid() {
        $v = $this->getSaveValue();
        if ( is_null($v) ) {
            return true;
        } elseif ( is_numeric($v) ) {
            return ($v>=$this->iMin and $v<=$this->iMax );
        } else {
            return false;
        }
    }

    /**
     * Отдает текстовое название для установленного значения
     * @return string
     */
    function getListShowValue() {
        $mVal = $this->isValid() ? $this->getValue() : '-ошибка-';
        if ( is_null($mVal) ) $mVal = '*';
        return $mVal;
    }

    /**
     * Отдает расширяющее описание элемента для формы
     * @return array
     */
    function getFormFieldDesc() {
        return array(
            // пусто - значение снято
            'allowBlank' => true,
            // ограничения
            'maxValue' => $this->iMax,
            'minValue' => $this->iMin
        );

    }

    /**
     * Отдает расширяющее описание элемента для списка
     * @return array
     */
    function getListFieldDesc() {
        return array(
            'width' => 40,
            'align' => 'center',
            'text' => substr( $this->getName(), 2 )
        );
    }

    /**
     * Возвращает значение для сохранения
     * @return mixed
     */
    public function getSaveValue(){
        $mVal = $this->getValue();
        if ( $mVal === '' )
            $mVal = null;
        return $mVal;
    }

    function getView() {
        return 'num';
    }

} 