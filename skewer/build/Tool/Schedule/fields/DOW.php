<?php


namespace skewer\build\Tool\Schedule\fields;

class DOW extends Minute{

    /** @var int минимальное число */
    protected $iMin = 1;

    /** @var int максимальное число */
    protected $iMax = 7;

} 