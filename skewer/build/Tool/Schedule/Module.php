<?php

namespace skewer\build\Tool\Schedule;


use skewer\build\Component\UI\StateBuilder;
use skewer\build\Tool;

/**
 * Интерфейс для работы с планировщиком задач
 * @todo переписать!!!
 */
class Module extends Tool\LeftList\ModulePrototype {

    /**
     * Первичное состояние
     */
    protected function actionInit() {

        // вывод списка
        $this->actionList();

    }

    /**
     * Список пользователей
     */
    protected function actionList() {

        // установка заголовка
        $this->setPanelName( \Yii::t('tasks', 'taskList') );

        $oFormBuilder = new StateBuilder();
        // создаем форму

        $oFormBuilder = $oFormBuilder::newList();
        //['id','title','c_min','c_hour','c_day','c_month','c_dow','priority','resource_use','target_area','status']
        $oFormBuilder
            ->addField('id', 'ID', 'i', 'string',array( 'listColumns' => array('flex' => 1) ) )

            ->addField('title', \Yii::t('tasks', 'title'), 's', 'string',array( 'listColumns' => array('flex' => 3) ) )

            ->addField('c_min', \Yii::t('tasks', 'c_min'), 's', 'string',array( 'listColumns' => array('flex' => 1) ) )
            ->addField('c_hour', \Yii::t('tasks', 'c_hour'), 's', 'string',array( 'listColumns' => array('flex' => 1) ) )
            ->addField('c_day', \Yii::t('tasks', 'c_day'), 's', 'string',array( 'listColumns' => array('flex' => 1) ) )
            ->addField('c_month', \Yii::t('tasks', 'c_month'), 's', 'string',array( 'listColumns' => array('flex' => 1) ) )
            ->addField('c_dow', \Yii::t('tasks', 'c_dow'), 's', 'string',array( 'listColumns' => array('flex' => 2) ) )

            ->addField('priority', \Yii::t('tasks', 'priority'), 's', 'string',array( 'listColumns' => array('flex' => 2) ) )
            ->addField('resource_use', \Yii::t('tasks', 'resource_use'), 's', 'string',array( 'listColumns' => array('flex' => 2) ) )
            ->addField('target_area', \Yii::t('tasks', 'target_area'), 's', 'string',array( 'listColumns' => array('flex' => 2) ) )
            ->addField('status', \Yii::t('tasks', 'status'), 's', 'string',array( 'listColumns' => array('flex' => 2) ) )

            ->addRowButtonUpdate()
            //->addRowButtonDelete()

            ->addButton(\Yii::t('adm','add'),'show','icon-add')
        ;

        /** todo вот тут в будущем должен нормальный запросник из API быть, сейчас мне делать это лениво и долго.
         *
         */
        $items =  Mapper::getAllItems();

        foreach($items as &$item){
            if (is_null($item['c_min'])) $item['c_min'] = '*';
            if (is_null($item['c_hour'])) $item['c_hour'] = '*';
            if (is_null($item['c_day'])) $item['c_day'] = '*';
            if (is_null($item['c_month'])) $item['c_month'] = '*';
            if (is_null($item['c_dow'])) $item['c_dow'] = '*';

            /** todo создаем объект для каждой записи. Серьезно? Омайгадбл */
            $target = new \skewer\build\Tool\Schedule\fields\Target();
            $item['target_area'] = \Yii::t('tasks', $target->getList()[$item['target_area']]);

            $status = new \skewer\build\Tool\Schedule\fields\Status();
            $item['status'] = \Yii::t('tasks', $status->getList()[$item['status']]);

            $resours = new \skewer\build\Tool\Schedule\fields\Resource();
            $item['resource_use'] = \Yii::t('tasks', $resours->getList()[$item['resource_use']]);

            $priority = new \skewer\build\Tool\Schedule\fields\Priority();
            $item['priority'] = \Yii::t('tasks', $priority->getList()[$item['priority']]);
        }

        $oFormBuilder->setValue($items);

        $this->setInterface($oFormBuilder->getForm());

    }

    /**
     * Отображение формы
     */
    protected function actionShow() {

        // взять id ( 0 - добавление, иначе сохранение )
        $iItemId = (int)$this->getInDataVal('id');

        // заголовок
        $this->setPanelName( $iItemId ? \Yii::t('tasks', 'edit') : \Yii::t('tasks', 'add') );

        // элемент
        $aItem = $iItemId ? Mapper::getItem($iItemId) : array();

        if ($aItem){
            $aItem['priority'] = \Yii::t('tasks', $aItem['priority']);
            $aItem['resource_use'] = \Yii::t('tasks', $aItem['resource_use']);
            $aItem['target_area'] = \Yii::t('tasks', $aItem['target_area']);
            $aItem['status'] = \Yii::t('tasks', $aItem['status']);
        }

        // если нет требуемой записи
        if ( $iItemId and !$aItem )
            throw new \Exception(\Yii::t('tasks', 'selectError') );

        // подключить автоматический генератор форм
        $oForm = new \ExtForm();

        // модель данных формы
        $oForm->setFields( Api::getEditModel() );

        // установить значения для элементов
        if ( $iItemId )
            $oForm->setValues( $aItem );
        else
            $oForm->setDefaultValues();

        // добавление кнопок
        $oForm->addBtnSave();
        $oForm->addBtnCancel();

        // кнопка удаления для заданного объекта
        if ( $iItemId ) {
            $oForm->addBtnSeparator('->');
            $oForm->addBtnDelete();
        }

        // вывод данных в интерфейс
        $this->setInterface( $oForm );
    }

    /**
     * Сохранение
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->get( 'data' );

        // подключить автоматический генератор форм
        $oValidator = new \ExtValidator();

        // модель данных формы
        $oValidator->setFields( Api::getEditModel() );

        // установить значения для элементов
        $oValidator->setValues( $aData );

        // валидация при сохранении
        if ( !$oValidator->validate($aData) )
            throw new \Exception( sprintf( \Yii::t('tasks', 'validateError').'<br />%s<br />', implode('<br />',$oValidator->getMessages()) ) );

        // преобразование сохраняемых данных объектами полей
        $aData = $oValidator->getSaveValues();

        // есть данные
        if ( $aData ){

            // сохранить
            Mapper::saveItem( $aData );

            // добавление записи в лог
            if ( isset($aData['id']) && $aData['id'] ) {
                $this->addMessage(\Yii::t('tasks', 'updateOk'));
                $this->addModuleNoticeReport(\Yii::t('tasks', 'editTabName'),$aData);
            } else {
                unset($aData['id']);
                $this->addMessage(\Yii::t('tasks', 'addOk'));
                $this->addModuleNoticeReport(\Yii::t('tasks', 'addTabName'),$aData);
            }
        }

        // вывод списка
        $this->actionInit();

    }


    /**
     * Удаление
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        // id записи
        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        // удаление
        Mapper::delItem( $iItemId );

        // добавление записи в лог
        $this->addMessage('Запись удалена');
        $this->addModuleNoticeReport("Удаление записи планировшика",$aData);

        // вывод списка
        $this->actionInit();

    }

}
