<?php
namespace skewer\build\Tool\Logger;


use skewer\build\Component\UI;
use skewer\build\Tool;
use skewer\models\Log;

class Module extends Tool\LeftList\ModulePrototype {

    protected $iOnPage = 20;
    protected $iPage = 0;

    // фильтр по модулям
    protected $mModuleFilter = false;
    // фильтр по пользователю
    protected $mLoginFilter = false;
    // фильтр по уровню событий
    protected $mLevelFilter = false;
    // фильтр по типу журнала
    protected $mLogFilter = false;
    // фильтры по датам
    protected $mDateFilter1 = false;
    protected $mDateFilter2 = false;

    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt('page');

        // запрос значений фильтров
        $this->mModuleFilter = $this->get('module',false);
        $this->mLoginFilter = $this->get('login',false);
        $this->mLevelFilter = $this->get('event_type',false);
        $this->mLogFilter = $this->get('log_type',false);
        $this->mDateFilter1 = $this->getDateFilter('date1');
        $this->mDateFilter2 = $this->getDateFilter('date2');

    }

    /**
     * Запрашивает значение фильтра по дате с валидацией
     * @param string $sName имя фильтра
     * @return bool|string
     */
    protected function getDateFilter( $sName ) {

        // запрос значения
        $mVal = $this->getStr($sName,false);

        // проверка значения
        if ( !$mVal ) return false;

        // валидация
        if ( preg_match('/^\d{4}-\d{2}-\d{2}$/',$mVal) ) {
            return $mVal;
        } else {
            return false;
        }

    }

    public function actionInit(){

        $oList = UI\StateBuilder::newList()

            ->fieldString('login',        \Yii::t('Logger','login'))
            ->fieldString('event_time',   \Yii::t('Logger','field_event_time'))
            ->fieldString('title',        \Yii::t('Logger','field_title'), ['listColumns' => [ 'flex' => 2 ]])
            ->fieldString('module_title', \Yii::t('Logger','field_module_title'), ['listColumns' => [ 'width' => 150 ]])
            ->fieldString('event_title',  \Yii::t('Logger','field_event_title'))
            ->fieldString('log_title',    \Yii::t('Logger','field_log_title'), ['listColumns' => [ 'width' => 150 ]])
            ->fieldString('ip',           \Yii::t('Logger','field_ip'))

            // фильтр по пользователям
            ->addFilterSelect( 'login',     Api::getUsersLogin(), $this->mLoginFilter, \Yii::t('logger', 'user') )

            // фильтр по модулям
            ->addFilterSelect( 'module',    Api::getModules(), $this->mModuleFilter, \Yii::t('logger', 'module') )

            // фильтр по уровню событий
            ->addFilterSelect( 'event_type',Api::getEventLevels(), $this->mLevelFilter, \Yii::t('logger', 'event_type') )

            // фильтр по уровню событий
            ->addFilterSelect( 'log_type',  Api::getLogTypes(), $this->mLogFilter, \Yii::t('logger', 'log_type') )

            // фильтр по дате
            ->addFilterDate( 'date', [$this->mDateFilter1, $this->mDateFilter2], \Yii::t('logger', 'date') );

        // логи чистить может только sys
        if (\CurrentAdmin::isSystemMode()){
            // кнопка очистки логов
            $oList->addFilterButton('clearLog', \Yii::t('logger', 'deleteLogs'), \Yii::t('logger', 'deleteLogsText'));
        }

        // добавление кнопок вывода детальной

        $oList->addRowButton(
            \Yii::t('logger', 'detailPage'),    // tooltip
            'icon-edit',                        // iconCls
            'showForm',                         // action
            'edit_form'                         // state
        );


        // собираем заброс и получаем данные

        $aFilter =
            [
                'limit' => $this->iOnPage ?
                    [
                        'start' => $this->iPage*$this->iOnPage,
                        'count' => $this->iOnPage
                    ] : false,

                'order' =>
                    [
                        'field' => 'event_time',
                        'way' => 'DESC'
                    ]
            ];

        /*
         * Фильтры
         */

        // по названию модуля
        if ( false !== $this->mModuleFilter )   $aFilter['module'] = $this->mModuleFilter;

        // по логину
        if ( false !== $this->mLoginFilter )    $aFilter['login'] = $this->mLoginFilter;

        // по уровню доступа
        if ( false !== $this->mLevelFilter )    $aFilter['event_type'] = $this->mLevelFilter;

        // по типу журнала
        if ( false !== $this->mLogFilter )      $aFilter['log_type'] = $this->mLogFilter;

        // по дате
        if ( $this->mDateFilter1 and $this->mDateFilter2 ){ // если заданы оба параметра

            $aFilter['event_time'] = [
                'sign' => 'BETWEEN',
                'value' => array($this->mDateFilter1, $this->mDateFilter2.' 23:59:59')
            ];

        } elseif ( $this->mDateFilter1 ) { // если только первый

            $aFilter['event_time'] = [
                'sign' => '>=',
                'value' => $this->mDateFilter1
            ];

        } elseif ( $this->mDateFilter2 ) { // если только второй

            $aFilter['event_time'] = [
                'sign' => '<=',
                'value' => $this->mDateFilter2.' 23:59:59'
            ];
        }

        // добавление набора данных
        $aItems = Api::getListItems($aFilter);

        $oList->setValue(
            $aItems['items'],
            $this->iOnPage,
            $this->iPage,
            $aItems['count']
        );


        // вывод данных в интерфейс
        $this->setInterface( $oList->getForm() );

    }

    protected function actionClearLog(){

        // логи чистить может только sys
        if (\CurrentAdmin::isSystemMode()){
            Api::clearLog();
        }

        $this->actionInit();
    }

    /**
     * Отображение формы
     */
    protected function actionShowForm() {

        // собираем структуру интерфейса

        $oForm = UI\StateBuilder::newEdit()
            ->fieldShow('id',           \Yii::t('Logger','field_id'))
            ->fieldShow('event_time',   \Yii::t('Logger','field_event_time'))
            ->fieldShow('event_type',   \Yii::t('Logger','field_event_type'))
            ->fieldShow('log_type',     \Yii::t('Logger','field_log_type'))
            ->fieldShow('title',        \Yii::t('Logger','field_title'))
            ->fieldShow('module',       \Yii::t('Logger','field_module'))
            ->fieldShow('initiator',    \Yii::t('Logger','field_initiator'))
            ->fieldShow('user',         \Yii::t('Logger','user'))
            ->fieldShow('ip',           \Yii::t('Logger','field_ip'))
            ->fieldShow('proxy_ip',     \Yii::t('Logger','field_proxy_ip'))
            ->fieldShow('external_id',  \Yii::t('Logger','field_external_id'));

        if(\CurrentAdmin::isSystemMode())
            $oForm->fieldShow('description', \Yii::t('Logger','field_description'),'s', ['labelAlign' => 'top']);

        $oForm->buttonCancel();


        // наполняем интерфейс данными

        $aData = $this->get('data');
        $iItemId = (is_array($aData) && isset($aData['id'])) ? (int)$aData['id'] : 0;

        if($oItem = Log::findOne($iItemId)) {

            $aItem = $oItem->getAttributes();

            $aModules = Api::getModules();

            // замена индексов на строки
            $aItem['event_type'] = Api::getLevelTitle( $oItem->event_type);
            $aItem['log_type'] = Api::getTypeTitle($aItem['log_type']);
            $aItem['module'] = isset($aModules[$oItem->module]) ? $aModules[$oItem->module] : 'unknown';
            $aItem['title'] = \Yii::t('logger', $aItem['title']);

            // пользователь
            $aUser = \AuthUsersMapper::getUserData($oItem->initiator);
            if ( $aUser )
                $aItem['user'] = sprintf('%s (%s)', $aUser['login'], $aUser['name']);
            else
                $aItem['user'] = sprintf('unknown');


            // форматирование данных описания
            $aDesc = json_decode($oItem->description);
            if ( !json_last_error() )
                $aItem['description'] = '<pre>'.print_r((array)$aDesc,true).'</pre>';

            $oForm->setValue($aItem);
        }

        $this->setInterface($oForm->getForm());

    }


    /**
     * Установка служебных данных
     * @param UI\State\BaseInterface $oIface
     */
    protected function setServiceData( UI\State\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData( array(
            'module' => $this->mModuleFilter,
            'event_type' => $this->mLevelFilter,
            'log_type' => $this->mLogFilter,
            'page' => $this->iPage,
            'date1' => $this->mDateFilter1,
            'date2' => $this->mDateFilter2,
        ) );

    }

}//class