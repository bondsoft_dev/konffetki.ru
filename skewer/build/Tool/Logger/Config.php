<?php

use skewer\build\Tool\LeftList;

$aConfig['name']     = 'Logger';
$aConfig['title']     = 'Система логирования';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Система логирования';
$aConfig['revision'] = '0001';
$aConfig['useNamespace'] = true;
$aConfig['layer']     = \Layer::TOOL;
$aConfig['group']     = LeftList\Group::SYSTEM;

return $aConfig;
