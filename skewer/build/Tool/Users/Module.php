<?php

namespace skewer\build\Tool\Users;

use skewer\build\Component\UI;
use skewer\build\Tool;
use yii\base\UserException;

class Module extends Tool\LeftList\ModulePrototype{

    // id текущего раздела
    protected $iSectionId = 0;

    // число элементов на страницу
    protected $iOnPage = 20;

    // фильтр по политике
    protected $mPolicyFilter = false;

    // фильтр по активности пользователей
    protected $mActiveFilter = false;

    // фильтр по тексту
    protected $sSearchFilter = '';

    // текущий номер страницы
    protected $iPage = 0;

    /**
     * Метод, выполняемый перед action меодом
     * @throws UserException
     * @return bool
     */
    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt('page');

        // id текущего раздела
        $this->iSectionId = $this->getInt('sectionId');

        // фильтры
        $this->mPolicyFilter = $this->get('policy',false);
        $this->mActiveFilter = $this->get('active',false);
        $this->sSearchFilter = $this->getStr('search');

    }


    /**
     * Первичное состояние
     */
    protected function actionInit() {

        // вывод списка
        $this->actionList();

    }

    protected function setListItems( \ExtList &$oList ) {

        // число записей на страницу
        $oList->setOnPage( $this->iOnPage );
        $oList->setPageNum( $this->iPage );

        // добавление набора данных
        $aFilter = array(
            'limit' => $this->iOnPage ? array (
                'start' => $this->iPage*$this->iOnPage,
                'count' => $this->iOnPage
            ) : false,

            /*Сортировка только по одному полю */
            'order' => array(
                'field' => 'login',
                'direction' => 'ASC',
            )

        );

        /*
         * Фильтры
         */

        // по политике
        if ( false !== $this->mPolicyFilter ) {
            $aFilter['policy'] = (int)$this->mPolicyFilter;
        }

        // по активновти
        if ( false !== $this->mActiveFilter ) {
            $aFilter['active'] = (int)$this->mActiveFilter;
        }

        // по тексту
        if ( $this->sSearchFilter ) {
            $aFilter['search'] = $this->sSearchFilter;
        }

        // только пользователи с паролем для не системных администраторов
        if(!\CurrentAdmin::isSystemMode())
            $aFilter['has_pass'] = true;

        // запросить всех пользователей
        $aItems = Api::getUsersList($aFilter);

        // форматирование данных
        foreach ( $aItems['items'] as $iKey => $aRow ) {
            // дата последнего захода
            if ( $aRow['lastlogin'] <= 1900 )
                $aItems['items'][$iKey]['lastlogin'] = '-';
        }

        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );

    }

    /**
     * Список пользователей
     */
    protected function actionList() {

        $this->setPanelName( \Yii::t('auth', 'userList') );

        // объект для построения списка
        $oList = new \ExtList();

        /**
         * Модель данных
         */

        $aFieldFilter = Mapper::getListFields();

        $aListModel = Api::getParamList($aFieldFilter);

        // задать модель данных для вывода
        $oList->setFields( $aListModel );

        /**
         * Фильтры
         */

        // текстовый фильтр
        $oList->addFilterText( 'search', $this->sSearchFilter );

        // добавляем фильтр по политикам
        $oList->addFilterSelect( 'policy', Api::getAllowedPolicyList(), $this->mPolicyFilter, \Yii::t('auth', 'policy') );

        // Активность
        $oList->addFilterSelect( 'active', array(
            1 => \Yii::t('auth', 'active'),
            0 => \Yii::t('auth', 'inactive')
        ), $this->mActiveFilter, \Yii::t('auth', 'activity') );

        /**
         * Данные
         */
        $this->setListItems( $oList );

        /**
         * Интерфейс
         */

        // добавление кнопок
        $oList->addRowBtnUpdate();

        // кнопка добавления
        $oList->addBtnAdd('show');

        // вывод данных в интерфейс
        $this->setInterface( $oList );
    }

    /**
     * Отображение формы
     * @throws \Exception
     */
    protected function actionShow() {

        // взять id ( 0 - добавление, иначе сохранение )
        $iItemId = (int)$this->getInDataVal('id');

        // заголовок
        $this->setPanelName( $iItemId ? \Yii::t('auth', 'editing') : \Yii::t('auth', 'adding') );

        // запись новости
        $aItem = $iItemId ? Api::getUser($iItemId) : Api::getBlankValues();

        // если нет требуемой записи
        if ( $iItemId and !$aItem )
            throw new \Exception(\Yii::t('auth', 'item_not_exists'));

        // есть id - должны быть и права на доступ
        $iItemId and Api::testAccessToUser($iItemId);

        // подключить автоматический генератор форм
        $oForm = new \ExtForm();

        // установить набор элементов формы
        $aFieldFilter = Mapper::getDetailFields( $iItemId );

        if ( $iItemId )
            $aFieldFilter['group_policy_id']['disabled'] = 'true';

        $bHide = false;
        $aDefaultUser = \AuthUsersMapper::getDefaultUserData();
        if (isset($aDefaultUser['id'])){
            $iDefaultId = $aDefaultUser['id'];
            if ($iItemId == \CurrentAdmin::getId() || $iItemId == $iDefaultId){
                $bHide = true;
            }
        }

        if ($bHide){
            if (isset($aFieldFilter['active']))
                $aFieldFilter['active']['view'] = 'hide';
        }

        $oForm->setFields( $aFieldFilter );
        
        // установить значения для элементов
        $oForm->setValues( $aItem );

        // добавление кнопок
        $oForm->addBtnSave();
        // редактирование пароля, если задан пользователь и он не тукущий системный
        if ( $iItemId and !Api::isCurrentSystemUser($iItemId) ) {
            $oForm->addDockedItem(array(
                'text' => \Yii::t('auth', 'pass') ,
                'iconCls' => 'icon-edit',
                'action' => 'pass'
            ));
        }
        $oForm->addBtnCancel();

        if ( $iItemId && !$bHide ) {
            $oForm->addBtnSeparator('->');
            $oForm->addBtnDelete();
        }

        // вывод данных в интерфейс
        $this->setInterface( $oForm );
    }

    /**
     * Отображение формы
     * @throws \Exception
     */
    protected function actionPass() {

        // номер записи
        $iItemId = (int)$this->getInDataVal('id');

        // id - обязательное поле
        if ( !$iItemId )
            throw new \Exception('нет id');

        // текущему системному пользователю нельзя изменять логин и пароль
        if ( Api::isCurrentSystemUser($iItemId) )
            throw new \Exception(\Yii::t('auth', 'current_user_changing_error'));

        // запись пользователя
        $aItem = \AuthUsersMapper::getUserData( $iItemId, array('id','login') );

        if ( !$aItem )
            throw new \Exception(\Yii::t('auth', 'item_not_exists'));

        // должны быть права на доступ
        Api::testAccessToUser($iItemId);

        // заголовок панели
        $this->setPanelName( \Yii::t('auth', 'password_changing') );

        // подключить автоматический генератор форм
        $oForm = new \ExtForm();

        // установить набор элементов формы

        $aFieldFilter = Mapper::getFullParamDefList(array('id','login','pass','pass2'));
        $oForm->setFields( $aFieldFilter );

        // установить значения для элементов
        $oForm->setValues( $aItem );

        // добавление кнопок
        $oForm->addBtnSave('savePass');
        $oForm->addBtnCancel('show');

        // вывод данных в интерфейс
        $this->setInterface( $oForm );
    }

    /**
     * Сохранение новости
     * @throws \Exception
     */
    protected function actionSave() {

        // номер записи
        $iItemId = (int)$this->getInDataVal('id');

        // поля для записи
        $aFields = $iItemId ? Mapper::getSaveFields() : Mapper::getAddFormFields();

        // взять данные
        $aData = $this->getInData( $aFields );

        if ( !$aData )
            throw new \Exception(\Yii::t('auth', 'no_data_for_saving'));

        // если добавление
        if ( !$iItemId ) {

            // проверить заданность login и pass
            $sLogin = $aData['login'];
            $sPass = $aData['pass'];

            if ( !\skValidator::isLogin($sLogin)) throw new \Exception(\Yii::t('auth', 'invalid_login'));

            // проверка доступности логина
            if ( !Api::loginIsFree( $sLogin ) )
                throw new \Exception(\Yii::t('auth', 'login_exists'));

            if ( !$sPass) throw new \Exception(\Yii::t('auth', 'password_expected'));

            // проверить соответствие поддтверждения пароля
            if ( $aData['pass']!==$aData['pass2'] )
                throw new \Exception(\Yii::t('auth', 'passwords_not_match'));

            // проверить задание политики доступа
            if ( !$aData['group_policy_id'] )
                throw new \Exception(\Yii::t('auth', 'group_policy_id_exptected'));

            Api::testAccessToPolicy( $aData['group_policy_id'] );

            $aData['pass'] = \Auth::buildPassword($sLogin,$sPass);

        } else {

            // обновление

            if( !isSet($aData['group_policy_id']) )
                throw new \Exception(\Yii::t('auth', 'group_policy_id_exptected'));

            if ( !$aData['group_policy_id'] ) unset($aData['group_policy_id']); // это чтобы не сохранить 0 вместо значения
            // должны быть права на доступ
            //\skewer\build\Tool\Users\Api::testAccessToPolicy( $aData['group_policy_id'] ); // не приходит значение политики, тк элемент неактивен // проверка пришедшей политики, а не сохраненной!
            Api::testAccessToUser($iItemId);

            // с себя активность снять нельзя
            if ( Api::isCurrentUser($iItemId) and !$aData['active'] ) {
                $aData['active'] = 1;
                $this->addError(\Yii::t('auth', 'current_user_change_activity'));
            }

            // с default активность нельзя снять
            $aCurUserData = Api::getUser($iItemId);
            if ( !\CurrentAdmin::isSystemMode() and $aCurUserData['login'] == 'default' and !$aData['active'] ) {
                $aData['active'] = 1;
                $this->addError(\Yii::t('auth', 'default_user_change_activity'));
            }

        }

        // есть данные - сохранить
        $bRes = Api::updUser( $aData );

        if ( $iItemId ) {
            if ( $bRes ) {
                $this->addMessage(\Yii::t('auth', 'data_saved'));
                $this->addModuleNoticeReport(\Yii::t('auth', 'user_editing'),$aData);
            } else {
                $this->addError(\Yii::t('auth', 'data_not_saved'));
            }
        }   else {
            if ( $bRes ) {
                $this->addMessage(\Yii::t('auth', 'user_added'));
                unset($aData['id']);
                unset($aData['pass']);
                unset($aData['pass2']);
                $this->addModuleNoticeReport(\Yii::t('auth', 'user_creating'),$aData);
            } else {
                $this->addError(\Yii::t('auth', 'user_not_added'));
            }
        }

        // вывод списка
        $this->actionList();

    }

    /**
     * Сохранение пароля
     * @throws \Exception
     * @throws UserException
     */
    protected function actionSavePass(){

        // запросить данные
        $aData = $this->get( 'data' );
        if ( !is_array($aData) )
            throw new UserException('wrong input data');

        // взять данные
        $iId = (int)(isset($aData['id']) ? $aData['id'] : 0);
        $sLogin = (string)(isset($aData['login']) ? $aData['login'] : '');
        $sPass1 = (string)(isset($aData['pass']) ? $aData['pass'] : '');
        $sPass2 = (string)(isset($aData['pass2']) ? $aData['pass2'] : '');

        // проверка наличия полей
        if ( !$iId ) throw new \Exception('no `id`');

        // текущему системному пользователю нельзя изменять логин и пароль
        if ( Api::isCurrentSystemUser($iId) )
            throw new \Exception(\Yii::t('auth', 'current_user_changing_error'));

        // проверка прав на доступ
        Api::testAccessToUser($iId);

        // todo проверить правомерность действий

        // логин - обязательное поле
        if ( !$sLogin ) throw new \Exception(\Yii::t('auth', 'login_expected'));

        // ошибка, если логин занят и не принадлежит изменяемому пользоватлю
        $iLoginId = Api::getIdByLogin( $sLogin );
        if ( $iLoginId and $iLoginId !== $iId )
            throw new \Exception(\Yii::t('auth', 'login_exists'));

        // пароль - обязательное поле
        if ( !$sPass1 ) throw new \Exception(\Yii::t('auth', 'password_expected'));

        // проверка правильности пароля
        if ( $sPass1!==$sPass2 )
            throw new \Exception(\Yii::t('auth', 'passwords_not_match'));

        // сохранить
        $aSaveArr = array(
            'id' => $iId,
            'login' => $sLogin,
            'pass' => \Auth::buildPassword($sLogin,$sPass1)
        );
        $bRes = Api::updUser( $aSaveArr );

        // выдать сообщение
        if ( $bRes ) {
            $this->addMessage(\Yii::t('auth', 'password_saved'));
            unset($aSaveArr['pass']);
            $this->addModuleNoticeReport(\Yii::t('auth', 'password_editing'),$aSaveArr);
        } else {
            $this->addError(\Yii::t('auth', 'password_not_changing'));
        }

        // вывод списка
        $this->actionShow();

    }

    /**
     * Удаляет запись
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        // id записи
        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        // проверка прав на доступ
        Api::testAccessToUser($iItemId);

        // запросить данные пользователя
        $aUser = \AuthUsersMapper::getUserData( $iItemId, array('id','login','name','email') );

        // удаление
        $bRes = Api::delUser( $iItemId );

        if ( $bRes ) {
            \Policy::incPolicyVersion();
            $this->addMessage(\Yii::t('auth', 'user_deleted'));
            $this->addModuleNoticeReport(\Yii::t('auth', 'user_deleting'),$aUser);
        } else {
            $this->addError(\Yii::t('auth', 'user_not_deleted'));
        }

        // вывод списка
        $this->actionList();

    }

    /**
     * Установка служебных данных
     * @param UI\State\BaseInterface $oIface
     */
    protected function setServiceData( UI\State\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData( array(
            'sectionId' => $this->iSectionId,
            'policy' => $this->mPolicyFilter,
            'active' => $this->mActiveFilter,
            'page' => $this->iPage
        ) );

    }

}
