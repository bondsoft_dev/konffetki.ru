<?php

namespace skewer\build\Tool\SiteStatus;

use skewer\build\Tool;

/**
 * Модуль для закрытия/открытия клиентской части сайта
 */
class Module extends Tool\LeftList\ModulePrototype {

    // перед выполнением
    protected function preExecute() {

    }

    /**
     * Инициализация
     */
    public function actionInit() {

        /* Строим список изображений */
        $oInterface = new \ExtUserFile( 'SiteStatus' );

        $oInterface->setModuleLangValues(array(
            'siteOpen',
            'siteClose'
        ));

        $bActive = (bool)\SysVar::get('site_open');

        $this->setData('siteStatus',$bActive);


        if ( $bActive ) {

            $oInterface->addDockedItem(array(
                'text' => \Yii::t('siteStatus', 'close'),
                'iconCls' => 'icon-stop',
                'action' => 'stop'
            ));


        } else {

            $oInterface->addDockedItem(array(
                'text' => \Yii::t('siteStatus', 'open'),
                'iconCls' => 'icon-visible',
                'action' => 'start'
            ));

        }

        $this->setInterface( $oInterface );

    }

    /**
     * Закрывает площадку
     */
    protected function actionStop() {

        \SysVar::set('site_open',0);

        \skewer\build\Tool\Redirect301\Api::makeHtaccessFile();

        $this->actionInit();

    }

    /**
     * Открывает площадку
     */
    protected function actionStart() {

        \SysVar::set('site_open',1);

        \skewer\build\Tool\Redirect301\Api::makeHtaccessFile();

        $this->actionInit();

    }

}
