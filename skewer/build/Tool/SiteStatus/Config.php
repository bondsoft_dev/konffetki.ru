<?php

use skewer\build\Tool\LeftList;

$aConfig['name']     = 'SiteStatus';
$aConfig['title']    = 'Статус сайта';
$aConfig['version']  = '1.000';
$aConfig['description']  = 'Управление статусом сайта';
$aConfig['revision'] = '0002';
$aConfig['layer']     = \Layer::TOOL;
$aConfig['group']     = LeftList\Group::ADMIN;
$aConfig['useNamespace'] = true;

return $aConfig;
