<?php

$aLanguage = array();

$aLanguage['ru']['tab_name'] = 'Статус сайта';
$aLanguage['ru']['close'] = 'Закрыть площадку';
$aLanguage['ru']['open'] = 'Открыть площадку';
$aLanguage['ru']['siteOpen'] = 'Сайт открыт.';
$aLanguage['ru']['siteClose'] = 'Сайт закрыт.';


$aLanguage['en']['tab_name'] = 'Site status';
$aLanguage['en']['close'] = 'Close site';
$aLanguage['en']['open'] = 'Open site';
$aLanguage['en']['siteOpen'] = 'Site open';
$aLanguage['en']['siteClose'] = 'Site closed';

return $aLanguage;