<?php

namespace skewer\build\Tool\SiteStatus;

use yii\web\AssetBundle;

class Asset extends AssetBundle {
    public $sourcePath = '@skewer/build/Tool/SiteStatus/web/';
}