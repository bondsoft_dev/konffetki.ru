/**
 * Created with JetBrains PhpStorm.
 * User: User
 * Date: 02.08.12
 * Time: 13:38
 * To change this template use File | Settings | File Templates.
 */
Ext.define('Ext.Tool.SiteStatus',{

    extend: 'Ext.panel.Panel',

    execute: function( data) {

        var me = this;
        var message;

        if ( data['siteStatus'] )
            message = me.lang.siteOpen;
        else
            message = me.lang.siteClose;

        // вывод текста
        this.add({
            padding: 10,
            border: 0,
            html: message
        });

    }

});
