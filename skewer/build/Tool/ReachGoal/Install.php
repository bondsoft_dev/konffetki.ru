<?php

namespace skewer\build\Tool\ReachGoal;

use skewer\build\Component\orm\Query;
use skewer\build\libs\ft;
use skewer\build\Component\ReachGoal\Target;


class Install extends \skModuleInstall {

    public function init() {
        return true;
    }// func

    public function install() {

        $oQuery = new Target();
        $oModel = $oQuery->getModel();

        $oEntity = ft\Entity::get( 'reach_goal_target' );
        $oEntity->setModel($oModel);
        $oEntity->build();

        return true;
    }// func

    public function uninstall() {
        Query::SQL('DROP TABLE `reach_goal_target`');
    }// func

}//class
