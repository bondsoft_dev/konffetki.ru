<?php

use skewer\build\Tool\LeftList;

/* main */
$aConfig['name']     = 'ReachGoal';
$aConfig['title']    = 'ReachGoal';
$aConfig['version']  = '1.000';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['useNamespace'] = true;
$aConfig['layer']     = \Layer::TOOL;
$aConfig['group']     = LeftList\Group::ADMIN;

return $aConfig;
