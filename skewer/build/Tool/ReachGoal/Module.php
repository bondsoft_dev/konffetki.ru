<?php

namespace skewer\build\Tool\ReachGoal;


use skewer\build\Component\Section\Parameters;

use skewer\build\Component\UI;
use skewer\build\Tool;
use skewer\build\Component\ReachGoal;
use skewer\build\Component\ReachGoal\Target;
use skewer\build\Component\ReachGoal\TargetRow;
use skewer\build\libs\ExtBuilder;
use yii\base\UserException;

/**
 * Модуль управления Целями (ReachGoal)
 * Class Module
 * @package skewer\build\Tool\ReachGoal
 */
class Module extends Tool\LeftList\ModulePrototype {

    /** имя параметра для счетчика яндекса */
    const yaCounter = 'yaReachGoalCounter';
    
    /** имя параметра с js кодами для счетчиков */
    const jsCounters = 'countersCode';

    protected function actionInit() {

        $oList = new \ExtList();

        $oList->setFieldsByFtModel( Target::getModel(), 'list' );

        $oList->setFieldWidth( Target::name, 200 );
        $oList->setFieldFlex( Target::title );

        $oList->addBtnAdd();

        $oList->addRowBtnUpdate();
        $oList->addRowBtnDelete();

        $oList->addBtnSeparator('->');
        $oList->addExtButton( \ExtDocked::create( \Yii::t( 'reachGoal', 'btn_settings' ) )
            ->setIconCls( \ExtDocked::iconConfiguration )
            ->setAction('settings')
        );

        $oList->setValues( Target::getAll() );

        $this->setInterface( $oList );

    }

    /**
     * Форма добавления цели
     */
    protected function actionAddForm() {
        $this->showEditForm();
    }

    /**
     * Форма добавления цели
     */
    protected function actionShow() {

        $iId = $this->getInDataValInt('id');

        $oTargetRow = Target::find($iId);
        if ( !$oTargetRow )
            throw new UserException( "not found target [$iId]" );

        $this->showEditForm( $oTargetRow );
    }

    /**
     * Выводит форму редактирования
     * @param TargetRow $oTargetRow
     * @internal param $iTargetId
     */
    private function showEditForm( TargetRow $oTargetRow = null ) {

        if ( !$oTargetRow )
            $oTargetRow = Target::getNewRow();

        $oForm = new \ExtForm();

        $oForm->setFieldsByFtModel( Target::getModel() );

        $oForm->setValues( $oTargetRow );

        $oForm->addBtnSave();
        $oForm->addBtnCancel();
        $oForm->addBtnSeparator('->');
        $oForm->addBtnDelete();

        $this->setInterface( $oForm );

    }

    /**
     * Удаление цели
     */
    protected function actionDelete() {

        $iId = $this->getInDataValInt('id');

        $oTargetRow = Target::find($iId);
        if ( !$oTargetRow )
            throw new UserException( "not found target [$iId]" );

        $oTargetRow->delete();

        $this->actionInit();

    }

    /**
     * Сохранение данных
     */
    protected function actionSave() {

        $oTargetRow = Target::getNewRow();
        $oTargetRow->setData( $this->getInData() );

        // проверка на оформат имени
        if ( !preg_match('/^[\w]+$/',$oTargetRow->name) )
            throw new UserException( \Yii::t( 'reachGoal','error_wrong_name') );

        // проверка на уникальтность
        $oRowByName = Target::getByName( $oTargetRow->name );
        if ( $oRowByName ) {
            if ( (int)$oRowByName->id != (int)$oTargetRow->id )
                throw new UserException( \Yii::t( 'reachGoal','error_name_exists') );
        }

        if ( $oTargetRow->save() )
            $this->actionInit();

    }


    /**
     * Отображение интерфейса настроек
     */
    protected function actionSettings() {

        /**
         * @todo собрать через построитель
         */

        $oForm = new \ExtForm();

        /**
         * Номер яндекс счетчика
         */
        $oField = new ExtBuilder\Field\String();
        $oField->setName( ReachGoal\Yandex::contName );
        $oField->setTitle( \Yii::t( 'reachGoal', 'yaCounter' ) );
        $oField->setValue( (string)ReachGoal\Yandex::getCounter() );

        $oForm->addField( $oField );

        /**
         * Код js счетчиков
         */
        $aCodesParam = Parameters::getByName(
            \Yii::$app->sections->root(),
            Parameters::settings,
            self::jsCounters,
            false
        );

        $oFieldCodes = new ExtBuilder\Field\Text();
        $oFieldCodes->setHeight(400);
        $oFieldCodes->setName( self::jsCounters );

        $oFieldCodes->setTitle(\Yii::t('editor', 'countersCode'));
        $oFieldCodes->setValue( $aCodesParam ? $aCodesParam['show_val'] : '' );

        $oForm->addField( $oFieldCodes );

        /*
         * ----
         */

        $oForm->addBtnSave( 'saveSettings' );
        $oForm->addBtnCancel();

        $this->setInterface( $oForm );

    }

    /**
     * Сохранение настроек
     */
    protected function actionSaveSettings() {

        $sYaCounter = $this->getInDataValInt( ReachGoal\Yandex::contName );
        $sCounterCode = $this->getInDataVal( self::jsCounters );

        ReachGoal\Yandex::setCounter( $sYaCounter );

        Parameters::setParams(\Yii::$app->sections->root(), Parameters::settings,
            self::jsCounters, '200', $sCounterCode);

        $this->addMessage( \Yii::t( 'reachGoal', 'ys_counter_saved', $sYaCounter ) );

        $this->actionInit();

    }

}