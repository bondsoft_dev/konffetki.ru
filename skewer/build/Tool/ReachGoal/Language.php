<?php

$aLanguage = array();

/* russian */
$aLanguage['ru']['tab_name'] = 'Цели';

$aLanguage['ru']['field_name'] = 'Идентификатор цели';
$aLanguage['ru']['field_title'] = 'Название';

$aLanguage['ru']['btn_settings'] = 'Настройки';

$aLanguage['ru']['yaCounter'] = 'Номер счетчика Яндекс';

$aLanguage['ru']['ys_counter_saved'] = 'Счетчик для Яндекса сохранен [{0}]';

$aLanguage['ru']['error_wrong_name'] = 'Имя цели должно состоять из только из латинских букв, цифр и подчеркиваний';
$aLanguage['ru']['error_name_exists'] = 'Такое имя события уже существует';

/* english */
$aLanguage['en']['tab_name'] = 'ReachGoal';

$aLanguage['en']['field_name'] = 'Target identiifier';
$aLanguage['en']['field_title'] = 'Title';

$aLanguage['en']['btn_settings'] = 'Settings';

$aLanguage['en']['yaCounter'] = 'Yandex counter number';

$aLanguage['en']['ys_counter_saved'] = 'Yandex counter saved [{0}]';

$aLanguage['en']['error_wrong_name'] = 'Name must contain only letters, digits and underscore sign';
$aLanguage['en']['error_name_exists'] = 'Name already exists';

return $aLanguage;
