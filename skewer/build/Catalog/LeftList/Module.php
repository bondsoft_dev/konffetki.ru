<?php

namespace skewer\build\Catalog\LeftList;


use skewer\build\Component\Catalog;
use skewer\build\Component\orm\Query;
use skewer\build\Cms;
use yii\base\UserException;

/**
 * @todo постирать закомментированный код
 * Модуль для вывода списка категорий
 * Class Module
 * @package skewer\build\Catalog\LeftList
 */
class Module extends Cms\LeftPanel\ModulePrototype {

    /**
     * Отдает название модуля
     */
    private function getModuleTitle() {
        return $this->title;
    }

    /**
     * Отдает класс-родитель, наследники которого могут быть добавлены в дерево процессов
     * в качестве вкладок
     * @return string
     */
    public function getAllowedChildClassForTab() {
        return 'skewer\build\Catalog\LeftList\ModuleInterface';
    }

    /**
     * Задает список модулей
     * @return int
     */
    public function actionInit(){

        // команда на инициализацию
        $this->setCmd( 'init' );

        $this->addInitParam('title', $this->getModuleTitle());

        // добавить библиотеку отображения
        $this->addLibClass('LeftListGrid');

//        $this->setModuleLangValues(array(
//            'btn_add',
//            'root_categ_title' => 'LeftList.root_categ_title',
//            'treeFormTitleTitle',
//            'paramFormClose',
//            'paramFormSaveUpd',
//            'treeDelRowHeader',
//            'treeDelRow',
//            'treeDelMsg',
//            'treeErrorOnDelete',
//            'treeErrorNoParent'
//        ));


        // запрос списка площадок
        $this->setData('items', $this->getModuleList());

    }

    /**
     * Отдает инициализационный массив для набора вкладок
     * @param int|string $mRowId идентификатор записи
     * @return string[]
     */
    public function getTabsInitList( $mRowId ) {

        foreach ( $this->getModuleList() as $aItem ) {
            if ( $aItem['id'] === $mRowId )
                return array( $aItem['id'] => $aItem['name'] );
        }
        return array();

    }


    /**
     * Отдает инициализационный массив для набора вкладок
     * @return string[]
     */
    private function getAvaliableModules() {

        $aList = ['Goods','CardEditor','Dictionary'];

        if ( \Yii::$app->register->moduleExists('Collections', \Layer::CATALOG) )
            $aList[] = 'Collections';

        $aList[] = 'ViewSettings';

        if ( \Yii::$app->register->moduleExists('Settings', \Layer::CATALOG) ){
            if (\CurrentAdmin::isSystemMode())
                $aList[] = 'Settings';
        }

        return $aList;

    }

    private function getModuleList() {

        $aOut = array();

        foreach( $this->getAvaliableModules() as $sModuleName ) {

            $oModuleConfig = \Yii::$app->register->getModuleConfig( $sModuleName, \Layer::CATALOG );

            // не выводить себя
            if ( get_class($this) === $oModuleConfig->getNameWithNamespace() )
                continue;

            $sTitle = $oModuleConfig->getTitle();

            $aOut[] = array(
                'id' => $oModuleConfig->getName(),
                'name' => $oModuleConfig->getNameFull(),
                'title' => $sTitle,
                );

        }

        return array_values($aOut);
    }


    /**
     * Отдает список категорий
     * @param int $iParentId
     * @return array
     */
    private function getCategoryList( $iParentId = 0 ) {

        $aSectionList = Catalog\Section::getList();

        $aOut = array();
        foreach ( $aSectionList as $iSection => $sTitle )
            $aOut[ $iSection ] = array(
                'id' => $iSection,
                'title' => $sTitle,
                'parent' => 0,
            );

        $aOut = $this->markLeafs( $aOut );

        return array_values($aOut);
    }

    /**
     * Состояние. Выбор подразделов заданного раздела
     * @return bool
     */
    protected function actionGetSubItems() {

        // заданный раздел
        $iId = $this->getInt('node');

        $this->setCmd( 'loadItems' );

        $this->setData('items', $this->getCategoryList( $iId ));

        return true;

    }


    /**
     * Задает дополнительные параметры для вкладок
     * @static
     * @param $mRowId
     * @return array
     */
    public function getTabsAddParams( $mRowId ) {

        $aOut = array();

        foreach ( $this->getTabsInitList($mRowId) as $sKey => $sModule ) {
            $aOut[$sKey]['pageId'] = $mRowId;
        }

        return $aOut;

    }

    /**
     * Просто возвращает данные для выбора раздела
     */
    protected function actionSelectNode(){

        // целевой раздел
        $iToSection = $this->getInt('sectionId');

        // отдать в вывод, если найдено
        $this->setCmd('selectNode');
        $this->setData('sectionId',$iToSection);

    }

    /**
     * Поветочно открывает дерево до нужного раздлела
     * @param int $iFromSection - корневая вершина
     * @param int $iToSection - целевая вершина
     * @param array &$aParents - набор радителей
     * @param array $aTail - набор подчиненных разделов
     * @return array
     */
    protected function getSectionsTree( $iFromSection=0, $iToSection = 0, &$aParents, $aTail = array() ) {

//        if ( !$iToSection ) $iToSection = $this->getInt('sectionId');
//
//        $oCategory = Catalog\Category::getById( $iToSection );
//
//        // родительский раздел
//        $iParentId = $oCategory->parent;
//
//        // если есть родитель и до конца не добрались
//        if( $iParentId and $iFromSection != $iToSection ){
//
//            // составление набора родительских элементов
//            $aParents[] = $iParentId;
//
//            // набор подчиненных записей
//            $aItems = Catalog\Category::getListByParent( $iParentId );
//
//            // перевод в массив массивов
//            $aArrItems = array();
//            foreach ( $aItems as $oItem )
//                $aArrItems[$oItem->id] = $oItem->getData();
//
//            // обобначить разделы без наследников
//            $aArrItems = $this->markLeafs( $aArrItems );
//
//            // рекурсивно спуститься ниже
//            $aArrItems = array_merge($this->getSectionsTree($iFromSection, $iParentId, $aParents, $aArrItems),$aTail);
//
//            return $aArrItems;
//        } // if parent
//        else {
//            return $aTail;
//        } // else

    } // func

    /**
     * Возвращает в поток дерево разделов, открытое до определенной вершины
     */
    protected function actionGetTree() {

        // целевой раздел
        $iToSection = $this->getInt('sectionId');

        // запросить дерево
        $aParents = array();
        $aItems = $this->getSectionsTree( 0, $iToSection, $aParents );

        // отдать в вывод, если найдено
        if ( $aItems ) {
            $this->setCmd('loadTree');
            $this->setData('sectionId',$iToSection);
            $this->setData( 'items', $aItems );
            $this->setData( 'parents', array_reverse($aParents) );
        }

    }

    /**
     * Отозначить разделы без наследников
     * @param $aItems
     * @return array
     */
    private function markLeafs( $aItems ) {

        if(!count($aItems)) return array();

        // набор id разделов
        $aIdList = array_keys($aItems);
        $aIdList = array_map( function($i){return (int)$i;}, $aIdList );


        // todo эта функция не используется?
        // сборка запроса
        $sQuery = sprintf(
            "SELECT `parent` FROM `%s` WHERE `parent` IN (%s) GROUP BY `parent`;",
            Catalog\model\CategoryTable::getTableName(),
            implode(',',$aIdList)
        );

        // выполенение запроса
        $oResult = Query::SQL( $sQuery );

        // сборка выходного массива
        $aHasChild = array();
        while ( $aRow = $oResult->fetchArray() )
            $aHasChild[] = $aRow['parent'];

        // добавить пустой контейнер тем, у кого наследников нет
        foreach ( $aIdList as $iId ) {
            if ( !in_array($iId,$aHasChild) )
                $aItems[$iId]['children'] = array();
        }

        return $aItems;

    }

    /**
     * Запрос параметров для построения формы добавления / редактирования
     * @throws UserException
     */
    protected function actionGetForm() {

        // добавить библиотеку отображения
        $this->addLibClass('CategoryForm');

        // запрос пришедших переменных
        $iSectionId = $this->getInt('selectedId');

        if ( $iSectionId ) {

            $this->setData( 'title', \Yii::t('catalog',  'form_name_edit' ) );
            $oItem = Catalog\model\CategoryTable::find( $iSectionId );
            if ( empty($oItem) )
                throw new UserException( \Yii::t('catalog',  'error_categ_not_found', $iSectionId ) );

        } else {

            $this->setData( 'title', \Yii::t('catalog',  'form_name_new' ) );
            $oItem = Catalog\model\CategoryTable::getNewRow();
            $oItem->title = \Yii::t('catalog',  'val_new_categ' );

        }

        // родительский раздел
        $aRow = $this->get('item');
        if ( is_array($aRow) and isset($aRow['parent'])  )
            $iParent = $aRow['parent'];
        else
            $iParent = 0;
        $oItem->parent = $iParent;

        // отдать параметры
        $this->setCmd( 'createForm' );
        $this->setData( 'form', $oItem->getData() );

    }

    /**
     * Сохранение/добавление раздела
     * @throws UserException
     * @return bool
     */
    protected function actionSaveCategory() {

//        // массив на сохранение
//        $aData = $this->get('item');
//
//        // id раздела
//        $iCategoryId = $this->getInt('sectionId');
//
//        /** @var bool $iIsNew флаг "Новая запись" */
//        $iIsNew = !$iCategoryId;
//
//        // состояние системы
//        $this->setCmd('saveItem');
//
//        // id родительского раздела
//        $iParentId = isset($aData['parent']) ? (int)$aData['parent'] : 0;
//
//        if ( $aData ) {
//
//            /** @var Catalog\model\CategoryRow $oCategory */
//            $oCategory = Catalog\model\CategoryTable::getNewRow();
//
//            if ( $iCategoryId )
//                $oCategory->id = $iCategoryId;
//
//            if ( isset($aData['parent']) and $aData['parent'] )
//                $oCategory->parent = (int)$aData['parent'];
//
//            if ( isset($aData['title']) and $aData['title'] )
//                $oCategory->title = $aData['title'];
//
//            // если новая запись - добавить параметр сортировки
//            if ( $iIsNew )
//                $oCategory->position = Catalog\Category::getNextPosByParentId( $iParentId );
//
//            // сохранить раздел
//            $oCategory->save();
//
//            if ( $oCategory->hasError() )
//                throw new \ModuleAdminErrorException( $oCategory->getError() );
//
////            if ( isset($aData[Tree::ID]) && $aData[Tree::ID] )
////                skLogger::addNoticeReport("Редактирование раздела",skLogger::buildDescription($aData),skLogger::logUsers,$this->getModuleName());
////            else {
////                unset($aData[Tree::ID]);
////                skLogger::addNoticeReport("Создание раздела",skLogger::buildDescription($aData),skLogger::logUsers,$this->getModuleName());
////            }
//
//            // статус сохранения
//            $this->setData('saveResult',true);
//
//            // выдать в ответ текущие данные в базе
//            $aNowItem = Catalog\model\CategoryTable::find( $oCategory->id );
//            $this->setData('item',$aNowItem);
//
//            $this->fireEvent('Goods:save');
//
//        } else {
//
//            // при ошибке сохранения
//            $this->setData('saveResult',false);
//
//            return false;
//
//        }
//
//        return true;

    }

    /**
     * Событие изменения положения раздела
     * @throws UserException
     */
    protected function actionChangePosition() {

//        // направление
//        $sDirection = $this->getStr('direction');
//        // id переносимого элемента
//        $iItemId = $this->getInt('itemId');
//        // id элемента относительного которого идет перемещение
//        $iOverId = $this->getInt('overId');
//
//        // проверка наличия параменных
//        if ( !$iItemId or !$sDirection )
//            throw new \ModuleAdminErrorException('badData');
//
//        // запросить записи элементов
//        $oRow = Catalog\Category::getByIdOrExcept($iItemId);
//
//        // направление переноса
//        switch ( $sDirection ) {
//
//            // добавить как подчиненный
//            case 'append':
//
//                $oOverRow = Catalog\Category::getById($iOverId);
//                if ( $iOverId and !$oOverRow )
//                    $this->riseError( \Yii::t('catalog', 'error_dest_not_found') );
//
//                // запросить максимальную позицию наследников раздела назначения
//                $iPosition = Catalog\Category::getNextPosByParentId( $iOverId );
//
//                // изменить положение и родителя раздела
//                $oRow->position = $iPosition;
//                $oRow->parent = $iOverId;
//                $oRow->save();
//
//                break;
//
//            // вставить до/после элемента
//            case 'before':
//            case 'after':
//
//                $oOverRow = Catalog\Category::getByIdOrExcept($iOverId);
//
//                // флаг "положить до"
//                $bBefore = (bool)($sDirection == 'before');
//
//                // взять позицию элемента привязки
//                $iOverPositionId = $oOverRow->position;
//
//                // новое положение
//                $iPosition = $bBefore ? $iOverPositionId : $iOverPositionId+1;
//
//                // выполнить сдвиг позиции
//                Catalog\Category::shiftPositionByParent( $oOverRow->parent, $iPosition );
//
//                // изменить положение и родителя раздела
//                $oRow->parent = $oOverRow->parent;
//                $oRow->position = $iPosition;
//                $oRow->save();
//
//                break;
//
//            // неподдерживаемый вариант
//            default:
//                throw new \ModuleAdminErrorException('badData');
//
//        }

        $this->setCmd('none');

    }

    /**
     * Удаление раздела
     */
    protected function actionDeleteCategory() {

//        // id раздела
//        $iCategoryId = $this->getInt('sectionId');
//
//        $oCategory = Catalog\Category::getByIdOrExcept( $iCategoryId );
//
//        // todo категорий больше нет и эта функция походу не работает
////        $oSearch = new \skewer\build\Catalog\Goods\Search();
////        $oSearch->hideSearchByCategory($iCategoryId);
//
//        $bRes = (bool)$oCategory->delete();
//
//        // возврат результата
//        $this->setCmd('deleteSection');
//        $this->setData( 'deletedId', $bRes ? $iCategoryId : 0 );
//
//        $this->fireEvent('Goods:delete');

    }

}
