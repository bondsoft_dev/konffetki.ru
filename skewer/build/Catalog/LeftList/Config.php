<?php
/**
 * User: kolesnikiv
 * Date: 31.07.13
 */
$aConfig['name']     = 'LeftList';
$aConfig['title']    = 'Список';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Список';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::CATALOG;
$aConfig['useNamespace'] = true;
$aConfig['languageCategory'] = 'catalog';

$aConfig['policy'] = array( array(
    'name'    => 'useCatalog',
    'title'   => 'Доступ к панели каталога',
    'default' => 1
));

return $aConfig;
