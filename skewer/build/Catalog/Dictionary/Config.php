<?php
/**
 * User: kolesnikiv
 * Date: 31.07.13
 */
$aConfig['name']     = 'Dictionary';
$aConfig['title']    = 'Словари';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Словари';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::CATALOG;
$aConfig['useNamespace'] = true;
$aConfig['languageCategory'] = 'dict';

return $aConfig;
