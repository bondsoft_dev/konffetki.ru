<?php

$aLanguage = array();

$aLanguage['ru']['Dictionary.Catalog.tab_name'] = 'Справочники';
$aLanguage['ru']['tab_name'] = 'Справочники';
$aLanguage['ru']['dict_name'] = 'Имя справочника';
$aLanguage['ru']['system_name'] = 'Техническое имя';
$aLanguage['ru']['create_dict'] = 'Создать справочник';
$aLanguage['ru']['dict_list_for_cat'] = 'Список справочников для каталога';
$aLanguage['ru']['dict_panel_name'] = 'Справочник: {0}';
$aLanguage['ru']['new_dict'] = 'Создание нового справочника';
$aLanguage['ru']['struct'] = 'Структура';

$aLanguage['ru']['title'] = 'Название';
$aLanguage['ru']['add'] = 'Добавить';
$aLanguage['ru']['back'] = 'Назад';
$aLanguage['ru']['del'] = 'Удалить';

$aLanguage['ru']['error_dict_not_found'] = 'Не найден справочник';
$aLanguage['ru']['error_row_not_found'] = 'Не найдена запись';
$aLanguage['ru']['error_noname_seted'] = 'Не задано имя справочника';
$aLanguage['ru']['error_not_selected'] = 'Не выбран справочник для удаления';
$aLanguage['ru']['error_field_cant_removed'] = 'Это поле не может быть удалено';

$aLanguage['ru']['title_edit_field'] = 'Редактирование поля справочника';
$aLanguage['ru']['title_new_field'] = 'Добавление нового поля';

/*****************/

$aLanguage['en']['Dictionary.Catalog.tab_name'] = 'Dictionary';
$aLanguage['en']['tab_name'] = 'Dictionary';
$aLanguage['en']['dict_name'] = 'Dictionary name';
$aLanguage['en']['system_name'] = 'System name';
$aLanguage['en']['create_dict'] = 'Create a new dictionary';
$aLanguage['en']['dict_list_for_cat'] = 'Catalog dictionary list';
$aLanguage['en']['dict_panel_name'] = 'Dictionary: {0}';
$aLanguage['en']['new_dict'] = 'New dictionary';
$aLanguage['en']['struct'] = 'Structure';

$aLanguage['en']['title'] = 'Name';
$aLanguage['en']['add'] = 'Add';
$aLanguage['en']['back'] = 'Back';
$aLanguage['en']['del'] = 'Delete';

$aLanguage['en']['error_dict_not_found'] = 'Dictionary not found';
$aLanguage['en']['error_row_not_found'] = 'Row not found';
$aLanguage['en']['error_noname_seted'] = 'Name not provided';
$aLanguage['en']['error_not_selected'] = 'Dictionary not selected';
$aLanguage['en']['error_field_cant_removed'] = 'This field can not be removed';

$aLanguage['en']['title_edit_field'] = 'Editing a field dictionary';
$aLanguage['en']['title_new_field'] = 'New field';

return $aLanguage;