<?php

namespace skewer\build\Catalog\Dictionary;

use skewer\build\Catalog\CardEditor\Api;
use skewer\build\Catalog\LeftList\ModulePrototype;
use skewer\build\Component\Catalog;
use skewer\build\Component\UI;
use skewer\build\libs\ft;
use yii\base\UserException;


/**
 * Модуль для редактирования записей в каталоге
 * Class Module
 * @package skewer\build\Catalog\Dictionary
 */
class Module extends ModulePrototype {


    // число элементов на страницу
    public $iOnPage = 20;

    // текущий номер страницы ( с 0, а приходит с 1 )
    public $iPage = 0;

    protected function getCard() {

        $card = $this->getInnerData( 'card', 0 );

        if ( !$card )
            $card = $this->getInDataVal( 'id' );


        $this->setInnerData( 'card', $card );

        return $card;
    }

//    protected function preExecute() {
//        $this->iPage = $this->getInt( 'page', $this->getInnerData( 'page', 0 ) );
//        $this->setInnerData( 'page', $this->iPage );
//    }


    /**
     * Иницализация
     */
    protected function actionInit() {
        $this->actionList();
    }


    /**
     * Список справочников
     */
    protected function actionList() {

        $this->setInnerData( 'card', 0 );

        $oFormBuilder = UI\StateBuilder::newList();

        // установка заголовка
        $this->setPanelName( \Yii::t('dict', 'dict_list_for_cat') );

        $oFormBuilder
            ->fieldHide( 'id', 'id', 'i', ['listColumns.width' => 40] )
            ->fieldString( 'title', \Yii::t('dict', 'dict_name'), ['listColumns.flex' => 1] )
            ->setValue( Catalog\Card::getDictionaries() )
            ->button( \Yii::t('dict', 'create_dict'), 'New', 'icon-add' )
            ->buttonRowUpdate( 'View' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Набор значений справочника
     * todo сделать постраничный вывод
     * todo сделать вывод дополнительных полей в списке
     */
    protected function actionView() {

        // обработка входных данных
        $card = $this->getCard();

        if ( !$card )
            throw new UserException( "Card not found!" );

        // получение контентных данных
        $oTable = ft\Cache::getMagicTable( $card );
        if ( !$oTable )
            throw new UserException( \Yii::t('dict', 'error_dict_not_found') );

        $aItems = $oTable->find()->asArray()->getAll();


        // установка заголовка
        $this->setPanelName( \Yii::t('dict', 'dict_panel_name', Catalog\Card::getTitle( $card )) );

        // контентная форма
        $oFormBuilder = UI\StateBuilder::newList();

        $oFormBuilder
            ->fieldHide( 'id', 'id', 'i', ['listColumns.width' => 40] )
            ->fieldString( 'title', \Yii::t('dict', 'title'), ['listColumns.flex' => 1] )
            ->setValue( $aItems )
            ->button( \Yii::t('dict', 'add'), 'ItemEdit', 'icon-add' )
            ->button( \Yii::t('dict', 'struct'), 'FieldList', 'icon-edit' )
            ->button( \Yii::t('dict', 'back'), 'List', 'icon-cancel' )
            ->buttonSeparator( '->' )
            ->button( \Yii::t('dict', 'del'), 'Remove', 'icon-delete' )
            ->buttonRowUpdate( 'ItemEdit' )
            ->buttonRowDelete( 'ItemRemove' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Список полей справочника
     * @throws UserException
     */
    protected function actionFieldList() {

        $card = $this->getInnerData( 'card' );

        if ( !$card )
            throw new UserException( "Card not found!" );

        // генерация объектов для работы
        $oCard = Catalog\Card::get( $card );


        // создаем интерфейс
        $oFormBuilder = UI\StateBuilder::newList();

        $sHeadText = \Yii::t( 'card', 'head_card_name', $oCard->title );
        $this->setPanelName( \Yii::t( 'card', 'title_field_list',$oCard->title) );

        $oFormBuilder->addHeadText( sprintf('<h1>%s</h1>', $sHeadText) );

        // добавляем поля
        $oFormBuilder
            ->fieldHide( 'id', 'id' )
            ->fieldString( 'name', \Yii::t( 'card', 'field_f_name') )
            ->fieldString( 'title', \Yii::t( 'card', 'field_f_title'), ['listColumns.flex' => 1] )
            ->fieldString( 'editor', \Yii::t( 'card', 'field_f_editor') )
            ->addWidget( 'editor', 'skewer\\build\\Catalog\\CardEditor\\Api', 'applyEditorWidget' );

        // устанавливаем значения
        $oFormBuilder
            ->setValue( $oCard->getFields() );

        // элементы управления
        $oFormBuilder
            ->button( \Yii::t( 'card', 'btn_add_field'), 'FieldEdit', 'icon-add' )
            ->button( \Yii::t( 'card', 'btn_back'), 'View', 'icon-cancel' );
        ;

        // обработчики на список
        $oFormBuilder
            ->buttonRowUpdate( 'FieldEdit' )
            ->buttonRowDelete( 'FieldRemove' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Интерфейс создания/редактирования поля справочника
     */
    protected function actionFieldEdit() {

        // входные параметры
        $card = $this->getInnerData( 'card' );
        $iFieldId = $this->getInDataVal( 'id', false );

        if ( !$card )
            throw new UserException( "Card not found!" );

        /** @var Catalog\model\FieldRow $oItem */
        $oCard = Catalog\Card::get( $card );

        if ( $iFieldId ) {
            $oItem = Catalog\Card::getField( $iFieldId );
            $this->setPanelName( \Yii::t('dict', 'title_edit_field') );
        } else {
            $oItem = Catalog\Card::getField();
            $this->setPanelName( \Yii::t('dict', 'title_new_field') );
        }


        // создание формы
        $oFormBuilder = UI\StateBuilder::newEdit();

        // добавление полей
        $oFormBuilder
            ->addHeadText( '<h1>' . \Yii::t( 'card', 'head_card_name', $oCard->title ) . '</h1>' )
            ->fieldHide( 'id', 'id' )
            ->fieldString( 'title', \Yii::t( 'card', 'field_f_title'), ['listColumns.flex' => 1] )
            ->fieldString( 'name', \Yii::t( 'card', 'field_f_name'), ['disabled' => (bool)$iFieldId] )
            ->fieldSelect( 'editor', \Yii::t( 'card', 'field_f_editor'), Api::getSimpleTypeList( false ) )
            ->fieldString( 'def_value', \Yii::t( 'card', 'field_f_def_value') )
        ;

        // добавление значений
        $oFormBuilder
            ->setValue( $oItem );

        // элементы управления
        $oFormBuilder
            ->button( \Yii::t('adm','save'), 'FieldSave', 'icon-save' )
            ->button( \Yii::t('adm','cancel'), 'FieldList', 'icon-cancel' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    protected function actionFieldSave() {

        $card = $this->getInnerData( 'card' );

        if ( !$card )
            throw new UserException( \Yii::t( 'card', 'error_card_not_found') );

        $data = $this->getInData();
        $id = $this->getInDataVal( 'id', null );

        if ( !$this->getInDataVal( 'title' ) )
            throw new UserException( \Yii::t( 'card', 'error_no_field_name') );

        if ( !$this->getInDataVal( 'editor' ) )
            throw new UserException( \Yii::t( 'card', 'error_no_editor_for_field') );

        $oField = Catalog\Card::getField( $id );
        $oField->setData( $data );
        $oField->entity = $card;
        $oField->save();

        $oCard = Catalog\Card::get( $card );
        $oCard->updCache();

        $this->actionFieldList();
    }


    protected function actionFieldRemove() {

        $id = $this->getInDataVal( 'id', null );
        $card = $this->getInnerData( 'card' );

        if ( !$id )
            throw new UserException( \Yii::t('dict', 'error_field_not_found') );

        $oField = Catalog\Card::getField( $id );

        if ( in_array( $oField->name, ['id', 'title'] ) )
            throw new UserException( \Yii::t('dict', 'error_field_cant_removed') );

        $oField->delete();

        // todo где перестроение сущность?

        $this->actionFieldList();
    }


    /**
     * Форма редактирования значения для справочника
     * @return int
     * @throws UserException
     */
    protected function actionItemEdit() {

        $id = $this->getInDataVal( 'id', 0 );
        $card = $this->getInnerData( 'card' );

        if ( !$card )
            throw new UserException( "Card not found!" );

        $oTable = ft\Cache::getMagicTable( $card );
        if ( !$oTable )
            throw new UserException( \Yii::t('dict', 'error_dict_not_found') );

        $oItem = $id ? $oTable->find( $id ) : $oTable->getNewRow();

        // установка заголовка
        $oDict = Catalog\Card::get( $card );
        $this->setPanelName( \Yii::t('dict', 'dict_panel_name', Catalog\Card::getTitle( $card )) );

        $oFormBuilder = UI\StateBuilder::newEdit();

        $oFormBuilder
            ->fieldHide( 'id', 'id' )
        ;

        $aFields = $oDict->getFields();
        foreach ( $aFields as $oField ) {
            $oFormBuilder->fieldByEntity( $oField );
        }

        $oFormBuilder
            ->setValue( $oItem )
            ->button( \Yii::t('adm','save'), 'ItemSave', 'icon-save' )
            ->button( \Yii::t('adm','cancel'), 'View', 'icon-cancel' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Сохранение значения для справочника
     */
    protected function actionItemSave() {

        $aData = $this->getInData();
        $id = $this->getInDataVal( 'id', 0 );
        $card = $this->getInnerData( 'card' );

        if ( !$card )
            throw new UserException( "Card not found!" );

        $oTable = ft\Cache::getMagicTable( $card );
        if ( !$oTable )
            throw new UserException( \Yii::t('dict', 'error_dict_not_found') );

        $oItem = $id ? $oTable->find( $id ) : $oTable->getNewRow();

        $oItem->setData( $aData );

        if ( $oItem->save() ) {

            $this->actionView();

        } else {
            $this->addError( $oItem->getError() );
        }

    }


    /**
     * Удаление записи из справочника
     */
    protected function actionItemRemove() {

        $id = $this->getInDataVal( 'id', 0 );
        $card = $this->getInnerData( 'card' );

        if ( !$id )
            throw new UserException( \Yii::t('dict', 'error_row_not_found') );

        $oCurTable = ft\Cache::getMagicTable( $card );
        if ( !$oCurTable )
            throw new UserException( \Yii::t('dict', 'error_dict_not_found') );

        // поиск полей связанных со справочником
        $oEntity = Catalog\Card::get( $card );

        // для связи -<
        $aFields = Catalog\model\FieldTable::find()
            ->where( 'link_type', ft\Relation::ONE_TO_MANY )
            ->where( 'link_id', $oEntity->id )
            ->getAll();

        foreach ( $aFields as $oField ) {

            $oTable = ft\Cache::getMagicTable( $oField->entity );

            if ( !$oTable )
                continue;

            // очищаем значения полей
            $oTable->update( [$oField->name => ''], [$oField->name => $id] );
        }

        // для связи ><
        $aFields = Catalog\model\FieldTable::find()
            ->where( 'link_type', ft\Relation::MANY_TO_MANY )
            ->where( 'link_id', $oEntity->id )
            ->getAll();

        foreach ( $aFields as $oField ) {

            $model = ft\Cache::softGet( $oField->entity );

            if ( !$model ) continue;

            $field = $model->getFiled( $oField->name );

            $field->unLinkAllRow( $id );
        }

        // удаление значения из справочника
        $oItem = $oCurTable->find( $id );
        $oItem->delete();

        $this->actionView();
    }


    /**
     * Добавление нового справочника
     * @return int
     */
    protected function actionNew() {

        $oCard = Catalog\Card::get();
        $this->setPanelName( \Yii::t('dict', 'new_dict') );

        $oFormBuilder = UI\StateBuilder::newEdit();
        $oFormBuilder
            ->addHeadText( '<h1>' . \Yii::t('dict', 'new_dict') . '</h1>' )
            ->fieldHide( 'id', 'id' )
            ->fieldString( 'title', \Yii::t('dict', 'dict_name'), ['listColumns.flex' => 1] )
            ->fieldString( 'name', \Yii::t('dict', 'system_name') )
            ->setValue( $oCard )
            ->button( \Yii::t('adm','save'), 'Add', 'icon-save' )
            ->button( \Yii::t('adm','cancel'), 'List', 'icon-cancel' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Состояние сохранения нового справочника
     */
    protected function actionAdd() {

        $aData = $this->getInData();

        $sDictTitle = $this->getInDataVal( 'title' );

        if ( !$sDictTitle )
            throw new UserException( \Yii::t('dict', 'error_noname_seted') );

        $oCard = Catalog\Card::addDictionary( $aData );

        $this->setInnerData( 'card', $oCard->id );

        $this->actionView();

    }


    /**
     * Состояние удаления словаря
     */
    protected function actionRemove() {

        $card = $this->getInnerData( 'card' );

        if ( !$card )
            throw new UserException( \Yii::t('dict', 'error_not_selected') );

        $oCard = Catalog\Card::get( $card );

        if ( !$oCard )
            throw new UserException( \Yii::t('dict', 'error_dict_not_found') );

        // поиск полей связанных со справочником
        $oEntity = Catalog\Card::get( $card );
        $aFields = Catalog\model\FieldTable::find()
            ->where( 'link_type', ft\Relation::ONE_TO_MANY )
            ->where( 'link_id', $oEntity->id )
            ->getAll();

        foreach ( $aFields as $oField ) {

            $oTable = ft\Cache::getMagicTable( $oField->entity );

            if ( !$oTable )
                continue;

            // очищаем значения полей
            $oTable->update( [$oField->name => ''] );
        }

        $oCard->delete();

        $this->setInnerData( 'card', 0 );

        $this->actionList();

    }

} 