<?php

$aConfig['name']     = 'ViewSettings';
$aConfig['title']    = 'Каталог. Настройка вывода';
$aConfig['version']  = '1.000';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::CATALOG;
$aConfig['useNamespace'] = true;
$aConfig['languageCategory'] = 'catalog';

return $aConfig;
