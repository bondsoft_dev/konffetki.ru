<?php

$aConfig['name']     = 'Filters';
$aConfig['title']    = 'Каталог. Редактор фильтров.';
$aConfig['version']  = '1.000a';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::CATALOG;
$aConfig['useNamespace'] = true;
$aConfig['languageCategory'] = 'catalogFilter';

return $aConfig;
