<?php

$aLanguage = array();

$aLanguage['ru']['Filters.Catalog.tab_name'] = 'Фильтры';
$aLanguage['ru']['title_filter_list'] = 'Список фильтров для каталога';
$aLanguage['ru']['filter_title'] = 'Название фильтра';
$aLanguage['ru']['btn_add_filter'] = 'Добавить фильтр';
$aLanguage['ru']['btn_edit_filter'] = 'Настройка фильтра';
$aLanguage['ru']['btn_add_field'] = 'Добавить поле';

$aLanguage['ru']['fields'] = 'Поля фильтра';
$aLanguage['ru']['field_title'] = 'Имя поля';
$aLanguage['ru']['filter_active'] = 'Активность';
$aLanguage['ru']['field_goods_section'] = 'Категория товаров';
$aLanguage['ru']['field_view_section'] = 'Раздел вывода';
$aLanguage['ru']['field_sub'] = 'Выводить в подразделах';
$aLanguage['ru']['field_all'] = 'Выводить на всех страницах';
$aLanguage['ru']['field_main'] = 'Выводить на главной';
$aLanguage['ru']['field_active'] = 'Активность';
$aLanguage['ru']['field_widget'] = 'Тип виджета';

$aLanguage['ru']['wg_input'] = 'Строка ввода';
$aLanguage['ru']['wg_check'] = 'Галочка';
$aLanguage['ru']['wg_num_slider'] = 'Диапазон значений';
$aLanguage['ru']['wg_check_group'] = 'Группа галочек';
$aLanguage['ru']['wg_select'] = 'Выпадающий список';

$aLanguage['ru']['empty_widget_error'] = 'Не задан тип виджета';

$aLanguage['en']['Filters.Catalog.tab_name'] = 'Filters';
$aLanguage['en']['title_filter_list'] = 'Catalog filter list';
$aLanguage['en']['filter_title'] = 'Filter title';
$aLanguage['en']['btn_add_filter'] = 'Add a new filter';
$aLanguage['en']['btn_edit_filter'] = 'Edit filter';
$aLanguage['en']['btn_add_field'] = 'Add field';

$aLanguage['en']['fields'] = 'Filter fields';
$aLanguage['en']['field_title'] = 'Field title';
$aLanguage['en']['filter_active'] = 'Active';
$aLanguage['en']['field_goods_section'] = 'Section with goods';
$aLanguage['en']['field_view_section'] = 'Section for view';
$aLanguage['en']['field_sub'] = 'Show in subsections';
$aLanguage['en']['field_all'] = 'Show on all pages';
$aLanguage['en']['field_main'] = 'Show on main';
$aLanguage['en']['field_active'] = 'Active';
$aLanguage['en']['field_widget'] = 'Widget type';

$aLanguage['en']['wg_input'] = 'Input';
$aLanguage['en']['wg_check'] = 'Checkbox';
$aLanguage['en']['wg_num_slider'] = 'The range of values';
$aLanguage['en']['wg_check_group'] = 'Group checkbox';
$aLanguage['en']['wg_select'] = 'Select';

$aLanguage['en']['empty_widget_error'] = 'Not specified the type of widget';

return $aLanguage;