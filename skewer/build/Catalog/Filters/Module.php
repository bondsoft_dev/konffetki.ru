<?php

namespace skewer\build\Catalog\Filters;

use skewer\build\Catalog\LeftList\ModulePrototype;
use skewer\build\Component\UI;
use skewer\build\Component\Catalog;
use yii\base\UserException;


/**
 * Модуль настройки кастомных фильтров для каталога
 * Class Module
 * @package skewer\build\Catalog\Filters
 */
class Module extends ModulePrototype {

    public $iFilterId = 0;


    protected function setServiceData( UI\State\BaseInterface $oIface ) {
        $oIface->setServiceData( array(
            'filter_id' => $this->iFilterId,
        ) );
    }


    public function preExecute() {
        //$this->iFilterId = $this->getInt('filter_id');
        if ( !$this->iFilterId )
            $this->iFilterId = $this->getEnvParam('filter_id');
    }


    public function actionInit() {
        throw new UserException('Модуль устарен и находится на реконструкции');
    }

} 