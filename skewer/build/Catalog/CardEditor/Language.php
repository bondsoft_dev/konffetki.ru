<?php

$aLanguage = array();

$aLanguage['ru']['CardEditor.Catalog.tab_name'] = 'Редактор карточек';
$aLanguage['ru']['module_name'] = 'Редактор карточек';
$aLanguage['ru']['remove_card_msg'] = 'При удалении карточки товара будут удалены все товары с этой карточкой. Удалить карточку товара?';

$aLanguage['ru']['title_card_list'] = 'Список карточек товара';
$aLanguage['ru']['title_card_editor'] = 'Редактор параметров карточки "{0}"';
$aLanguage['ru']['title_new_card'] = 'Создание новой карточки товара';
$aLanguage['ru']['title_field_list'] = 'Список полей для карточки "{0}"';
$aLanguage['ru']['title_edit_field'] = 'Редактирование поля';
$aLanguage['ru']['title_new_field'] = 'Добавление нового поля';
$aLanguage['ru']['title_edit_attr'] = 'Редактор шаблона атрибутов';
$aLanguage['ru']['title_new_attr'] = 'Добавление шаблона атрибута';
$aLanguage['ru']['title_group_list'] = 'Группы полей для карточки товара';
$aLanguage['ru']['title_edit_group'] = 'Редактор группы для карточки товара';
$aLanguage['ru']['title_new_group'] = 'Добавление группы для карточки товара';
$aLanguage['ru']['title_attr_list'] = 'Список шаблонов атрибутов для карточек';

$aLanguage['ru']['head_card_name'] = 'Карточка "{0}"';
$aLanguage['ru']['head_new_card'] = 'Создание новой карточки товара';
$aLanguage['ru']['head_attr_list'] = 'Список атрибутов';
$aLanguage['ru']['head_edit_attr'] = 'Редактирование шаблона атрибута';
$aLanguage['ru']['head_new_attr'] = 'Добавление нового шаблона атрибута';
$aLanguage['ru']['head_card_group'] = 'Карточка "{0}" - группы';

$aLanguage['ru']['field_type'] = 'Тип карточки';
$aLanguage['ru']['field_title'] = 'Имя карточки';
$aLanguage['ru']['field_name'] = 'Техническое имя';
$aLanguage['ru']['field_base_card'] = 'Базовая карточка';

$aLanguage['ru']['field_f_title'] = 'Имя поля';
$aLanguage['ru']['field_f_name'] = 'Техническое имя';
$aLanguage['ru']['field_f_group'] = 'Группа полей';
$aLanguage['ru']['field_f_editor'] = 'Тип отображения';
$aLanguage['ru']['field_f_link_id'] = 'Сущность';
$aLanguage['ru']['field_f_widget'] = 'Виджет';
$aLanguage['ru']['field_f_sub_link'] = 'Справочник';
$aLanguage['ru']['field_f_multisub_link'] = 'Справочник (мультисписок)';
$aLanguage['ru']['field_f_sub_link_desc'] = 'Для выбранного типа "Подчиненная сущность"';
$aLanguage['ru']['field_f_validator'] = 'Валидаторы';
$aLanguage['ru']['field_f_def_value'] = 'Значение по умолчанию';
$aLanguage['ru']['field_f_brands'] = 'Коллекция';

$aLanguage['ru']['field_a_name'] = 'Техническое имя';
$aLanguage['ru']['field_a_title'] = 'Имя атрибута';
$aLanguage['ru']['field_a_type'] = 'Тип редактора';

$aLanguage['ru']['field_at_name'] = 'Техническое имя';
$aLanguage['ru']['field_at_entity_filter'] = 'Фильтр по сущности';
$aLanguage['ru']['field_at_field_filter'] = 'Фильтр по полю';
$aLanguage['ru']['field_at_title'] = 'Имя шаблона для атрибутов';
$aLanguage['ru']['field_at_type'] = 'Тип редактора';
$aLanguage['ru']['field_at_default'] = 'Значение по умолчанию';

$aLanguage['ru']['field_g_name'] = 'Техническое имя';
$aLanguage['ru']['field_g_title'] = 'Имя группы';

$aLanguage['ru']['btn_add_card'] = 'Создать карточку';
$aLanguage['ru']['btn_attr'] = 'Атрибуты';
$aLanguage['ru']['btn_add_field'] = 'Добавить поле';
$aLanguage['ru']['btn_params'] = 'Параметры';
$aLanguage['ru']['btn_groups'] = 'Группы';
$aLanguage['ru']['btn_back'] = 'Назад';
$aLanguage['ru']['btn_add_tpl'] = 'Добавить шаблон';

$aLanguage['ru']['error_no_card_name'] = 'Не задано имя карточки';
$aLanguage['ru']['error_no_field_name'] = 'Не задано имя поля';
$aLanguage['ru']['error_no_base_card'] = 'Не выбрана базовая карточка';
$aLanguage['ru']['error_card_identification'] = 'Невозможно инетрифицировать карточку';
$aLanguage['ru']['error_card_not_found'] = 'Не найдена карточка';
$aLanguage['ru']['error_no_card_for_field'] = 'Не задана карточка для поля';
$aLanguage['ru']['error_no_editor_for_field'] = 'Не задан тип отображения поля';
$aLanguage['ru']['error_field_exists'] = 'Поле с таким техническим именем уже существует';
$aLanguage['ru']['error_field_identification'] = 'Невозможно идентифицировать поле';
$aLanguage['ru']['error_field_not_found'] = 'Не найдено поле';
$aLanguage['ru']['error_no_group_name'] = 'Не задано имя группы';
$aLanguage['ru']['error_no_group_card'] = 'Не задана карточка для группы';
$aLanguage['ru']['error_group_exists'] = 'Группа с таким техническим именем уже существует';

$aLanguage['ru']['widget_check_group'] = 'Группа галочек';
$aLanguage['ru']['widget_select'] = 'Выпадающий список';


$aLanguage['ru']['base_group'] = 'Базовая группа';
$aLanguage['ru']['validator_set'] = 'Задан';
$aLanguage['ru']['validator_unique'] = 'Уникальное поле';

/************************/

$aLanguage['en']['CardEditor.Catalog.tab_name'] = 'Card editor';
$aLanguage['en']['module_name'] = 'Card editor';
$aLanguage['en']['remove_card_msg'] = 'When you delete an item card will delete all goods with this card. Remove the card product?';

$aLanguage['en']['title_card_list'] = 'Goods card list';
$aLanguage['en']['title_card_editor'] = 'Card editor "{0}"';
$aLanguage['en']['title_new_card'] = 'New card';
$aLanguage['en']['title_field_list'] = 'Field list for card "{0}"';
$aLanguage['en']['title_edit_field'] = 'Edit field';
$aLanguage['en']['title_new_field'] = 'New field';
$aLanguage['en']['title_edit_attr'] = 'Attribute edit';
$aLanguage['en']['title_new_attr'] = 'New arrtibute template';
$aLanguage['en']['title_group_list'] = 'Card group list';
$aLanguage['en']['title_edit_group'] = 'Edit group';
$aLanguage['en']['title_new_group'] = 'New group';
$aLanguage['en']['title_attr_list'] = 'List tempalte attributes';

$aLanguage['en']['head_card_name'] = 'Card "{0}"';
$aLanguage['en']['head_new_card'] = 'New card';
$aLanguage['en']['head_attr_list'] = 'Attribute list';
$aLanguage['en']['head_edit_attr'] = 'Editing template’s attribute';
$aLanguage['en']['head_new_attr'] = 'New attribute template';
$aLanguage['en']['head_card_group'] = 'Card "{0}" - groups';

$aLanguage['en']['field_type'] = 'Card type';
$aLanguage['en']['field_title'] = 'Title';
$aLanguage['en']['field_name'] = 'Name';
$aLanguage['en']['field_base_card'] = 'Base card';

$aLanguage['en']['field_f_title'] = 'Title';
$aLanguage['en']['field_f_name'] = 'Name';
$aLanguage['en']['field_f_group'] = 'Group';
$aLanguage['en']['field_f_editor'] = 'Editor type';
$aLanguage['en']['field_f_link_id'] = 'Entity';
$aLanguage['en']['field_f_widget'] = 'Widget';
$aLanguage['en']['field_f_sub_link'] = 'Linked entity';
$aLanguage['en']['field_f_multisub_link'] = 'Linked entity (multiselect)';
$aLanguage['en']['field_f_sub_link_desc'] = 'For type "Linked entity';
$aLanguage['en']['field_f_validator'] = 'Validator';
$aLanguage['en']['field_f_def_value'] = 'Default value';
$aLanguage['en']['field_f_brands'] = 'Brand (linked entity)';

$aLanguage['en']['field_a_name'] = 'Name';
$aLanguage['en']['field_a_title'] = 'Title';
$aLanguage['en']['field_a_type'] = 'Type';

$aLanguage['en']['field_at_name'] = 'Name';
$aLanguage['en']['field_at_entity_filter'] = 'Filter by entity';
$aLanguage['en']['field_at_field_filter'] = 'Filter by field';
$aLanguage['en']['field_at_title'] = 'Title';
$aLanguage['en']['field_at_type'] = 'Type';
$aLanguage['en']['field_at_default'] = 'Default';

$aLanguage['en']['field_g_name'] = 'Name';
$aLanguage['en']['field_g_title'] = 'Title';

$aLanguage['en']['btn_add_card'] = 'Add a card';
$aLanguage['en']['btn_attr'] = 'Attributes';
$aLanguage['en']['btn_add_field'] = 'Add field';
$aLanguage['en']['btn_params'] = 'Parameters';
$aLanguage['en']['btn_groups'] = 'Groups';
$aLanguage['en']['btn_back'] = 'Back';
$aLanguage['en']['btn_add_tpl'] = 'Add template';

$aLanguage['en']['error_no_card_name'] = 'Card name is not provided';
$aLanguage['en']['error_no_field_name'] = 'Field name is not provided';
$aLanguage['en']['error_no_base_card'] = 'Base card not selected';
$aLanguage['en']['error_card_identification'] = 'Can\'t identify card';
$aLanguage['en']['error_card_not_found'] = 'Can\'t find a card';
$aLanguage['en']['error_no_card_for_field'] = 'No card for field';
$aLanguage['en']['error_no_editor_for_field'] = 'Show type not provided';
$aLanguage['en']['error_field_exists'] = 'Field with such name already exists';
$aLanguage['en']['error_field_identification'] = 'Can\'t identify field';
$aLanguage['en']['error_field_not_found'] = 'Field not found';
$aLanguage['en']['error_no_group_name'] = 'Group name not provided';
$aLanguage['en']['error_no_group_card'] = 'Card for group not provided';
$aLanguage['en']['error_group_exists'] = 'Group with such name already exists';

$aLanguage['en']['widget_check_group'] = 'check group';
$aLanguage['en']['widget_select'] = 'select';


$aLanguage['en']['base_group'] = 'Base group';
$aLanguage['en']['validator_set'] = 'Required';
$aLanguage['en']['validator_unique'] = 'Unique';

return $aLanguage;