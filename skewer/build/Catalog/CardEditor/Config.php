<?php
/**
 * User: kolesnikiv
 * Date: 31.07.13
 */
$aConfig['name']     = 'CardEditor';
$aConfig['title']    = 'Каталог. Редактор карточек.';
$aConfig['version']  = '1.000a';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::CATALOG;
$aConfig['useNamespace'] = true;
$aConfig['languageCategory'] = 'card';

return $aConfig;
