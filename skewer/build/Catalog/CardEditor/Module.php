<?php

namespace skewer\build\Catalog\CardEditor;


use skewer\build\Catalog\LeftList\ModulePrototype;
use skewer\build\Component\Catalog;
use skewer\build\Component\UI;
use skewer\build\libs\ft;
use yii\base\UserException;
use yii\helpers\ArrayHelper;


/**
 * Модуль редактора карточек товаров
 * Class Module
 * @package skewer\build\Catalog\CardEditor
 */
class Module extends ModulePrototype {

    // число элементов на страницу
    public $iOnPage = 20;

    // текущий номер страницы ( с 0, а приходит с 1 )
    public $iPage = 0;

    private function getCardId() {
        $card = $this->getInnerData( 'card', 0 );
        if ( !$card ) $card = $this->getInDataVal( 'id' );
        if ( !$card ) throw new UserException( 'Card not found' );
        $this->setInnerData( 'card', $card );
        return $card;
    }


    public function actionInit() {
        $this->actionCardList();
    }


    /**
     * Список карточек товара
     */
    public function actionCardList() {

        $this->setInnerData( 'card', 0 );

        // создаем форму
        $oFormBuilder = UI\StateBuilder::newList();

        // установка заголовка
        $this->setPanelName( \Yii::t( 'card', 'title_card_list' ) );

        // добавляем поля
        $oFormBuilder
            ->fieldHide( 'id', 'id', 'i', ['listColumns.width' => 40] )
            ->fieldString( 'type', \Yii::t('card', 'field_type') )
            ->fieldString( 'title', \Yii::t('card', 'field_title'), ['listColumns.flex' => 1] )
            ->addWidget( 'type', 'skewer\\build\\Catalog\\CardEditor\\Api', 'applyTypeWidget' );

        // добавляем данные
        $oFormBuilder
            ->setValue( Catalog\Card::getGoodsCards( true ) );

        // элементы управления
        $oFormBuilder
            ->button( \Yii::t('card', 'btn_add_card'), 'CardEdit', 'icon-add' )
            ->buttonRowUpdate( 'FieldList' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

    }


    /**
     * Форма редактирования основных параметров карточки товара
     */
    public function actionCardEdit() {

        $iCardId = $this->getInnerData( 'card' );

        if ( $iCardId ) {
            $oCard = Catalog\Card::get( $iCardId );
            $this->setPanelName( \Yii::t('card', 'title_card_editor', $oCard->title) );
        } else {
            $oCard = Catalog\Card::get();
            $this->setPanelName( \Yii::t('card', 'title_new_card') );
        }


        // создание и заполнение формы
        $oFormBuilder = UI\StateBuilder::newEdit();

        // добавляем поля
        $oFormBuilder
            ->fieldHide( 'id', 'id' )
            ->fieldString( 'title', \Yii::t('card', 'field_title'), ['listColumns.flex' => 1] )
            ->fieldString( 'name', \Yii::t('card', 'field_name'), ['disabled' => (bool)$iCardId] )
        ;

        if ( !$oCard->isBasic() )
            $oFormBuilder
                ->fieldSelect( 'parent', \Yii::t('card', 'field_base_card'), Api::getBasicCardList( $iCardId ) )
                ->fieldHide( 'type', '', 's' )
            ;
        else
            $oFormBuilder
                ->fieldHide( 'parent', \Yii::t('card', 'field_base_card'), 's' )
                ->fieldHide( 'type', '' )
            ;

        // устанавливаем значения
        $oFormBuilder->setValue( $oCard );

        // добавляем элементы управления
        $oFormBuilder
            ->button( \Yii::t('adm','save'), 'CardSave', 'icon-save' )
            ->button( \Yii::t('adm','cancel'), $iCardId ? 'FieldList' : 'CardList', 'icon-cancel' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Сохранение основных параметров карточки товара
     */
    public function actionCardSave() {

        $aData = $this->getInData();
        $id = ArrayHelper::getValue( $aData, 'id', null );
        $type = ArrayHelper::getValue( $aData, 'type', '' );
        $parent = ArrayHelper::getValue( $aData, 'parent', '' );

        if ( !($title = ArrayHelper::getValue( $aData, 'title', '' )) )
            throw new UserException( \Yii::t('card', 'error_no_card_name') );

        if ( $type != Catalog\Card::TypeBasic && !$parent )
            throw new UserException( \Yii::t('card', 'error_no_base_card') );

        // всегда только расширенная карточка
        if ( $type != Catalog\Card::TypeBasic )
            $aData['type'] = Catalog\Card::TypeExtended;

        $aData['module'] = 'Catalog';

        $oCard = Catalog\Card::get( $id );
        $oCard->setData( $aData );
        $oCard->save();
        $oCard->updCache();

        $this->setInnerData( 'card', $oCard->id );

        $this->actionFieldList();

    }


    /**
     * Удаление карточки товара
     */
    public function actionCardRemove() {

//        $aData = $this->getInData();
//        $id = ArrayHelper::getValue( $aData, 'id', null );
        $id = $this->getCardId();

        $oCard = Catalog\Card::get( $id );

        if ( !$id || !$oCard )
            throw new UserException( \Yii::t('card', 'error_card_not_found') );

        $oCard->delete();

        // todo здесь вызов удаления карточек для новых товаров

        $this->actionCardList();
    }


    /**
     * Список полей для карточки товара
     */
    public function actionFieldList() {

        $oCard = Catalog\Card::get( $this->getCardId() );

        $sHeadText = \Yii::t( 'card', 'head_card_name', $oCard->title );
        $this->setPanelName( \Yii::t('card', 'title_field_list',$oCard->title) );


        // создаем интерфейс
        $oFormBuilder = UI\StateBuilder::newList();

        $oFormBuilder
            ->addHeadText( sprintf('<h1>%s</h1>', $sHeadText) );

        // добавляем поля
        $oFormBuilder
            //->fieldHide( 'id', 'id' )
            ->fieldString( 'name', \Yii::t('card', 'field_f_name') )
            ->fieldString( 'title', \Yii::t('card', 'field_f_title'), ['listColumns.flex' => 1] )
            //->fieldString( 'group', \Yii::t('card', 'field_f_group') )
            ->fieldString( 'editor', \Yii::t('card', 'field_f_editor') )
            ->setGroups( 'group' )
            ->addWidget( 'group', 'skewer\\build\\Catalog\\CardEditor\\Api', 'applyGroupWidget' )
            ->addWidget( 'editor', 'skewer\\build\\Catalog\\CardEditor\\Api', 'applyEditorWidget' );

        // устанавливаем значения
        $oFormBuilder->setValue( $oCard->getFields() );

        // элементы управления
        $oFormBuilder
            ->button( \Yii::t('card', 'btn_add_field'), 'FieldEdit', 'icon-add' )
            ->button( \Yii::t('card', 'btn_params'), 'CardEdit', 'icon-edit' )
            ->button( \Yii::t('card', 'btn_groups'), 'GroupList', 'icon-edit' )
            ->button( \Yii::t('card', 'btn_back'), 'CardList', 'icon-cancel' );

        if ( $oCard->isExtended() )
            $oFormBuilder
                ->buttonSeparator( '->' )
                ->buttonConfirm( \Yii::t('adm','del'), 'CardRemove', 'icon-delete', \Yii::t('card', 'remove_card_msg') )
            ;

        // обработчики на список
        $oFormBuilder
            ->buttonRowUpdate( 'FieldEdit' )
            ->buttonRowDelete( 'FieldRemove' )
            ->enableDragAndDrop( 'sortFields' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Обработчик события сортировки полей карточки товара
     */
    protected function actionSortFields() {

        $aData = $this->get( 'data' );
        $aDropData = $this->get( 'dropData' );
        $sPosition = $this->get( 'position' );

        // fixme доработать передачу параметров и перестроение сущности
        if ( Catalog\model\FieldTable::sort( $aData, $aDropData, $sPosition ) )
            Catalog\Card::build( $aData['entity'] );

    }


    /**
     * Форма редактирование поля карточки товара
     * @return int
     */
    public function actionFieldEdit() {

        $iFieldId = $this->getInDataValInt( 'id' );
        $iCardId = $this->getInnerData( 'card', 0 );


        $oCard = Catalog\Card::get( $iCardId );
        $sHeadText = \Yii::t('card', 'head_card_name', $oCard->title);

        if ( $iFieldId ) {
            $oField = Catalog\Card::getField( $iFieldId );
            $oField->validator = $oField->getValidatorList();
            $this->setPanelName( \Yii::t('card', 'title_edit_field') );
        } else {
            $oField = Catalog\Card::getField();
            $this->setPanelName( \Yii::t('card', 'title_new_field') );
        }


        // создание формы
        $oFormBuilder = UI\StateBuilder::newEdit();

        $oFormBuilder
            ->addHeadText( sprintf('<h1>%s</h1>', $sHeadText) );

        // добавление полей
        $oFormBuilder
            ->fieldHide( 'id', 'id' )
            ->fieldString( 'title', \Yii::t('card', 'field_f_title'), ['listColumns.flex' => 1] )
            ->fieldString( 'name', \Yii::t('card', 'field_f_name'), ['disabled' => (bool)$iFieldId] )
            ->fieldSelect( 'group', \Yii::t('card', 'field_f_group'), $oCard->getGroupList() )
            ->fieldSelect( 'editor', \Yii::t('card', 'field_f_editor'), Api::getSimpleTypeList(), ['onUpdateAction'=>'updFields'] )
            ->fieldSelect( 'link_id', \Yii::t('card', 'field_f_link_id'), Api::getEntityList( $oField ), ['disabled' => !$oField->isLinked()] )
            ->fieldSelect( 'widget', \Yii::t('card', 'field_f_widget'), Api::getSimpleWidgetList( $oField ) )
            ->fieldMultiSelect( 'validator', \Yii::t('card', 'field_f_validator'), Catalog\Validator::getListWithTitles(), $oField->getValidatorList() )
            ->fieldString( 'def_value', \Yii::t('card', 'field_f_def_value') )
        ;

        // добавление арибутов в редактор
        $aAttrList = $oField->getAttr();
        if ( count( $aAttrList ) ) {
            foreach ( $aAttrList as $aAttrParam ) {
                $oFormBuilder->fieldWithValue( 'attr_' . $aAttrParam['id'], \Yii::t( 'catalog', 'attr_' . $aAttrParam['name'] ), $aAttrParam['type'], $aAttrParam['value'] );
            }
        }

        // добавление значений
        $oFormBuilder
            ->setValue( $oField );

        // элементы управления
        $oFormBuilder
            ->button( \Yii::t('adm','save'), 'FieldSave', 'icon-save' )
            ->button( \Yii::t('adm','cancel'), 'FieldList', 'icon-cancel' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }


    /**
     * Обработчик изменения значения поля editor ("Тип отображения")
     */
    public function actionUpdFields() {

        $aFormData = $this->get('formData', array());
        $sEditor = isset($aFormData['editor']) ? $aFormData['editor'] : '';
        $iTypeId = isset($aFormData['link_id']) ? $aFormData['link_id'] : '';
        $sWidget = isset($aFormData['widget']) ? $aFormData['widget'] : '';

        $id = isset($aFormData['id']) ? $aFormData['id'] : null;

        $oField = Catalog\Card::getField( $id );
        $oField->editor = $sEditor;

        $aWidgetList = Api::getSimpleWidgetList( $oField );

        $oTypeField = new \ExtFormRows();
        $oTypeField->addFieldSelect( 'link_id', Api::getEntityList( $oField ), $iTypeId, !$oField->isLinked() );
        $oTypeField->addFieldSelect( 'widget', $aWidgetList, $sWidget );

        $oTypeField->setData( $this );
    }


    /**
     * Состояние сохранение поля
     */
    public function actionFieldSave() {

        $card = $this->getInnerData( 'card', 0 );
        $data = $this->getInData();
        $id = ArrayHelper::getValue( $data, 'id', null );
        $title = ArrayHelper::getValue( $data, 'title', '' );
        $editor = ArrayHelper::getValue( $data, 'editor', '' );
        $validators = ArrayHelper::getValue( $data, 'validator', '' );

        if ( !$title )
            throw new UserException( \Yii::t('card', 'error_no_field_name') );

        if ( !$card )
            throw new UserException( \Yii::t('card', 'error_card_not_found') );

        if ( !$editor )
            throw new UserException( \Yii::t('card', 'error_no_editor_for_field') );

        //\Yii::t('card', 'error_field_identification') \Yii::t('card', 'error_field_exists')

        $oField = Catalog\Card::getField( $id );
        $oField->setData( $data );
        $oField->entity = $card;
        $oField->save();

        // сохранение валидаторов
        $oField->setValidator( $validators );

        // сохранение атрибутов для поля
        foreach ( $data as $sKey => $sValue )
            if ( !isSet( $oField->$sKey ) && strpos($sKey,'attr_') === 0 )
                $oField->setAttr( (int)substr($sKey, 5), $sValue );

        // rebuild card
        Catalog\Card::build( $oField->entity );

        $this->actionFieldList();

    }


    /**
     * Удаление поля
     * @throws \skewer\build\libs\ft\Exception
     */
    public function actionFieldRemove() {

        $data = $this->getInData();

        $id = ArrayHelper::getValue( $data, 'id', null );

        if ( !$id )
            throw new UserException( \Yii::t('card', 'error_field_not_found') );

        $oField = Catalog\Card::getField( $id );

        if ( in_array( $oField->name, ['id', 'title', 'active', 'alias'] ) )
            throw new UserException( \Yii::t('dict', 'error_field_cant_removed') );

        $oField->delete();

        // rebuild card
        Catalog\Card::build( $oField->entity );

        $this->actionFieldList();

    }


    /**
     * Список групп для карточки товара
     */
    public function actionGroupList() {

        $iCardId = $this->getInnerData( 'card' );

        if ( !$iCardId )
            throw new UserException( \Yii::t( 'card', 'error_card_not_found' ) );

        $oCard = Catalog\Card::get( $iCardId );

        $this->setPanelName( \Yii::t( 'card', 'title_group_list' ) . ' "' . $oCard->title . '"' );


        // создание формы
        $oFormBuilder = UI\StateBuilder::newList();

        // добавление полей
        $oFormBuilder
            ->fieldHide( 'id', 'id', 'i' )
            ->fieldString( 'name', \Yii::t( 'card', 'field_g_name' ) )
            ->fieldString( 'title', \Yii::t( 'card', 'field_g_title' ), ['listColumns.flex' => 1] )

        // добавление данных
            ->setValue( $oCard->getGroups() )

        // элементы управления
            ->button( \Yii::t('adm','add'), 'GroupEdit', 'icon-add' )
            ->button( \Yii::t('adm','cancel'), 'FieldList', 'icon-cancel' )
            ->buttonRowUpdate( 'GroupEdit' )
            ->buttonRowDelete( 'GroupRemove' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Форма редактирования групп
     * @return int
     */
    public function actionGroupEdit() {

        $iGroupId = $this->getInDataVal( 'id' );
        $iCardId = $this->getInnerData( 'card' );

        $oCard = Catalog\Card::get( $iCardId );

        if ( $iGroupId ) {
            $this->setPanelName( \Yii::t('card', 'title_edit_group') . ' "' . $oCard->title . '"' );
            $oGroup = Catalog\Card::getGroup( $iGroupId );
        } else {
            $this->setPanelName( \Yii::t('card', 'title_new_group') . ' "' . $oCard->title . '"' );
            $oGroup = Catalog\Card::getGroup();
        }


        // создание формы
        $oFormBuilder = UI\StateBuilder::newEdit();

        // установка полей
        $oFormBuilder
            ->fieldHide( 'id', 'id' )
            ->fieldString( 'title', \Yii::t('card', 'field_g_title'), ['listColumns.flex' => 1] )
            ->fieldString( 'name', \Yii::t('card', 'field_g_name'), ['disabled' => (bool)$iGroupId] )
        ;

        // установка данных
        $oFormBuilder->setValue( $oGroup );

        // элементы управления
        $oFormBuilder
            ->button( \Yii::t('adm','save'), 'GroupSave', 'icon-save' )
            ->button( \Yii::t('adm','cancel'), 'GroupList', 'icon-cancel' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }


    /**
     * Сохранение группы
     */
    public function actionGroupSave() {

        $id = $this->getInDataVal( 'id' );
        $title = $this->getInDataVal( 'title' );
        $name = $this->getInDataVal( 'name' );
        $entity = $this->getInnerData( 'card' );

        if ( !$title )
            throw new UserException( \Yii::t( 'card', 'error_no_group_name' ) );

        if ( !$entity )
            throw new UserException( 'error_no_group_card' );

        $oGroup = Catalog\Card::getGroup( $id );
        $oGroup->title = $title;
        $oGroup->entity = $entity;
        $oGroup->name = $name;
        $oGroup->save();

        $this->actionGroupList();

    }


    /**
     * Удаление группы
     */
    public function actionGroupRemove() {

        $iGroupId = $this->getInDataVal( 'id' );

        $oGroup = Catalog\Card::getGroup( $iGroupId );

        if ( $iGroupId && $oGroup )
            $oGroup->delete();

        $this->actionGroupList();
    }

}