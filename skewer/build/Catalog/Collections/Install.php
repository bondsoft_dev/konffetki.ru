<?php

namespace skewer\build\Catalog\Collections;


use skewer\build\Component\Gallery\Format;
use skewer\build\Component\Gallery\Profile;
use skewer\build\Component\Section\Parameters;
use skewer\build\Component\SEO;

class Install extends \skModuleInstall {


    public function init() {
        return true;
    }


    public function install() {

        /**
         * TODO: когда будет реализован апи галереи, переписать косныль ниже
         *  тут должно быть добавление нового профиля с 3мя форматами
         */

        // добавляем новый профиль форматов
        $profile = Profile::setProfile([
            'title' => \Yii::t('data/gallery', 'profile_collection_name', [], \Yii::$app->language ),
            'name' => 'collection',
            'active' => 1
        ]);

        if ( !$profile )
            $this->fail('cant create gallery profile');

        $oMinFormat = Format::setFormat([
            'profile_id' => $profile,
            'title' => \Yii::t('data/gallery', 'format_collection_min_name', [], \Yii::$app->language ),
            'name' => 'colmin',
            'width' => 160,
            'height' => 98,
            'resize_on_larger_side' => 0,
            'scale_and_crop' => 1,
            'use_watermark' => 0,
            'watermark' => '',
            'watermark_align' => 84,
            'active' => 1
        ]);

        $oMedFormat = Format::setFormat([
            'profile_id' => $profile,
            'title' => \Yii::t('data/gallery', 'format_collection_med_name', [], \Yii::$app->language ),
            'name' => 'colmed',
            'width' => 200,
            'height' => 200,
            'resize_on_larger_side' => 0,
            'scale_and_crop' => 1,
            'use_watermark' => 0,
            'watermark' => '',
            'watermark_align' => 84,
            'active' => 1
        ]);

        $oMaxFormat = Format::setFormat([
            'profile_id' => $profile,
            'title' => \Yii::t('data/gallery', 'format_collection_max_name', [], \Yii::$app->language ),
            'name' => 'colmax',
            'width' => 800,
            'height' => 0,
            'resize_on_larger_side' => 0,
            'scale_and_crop' => 1,
            'use_watermark' => 0,
            'watermark' => '',
            'watermark_align' => 84,
            'active' => 1
        ]);


        // todo создание SEO шаблона для коллекций
        $tpl = new SEO\TemplateRow();
        $tpl->sid = SEO\Api::TPL_CATALOG_COLLECTION;
        $tpl->name = 'Страница коллекции в каталоге';
        $tpl->title = '[Название коллекции]. [Название страницы]. [Цепочка разделов до главной] [Название сайта]';
        $tpl->description = '[Название сайта], [Цепочка разделов до главной], [Название страницы], [Название коллекции]';
        $tpl->keywords = '[название коллекции], [название страницы]';
        $tpl->info = "[Название коллекции] - название коллекции\n[название коллекции] - название коллекции с маленькой буквы";
        $tpl->save();

        $tpl = new SEO\TemplateRow();
        $tpl->sid = SEO\Api::TPL_CATALOG_COLLECTION_ELEMENT;
        $tpl->name = 'Страница элемента коллекции в каталоге';
        $tpl->title = '[Элемент коллекции]. [Название коллекции]. [Название страницы]. [Цепочка разделов до главной] [Название сайта]';
        $tpl->description = '[Название сайта], [Цепочка разделов до главной], [Название страницы], [Название коллекции], [Элемент коллекции]';
        $tpl->keywords = '[элемент коллекции], [название коллекции], [название страницы]';
        $tpl->info = "[Элемент коллекции] - название элемента коллекции\n[элемент коллекции] - название элемента коллекции с маленькой буквы";
        $tpl->save();

        // добавление модуля для вывода на главной
        $group = 'collection';
        Parameters::setParams( \Yii::$app->sections->main(), $group, '.title', 'Коллекции каталога' );
        Parameters::setParams( \Yii::$app->sections->main(), $group, 'layout', 'content' );
        Parameters::setParams( \Yii::$app->sections->main(), $group, 'titleOnMain', 'Catalog collection', '', \Yii::t('catalog','param_title_on_main', [], \Yii::$app->language ) );
        Parameters::setParams( \Yii::$app->sections->main(), $group, 'object', 'CatalogViewer' );
        Parameters::setParams( \Yii::$app->sections->main(), $group, 'onMainCollection', '1' );
        Parameters::setParams( \Yii::$app->sections->main(), $group, 'listTemplate', 'list', 'list editor.type_collection_list' . "\n"
            .'slider editor.type_collection_slider', \Yii::t('editor', 'type_category_view', [], \Yii::$app->language ) );



        return true;
    }

    public function uninstall() {
        $this->fail("Нельзя удалить компонент каталога");
        return true;
    }

}