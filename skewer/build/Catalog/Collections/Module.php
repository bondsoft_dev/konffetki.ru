<?php

namespace skewer\build\Catalog\Collections;

use skewer\build\Catalog\Dictionary as Dict;
use skewer\build\Component\Catalog;
use skewer\build\Component\orm\Query;
use skewer\build\Component\UI;
use skewer\build\libs\ft;
use yii\base\UserException;
use yii\helpers\ArrayHelper;


/**
 * Модуль настройки брендов
 * Class Module
 * @package skewer\build\Catalog\Collections
 */
class Module extends Dict\Module {


    public function getTitle() {
        return \Yii::t('collections', 'module_name');
    }


    protected function actionInit() {
        $this->actionList();
    }


    protected function actionTools() {}


    /**
     * Список коллекций каталога
     */
    protected function actionList() {

        $this->setInnerData( 'card', 0 );
        $this->setInnerData( 'rowId', 0 );

        $oFormBuilder = UI\StateBuilder::newList();

        // установка заголовка
        $this->setPanelName( \Yii::t('collections', 'coll_list_for_cat') );

        $oFormBuilder
            ->fieldHide( 'id', 'id', 'i', ['listColumns.width' => 40] )
            ->fieldString( 'title', \Yii::t('collections', 'coll_name'), ['listColumns.flex' => 1] )
            ->setValue( Catalog\Card::getCollections() )
            ->button( \Yii::t('collections', 'create_coll'), 'Edit', 'icon-add' )
            ->buttonRowUpdate( 'View' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Форма редактирования параметров коллекции
     */
    protected function actionEdit() {

        $oCard = Catalog\Card::get();
        $this->setPanelName( \Yii::t('collections', 'new_coll') );

        $oFormBuilder = UI\StateBuilder::newEdit();
        $oFormBuilder
            ->addHeadText( '<h1>' . \Yii::t('collections', 'new_coll') . '</h1>' )
            ->fieldHide( 'id', 'id' )
            ->fieldString( 'title', \Yii::t('collections', 'coll_name'), ['listColumns.flex' => 1] )
            ->fieldString( 'name', \Yii::t('collections', 'system_name') )
            ->setValue( $oCard )
            ->button( \Yii::t('adm', 'save'), 'Save', 'icon-save' )
            ->button( \Yii::t('adm', 'cancel'), 'List', 'icon-cancel' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Метод сохранения параметров коллекции
     * @throws UserException
     */
    protected function actionSave() {

        $aData = $this->getInData();

        $sDictTitle = $this->getInDataVal( 'title' );

        if ( !$sDictTitle )
            throw new UserException( \Yii::t('collections', 'error_noname_seted') );

        $oCard = Catalog\Card::addCollection( $aData );

        $this->setInnerData( 'card', $oCard->id );

        $this->actionView();
    }


    /**
     * Список позиций в коллекции
     * @throws UserException
     */
    protected function actionView() {

        $title = $this->getStr( 'title', $this->getInnerData( 'title', '' ) );
        $active = $this->getStr( 'active', $this->getInnerData( 'active', '' ) );
        $card = $this->getCard();

        // получение контентных данных
        $oTable = ft\Cache::getMagicTable( $card );
        if ( !$oTable )
            throw new UserException( \Yii::t('collections', 'error_dict_not_found') );

        // набор объектов на вывод
        $query = $oTable->find()->asArray();
        if ( $title )
            $query->where( 'title LIKE ?', '%' . $title . '%' );
        if ( $active )
            $query->where( 'active', $active == 1 );


        // установка заголовка
        $this->setPanelName( \Yii::t('collections', 'coll_panel_name', Catalog\Card::getTitle( $card )) );

        // контентная форма
        $oFormBuilder = UI\StateBuilder::newList();

        $oFormBuilder
            ->addFilterText( 'title', $title, \Yii::t('collections', 'field_title') )
            ->addFilterSelect( 'active', [
                1 => \Yii::t('page', 'yes'),
                2 => \Yii::t('page', 'no')
            ], $active, \Yii::t('collections', 'field_active') )
            ->addFilterAction( 'view' )

            ->fieldHide( 'id', 'id', 'i', ['listColumns.width' => 40] )
            ->fieldString( 'title', \Yii::t('collections', 'field_title'), ['listColumns.flex' => 2] )
            ->fieldCheck( 'active', \Yii::t('collections', 'field_active'), ['listColumns.flex' => 1] )
            ->fieldCheck( 'on_main', \Yii::t('collections', 'field_on_main'), ['listColumns.flex' => 1] )
            ->setValue( $query->getAll() )
            ->button( \Yii::t('collections', 'add'), 'ItemEdit', 'icon-add' )
            ->button( \Yii::t('collections', 'edit_title'), 'EditTitle', 'icon-edit' )
            ->button( \Yii::t('adm', 'back'), 'List', 'icon-cancel' )
            ->buttonSeparator( '->' )
            ->buttonConfirm( \Yii::t('adm', 'del'), 'Remove', 'icon-delete', \Yii::t('collections', 'msg_remove_coll', Catalog\Card::getTitle( $card ) ) )
            ->buttonRowUpdate( 'ItemEdit' )
            ->buttonRowDelete( 'ItemRemove' )

            ->setEditableFields( ['active','on_main'], 'ItemFastSave' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Форма редактирования значения для справочника
     * @return int
     */
    protected function actionItemEdit() {

        $id = $this->getInDataVal( 'id', $this->getInnerData( 'rowId', 0 ) );
        $this->setInnerData( 'rowId', $id );
        $card = $this->getInnerData( 'card' );

        if ( !$card )
            throw new UserException( "Card not found!" );

        $oTable = ft\Cache::getMagicTable( $card );
        if ( !$oTable )
            throw new UserException( \Yii::t('collections', 'error_dict_not_found') );

        $oItem = $id ? $oTable->find( $id ) : $oTable->getNewRow();

        // def values
        if ( !$id ) {
            $oItem->title = '';
            $oItem->active = 1;
        }

        // установка заголовка
        $oDict = Catalog\Card::get( $card );
        $this->setPanelName( \Yii::t('collections', 'dict_panel_name', Catalog\Card::getTitle( $card )) );

        $oFormBuilder = UI\StateBuilder::newEdit();

        $oFormBuilder
            ->fieldHide( 'id', 'id' )
        ;

        $aFields = $oDict->getFields();
        foreach ( $aFields as $oField ) {
            $oFormBuilder->fieldByEntity( $oField );
        }

        $oFormBuilder
            ->setValue( $oItem )
            ->button( \Yii::t('adm', 'save'), 'ItemSave', 'icon-save' )
            ->button( \Yii::t('adm', 'cancel'), 'View', 'icon-cancel' )
        ;

        if ( $id ) {
            $oFormBuilder
                ->buttonSeparator()
                ->button( \Yii::t('collections', 'goods'), 'GoodsView', 'icon-view' )
            ;
        }

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Сохранение значения всех полей для коллекции
     */
    protected function actionItemSave() {

        $aData = $this->getInData();
        $id = $this->getInDataVal( 'id', 0 );
        $card = $this->getInnerData( 'card' );

        if ( !$card )
            throw new UserException( "Card not found!" );

        $oTable = ft\Cache::getMagicTable( $card );
        if ( !$oTable )
            throw new UserException( \Yii::t('collections', 'error_dict_not_found') );

        $oItem = $id ? $oTable->find( $id ) : $oTable->getNewRow();

        $oItem->setData( $aData );

        if ( !$oItem->title )
            throw new UserException( \Yii::t('collections', 'error_title_not_found') );

        // генерация alias для коллекции
        if ( !$oItem->alias )
            $oItem->alias = $oItem->title;

        $oItem->alias = $this->getUniqueAlias( $card, $oItem->alias, $id );

        if ( $oItem->save() ) {

            $oSearch = new Search();
            $oSearch->setCard( $oTable->getName() );
            $oSearch->updateByObjectId( $oItem->id );

            $this->actionView();

        } else {
            $this->addError( $oItem->getError() );
        }

    }

    protected function actionItemRemove() {

        $id = $this->getInDataVal( 'id', 0 );
        $card = $this->getInnerData( 'card' );

        $this->setInnerData( 'rowId', 0 );

        if ( !$id )
            throw new UserException( \Yii::t('collections', 'error_row_not_found') );

        $oCurTable = ft\Cache::getMagicTable( $card );
        if ( !$oCurTable )
            throw new UserException( \Yii::t('collections', 'error_dict_not_found') );

        // поиск полей связанных со справочником
        $oEntity = Catalog\Card::get( $card );
        $aFields = Catalog\model\FieldTable::find()
            ->where( 'link_type', ft\Relation::ONE_TO_MANY )
            ->where( 'link_id', $oEntity->id )
            ->getAll();

        foreach ( $aFields as $oField ) {

            $oTable = ft\Cache::getMagicTable( $oField->entity );

            if ( !$oTable )
                continue;

            // очищаем значения полей
            $oTable->update( [$oField->name => ''], [$oField->name => $id] );
        }

        // удаление значения из справочника
        $oItem = $oCurTable->find( $id );
        $oItem->delete();

        $oSearch = new Search();
        $oSearch->setCard( $oCurTable->getName() );
        $oSearch->deleteByObjectId( $oItem->id );

        $this->actionView();
    }


    /**
     * Генерация уникального алиаса для сущности
     * @param $card
     * @param $alias
     * @param $id
     * @return array|string
     * @throws ft\Exception
     * todo найти место для этой функции
     */
    private function getUniqueAlias( $card, $alias, $id ) {

        $model = ft\Cache::get( $card );

        $alias = \skTranslit::generateAlias( $alias );

        $flag = (bool)Query::SelectFrom( $model->getTableName() )
            ->where( 'alias', $alias )
            ->andWhere( 'id!=?', $id )
            ->getCount()
        ;

        if ( !$flag )
            return $alias;

        preg_match( '/^(\S+)(-\d+)?$/Uis', $alias, $res );
        $aliasTpl = isSet( $res[1] ) ? $res[1] : $alias;
        $iCnt = isSet( $res[2] ) ? -(int)$res[2] : 0;
        while ( substr( $aliasTpl, -1 ) == '-' )
            $aliasTpl = substr( $aliasTpl, 0 , strlen($aliasTpl) - 1 );

        do {

            $iCnt++;
            $alias = $aliasTpl . '-' . $iCnt;

            $flag = (bool)Query::SelectFrom( $model->getTableName() )
                ->where( 'alias', $alias )
                ->andWhere( 'id!=?', $id )
                ->getCount()
            ;

        } while ( $flag );


        return $alias;
    }


    /**
     * Метод сохранения значения поля для коллекции (редактирование из списка)
     * @throws UserException
     */
    protected function actionItemFastSave() {

        $data = $this->get( 'data' );
        $field = $this->get( 'field_name' );
        $card = $this->getInnerData( 'card' );

        $id = ArrayHelper::getValue( $data, 'id', 0 );
        $value = ArrayHelper::getValue( $data, $field, '' );

        if ( !$id )
            throw new UserException( "Card not found!" );

        if ( !$card )
            throw new UserException( "Card not found!" );

        $oTable = ft\Cache::getMagicTable( $card );
        if ( !$oTable )
            throw new UserException( \Yii::t('collections', 'error_dict_not_found') );

        $oItem = $id ? $oTable->find( $id ) : $oTable->getNewRow();

        $oItem->setData( [$field => $value] );

        if ( $oItem->save() ) {

            $oSearch = new Search();
            $oSearch->setCard( $oTable->getName() );
            $oSearch->updateByObjectId( $oItem->id );

            $this->actionView();

        } else {
            $this->addError( $oItem->getError() );
        }
    }


    /**
     * Список товаров связанных с коллекцией
     */
    protected function actionGoodsView() {

        $id = $this->getInnerData( 'rowId', 0 );
        $card = $this->getInnerData( 'card' );

        // fixme Обработка нескольких связей
        $field = Catalog\model\FieldTable::find()
            ->where( 'link_id', $card )
            ->getOne();

        $items = [];

        if ( $field ) {
            $items = Catalog\GoodsSelector::getList4Collection( 0, $field->name, $id )->parse();
        }

        $oFormBuilder = UI\StateBuilder::newList();


        $oTable = ft\Cache::getMagicTable( $card );
        if ( !$oTable )
            throw new UserException( \Yii::t('collections', 'error_dict_not_found') );
        $oItem = $id ? $oTable->find( $id ) : $oTable->getNewRow();

        // установка заголовка
        //$this->setPanelName( \Yii::t('collections', 'coll_list_for_cat') );
        $this->setPanelName( \Yii::t('collections', 'goods_coll_list_panel', [$oItem->title, Catalog\Card::getTitle( $card )] ) );

        $oFormBuilder
            ->fieldHide( 'id', 'id', 'i', ['listColumns.width' => 40] )
            ->fieldString( 'title', \Yii::t('collections', 'goods_name'), ['listColumns.flex' => 1] )
            ->setValue( $items )
            ->button( \Yii::t('adm', 'back'), 'ItemEdit', 'icon-cancel' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

    }


    /**
     * Интерфейс редактиования заголовка
     */
    protected function actionEditTitle() {

        $card = $this->getInnerData( 'card' );
        $oCard = Catalog\Card::get( $card );

        // создание и заполнение формы
        $oFormBuilder = UI\StateBuilder::newEdit();

        // добавляем поля
        $oFormBuilder
            ->fieldHide( 'id', 'id' )
            ->fieldString( 'title', \Yii::t('collections', 'field_title'), ['listColumns.flex' => 1] )
        ;

        // устанавливаем значения
        $oFormBuilder->setValue( $oCard );

        // добавляем элементы управления
        $oFormBuilder
            ->button( \Yii::t('adm', 'save'), 'SaveTitle', 'icon-save' )
            ->button( \Yii::t('adm', 'cancel'), 'View', 'icon-cancel' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );


    }


    /**
     * Состояние сохранения заголовка
     * @throws \Exception
     * @throws UserException
     */
    protected function actionSaveTitle() {

        $aData = $this->getInData();
//        $type = ArrayHelper::getValue( $aData, 'type', '' );
//        $parent = ArrayHelper::getValue( $aData, 'parent', '' );

        $card = $this->getInnerData( 'card' );

        if ( !($title = ArrayHelper::getValue( $aData, 'title', '' )) )
            throw new UserException( \Yii::t('collections', 'error_no_card_name') );

//        if ( $type != Catalog\Card::TypeBasic && !$parent )
//            $this->riseError( \Yii::t('collections', 'error_no_base_card') );

        // всегда только расширенная карточка
//        if ( $type != Catalog\Card::TypeBasic )
//            $aData['type'] = Catalog\Card::TypeExtended;

        $oCard = Catalog\Card::get( $card );
        $oCard->setData( ['title' => $title] );
        $oCard->save();
        $oCard->updCache();

        $this->actionView();
    }

    /**
     * @inheritDoc
     */
    protected function actionRemove() {

        $card = $this->getInnerData( 'card' );

        $oCard = Catalog\Card::get( $card );

        if ( $oCard ) {

            $oSearch = new Search();
            $oSearch->setCard( $oCard->name );
            $oSearch->deleteAll();

        }

        parent::actionRemove();

    }


}