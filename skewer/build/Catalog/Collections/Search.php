<?php

namespace skewer\build\Catalog\Collections;

use skewer\build\Component\orm\Query;
use skewer\build\Component\Search\Prototype;
use skewer\build\Component\Search\Row;
use skewer\build\Component\Catalog;
use skewer\build\Component\Section\Tree;
use skewer\build\libs\ft;
use yii\helpers\ArrayHelper;
use skewer\build\Component;


/**
 * Поисковый движок для каталожных коллекций
 * Умеет работать со множеством коллеций
 *
 * !При работе после инициализации требует вызова функции provideName() или setCard()
 */
class Search extends Prototype {

    const NAME_PREFIX = 'CollectionViewer_';

    /** @var string имя карточки */
    private $sCard = '';

    /**
     * отдает имя идентификатора ресурса для работы с поисковым индексом
     * @return string
     * @throws \Exception
     */
    public function getName() {
        if ( !$this->sCard )
            throw new \Exception( 'Card for Collection\Search is not set' );
        return self::NAME_PREFIX.$this->sCard;
    }

    /**
     * @inheritDoc
     */
    public function getModuleTitle() {

        $sTitle = \Yii::t('collections', 'tab_name');

        $coll = $this->getCollection();

        if ( !$coll )
            return $sTitle;

        $sTitle = sprintf( '%s (%s)', $sTitle, $coll->title );

        return $sTitle;
    }

    /**
     * Отдает сущность коллекции
     * @return Catalog\model\EntityRow|null
     */
    private function getCollection() {

        $c = Catalog\Card::getCollection( $this->sCard );
        return $c ? : null;

    }

    /**
     * Класс для сборки списка автивных поисковых движков
     * @param Component\Search\GetEngineEvent $event
     */
    public static function getSearchEngine( Component\Search\GetEngineEvent $event ) {

        foreach ( Catalog\Card::getCollections() as $c )
            $event->addSearchEngine( Search::className(), self::NAME_PREFIX.$c->name );

    }

    /**
     * Задает имя карточки для коллекции
     * @param $sCardName
     */
    public function setCard($sCardName) {
        $this->sCard = $sCardName;
    }

    /**
     * @inheritDoc
     */
    public function provideName($sName) {
        parent::provideName($sName);
        // вычисляем имя карточки
        $this->setCard( str_replace( self::NAME_PREFIX, '', $sName ) );
    }

    /**
     * @inheritdoc
     */
    protected function update( Row $oSearchRow ) {

        $oSearchRow->class_name = $this->getName();

        if (!$oSearchRow->object_id)
            return false;

        $coll = $this->getCollection();

        if ( !$coll )
            return false;

        // ищем раздел для коллекции
        $iSectionId = Catalog\Section::get4Collection( $coll->id );
        $oSearchRow->section_id = $iSectionId;

        // проверка существования раздела и реального url у него
        $oSection = Tree::getSection( $oSearchRow->section_id );
        if ( !$oSection || !$oSection->hasRealUrl() )
            return false;

        // ищем элемент коллекции
        $oTable = ft\Cache::getMagicTable( $coll->name );
        if ( !$oTable )
            return false;

        $obj = $oSearchRow->object_id ? $oTable->find( $oSearchRow->object_id ) : $oTable->getNewRow();
        $title = ArrayHelper::getValue( $obj, 'title', '' );
        $alias = ArrayHelper::getValue( $obj, 'alias', '' );
        $active = ArrayHelper::getValue( $obj, 'active', true );

        $sText = trim (
            $this->stripTags( ArrayHelper::getValue( $obj, 'info', '' ) ) . ' ' .
            $this->stripTags( $title ) . ' ' .
            $this->stripTags( $alias )
        );

        /** условия для удаления товара из поискового индекса */
        if ( !$active )
            return false;

        $oSearchRow->search_text = $this->stripTags( $sText );
        $oSearchRow->search_title = $this->stripTags( $title );

        $oSearchRow->href  = empty( $alias ) ?
            \Yii::$app->router->rewriteURL('['.$iSectionId.'][CatalogViewer?item='.$oSearchRow->object_id.']'):
            \Yii::$app->router->rewriteURL('['.$iSectionId.'][CatalogViewer?goods-alias='.$alias.']');

        $oSearchRow->status = 1;
        $oSearchRow->use_in_search = true;

        $oSearchRow->language = Component\Section\Parameters::getLanguage($oSearchRow->section_id);

        return $oSearchRow->save();

    }

    /**
     *  воссоздает полный список пустых записей для сущности, отдает количество добавленных
     * @return int
     */
    public function restore() {
        $sql = "INSERT INTO search_index(`status`,`class_name`,`object_id`) SELECT '0','".$this->getName()."',id  FROM cd_{$this->sCard}";
        Query::SQL($sql);
    }
}