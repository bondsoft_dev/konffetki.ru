<?php

$aLanguage = [];

$aLanguage['ru']['module_name'] = 'Коллекции';
$aLanguage['ru']['tab_name'] = 'Коллекции';

$aLanguage['ru']['create_coll'] = 'Создать коллекцию';
$aLanguage['ru']['new_coll'] = 'Создание новой коллекции';
$aLanguage['ru']['coll_name'] = 'Имя коллекции';
$aLanguage['ru']['goods_name'] = 'Имя товара';
$aLanguage['ru']['system_name'] = 'Системное имя';

$aLanguage['ru']['error_noname_seted'] = 'Не задано имя коллекции';
$aLanguage['ru']['error_title_not_found'] = 'Не заполнено поле "Название"';
$aLanguage['ru']['msg_remove_coll'] = 'Удалить {0}?';

$aLanguage['ru']['coll_list_for_cat'] = 'Список товарных коллекций для каталога';
$aLanguage['ru']['coll_panel_name'] = 'Коллекция товаров "{0}"';
$aLanguage['ru']['edit_elem_coll_panel_name'] = 'Редактор элемента коллекции "{0}"';
$aLanguage['ru']['goods_coll_list_panel'] = 'Товары элемента "{0}" коллекции "{1}"';
$aLanguage['ru']['dict_panel_name'] = 'Коллекция товаров: {0}';
$aLanguage['ru']['add'] = 'Добавить';
$aLanguage['ru']['back'] = 'Назад';
$aLanguage['ru']['goods'] = 'Товары';
$aLanguage['ru']['edit_title'] = 'Редактировать';

$aLanguage['ru']['field_title'] = 'Название';
$aLanguage['ru']['field_alias'] = 'Псевдоним';
$aLanguage['ru']['field_gallery'] = 'Галерея';
$aLanguage['ru']['field_info'] = 'Описание';
$aLanguage['ru']['field_tag_title'] = 'Заголовок страницы';
$aLanguage['ru']['field_keywords'] = 'Ключевые слова';
$aLanguage['ru']['field_description'] = 'Описание';
$aLanguage['ru']['field_sitemap_priority'] = 'Приоритет страницы';
$aLanguage['ru']['field_sitemap_frequency'] = 'Частота изменения товара';
$aLanguage['ru']['field_active'] = 'Активность';
$aLanguage['ru']['field_on_main'] = 'На главной';

$aLanguage['ru']['param_title'] = 'Название для главной';



$aLanguage['en']['module_name'] = 'Collections';
$aLanguage['en']['tab_name'] = 'Collections';

$aLanguage['en']['param_title'] = 'The name for the main';

$aLanguage['en']['create_coll'] = 'Create a Collection';
$aLanguage['en']['new_coll'] = 'Creating a new collection';
$aLanguage['en']['coll_name'] = 'Collection Name';
$aLanguage['en']['goods_name'] = 'Goods name';
$aLanguage['en']['system_name'] = 'System Name';

$aLanguage['en']['error_noname_seted'] = 'Name not provided';
$aLanguage['en']['error_title_not_found'] = 'Please enter "Title"';
$aLanguage['en']['msg_remove_coll'] = 'Delete {0}?';

$aLanguage['en']['coll_list_for_cat'] = 'Collection list';
$aLanguage['en']['coll_panel_name'] = 'Collection "{0}"';
$aLanguage['en']['edit_elem_coll_panel_name'] = 'Editor collection element "{0}"';
$aLanguage['en']['goods_coll_list_panel'] = 'Goods collection "{0}" element "{1}"';
$aLanguage['en']['dict_panel_name'] = 'Collection: {0}';
$aLanguage['en']['add'] = 'Add';
$aLanguage['en']['back'] = 'Back';
$aLanguage['en']['goods'] = 'Goods';
$aLanguage['en']['edit_title'] = 'Edit title';

$aLanguage['en']['field_title'] = 'Title';
$aLanguage['en']['field_alias'] = 'Alias';
$aLanguage['en']['field_gallery'] = 'Gallery';
$aLanguage['en']['field_info'] = 'Description';
$aLanguage['en']['field_tag_title'] = 'Page title';
$aLanguage['en']['field_keywords'] = 'Keywords';
$aLanguage['en']['field_description'] = 'Seo description';
$aLanguage['en']['field_sitemap_priority'] = 'Sitemap priority';
$aLanguage['en']['field_sitemap_frequency'] = 'Sitemap frequency';
$aLanguage['en']['field_active'] = 'Active';
$aLanguage['en']['field_on_main'] = 'On main';


return $aLanguage;