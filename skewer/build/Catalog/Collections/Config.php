<?php

namespace skewer\build\Catalog\Collections;

use skewer\build\Component;

$aConfig['name']     = 'Collections';
$aConfig['title']    = 'Каталог. Редактор коллекций товаров.';
$aConfig['version']  = '1.000a';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::CATALOG;
$aConfig['useNamespace'] = true;

$aConfig['events'][] = [
    'event' => Component\Search\Api::EVENT_GET_ENGINE,
    'class' => Search::className(),
    'method' => 'getSearchEngine',
];

return $aConfig;
