<?php

namespace skewer\build\Catalog\Settings;

use skewer\build\Catalog\LeftList\ModulePrototype;
use skewer\build\Component\UI;
use yii\base\UserException;


/**
 * Модуль настройки каталога
 */
class Module extends ModulePrototype {

    public function actionInit() {

        $goodsInclude = \SysVar::get('catalog.goods_include');
        $goodsAnalog = \SysVar::get('catalog.goods_analog');
        $bHidePriceFractional = \SysVar::get('catalog.hide_price_fractional');
        $iCountShowGoods = \SysVar::get('catalog.countShowGoods');
        $iParametricSearch = \SysVar::get('catalog.parametricSearch');
//        $iHide2LvlGoodsLinks = \SysVar::get('catalog.hide2lvlGoodsLinks');
//        $iHideBuy1LvlGoods = \SysVar::get('catalog.hideBuy1lvlGoods');
        $iShadowParamFilter = \SysVar::get('catalog.shadow_param_filter');

        $iGuestBookShow = \SysVar::get('catalog.guest_book_show');

        if ( ($goodsAnalogs = \SysVar::get('catalog.goods_analogs')) == null )
            $goodsAnalogs = 0;

        $form = UI\StateBuilder::newEdit();

        $form
            ->addField( 'goods_include', \Yii::t('catalog', 'goods_include'), 's', 'check' )
            ->addField( 'goods_analog', \Yii::t('catalog', 'goods_analog'), 's', 'check' )
            ->addField( 'goods_analogs', \Yii::t('catalog', 'goods_analogs'), 's', 'check' )
            ->addField( 'hide_price_fractional', \Yii::t('catalog', 'hide_price_fractional'), 's', 'check' )
            ->addField( 'countShowGoods', \Yii::t('catalog', 'countShowGoods'), 'i', 'int' )
            ->addField( 'parametric_search', \Yii::t('catalog', 'parametric_search'), 's', 'check' )
//            ->addField( 'hide2lvlGoodsLinks', \Yii::t('catalog', 'hide2lvlGoodsLinks'), 's', 'check' )
//            ->addField( 'hideBuy1lvlGoods', \Yii::t('catalog', 'hideBuy1lvlGoods'), 's', 'check' )
            ->addField( 'shadow_param_filter', \Yii::t('catalog', 'shadow_param_filter'), 's', 'check' )
            ->addField( 'guest_book_show', \Yii::t('catalog', 'guest_book_show'), 's', 'check' )
        ;

        $form->addButtonSave('save');

        $form->setValue([
            'goods_include' => $goodsInclude,
            'goods_analog' => $goodsAnalog,
            'goods_analogs' => $goodsAnalogs,
            'hide_price_fractional' => $bHidePriceFractional,
            'countShowGoods' => $iCountShowGoods,
            'parametric_search' => $iParametricSearch,
//            'hide2lvlGoodsLinks' => $iHide2LvlGoodsLinks,
//            'hideBuy1lvlGoods' => $iHideBuy1LvlGoods,
            'shadow_param_filter' => $iShadowParamFilter,
            'guest_book_show' => $iGuestBookShow,
        ]);

        $this->setInterface($form->getForm());
    }

    public function actionSave(){

        $countShowGoods = $this->getInDataValInt('countShowGoods');
        if ($countShowGoods <= 0){
            throw new UserException(\Yii::t('catalog', 'countShowGoodsError'));
        }

        \SysVar::set('catalog.goods_include',(int)$this->getInDataVal('goods_include'));
        \SysVar::set('catalog.goods_analog', (int)$this->getInDataVal('goods_analog'));
        \SysVar::set('catalog.goods_analogs', (int)$this->getInDataVal('goods_analogs'));
        \SysVar::set('catalog.hide_price_fractional', (int)$this->getInDataVal('hide_price_fractional'));
        \SysVar::set('catalog.parametricSearch', (int)$this->getInDataVal('parametric_search'));
//        \SysVar::set('catalog.hide2lvlGoodsLinks', (int)$this->getInDataVal('hide2lvlGoodsLinks'));
//        \SysVar::set('catalog.hideBuy1lvlGoods', (int)$this->getInDataVal('hideBuy1lvlGoods'));
        \SysVar::set('catalog.shadow_param_filter', (int)$this->getInDataVal('shadow_param_filter'));

        \SysVar::set('catalog.countShowGoods', $countShowGoods);
        \SysVar::set('catalog.guest_book_show', (int)$this->getInDataVal('guest_book_show'));

        $this->actionInit();
    }
}