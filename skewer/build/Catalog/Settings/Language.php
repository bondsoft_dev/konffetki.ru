<?php

$aLanguage = array();

$aLanguage['ru']['Settings.Catalog.tab_name'] = 'Настройки';
$aLanguage['ru']['goods_include'] = 'Комплекты товаров';
$aLanguage['ru']['goods_analog'] = 'Сопутствующие товары';
$aLanguage['ru']['goods_analogs'] = 'Модификации товаров';
$aLanguage['ru']['parametric_search'] = 'Параметрический поиск';
$aLanguage['ru']['hide_price_fractional'] = 'Скрыть копейки';
$aLanguage['ru']['countShowGoods'] = 'Количество товаров в списке (CMS)';
$aLanguage['ru']['countShowGoodsError'] = 'Количество выводимых записей должно быть больше нуля';
$aLanguage['ru']['hide2lvlGoodsLinks'] = 'Скрыть подробное описание товара 2 уровня';
$aLanguage['ru']['hideBuy1lvlGoods'] = 'Запрет покупки товара 1 уровня';
$aLanguage['ru']['shadow_param_filter'] = 'Раскраска недоступных значений полей фильтра';
$aLanguage['ru']['guest_book_show'] = 'Отзывы в товарах';

$aLanguage['en']['Settings.Catalog.tab_name'] = 'Settings';
$aLanguage['en']['goods_include'] = 'Include goods';
$aLanguage['en']['goods_analog'] = 'Related goods';
$aLanguage['en']['goods_analogs'] = 'Goods modifications';
$aLanguage['en']['parametric_search'] = 'Parametric search';
$aLanguage['en']['hide_price_fractional'] = 'Hide price fractional';
$aLanguage['en']['countShowGoods'] = 'Number of products on the list (CMS)';
$aLanguage['en']['countShowGoodsError'] = 'Number of output records must be greater than zero';
$aLanguage['en']['hide2lvlGoodsLinks'] = 'Hide detailed description of the product 2 level';
$aLanguage['en']['hideBuy1lvlGoods'] = 'The ban purchases of goods Level 1';
$aLanguage['en']['shadow_param_filter'] = 'Coloring inaccessible field values filter';
$aLanguage['en']['guest_book_show'] = 'Reviews in goods';

return $aLanguage;