<?php

namespace skewer\build\Catalog\Goods;

use skewer\build\Catalog\LeftList\ModulePrototype;
use skewer\build\Component\Catalog;
use skewer\build\Component\Section\Tree;
use skewer\build\libs\ft;
use skewer\build\Component\UI;
use skewer\build\Component\Installer;
use yii\base\UserException;
use yii\helpers\ArrayHelper;


/**
 * Каталог. Админка.
 * Class Module
 * @package skewer\build\Catalog\Goods
 */
class Module extends ModulePrototype {

    const categoryField = '__category';
    const mainLinkField = '__main_link';

    /** @var int число элементов на страницу */
    public $iOnPage = 30;

    /** @var int текущий номер страницы ( с 0, а приходит с 1 ) */
    public $iPage = 0;

    /** @var array Данные для фильтрации списка товаров, чтобы исключить выбор самих себя */
    public $aFilterData = array();

    /**
     * Получение идентификатора текущего раздела
     * @return int|mixed
     */
    protected function getCurrentSection() {
        $section = $this->getFilterSection();
        if ( !$section )
            $section = $this->getPageId();
        return $section;
    }


    /**
     * Возвращает имя карточки для раздела $section
     * @param int $section ID раздела
     * @return mixed
     */
    protected function getCard4Section( $section ) {
        return Catalog\Section::getDefCard( $section );
    }


    /**
     * Возвращает имя текущей каталожной карточки
     * @return string
     * @throws UserException
     */
    public function getCardName() {
        return $this->getCard4Section( $this->getCurrentSection() );
    }


    protected function preExecute() {
        $this->iPage = $this->getInt( 'page', $this->getInnerData( 'page', 0 ) );
        $this->setInnerData( 'page', $this->iPage );
    }


    /**
     * Получение значений полей фильтра
     * @param string[] $aFilterFields Список полей фильтра
     * @return array
     */
    protected function getFilterVal( $aFilterFields ) {

        $aFilter = array();

        foreach ( $aFilterFields  as $sField ) {
            $sName = 'filter_' . $sField;
            $sVal = $this->getStr( $sName, $this->getInnerData( $sName, '' ) );
            $this->setInnerData( $sName, $sVal );
            $aFilter[ $sField ] = $sVal;
        }

        return $aFilter;
    }


    /**
     * Возвращает значения поля раздел из фильтра
     * @return mixed
     */
    public function getFilterSection() {
        return ArrayHelper::getValue( $this->getFilterVal( ['section'] ), 'section', 0 );
    }


    /**
     * Набор выделенных позиций при мультивыделении в списке
     * @return array
     */
    protected function getMultipleData() {

        $aData = $this->get( 'data' );
        $aItems = [];

        if ( $this->getInDataVal( 'multiple' ) ) {

            if ( isset($aData['items']) && is_array($aData['items']) )
                foreach ( $aData['items'] as $aItem )
                    $aItems[] = ArrayHelper::getValue( $aItem, 'id', 0 );

        } else {

            if ( $id = $this->getInDataValInt( 'id' ) )
                $aItems = [$id];

        }

        return $aItems;
    }


    protected function actionInit() {

        $this->iOnPage = \SysVar::get('catalog.countShowGoods');
        if ( !$this->iOnPage ) $this->iOnPage = 30;

        $this->pageId = 0;

        $this->actionList();
    }


    /**
     * Интерфейс задания карточки для категории
     */
    public function actionSetCard() {

        $this->setPanelName( \Yii::t('catalog', 'goods_card_select') );

        $oForm = UI\StateBuilder::newEdit();
        $oForm
            ->addSelectField( 'card', \Yii::t('catalog', 'section_goods' ), 's', Catalog\Card::getGoodsCardList( true ) )
            ->setValue( ['card' => $this->getCardName()] )
            ->addButtonSave( 'saveCard' )
            ->addButtonCancel( 'list' )
        ;

        $this->setInterface( $oForm->getForm() );
    }


    /**
     * Состояние сохранения карточки для категории
     */
    public function actionSaveCard() {

        if ( !( $sCardName = $this->getInDataVal( 'card' ) ) ) {

            $this->addError( \Yii::t('catalog', 'error_no_card') );
            $this->actionSetCard();
        } else {

            Catalog\Section::setDefCard( $this->getCurrentSection(), $sCardName );

            if (isset($this->defCard))
                $this->defCard = $sCardName;

            $this->actionInit();
        }

    }


    protected function actionList() {

        $this->setInnerData( 'currentGoods', 0 );
        $this->setPanelName( \Yii::t('catalog', 'goods_list') );

        $model = model\SectionList::get( $this->getCurrentSection() )
            ->setFilter( $this->getFilterVal( ['article', 'title', 'price', 'active'] ) )
            ->limit( $this->iPage, $this->iOnPage )
        ;

        $this->setInterface( view\SectionList::get( $model, $this ) );
    }


    /**
     * Сортировка товарных позиций внутри раздела
     */
    protected function actionSort() {

        $aDropData = $this->get( 'dropData' );
        $sPosition = $this->get( 'position' );

        $iDropId = isSet( $aDropData['id'] ) ? $aDropData['id'] : false;
        $aItems = $this->getMultipleData();

        if( !count($aItems) || !$iDropId || !$sPosition )
            $this->addError('Ошибка! Неверно заданы параметры сортировки');

        if ( $sPosition == 'after' )
            $aItems = array_reverse($aItems);

        // todo научить сортировать в один проход
        foreach ( $aItems as $iSelectId )
            Catalog\model\SectionTable::sortSwap( $this->getCurrentSection(), $iSelectId, $iDropId, $sPosition );
    }


    /**
     * Установка товарной позиции на первое место в списке
     */
    protected function actionSetFirst() {

        $aData = $this->getInData();

        if( !isSet($aData['id']) || !$aData['id'] )
            $this->addError('Ошибка! Неверно заданы параметры сортировки');

        Catalog\model\SectionTable::sortUp( $this->getCurrentSection(), $aData['id'] );

        $this->actionList();
    }


    protected function actionEdit() {

        $this->setPanelName( \Yii::t('catalog', 'good_editor') );

        $model = model\GoodsEditor::get()
            ->setSectionData( $this->getCurrentSection(), $this->getCardName(), $this->getInnerData( 'currentGoods' ) )
            ->setData( $this->get( 'data' ), $this->get( 'from' ) )
            ->setUpdField( $this->get( 'field_name' ) )
        ;

        if ( $model->save() ) {

            $this->actionList();
        } else {

            if ( $error = $model->getErrorMsg() )
                $this->addError( $error );

            $this->setInnerData( 'currentGoods', $model->getGoodsRow()->getRowId() );
            $this->setInnerData( 'page', 0 );

            $this->setInterface( view\GoodsEditor::get( $model, $this ) );
        }

    }


    /**
     * Метод, меняющий на лету значения поля "Основной раздел" при
     * изменении поля "Категория"
     */
    protected function actionLoadSection() {

        // todo ref
        $aFormData = $this->get('formData', array());
        $mSectionList = isset($aFormData[self::categoryField]) ? $aFormData[self::categoryField] : '';
        $iSection = isset($aFormData[self::mainLinkField]) ? $aFormData[self::mainLinkField] : 0;

        $aSectionKeys = explode( ',', $mSectionList );
        $aSectionList = Tree::getSectionsTitle( $aSectionKeys );

        if ( count($aSectionKeys) && ( !$iSection || !in_array($iSection, $aSectionKeys) ) )
            $iSection = array_shift( $aSectionKeys );

        $oListVals = new \ExtFormRows();
        $oListVals->addFieldSelect(self::mainLinkField, $aSectionList, $iSection);

        $oListVals->setData( $this );

    }


    /**
     * Дублирование товара
     */
    protected function actionClone() {

        $model = model\GoodsEditor::get()
            ->setSectionData( $this->getCurrentSection(), '' )
            ->setData( $this->get( 'data' ), $this->get( 'from' ) )
        ;

        if ( $id = $model->saveNew() ) {
            $this->actionList();
        } else {

            if ( $error = $model->getErrorMsg() )
                $this->addError( $error );

        }

    }


    /**
     * Удалнение записи
     */
    public function actionDelete() {

        $model = model\GoodsEditor::get()
            ->setSectionData( $this->getCurrentSection(), '' )//
            ->setData( $this->get( 'data' ), $this->get( 'from' ) )
        ;

        if ( $model->multipleDelete() ) {

            $this->actionList();
        } else {

            if ( $error = $model->getErrorMsg() )
                $this->addError( $error );

        }

    }


    // -- RELATED --
    public function actionRelatedItems() {

        $this->setPanelName( \Yii::t('catalog', 'relatedItems_title') );

        $model = model\RelatedList::get( $this->getInnerData( 'currentGoods' ) );

        $this->setInterface( view\RelatedList::get( $model, $this ) );
    }


    public function actionAddRelatedItem() {

        $this->setPanelName( \Yii::t('catalog', 'relatedItems_add_title') );

        $model = model\FullCustomList::get( $this->getStr( 'filter_section', $this->getInnerData( 'filter_section', 0 ) ) )
            ->getWithoutRelated( $this->getInnerData( 'currentGoods' ) )
            ->setFilter( $this->getFilterVal( array('title', 'section') ) )
            ->limit( $this->iPage, $this->iOnPage )
        ;

        $this->setInterface( view\AddRelatedList::get( $model, $this ) );
    }


    public function actionLinkRelatedItem() {

        $data = $this->getInData();
        $list = [];

        if ( $this->getInDataVal('multiple') ) {

            $items = ArrayHelper::getValue( $data, 'items', [] );
            if ( $items )
                foreach ( $items as $item )
                    $list[] = isset( $item['id'] ) ? $item['id'] : 0;

        } else {
            if ( $id = $this->getInDataVal('id') )
                $list[] = $id;
        }

        if ( count( $list ) )
            foreach ( $list as $id )
                if ( $id ) // fixme получение карточки
                    Catalog\model\SemanticTable::link( Catalog\Semantic::TYPE_RELATED, $this->getInnerData( 'currentGoods' ), 1, $id, 1 );

        $this->actionRelatedItems();

    }


    public function actionRemoveRelatedItem() {

        $data = $this->getInData();
        $list = [];

        if ( $this->getInDataVal('multiple') ) {

            $items = ArrayHelper::getValue( $data, 'items', [] );
            if ( $items )
                foreach ( $items as $aItem )
                    $list[] = isset( $aItem['id'] ) ? $aItem['id'] : 0;

        } else {
            if ( $id = $this->getInDataVal('id') )
                $list[] = $id;
        }

        if ( count( $list ) )
            foreach ( $list as $id )
                if ( $id ) // fixme получение карточки
                    Catalog\model\SemanticTable::unlink( Catalog\Semantic::TYPE_RELATED, $this->getInnerData( 'currentGoods' ), 1, $id, 1 );

        $this->actionRelatedItems();

    }


    // -- INCLUDED --
    public function actionIncludedItems() {

        $this->setPanelName( \Yii::t('catalog', 'includedItems_title') );

        $model = model\IncludedList::get( $this->getInnerData( 'currentGoods' ) );

        $this->setInterface( view\IncludedList::get( $model, $this ) );
    }


    public function actionAddIncludedItem() {

        $this->setPanelName(\Yii::t('catalog', 'includedItems_add_title') );

        $model = model\FullCustomList::get( $this->getStr( 'filter_section', $this->getInnerData( 'filter_section', 0 ) ) )
            ->getWithoutIncluded( $this->getInnerData( 'currentGoods' ) )
            ->setFilter( $this->getFilterVal( array('title', 'section') ) )
            ->limit( $this->iPage, $this->iOnPage )
        ;

        $this->setInterface( view\AddIncludedList::get( $model, $this ) );
    }


    public function actionLinkIncludedItem() {

        $data = $this->getInData();
        $list = [];

        if ( $this->getInDataVal('multiple') ) {

            $items = ArrayHelper::getValue( $data, 'items', [] );
            if ( $items )
                foreach ( $items as $item )
                    $list[] = isset( $item['id'] ) ? $item['id'] : 0;

        } else {
            if ( $id = $this->getInDataVal('id') )
                $list[] = $id;
        }

        if ( count( $list ) )
            foreach ( $list as $id )
                if ( $id ) // fixme получение карточки
                    Catalog\model\SemanticTable::link( Catalog\Semantic::TYPE_INCLUDE, $this->getInnerData( 'currentGoods' ), 1, $id, 1 );

        $this->actionIncludedItems();

    }


    public function actionRemoveIncludedItem() {

        $data = $this->getInData();
        $list = [];

        if ( $this->getInDataVal('multiple') ) {

            $items = ArrayHelper::getValue( $data, 'items', [] );
            if ( $items )
                foreach ( $items as $aItem )
                    $list[] = isset( $aItem['id'] ) ? $aItem['id'] : 0;

        } else {
            if ( $id = $this->getInDataVal('id') )
                $list[] = $id;
        }

        if ( count( $list ) )
            foreach ( $list as $id )
                if ( $id ) // fixme получение карточки
                    Catalog\model\SemanticTable::unlink( Catalog\Semantic::TYPE_INCLUDE, $this->getInnerData( 'currentGoods' ), 1, $id, 1 );

        $this->actionIncludedItems();

    }


    // -- ANALOGS --
    public function actionAnalogItems() {

        // получаем идентификатор основного товара
        if ( !( $id = $this->getInnerData( 'currentGoods' ) ) ) {
            $aData = $this->get( 'data' );
            if ( isSet( $aData['id'] ) && $aData['id'] ) {
                $id = $aData['id'];
                $this->setInnerData( 'currentGoods', $id );
            }

        }

        $model = model\ModificList::get( $id )
            //->limit( $this->iPage, $this->iOnPage )
        ;

        $data = $model->getCurrentObject()->getData();
        $this->setPanelName( \Yii::t('catalog', 'analogItems_title') . ' "' . $data['title'] . '"' );

        $this->setInterface( view\ModificList::get( $model, $this ) );
    }


    public function actionEditAnalogItem() {

        $model = model\ModGoodsEditor::get( $this->getInnerData( 'currentGoods' ) )
            ->setData( $this->get( 'data' ), $this->get( 'from' ) )
            ->setUpdField( $this->get( 'field_name' ) )
        ;

        $data = $model->getMainObject()->getData();
        $this->setPanelName( \Yii::t('catalog', 'analogItems_edit_title') . ' "' . $data['title'] . '"' );

        if ( $model->save() ) {

            $this->actionAnalogItems();
        } else {

            if ( $error = $model->getErrorMsg() )
                $this->addError( $error );

            $this->setInterface( view\ModGoodsEditor::get( $model, $this ) );
        }

    }


    public function actionDeleteAnalogItem() {

        $model = model\ModGoodsEditor::get( $this->getInnerData( 'currentGoods' ) )
            ->setData( $this->get( 'data' ), $this->get( 'from' ) )
        ;

        if ( $model->multipleDelete() ) {

            $this->actionAnalogItems();
        } else {

            if ( $error = $model->getErrorMsg() )
                $this->addError( $error );

        }
    }


}
