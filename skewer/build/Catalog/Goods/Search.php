<?php


namespace skewer\build\Catalog\Goods;

use skewer\build\Component\orm\Query;
use skewer\build\Component\Search\Prototype;
use skewer\build\Component\Search\Row;
use skewer\build\Component\Catalog\Card;
use skewer\build\Component\Catalog\Section;
use skewer\build\Component\Section\Parameters;
use skewer\build\Component\Section\Tree;

class Search extends Prototype{
    /**
     * отдает имя идентификатора ресурса для работы с поисковым индексом
     * @return string
     */
    public function getName() {
        return "CatalogViewer";
    }

    /**
     * @inheritdoc
     */
    protected function update(Row $oSearchRow) {
        $oSearchRow->class_name = $this->getName();

        if (!$oSearchRow->object_id)
            return false;

        // todo здесь нузно знать с какой базовой карточкой мы работаем
        // если появится множество базовых карточек, то можно применить подход с
        //      вычислением базовой карточки как в коллекциях
        $iBaseCard = 1;

        $oGoods = Card::getItemRow( $iBaseCard, ['id' => $oSearchRow->object_id] );

        $iSectionId = Section::getMain4Goods( $oSearchRow->object_id );

        if (!$iSectionId)
            return false;

        $oSearchRow->section_id = $iSectionId;

        // проверка существования раздела и реального url у него
        $oSection = Tree::getSection( $oSearchRow->section_id );
        if ( !$oSection || !$oSection->hasRealUrl() )
            return false;

        // todo пересмотреть формирование текста - должно быть по галочке
        $sText = trim (
            $this->stripTags(isSet($oGoods->announce) ? $oGoods->announce : '')
            . $this->stripTags(isSet($oGoods->obj_description) ? ' ' . $oGoods->obj_description : '')
            . (isSet($oGoods->title) ? ' ' . $oGoods->title : '')
            . (isSet($oGoods->article) ? ' ' . $oGoods->article : '')
        );

        /** условия для удаления товара из поискового индекса */
        if ( !isSet($oGoods->active) || !$oGoods->active )
            return false;

        /**
         * @todo добавляться должны несколько записей ???
         */
        $oSearchRow->language = Parameters::getLanguage($iSectionId);

        $oSearchRow->search_text = $this->stripTags($sText);
        $oSearchRow->search_title = $this->stripTags($oGoods->title);

        $oSearchRow->href  = empty( $oGoods->alias ) ?
            \Yii::$app->router->rewriteURL('['.$iSectionId.'][CatalogViewer?item='.$oSearchRow->object_id.']'):
            \Yii::$app->router->rewriteURL('['.$iSectionId.'][CatalogViewer?goods-alias='.$oGoods->alias.']');

        $oSearchRow->status = 1;
        $oSearchRow->use_in_search = true;

        return $oSearchRow->save();

    }

    /**
     *  воссоздает полный список пустых записей для сущности, отдает количество добавленных
     * @return int
     */
    public function restore() {
        $sql = "INSERT INTO search_index(`status`,`class_name`,`object_id`)  SELECT '0','{$this->getName()}',base_id  FROM c_goods WHERE `parent`=`base_id`";
        Query::SQL($sql);
    }

} 