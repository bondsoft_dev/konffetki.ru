<?php

namespace skewer\build\Catalog\Goods\view;

use skewer\build\Component\Catalog;
use skewer\build\Component\Installer;


/**
 * Построитель списка товарных позиций для раздела
 * Class SectionList
 * @package skewer\build\Catalog\Goods\view
 * todo стандартные функции перенести в прототип
 */
class SectionList extends ListPrototype {


    protected function build() {

        // собираем фильтр

        if ( !$this->module->getPageId() )
            $this->addCustomFilter(
                'section',
                \Yii::t('catalog', 'section'),
                $this->model->getSection(),
                'SELECT',
                Catalog\Section::getList()
            );

        $this
            ->addFilter( 'article', $this->model->getFilter( 'article' ) )
            ->addFilter( 'title', $this->model->getFilter( 'title' ) )
            ->addFilter( 'price', $this->model->getFilter( 'price' ) )
            ->addFilter( 'active', $this->model->getFilter( 'active' ), 'SELECT', [
                    1 => \Yii::t('catalog', 'yes'),
                    2 => \Yii::t('catalog', 'no')
            ])
        ;

        // собираем набор полей для таблицы
        $this
            ->addField( 'id', 'string', array( 'flex' => 1 ) )
            ->addField( 'article', 'string', array( 'flex' => 3 ) )
            ->addField( 'title', 'string', array( 'flex' => 3 ) )
            ->addField( 'price', 'money', array( 'flex' => 1 ) )
            ->addField( 'buy', 'check', array( 'flex' => 1 ) )
            ->addField( 'fastbuy', 'check', array( 'flex' => 1 ) )
            ->addField( 'active', 'check', array( 'flex' => 1 ) )
            ->addField( 'on_main', 'check', array( 'flex' => 1 ) )
            ->addField( 'hit', 'check', array( 'flex' => 1 ) )
            ->addField( 'new', 'check', array( 'flex' => 1 ) )
            ->addField( 'discount', 'check', array( 'flex' => 1 ) )
            ->addYandexExportField( 'in_yandex' )
        ;

        // устанавливаем редактируемые поля
        $this->setEditableFields( $this->model->getEditableFields( ['in_yandex'] ), 'edit' );

        // разрешаем сортировку в разделах
        if ( $this->isSection() )
            $this->setSorting( 'sort' );

        // Вывод галочек для множественный операций
        $this->form->showCheckboxSelection();


        // элементы управления
        $this
            ->btnAddGoods()
            ->btnTools()
            ->btnDelGoods()

            ->btnRowModGoods()
            ->btnRowEdit( 'edit' )
            ->btnRowSetFirst()
            ->btnRowClone()
        ;

    }


    /**
     * Кнопка клонировать
     * @param string $state
     * @return $this
     */
    protected function btnRowClone( $state = 'clone' ) {

        if ( $this->isSection() )
            $this->form->addRowButton( \Yii::t('adm','clone'), 'icon-clone', $state, $state );

        return $this;
    }


    /**
     * Кнопка сортировки: товар переносится в начало списка
     * @param string $state
     * @return $this
     */
    protected function btnRowSetFirst( $state = 'setFirst' ) {

        if ( $this->isSection() )
            $this->form->addRowButton( \Yii::t('adm','set_first'), 'icon-upgrade', $state, $state );

        return $this;
    }


    /**
     * Кнопка перехода к списку модификаций
     * @return $this
     */
    protected function btnRowModGoods() {

        if ( \SysVar::get( 'catalog.goods_analogs' ) ) {

            $this->form->addRowCustomBtn(
                'ViewAnalogBtn',
                \Layer::CATALOG,
                'Goods',
                array(
                    'tooltip' => \Yii::t('catalog', 'analogItems')
                ) );
        }

        return $this;
    }


    /**
     * Кнопка добавления товара
     * @return $this
     */
    protected function btnAddGoods() {

        if ( $this->isSection() && $this->isCard() )
            $this->form->addButton( \Yii::t('adm','add'), 'edit', 'icon-add' );

        return $this;
    }


    /**
     * Кнопка удаления товаров
     * @return $this
     */
    private function btnDelGoods() {
        $this->form->addBtnSeparator();
        $this->form->addButtonDeleteMultiple();
        return $this;
    }


    /**
     * Кнопка перехода в настройки
     * @return $this
     */
    protected function btnTools() {

        // кнопка установки карточки для добавления товара
        if (  $this->getLayer() == 'Adm' ) {

            $this->form->addButton( \Yii::t('catalog', 'btn_settings'), 'settings', 'icon-edit' );
        } else {

            if ( $this->isSection() )
                $this->form->addExtButton(\ExtDocked::create(\Yii::t('catalog', 'card'))
                    ->setIconCls(\ExtDocked::iconEdit)
                    ->setState('setCard')
                    ->setAction('setCard')
                    ->unsetDirtyChecker()
                );
        }


        return $this;
    }


    /**
     * Добавление поля YandexExport
     * @param $sField
     * @return $this
     */
    protected function addYandexExportField( $sField ) {

        $installer = new Installer\Api();

        $oField = $this->model->getField( $sField );

        $this->form->addFieldIf(
            $sField,
            $oField ? $oField->getTitle() : '',
            'i', 'check',
            array( 'listColumns' => array( 'flex' => 1 ) ),
            $installer->isInstalled( "YandexExport", \Layer::TOOL ) && $oField && $oField->getAttr('active')
        );

        return $this;
    }


    /**
     * Факт нахождения в разделе
     * @return bool
     */
    private function isSection() {
        return $this->module->getPageId() || $this->model->getSection();
    }


    /**
     * Факт наличия привязанной карточки к разделу
     * @return bool
     */
    private function isCard() {
        $card = $this->module->getCardName();
        return (bool)$card;
    }

    /**
     * Имя слоя вывода
     * @return mixed
     */
    private function getLayer() {
        return $this->module->getLayerName();
    }

}