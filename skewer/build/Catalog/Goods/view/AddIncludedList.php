<?php

namespace skewer\build\Catalog\Goods\view;


use skewer\build\Component\Catalog\Section;


/**
 * Построение интерфейса списка товаров для добавления в комплект
 * Class AddIncludedList
 * @package skewer\build\Catalog\Goods\view
 */
class AddIncludedList extends ListPrototype {

    protected function build() {

        $this->form
            // добавляем фильтр
            ->addFilterText( 'filter_title', $this->model->getFilter( 'title' ), \Yii::t( 'catalog', 'goods_title' ) )
            ->addFilterSelect( 'filter_section', Section::getList(), (int)$this->model->getFilter( 'section' ), \Yii::t('catalog', 'section') )
            ->addFilterAction( 'AddIncludedItem' )

            // добавляем поля
            ->field( 'id', 'id', 'i', 'hide', ['listColumns.width' => 40] )
            ->field( 'title', \Yii::t('catalog', 'goods_title'), 's', 'string', ['listColumns.flex' => 3] )
            ->field( 'price', \Yii::t('catalog', 'goods_price'), 's', 'string', ['listColumns.flex' => 1] )
            
            // элементы управления
            ->addButtonAddMultiple( 'linkIncludedItem' )
            ->button( \Yii::t('adm', 'back'), 'IncludedItems', 'icon-cancel' )
            ->addRowButtonAdd( 'linkIncludedItem', 'edit_form' )

            // Вывод галочек для множественный операций
            ->showCheckboxSelection()
        ;

    }

}