<?php

namespace skewer\build\Catalog\Goods\view;


/**
 * Построитель списка товаров в комплекте
 * Class IncludedList
 * @package skewer\build\Catalog\Goods\view
 */
class IncludedList extends ListPrototype {

    protected function build() {

        // добавляем поля
        $this
            ->addField( 'id', 'id', ['width' => 40] )
            ->addField( 'title', \Yii::t('catalog', 'goods_title'), ['flex' => 3] )
            ->addField( 'price', \Yii::t('catalog', 'goods_price'), ['flex' => 1] )
        ;

        // элементы управления
        $this->form
            ->button( \Yii::t('adm', 'add'), 'AddIncludedItem', 'icon-add' )
            ->button( \Yii::t('adm', 'back'), 'Edit', 'icon-cancel' )
            ->buttonSeparator()
            ->addButtonDeleteMultiple( 'removeIncludedItem' )
        ;

        // Вывод галочек для множественный операций
        $this->form->showCheckboxSelection();

    }
}