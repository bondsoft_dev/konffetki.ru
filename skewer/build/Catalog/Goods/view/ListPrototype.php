<?php

namespace skewer\build\Catalog\Goods\view;


use skewer\build\Component\UI;


/**
 * Прототип построителя интерфейса списка товарных позиций
 * Class ListPrototype
 * @package skewer\build\Catalog\Goods\view
 */
abstract class ListPrototype {

    /** @var UI\StateBuilder $form */
    protected $form = null;

    /** @var \skewer\build\Catalog\Goods\model\ListPrototype $model */
    protected $model = null;

    /** @var \skewer\build\Catalog\Goods\Module Модуль в котором осуществляется вывод интерфейса */
    protected $module = null;


    private $aData = array();


    /**
     * Основная функция построение в модуле $oModule интерфейса по модели $oModel
     * @param $oModel
     * @param $oModule
     * @return mixed
     */
    public static function get( $oModel, $oModule ) {
        $obj = new static();
        $obj->model = $oModel;
        $obj->module = $oModule;
        $obj->form = UI\StateBuilder::newList();
        $obj->build();
        return $obj->show();
    }

    
    abstract protected function build();


    /**
     * Установка фильтра по полю для списка
     * @param string $sField Имя поля для фильтрации
     * @param string $mDefValue Значение поля из фильтра
     * @param string $sType Тип вывода фильтра (TEXT|SELECT)
     * @param array $mData Данные для построения фильтра
     * @return $this
     * @throws \Exception
     */
    protected function addFilter( $sField, $mDefValue = '', $sType = 'TEXT', $mData = array() ) {

        if ( !$this->form )
            throw new \Exception( 'ListView: StateBuilder not found!' );

        $oField = $this->model->getField( $sField );

        switch ( $sType ) {
            case 'TEXT':
                $this->form->addFilterTextIf(
                    $oField && $oField->getAttr('active'),
                    'filter_'.$sField, $mDefValue, \Yii::t( 'catalog', 'filter_'.$sField )
                );
                break;
            case 'SELECT':
                $this->form->addFilterSelectIf(
                    $oField && $oField->getAttr('active'),
                    'filter_'.$sField, $mData, $mDefValue, \Yii::t( 'catalog', 'filter_'.$sField )
                );
                break;
        }


        return $this;
    }


    protected function addCustomFilter( $sField, $sTitle = '', $mDefValue = '', $sType = 'TEXT', $mData = array() ) {

        if ( !$this->form )
            throw new \Exception( 'ListView: StateBuilder not found!' );

        switch ( $sType ) {
            case 'TEXT':
                $this->form->addFilterText( 'filter_'.$sField, $mDefValue, $sTitle );
                break;
            case 'SELECT':
                $this->form->addFilterSelect( 'filter_'.$sField, $mData, $mDefValue, $sTitle );
                break;
        }

        return $this;
    }


    /**
     * Добавление поля в вывод
     * @param string $sField Имя поля
     * @param string $sType Тип вывода
     * @param array $addData Дополнительные параметры
     * @return $this
     */
    protected function addField( $sField, $sType, $addData = array() ) {

        $oField = $this->model->getField( $sField );

        // todo уточнить правило
        $sDataType = ($oField && $oField->getDatatype() == 'int') ? 'i' : 's';

        $this->form->addFieldIf(
            $sField,
            $oField ? $oField->getTitle() : '',
            $sDataType, $sType,
            array( 'listColumns' => $addData ),
            $oField && $oField->getAttr('active')
        );

        return $this;
    }


    /**
     * Установка списка редактируемых полей
     * @param array $aFieldList Список полей
     * @param string $state Состояние в котором происходит сохранение
     * @return $this
     */
    protected function setEditableFields( $aFieldList, $state = 'update' ) {

        $this->aData['EditableFields'] = array(
            'fields' => $aFieldList,
            'state' => $state
        );

        return $this;
    }


    /**
     * Включение сортировки в списке
     * @param string $state Состояние в котором выполняется сортировка
     * @return $this
     */
    protected function setSorting( $state = 'sort' ) {
        $this->form->enableDragAndDrop( $state );
        return $this;
    }


    /**
     * Добавление для каждой записи кнопки типа редактирования
     * @param string $state Состояние обработки действия
     * @return $this
     */
    protected function btnRowEdit( $state = 'edit' ) {
        $this->form->addRowButtonUpdate( $state );
        return $this;
    }


    /**
     * Добавление для каждой записи кнопки типа удалить
     * @param string $state Состояние обработки действия
     * @return $this
     */
    protected function btnRowDelete( $state = 'delete' ) {
        $this->form->addRowButtonDelete( $state );
        return $this;
    }


    /**
     * Функция сборки интерфейса
     * @return \ExtForm|\ExtList
     * @throws \Exception
     */
    protected function show() {

        $this->form->setValue(
            $this->model->getData(),
            $this->model->getOnPage(),
            $this->model->getPage(),
            $this->model->getTotalItems()
        );

        if ( isSet( $this->aData['EditableFields'] ) )
            $this->form->setEditableFields(
                $this->aData['EditableFields']['fields'],
                $this->aData['EditableFields']['state']
            );


        return $this->form->getForm();
    }

}