<?php

namespace skewer\build\Catalog\Goods\view;

use skewer\build\Component\Catalog;
use skewer\build\libs\ft;


/**
 * Построение интерфейса редактирование модификации товара
 * Class ModGoodsEditor
 * @package skewer\build\Catalog\Goods\view
 */
class ModGoodsEditor extends GoodsEditor {

    protected $aUniqFields = array( 'id', 'alias', 'active' );

    protected function build() {

        $aData = $this->model->getData();

        /** @var Catalog\GoodsRow $oGoodsRow */
        $oGoodsRow = $this->model->getGoodsRow();

        // обработка полей для вывода по типам
        $aFields = $oGoodsRow->getFields();
        foreach( $aFields as $oField ) {

            if ( $oField->getEditorName() == ft\Editor::WYSWYG )
                $aData[$oField->getName()] = \ImageResize::restoreTags( $aData[$oField->getName()] );

        }

        $this->form = new \ExtForm();

        // кнопки
        $this->form->addBtnSave( 'EditAnalogItem' );
        $this->form->addBtnCancel( 'AnalogItems' );

        if ( $this->model->getGoodsRow()->getRowId() ) {

            $this->form->addBtnSeparator();
            $this->form->addBtnDelete( 'DeleteAnalogItem' );
        }

        $this->form->useSpecSectionForImages();

        // задание набора полей для интерфейса
        $this->form->setFieldsByUiForm( $this->initForm( array( 'active' ), $this->aUniqFields, true ) );

        // контент
        $this->form->setValues( $aData );
    }
}