<?php

namespace skewer\build\Catalog\Goods\view;

use skewer\build\Component\Catalog;
use skewer\build\Component\Gallery\Format;
use skewer\build\Component\Section\Tree;
use skewer\build\Component\UI;
use skewer\build\libs\ExtBuilder\Field\Show;
use skewer\build\libs\ft;


/**
 * Построение интерфейса редактирование товара
 * Class GoodsEditor
 * @package skewer\build\Catalog\Goods\view
 * todo рефакторинг после создания построителя интерфейсов
 */
class GoodsEditor extends FormPrototype {

    const categoryField = '__category';
    const mainLinkField = '__main_link';

    const seoGroupField = '__seo_catalog_group';

    const seoGroup = 'seo';

    /** @var array SEO поля */
    protected $aSeoFields = [];

    /** @var int SEO группа */
    protected $iSeoGroup = 0;

    /** @var string Заголовок SEO группы */
    protected $sSeoGroupTitle = '';

    protected function build() {

        /** @var Catalog\GoodsRow $oGoodsRow */
        $oGoodsRow = $this->model->getGoodsRow();

        // обработка полей для вывода по типам
        $aFields = $oGoodsRow->getFields();
        $aData =  $oGoodsRow->getData();
        foreach( $aFields as $oField ) {

            if ( $oField->getEditorName() == ft\Editor::WYSWYG )
                $aData[$oField->getName()] = \ImageResize::restoreTags( $aData[$oField->getName()] );

        }
        $oGoodsRow->setData( $aData );

        $this->form = new \ExtForm();

        // кнопки
        $this->form->addBtnSave( 'edit', 'edit' );
        $this->form->addBtnCancel();

        if ( $oGoodsRow->getRowId() ) {

            $this->form->addBtnSeparator();
            $this->btnShowRelatedItems();
            $this->btnShowIncludedItems();
            $this->btnShowAnalogItems();

            if ( \SysVar::get('catalog.goods_include') || \SysVar::get('catalog.goods_analog') )
                $this->form->addBtnSeparator();

            $this->form->addBtnDelete();
        }

        $this->form->useSpecSectionForImages();

        // задание набора полей для интерфейса
        $this->form->setFieldsByUiForm( $this->initForm() );

        // контент
        $aData = $oGoodsRow->getData();

        //seo
        $this->addSeoField( $aData );

// todo приведение типов
        foreach ( $oGoodsRow->getFields() as $oFtField ) {
            if ( isSet( $aData[ $oFtField->getName() ] ) && $oFtField->getDatatype() == 'int' )
                $aData[ $oFtField->getName() ] = (int)$aData[ $oFtField->getName() ];
            if ( isSet( $aData[ $oFtField->getName() ] ) && $oFtField->getDatatype() == 'decimal(12,2)' )
                $aData[ $oFtField->getName() ] = (float)$aData[ $oFtField->getName() ];
        }

        $this->form->setValues( $aData );

    }

    private function addSeoField( $aData = [] ){

        if ( !$this->form instanceof \ExtForm  || !count($this->aSeoFields))
            return;

        $aSeoFields = [];
        /** @var UI\Form\Field $oItem */
        foreach($this->aSeoFields as $oItem){

            $oItem->setGroupTitle('');
            $oItem->setValue((isset($aData[$oItem->getName()])) ? $aData[$oItem->getName()] : '');
            $aParams = array_merge( array(
                'name' => $oItem->getName(),
                'title' => $oItem->getTitle(),
                'view' => $oItem->getEditor()
            ), $oItem->getOutParamList() );

            // создаем объект из массивов
            $oIfaceField = \ExtFT::createFieldByUi( $aParams, $oItem );

            $aItem = \ExtForm::getFieldDesc( $oIfaceField );

            $aSeoFields[] = $aItem;

        }

        $oField = new Show();
        $oField->setName(static::seoGroupField);
        $oField->setTitle( $this->sSeoGroupTitle );
        
        $oField->setAddDesc([
                'group_name' => $this->iSeoGroup,
                'collapsed' => true,
                'customField' => 'SEOFields',
                'layer' => 'Adm',
                'fieldConfig' => $aSeoFields
                ]
            );

        $this->form->addField( $oField );
        $this->module->addLibClass( 'SEOFields', 'Adm', 'SEO' );
    }


    /**
     * Создание формы по модели
     * @param array $aAttr
     * @param array $aFields
     * @param bool $bDisableUniq
     * @return UI\Form
     * @throws \Exception
     * todo переписать всю цепочку построения формы
     */
    protected function initForm( $aAttr = array('active'), $aFields = array( 'id' ), $bDisableUniq = false ) {

        /** @var \skewer\build\Component\Catalog\GoodsRow $oGoodsRow */
        $oGoodsRow = $this->model->getGoodsRow();

        // сущность расширенной карточки
        $sExtCardName = $oGoodsRow->getExtCardName();
        $oExtEntity = Catalog\model\EntityTable::getByName( $sExtCardName );
        if ( !$oExtEntity )
            throw new \Exception( "Не найдена сущность с именем '$sExtCardName'" );

        // наборы групп
//        $aBaseGroupList = Catalog\Card::getGroupList( $oExtEntity->parent );
//        $aExtGroupList = Catalog\Card::getGroupList( $oExtEntity->id );
//        $aGroupList = $aBaseGroupList + $aExtGroupList;
        $aGroupList = $oExtEntity->getGroupList( true );

        $oForm = new UI\Form();

        $aResult = array();

        foreach ( $oGoodsRow->getFields() as $oFtField ) {

            // интерфейсное отображение поля
            $oUiField = $this->makeFormFieldByFtModel( $oFtField );

            // группа
            $iGroup = (int)$oFtField->getParameter( '__group_id' );

            if ( isset($aGroupList[$iGroup]) )
                $oUiField->setGroupTitle( $aGroupList[$iGroup] );

            // добавить в вывод, если установлены требуемые атрибуты
            $bAddFlag = true;
            if ( count( $aAttr ) ) {
                foreach ( $aAttr as $sAttrName ) {
                    $mAttrValue = $oFtField->getAttr( $sAttrName );
                    if ( !$mAttrValue ) {
                        $bAddFlag = false;
                    }
                }
            }

            if ( $bAddFlag || in_array( $oFtField->getName(), $aFields ) ) {

                if ( $bDisableUniq && !in_array( $oFtField->getName(), $aFields ) && !$oFtField->getAttr( 'is_uniq' ) )
                    $oUiField->setOutParam( 'disabled', true );

                $aResult[$iGroup][] =  $oUiField;
            }
        }

        /** Поиск SEO полей */
        $iSeoGroup = Catalog\Card::getGroupByName( 1, static::seoGroup );
        if ($iSeoGroup && isset($aResult[$iSeoGroup->id])){
            $this->iSeoGroup = $iSeoGroup->id;
            $this->aSeoFields = $aResult[$this->iSeoGroup];
            $this->sSeoGroupTitle = $aGroupList[$this->iSeoGroup];
            unset($aResult[$this->iSeoGroup]);
        }

        /** Прикрепление полей к форме */
        foreach($aResult as $aFieldList){
            foreach($aFieldList as $item){
                $oForm->addField( $item );
            }
        }

        // errors
        $aError = $oGoodsRow->getErrorList();
        $bHasError = (bool)$aError;
//        $bExistingRow = $oGoodsRow->getRowId() or !$bHasError;
//        // идентификатор записи
//        if ( $bExistingRow and !$iItemId )
//            $this->riseError( \Yii::t('catalog', 'error_no_pk_set' ) );
//
//        // данные записи
//        $aData = $oGoodsRow->getVars();
//        if ( $bExistingRow and !$aData )
//            $this->riseError( \Yii::t('catalog', 'error_row_not_found', $sBasePKey, $iItemId ) );

        // Если поля были отредактированы - заменяем
        if ( $bHasError ) {
// todo разобраться с этой заменой v
//            foreach ( $aInData as $sFieldName => $sFieldValue )
//                $aData[$sFieldName] = $sFieldValue;

            foreach ( $aError as $sFieldName => $sErrorText ) {
                if ( $oField = $oForm->getField( $sFieldName ) )
                    $oField->setError($sErrorText);
            }
        }

        // spec fields // ( $this->module->getLayerName() !== 'Adm' ) &&
        if ( $oGoodsRow->isMainRow() && !$bDisableUniq )
            $this->addSpecialFields( $oForm );

        return $oForm;
    }

    /**
     * Отдает построенное поле формпеределать
     * @param ft\model\Field $oFtField
     * @return UI\Form\Field
     */
    private function makeFormFieldByFtModel( ft\model\Field $oFtField ) {

        // todo #link пересмотреть подход после внедрения ajax выборки для полей

        // если есть подчиненная сущность у поля

        $oRelationList = $oFtField->getRelationList();

        if ( count($oRelationList) )
            $oRelation = $oRelationList[0];
        else
            $oRelation = null;

        if ($oFtField->getEditorName() == ft\Editor::GALLERY){
            $oFtField->setEditor( ft\Editor::GALLERY, ['gallery_format' => Format::TYPE_CATALOG] );
        }

        if ( $oRelation and (in_array($oFtField->getEditorName(), [ft\Editor::SELECT,ft\Editor::MULTISELECT,ft\Editor::COLLECTION])) ) {

            /** @var UI\Form\Select|UI\Form\MultiCheck $oField */
            if ( $oFtField->getEditorName() == ft\Editor::MULTISELECT ) {
                $oField = UI\Form\MultiCheck::makeByFt( $oFtField );
                $list = [];
            } else {
                $oField = UI\Form\Select::makeByFt( $oFtField );
                $list = ['---'];
            }

            $oModel = ft\Cache::get( $oRelation->getEntityName() );
            $oQuery = ft\Cache::getMagicTable( $oRelation->getEntityName() );

            $sValName = $oRelation->getExternalFieldName();
            $sTitleField = $sValName;
            if ( $oModel->hasField('title') )
                $sTitleField = 'title';
            elseif ( $oModel->hasField('name') )
                $sTitleField = 'name';

            $aItems = $oQuery->find()->getAll();
            foreach ( $aItems as $oItem )
                $list[ $oItem->$sValName ] = $oItem->$sTitleField;

            $oField->setItems( $list );

            return $oField;

        } else {
            return UI\Form\Field::makeByFt( $oFtField );
        }

    }


    private function addSpecialFields( UI\Form $oForm ) {

        $iItemId = $this->model->getGoodsRow()->getRowId();

        $aCatSection = Catalog\Section::getList();
        $aGoodsSection = $this->model->getGoodsRow()->getViewSection();

        // текущий раздел для нового товара
        if ( !$iItemId && !$aGoodsSection )
            $aGoodsSection = array( $this->module->getPageId() );

        $aGoodsSectionWithTitle = Tree::getSectionsTitle( $aGoodsSection ?: [] );
        $iMainSectionId = $this->model->getGoodsRow()->getMainSection();


        // настройки
        /* основной раздел */
        $oSectionLinkField = new UI\Form\Select(
            self::mainLinkField,
            \Yii::t('catalog', 'main_section')
        );
        $oSectionLinkField->setGroupTitle(\Yii::t('catalog', 'settings'));
        $oSectionLinkField->setItems( $aGoodsSectionWithTitle );
        $oSectionLinkField->setValue( $iMainSectionId );
        $oForm->prependField($oSectionLinkField);

        /* категория */

        $oCategoryField = new UI\Form\MultiCheck(
            self::categoryField,
            \Yii::t('catalog', 'category')
        );
        $oCategoryField->setGroupTitle(\Yii::t('catalog', 'settings'));
        $oCategoryField->setItems( $aCatSection );
        $oCategoryField->setValue( $aGoodsSection );
        $oCategoryField->setOnUpdAction( 'loadSection' );
        $oForm->prependField($oCategoryField);

    }


    protected function show() {
        return $this->form;
    }


    private function btnShowRelatedItems() {
        if ( \SysVar::get('catalog.goods_analog') )
            $this->form->addExtButton(\ExtDocked::create(\Yii::t('catalog', 'relatedItems'))
                ->setIconCls(\ExtDocked::iconEdit)
                ->setState('RelatedItems')
                ->setAction('RelatedItems')
                ->unsetDirtyChecker()
            );
    }

    private function btnShowIncludedItems() {
        if ( \SysVar::get('catalog.goods_include') )
            $this->form->addExtButton(\ExtDocked::create(\Yii::t('catalog', 'includedItems'))
                ->setIconCls(\ExtDocked::iconEdit)
                ->setState('IncludedItems')
                ->setAction('IncludedItems')
                ->unsetDirtyChecker()
            );
    }

    private function btnShowAnalogItems() {
        if ( \SysVar::get('catalog.goods_analogs') )
            $this->form->addExtButton(\ExtDocked::create(\Yii::t('catalog', 'analogItems'))
                ->setIconCls(\ExtDocked::iconEdit)
                ->setState('AnalogItems')
                ->setAction('AnalogItems')
                ->unsetDirtyChecker()
            );
    }

}