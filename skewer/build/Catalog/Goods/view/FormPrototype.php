<?php

namespace skewer\build\Catalog\Goods\view;

use skewer\build\Component\UI;


/**
 * Прототип построителя интерфейса редактирования товара
 * Class ListPrototype
 * @package skewer\build\Catalog\Goods\view
 */
abstract class FormPrototype {

    /** @var UI\StateBuilder $form */
    protected $form = null;

    /** @var \skewer\build\Catalog\Goods\model\FormPrototype $model */
    protected $model = null;

    /** @var \skewer\build\Catalog\Goods\Module Модуль в котором осуществляется вывод интерфейса */
    protected $module = null;


    /**
     * Основная функция построение в модуле $oModule интерфейса по модели $oModel
     * @param $oModel
     * @param $oModule
     * @return mixed
     */
    public static function get( $oModel, $oModule ) {
        $obj = new static();
        $obj->model = $oModel;
        $obj->module = $oModule;
        $obj->form = UI\StateBuilder::newEdit();
        $obj->build();
        return $obj->show();
    }


    abstract protected function build();


    protected function show() {
        return $this->form->getForm();
    }
}