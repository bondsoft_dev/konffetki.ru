<?php

namespace skewer\build\Catalog\Goods\view;


/**
 * Построитель списка связанных товаров
 * Class RelatedList
 * @package skewer\build\Catalog\Goods\view
 */
class RelatedList extends ListPrototype {

    protected function build() {

        // добавляем поля
        $this
            ->addField( 'id', 'id', ['width' => 40] )
            ->addField( 'title', \Yii::t('catalog', 'goods_title'), ['flex' => 3] )
            ->addField( 'price', \Yii::t('catalog', 'goods_price'), ['flex' => 1] )
        ;

        // элементы управления
        $this->form
            ->button( \Yii::t('adm', 'add'), 'AddRelatedItem', 'icon-add','init' )
            ->button( \Yii::t('adm', 'back'), 'Edit', 'icon-cancel' )
            ->buttonSeparator()
            ->addButtonDeleteMultiple( 'removeRelatedItem' )
        ;

        // Вывод галочек для множественный операций
        $this->form->showCheckboxSelection();

    }
}