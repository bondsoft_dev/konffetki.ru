<?php

namespace skewer\build\Catalog\Goods\view;


/**
 * Построитель списка модификаций для товара
 * Class ModificList
 * @package skewer\build\Catalog\Goods\view
 */
class ModificList extends ListPrototype {


    /** @var string[] Список обязательных уникальных полей */
    protected $aUniqFields = [ 'id', 'alias', 'active' ];


    protected function build() {

        foreach ( $this->model->getFields() as $oField )
            if ( $oField->getAttr( 'is_uniq' ) || in_array( $oField->getName(), $this->aUniqFields ) )
                if ( !in_array( $oField->getEditorName(), [ 'wyswyg', 'html', 'gallery' ] ) ) {

                    switch ( $oField->getEditorName() ) {
                        case 'check':
                            $sTypeName = 'check';
                            break;
                        case 'money':
                            $sTypeName = 'money';
                            break;
                        default:
                            $sTypeName = 'string';
                    }

                    $this->form->field( $oField->getName(), $oField->getTitle(), 's', $sTypeName, array('listColumns' => array('flex' => 1)) );
                }

        $this->setEditableFields( $this->model->getEditableFields(), 'EditAnalogItem' );

        // элементы управления
        $this->form
            ->buttonRowUpdate( 'EditAnalogItem' )
            ->button( \Yii::t('adm', 'add'), 'EditAnalogItem', 'icon-add' )
            ->button( \Yii::t('adm', 'back'), 'Edit', 'icon-cancel' )
            ->buttonSeparator()
            ->addButtonDeleteMultiple( 'DeleteAnalogItem' )
        ;

        // Вывод галочек для множественный операций
        $this->form->showCheckboxSelection();


    }

}