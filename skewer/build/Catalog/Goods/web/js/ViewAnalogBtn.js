Ext.define('Ext.Catalog.ViewAnalogBtn', {
    extend: 'Ext.Component',

    getClass: function(value, meta, rec) {
        if ( rec.get('analogs') )
            return 'icon-page';

        this.items[0].tooltip = '';
        return '';
    },

    handler: function(grid, rowIndex) {

        var rec = grid.getStore().getAt( rowIndex );
        var mainContainer = processManager.getMainContainer( grid );
        var data = rec.data;

        if ( rec.get('analogs') ) {
            processManager.setData(mainContainer.path,Ext.merge({
                cmd: 'analogItems',
                data: data
            },mainContainer.serviceData));
            processManager.postData();
        }

    }
});