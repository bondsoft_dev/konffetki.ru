<?php

namespace skewer\build\Catalog\Goods\model;

use skewer\build\Component\Catalog\GoodsRow;
use skewer\build\libs\Catalog;
use skewer\build\libs\ft;
use skewer\build\Component\Gallery;


/**
 * Модель для работы с товарной модификации
 * Class ModGoodsEditor
 * @package skewer\build\Catalog\Goods\model
 */
class ModGoodsEditor extends FormPrototype {

    /** @var GoodsRow */
    private $oMainGoods = null;


    public static function get( $iMainGoodsId ) {

        $obj = new static();
        $obj->oMainGoods = GoodsRow::get( $iMainGoodsId );

        return $obj;
    }

    /**
     * Получить id основного товара, для которого кастомизируется фильтр
     * @return int
     */
    public function getMainObject() {
        return $this->oMainGoods;
    }


    public function load( $bGoNext = false ) {

        if ( $id = $this->getDataField( 'id' ) ) {

            $this->oGoodsRow = GoodsRow::get( $id );
        } else {

            $aData = $this->oMainGoods->getData();
            unset( $aData['id'] );

            // hack для галерей todo перенестим в goodsrow
            foreach ($this->oMainGoods->getFields() as $oFtField) {
                if ( $oFtField->getEditorName() == ft\Editor::GALLERY ) {
                    $sFieldName = $oFtField->getName();
                    if ( $oFtField->getAttr('is_uniq') ) {
                        // создаем новую (клонируем) галерею
                        if ($aData[$sFieldName])
                            $aData[$sFieldName] = Gallery\Album::copyAlbum($aData[$sFieldName]);
                    }
                }
            }

            $this->oGoodsRow = GoodsRow::create($this->oMainGoods->getExtCardName());
            $this->oGoodsRow->setMainRowId($this->oMainGoods->getRowId());
            $this->oGoodsRow->setData($aData);
        }

        if ($this->type == 'form') {

            $aData = $this->data;

            // обработка полей по типу
            $aFields = $this->oGoodsRow->getFields();
            foreach ($aFields as $oField) {

                if ( $oField->getEditorName() == ft\Editor::WYSWYG && isSet($aData[$oField->getName()]) )
                    $aData[$oField->getName()] = \ImageResize::wrapTags( $aData[$oField->getName()] );

            }

            $this->oGoodsRow->setData( $aData );

        } elseif ($this->type == 'field') {

            if (!$this->updField) return false;

            $aData[$this->updField] = $this->getDataField($this->updField);
            $this->oGoodsRow->setData($aData);

        } else {
            $this->data = $this->oGoodsRow->getData();
            return $bGoNext;
        }

        return true;
    }

    public function save() {

        if (!$this->load()) return false;

        if ( !($iId = $this->oGoodsRow->save()) ) {
            return false; // todo error view: $this->addError( $this->makeErrorText( $oGoodsRow ) );
        }

        if ( !($section = $this->oGoodsRow->getMainSection()) ) {
            $section = $this->oMainGoods->getMainSection();
            $this->oGoodsRow->setMainSection( $section );
        }

        return true;
    }

    public function delete() {

        if ( !$this->load(true) )
            return false;

        if ( !($id = $this->oGoodsRow->getRowId()) )
            return false;

        if ( !($iId = $this->oGoodsRow->delete()) ) {
            return false;
        }

        return true;
    }


    /**
     * Удаление набора модификаий товаров
     * @return bool
     * @throws \Exception
     */
    public function multipleDelete() {

        $aItems = array();
        if ( $this->getDataField('multiple') ) {

            if ( $items = $this->getDataField('items') )
                foreach ( $items as $aItem )
                    $aItems[] = isset( $aItem['id'] ) ? $aItem['id'] : 0;

        } else {
            if ( $id = $this->getDataField('id') )
                $aItems[] = $id;
        }

        if ( count( $aItems ) )
            foreach ( $aItems as $id ) {
                $this->oGoodsRow = GoodsRow::get( $id );
                if ( !$this->deleteGoods() )
                    return false;
            }

        return true;
    }


    private function deleteGoods() {

        if ( !($id = $this->oGoodsRow->getRowId()) )
            return false;

        if ( !($iId = $this->oGoodsRow->delete()) ) {
            return false;
        }

        return true;
    }

}