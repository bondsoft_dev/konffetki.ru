<?php

namespace skewer\build\Catalog\Goods\model;

use skewer\build\Component\Catalog;


/**
 * Модель для получения списка модификаций
 * Class ModificList
 * @package skewer\build\Catalog\Goods\model
 */
class ModificList extends ListPrototype {

    /** @var int Ид базового товара */
    private $currentObject = 0;

    /** @var Catalog\GoodsRow Базовый товар */
    private $curObj = null;


    public function __construct( $iGoodsId ) {
        $this->currentObject = $iGoodsId;
        $this->curObj = Catalog\GoodsRow::get( $iGoodsId );
        $this->fields = $this->curObj->getFields();
        $this->initQuery();
    }


    /**
     * Функция статической инициализации класса
     * @param int $iGoodsId Ид базового товара
     * @return static
     */
    public static function get( $iGoodsId ) {
        return new static( $iGoodsId );
    }


    /**
     * Получить id основного товара, для которого кастомизируется фильтр
     * @return int
     */
    public function getCurrentObject() {
        return $this->curObj;
    }


    /**
     * Функция инициализации выборки
     */
    protected function initQuery() {
        $this->list = Catalog\GoodsSelector::getModificationList( $this->currentObject );
    }

}