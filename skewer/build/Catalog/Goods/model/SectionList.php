<?php

namespace skewer\build\Catalog\Goods\model;

use skewer\build\Component\Catalog;


/**
 * Модель для построения спискового интерфейса для товаров раздела
 * Class SectionList
 * @package skewer\build\Catalog\Goods\model
 */
class SectionList extends ListPrototype {

    /** @var int Раздел */
    private $section = 0;


    public function __construct( $sCardName, $iSectionId ) {
        $this->section = $iSectionId;
        parent::__construct( $sCardName );
    }


    /**
     * Функция статической инициализации класса
     * @param int $iSectionId Ид раздела для выборки
     * @return static
     */
    public static function get( $iSectionId ) {
        return new static( Catalog\Card::DEF_BASE_CARD, $iSectionId );
    }


    /**
     * Функция инициализации выборки
     */
    protected function initQuery() {
        if ( $this->section )
            $this->list = Catalog\GoodsSelector::getList4Section( $this->section );
        else
            $this->list = Catalog\GoodsSelector::getList( Catalog\Card::DEF_BASE_CARD );
    }


    /**
     * Получение результатов выборки
     * @return mixed
     */
    public function getData() {

        $aItems = parent::getData();

        if ( \SysVar::get( 'catalog.goods_analogs' ) ) {

            // Дописываем в каждый кортеж кол-во аналогов для товара

            $aKeys = array();
            foreach ( $aItems as $key => $aItem ) {
                $aKeys[$aItem['id']] = $key;
            }

            if ( count( $aKeys ) ) {

                $list = Catalog\model\GoodsTable::getChildCount( array_keys( $aKeys ) );

                foreach ( $list as $key => $val ) {
                    if ( isSet($aKeys[$key]) && isSet($aItems[ $aKeys[$key] ]) )
                        $aItems[ $aKeys[$key] ]['analogs'] = $val;
                }

            }
        }

        return $aItems;
    }


    public function getSection() {
        return $this->section;
    }

}