<?php
/**
 * User: kolesnikov
 * Date: 31.07.13
 */

use skewer\models\TreeSection;
use skewer\build\Component\Catalog\GoodsRow;
use skewer\build\Component\Search;

$aConfig['name']     = 'Goods';
$aConfig['title']    = 'Товары';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Товары';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::CATALOG;
$aConfig['useNamespace'] = true;
$aConfig['languageCategory'] = 'catalog';

$aConfig['events'][] = [
    'event' => TreeSection::EVENT_BEFORE_DELETE,
    'eventClass' => TreeSection::className(),
    'class' => GoodsRow::className(),
    'method' => 'removeSection',
];

$aConfig['events'][] = [
    'event' => Search\Api::EVENT_GET_ENGINE,
    'class' => GoodsRow::className(),
    'method' => 'getSearchEngine',
];

return $aConfig;
