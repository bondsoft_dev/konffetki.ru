<?php
/**
 *
 * @class PatchInstallPrototype
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project JetBrains PhpStorm
 * @package kernel
 */

abstract class PatchInstallPrototype extends skUpdateHelper implements skPatchInterface {

    /**
     * Описание патча
     * @var string
     */
    public $sDescription = '';

    /**
     * Флаг необходимости перестроения кэша
     * Пересобираются:
     *  * конфиги модулей
     *  * языковые метки
     *  * css настройки
     * @var bool
     */
    public $bUpdateCache = true;

    /**
     * @throws UpdateException
     */
    public function __construct() {

        parent::__construct();
    }// constructor

    /**
     * Базовый метод запуска обновления
     * @throws skException
     * @throws UpdateException
     * @return bool
     */
    public function execute() {}// func

}// class

