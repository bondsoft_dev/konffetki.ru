<?php
/**
 *
 * @class CurrentUser
 *
 * @author Andrew, $Author$
 * @version $Revision$
 * @date $Date$
 * @project skewer
 * @package Build
 */

class CurrentUserPrototype {

    /**
     * Область видимости
     * @var string
     */
    protected static $sLayer = 'public';

    /**
     * Получить id текущего авторизованного пользователя
     * @static
     * @return int|bool
     */
    public static function getId(){

        return Auth::getUserId(static::$sLayer);
    }

    /**
     * Получить id текущей политики доступа
     * @static
     * @return int|bool
     */
    public static function getPolicyId(){

        return Auth::getPolicyId(static::$sLayer);
    }

    /**
     * Получить уровень для текущей политики доступа
     * @static
     * @return int|bool
     */
    public static function getAccessLevel(){

        return Auth::getAccessLevel(static::$sLayer);
    }

    /**
     * Проверяет является ли пользователь обладатель ограниченных прав
     * @static
     * @return bool
     */
    public static function isLimitedRights(){

        return (bool)(Auth::getAccessLevel(static::$sLayer) > 1);
    }
    
    /**
     * @todo Дописать
     * Получить массив свойств текущего пользователя (дополнительные поля)
     * @static
     * @return array|bool
     */
    public static function getProperties(){

        return false;
    }

    /**
     * @todo Дописать
     * Получить значение свойства текущего пользователя
     * @static
     * @param string $propertyName Имя параметра
     * @param mixed $defValue Значение по-умолчанию если параметра с таким именем нет
     * @return mixed
     */
    public static function getProperty($propertyName, $defValue=null){

        return false;
    }

    /**
     * Для киентской части возвращает true. Политика всегда установлена.
     * @return bool
     */
    public static function isLoggedIn() {
        return Auth::isLoggedIn(static::$sLayer);
    }// func


    /**
     * Проверяет доступен ли текущему пользователю раздел на запись
     * @static
     * @param int $sectionId Id раздела
     * @return bool
     */
    public static function canRead($sectionId){

        return Auth::isReadable(static::$sLayer, $sectionId);

    }// func

    /**
     * Возвращает значение параметра функционального уровня политики доступа (группа+пользователь)
     * @param $sModuleClassName string
     * @param $sParamName string
     * @param null $mDefValue mixed
     * @return mixed
     */
    public function getModuleParam($sModuleClassName, $sParamName, $mDefValue=NULL){

        return Auth::getModuleParam(static::$sLayer, $sModuleClassName, $sParamName, $mDefValue);
    }// func


    /**
     * Возвращает значение параметра функционального уровня политики доступа (группа+пользователь) приведенное
     * к булевому типу
     * @static
     * @param string $moduleClassName Имя класса модуля
     * @param string $paramName Имя параметра
     * @param mixed $defValue Значение по-умолчанию если параметра с таким именем нет
     * @return bool|mixed
     */
    public static function canDo($moduleClassName, $paramName, $defValue=null){

        return Auth::canDo(static::$sLayer, $moduleClassName, $paramName, $defValue);
    }// func

    /**
     * Проверяет наличие запрашиваемого модуля в списке разрешенных модулей текущей политики
     * @param $moduleClassName
     * @return bool
     */
    public static function canUsedModule($moduleClassName) {

        return Auth::canUsedModule(static::$sLayer, $moduleClassName);
    }

    /**
     * Возвращает массив разделов доступных для чтения текущему пользователю
     * @return array
     */
    public static function getReadableSections(){

        return Auth::getReadableSections(static::$sLayer);
    }// func

    public static function getUserData(){

        return Auth::getUserData(static::$sLayer);
    }// func

    public static function reloadPolicy(){

        return Auth::reloadPolicy(static::$sLayer);

    }// func

    public static function login($sLogin = '', $sPassword = ''){

        return Auth::login(static::$sLayer, $sLogin, $sPassword);

    }// func

    /**
     * Выход авторизованного пользователя
     * @static
     * @return bool
     */
    public static function logout(){
        return Auth::logout(static::$sLayer);
    }// func

} // class
