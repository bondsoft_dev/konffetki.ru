<?php
/**
 *
 * @class DesignManager
 *
 * @author ArmiT, $Author: sapozhkov $
 * @version $Revision: 2732 $
 * @date $Date: 2013-12-18 16:19:12 +0400 (Ср., 18 дек. 2013) $
 * @project Skewer
 * @package Build
 */
class DesignManager {

    /** @var string имя таблицы групп */
    private static $sTableGroups = 'css_data_groups';

    /** @var string имя таблицы параметров */
    private static $sTableParams = 'css_data_params';

    /** @var array Массив для хранения выбранных параметров */
    private $paramsWithRef = [];


    /**
     * Собирает массив групп для параметров или родительских групп для групп
     * На входе массив [
     *      default => [
     *                     base.base_font-family.font-size
     *                    ]
     *               ]
     *
     * Усекает название до последней точки, получая группу или родительскую группу
     *
     * На выходе [
     *      default => [
     *                     base.base_font-family.font-size => 7 - id группы base.base_font-family
     *                    ]
     *               ]
     * @param array $aItems
     * @return array
     */
    private function getGroupsId( array $aItems ){

        $aLayerGroups = [];

        foreach($aItems as $sLayerKey => $aItem){
            $aKeys = [];
            foreach( array_unique(array_keys($aItem)) as $sKey ){
                if (!isset($aKeys[$sKey])){
                    $iPoint = strrpos($sKey, '.');
                    $aKeys[$sKey] = ( $iPoint!==false )? substr($sKey, 0, $iPoint): '';
                }
            }

            $aGroups = CssGroupsMapper::getGroupsIdByName(array_unique($aKeys), $sLayerKey);

            $aLayerGroups[$sLayerKey] = array_map(
                function( $name ) use ($aGroups) {
                    return isset($aGroups[$name])?$aGroups[$name]:0;
                },
                $aKeys
            );
        }

        return $aLayerGroups;
    }


    /**
     * Обновление групп
     * @param array $aParams
     */
    private function updateDesignGroups( array $aParams ){

        /** @var array Массив групп $aLayerGroups */
        $aLayerGroups = $this->getGroupsId($aParams);

        foreach( $aParams as $sLayerKey=>$aLayer ){

            // Проход по массиву групп
            foreach( $aLayer as $sGroupKey=>$sGroup ){

                $aData = CssGroupsMapper::getItem(['where_condition' => [
                    'name' => [
                        'sign' => '=',
                        'value' => $sGroupKey
                    ],
                    'layer' => [
                        'sign' => '=',
                        'value' => $sLayerKey
                    ]
                ]]);

                if ( !$aData ) {
                    $aData = [];
                    $aData['name'] = $sGroupKey;
                    $aData['layer'] = $sLayerKey;
                    $aData['title'] = $sGroup;
                }

                $aData['parent'] = isset($aLayerGroups[$sLayerKey][$sGroupKey])?$aLayerGroups[$sLayerKey][$sGroupKey]:'';

                // Сохранить в БД
                $id = CssGroupsMapper::saveItem( $aData );

                if ($id){
                    /** Группа пересохранена - отметим ее ид в ее наследниках */
                    foreach( $aLayerGroups[$sLayerKey] as $sKey => $val ){
                        $iPoint = strrpos($sKey, '.');
                        $sParentKey = ( $iPoint!==false )? substr($sKey, 0, $iPoint): '';

                        if ($sParentKey == $sGroupKey){
                            $aLayerGroups[$sLayerKey][$sKey] = $id;
                        }
                    }
                }
            }
        }

    }


    /**
     * Обновление параметров
     * @param array $aParams
     */
    private function updateDesignParams( array $aParams ){

        $aLayerGroups = $this->getGroupsId($aParams);

        // Проход по слоям
        foreach($aParams as $sLayerKey=>$aLayer){

            // Проход по параметрам
            foreach( $aLayer as $sParamKey=>$aParameter ){

                $aData = [];

                $aData['name'] = $sParamKey;
                // Получить id группы по её имени
                $aData['group'] = isset($aLayerGroups[$sLayerKey][$sParamKey])?$aLayerGroups[$sLayerKey][$sParamKey]:'';
                $aData['layer'] = $sLayerKey;
                $aData['title'] = $aParameter['title'];
                $aData['type'] = $aParameter['type'];
                $aData['default_value'] = $aParameter['default'];
                $aData['value'] = $aParameter['default'];

                // Вставить параметр
                CssParamsMapper::insertItem($aData);
            }
        }
    }


    /**
     * Метод по обновлению групп и параметров в базе
     * @param array $aUpdateParams
     * @return bool
     */
    public function updateDesignSettings( $aUpdateParams = array() ){

        if ( !$aUpdateParams ) return false;

        // Проход по переданным группам
        if ( sizeof($aUpdateParams['groups']) ){

            $this->updateDesignGroups( $aUpdateParams['groups'] );

        }

        // Проход по переданным css-параметрам
        if( sizeof($aUpdateParams['params']) ){

            $this->updateDesignParams( $aUpdateParams['params'] );

        }

        return true;
    }//function updateDesignSettings()

    /**
     * Добавляет либо обновляет ссылки между параметрами css в таблице css_data_inheriting
     * @param $references
     * @return bool
     */
    public function saveReferences($references) {

        $aNames = [];

        foreach($references as $ancestor=>$descendants) {
            if(strpos($ancestor, '..') !== false){
                $aNames[] = substr($ancestor, strpos($ancestor, '..') + 2);
            }
            foreach($descendants as $item) {
                $aNames[] = substr($item, strpos($item, '..') + 2);
            }
        }

        $aParams = $this->getParamsByNames($aNames);
        $aParams = \yii\helpers\ArrayHelper::index($aParams, 'name');

        $aDepParams = [];

        foreach($references as $ancestor=>$descendants) {

            $ancestorLayer = null;
            if(strpos($ancestor, '..') !== false)
                list($ancestorLayer, $ancestor) = explode('..', $ancestor);

            foreach($descendants as $item) {

                $descendantLayer = null;
                if(strpos($item, '..') !== false)
                    list($descendantLayer, $item) = explode('..', $item);

                if (isset($aParams[$ancestor]) && $aParams[$ancestor]['layer'] == $ancestorLayer &&
                    isset($aParams[$item]) && $aParams[$item]['layer'] == $descendantLayer
                ) {
                    $aDepParams[] = [
                        $ancestor, $item
                    ];
                }

            }
        }

        CssParamsMapper::saveReferences( $aDepParams );
    }


    /**
     * Список параметров по списку имен
     * @param array $aNames
     * @return array
     */
    private function getParamsByNames( array $aNames ){

        $aItems = CssParamsMapper::getItems([
            'select_fields' => array(
                'id', 'name', 'layer'
            ),
            'where_condition' => [
                'name' => [
                    'sign' => 'IN',
                    'value' => $aNames
                ]
            ]
        ]);

        return (isset($aItems['items']))?$aItems['items']:[];

    }

    public function getGroupByName($sGroupName, $sLayer='default') {

        $sTable = self::$sTableGroups;
        $oResult = \skewer\build\Component\orm\Query::SQL(
            "SELECT * FROM `$sTable` WHERE `name`=:groupName AND `layer`=:layer LIMIT 0, 1;",
            array(
                'groupName' => $sGroupName,
                'layer' => $sLayer
            )
        );

        return $oResult->fetchArray();
    }

    /**
     * Добавление / изменение параметра
     * @param $aData
     * @return bool
     */
    public static function saveCSSParam( $aData ) {

        return CssParamsMapper::saveItem( $aData );

    }// func

    /**
     * Сохраняет значение параметра по id
     * @static
     * @param $iId - id записи
     * @param $sValue - значение для сохранения
     * @return bool
     */
    public static function saveCSSParamValue( $iId, $sValue ) {

        /*
         * Правки по задаче #5510
         * При попытке установки в режиме дизайнера пустого значения для параметра и, при условии, что параметр - типа url,
         * автоматически подставляется заглушка empty.gif.
         * Правки сделаны для того, чтобы имелась возможность сбросить картинку и чтобы при этом не поехала верстка.
         */
        $sType = DesignManager::getParamTypeById($iId);

        if ( $sType=='url' && !$sValue)
            $sValue = '/images/empty.gif';

        if( $sType == 'px' )
            $sValue = !$sValue ? ($sValue==0 ? 0 : '') : ((int)$sValue).'px';

        if( $sType == 'em' )
            $sValue = !$sValue ? ($sValue==0 ? 0 : '') : ((float)$sValue).'em';

        if( $sType == 'size' )
            $sValue = (is_numeric($sValue) && $sValue!='0') ? ($sValue.'px') : $sValue;

        return CssParamsMapper::saveItem( array(
            'id' => $iId,
            'value' => $sValue
        ) );

    }

    /**
     * Откатывает значение параметра на стандартное
     * @static
     * @param int $iId
     * @return bool
     */
    public static function revertSCCParam( $iId ){

        $aData = CssParamsMapper::getItem( (int)$iId );

        if ( !$aData )
            return false;

        $aData['value'] = $aData['default_value'];

        return (bool)CssParamsMapper::saveItem( $aData );
    }

    /**
     * Запросить все параметры группы
     * @param int $sGroupId - id группы
     * @return array|bool
     */
    public static function getParamsByGroup( $sGroupId ) {

        return CssParamsMapper::getParamListByGroupIdWthRefs( $sGroupId );

    }// func


    /**
     * Получение группы для параметра
     * @param int $iParamId - id параметра
     * @return mixed
     */
    public static function getGroupByParam( $iParamId ) {

        return CssParamsMapper::getGroupByParam( $iParamId );

    }

    /**
     * Отдает все группы в древовидном виде
     * @param string $sLayer
     */
    public static function getAllGroupsAsTree( $sLayer='default' ) {

        // сборка запроса
        $sQuery =
            'SELECT `id`, `title`, `name`, `visible`
            FROM `css_data_groups` AS `groups`
            WHERE `groups`.`layer`=:layer
            ORDER BY `id`';

        // данные для запроса
        $aData = ['layer' => $sLayer];

        // выполнение запроса
        $oResult = \Yii::$app->db->createCommand($sQuery, $aData)->query();

        /*
         * сборка списка
         */

        // полный список со значениями
        $aList = [];

        // сборка
        while ( $aRow = $oResult->read() ) {
            $aRow['level'] = substr_count($aRow['name'], '.'); // число точек - уровень вложенности
            $aList[(int)$aRow['id']] = $aRow;
        }

        // выбираем количество записей в группах
        $oResult = \Yii::$app->db->createCommand(
            'SELECT `group`, COUNT(*) AS `cnt` FROM `css_data_params` GROUP BY `group`'
        )->query();
        $aCnt = [];
        while ( $aRow = $oResult->read() )
            $aCnt[(int)$aRow['group']] = (int)$aRow['cnt'];

        // сортируем по уровню и заголовку
        usort($aList, function($a, $b){
            if ($a['level'] == $b['level']) {
                if ($a['title'] == $b['title'])
                    return 0;
                return ($a['title'] < $b['title']) ? -1 : 1;
            }
            return ($a['level'] < $b['level']) ? -1 : 1;
        });

        /*
         * Сборка выходного массива
         */
        $aOut = ['children' => []];
        $aRef = ['' => &$aOut['children']];
        foreach ( $aList as $aRow ) {

            // положение последней точки (для вычичления родительской группы)
            $iDotPos = strrpos($aRow['name'], '.');
            $iGroup = (int)$aRow['id'];

            // дополнительные поля
            $aRow['children'] = [];
            $aRow['cnt'] = isset($aCnt[$iGroup]) ? $aCnt[$iGroup] : 0;
            unset($aRow['level']);

            // определение родителя
            $sParentLabel = $iDotPos ? substr($aRow['name'], 0, $iDotPos)  : '';
            if ( !isset($aRef[$sParentLabel]) )
                $sParentLabel = '';

            // занесение в массив и установка ссылки для добавления
            $aRef[$sParentLabel][] = $aRow;
            $aRef[$aRow['name']] = &$aRef[$sParentLabel][count($aRef[$sParentLabel])-1]['children'];

        }

        return($aOut['children']);

    }

    /**
     * Отдает список всех групп
     * @static
     * @param string $sLayer слой
     * @param array $aFields набор полей
     * @return array
     */
    public static function getGroupList( $sLayer='default', $aFields = array() ) {

        // выходная переменная
        $aOut = array();

        // сборка запроса
        $sQuery = sprintf(
            "SELECT %s FROM `%s` WHERE layer=:layer ORDER BY `name` ASC;",
            ( is_array($aFields) and count($aFields) ) ? '`' . implode( '`,`', $aFields ) . '`' : '*',
            self::$sTableGroups
        );

        // данные для запроса
        $aData = array(
            'layer' => $sLayer
        );

        // выполнение запроса
        $oResult = \skewer\build\Component\orm\Query::SQL( $sQuery, $aData );

        // обойти все полученные данные
        while ( $aGroup = $oResult->fetchArray() ) {

            // дополнить выходноя массив
            $aOut[] = $aGroup;
        }

        // отдать набор
        return $aOut;
    }

    /**
     * Возвращает список групп до текущей от 0-го уровня по $sCurrentGroupPath
     * @param string $sCurrentGroupPath Путь по названиям групп от корня до требуемой
     * @param string $sLayer режим отображения (по-умолчанию или pda версия)
     * @return array|bool Возвращает массив, отсортированный по группам в порядке из вложенности либо false
     */
    public function getGroupPath($sCurrentGroupPath, $sLayer='default') {

        $aOut = array();

        $aParents = explode('.', $sCurrentGroupPath);
        foreach($aParents as $sGroup){
            if(!$aCurrent = $this->getGroupByName($sGroup, $sLayer)) return false;
            $aOut[] = $aCurrent;
        }

        return $aOut;
    }// func

    /**
     * Метод для выборки css-параметров из БД
     * @var bool $withInheritance Если указан, то в процессе выборки будет учтено наследование параметров
     * @return array
     */
    public function getParams($withInheritance = false) {


        if(!$withInheritance) {
            // Формируем фильтр
            $aFilter = array(
                'select_fields' => array(
                    'name',
                    'layer',
                    'value'
                ),
                'where_condition' => array(
                    'group' => array(
                        'sign' => '<>',
                        'value' => 0
                    )
                )
            );

            // Получаем массив параметров
            $aTempParams = CssParamsMapper::getItems($aFilter);
        } else {

            if (!$this->paramsWithRef)
                $this->paramsWithRef = CssParamsMapper::getParamsWithRef();

            $aTempParams = $this->paramsWithRef;
        }


        $aParams = array();

        /*
         * Пересобираем массив в необходимый вид
         * array[<имя слоя>][<имя параметра>] = <значение параметра>
         */
        foreach( $aTempParams['items'] as $aItem ){

            $aParams[$aItem['layer']][$aItem['name']] = $aItem;
        }

        return $aParams;

    }// func

    public static function getParam( $sKey, $sLayer='default' ){

        $aParam = CssParamsMapper::getParamWithRef($sKey, $sLayer);

        return ( $aParam['count'] )? $aParam['items'][0]: false;
    }

    public static function clearCSSTables(){

        CssGroupsMapper::clearTable();
        CssParamsMapper::clearTable();
        CssDataReferencesMapper::clearTable();

        return true;
    }

    public static function getParamTypeById($iParamId){

        $aParam = CssParamsMapper::getItem($iParamId);

        return (sizeof($aParam))? $aParam['type']: false;
    }

    public function delGroup($sGroupName, $sLayer='default'){

        if($group = $this->getGroupByName($sGroupName, $sLayer)){

            // выбираем потомков чтобы удалить
            $sTable = self::$sTableGroups;
            $oResult = \skewer\build\Component\orm\Query::SQL(
                "SELECT * FROM `$sTable` AS `groups` WHERE `groups`.`layer`=:layer AND `groups`.`parent`=:parent",
                array(
                    'parent' => $group['id'],
                    'layer' => $group['layer']
                )
            );

            // обойти все полученные данные
            while ( $aCurrentGroup = $oResult->fetchArray() )
                $this->delGroup($aCurrentGroup['name'], $aCurrentGroup['layer']);

            // удалить параметры текущей группы
            $params = $this->getParamsByGroup($group['id']);
            if($params and count($params))
                foreach($params as $param)
                    CssParamsMapper::delItem($param);

            // покончить с самой группой
            CssGroupsMapper::delItem($group);
            return true;
        }

        return false;
    }

    public static function setActiveParamRefs($id, $active){

        $param = CssParamsMapper::getItem($id);

        return \skewer\build\Component\orm\Query::SQL(
            "UPDATE `css_data_references` SET `active`=:active WHERE `descendant`=:name;",
            array(
                'active' => $active ? 0 : 1,
                'name' => isset( $param['name'] ) ? $param['name'] : false
            )
        );
    }

}// class
