<?php

/**
 * Фасад для доступа к основным ресурсам и компонентам сайта
 */
class sk {

    /**
     * @param string $sQuery Текст запроса
     * @param array|string|int [$aData] Переменные для запроса
     * @param mixed [$param2]
     * @return \skewer\build\Component\orm\service\DataBaseAdapter
     * @throws \Exception
     */
    public static function query($sQuery) {
        return call_user_func_array(
            array(
                'skewer\build\Component\orm\Query',
                'SQL'
            ),func_get_args()
        );
    }

}