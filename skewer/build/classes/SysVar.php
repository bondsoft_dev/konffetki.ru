<?php
/**
 * Класс для работы с системными переменными
 */
class SysVar {

    /**
     * @var string Имя текущей таблицы, с которой работает маппер
     */
    protected static $sCurrentTable = 'sys_vars';

    private static $cache = null;

    /**
     * возвращает значение системной переменной
     * @static
     * @param string $sName
     * @return string|null
     */
    public static function get($sName){

        if (!$sName or !is_string($sName))
            return null;

        $sTable = self::$sCurrentTable;

        if (is_null(self::$cache)){
            self::$cache = \yii\helpers\ArrayHelper::map(Yii::$app->db->createCommand("SELECT * FROM `$sTable` WHERE 1;" )->queryAll(), 'sv_name', 'sv_value');
        }

        return \yii\helpers\ArrayHelper::getValue(self::$cache, $sName, null);
    }

    /**
     * Если SysVar не задан, устанавливаем в дефолтное значение
     * @param $sName
     * @param string $sDefault
     * @return null|string
     */
    public static function getSafe($sName,$sDefault = ""){

        $result = self::get($sName);
        if ($result === null){
            $bRes = self::set($sName,$sDefault);
            return $bRes ? $sDefault : null;
        } else return $result;

    }


    /**
     * сохранение значения системной переменной
     * @static
     * @param string $sName название переменной
     * @param mixed $mValue значение переменной
     * @return bool
     */
    public static function set( $sName, $mValue ) {

        if ( !$sName or !is_string($sName) )
            return false;

        $sTable = self::$sCurrentTable;

        $oResult = sk::query(
            "INSERT INTO `$sTable` (sv_name, sv_value)
                    VALUES (:name, :value)
                    ON DUPLICATE KEY UPDATE sv_value=:value_update",
            array(
                'name' => $sName,
                'value' => $mValue,
                'value_update' => $mValue,
            )
        );

        self::$cache = null;

        return (bool)$oResult->affectedRows();
    }

    /**
     * Возворащает состояние сервера (продакшн/тест)
     * @return bool
     * @throws GatewayException
     */
    public static function isProductionServer() {

        if(!INCLUSTER) return true;

        $oClient = skGateway::createClient();

        $bResultStatus = false;

        $oClient->addMethod('HostTools', 'isProductionServer', array(), function($mResult, $mError) use (&$bResultStatus) {

                $bResultStatus = $mResult;

        });

        if(!$oClient->doRequest()) return false;

        return $bResultStatus;
    }

}