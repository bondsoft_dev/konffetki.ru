<?php
/**
 *
 * @class Auth
 *
 * @author Andrew, $Author$
 * @version $Revision$
 * @date $Date$
 * @project skewer
 * @package Build
 */
class Auth {

    // Секретное слово для генерации пароля
    private static $sSalt = "Canape3.0";

    /**
     * @var null|\skFirewall
     */
    public static $oFirewall = null;

    /**
     * id группы неавторизованного пользователя
     * @return mixed|null
     */
    public static function getDefaultGroupId(){
        return \Yii::$app->params['auth']['public_default_id'];
    }

    /**
     * id группы авторизованного пользователя
     * @return mixed|null
     */
    public static function getUserGroupId(){
        return \Yii::$app->params['auth']['group_user_id'];
    }

    public static function init() {

        /*
         *  Если IP в сессии пользователя не совпадает с пришедшим - разлогиневаем текущего
         * публичного и админа
         */
        /* Мы уже залогонились и имеем IP  */
        if(isSet($_SESSION['auth']['userIP']))
            if($_SESSION['auth']['userIP'] != \Yii::$app->request->getUserIP()) {
                Auth::logout('public');
                Auth::logout('admin');
            }

        /* Отлавливаем попытку подмены куки  */
        if(isSet($_SESSION['auth']['hostName']))
            if($_SESSION['auth']['hostName'] != $_SERVER['SERVER_NAME']) {
                Auth::logout('public');
                Auth::logout('admin');
            }

        /* Если первое открытие страницы - грузим пользователя по-умолчанию */
        if(!isSet($_SESSION['auth']['public']['userData']['id'])) {

            /* Выбор дефолтной политики по фильтрам */
            $aUserData =  AuthUsersMapper::getDefaultUserData();
            $aUserData['group_policy_id'];

            self::$oFirewall = new skFirewall();
            $iUserId = self::$oFirewall->getUserId(\Yii::$app->request->getUserIP(), $aUserData['group_policy_id'], $aUserData['id']);
            /* Если запрашиваемый пользователь не изменился - отключаем firewall */
            if($iUserId == $aUserData['id'])
                self::$oFirewall->enable(false);

            self::loadUser('public', $iUserId);

        } else {
            if(self::getPolicyVersion('public') != Policy::getPolicyVersion()) {
                self::loadUser('public',self::getUserId('public'));
                self::reloadPolicy('public');
            }
        }

        if(self::getUserId('admin'))
            if(self::getPolicyVersion('admin') != Policy::getPolicyVersion()){
                self::loadUser('admin',self::getUserId('admin'));
                self::reloadPolicy('admin');
            }
    }

    // @todo Проверить!!
    public function smsLogin($sLogin, $sTicket) {

        if ( !$sLogin || !$sTicket ) return false;

        $aFilter = array(
            'login' => $sLogin,
            'login_area' => 'admin'
        );
        /*проверка тикета*/
        $iUserId = AuthUsersMapper::checkUser($aFilter, 'true');

        if( $iUserId )
            return self::loadUser('admin', $iUserId);

        return false;
    }// func

    public static function loadPolicy($sLayer, $iPolicyId, /** @noinspection PhpUnusedParameterInspection */$iUserId = 0) {

        $aGroupPolicy = Policy::getPolicyHeader( $iPolicyId );
        if ( !$aGroupPolicy or !$aGroupPolicy['active'] )
            return false;

        // Считываем список разрешенных для чтения разделов
        $aGroupPolicyData = Policy::getGroupPolicyData($iPolicyId);

        $_SESSION['auth'][$sLayer]['policy_version'] = (int)$aGroupPolicyData['version'];
        $_SESSION['auth'][$sLayer]['start_section'] = ((int)$aGroupPolicyData['start_section'])? (int)$aGroupPolicyData['start_section']: \Yii::$app->sections->main();
        $_SESSION['auth'][$sLayer]['read_access'] = $aGroupPolicyData['read_access'];
        $_SESSION['auth'][$sLayer]['actions_access']  = $aGroupPolicyData['actions_access'];
        $_SESSION['auth'][$sLayer]['modules_access']  = $aGroupPolicyData['modules_access'];

        if(count($aGroupPolicyData['read_disable']))
            $_SESSION['auth'][$sLayer]['read_disable'] = $aGroupPolicyData['read_disable'];

        return true;
    }// func

    /**
     * Возвращает id стартового раздела текущей политики
     * @static
     * @param $sLayer
     * @return bool
     */
    public static function getMainSection($sLayer = 'public') {
        return (isSet($_SESSION['auth'][$sLayer]['start_section']))? $_SESSION['auth'][$sLayer]['start_section']: false;
    }// func

    public static function logout($sLayer = 'public'){

        switch( $sLayer ){

            case 'public':
                if ( isset($_SESSION['auth'][$sLayer]) ) unset($_SESSION['auth'][$sLayer]);
            break;

            case 'admin':
                if ( isset($_SESSION['auth'][$sLayer]) ) unset($_SESSION['auth'][$sLayer]);
                Design::unsetModeGlobalFlag(); // дополнительно, сбрасываем флаг дизайнерского режима, чтобы не палить js
            break;
        }

        return true;
    }

    /**
     * Проверяет пользователя $sLogin с паролем $sPassword через шлюз в системе управления сайтами
     * Проверка осуществляется по двум критериям:
     * 1. Пользователь должен существовать и иметь установленный флаг активности.
     * 2. Пользователь должен находиься в группе владельцев сайта с которого происходит запрос авторизации
     * @param string $sLogin Логин пользователя
     * @param string $sPassword Пароль пользователя
     * @throws GatewayException
     * @return bool Возвращает true если пользователь найден либо false в противном случае.
     */
    protected static function checkGlobalUser($sLogin, $sPassword) {

        $bSuccess = false;
        
        /* Сайт не подключен к кластеру */
        if(!INCLUSTER) {
            
            /* Получаем список прописанных в конфиге пользователей */
            $aUsers = \Yii::$app->params['users'];
            
            if(!$aUsers) return false;
            
            /* Обходим пользователей, если есть совпадение - разрешаем авторизацию */
            foreach($aUsers as $aUser) {
                if( $aUser['login'] == $sLogin && $aUser['pass'] == $sPassword ) return true;
            }
            
            return false;
        }
        
        try {

            $oClient = skGateway::createClient();

            $aData['login']     = $sLogin;
            $aData['password']  = $sPassword;

            $oClient->addMethod('HostTools', 'login', $aData, function($mResult, $mError) use (&$bSuccess) {

                /* Ошибок нет и авторизация подтверждена */
                if(!$mError && $mResult) $bSuccess = true;

            });

            if(!$oClient->doRequest()) throw new GatewayException($oClient->getError());

        } catch(GatewayException $e) {

            skLogger::dump('Global User Auth error: '.$e->getMessage());
        }
        
        return $bSuccess;

    }// func

    /**
     * Метод проверки существования пользователя.
     * В случае успешной выборки метод возвращает id пользователя, прошедшего авторизацию
     * @param $sLayer
     * @param $sLogin
     * @param $sPassword
     * @return bool|array Массив данных по пользователю либо false
     */
    public static function checkUser($sLayer, $sLogin, $sPassword){

        $aFilter = array(
            'login' => $sLogin,
            'password' => self::buildPassword($sLogin, $sPassword),
            'login_area' => $sLayer,
            'active' => '1',
        );

         // Выбрать пользователя
        $mUser = AuthUsersMapper::checkUser($aFilter);

        if (!$mUser) return false;

        /* Проверяем через внешнюю авторизацию */
        if (($mUser['global_id']))
            return (Auth::checkGlobalUser($sLogin, $sPassword))? $mUser: false;

        return $mUser;
    }// function checkUser()

    // Генерация хэша пароля по логину, паролю и секретному слову
    public static function buildPassword( $sLogin, $sPassword ) {

        return md5($sLogin.$sPassword.self::$sSalt);
    }// function buildPassword()

    /**
     * Возвращает доступен ли пользователю раздел для чтения
     * @param $sLayer
     * @param int $iSectionId id раздела
     * @return bool
     */
    public static function isReadable($sLayer, $iSectionId){

        // Если запрошенный раздел есть в списке разрешенных - отдать true
        if(!isset($_SESSION['auth'][$sLayer])) return false;
        // !! не учнена политика системного администратора - обрабатывается вызывающим методом
        if ( !$iSectionId || array_search($iSectionId, $_SESSION['auth'][$sLayer]['read_access']) !== false ) return true;
        // todo архитектурный казус, все строится от 0 раздела, который является какбы родительским для 1. Могут быть проблемы с безопасностью.

        return false;
    }// func


    /**
     * Возвращает значение параметра функционального уровня политики доступа установленной
     * для пользователя (группа+персональная политика)D
     * @static
     * @param $sLayer
     * @param string $moduleClassName имя класса модуля
     * @param string $paramName имя параметра модуля
     * @param mixed $defValue Значение по-умолчанию если параметра с таким именем нет
     * @internal param int $userId id пользователя
     * @return mixed
     */
    public static function getModuleParam($sLayer, $moduleClassName, $paramName, $defValue=null){

        return false;

        /*if ( isset($this->aGroupPolicy['actions_access'][$sModuleClassName][$sParamName] ) ){

            return $this->aGroupPolicy['actions_access'][$sModuleClassName][$sParamName]['value'];
        }
        else return $mDefValue;*/
    }

    /**
     * Возвращает булевое значение параметра функционального уровня политики доступа установленной
     * для пользователя (группа+персональная политика)
     * @static
     * @param int $userId id пользователя
     * @param string $moduleClassName имя класса модуля
     * @param string $paramName имя параметра модуля
     * @param mixed $defValue Значение по-умолчанию если параметра с таким именем нет
     * @return bool|mixed
     */
    public static function userCanDo($userId, $moduleClassName, $paramName, $defValue=null){

        return false;
    }


    /**
     * Метод авторизации пользователя
     * @param $sLayer
     * @param string $sLogin
     * @param string $sPassword
     * @return bool
     */
    public static function login($sLayer,  $sLogin = '', $sPassword = ''){

        /**
         * Проверить доступ к политике по фильтрам
         */

        $aUser = self::checkUser($sLayer, $sLogin, $sPassword);

        /* Пользователь не существует либо параметры не верны */
        if(!$aUser) return false;

        $bRes = self::loadUser($sLayer, $aUser['id']);

        // обновляем дату последнего захода
        Users::updateLoginTime($aUser['id']);

        return $bRes;
    }// function login()

    /**
     * @param $sLayer
     * @param int $iUserId
     * @throws Exception
     * @return bool
     */
    public static function loadUser($sLayer, $iUserId = 0) {

        try {

            // Проверяем пользователя на существование и на активность
            $aUserData= Users::getUserData($iUserId);

            $iStatusActive = (int) $aUserData['active'];

            if(!$aUserData OR $iStatusActive!=1)
                throw new Exception('User not found or not active');

            // Загружаем персональную информацию о пользователе по его ID
            $_SESSION['auth'][$sLayer]['userData'] = $aUserData;
            $iUserId = $_SESSION['auth'][$sLayer]['userData']['id'];
            $iPolicyId = (int)$_SESSION['auth'][$sLayer]['userData']['group_policy_id'];


            /** @todo Разобраться, почему в adm слое не используется Auth::init. Почему у класса отсутствует секция однозначной инициализации
            */

            /* Убрать после выяснения вопроса выше */
            if(!(self::$oFirewall instanceof skFirewall))
                self::$oFirewall = new skFirewall();


            if(!self::$oFirewall->checkAccess(\Yii::$app->request->getUserIP(),$iPolicyId))
                throw new Exception('Firewall blocked access!');

            /* Сохраняем в сессию IP пользователя, под которым логинились */
            // todo В метод
            $_SESSION['auth']['userIP'] = \Yii::$app->request->getUserIP();

            // Сохраняем в сессию host_name для пользователя
            $_SESSION['auth']['hostName'] = isset($_SERVER['SERVER_NAME']) ? $_SERVER['SERVER_NAME'] : '';

            if (!$iPolicyId)
                throw new Exception('No policy');

            $bPolicy = self::loadPolicy($sLayer, $iPolicyId, $iUserId);
            if ( !$bPolicy )
                throw new Exception('Policy not found or not active');

            /* Получаем данные по политике */
            $aPolicyHeader = Policy::getPolicyHeader($iPolicyId);
            $_SESSION['auth'][$sLayer]['userData']['policyAlias'] = $aPolicyHeader['alias'];
            $_SESSION['auth'][$sLayer]['userData']['access_level'] = $aPolicyHeader['access_level'];
            $_SESSION['auth'][$sLayer]['userData']['systemMode'] = false;

            /* Проверка на системного пользователя */
            if ($_SESSION['auth'][$sLayer]['userData']['login'] == 'sys' AND
                $sLayer == 'admin' AND
                    isSet($aPolicyHeader['alias']) AND
                        $aPolicyHeader['alias'] == 'sysadmin' // todo константа
            )
                /* Пользователь системный - взводим флаг */
                $_SESSION['auth'][$sLayer]['userData']['systemMode'] = true;

            return true;

        } catch ( Exception $e ) {
            self::logout($sLayer);
            skProcessSession::flushStorage();
            return false;
        }
    }// func


    public static function reloadPolicy($sLayer){

        if(! $iPolicyId = self::getPolicyId($sLayer)) return false;
        Policy::checkCache($iPolicyId);
        self::loadPolicy($sLayer, $iPolicyId);

        return true;

    }// func

    public static function getUserId($sLayer) {

         return (!isSet($_SESSION['auth'][$sLayer]['userData']['id']))? false: $_SESSION['auth'][$sLayer]['userData']['id'];

    }// func

    public static function getPolicyId($sLayer) {

        return (!isSet($_SESSION['auth'][$sLayer]['userData']['group_policy_id']))? false: $_SESSION['auth'][$sLayer]['userData']['group_policy_id'];

    }// func

    public static function getAccessLevel($sLayer) {

        return (!isSet($_SESSION['auth'][$sLayer]['userData']['access_level']))? 1000: $_SESSION['auth'][$sLayer]['userData']['access_level'];

    }// func

    public static function getPolicyVersion($sLayer) {

        return (!isSet($_SESSION['auth'][$sLayer]['policy_version']))? false: $_SESSION['auth'][$sLayer]['policy_version'];

    }// func

    public static function isLoggedIn($sLayer) {
        return (bool)self::getUserId($sLayer);
    }// func

    public static function getUserData($sLayer) {

        return (isSet($_SESSION['auth'][$sLayer]['userData']))? $_SESSION['auth'][$sLayer]['userData']: false;

    }// func

    public static function getReadableSections($sLayer) {

        return (isSet($_SESSION['auth'][$sLayer]['read_access']))? $_SESSION['auth'][$sLayer]['read_access']: false;

    }// func

    public static function getDenySections($sLayer) {

        return (isSet($_SESSION['auth'][$sLayer]['read_disable']))? $_SESSION['auth'][$sLayer]['read_disable']: false;

    }// func

    /**
     * Возвращает список запрещенных к чтению разделов для пользователя $iUserId. Если $iUserId не указан
     * то возвращает список для default пользователя
     * @param bool $iUserId
     * @return array Возвращает массив id разделов либо пустой массив
     * @throws Exception В случае нарушения целостности генерирует исключение
     */
    public static function getDenySectionByUserId($iUserId = false) {

        if($iUserId !== false)
            $aUserData = Users::getUserData($iUserId);
        else
            $aUserData =  AuthUsersMapper::getDefaultUserData();

        if(!$aUserData['group_policy_id'])
            throw new \Exception('Wanted user does not exists!');

        $aGroupPolicyData = Policy::getGroupPolicyData($aUserData['group_policy_id']);
        if(!count($aGroupPolicyData))
            throw new \Exception('The policy does not have a permissions cache!');

        if(count($aGroupPolicyData['read_disable']))
            $aGroupPolicyData['read_disable'];

        return $aGroupPolicyData['read_disable'];

    }


    public static function getAvailableModules($sLayer) {

        return (isSet($_SESSION['auth'][$sLayer]['modules_access']))? $_SESSION['auth'][$sLayer]['modules_access']: false;
    }

    public static function canDo($sLayer, $moduleClassName, $paramName, $defValue=false){

        return (isSet($_SESSION['auth'][$sLayer]['actions_access'][$moduleClassName][$paramName]))? $_SESSION['auth'][$sLayer]['actions_access'][$moduleClassName][$paramName]['value']: $defValue;
    }

    public static function canUsedModule($sLayer, $moduleClassName) {

        return isSet($_SESSION['auth'][$sLayer]['modules_access'][$moduleClassName]);
    }


    public static function authUserByToken($sToken){

        if ( !INCLUSTER )
            return;

        $oClient = skGateway::createClient();

        $aParam = array($sToken);

        $oClient->addMethod('HostTools', 'getAuthAdmin', $aParam, function($mResult, $mError) {

            if($mResult){

                $aResult = explode(':',$mResult);

                if( isset($aResult[1]) && $aResult[1].'/'==WEBROOTPATH ){

                    $aUserData = Users::getUserDataByName($aResult[0]);

                    if(!$aUserData)
                        $aUserData = Users::getUserDataByName('admin');

                    if($iUserId = $aUserData['id']){
                        /* Убрать после выяснения вопроса выше */
                        if(!(Auth::$oFirewall instanceof skFirewall))
                            Auth::$oFirewall = new skFirewall();

                        Auth::$oFirewall->enable(false);

                        Auth::loadUser('admin', $iUserId);

                        // обновляем дату последнего захода
                        Users::updateLoginTime($iUserId);

                    }

                }

            }

        });

        if(!$oClient->doRequest()) throw new Exception($oClient->getError());

    }

}
