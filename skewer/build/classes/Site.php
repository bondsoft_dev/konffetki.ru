<?php

use skewer\build\Component\Site\Type as SiteType;
use \skewer\build\Component\Section\Parameters;


/**
 * Класс, отвечающий за переменные, относящиеся к сайту
 */
class Site {

    /**
     * Отдает название сайта
     * @return string
     */
    public static function getSiteTitle() {

        return Parameters::getValByName(
            \Yii::$app->sections->languageRoot(),
            Parameters::settings, 'site_name', true
        );
    }

    /**
     * Отдает email админа
     * @return string
     */
    public static function getAdminEmail(){
        return Parameters::getValByName( \Yii::$app->sections->root(), Parameters::settings, 'email', false );
    }

    /**
     * Отдает email для отправки
     * @return string
     */
    public static function getNoReplyEmail(){
        return Parameters::getValByName( \Yii::$app->sections->root(), Parameters::settings, 'send_email', false );
    }


    /**
     * Отдает текущую версию сборки в виде 3.19.3(dev)
     *      при BUILDNUMBER = 0019m3(dev)
     * @return string
     */
    public static function getCmsVersion() {
        $sVersion = BUILDNUMBER;
        $sVersion = ltrim($sVersion,'0');
        $sVersion = preg_replace( '/^(\d+)m(\d+.*)/', '$1.$2', $sVersion );
        return sprintf('3.%s [%s]', $sVersion, SiteType::getAlias());
    }

    /**
     * Отдает основной домен для площадки <br />
     * Example: "www.domain.com"
     * @return string
     */
    public static function domain() {
        return \skewer\build\Tool\Domains\Api::getCurrentDomain();
    }

    /**
     * Отдает основной домен для площадки <br />
     * Example: "http://www.domain.com"
     * @return string
     */
    public static function httpDomain() {
        return 'https://'.\skewer\build\Tool\Domains\Api::getCurrentDomain();
    }

    /**
     * Отадет домен с http и / в конце <br />
     * Example: "http://www.domain.com/"
     * @return string
     */
    public static function httpDomainSlash() {
        return self::httpDomain().'/';
    }

}
