<?php

use yii\web\ServerErrorHttpException;

/**
 * Класс-помошник для работы с именами модулей
 * Class Module
 */
class Module {

    /**
     * Формирует имя класса по укороченному псевдониму
     * @param string $sAlias
     * @param string $sLayer
     * @param string $sType
     * @throws ServerErrorHttpException
     * @return string
     */
    public static function getClass( $sAlias, $sLayer, $sType = 'Module' ) {

        if ( !$sLayer )
            $sLayer = \Layer::PAGE;

        if ( strpos( $sAlias, $sType ) === false ) { // no <Layer>Module on name

            // if namespace
            $iPos = strpos( $sAlias, '\\' );
            if ( $iPos ) {

                if ( strpos( $sAlias, '\\', $iPos+1 ) ) {
                    $sModulePathName = $sAlias;
                } else {
                    list( $sModuleLayer, $sModuleName ) = explode( '\\', $sAlias );
                    $sModulePathName = 'skewer\\build\\'.$sModuleLayer.'\\'.$sModuleName.'\\'.$sType;
                }

            } else {
                $sModuleLayer = $sLayer;
                $sModuleName = $sAlias;
                $sModulePathName = 'skewer\\build\\'.$sModuleLayer.'\\'.$sModuleName.'\\'.$sType;
            }

        } else {

            $sModulePathName = $sAlias;

        }

        return $sModulePathName;

    }

    /**
     * Отдает имя класса по псевдониму или выбрасывает исключение
     * @param string $sAlias
     * @param string $sLayer
     * @param string $sType
     * @throws ServerErrorHttpException
     * @return string
     */
    public static function getClassOrExcept( $sAlias, $sLayer, $sType = 'Module' ) {

        $sModulePathName = self::getClass($sAlias, $sLayer, $sType);

        if ( !class_exists($sModulePathName) )
            throw new ServerErrorHttpException ( "Class [$sModulePathName] not found in files." );

        return $sModulePathName;

    }

}