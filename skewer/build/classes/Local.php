<?php
/**
 * Библиотека для работы с локально вызываемыми модулями
 */
class Local {

    /** допустимый родитель подчиненного класса */
    const validParentClass = 'LocalModulePrototype';

    /** имя параметра для контроллера */
    const keyController = 'ctrl';

    /** суффикс имени модуля */
    const classSuffix = 'LocalModule';

    /**
     * Отдает локальную ссылку на указанный модуль с заданными параметрами
     * @param string $sClassName
     * @param array $aParams
     * @throws Exception
     * @return string
     */
    public static function getLink( $sClassName, $aParams=array() ) {

        // проверка имени модуля
        if ( !preg_match('/^([A-z]+)'.self::classSuffix.'$/',$sClassName,$aMatches) )
            throw new Exception('Local: wrong class name');

        // имя модуля без суфикса
        $sModuleName = $aMatches[1];

        // проверка принадлежности контроллера к родительскому классу
        if ( !self::isValidClass( $sClassName ) )
            throw new Exception('Local: wrong controller layer');

        // убрать из параметров ключевое поле с контроллером, если есть
        if ( isset($aParams[self::keyController]) )
            unset($aParams[self::keyController]);

        $sUrl = sprintf('%s/local/?%s=%s',WEBROOTPATH,self::keyController,$sModuleName);
        foreach ( $aParams as $sKey => $sVal )
            $sUrl .= '&'.urlencode($sKey).'='.urlencode($sVal);

        return $sUrl;

    }

    /**
     * Определяет является ли класс подходящим для работы
     * Отдает true если класс унаследован от нужного родителя
     * @param $sClassName
     * @return bool
     */
    public static function isValidClass( $sClassName ) {
        $oRC = new ReflectionClass($sClassName);
        return $oRC->isSubclassOf( self::validParentClass );
    }

}
