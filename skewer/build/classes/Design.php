<?php

/**
 * @class Design
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author Andy Mitrich, $Author$
 * @version $Revision$
 * @date 27.01.12 16:07 $
 *
 */

class Design {

    /** имя метки для хранения последнего времени обновления */
    const lastUpdatedTime = 'last_updated_time';

    /** версия сайта - обычный сайт */
    const versionDefault = 'default';

    /** путь к директории хранения css файлов */
    const cssDirPath = 'files/css/';

    /** имя директории для хранения изображений дизайнерского режима */
    const imageDirName = 'design';

    /**
     * Лидает надор доступных слоев
     * @return array
     */
    public static function getVersionList() {
        return array(
            self::versionDefault
        );
    }

    /**
     * @static Метод получения значения css-параметра
     * @param string $sGroup Название группы для параметра
     * @param string $sKey Название параметра
     * @param string $sLayer Название слоя
     * @return string|null
     */
    public static function get($sGroup, $sKey, $sLayer = 'default'){

        $aParam = DesignManager::getParam($sGroup.'.'.$sKey, $sLayer);
        if ( $aParam['value'] ){
            $sVal = $aParam['value'];

            // todo #stab_check проверить актуально ли еще
            preg_match('/^\/skewer\/build\/Page\/Main(.+)/', $sVal, $match);
            if (isset($match[1])){
                $path = Yii::$app->getAssetManager()->getBundle('skewer\build\Page\Main\Asset')->baseUrl.$match[1];
                return $path = str_replace("/web/","/",$path);
            }
            return $aParam['value'];
        }
        return null;
    }

    public function getList(){
        return [
            '/skewer/build/Page/Main'=>''
        ];
    }

    /** имя флага активности режима дизайнера */
    protected static $sGlobalFlagName = '__design_mode_active';

    /**
     * Устанавливает глобальных флаг активации дизайнерского режима
     * @static
     *
     */
    public static function setModeGlobalFlag() {
        $_SESSION[self::$sGlobalFlagName] = true;
    }

    /**
     * Сбрасывает флаг активности дизайнерского режима
     */
    public static function unsetModeGlobalFlag() {
        if(isSet($_SESSION[self::$sGlobalFlagName])) unSet($_SESSION[self::$sGlobalFlagName]);
    }

    /**
     * Отдает true, если режим дизайнера активен
     * @static
     * @return bool
     */
    public static function modeIsActive() {
        return ( isset($_SESSION[self::$sGlobalFlagName]) and $_SESSION[self::$sGlobalFlagName] );
    }

    /**
     * Отдает имя директории дизайнерского режима для клиентской части
     * @static
     * @return string
     */
    public static function getDirList() {
        $sDir = '/skewer/build/Design/Frame/';
        return array(
            'jsDir'  => $sDir.'js/design/',
            'cssDir' => $sDir.'css/design/',
        );
    }

    /**
     * По url вычисляет состояние
     * @param string $sUrl
     * @return string
     */
    public static function getVersionByUrl( /** @noinspection PhpUnusedParameterInspection */
        $sUrl ) {
        return self::versionDefault;
    }

    /**
     * Отдает название версии сайта по псевдониму
     * @param $sType
     * @return string
     */
    public static function getVersionTitle( $sType ) {
        switch ( $sType ) {
            case self::versionDefault:
                return 'Обычная версия';
            default:
                return '-не определена-';
        }
    }

    /**
     * Отдает заведомо допустимый тип отображения
     * @param $sType
     * @return string
     */
    public static function getValidVersion( $sType ) {
        if ( !in_array( $sType, self::getVersionList() ) )
            return self::versionDefault;
        else return $sType;
    }

    /**
     * Отдает системный путь к директории дополнительных файлов
     * @return string
     */
    public static function getAddCssDirPath() {
        return WEBPATH.self::cssDirPath;
    }

    /**
     * Отдает имя файла типа add_default.css
     * @param $sViewMode
     * @return string
     */
    public static function getLocalCssFileName( $sViewMode ) {
        return sprintf('add_%s.css', self::getValidVersion($sViewMode) );
    }

    /**
     * Отдает имя файла с директорией типа files/css/add_default.css
     * @param $sViewMode
     * @return string
     */
    public static function getlocalCssFileNameWithDir( $sViewMode ) {
        return self::cssDirPath.self::getLocalCssFileName($sViewMode);
    }

    /**
     * @param string $sViewMode
     * @return string
     */
    public static function getAddCssFilePath( $sViewMode = 'default' ) {
        return WEBPATH.self::getLocalCssFileNameWithDir($sViewMode);
    }

    /**
     * Отдает набор подключенных JS файлов
     * @return array
     */
    public static function getJSFiles() {
        return skLinker::getJSFiles();
    }

    /**
     * Возвращает время последнего обновления
     * @return bool|string
     */
    public static function getLastUpdatedTime(){
        return SysVar::get(self::lastUpdatedTime);
    }

    /**
     * Устанавливает время последнего обновления
     * @param null $sVal желаемое значение (если не указано - time())
     * @return void
     */
    public static function setLastUpdatedTime( $sVal = null ){
        if ( !func_num_args() )
            $sVal = time();
        SysVar::set(self::lastUpdatedTime, (string)$sVal);
    }

    /**
     * Самописный assetmanager
     * @todo сделан временно, чтобы подключать правильно js, потом обязательно переписать на yii AssetManager
     * @return array
     */
    public static function assetJs(){

        $js = skLinker::getJSFiles(false);

        foreach($js as &$item){
            $item['current_path'] = ROOTPATH.substr($item['filePath'],1);
            $item['web_path'] = str_replace('skewer/build/','',$item['filePath']);
            $item['asset_path'] = $item['web_path'];
            $item['asset_file_path'] = WEBPATH.'assets/js'.$item['web_path'];
            $item['line'] = \yii\helpers\Html::tag('script', '', ['type'=>'text/javascript','charset'=>'utf-8','src'=>$item['asset_path']]);
        }
        return $js;
    }

    /**
     * Регистрация ассетов нужного слоя
     * @param \yii\web\View $view
     * @param string $sLayer
     */
    public static function assetsRegister( \yii\web\View $view, $sLayer = \Layer::PAGE ){

        $oInstaller = new \skewer\build\Component\Installer\Api();

        $aModules = $oInstaller->getInstalledModules( $sLayer );

        if (count($aModules)){
            foreach( $aModules as $oModule ){

                /** @var yii\web\AssetBundle $sAssets имя Asset класса для модуля */
                $sAssets = \Module::getClass( $oModule->moduleName, $sLayer, 'Asset');

                if ($sAssets && class_exists($sAssets) && in_array( 'yii\web\AssetBundle', class_parents($sAssets)))
                    $sAssets::register( $view );

            }
        }

    }
}