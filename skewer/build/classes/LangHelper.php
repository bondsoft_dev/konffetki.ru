<?php

/**
 * @deprecated Класс для работы с языками в шаблонах Twig, в php файлах не должен быть использован!
 * Class Lang
 */
class LangHelper {

    /**
     * Возвращает значение ключа из языковых шаблонов
     * @param $key
     * @param mixed [$mVal] значения по принциапу sprintf
     * @return string
     */
    public static function get($key){

        if (!$key)
            return '';

        if (($iPos = strpos($key, '.')) !== false){
            $sCategory = substr($key, 0, $iPos);
            $sMessage = substr($key, $iPos+1);

            $aParam = func_get_args();
            array_shift($aParam);

            return \Yii::t($sCategory, $sMessage, $aParam);
        }

        return $key;

    }

}