<?php

/**
 * Класс для работы с html кодом
 * Class Html
 */
class Html {

    /**
     * Отпределяет если ли контент в заданном тексте - срезает тэги и смотрит на оставшееся
     * @param string $sText
     * @return bool
     */
    public static function hasContent($sText) {
        return str_replace('&nbsp;', '', trim( strip_tags( $sText, '<img>' ) ));
    }

}