<?php
/**
 * @todo Рассмотеть актуальность использования, возможно все решается через средствами yii
 * Класс helper для вывода системных и сервисных данных в шаблонах
 * @class Vars
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project Skewer
 * @package Build
 *
 */
class Vars {

    /**
     * Возвращает текущую дату в указанном формате
     * @param  string $sFormat Формат возвращаемого значения
     * @return string
     */
    public function getDate($sFormat) {

        return date($sFormat);
    }// func

    /**
     * Заменяет временнЫе метки в строке
     * @param $sIn
     * @return mixed
     */
    public function parseDateLabels( $sIn ) {
        return str_replace('[Year]', date('Y'),$sIn);
    }

    /**
     * Возвращает базовый URL
     * @return string|bool
     */
    public function baseUrl() {

        return WEBROOTPATH;
    }// func
}// class
