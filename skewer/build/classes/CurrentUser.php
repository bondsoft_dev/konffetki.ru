<?php
/**
 *
 * @class CurrentUser
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project Skewer
 * @package Build
 */
class CurrentUser extends CurrentUserPrototype {

protected static $sLayer = 'public';

}// class
