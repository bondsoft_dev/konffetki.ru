<?php
use yii\base\UserException;

/**
 *
 * @class CurrentAdmin
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project Skewer
 * @package Build
 */
class CurrentAdmin extends CurrentUserPrototype {

    protected static $sLayer = 'admin';

    /**
     * Если текущий пользователь - системный администратор возвращает true
     * @static
     * @return bool
     */
    public static function isSystemMode() {

        return (isSet($_SESSION['auth'][self::$sLayer]['userData']['systemMode']) AND
            $_SESSION['auth'][self::$sLayer]['userData']['systemMode'])? true: false;

    }// func

    /**
     * Возвращает id стартового раздела текущей админской политики
     * @static
     * @return bool
     */
    public static function getMainSection() {
        return Auth::getMainSection( self::$sLayer );
    }

    /**
     * Возвращает true, если текущий пользователь системы является администратором либо системным администратором
     * или false в противном случае
     * @return bool
     */
    public static function isAdminPolicy(){

        if(self::isSystemMode()) return true;

        return (isSet($_SESSION['auth'][self::$sLayer]['userData']['policyAlias']) AND
            $_SESSION['auth'][self::$sLayer]['userData']['policyAlias'] == 'admin')? true: false;
    }// func

    public static function canRead($sectionId){

        if(static::isSystemMode()) return true;

        return Auth::isReadable(static::$sLayer, $sectionId);

    }// func

    public static function getReadableSections() {

        if(static::isSystemMode()) {

            $out = [];
            $sections = \skewer\models\TreeSection::find()->all();
            foreach ( $sections as $section )
                $out[] = $section->id;

            return $out;

        }
        return (isSet($_SESSION['auth'][static::$sLayer]['read_access']))? $_SESSION['auth'][static::$sLayer]['read_access']: false;

    }// func

    public static function getAvailableModules() {

        return Auth::getAvailableModules(static::$sLayer);
    }

    /**
     * Проверяет права доступа к контрольной панели и если их нет - выкидывает исключение
     * @static
     * todo вынести в проверку в родительский класс модулей
     * @throws UserException
     */
    public static function testControlPanelAccess() {
        // todo #namespace
        if ( !self::isSystemMode() and !CurrentAdmin::canDo('skewer\\build\\Tool\\Policy\\Module','useControlPanel') )
            throw new UserException('Access denied');
    }

    /**
     * Проверяет можно ли использовать в дизайнерский режим
     * @static
     */
    public static function allowDesignAccess() {
        // todo #namespace
        return self::isSystemMode() or CurrentAdmin::canDo('skewer\\build\\Tool\\Policy\\Module','useDesignMode');
    }

    public static function testUsedModule( $sModuleName ){

        if ( !self::isSystemMode() && !CurrentAdmin::canUsedModule( $sModuleName ) )
            throw new UserException('Access denied!');

    }

    /**
     * @inheritDoc
     */
    public static function logout()
    {
        \Yii::$app->i18n->admin->clearLang();
        return parent::logout();
    }


}// class
