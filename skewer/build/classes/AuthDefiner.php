<?php
/**
 * 
 * @class AuthDefiner
 *
 * @author Artem, $Author$
 * @version $Revision$
 * @date $Date$
 * @project JetBrains PhpStorm
 * @package Kernel
 */ 
class AuthDefiner extends FirewallCallbackPrototype {

    /**
     * Action for Firewall. Возвращает Id пользователя, который должен быть загружен в случае срабатывания фильтров.
     * @param $iWantedUserId
     * @return mixed
     */
    public function defineDefaultUserId($iWantedUserId) {
        return $iWantedUserId;
    }

}// class
