<?php

use skewer\build\Component\SEO;
use Symfony\Component\Yaml\Yaml;
use skewer\models\Log;

/**
 * Инструменты площадки, разрешенные к удвленному запуску
 * @class HostService
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project JetBrains PhpStorm
 * @package kernel
 */
class HostTools extends ServicePrototype {

    /**
     * Возвращает статус площадки
     * @return string
     */
    public function getStatus() {

        $aOut['status'] = '200OK';
        $aOut['host']   = $_SERVER['HTTP_HOST'];
        $aOut['build_number'] = BUILDNUMBER;
        $aOut['build_name']  = BUILDNAME;
        return Yaml::dump($aOut);

    }// func

    /**
     * Устанавливает патч
     * @param string $sPatchFile путь к исполняемому файлу патча относительно корня директории обновлений
     * @throws skGatewayExecuteException
     * @return bool|array true/false/массив для отправки
     */
    public function installPatch($sPatchFile) {

        try {

            $oUpdateHelper = new skUpdateHelper();
            $mResult = $oUpdateHelper->installPatch( $sPatchFile );

        } catch(UpdateException $e) {

            /* что-то пошло не так. Файл обновления имеет неверный формат либо не достаточно входных параметров */
            throw new skGatewayExecuteException($e->getMessage());

        }

        return $mResult;
    }// func

    /**
     * Запуск после обновления
     * @param bool $bClear
     * @return bool
     */     
    public function UpdateComplete($bClear = true) {
       // todo при разпиле сделать два метода - для сознания новой площадки и для переноса текущей
        // clear
        if($bClear) {

            Log::deleteAll(); // чистим таблицу с логами

            /**
             * @fixme тут была чистка таблицы задач. Что делать в этом случае????
             */

            //\skewer\build\Tool\Domains\Mapper::clearLog(); // todo закоментировать в транке
            \skewer\build\Tool\Subscribe\Api::clearPostingLog();
		}
		
        // upd
        \skewer\build\Tool\Redirect301\Api::makeHtaccessFile();
        SEO\Service::updateSiteMap();
        SEO\Service::updateRobotsTxt( \skewer\build\Tool\Domains\Api::getMainDomain() );

        return true;
    }// func


    /**
     * @param $sTime
     * @return bool
     */
    public function updTimeBackup($sTime) {

        \skewer\build\Tool\Backup\Api::updBackupTime($sTime);

        return true;
    }


    public function updRobotTxt($sDomain = false){

        SEO\Service::updateRobotsTxt($sDomain);

        return true;
    }


    public function syncDomain($aDomains){

        \skewer\build\Tool\Domains\Api::syncDomains($aDomains);

        return true;
    }

    /**
     * Изменение пароля для политики admin
     * @param $sNewPass
     * @return bool|int
     */
    public function replaceAdmPass($sNewPass){

        $iLoginId = \skewer\build\Tool\Users\Api::getIdByLogin( 'admin' );

        if(!$iLoginId)
            return false;

        $aSaveArr = array(
            'id' => $iLoginId,
            'login' => 'admin',
            'pass' => Auth::buildPassword('admin',$sNewPass)
        );

        $bRes = \skewer\build\Tool\Users\Api::updUser( $aSaveArr );

        return $bRes;
    }
    
    /**
     * Разрещает либо запрещает работу процессоров в
     * @param bool $bEnabled
     * @return bool Возвращает true в случае успешной смены состояния процессоров. В случае если данное состояние к
     * процессорам уже применено, то будет возвращено false
     */
    public function enableProcessor($bEnabled = true) {

        if ( (bool)\SysVar::get('ProcessorEnable') === (bool)$bEnabled )
            return false;
        return \SysVar::set('ProcessorEnable', $bEnabled);

    }// func    

}// class
