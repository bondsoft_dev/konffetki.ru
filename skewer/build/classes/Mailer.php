<?php




/**
 * Класс для работы с письмами
 * Class Mailer
 */
class Mailer{

    private $aParams = array();

    private static $oInstance = null;

    /**
     * @return Mailer
     */
    private static function getInstance() {

        if (is_null(static::$oInstance)){
            static::$oInstance = new Mailer();
        }
        return static::$oInstance;

    }

    /**
     * Конвертирует строку
     * @param $sString
     * @return mixed
     */
    private function idnToUtf8($sString){

        if (function_exists('idn_to_utf8'))
            return idn_to_utf8($sString);

        /**
         * @todo найти библиотеку для декодирования
         */

        return $sString;

    }

    private function __construct(){

        $sDomain = \Site::domain();

        $sDomain = $this->idnToUtf8($sDomain);

        $sSiteName = \Site::getSiteTitle();

        $aValues = \Yii::$app->getI18n()->getValues('app', 'site_label');
        foreach ($aValues as $sLabel){
            $this->aParams[$sLabel] = $sSiteName;
        }
        $aValues = \Yii::$app->getI18n()->getValues('app', 'url_label');
        foreach ($aValues as $sLabel){
            $this->aParams[$sLabel] = $sDomain;
        }
        $this->aParams['site'] = $sDomain;
    }

    /**
     * Отправка письма на почту администратору сайта
     * @param string $sSubject Заголовок
     * @param string $sBody Тело письма
     * @param array $aParams Параметры для замены
     * @param string $sMailFrom Email отправителя
     * @param string $sEncoding Кодировка
     * @return bool
     */
    public static function sendMailAdmin( $sSubject, $sBody, $aParams = array(), $sMailFrom = '', $sEncoding = 'utf-8' ){

         return static::sendMail( \Site::getAdminEmail(), $sSubject, $sBody, $aParams, $sMailFrom, $sEncoding );

    }

    /**
     * Отправка письма
     * @param string $sMailTo Email, на который уходит письмо
     * @param string $sSubject Заголовок
     * @param string $sBody Тело письма
     * @param array $aParams Параметры для замены
     * @param string $sMailFrom Email отправителя
     * @param string $sEncoding Кодировка
     * @return bool
     */
    public static function sendMail( $sMailTo, $sSubject, $sBody, $aParams = array(), $sMailFrom = '', $sEncoding = 'utf-8' ){

        $oMailer = static::getInstance();

        return $oMailer->send( $sMailTo, $sSubject, $sBody, $aParams, $sMailFrom, $sEncoding );
    }

    /**
     * Отправка письма
     * @param string $sMailTo Email, на который уходит письмо
     * @param string $sSubject Заголовок
     * @param string $sBody Тело письма
     * @param array $aParams Параметры для замены
     * @param array $aAttach Прикрепленные файлы
     * @param string $sMailFrom Email отправителя
     * @param string $sEncoding Кодировка
     * @return bool
     */
    public static function sendMailWithAttach( $sMailTo, $sSubject, $sBody, $aParams = array(), $aAttach = array(), $sMailFrom = '', $sEncoding = 'utf-8' ){

        $oMailer = static::getInstance();

        return $oMailer->sendWithAttach( $sMailTo, $sSubject, $sBody, $aParams, $aAttach, $sMailFrom, $sEncoding );
    }

    /**
     * Отправка писем по рассылке
     * @param $sMailTo
     * @param $sSubject
     * @param $sBody
     * @param $aParams
     * @param string $sMailFrom
     * @param string $sMailPerson
     * @return bool
     */
    public static function sendReadyMail( $sMailTo, $sSubject, $sBody, $aParams, $sMailFrom = '', $sMailPerson = '' ){

        $oMailer = static::getInstance();

        return $oMailer->sendReady( $sMailTo, $sSubject, $sBody, $aParams, $sMailFrom , $sMailPerson );
    }

    /**
     * Замена меток в письмах
     * @param $sText
     * @param array $aValues
     * @return mixed
     */
    private function parse( $sText , $aValues = array() ){

        $aValues = array_merge($aValues, $this->aParams);

        foreach( $aValues as $sKey => $sValue){
            $sText = str_replace( '[' . $sKey . ']', $sValue, $sText);
        }

        return $sText;
    }

    /**
     * Отправка письма
     * @param string $sMailTo Email, на который уходит письмо
     * @param string $sSubject Заголовок
     * @param string $sBody Тело письма
     * @param array $aParams Параметры для замены
     * @param string $sMailFrom Email отправителя
     * @param string $sEncoding Кодировка
     * @return bool
     */
    private function send( $sMailTo, $sSubject, $sBody, $aParams = array(), $sMailFrom = '', $sEncoding = 'utf-8' ){

        $sBody = $this->parse( $sBody, $aParams);
        $sSubject = $this->parse( $sSubject, $aParams);

        $sMailFrom = $sMailFrom ? $sMailFrom : static::getEmail4Send();

        return \skMailer::sendMail($sMailTo, $sMailFrom, $sSubject, $sBody, $sEncoding);
    }

    /**
     * Отправка письма
     * @param string $sMailTo Email, на который уходит письмо
     * @param string $sSubject Заголовок
     * @param string $sBody Тело письма
     * @param array $aParams Параметры для замены
     * @param array $aAttach Прикрепленные файлы
     * @param string $sMailFrom Email отправителя
     * @param string $sEncoding Кодировка
     * @return bool
     */
    private function sendWithAttach( $sMailTo, $sSubject, $sBody, $aParams = array(), $aAttach = array(), $sMailFrom = '', $sEncoding = 'utf-8' ){

        $sBody = $this->parse( $sBody, $aParams);
        $sSubject = $this->parse( $sSubject, $aParams);

        $sMailFrom = $sMailFrom ? $sMailFrom : static::getEmail4Send();

        return \skMailer::sendMailWithAttach($sMailTo, $sMailFrom, $sSubject, $sBody, $aAttach, $sEncoding);
    }

    /**
     * Отправка писем по рассылке
     * @param $sMailTo
     * @param $sSubject
     * @param $sBody
     * @param $aParams
     * @param string $sMailFrom
     * @param string $sMailPerson
     * @return bool
     */
    private function sendReady( $sMailTo, $sSubject, $sBody, $aParams, $sMailFrom = '', $sMailPerson = '' ){

        $sBody = $this->parse( $sBody, $aParams);
        $sSubject = $this->parse( $sSubject, $aParams);

        $sMailFrom = $sMailFrom ? $sMailFrom : \Site::domain();

        $aMail = \skMailer::getMail($sSubject, $sBody, $sMailFrom, $sMailPerson);
        return \skMailer::sendReadyMail( $aMail, $sMailTo );

    }

    /**
     * Отдает email для поля "От кого" в письмах
     *
     * Пытается взять 3:.:'send_mail' из базы, если нет/не валидный - отдает системный
     * @throws skException
     * @return string
     */
    public static function getEmail4Send() {
        $sMailFrom = \Site::getNoReplyEmail();
        if ( !skValidator::isEmail($sMailFrom) )
            $sMailFrom = \Yii::$app->params['notifications']['noreplay_email'];
        if ( !$sMailFrom )
            throw new skException('No email fo "From" field found in Parameters and Config');
        return $sMailFrom;
    }

}