<?php

/**
 * Класс для сброса файлового кэша
 * Есть набор констант, они задают какие именно подсистемы нужно обновить
 */
class CacheUpdater {

    /** флаг обновления конфигов модулей */
    const type_config = 1;

    /** флаг активации debug режима для Twig */
    const type_parser = 2;

    /**
     * флаг обновления css файлов
     * Будут обновлены только файлы по существующим меткам.
     * Чтобы запустить обновление меток - нужно добавить флаг type_assets
     */
    const type_css = 4;

    /** флаг обновления asset файлов - стираются все файлы */
    const type_assets = 8;

    /** флаг обновить все */
    const type_all = 15;

    /** имя переменной для флага сброса кэша */
    protected static $UpdFlag = '__cache_upd_flag';

    /**
     * Отдает флаг сброса кэша
     * @return bool
     */
    public static function hasUpdFlag() {
        return isset( $_SESSION[ static::$UpdFlag ] );
    }

    /**
     * Отдает значение флага для обновления
     * @return int
     */
    public static function getFlagVal(){
        return isset( $_SESSION[ static::$UpdFlag ] ) ? (int)$_SESSION[ static::$UpdFlag ] : 0;
    }

    /**
     * Отдает true если есть флаг обновления парсера
     * @return int
     */
    public static function hasParserFlag() {
        return (bool)(self::getFlagVal() & self::type_parser);
    }

    /**
     * Отдает true если есть флаг обновления конфигов модулей
     * @return int
     */
    public static function hasConfigFlag() {
        return (bool)(self::getFlagVal() & self::type_config);
    }

    /**
     * Отдает true если есть флаг обновления css файлов
     * @return int
     */
    public static function hasCssFlag() {
        return (bool)(self::getFlagVal() & self::type_css);
    }

    /**
     * Отдает true если есть флаг обновления asset файлов
     * @return int
     */
    public static function hasAssetFlag() {
        return (bool)(self::getFlagVal() & self::type_assets);
    }

    /**
     * Устанавливает флаг сброса кэша
     * Если флаги уже стояли, то новый будет суммирован с предыдущим
     * @param int $iFlag набор битовых флагов для обновления
     */
    public static function setUpdFlag( $iFlag = self::type_all ) {
        $_SESSION[ static::$UpdFlag ] = self::getFlagVal()|$iFlag;
    }

    /**
     * Сбрасывает флаг сброса кэша
     * @return bool
     */
    public static function unsetUpdFlag() {
        if ( isset( $_SESSION[ static::$UpdFlag ] ) )
            unset( $_SESSION[ static::$UpdFlag ] );
    }

}