<?php

/**
 * @todo in app
 */

$aLang = array();

/* сообщения для общесистемных API */
/* работа с параметрами */
$aLang['ParamNotSaved']         = 'Error: Параметр [name: %s, section: %d] не сохранен!';
$aLang['ParamNotDeleted']       = 'Error: Параметр [id: %d] не удален!';
$aLang['ParamsWrongData']       = 'Error: Параметр [name: %s, section: %d] не может быть сохранен - неверные параметры!';
$aLang['ParamNotFound']         = 'Error: Параметр [name: %s, section: %d] не найден!';

/*работа с файловой системой*/
$aLang['FolderNotCreated']      = 'Error: Директория [path: %s] не создана!';
$aLang['FolderNotMoved']        = 'Error: Директория [of: %s to: $s] не перемещена!';
$aLang['FolderNotRemoved']      = 'Error: Директория [%s] не удалена!';
$aLang['FileNotRemoved']        = 'Error: Файл [%s] не удален!';
$aLang['FileNotFound']          = 'Error: Файл [%s] не найден!';

/*астрактные ответы*/
$aLang['WrongData']             = 'Error: Неверные параметры!';

/*Ошибки парсинга*/
$aLang['ParseError']             = 'Error обработки шаблона!';

/* сообщения, используемые при установке модулей и обновлении площадок  */
$aLang['updateError_wrongConstrData']       = 'Error: Система обновлений не может быть запущена! Переданы неверные параметры.';
$aLang['updateError_NoConstrData']          = 'Error: Система обновлений не может быть запущена! Отсутствуют обязательные параметры [%s].';
$aLang['updateError_sqlfile_notfound']      = 'Error: SQL Файл [%s] не найден!';
$aLang['updateError_wrong_query']           = 'Error: SQL Файл не выполнен с ошибкой: [%s]';
$aLang['updateError_tplNotFound']           = 'Error: Шаблонный раздел [%d] не найден!';
$aLang['updateError_sectionNotAdded']       = 'Error: Раздел [%s] не добавлен!';
$aLang['updateError_AliasNotFound']         = 'Error: Раздел с псевдонимом [%s] не найден!';
$aLang['updateError_SectionNotDeleted']     = 'Error: Раздел [%s] не удален!';
$aLang['updateError_storageNotFound']       = 'Error: Хранилище [%s] реестра не найдено!';

/* Сообщения, используемые в файлах обновлений (патчах) */
$aLang['patchError_wrongConstrData']        = 'Отсутствуют необходимые параметры запуска обновления!';

return $aLang;
