<?php

namespace skewer\build\Page\GuestBook;


use skewer\build\Adm\GuestBook\ar;
use skewer\build\Component\orm;
use skewer\build\Tool\Review\Api;

class Form extends orm\FormRecord {

    public $name = '';
    public $email = '';
    public $city = '';
    public $content = '';

    public $captcha = '';

    public $cmd = 'sendNewComment';
    public $parent = 0;
    public $parent_class = '';
    public $rating = 0;

    public function rules() {
        return array(
            array( array('name','email','content','captcha'), 'required', 'msg'=>\Yii::t('forms', 'err_empty_field' ) ),
            array( array('email',), 'email', 'msg'=>\Yii::t('forms', 'err_incorrect_email' ) ),
            array( array('captcha'), 'captcha' ),
        );
    }

    public function getLabels() {
        return array(
            'name' => \Yii::t('review', 'field_name' ),
            'email' => \Yii::t('review', 'field_email' ),
            'city' => \Yii::t('review', 'field_city' ),
            'content' => \Yii::t('review', 'field_comment' ),
            'rating' => \Yii::t('review', 'field_rating' ),
        );
    }

    public function getEditors() {
        return array(
            'password' => 'password',
            'rating' => 'hidden',
            'cmd' => 'hidden',
            'captcha' => 'captcha',
            'comment' => 'textarea',
            'parent' => 'hidden',
            'parent_class' => 'hidden',
        );
    }

    public function sendComment() {

        $aData = $this->getData();
        foreach( $aData as &$mValue )
            $mValue = strip_tags( $mValue );

        $oRow = new ar\GuestBookRow();
        $oRow->setData( $aData );
        $oRow->date_time = date( 'Y-m-d H:i:s' );

        $oRow->status =  ar\GuestBook::statusNew;

        if ( $oRow->save() ) {

            Api::sendMailToClient( $oRow );
            Api::sendMailToAdmin( $oRow );


        } else {
            $this->addError( 'Ошибка! Данные не сохранены' );
        }

        return true;
    }

}