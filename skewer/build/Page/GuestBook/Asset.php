<?php

namespace skewer\build\Page\GuestBook;

use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle {
    
    public $sourcePath = '@skewer/build/Page/GuestBook/web/';

    public $css = [
        'css/custom.css'
    ];

    public $js = [
        'js/jquery.rating.js',
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public $depends = [
    ];

}