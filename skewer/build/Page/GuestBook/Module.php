<?php

namespace skewer\build\Page\GuestBook;

use skewer\build\Adm\GuestBook\ar;
use skewer\build\Component\I18N\ModulesParams;
use skewer\build\Component\Section\Tree;

/**
 * Пользовательский модуль отзывов
 * Class Module
 * @package skewer\build\Page\GuestBook
 */
class Module extends \skModule {

    public $iSectionId;
    public $hideTitle;
    public $altTitle = '';

    public $objectId;
    public $className = '';
    public $actionForm = '';

    public $showOnMain = 0;
    public $showList = false;

    public $titleOnMain= '';
    public $maxLen = 500;
    public $section_id;

    public $onPage = 10;

    public $revert = 0;
    public $rating = 0;

    public function init() {

        $this->iSectionId = $this->getEnvParam('sectionId');

        $this->setParser(parserTwig);

        $this->rating = ModulesParams::getByName('review', 'rating');
    }

    protected function dateToText($sDate) {

        $arr = explode(' ', $sDate);
        $res = array();
        $res['date'] = $arr[0];
        $res['time'] = $arr[1];
        list($res['year'], $res['month'], $res['day']) = explode('-', $arr[0]);
        list($res['hour'], $res['min'], $res['sec']) = explode(':', $arr[1]);

        //$sNewDate = $res['day'] . ' ' . \Yii::t('app', 'month_' . $res['month']) . ' ' . $res['year'];
        $sNewDate = $res['day'] . '.' . $res['month'] . '.' . $res['year'];
        return $sNewDate;
    }

    /**
     * Вывод формы
     */
    public function showForm() {

        $oCommentForm = new Form();

        $this->setData('revert',$this->revert);

        if ($oCommentForm->load($this->getPost()) && $oCommentForm->isValid() && $oCommentForm->sendComment()) {

            $this->setData('msg', \Yii::t('review', 'send_msg')); // todo нужное сообщение
            $this->setData('back_link', 1);


        } else {

            $aParams = array(
                'show_rating' => $this->rating,
                'page' => $this->iSectionId,
                'url' => $this->actionForm
            );

            $oCommentForm->parent = $this->objectId ? $this->objectId : $this->iSectionId;
            $oCommentForm->parent_class = $this->className;

            $sTpl = 'form.twig';

            $this->setData('form', $oCommentForm->getForm($sTpl, __DIR__, $aParams));
        }

    }


    public function execute() {

        // номер текущей страницы
        $iPage = $this->getInt('page', 1);

        $this->setData('show_rating', $this->rating);

        /**
         * Вывод списка для правой/левой колонки
         */
        if ( $this->showList ) {

            $aData = ar\GuestBook::find()
                ->where('status', 1)
                ->where('on_main', 1);

            /**
             * Если указан раздел - тащим из раздела, если нет - по всему сайту
             */
            $this->section_id = (int)$this->section_id;
            if ($this->section_id){
                $aData->where('parent',$this->section_id)
                    ->where('parent_class','');
            }else{
                $aSections = Tree::getAllSubsection(\Yii::$app->sections->languageRoot());
                if ( $aSections ) {
                    $aData->whereRaw("(parent_class != '' OR parent IN (".implode(', ', $aSections).'))');
                }
            }

            $aData = $aData->order('date_time', 'DESC')
                ->limit($this->onPage)
                ->asArray()
                ->getAll();

            if (!empty($aData)){

                $this->setData('title', \Yii::t('review', $this->titleOnMain));
                $this->setData('items', $aData);
                $this->setData('maxLen',$this->maxLen);

                if ($this->section_id){
                    $this->setData('section_id', $this->section_id);
                }
            }

            $this->setTemplate('main.twig');

        } else {

            $count = 0;
            $aData = ar\GuestBook::find()
                ->where('status', 1)
                ->where('parent', $this->objectId ? $this->objectId : $this->iSectionId)
                ->where('parent_class', $this->className)
                ->limit($this->onPage, ($iPage - 1) * $this->onPage)
                ->setCounterRef($count)
                ->order('date_time', 'DESC')
                ->asArray()
                ->getAll();

            // задать шаблон
            $this->setTemplate('view.twig');

            // расставить отформатированииые даты в записях
            foreach ($aData as &$aItem) { // todo перенести в Row

                $aItem['date_time'] = $this->dateToText($aItem['date_time']);
                $aItem['content'] = nl2br(strip_tags( $aItem['content']));
            }

            // задать данные для парсинга
            $this->setData('items', $aData);

            $this->showForm();

            // ссыки постраничного
            $this->getPageLine($iPage, $count, $this->iSectionId, array(), array('onPage' => $this->onPage));

        }

        return psComplete;
    }

}