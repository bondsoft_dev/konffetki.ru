<?php

namespace skewer\build\Page\GuestBook;

/**
 * @class GuestBookInstall
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author kolesnikov, $Author:  $
 * @version $Revision: $
 * @date $
 *
 */

class Install extends \skModuleInstall {

    public function init() {
        return true;
    }// func

    public function install() {
        return true;
    }// func

    public function uninstall() {
        return true;
    }// func

}//class
