<?php

$aLanguage = array();

$aLanguage['ru']['GuestBook.Page.tab_name'] = 'Отзывы';
$aLanguage['ru']['answer_title_template'] = 'Отзыв с сайта';
$aLanguage['ru']['send_msg'] = 'Ваш комментарий успешно отправлен';
$aLanguage['ru']['field_name'] = 'ФИО';
$aLanguage['ru']['field_email'] = 'E-mail';
$aLanguage['ru']['field_city'] = 'Город';
$aLanguage['ru']['field_comment'] = 'Ваше мнение';
$aLanguage['ru']['field_rating'] = 'Оценка';
$aLanguage['ru']['answer_body_template'] = 'На адрес сайта оставлен новый отзыв.';
$aLanguage['ru']['answer_body_template_client'] = 'Спасибо за ваш отзыв. После прохождения модерации он появится на сайте';
$aLanguage['ru']['revert'] = 'Текст над формой в отзывах';
$aLanguage['ru']['settings_rating'] = 'Выводить оценку';

$aLanguage['en']['GuestBook.Page.tab_name'] = 'Guestbook';
$aLanguage['en']['answer_title_template'] = 'Review from site';
$aLanguage['en']['send_msg'] = 'Your comment has been successfully sent';
$aLanguage['en']['field_name'] = 'You name';
$aLanguage['en']['field_email'] = 'E-mail';
$aLanguage['en']['field_city'] = 'City';
$aLanguage['en']['field_comment'] = 'Comment';
$aLanguage['en']['field_rating'] = 'Rating';
$aLanguage['en']['answer_body_template'] = 'On the website address by a new review.';
$aLanguage['en']['answer_body_template_client'] = 'On the website address by a new question.';
$aLanguage['en']['revert'] = 'Change text and form';
$aLanguage['en']['settings_rating'] = 'Show rating';

return $aLanguage;