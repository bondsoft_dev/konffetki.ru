<?php

/* main */
$aConfig['name']     = 'Languages';
$aConfig['title']    = 'Переключатель языков';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Переключатель языковых версий';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::PAGE;

return $aConfig;
