<?php

namespace skewer\build\Page\Languages;


use skewer\build\Component\I18N\Languages;
use yii\helpers\ArrayHelper;

class Module  extends \skModule {

    public function init(){

        $this->setParser(parserPHP);

    }

    public function execute(){

        $oMain = $this->getProcess('out');
        if ($oMain->getStatus() != psComplete)
            return psWait;

        $aLanguages = ArrayHelper::index(Languages::getAllActive(), 'name');

        $names = \Yii::$app->sections->getByValue($this->getEnvParam('sectionId'));

        $aLinks = [];
        if ($names){
            $name = array_shift($names);
            $aLinks = \Yii::$app->sections->getValues($name);
        }

        if (isset($aLanguages[\Yii::$app->language]))
            $aLanguages[\Yii::$app->language]['current'] = true;

        $aLangLinks = [];

        foreach( \Yii::$app->sections->getValues('main') as $lang => $main){
            if (isset($aLanguages[$lang]))
                $aLanguages[$lang]['main'] = $main;

            if (isset($aLinks[$lang])){
                $aLangLinks[] = [
                    'hreflang' => $lang,
                    'href' => \Site::httpDomain().\Yii::$app->router->rewriteURL('[' . $aLinks[$lang] . ']')
                ];
            }
        }

        $oMain->setData('aLangLinks' , $aLangLinks);

        $this->setData('Languages', $aLanguages);
        $this->setTemplate('languageSwitch.php');

        return psComplete;
    }
}