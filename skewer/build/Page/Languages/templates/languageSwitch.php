<? if (isset($Languages) && $Languages): ?>
<div class="b-lang <? if (Design::modeIsActive()): ?>g-ramaborder js-designDrag-right" sktag="modules.language"<? endif ?>">
    <? if (Design::modeIsActive()): ?>
        <div class="b-desbtn"><span>Language</span><ins></ins></div>
    <? endif ?>
    <? foreach ($Languages as $lang): ?>
        <div class="lang__item">
            <a class="lang__title <? if (isset($lang['current']) && $lang['current']): ?>lang__title-on<? endif ?>"
               href="<? if (isset($lang['current']) && $lang['current']): ?>javascript:return false;<? else: echo '['.$lang['main'].']'; endif ?>">
                <img alt="" src="<?= $lang['icon'] ?>" />
            </a>
        </div>
    <? endforeach ?>
</div>
<? endif ?>