<?php

namespace skewer\build\Page\Auth;


use skewer\build\Component\orm;


class AuthForm extends orm\FormRecord {

    public $login = '';
    public $password = '';
    public $cmd = 'ShowAuthForm';

    public function rules() {
        return array(
            array( array('login','password'), 'required', 'msg'=>\Yii::t('auth', 'err_empty_field' ) )
        );
    }

    public function getLabels() {
        return array(
            'login' => \Yii::t('auth', 'login_mail' ),
            'password' => \Yii::t('auth', 'password' ),
        );
    }

    public function getEditors() {
        return array(
            'password' => 'password',
            'cmd' => 'hidden'
        );
    }

    public function auth() {

        $bRes = \CurrentUser::login( $this->login, $this->password );

        if ( !$bRes )
            $this->addError( \Yii::t('auth', 'incorrect_login_or_pass' ) );

        return $bRes;
    }
}