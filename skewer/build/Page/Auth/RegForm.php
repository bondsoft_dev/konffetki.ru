<?php

namespace skewer\build\Page\Auth;


use skewer\build\Component\orm;
use skewer\build\Adm\Auth as AuthAdm;
use skewer\build\Component\I18N\ModulesParams;


class RegForm extends orm\FormRecord {

    public $login = '';
    public $password = '';
    public $wpassword = '';
    public $type_user = '';

    public $accepts_the_offer = 0;
    public $captcha = '';

    public $cmd = 'register';

    public function rules() {
        return array(
            array( array('login','password','wpassword','captcha','type_user'), 'required', 'msg'=>\Yii::t('auth', 'err_empty_field' ) ),
            array( array('login'), 'email', 'msg'=>\Yii::t('auth', 'no_email_valid' ) ),
            array( array('password', 'wpassword'), 'minlength', 'length' => 6, 'msg'=>\Yii::t('auth', 'err_short_pass' ) ),
            array( array('wpassword'), 'compare', 'compareField'=>'password', 'msg'=>\Yii::t('auth', 'err_pass_not_mutch' ) ),
            array( array('login'), 'check', 'method'=>'checkLogin', 'msg'=>\Yii::t('auth', 'err_login_exsist' ) ),
            array( array('accepts_the_offer'), 'required', 'msg'=>\Yii::t('auth', 'err_empty_field' ) ),
            array( array('captcha'), 'captcha' ),
        );
    }

    public function getLabels() {
        return array(
            'login' => \Yii::t('auth', 'login_mail' ),
            'password' => \Yii::t('auth', 'password' ),
            'wpassword' => \Yii::t('auth', 'wpassword' ),
            'accepts_the_offer' => \Yii::t('auth', 'accepts_the_offer' ),
            'type_user' => \Yii::t('auth', 'type_user' ),
        );
    }

    public function getEditors() {
        return array(
            'password' => 'password',
            'wpassword' => 'password',
            'accepts_the_offer' => 'checkbox',
            'type_user' => array('select', 'method'=>'getTypeUser'),
            'cmd' => 'hidden',
            'captcha' => 'captcha',
        );
    }


    /**
     * проверка на совпадение логина
     * @return bool
     */
    public function checkLogin() {

        $oItem =  AuthAdm\ar\Users::find()->where( 'login', $this->login )->get();

        if ( $oItem ) {
            $this->setFieldError( 'login', \Yii::t('auth', 'alredy_taken') );
            return false;
        }

        return true;
    }

    public function getTypeUser() {
        return  array(  UserType::TYPE_INDIVIDUAL => UserType::TITLE_INDIVIDUAL,
            UserType::TYPE_ENTITY => UserType::TITLE_ENTITY,
            UserType::TYPE_INDIVID_ENTREP => UserType::TITLE_INDIVID_ENTREP,
        );
    }



    /**
     * Сохранение нового пользователя
     * @return bool
     */
    public function saveUser() {

        if ( $this->password != $this->wpassword )
            return false;

        $oItem = new AuthAdm\ar\UsersRow();

        $oItem->login = $this->login;
        $oItem->pass = $this->password;
        $oItem->type_user = $this->type_user;

        if ( $oItem->validate() ) {

            // генерим токен
            $oItem->active = 0;

            $iStatus = \SysVar::get( 'auth.activate_status' );

            //если автоматическая регистрация, то выставляем сразу активность
            $oItem->active = ( $iStatus == 1 ) ? 1 : 0;

            if ( $oItem->insert() ) {

                $oTicket = new AuthTicket();
                $oTicket->setModuleName('auth');
                $oTicket->setActionName('activate');
                $oTicket->setObjectId($oItem->id);
                $sToken = $oTicket->insert();

                $aParams = array();

                // активация по e-mail
                if ( $iStatus == 2 ) {
                    $sBody = ModulesParams::getByName('auth', 'mail_user_activate');

                    $aParams['link'] = \Site::httpDomain().Api::getAuthPath().'?cmd=AccountActivation&token='.$sToken;

                    \Mailer::sendMail( $oItem->email, ModulesParams::getByName('auth', 'mail_title_mail_activate'), $sBody, $aParams);
                }

                $sBody = ModulesParams::getByName('auth', 'mail_admin_activate');

                $aParams['link'] = \Site::httpDomainSlash().'admin/#out.left.tools=Auth;out.tabs=tools_Auth';

                \Mailer::sendMailAdmin( ModulesParams::getByName('auth', 'mail_title_admin_newuser'), $sBody, $aParams);

                return true;
            }
        };

        return false;
    }
} 