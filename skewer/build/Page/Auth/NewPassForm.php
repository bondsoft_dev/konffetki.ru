<?php

namespace skewer\build\Page\Auth;


use skewer\build\Component\orm;

class NewPassForm extends orm\FormRecord {

    public $login = '';
    public $token = '';
    public $pass = '';
    public $wpass = '';
    public $cmd = 'saveNewPass';

    public function rules() {
        return array(
            array( array('pass'), 'required', 'msg'=>\Yii::t('auth', 'err_empty_field' ) ),
            array( array('wpass'), 'compare', 'compareField'=>'pass', 'msg'=>\Yii::t('auth', 'err_pass_not_mutch' ) ),
            array( array('pass', 'wpass'), 'minlength', 'length' => 6, 'msg'=>\Yii::t('auth', 'err_short_pass' ) ),
        );
    }

    public function getLabels() {
        return array(
            'pass' => \Yii::t('auth', 'new_pass' ),
            'wpass' => \Yii::t('auth', 'wpassword' ),
        );
    }

    public function getEditors() {
        return array(
            'pass' => 'password',
            'wpass' => 'password',
            'login' => 'hidden',
            'token' => 'hidden',
            'cmd' => 'hidden',
        );
    }

} 