<?php

namespace skewer\build\Page\Auth;


use skewer\build\Component\orm;

class RecoverForm extends orm\FormRecord {

    public $login = '';
    public $captcha = '';
    public $cmd = 'recover';

    public function rules() {
        return array(
            array( array('login','captcha'), 'required', 'msg'=>\Yii::t('auth', 'err_empty_field' ) ),
            array( array('captcha'), 'captcha' ),
        );
    }

    public function getLabels() {
        return array(
            'login' => \Yii::t('auth', 'your_email' ),
        );
    }

    public function getEditors() {
        return array(
            //'email' => 'password',
            'cmd' => 'hidden',
            'captcha' => 'captcha',
        );
    }
} 