<?php

namespace skewer\build\Page\Auth;


use \skewer\build\Component\Site;

/**
 *
 *
 * @author kolesnikov, $Author: sapozhkov $
 * @version $Revision: $
 * @date $Date: $
 */
class Install extends \skModuleInstall {

    public function init() {
        return true;
    }// func

    public function install() {

        $iNewPageSection = \Yii::$app->sections->tplNew();

        $this->addParameter( $iNewPageSection, 'object', 'Auth', '', 'auth', '');
        $this->addParameter( $iNewPageSection, 'layout', 'left,right', '', 'auth', '');
        $this->addParameter( $iNewPageSection, 'mini_auth', '1', '', 'auth', '');

        $this->addParameter( $iNewPageSection, 'object', 'Auth', '', 'authHead', '');
        $this->addParameter( $iNewPageSection, 'layout', 'head', '', 'authHead', '');
        $this->addParameter( $iNewPageSection, 'mini_auth', '1', '', 'authHead', '');
        $this->addParameter( $iNewPageSection, 'head', '1', '', 'authHead', '');

        return true;
    }// func

    public function uninstall() {

        $iNewPageSection = \Yii::$app->sections->tplNew();

        $this->removeParameter( $iNewPageSection, 'object', 'auth');
        $this->removeParameter( $iNewPageSection, 'layout', 'auth');
        $this->removeParameter( $iNewPageSection, 'mini_auth', 'auth' );
        $this->removeParameter( $iNewPageSection, 'head', 'auth' );

        $this->removeParameter( $iNewPageSection, 'object', 'authHead');
        $this->removeParameter( $iNewPageSection, 'layout', 'authHead');
        $this->removeParameter( $iNewPageSection, 'mini_auth', 'authHead' );
        $this->removeParameter( $iNewPageSection, 'head', 'authHead' );

        return true;
    }// func

}// class
