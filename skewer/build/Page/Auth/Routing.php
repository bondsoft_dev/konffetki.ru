<?php

namespace skewer\build\Page\Auth;

use skewer\build\Component\Router\RoutingInterface;


class Routing implements RoutingInterface {


    public static function getRoutePatterns() {

        return array(
            '/cmd/',
            '/!response/'
        );
    }// func
}
