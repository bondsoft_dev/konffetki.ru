<?php

namespace skewer\build\Page\Auth;


use skewer\build\Adm\Auth\ar\Users;
use skewer\build\Adm\Auth\ar\UsersRow;
use skewer\build\Component\I18N\ModulesParams;
use skewer\build\Component\Section\Tree;
use skewer\build\Component\Site;
use yii\web\NotFoundHttpException;

/**
 * Модуль регистрации и авторизации
 * Class Module
 * @package skewer\build\Page\Auth
 */
class Module extends \skModule {

    public $mini_auth = 0;

    public $head = false;

    private $authSection = 0;
    
    /**
     * Отдает флаг использования правил разбора url
     * @return boolean
     */
    public function useRouting() {
        return !$this->mini_auth;
    }

    /**
     * Прототип - выполняется до вызова метода Execute
     * @return bool
     */
    public function init()
    {
        parent::init();

        $this->authSection = \Yii::$app->sections->getValue('auth');
    }


    public function execute() {

        $this->setData('page', \Yii::$app->sections->auth());

        $this->setData('profile_url',  Api::getProfilePath());
        $this->setData('auth_url',  Api::getAuthPath());

        if ( \Design::modeIsActive() ) {
            $this->setData('designMode', \Design::getDirList() );
        }

        if ($this->head && $this->mini_auth){
            $this->setTemplate( 'AuthFormMiniHead.twig' );
        }else{
            $this->setTemplate( 'detail.twig' );
        }

        if ($this->mini_auth) {
            $this->actionInit();
            return psComplete;
        }

        $sCmd = $this->getStr( 'cmd' );

        if ( $sCmd ) {

            $sActionName = 'action' . ucfirst( $sCmd );

            if ( method_exists( $this, $sActionName ) )
                $this->$sActionName();
            else {
                throw new NotFoundHttpException();
            }

        } else {
            $this->actionInit();
        }

        return psComplete;
    }


    protected function actionInit() {

        if ( \CurrentUser::isLoggedIn() && \CurrentUser::getPolicyId() != 2 ) {
            $this->setData( 'current_user', \CurrentUser::getUserData() );
        } else {
            $this->actionShowAuthForm();
        }

    }


    /**
     * Выводит форму авторизации
     */
    protected function actionShowAuthForm() {

        $oAuthForm = new AuthForm();

        if ( $oAuthForm->load( $this->getPost() ) && $oAuthForm->isValid() && $oAuthForm->auth() ) {

            $this->setData( 'current_user', \CurrentUser::getUserData() );
            \Auth::reloadPolicy( 'public' );

            \Yii::$app->getResponse()->redirect( Api::getProfilePath() )->send();
            exit;

        } else {

            $aParams = array(
                'page' => $this->authSection,
                'url' => \Yii::$app->router->rewriteURL( "[$this->authSection]" )
            ) ;

            $sTpl = $this->mini_auth ? ($this->head?'AuthFormMiniHead.twig':'AuthFormMini.twig') : 'AuthForm.twig';
            $this->setData( 'forms', $oAuthForm->getForm( $sTpl, __DIR__, $aParams ) );
        }

    }


    /**
     * Регистрация нового пользователя
     */
    protected function actionRegister() {

        $oRegForm = new RegForm();

        if ( $oRegForm->load( $this->getPost() ) && $oRegForm->isValid() && $oRegForm->saveUser() ) {

            $iActivateStatus = \SysVar::get('auth.activate_status');

            if ( $iActivateStatus == 1 ) {

                $this->setData( 'msg', \Yii::t('auth', 'msg_instruct_auth' ) );
                $this->actionShowAuthForm();

            } else {

                $sTitle = \Yii::t('auth', 'head_register');

                Site\Page::setTitle( $sTitle );

                Site\Page::setAddPathItem( $sTitle );

                if ($iActivateStatus == 2){
                    $this->setData( 'msg', \Yii::t('auth', 'msg_instruct_reg' ) );
                    $this->setData('status', 'success');
                }
                elseif ($iActivateStatus == 3){
                    $this->setData( 'msg', \Yii::t('auth', 'msg_instruct_admin' ) );
                }

            }

        } else {

            $sTitle = \Yii::t('auth', 'head_register');

            Site\Page::setTitle( $sTitle );

            Site\Page::setAddPathItem( $sTitle );

            $aParams = array(
                'page' => $this->authSection,
                'url' => \Yii::$app->router->rewriteURL( "[$this->authSection]" ),
                'license_agreement' => ModulesParams::getByName('auth', 'reg_license')
            ) ;

            $this->setData( 'forms', $oRegForm->getForm( 'RegForm.twig', __DIR__, $aParams ) );
        }

    }


    /**
     * Сохранение нового пароля
     */
    protected function actionSaveNewPass() {

        $sPassword = $this->getStr( 'pass' );
        $sWPassword = $this->getStr( 'wpass' );
        $sToken = $this->getStr( 'token' );

        $oTicket =  AuthTicket::get($sToken);
        if ($oTicket && $oTicket->moduleNameIs('auth') && $oTicket->actionNameIs('recover_pass') && $oTicket->getObjectId()){

            $iUserId = $oTicket->getObjectId();
            /** @var UsersRow $oItem */
            $oItem = Users::find()->where( 'id', $iUserId )->getOne();

            $oNewPassForm = new NewPassForm();
            $oNewPassForm->login = $oItem->login;
            $oNewPassForm->token = $sToken;
            $oNewPassForm->pass = $sPassword;
            $oNewPassForm->wpass = $sWPassword;

            if ( !$oItem || !$sToken ) {

                $this->setData( 'msg', \Yii::t('auth', 'msg_error_token' ) );

                $this->actionShowAuthForm();

            } elseif ( !$oNewPassForm->isValid() ) {

                $aParams = array(
                    'page' => $this->authSection,
                    'url' => \Yii::$app->router->rewriteURL( "[$this->authSection]" )
                ) ;

                $sTitle = \Yii::t('auth', 'head_restore');

                Site\Page::setTitle( $sTitle );

                Site\Page::setAddPathItem( $sTitle );

                $this->setData( 'forms', $oNewPassForm->getForm( 'NewPass.twig', __DIR__, $aParams ) );

            } else {

                Api::saveNewPass( $oItem, $sPassword );
                $oTicket->delete($sToken);

                $this->setData( 'msg', \Yii::t('auth', 'msg_new_pass' ) );

                $this->actionShowAuthForm();
            }
        }




    }


    /**
     * Форма ввода нового пароля
     */
    protected function actionNewPassForm() {

        $sToken = $this->getStr( 'token' );
        $oTicket =  AuthTicket::get($sToken);

        $sTitle = \Yii::t('auth', 'head_restore');

        Site\Page::setTitle( $sTitle );

        Site\Page::setAddPathItem( $sTitle );

        if ($oTicket && $oTicket->moduleNameIs('auth') && $oTicket->actionNameIs('recover_pass') && $oTicket->getObjectId()){
            $iUserId = $oTicket->getObjectId();

            /** @var UsersRow $oItem */
            $oItem = Users::find()->where( 'id', $iUserId )->getOne();

            if ( $oItem ) {

                $aParams = array(
                    'page' => $this->authSection,
                    'url' => \Yii::$app->router->rewriteURL( "[$this->authSection]" )
                ) ;

                $oNewPassForm = new NewPassForm();
                $oNewPassForm->login = $oItem->login;
                $oNewPassForm->token = $sToken;

                $this->setData( 'forms', $oNewPassForm->getForm( 'NewPass.twig', __DIR__, $aParams ) );

            } else
                $this->setData( 'msg', \Yii::t('auth', 'msg_error_token' ) );

        } else
            $this->setData( 'msg', \Yii::t('auth', 'msg_error_token' ) );
    }


    /**
     * Восстановление пароля
     */
    protected function actionRecover() {

        $oRecoverForm = new RecoverForm();

        $sLogin = $this->getStr( 'login' );

        $sTitle = \Yii::t('auth', 'restore');

        Site\Page::setTitle( $sTitle );

        Site\Page::setAddPathItem( $sTitle );

        try {

            if ( !$oRecoverForm->load( $this->getPost() ) || !$oRecoverForm->isValid() )
                throw new \Exception( '' );

            $oUser = $sLogin ? Api::getUserByLogin( $sLogin ) : null;

            if ( is_null( $oUser ) || !$oUser )
                throw new \Exception( \Yii::t('auth', 'msg_not_found_user' ) );

            // сгенерим токен и отправим на mail
            if ( !Api::recoverPass( $oUser ) )
                throw new \Exception( '' );

            $this->setData( 'msg', \Yii::t('auth', 'msg_recover_instruct' ) );

            //$this->actionShowAuthForm();

        } catch ( \Exception $e ) {

            // форма востановления пароля
            if ( $e->getMessage() )
                $oRecoverForm->addError( $e->getMessage() );

            $aParams = array(
                'page' => $this->authSection,
                'url' => \Yii::$app->router->rewriteURL( "[$this->authSection]" )
            ) ;

            $this->setData( 'forms', $oRecoverForm->getForm( 'RecoverForm.twig', __DIR__, $aParams ) );
        }

    }


    /**
     * выход из системы
     */
    protected function actionLogout() {

        \CurrentUser::logout();

        /**
         * @fixme перезагрузка политик не отрабатывает как нужно. С политиками надо разбираться. Закрыл пока переадресацией
         */

        \Yii::$app->getResponse()->redirect(Tree::getSectionAliasPath($this->getEnvParam('sectionId')))->send();

    }


    /**
     * Активация аккаунта
     */
    protected function actionAccountActivation() {

        $sToken = $this->getStr( 'token', '' );

        if ( !$sToken )
            throw new NotFoundHttpException();

        $oTicket = AuthTicket::get($sToken);

        if (!$oTicket){
            throw new NotFoundHttpException();
        }

        if ($oTicket && $oTicket->moduleNameIs('auth') && $oTicket->actionNameIs('activate') && $oTicket->getObjectId()){
            $iUserId = $oTicket->getObjectId();

            /**
             * @var UsersRow $oUser
             */
            $oUser = Users::find()->where( 'id', $iUserId )->where( 'active', 0 )->getOne();

            if ( !$oUser )
                throw new NotFoundHttpException();

            if ( Api::accountActivate( $oUser ) ){

                $oTicket->delete($sToken);

                $this->setData( 'msg', \Yii::t( 'auth', 'msg_verifed' ) );

                $oAuthForm = new AuthForm();

                $aParams = array(
                    'page' => $this->authSection,
                    'url' => \Yii::$app->router->rewriteURL( "[$this->authSection]" )
                ) ;

                $sTpl = 'AuthForm.twig';
                $this->addCSSFile( 'authmain.css' );
                $this->setData( 'forms', $oAuthForm->getForm( $sTpl, __DIR__, $aParams ) );

            } else {
                throw new NotFoundHttpException();
            }
        } else {
            throw new NotFoundHttpException();
        }

    }

} 
