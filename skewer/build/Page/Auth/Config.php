<?php

/* main */
$aConfig['name']     = 'Auth';
$aConfig['title']    = 'Регистрация';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Модуль Регистрации';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::PAGE;
$aConfig['languageCategory']     = 'auth';

$aConfig['dependency'] = [
    ['Profile', \Layer::PAGE],
    ['Auth', \Layer::TOOL],
    ['Auth', \Layer::ADM],
];


return $aConfig;
