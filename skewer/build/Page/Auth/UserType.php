<?php
/**
 * Created by PhpStorm.
 * User: lukas
 * Date: 18.03.2016
 * Time: 15:18
 */

namespace skewer\build\Page\Auth;


class UserType
{
    const TYPE_INDIVIDUAL = 1;
    const TYPE_ENTITY = 2;
    const TYPE_INDIVID_ENTREP = 3;

    const TITLE_INDIVIDUAL = 'Физическое лицо';
    const TITLE_ENTITY = 'Юридическое лицо';
    const TITLE_INDIVID_ENTREP = 'Индивидуальный предприниматель';



}