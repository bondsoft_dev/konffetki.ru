<?php

namespace skewer\build\Page\Poll;

use skewer\build\Component\Section\Tree;


class Module extends \skModule{

    public $Location = 'right';
    /* @var null|Api */
    public $oPollApi = NULL;
    private $sFileTemplate;
    private $sFileAnswerTemplate;
    public $SectionId = 0;
    /**
    * @var int ID главной страницы
    */
    private $defaultSection = 0;

    public function init(){

       $this->oPollApi = new Api();
       $this->setParser(parserTwig);
       $this->SectionId = $this->getEnvParam('sectionId');
       $this->defaultSection = \Yii::$app->sections->main();
       $this->sFileTemplate = 'poll_'.$this->Location.'.twig';
       $this->sFileAnswerTemplate = 'answer_'.$this->Location.'.twig';
    }

    public function execute(){

        $sCmd = $this->getStr('cmd','show');

        switch($sCmd){

            case 'show':
            default:

                $aParams = array();
                $aParams['location'] = $this->Location;
                $aParams['current_section'] = $this->SectionId;

                if ( $this->SectionId == $this->defaultSection ){
                    $aPolls = $this->oPollApi->getPollsOnMain($aParams);
                }
                else{

                    $aParentSections = Tree::getSectionParents($this->SectionId);
                    if ( $aParentSections )
                        $aParams['parent_sections'] = implode(',', $aParentSections);
                    else $aParams['parent_sections'] = $this->SectionId;

                    $aPolls = $this->oPollApi->getPollsOnInternal($aParams);
                }

                if ( $aPolls )
                    $this->setData('aPolls', $aPolls);

                $this->setTemplate($this->sFileTemplate);

                return psComplete;

            break;

            case 'vote_ajax':

                $iPollId = $this->getInt('poll');
                $iAnswerId = $this->getInt('answer');
                $aOut = array();

                $this->oPollApi->addVote(array('poll'=>$iPollId,'answer'=>$iAnswerId));

                $aOut['aPoll'] = $this->oPollApi->getPollHeader($iPollId);
                $aAnswers = $this->oPollApi->getAnswers($iPollId);
                $aOut['aAnswers'] = $aAnswers['items'];
                $aOut['iAllCount'] = $aAnswers['answers_count'];

                $sRendered = $this->renderTemplate($this->sFileAnswerTemplate,$aOut);
                $this->setData('out', $sRendered);

                return psRendered;
            break;

            case 'showResults_ajax':

                $iPollId = $this->getInt('poll');
                $aOut = array();
                $aOut['aPoll'] = $this->oPollApi->getPollHeader($iPollId);
                $aAnswers = $this->oPollApi->getAnswers($iPollId);
                $aOut['aAnswers'] = $aAnswers['items'];
                $aOut['iAllCount'] = $aAnswers['answers_count'];

                $sRendered = $this->renderTemplate($this->sFileAnswerTemplate,$aOut);
                $this->setData('out', $sRendered);

                return psRendered;
            break;
        }

    }

    public function shutdown(){

    }


}// class