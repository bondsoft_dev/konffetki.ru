<?php

namespace skewer\build\Page\Poll;

class Api {

    public function getPollsOnMain( $aParams ){

        return Mapper::getPollsOnMain($aParams);
    }// function getPollsOnMain()

    public function getPollsOnInternal( $aParams ){

        return Mapper::getPollsOnInternal($aParams);
    }// function getPollsOnInternal()

    public function getPolls($sLocation){

        $aPolls = Mapper::getPolls($sLocation);

        return $aPolls;
    }

    public function addVote($aInputData){

        return AnswersMapper::addVote($aInputData);
    }

    public function getAnswers($iPollId){

        if ( !$iPollId ) return false;

        $aFilter = array(
            'select_fields' => array('answer_id','title','value'),
            'where_condition' => array(
                'parent_poll' => array(
                    'sign' => '=',
                    'value' => $iPollId
                )
            ),
            'order' => array(
                'field' => 'sort',
                'way' => 'ASC'
            )
        );

        $aAnswers = AnswersMapper::getAnswers($aFilter);

        $iAllCount = 0;
        $iMaxElement = 0;
        $iMaxValue = 0;
        foreach( $aAnswers['items'] as $iKey=>$aAnswer ){

            $iAllCount+= $aAnswer['value'];
            if ( $aAnswer['value']>$iMaxValue ){
                $iMaxElement = $iKey;
                $iMaxValue = $aAnswer['value'];
            }
        }

        $aAnswers['answers_count'] = $iAllCount;

        foreach( $aAnswers['items'] as $iKey=>&$aAnswer ){

            //$aAnswer['percent'] = number_format(($aAnswer['value']*$iAllCount)/$iMaxValue, 1, '.', '');
            $aAnswer['percent'] = number_format(($aAnswer['value']*100)/$iAllCount, 1, '.', '');

            if ( $iKey!=$iMaxElement ){
                $iWidth = number_format(($aAnswer['value']*100)/$iMaxValue, 1, '.', '');
            }
            else
                $iWidth = 100;

            $sColor = 'rgb(0,128,180)';
            $aAnswer['style'] = 'style="height: 10px; width:'.$iWidth.'%; background:'.$sColor.'"';
        }

        return $aAnswers;
    }

    public function getPollHeader($iPollId){

        if ( !$iPollId ) return false;

        $aFilter = array(
            'select_fields' => array('title','question'),
            'where_condition' => array(
                'id' => array(
                    'sign' => '=',
                    'value' => $iPollId
                )
            )
        );

        $aPoll = Mapper::getItems($aFilter);

        return $aPoll['items'][0];
    }

}// class