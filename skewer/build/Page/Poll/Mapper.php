<?php

namespace skewer\build\Page\Poll;


use skewer\build\Adm\Poll as PollAdm;
use skewer\build\Component\orm\Query;

class Mapper extends \skMapperPrototype {

    /**
     * @var string Имя таблицы, с которой работает маппер
     */
    protected  static $sCurrentTable = 'polls';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {

        return static::$sCurrentTable;
    }// function getCurrentTable()

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'id' => 'i:hide:poll.id',
        'title' => 's:str:poll.title',
        'question' => 's:str:poll.question',
        'active' => 'i:check:poll.active',
        'on_main' => 'i:check:poll.on_main',
        'on_allpages' => 'i:check:poll.on_allpages',
        'on_include' => 'i:check:poll.on_include',
        'location' => 's:str:poll.location',
        'locationTitle' => 's:str:poll.locationTitle',
        'section' => 'i:str:poll.section',
        'sort' => 'i:str:poll.sort',
    );

    protected static function getAddParamList(){
        return array(
            'id' => array(
                'view' => 'hide',
                'listColumns' => array(
                    'hidden' => true
                )
            ),
            'active' => array(
                'listColumns' => array(
                    'hidden' => true
                )
            ),
            'title' => array(
                'listColumns' => array(
                    'flex' => 1
                )
            ),
            'location' => array_merge(\ExtForm::getDesc4SelectFromArray(PollAdm\Api::getPollLocations()), array('listColumns' => array('hidden'=>true))),
            'section' => \ExtForm::getDesc4SelectFromArray(PollAdm\Api::getSectionTitle()),
            'on_main' => array(
                'title' => \Yii::t('poll', 'onMainTitle'),
                'listColumns' => array(
                    'width' => 70,
                    'align' => 'center'
                )
            ),
            'on_allpages' => array(
                'title' => \Yii::t('poll', 'onAllTitle'),
                'listColumns' => array(
                    'width' => 50,
                    'align' => 'center'
                )
            ),
            'on_include' => array(
                'title' => \Yii::t('poll', 'onInternalTitle'),
                'listColumns' => array(
                    'width' => 90,
                    'align' => 'center'
                )
            ),
            'sort' => array(
                'title' => \Yii::t('poll', 'sortTitle'),
                'listColumns' => array(
                    'width' => 55,
                    'align' => 'center'
                )
            )
        );
    }// function getAddParamList()

    public static function getPollsOnInternal($aParams){

        $sParentSection = $aParams['parent_sections'];
        $sQuery = "
            SELECT
                *
            FROM
                `polls`
            WHERE
                `active` = 1 AND
                ( `section` = ? OR
                ( `on_include` = 1 AND `section` IN ($sParentSection) ) OR
                `on_allpages` = 1 ) AND
                `location` LIKE ?
            ORDER BY
                `sort`;";


        $res = Query::SQL($sQuery,$aParams['current_section'],$aParams['location']);

        $aPolls = array();
        while( $aRow = $res->fetchArray() ){

            $aFilter = array(
                'where_condition' => array(
                    'parent_poll' => array(
                        'sign' => '=',
                        'value' => $aRow['id']
                    )
                ),
                'order' => array(
                    'field' => 'sort',
                    'way' => 'ASC'
                )
            );

            $aAnswers = AnswersMapper::getAnswers($aFilter);
            $aRow['answers'] = $aAnswers['items'];

            $aPolls[] = $aRow;
        }

        return $aPolls;
    }// function getPollsOnInternal()

    public static function getPollsOnMain($aParams){

        $sQuery = "
            SELECT
                *
            FROM
                `polls`
            WHERE
                `active` <> 0 AND
                ( `section` = ? OR `on_main`=1 ) AND
                `location` LIKE ?
            ORDER BY
                `sort`;";

        $rResult = Query::SQL($sQuery,$aParams['current_section'],$aParams['location']);

        // Собираем результирующий массив
        $aPolls = array();
        while( $aRow = $rResult->fetchArray() ){

            $aFilter = array(
                'where_condition' => array(
                    'parent_poll' => array(
                        'sign' => '=',
                        'value' => $aRow['id']
                    )
                ),
                'order' => array(
                    'field' => 'sort',
                    'way' => 'ASC'
                )
            );

            $aAnswers = AnswersMapper::getAnswers($aFilter);
            $aRow['answers'] = $aAnswers['items'];

            $aPolls[] = $aRow;
        }

        return $aPolls;
    }// function getPollsOnMain()

    public static function delPoll($iPollId){
        if ( !$iPollId ) return false;
        Query::SQL("DELETE FROM `polls` WHERE `id` = ?;",$iPollId);
        Query::SQL("DELETE FROM `polls_answers` WHERE `parent_poll` = ?;",$iPollId);
        return true;
    }

    /**
     * @static Метод сохранения опроса
     * @param $aInputData
     * @return bool
     */
    public static function updPoll($aInputData){

        // Если во входящем массиве с данными нет значения сортироки или оно равно нулю - получаем максимальный порядок для текущей позиции
        if ( !isset($aInputData['sort']) || !$aInputData['sort'] )
            $aInputData['sort'] = self::getMaxOrder($aInputData['location'])+1;

        // Сохраняем опрос
        return static::saveItem($aInputData);
    }// function updPoll()

    /**
     * @static Метод для получения максимального значения поля "sort" для текущей позиции
     * @param string $sLocation Позиция, для которой выбирам максимальный порядок
     * @return mixed
     */
    public static function getMaxOrder($sLocation){

        $sQuery = "
            SELECT
                MAX(`sort`) as sort
            FROM
                `polls`
            WHERE
                `location` LIKE ?;";

        $res = Query::SQL($sQuery,$sLocation);

        $aValue = $res->fetchArray();

        return $aValue['sort'];
    }// function getMaxOrder()

    public static function getPolls($sLocation){

        $aPolls = array();

        $sQuery = "
            SELECT
                *
            FROM
                `polls`
            WHERE
                `location` LIKE ?
            ORDER BY
                `sort`;";

        $res = Query::SQL($sQuery,$sLocation);

        while( $aRow = $res->fetchArray() ){

            $sAnswerQuery = "
                SELECT
                    *
                FROM
                    `polls_answers`
                WHERE
                    `parent_poll` = ?
                ORDER BY
                    `sort`;";

            $rAnswerResult = Query::SQL($sAnswerQuery,$aRow['id']);

            $aAnswers = array();
            while( $aAnswerRow = $rAnswerResult->fetchArray() )
                $aAnswers[] = $aAnswerRow;

            $aRow['answers'] = $aAnswers;

            $aPolls[] = $aRow;
        }

        return $aPolls;
    }// function getPolls()

}// class