<?php

namespace skewer\build\Page\Poll;


use skewer\build\Component\orm\Query;

class AnswersMapper extends \skMapperPrototype{

    protected static $sCurrentTable = 'polls_answers';

    public static function getCurrentTable() {

        return static::$sCurrentTable;
    }// function getCurrentTable()

    /**
     * @var array Конфигурация полей таблицы polls_answers
     */
    protected static $aParametersList = array(
        'answer_id' => 'i:hide:poll.answer_id',
        'title' => 's:str:poll.answer_title',
        'parent_poll' => 's:str:poll.answer_parent_poll',
        'value' => 'i:str:poll.answer_value',
        'sort' => 'i:str:poll.answer_sort'
    );

    public static function updAnswer($aInputData){

        // Если во входящем массиве с данными нет значения сортироки или оно равно нулю - получаем максимальный порядок для текущей позиции
        if ( !isset($aInputData['sort']) || !$aInputData['sort'] )
        $aInputData['sort'] = self::getMaxOrder($aInputData['parent_poll'])+1;

        // Сохраняем опрос
        return self::saveItem($aInputData);
    }// function updAnswer()

    public static function getMaxOrder($iParentPoll){

        $sQuery = "
            SELECT
                MAX(`sort`) as sort
            FROM
                `polls_answers`
            WHERE
                `parent_poll` = ?;";

        $res = Query::SQL($sQuery,$iParentPoll);

        $aValue = $res->fetchArray();
        return $aValue['sort'];
    }// function getMaxOrder()

    public static function delAnswer($aInputData){

        if ( !$aInputData ) return false;

        $sQuery = "
            DELETE
            FROM
                `polls_answers`
            WHERE
                `answer_id` = ? AND
                `parent_poll` = ?;";

        return Query::SQL($sQuery, $aInputData['answer_id'],$aInputData['parent_poll']);
    }// function delAnswer()

    public static function addVote($aInputData){

        $sQuery = "
            UPDATE
                `polls_answers`
            SET
                `value` = `value` + 1
            WHERE
                `parent_poll` = ? AND
                `answer_id` = ?;";

        return Query::SQL($sQuery,$aInputData['poll'],$aInputData['answer']);
    }

    public static function getAnswers($aFilter){

        return static::getItems($aFilter);
    }

    protected static function getAddParamList(){

        return array(
            'answer_id' => array(
                'view' => 'hide',
                'listColumns' => array(
                    'hidden' => true
                )
            ),
            'parent_poll' => array(
                'view' => 'hide',
                'listColumns' => array(
                    'hidden' => true
                )
            ),
            'title' => array(
                'title' => \Yii::t('poll', 'answer_title'),
                'listColumns' => array(
                    'flex' => 1
                )
            ),
            'sort' => array(
                'listColumns' => array(
                    'width' => 55,
                    'align' => 'center'
                )
            )
        );
    }// function getAddParamList()

}//class