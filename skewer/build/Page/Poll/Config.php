<?php

/* main */
$aConfig['name']     = 'Poll';
$aConfig['title']     = 'Голосование';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Голосование';
$aConfig['revision'] = '0002';
$aConfig['layer']     = \Layer::PAGE;

return $aConfig;
