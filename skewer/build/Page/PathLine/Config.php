<?php

/* main */
$aConfig['name']     = 'PathLine';
$aConfig['title']    = 'Хлебные крошки';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Хлебные крошки';
$aConfig['revision'] = '0002';
$aConfig['layer']     = \Layer::PAGE;
$aConfig['languageCategory'] = 'page';

return $aConfig;
