<?php

namespace skewer\build\Page\PathLine;


use skewer\build\Component\Section\Tree;


/**
 * Модуль вывода "хлебных крошек" на страницы
 * Class Module
 * @package skewer\build\Page\PathLine
 */
class Module extends \skModule {

    /** @var string Набор конечных разделов */
    public $stopSections = '';

    /** @var bool Флаг вывода ссылки на главную страницу */
    public $withMain = false;

    public function execute() {

        $stopSections = explode(',', $this->stopSections);

        /**
         * Принудительно добавим в стоп-массив все известные менюшки
         */
        $stopSections[] = \Yii::$app->sections->topMenu();
        $stopSections[] = \Yii::$app->sections->leftMenu();
        $stopSections[] = \Yii::$app->sections->serviceMenu();
        $stopSections[] = \Yii::$app->sections->tools();

        $to = $this->getEnvParam('sectionId');
        $from = \Yii::$app->sections->root();
        $mainSectionId = \Yii::$app->sections->main();

        // выбираем все закешированные разделы
        $sections = Tree::getCachedSection();

        // собираем путь из разделов
        $out = [];
        $id = $to;

        // хвост пришедший из других модулей
        if ( $tail = $this->getEnvParam( 'pathline_additem' ) ) {
            $tail['selected'] = true;
            $out[] = $tail;
        }

        // последовательно собираем разделы
        while ( isSet($sections[$id]) && $id != $from ) {

            $section = $sections[$id];

            if ( $id == $mainSectionId ) {
                $id = $section['parent'];
                continue;
            }

            if ( in_array( $id, $stopSections ) )
                break;

            $section['href'] = $section['link'] ?: $section['alias_path'];
            $section['selected'] = $id==$to && !$tail;

            array_unshift( $out, $section );
            $id = $section['parent'];
        }

        // Ссылка на главный раздел
        if ( $this->withMain && count($out) && isSet($sections[$mainSectionId]) ) {

            $section = $sections[$mainSectionId];
            $section['href'] = $section['link'] ?: $section['alias_path'];
            $section['href'] = $section['href']?:'/';
            $section['selected'] = $id==$to && !$tail;

            $this->setData('main_page', $section);
        }

        if ( $this->withMain || count($out) > 1 )
            $this->setData( 'items', $out );

        $this->setTemplate('pathLine.twig');

        return psComplete;
    }

}
