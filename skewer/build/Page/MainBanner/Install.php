<?php

namespace skewer\build\Page\MainBanner;

/**
 *
 * @class MainBannerInstall
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date $Date: $
 * @package kernel
 */
class Install extends \skModuleInstall {
    public function init() {
        return true;
    }// func

    public function install() {
        return true;
    }// func

    public function uninstall() {
        return true;
    }// func
}
