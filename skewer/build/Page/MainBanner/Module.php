<?php

namespace skewer\build\Page\MainBanner;

use skewer\build\libs\ft;
use skewer\build\Adm\Slider;


/**
 * Модуль вывода банера в шапке
 * Class Module
 * @package skewer\build\Page\MainBanner
 * @extends skModule
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date $Date: $
 */
class Module extends \skModule {

    private $iCurrentSection;


    public function init() {

        $this->setParser(parserTwig);

        $this->iCurrentSection = $this->getEnvParam('sectionId');

    }


    public function execute() {

        /* получаем список доступных баннеров */
        $aBanners = Slider\Banner::getBanner4Section( $this->iCurrentSection );

        /* выбираем баннер */
        $iBannerCount = count($aBanners);

        if( !$iBannerCount )
            return psBreak;

        // todo написать ротацию на сессии
        $iBannerId = rand(0,$iBannerCount-1);

        $aCurrentBanner = $aBanners[$iBannerId];
        $iBannerId = $aBanners[$iBannerId]['id'];

        /* получаем список слайдов выбранного банера */
        $aSlides = Slider\Slide::getSlides4Banner( $iBannerId );

        /* вывод баннера */
        if( count($aSlides) == 1 ) {

            $aBannerTools = Slider\Banner::getAllTools();
            $aBannerTools['bullet'] = $aCurrentBanner['bullet'];
            $aBannerTools['scroll'] = $aCurrentBanner['scroll'];
            $aBannerTools['banner_h']  = (isset($aBannerTools['maxHeight']))  ? (int)$aBannerTools['maxHeight']  : 0;

            $this->setData('tools', $aBannerTools);
            
            $this->setData('image', $aSlides[0]);
            $this->setTemplate('image.twig');

        } elseif ( count($aSlides) > 1 ) {

            $aBannerTools = Slider\Banner::getAllTools();
            $aBannerTools['pager']      = (isset($aCurrentBanner['bullet']))     ? (bool)$aCurrentBanner['bullet']    : false;
            $aBannerTools['controls']   = (isset($aCurrentBanner['scroll']))     ? (bool)$aCurrentBanner['scroll']    : false;
            $aBannerTools['responsive'] = (isset($aBannerTools['responsive'])) ? (int)$aBannerTools['responsive'] : false;
            $aBannerTools['maxHeight']  = (isset($aBannerTools['maxHeight']))  ? (int)$aBannerTools['maxHeight']  : 0;
            $aBannerTools['easing'] = 'linear';

            $iMaxHeight = isset($aBannerTools['maxHeight']) ? (int)$aBannerTools['maxHeight'] : 0;
            $height = $iMaxHeight ? $aBannerTools['maxHeight'].'px' : 'auto';
            $this->setData('height', $height);

            $this->setData('config', json_encode($aBannerTools));

            $this->setData('banner', $aSlides);

            $this->setTemplate('banner.twig');

        } else {
            return psBreak;
        }

        return psComplete;
    }

}
