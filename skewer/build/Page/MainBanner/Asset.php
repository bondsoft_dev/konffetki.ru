<?php

namespace skewer\build\Page\MainBanner;

use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle {
    public $sourcePath = '@skewer/build/Page/MainBanner/web/';


    public $css = [
        'css/jquery.bxslider.css',
        'css/bxslider.settings.css',
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public $js = [
    	'js/jquery.easing.js',
        'js/jquery.bxslider.min.js',
        'js/initBanner.js',
    ];
}