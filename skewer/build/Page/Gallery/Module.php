<?php

namespace skewer\build\Page\Gallery;

use skewer\build\Component\SEO;
use skewer\build\Component\Gallery;
use skewer\build\Component\Site;
use yii\web\NotFoundHttpException;

/**
 * Модуль фотогаллереи
 */
class Module extends \skModule {

    public $AlbumsList  = 'albumsList.twig';
    public $AlbumDetail = 'showAlbum.twig';
    public $openAlbum = false;
    /*
     * @var int id раздела источника данных
     * Если заполнено - в качестве раздела будет изпользован заданный
     *   при этом ссылки будут вести на целевую страницу
     * Если альбом один - будет сразу развернут
     * Если альбомов несколько - откроется выбранный но в разделе указанном в параметре
     */
    public $sourceSection = 0;

    private $iCurrentSection;

    public function init() {

        $this->setParser(parserTwig);
        $this->iCurrentSection = $this->getEnvParam('sectionId');

        if ( $this->sourceSection )
            $this->iCurrentSection = $this->sourceSection;

    }// func

    public function execute() {

        $sAlbumAlias = urldecode($this->getStr('alias', ''));

        if ($sAlbumAlias) {
            $bResult = $this->showByAlias($sAlbumAlias);
        }else{
            $bResult = $this->showBySection();
        }

        if ($bResult)
            return psComplete;

        throw new NotFoundHttpException();

    }// func


    /**
     * Вывод альбома по алиасу
     * @param $sAlbumAlias
     * @return bool
     */
    private function showByAlias($sAlbumAlias)
    {
        if ($this->openAlbum)
            return false;// Если альбомы открыты, но запрошен alias альбома то выдать 404

        if (!$aAlbum = Gallery\Album::getByAlias($sAlbumAlias, $this->iCurrentSection))
            return false;

        if (!$aAlbum['visible'])
            return false;

        $this->setSeo($aAlbum['title'], $aAlbum['id']);
        $this->setData('description', $aAlbum['description']);

        $this->setPhotos($aAlbum['id']);

        return true;
    }// func

    /**
     * Вывод по разделу
     * @return bool
     */
    private function showBySection(){

        if (!$this->iCurrentSection)
            return false;

        $aAlbums = Gallery\Album::getBySection($this->iCurrentSection);

        if (!$aAlbums)
            return true;

        if (count($aAlbums) == 1)
            $this->openAlbum = true;

        if ($this->openAlbum) {
            $this->setData('hideBack', true);
            $this->setPhotos(\yii\helpers\ArrayHelper::getColumn($aAlbums, 'id'));
            return true;
        }

        return $this->showAlbums($aAlbums);

    }

    /** Показывает список альбомов
     * @param array $aAlbums Данные альбомов
     * @return int
     */
    private function showAlbums(array &$aAlbums) {

        if (!$aAlbums)
            return psComplete;

        $aAlbums = Gallery\Album::setCountsAndPreview($aAlbums, true);

        $this->setData('albums', $aAlbums);
        $this->setData('sectionId', $this->iCurrentSection);
        $this->setTemplate($this->AlbumsList);

        return psComplete;
    }// func

    /**
     * Добавляет изображения альбомов
     * @param array $aAlbumsId Альбомы
     */
    private function setPhotos($aAlbumsId) {

        $this->addJSFile(\Yii::$app->getAssetManager()->getBundle(Asset::className())->baseUrl.'/js/galleryInit.js');

        $this->setData('images', Gallery\Photo::getFromAlbum($aAlbumsId, true, null, false));
        $this->setData('openAlbum', $this->openAlbum);
        $this->setTemplate($this->AlbumDetail);

        $this->setEnvParam('gallery_photos', 1);
    }

    /**
     * @param string $title Заголовок
     * @param int $id id
     */
    private function setSeo($title, $id)
    {
        // меняем заголовок страницы
        Site\Page::setTitle($title);

        // добавляем элемент в pathline
        Site\Page::setAddPathItem($title);

            // Метатеги
            $SEOData = $this->getEnvParam('SEOTemplates', array());
            $SEOData[SEO\Api::TPL_GALLERY] = array('title' => '', 'description' => '', 'keywords' => '');
            if ($oDataRow = SEO\Api::get('gallery', $id))
                $oDataRow->setParams($SEOData, SEO\Api::TPL_GALLERY);
            $this->setEnvParam( 'SEOTemplates', $SEOData );
            $SEOVars = $this->getEnvParam( 'SEOVars', array() );
            $SEOVars['label_gallery_title_upper'] = $title;
            $SEOVars['label_gallery_title_lower'] = mb_strtolower( $title );
            $this->setEnvParam( 'SEOVars', $SEOVars );

        Site\Page::reloadSEO();
    }

}// class
