<?php

namespace skewer\build\Page\Gallery;

use skewer\build\Component\Router\RoutingInterface;


class Routing implements RoutingInterface {
    /**
     * Возвращает паттерны разбора URL
     * @static
     * @return bool | array
     */
    public static function getRoutePatterns() {

        return array(
            '/alias/',
            '/!response/',
            '/alias/!response/'
        );
    }// func
}// class
