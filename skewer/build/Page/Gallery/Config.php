<?php

/* main */
$aConfig['name']     = 'Gallery';
$aConfig['title']     = 'Галерея';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Модуль фотогаллереи';
$aConfig['revision'] = '0002';
$aConfig['layer']     = \Layer::PAGE;
$aConfig['languageCategory']     = 'gallery';

return $aConfig;
