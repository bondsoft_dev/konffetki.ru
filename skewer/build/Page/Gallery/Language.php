<?php

$aLanguage = array();

$aLanguage['ru']['Gallery.Page.tab_name'] = 'Галерея';
$aLanguage['ru']['back'] = 'Вернуться к списку альбомов';
$aLanguage['ru']['water_top_left'] = 'Верхний левый угол';
$aLanguage['ru']['water_top_right'] = 'Верхний правый угол';
$aLanguage['ru']['water_bottom_left'] = 'Нижний левый угол';
$aLanguage['ru']['water_bottom_right'] = 'Нижний правый угол';
$aLanguage['ru']['water_center'] = 'По центру';
$aLanguage['ru']['new_profile'] = 'Новый профиль';
$aLanguage['ru']['show_album'] = 'Показывать только фотографии';

$aLanguage['en']['Gallery.Page.tab_name'] = 'Gallery';
$aLanguage['en']['back'] = 'Return to gallery';
$aLanguage['en']['water_top_left'] = 'Top left';
$aLanguage['en']['water_top_right'] = 'Top right';
$aLanguage['en']['water_bottom_left'] = 'Bottom left';
$aLanguage['en']['water_bottom_right'] = 'Bottom right';
$aLanguage['en']['water_center'] = 'Center';
$aLanguage['en']['new_profile'] = 'New profile';
$aLanguage['en']['show_album'] = 'Show only photo';

return $aLanguage;