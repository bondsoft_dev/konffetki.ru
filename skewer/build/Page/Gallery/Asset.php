<?php

namespace skewer\build\Page\Gallery;

use yii\web\AssetBundle;

class Asset extends AssetBundle {
    public $sourcePath = '@skewer/build/Page/Gallery/web/';
}