<?php

$aLanguage = array();

$aLanguage['ru']["MiniCart.Page.tab_name"] = "Мини-корзина";

$aLanguage['ru']["basket_empty"] = "Корзина пуста";
$aLanguage['ru']["add_to_card"] = "Добавление товара в корзину";
$aLanguage['ru']["add_success1"] = "Товар добавлен в корзину";
$aLanguage['ru']["add_success2"] = "Перейти в корзину";
$aLanguage['ru']["order"] = "Товаров";
$aLanguage['ru']["close_link"] = "Продолжить покупки";

$aLanguage['en']["MiniCart.Page.tab_name"] = "Mini shopping cart";

$aLanguage['en']["basket_empty"] = "Your shopping cart is empty";
$aLanguage['en']["add_to_card"] = "Adding product to cart";
$aLanguage['en']["add_success1"] = "Product was added to your cart";
$aLanguage['en']["add_success2"] = "Proceed to checkout";
$aLanguage['en']["order"] = "Order";
$aLanguage['en']["close_link"] = "Continue shopping";

return $aLanguage;