$(document).ready(function(){

    $.fn.cart = function(o) {

        o = jQuery.extend({

            ajaxHandler: '/ajax/ajax.php',
            ajaxModuleName: 'Cart',
            ajaxCmd: 'setItem',
            count: '',
            total: '',
            contentCls: '',
            emptyCls: '',
            afterClick: function() {
            }
        }, o);

        var object = $(this);

        object.unbind('click').on('click', function() { return objClick($(this)) });
        $('body').on('changeCart', function(e, jsonData) { updateCart(jsonData) });

        function updateCart(jsonData) {

            var data = jQuery.parseJSON(jsonData);

            var contentBlock = $('.' + o.contentCls);
            var emptyBlock = $('.' + o.emptyCls);
            if ( contentBlock.is( '.' + o.contentCls + '-hidden' ) ) {
                contentBlock.removeClass(o.contentCls + '-hidden');
                emptyBlock.addClass(o.emptyCls + '-hidden');
            }

            if (!data.count) {
                contentBlock.addClass(o.contentCls + '-hidden');
                emptyBlock.removeClass(o.emptyCls + '-hidden');
            }

            if (data.count) $(o.count).text(data.count);
            if (data.total)
                $(o.total).text(data.total);
            else
                $(o.total).text(0);
        }

        function objClick(e) {

            var objectId = e.attr('data-id');

            var params = {};
            params.moduleName = o.ajaxModuleName;
            params.cmd = o.ajaxCmd;
            params.objectId = objectId;
            params.count = parseInt( getCount(objectId) );
            params.language = $('#current_language').val();

            $.post(o.ajaxHandler, params, function(response) {
                $('body').trigger('changeCart', response);
                o.afterClick(e, response);
            });

            return false;
        }

        function getCount(objectId) {
            var count = $('input[data-id=' + objectId + ']');
            var res = 1;
            if ( count.size() ) {
                count.each(function( key, itm ) {
                    var val = parseInt($(itm).val());
                    res = Math.max( val, res );
                });
            }
            return res;
        }

    };

    $('.btnBuy[href="#tocart"]').cart({
        count: '.b-basketmain .goods-count',
        total: '.b-basketmain .goods-total',
        contentCls: 'minicart__content',
        emptyCls: 'minicart__empty',
        afterClick: function(e, response){
            var objectId = e.attr('data-id');
            var cntCont = $('input[data-id=' + objectId + ']');
            var count;
            //var noCnt;

            if ( cntCont.size() ) {
                //noCnt = false;
                count = 1;
                cntCont.each(function( key, itm ) {
                    count = Math.max( parseInt($(itm).val()), count );
                });
            } else {
                //noCnt = true;
                count = 1;
            }

            var oResponse = $.parseJSON(response);

            var container = $(".js_cart_fancy_add:first");

            container.find('.basket-name').html(oResponse.lastItem.title);

            if (count > 0 ) {// && ( noCnt || count==cntCont.val() )

                $.fancybox.open(container.html(),{
                    dataType : 'html',
                    autoSize: true,
                    autoCenter: true,
                    openEffect: 'none',
                    closeEffect: 'none'
                });

            }
            else alert('Введено неправильное количество товара');
        }
    });

    $('.js_catalogbox_plus').click(function(){
        var input = $(this).parents('.catalogbox__inputbox').find('input[type=text]');
        var value = parseInt(input.val());
        if ( isNaN(value) )
            value = 0;
        if ( value >= 0 )
            input.val( value + 1 );
        else
            input.val( 1 );
    });
    $('.js_catalogbox_minus').click(function(){
        var input = $(this).parents('.catalogbox__inputbox').find('input[type=text]');
        var value = parseInt(input.val());
        if ( isNaN(value) )
            value = 2;
        if (value > 1) {
            input.val( value - 1 );
        } else {
            input.val(1);
        }
    });

    $(document).on('click', '.js-basket-close', function(){
        $.fancybox.close();
        return false;
    })

});