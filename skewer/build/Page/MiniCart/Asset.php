<?php

namespace skewer\build\Page\MiniCart;

use yii\helpers\FileHelper;
use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle {

    public $sourcePath = '@skewer/build/Page/MiniCart/web/';

    public $css = [
        'css/main.css',
    ];

    public $js = [
//        'js/cart.js',
        'js/main.js'
    ];

    public function init()
    {
        parent::init();
        $this->publishOptions['beforeCopy'] = function ($from, $to) {
            if (is_dir($from) && basename($from)=='images'){
                FileHelper::copyDirectory($from,WEBPATH.'images/');
            }
            return true;
        };
    }

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];


}