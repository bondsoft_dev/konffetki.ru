<?php

namespace skewer\build\Page\MiniCart;

use skewer\build\Page\Cart as Cart;


class Module extends \skModule {

    public $template = 'cart.twig';

    /**
     * @return int
     */
    public function execute() {

        if ( \Design::modeIsActive() ) {
            $this->setData('designMode', \Design::getDirList() );
        }

        $this->setTemplate($this->template);

        $this->setData('order', Cart\Api::getOrder());
        $this->setData('cartSectionId', \Yii::$app->sections->getValue('cart'));

        return psComplete;
    }
} 