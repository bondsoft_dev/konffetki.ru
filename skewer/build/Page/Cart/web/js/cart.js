$(document).ready(function(){

    updateDeliveryPrice();

    $('.js_cart_remove').click(function() {

        var me = this;
        var id = $(this).attr('data-id');
        var params = {};
        params.moduleName = 'Cart';
        params.cmd = 'removeItem';
        params.id = id;
        params.language = $('#current_language').val();

        $.post('/ajax/ajax.php', params, function(jsonData) {

            var data = jQuery.parseJSON(jsonData);
            $(me).parents('.cart__row').remove();

            if (!data.count) {
                $('.cart__content').hide();
                $('.cart__empty').removeClass('cart__empty-hidden');
            }

            if (data.total) {
                $('.total').text(data.total);
            } else {
                $('.total').text(0);
            }

            $('body').trigger('changeCart', jsonData);
        });

        return false;
    });

    $(".js_cart_minus").click(
        function(){
            var me = this;
            var id = $(me).attr('data-id');

            var row = $('.js_cart_amount[data-id=' + id + ']');
            var count = parseInt(row.val());

            // если не число - поставить 1
            if ( isNaN(count) )
                count = 1;

            // меньше 1 быть не может
            if (count > 1)
                count--;
            else
                count = 1;

            var params = {};
            params.moduleName = 'Cart';
            params.cmd = 'recountItem';
            params.id = id;
            params.count = count;

            sendParam(params);

            row.val(count);

        }
    );

    $(".js_cart_plus").click(
        function(){
            var me = this;
            var id = $(me).attr('data-id');

            var row = $('.js_cart_amount[data-id=' + id + ']');
            var count = parseInt(row.val());

            // проверка на валидность
            if ( isNaN(count) )
                count = 0;

            // увеличть на 1 (если меньше 0 - привести к 1)
            if ( count >= 0 )
                count++;
            else
                count = 1;

            var params = {};
            params.moduleName = 'Cart';
            params.cmd = 'recountItem';
            params.id = id;
            params.count = count;
            sendParam(params);

            row.val(count);
        }
    );

    function sendParam(params){

        params.language = $('#current_language').val();

        $.post('/ajax/ajax.php', params, function(jsonData) {

            var data = jQuery.parseJSON(jsonData);
            var row = $('.cart__row[data-id=' + params.id + ']');

            for (var key in data.items) {
                if (data.items.hasOwnProperty(key) && data.items[key].id == params.id) {
                    row.find('.item_total').text(data.items[key].total);
                }
            }

            if (data.total) {
                $('.total').text(data.total);
            }

            $('body').trigger('changeCart', jsonData);
        });
    }


    $('.js_cart_amount')
        .keypress(function(e){
            if (e.keyCode==13){
                $(this).blur();
            }
        })
        .blur(function() {

            var me = this;
            var id = $(me).attr('data-id');
            var params = {};
            params.moduleName = 'Cart';
            params.cmd = 'recountItem';
            params.id = id;

            var countValue = parseInt($(me).val());
            if ( isNaN(countValue) )
                countValue = 1;
            if (countValue>0 && countValue==$(me).val()){

                params.count = countValue;
                sendParam(params);
            } else alert($('#js_translate_msg_count_gt_zero').html());
        })
    ;

    $('.cart__reset').click(function(){

        if(!confirm($('#js_translate_msg_dell_all').html())) return false;

        var params = {};
        params.moduleName = 'Cart';
        params.cmd = 'unsetAll';
        params.language = $('#current_language').val();

        $.post('/ajax/ajax.php', params, function(jsonData) {

            $('.cart__content').hide();
            $('.cart__empty').removeClass('cart__empty-hidden');
            $('body').trigger('changeCart', jsonData);
        });

        return false;
    });

    $('.agreed_readmore').fancybox();

    $('body').on( 'click', '.fast_agreed_readmore', function(){

        var agreed_container = $('.js_agreed_readmore');

        if (agreed_container.hasClass('open')){
            agreed_container
                .removeClass('open')
                .hide()
            ;
        }else{
            agreed_container
                .addClass('open')
                .show()
            ;
        }

        return false;

    });

    $(document).on('change','.js-deliv',function(){
        updateDeliveryPrice();
    });

//    обработка отображения на формах

//смена формы в контенте в зависимости от типа
    $(document).on('click','.js-type-radio', function(){
        var type = $(this);
        var content = $(this).closest('.b-form');
        $.post( '/ajax/ajax.php', {
            moduleName: 'Cart',
            cmd: 'change',
            type: type.val(),
            fast: $("input[name=fast]").val(),
            language: $('#current_language').val()
        }, function ( mResponse ) {
            if (!mResponse) return false;
            var oResponse = $.parseJSON(mResponse);
            $(document).find(content).replaceWith(oResponse.forms);
            maskInit();
            updateDeliveryPrice();
        });
    });

    $(document).on('click','.js-legal_actual', function(){
        if($(this).val() == 1){
            $("#js-actual-address").show();
        } else {
            $("#js-actual-address").hide();
        }
    });

    $(document).on('click','.js-actual_delivery', function(){
        if($(this).val() == 1){
            $("#js-delivery-address").show();
        } else {
            $("#js-delivery-address").hide();
        }
    });

    /**
     * метод обновляет стоимость доставки и
     * корректирует итоговую сумму с учетом стоимости доставки
     * @returns {boolean}
     */
    function updateDeliveryPrice() {
        let deliveryId = $('.js-deliv').val(),
        deliveryPrice = $('.js-delivery-type[value='+deliveryId+']').attr('price'),
        total = parseFloat($('.js-goods-total').text()),
        totalSum = 0;

        $(document).find("#js-deliv_price").text(deliveryPrice);

        deliveryPrice = parseFloat(deliveryPrice);
        totalSum = (deliveryPrice + total);
        totalSum = totalSum.toFixed(2);
        $(document).find("#js-total_price_deliv").text(totalSum);

        return false;
    }

});