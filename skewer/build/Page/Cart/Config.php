<?php

$aConfig['name']     = 'Cart';
$aConfig['version']  = '1.0';
$aConfig['title']    = 'Корзина';
$aConfig['description']  = 'Вывод корзины';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::PAGE;
$aConfig['languageCategory']     = 'order';

return $aConfig;
