<?php

namespace skewer\build\Page\Cart;


use skewer\build\Adm\Order\model\Status;
use skewer\build\Component\I18N\Languages;
use skewer\build\Component\I18N\ModulesParams;
use skewer\build\Component\Section\Tree;
use skewer\build\Component\Section\Visible;
use \skewer\build\Component\Site;
use \skewer\build\Component\orm;
use \skewer\build\Catalog;
use skewer\build\Component\Catalog\Card;
use skewer\build\Component\Catalog\Generator;
use skewer\build\Adm\Order\ar\Goods;
use skewer\build\Adm\Order\ar\TypeDelivery;
use skewer\build\Adm\Order\ar\TypePayment;
use skewer\build\libs\ft\Cache;
use yii\helpers\ArrayHelper;

class Install extends \skModuleInstall {

    const CART_ALIAS = 'cart';

    private $languages = [];

    private $status = [
        'new',
        'paid',
        'fail',
        'formed',
        'send',
        'close',
        'cancel',
    ];

    private $moduleParamKeys =
    [
        'title_change_status_mail', 'status_content',
        'title_user_mail', 'user_content',
        'title_adm_mail', 'adm_content',
        'license'
    ];

    public function init() {

        $this->languages = ArrayHelper::map(Languages::getAllActive(), 'name', 'name');
        return true;
    }

    public function install() {

        if (!Site\Type::hasCatalogModule())
            $this->fail('Нельзя установить корзину на некаталожный сайт!');

        $iNewPageSection = \Yii::$app->sections->tplNew();

        foreach (\Yii::$app->sections->getValues('tools') as $sLang => $iSection) {

            $oSection = Tree::addSection($iSection, \Yii::t('data/order', 'cart_section_title', [], $sLang), $iNewPageSection, self::CART_ALIAS, Visible::HIDDEN_FROM_MENU);
            $this->setParameter($oSection->id, 'object', 'content', 'Cart');
            $this->setParameter($oSection->id, 'hide_right_column', '.', '1');
            $this->setParameter($oSection->id, '.title', '.layout', \Yii::t('data/order', 'cart_section_title', [], $sLang) . ' (' . $sLang . ')');

            \Yii::$app->sections->setSection('cart', \Yii::t('site', 'cart', [], $sLang), $oSection->id, $sLang);

        }

        /** Поле с кол-вом в карточку товара */
        $this->addCountByField();

        /** Перестроение таблиц */
        $this->rebuildTable();

        /** Добавление параметров */
        $this->setModuleParams();

        return true;
    }

    protected function setModuleParams()
    {
        foreach ($this->languages as $lang) {
            foreach ($this->moduleParamKeys as $key) {
                ModulesParams::setParams('order', $key, $lang, \Yii::t('data/order', $key, [], $lang));
            }
        }
    }

    public function uninstall() {

        foreach (\Yii::$app->sections->getValues('order') as $value){
            Tree::removeSection($value);
        }

        ModulesParams::deleteByModule('order');

        return true;
    }

    /**
     * Поле с кол-вом в карточку товара
     */
    private function addCountByField(){

        $iBaseCard = 1;

        $oGroup = Card::getGroupByName($iBaseCard, 'controls');

        if ($oGroup){

            Generator::createField( $iBaseCard, [
                'name' => 'countbuy',
                'title' => \Yii::t('data/catalog', 'field_countbuy_title', [], \Yii::$app->language),
                'group' => $oGroup->id,
                'editor' => 'check',
                'entity' => $iBaseCard,
                'def_value' => 1,
            ] );

            Card::get($iBaseCard)->updCache();

            /** Выставление товаров */
            orm\Query::UpdateFrom(Cache::get( 'base_card' )->getTableName())->set('countbuy', 1)->get();
        }

    }

    /**
     * Перестроение таблиц
     */
    private function rebuildTable(){

        orm\Query::SQL('CREATE TABLE IF NOT EXISTS `orders_status` (
         `id` int(11) NOT NULL AUTO_INCREMENT,
         `name` varchar(64) NOT NULL,
         PRIMARY KEY (`id`),
         UNIQUE KEY `name` (`name`)
        ) ENGINE=InnoDB AUTO_INCREMENT=18 DEFAULT CHARSET=utf8');

        Goods::rebuildTable();
        TypeDelivery::rebuildTable();
        TypeDelivery::delete()->get();
        TypePayment::rebuildTable();
        TypePayment::delete()->get();

        /** Status */
        foreach( $this->status as $sName ){
            $this->setStatus($sName);
        }

        /** Типы доставки */
        TypeDelivery::getNewRow(['title' => \Yii::t('order', 'tp_deliv_1', [], \Yii::$app->language)])->save();
        TypeDelivery::getNewRow(['title' => \Yii::t('order', 'tp_deliv_2', [], \Yii::$app->language)])->save();
        TypeDelivery::getNewRow(['title' => \Yii::t('order', 'tp_deliv_3', [], \Yii::$app->language)])->save();

        /** Типы оплат */
        TypePayment::getNewRow(['title' => \Yii::t('order', 'tp_pay_1', [], \Yii::$app->language)])->save();
        TypePayment::getNewRow(['title' => \Yii::t('order', 'tp_pay_2', [], \Yii::$app->language)])->save();
        TypePayment::getNewRow(['title' => \Yii::t('order', 'tp_pay_3', [], \Yii::$app->language)])->save();

    }

    /**
     * @param $sName
     */
    private function setStatus($sName)
    {
        /**
         * @var Status $oStatus
         */
        $oStatus = Status::find()
            ->multilingual()
            ->where(['name' => $sName])
            ->one();

        if (!$oStatus) {
            $oStatus = new Status();
            $oStatus->name = $sName;
        }

        $aData = [];
        foreach($this->languages as $lang){
            $aData['active_' . $lang] = 1;
            $aData['title_' . $lang] = \Yii::t('data/order', 'status_'.$sName, [], $lang);
        }

        $oStatus->setLangData($aData);

        $oStatus->save();
    }

} 