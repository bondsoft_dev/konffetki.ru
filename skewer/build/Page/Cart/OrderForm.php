<?php

namespace skewer\build\Page\Cart;

use skewer\build\Adm\Order\ar;
use skewer\build\Adm\Order\model\Delivery;
use skewer\build\Component\Forms\Field as FormFields;
use skewer\build\Component\orm;
use skewer\build\Component\Section\Tree;
use skewer\build\Page\Auth\UserType;
use skewer\build\Tool\Payments;
use skewer\build\Adm\Auth\ar\UsersRow;
use skewer\build\Adm\Auth\ar\Users;
use yii\helpers\ArrayHelper;


class OrderForm extends orm\FormRecord {

    public $person = '';
//    public $postcode = '';
//    public $address = '';
    public $phone = '';
    public $mail = '';
    public $tp_deliv = 1;
    public $tp_pay = 1;
    public $source_info = 1;
    public $accepts_the_licen = 0;
    public $text = '';

    public $deliv_cost = 0;

    //данные для всех 3 типов
    //адрес доставки
    public $delivery_region = '';
    public $delivery_city = '';
    public $delivery_street = '';
    public $delivery_house = '';
    public $delivery_housing = '';
    public $delivery_room = '';

    //для юриков и ип
    //правовая форма
    public $legal_form = '';
    //название организации
    public $organization = '';
    //ИНН
    public $inn = '';
    //КПП
    public $kpp = '';

    //юридический адрес
    public $legal_region = '';
    public $legal_city = '';
    public $legal_street = '';
    public $legal_house = '';
    public $legal_housing = '';
    public $legal_room = '';

    //фактический адрес такой же как юридический
    public $legal_actual = 0;

    //фактический адрес
    public $actual_region = '';
    public $actual_city = '';
    public $actual_street = '';
    public $actual_house = '';
    public $actual_housing = '';
    public $actual_room = '';

    //адрес доставки такой же как фактический
    public $actual_delivery = 0;

    /** @var bool Флаг на быстрый заказ через 1 клик */
    private $fast = false;
    private $user = false;

    /** @var int Вид формы для вывода
    * ИД - 3
    * Юридическое лицо - 2
    * Физическое лицо - 1
     */
    public $type_form = 1;

    public function rules() {
        if ($this->type_form == UserType::TYPE_INDIVIDUAL ){
            return array(
                array(array('person', 'phone', 'mail', 'tp_deliv', 'tp_pay', 'accepts_the_licen','delivery_city'), 'required', 'msg' => \Yii::t('forms', 'err_empty_field')),
                array(array('mail'), 'email', 'msg' => \Yii::t('order', 'incor_email')),
            );
        } elseif ($this->type_form == UserType::TYPE_ENTITY) {
            return array(
                array(array('person', 'phone', 'mail', 'tp_deliv', 'tp_pay', 'accepts_the_licen', 'legal_city', 'organization', 'legal_form', 'inn', 'kpp'), 'required', 'msg' => \Yii::t('forms', 'err_empty_field')),
                array(array('mail'), 'email', 'msg' => \Yii::t('order', 'incor_email')),
            );
        } elseif($this->type_form == UserType::TYPE_INDIVID_ENTREP){
            return array(
                array(array('person', 'phone', 'mail', 'tp_deliv', 'tp_pay', 'accepts_the_licen', 'legal_city', 'organization', 'legal_form', 'inn'), 'required', 'msg' => \Yii::t('forms', 'err_empty_field')),
                array(array('mail'), 'email', 'msg' => \Yii::t('order', 'incor_email')),
            );
        }else{
            return array();
        }
    }

    public function getLabels() {
        $aLabels =  array(
            'postcode' => \Yii::t('order', 'postcode' ),
            'address' => \Yii::t('order', 'address' ),
            'phone' => \Yii::t('order', 'phone' ),
            'mail' => \Yii::t('order', 'mail' ),
            'tp_deliv' => \Yii::t('order', 'tp_deliv' ),
            'tp_pay' => \Yii::t('order', 'tp_pay' ),
            'source_info' => \Yii::t('order', 'source_info' ),
            'text' => \Yii::t('order', 'text' ),
            'accepts_the_licen' => \Yii::t('order', 'accepts_the_licen' ),
            'deliv_cost' => \Yii::t('order', 'deliv_cost' ),
            'delivery_region' => \Yii::t('order', 'delivery_region' ),
            'delivery_city' => \Yii::t('order', 'delivery_city' ),
            'delivery_street' => \Yii::t('order', 'delivery_street' ),
            'delivery_house' => \Yii::t('order', 'delivery_house' ),
            'delivery_housing' => \Yii::t('order', 'delivery_housing' ),
            'delivery_room' => \Yii::t('order', 'delivery_room' ),
            'legal_form' => \Yii::t('order', 'legal_form' ),
            'organization' => \Yii::t('order', 'organization' ),
            'inn' => \Yii::t('order', 'inn' ),
            'kpp' => \Yii::t('order', 'kpp' ),
            'legal_region' => \Yii::t('order', 'legal_region' ),
            'legal_city' => \Yii::t('order', 'legal_city' ),
            'legal_street' => \Yii::t('order', 'legal_street' ),
            'legal_house' => \Yii::t('order', 'legal_house' ),
            'legal_housing' => \Yii::t('order', 'legal_housing' ),
            'legal_room' => \Yii::t('order', 'legal_room' ),
            'legal_actual' => \Yii::t('order', 'legal_actual' ),
            'actual_region' => \Yii::t('order', 'actual_region' ),
            'actual_city' => \Yii::t('order', 'actual_city' ),
            'actual_street' => \Yii::t('order', 'actual_street' ),
            'actual_house' => \Yii::t('order', 'actual_house' ),
            'actual_housing' => \Yii::t('order', 'actual_housing' ),
            'actual_room' => \Yii::t('order', 'actual_room' ),
            'actual_delivery' => \Yii::t('order', 'actual_delivery' ),
            'type_form' => \Yii::t('order', 'type_form' ),

        );
        if ($this->type_form == UserType::TYPE_INDIVIDUAL) {
            $aLabels['person'] = \Yii::t('order', 'fio');
        } else {
            $aLabels['person'] = \Yii::t('order', 'person' );
        }
        if ($this->type_form == UserType::TYPE_INDIVID_ENTREP){
            $aLabels['organization'] = \Yii::t('order', 'fio' );
        } else{
            $aLabels['organization'] = \Yii::t('order', 'organization' );
        }
        return $aLabels;
    }

    public function getEditors() {
        return array(
            'tp_deliv' => array( 'select', 'method'=>'getTypeDelivList' ),
            'tp_pay' => array( 'select', 'method'=>'getTypePayList' ),
            'source_info' => array( 'select', 'method'=>'getSourcesInfo' ),
            'accepts_the_licen' => 'checkbox',
            'legal_actual' => array( 'select', 'method'=>'getLegalActual' ),
            'actual_delivery' => array( 'select', 'method'=>'getActualDelivery' ),
            'type_form' => array( 'select', 'method'=>'getTypeForm' ),
        );
    }

    /**
     * Установка флага о быстром заказе
     * @param bool $fast
     */
    public function setFast($fast){
        $this->fast = $fast;
    }

    /**
     * Быстрый заказ
     * @return bool
     */
    public function getFast(){
        return $this->fast;
    }

    /**
     * Установка типа формы
     * @param bool $user
     */
    public function setUser($user){
        $this->user = $user;
    }

    /**
     * Вид формы
     * @return bool
     */
    public function getUser(){
        return $this->user;
    }

    public function getTypeDelivList() {
        return ArrayHelper::map(Delivery::getList(),'id','title');
    }

    public function getTypePayList() {
        return ArrayHelper::map(ar\TypePayment::find()->asArray()->getAll(),'id','title');
    }

    public function getSourcesInfo() {
        return ArrayHelper::map(ar\SourcesInfo::find()->asArray()->getAll(),'id','title');
    }

    public function getPayList() {
        $aPayments = Payments\Api::getPaymentsList( true );
        foreach( $aPayments as &$aPayment){
            $aPayment['title'] = \Yii::t('payments', $aPayment['type']);
        }
        return $aPayments;
    }

    public function getLegalActual()    {
        return array('Совпадает с юридическим адресом','Другой');
    }

    public function getActualDelivery() {
        return array('Совпадает с фактическим адресом','Другой');
    }

    public function getTypeUser() {
        return  array(  'individual' => UserType::TYPE_INDIVIDUAL,
            'entity' => UserType::TYPE_ENTITY,
            'individ_entrep' => UserType::TYPE_INDIVID_ENTREP,
        );
    }

    public function getTypeForm() {
        return  array(  UserType::TYPE_INDIVIDUAL => UserType::TITLE_INDIVIDUAL,
            UserType::TYPE_ENTITY => UserType::TITLE_ENTITY,
            UserType::TYPE_INDIVID_ENTREP => UserType::TITLE_INDIVID_ENTREP,
        );
    }

    // js валидация
    public function getRules() {
        $aRules = array();
        $aRules['rules'] = array(
            'person' => array('required'=>true,"maxlength"=>255),
//            'postcode' => array('required'=>true,"maxlength"=>255),
//            'address' => array('required'=>true,"maxlength"=>255),
            'phone' => array('required'=>true,"maxlength"=>255),
            'mail' => array('required'=>true,"maxlength"=>255,"email"=>true),
            'tp_deliv' => array('required'=>true,"maxlength"=>255),
            'tp_pay' => array('required'=>true,"maxlength"=>255),
            'source_info' => array('required'=>true,"maxlength"=>255),
            'text' => array('required'=>false,"maxlength"=>255),
            'accepts_the_licen' => array('required'=>true,"maxlength"=>255),
        );
        return json_encode($aRules);
    }


    /**
     * Заполнение формы используя анкетные данные пользователя
     * @param null|int $id
     */
    public function setUserData( $id = null ) {

        if ( !is_null( $id ) ) {

            /** @var UsersRow $oUser */
            $oUser = Users::find( \CurrentUser::getId() );
            $this->person = $oUser->name;
//            $this->postcode = $oUser->postcode;
            $this->mail = $oUser->email;
//            $this->address = $oUser->address;
            $this->phone = $oUser->phone;
        }
    }


}