<?php

namespace skewer\build\Page\Cart;

use skewer\build\Adm\Auth\ar\Users;
use skewer\build\Adm\Order as AdmOrder;
use skewer\build\Adm\Order\model\Delivery;
use skewer\build\Component\Section\Parameters;
use skewer\build\Component\Section\Tree;
use skewer\build\Tool\Payments as Payments;

use skewer\build\Page;
use skewer\build\Component\I18N\ModulesParams;
use yii\helpers\ArrayHelper;
use skewer\build\Component\Site;
use yii\web\NotFoundHttpException;

class Module extends \skModule {
    /** @var string путь до страницы с оформленным заказом */
    private static $sOrderDonePath = "/cart/done/?token=";

    /** @var string параметр для определения перехода по ссылке оплатить онлайн из письма */
    private static $sWant2PayParamName = "want2pay";

    /** @var int ID текущего раздела */
    public $sectionId;

    public $bIsFustBuy = false;

    /**
     * Инициализация
     * @return void
     */
    public function init() {
        $this->sectionId = $this->getEnvParam('sectionId');
    }

    /**
     * Запуск модуля
     * @return int
     */
    public function execute() {

        $this->executeRequestCmd();
        $action = $this->getStr('action', 'list');

        $this->bIsFustBuy = $this->getStr('ajaxShow', 0);


        if ($this->getStr('cmd', '') && $this->bIsFustBuy){
            return $this->executeAction($this->getStr('cmd'));
        }

        return $this->executeAction($action);
    }

    /**
     * Поиск и запуск определенной команды
     * @return void
     */
    private function executeRequestCmd() {

        $cmd = $this->getStr('cmd');
        $method = 'cmd'.ucfirst($cmd);
        if (method_exists($this, $method)) {
            $this->$method();
        }
    }

    /**
     * Запускает экшн
     * @param $action string Имя задания
     * @return int
     * @throws NotFoundHttpException
     */
    private function executeAction($action) {

        $method = 'action'.ucfirst($action);
        if (method_exists($this, $method)) {
            return $this->$method();
        }
        else {
            throw new NotFoundHttpException();
        }
    }

    /**
     * Список позиций заказа
     * @return int
     */
    public function actionList() {

        $this->setTemplate('list.twig');
        $this->setData('order', Api::getOrder());
        $this->setData('sectionId', $this->sectionId);
        $this->setData('mainSection', \Yii::$app->sections->main());

        return psComplete;
    }

    /**
     * Форма оформления заказа
     * @return int
     * @throws NotFoundHttpException
     */
    public function actionCheckout() {


        $aParams = array(
            'section_path' => Tree::getSectionAliasPath(\Yii::$app->sections->getValue('cart'), true),
            'license_agreement' => ModulesParams::getByName('order', 'license'),
            'fastBuy' => ($this->bIsFustBuy),
            'deliveryList' => Delivery::getList(),
        );

        if ($this->bIsFustBuy){

            $this->sectionId = \Yii::$app->sections->getValue('cart');

            $order = Api::getOrder(true);
            $order->unsetAll();
            Api::setOrder($order, true);

            $iObj = $this->getInt('idObj');

            if (!$iObj) return 0;
            $count = $this->getInt('count',1);
            if (!$count) return 0;
            Api::setItem($iObj, $count, true);
            $this->setData('fastBuy',1);
        }

        $this->setTemplate('checkout.twig');

        $fast = $this->get('fast', $this->bIsFustBuy);

        $oOrder = Api::getOrder($fast);

        $aItems = $oOrder->getItems();
        /** Не выводим форму, если заказ пустой */
        if (!$aItems || empty($aItems)){
            throw new NotFoundHttpException();
        }

        $oForm = new OrderForm();
        $oForm->setFast($fast);

        $this->setData('order', $oOrder);

        if ( ( $iUserId = \CurrentUser::getId() ) && ( \CurrentUser::getPolicyId() != \Auth::getDefaultGroupId() ) )
            $oForm->setUserData( $iUserId );

        $oUser = Users::find( \CurrentUser::getId() );

        if ($oUser->type_user != '0' ) {
            $oForm->type_form = $oUser->type_user;
            $oForm->setUser(true);
        }


        if ( $oForm->load( $this->getPost() ) && $oForm->isValid() ) {

            $oService = new AdmOrder\Service();

            $id = $oService->saveOrderForm( $oForm );
            $sToken = '';

            if ($id){
                $aOrder = AdmOrder\ar\Order::find()->where('id',$id)->asArray()->getOne();

                if (isset($aOrder['token'])){
                    $sToken = $aOrder['token'];
                }
            }

            \Yii::$app->getResponse()->redirect(\Yii::$app->router->rewriteURL("[$this->sectionId][Cart?action=done&token=$sToken]" ),'301')->send();
        }

        $this->setData( 'form', $oForm->getForm( 'OrderForm.twig', __DIR__, $aParams ) );

        // rename title and pathline
        $sTitle = \Yii::t('order', 'title_checkout' );

        Site\Page::setTitle( $sTitle );

        Site\Page::setAddPathItem( $sTitle );

        return psComplete;
    }

    /**
     * Завершение заказа
     * @return int
     */
    public function actionDone() {

        // rename title and pathline
        $sTitle =\Yii::t('order', 'title_checkout' );

        Site\Page::setTitle( $sTitle );

        Site\Page::setAddPathItem( $sTitle );

        $sToken = $this->get('token','');

        $bWant2Pay =(bool) $this->get(self::$sWant2PayParamName,'');

        if ($sToken){

            $aOrder =  AdmOrder\ar\Order::find()->where('token', $sToken)->order('id','DESC')->asArray()->getOne();

            if ($aOrder){

                $aGoods = AdmOrder\ar\Goods::find()->where('id_order', $aOrder['id'])->asArray()->getAll();

                $total = 0;
                $titleGoods = "";

                foreach ($aGoods as $item) {
                    $total += $item['total'];
                    $titleGoods .= sprintf("название: %s, кол-во: %s, цена: %s\r\n",$item['title'],$item['count'],$item['total']);
                }
                \skLogger::dump($aOrder);
                if ($total>0){

                    if ($aOrder['status'] == AdmOrder\model\Status::getIdByNew()) {

                        /**
                         * @var $oPaymentType AdmOrder\ar\TypePaymentRow
                         */
                        if ($bWant2Pay){
                            $oPaymentType = AdmOrder\ar\TypePayment::find( self::getDefaultPayment() );
                        }else{
                            $oPaymentType = AdmOrder\ar\TypePayment::find( $aOrder['type_payment'] );
                        }

                        if ($oPaymentType && $oPaymentType->payment){
                            /**
                             * @var Payments\Payment $oPayment
                             */
                            $oPayment = Payments\Api::make( $oPaymentType->payment );

                            if ($oPayment){
                                $oPayment->setOrderId($aOrder['id']);
                                $totalSum = (int)$total+(int)Delivery::getDeliveryPrice($aOrder['type_delivery']);
                                $oPayment->setSum($totalSum);

                                $this->setData( 'paymentForm', $oPayment->getForm() );
                            }
                        }

                        $sCrmToken = \SysVar::get('crm_token');
                        $sCrmEmail = \SysVar::get('crm_email');

                        if ( $sCrmToken and $sCrmEmail ) {

                            require_once(RELEASEPATH . '/core/libs/CRM/CrmSender.php');

                            $crmSender = new \CrmSender();
                            $crmSender->setToken($sCrmToken);
                            $crmSender->setEmail($sCrmEmail);
                            $crmSender->setDomain(\Yii::$app->request->getServerName());
                            //$crmSender->setDealTitle('Заказ от ' . date('d-m-Y H:i:s'));
                            $crmSender->setDealTitle('Заявка с сайта '.\Yii::$app->request->getServerName().' от '. date('d-m-Y H:i:s'));
                            $crmSender->setDealContent($titleGoods);
                            $crmSender->setContactClient(ArrayHelper::getValue($aOrder, 'person', ''));
                            $crmSender->setContactPhone(ArrayHelper::getValue($aOrder, 'phone', ''));
                            $crmSender->setContactEmail(ArrayHelper::getValue($aOrder, 'mail', ''));
                            $crmSender->setContactMobile(ArrayHelper::getValue($aOrder, 'mobile', ''));
                            $crmSender->sendMail();

                        }
                    }
                }
            }
        }

        $this->setTemplate( 'done.twig' );
        $this->setData('mainSection', \Yii::$app->sections->main());

        return psComplete;
    }

    /**
     * Добавление новой позиции в заказ
     * @return void
     */
    public function cmdSetItem() {

        $objectId = $this->getInt('objectId');
        $count = $this->getInt('count');

        if ($count && $count>0) Api::setItem($objectId, $count);

        Api::sendJSON();
    }

    /**
     * Удаление позиции заказа
     * @return void
     */
    public function cmdRemoveItem() {

        $id = $this->getInt('id');
        $order = Api::getOrder();
        $order->unsetItem($id);

        Api::setOrder($order);
        Api::sendJSON();
    }

    /**
     * Пересчет количества позиции
     * @return void
     */
    public function cmdRecountItem() {

        $id = $this->getInt('id');
        $count = $this->getInt('count');

        if (!$count || $count<0) exit;

        $order = Api::getOrder();

        /** @var OrderItem $item */
        $item = $order->getItemById($id);
        $item->setCount($count, false);

        $item->setTotal($count * Api::priceFormat($item->getPrice()));

        $order->setItem($item);

        Api::setOrder($order);
        Api::sendJSON();
    }

    /**
     * Удаление всех позиций
     * @return void
     */
    public function cmdUnsetAll() {

        $order = Api::getOrder();
        $order->unsetAll();

        Api::setOrder($order);
        Api::sendJSON();
    }

    /**
     * Запись в заказ стоимости доставки
     * @return void
     */
    public function cmdSetDeliv() {

        $deliv = $this->get('deliv');
        $fast = $this->get('fast');
        $order = Api::getOrder($fast);
        $order->setDelivPrice($deliv);
        Api::setOrder($order,$fast);
        Api::sendJSON($fast);

    }

    public function cmdChange() {

        $type_form = $this->get('type');

        $aParams = array(
            'section_path' => Tree::getSectionAliasPath(\Yii::$app->sections->getValue('cart'), true),
            'license_agreement' => ModulesParams::getByName('order', 'license'),
            'fastBuy' => ($this->bIsFustBuy),
            'deliveryList' => Delivery::getList(),
        );
        if ($this->bIsFustBuy){

            $this->sectionId = \Yii::$app->sections->getValue('cart');

            $order = Api::getOrder(true);
            $order->unsetAll();
            Api::setOrder($order, true);

            $iObj = $this->getInt('idObj');

            if (!$iObj) return 0;
            $count = $this->getInt('count',1);
            if (!$count) return 0;
            Api::setItem($iObj, $count, true);
            $this->setData('fastBuy',1);
        }

        $this->setTemplate('checkout.twig');

        $fast = $this->get('fast', $this->bIsFustBuy);

        $oOrder = Api::getOrder($fast);

        $aItems = $oOrder->getItems();
        /** Не выводим форму, если заказ пустой */
        if (!$aItems || empty($aItems)){
            throw new NotFoundHttpException();
        }

        $oForm = new OrderForm();
        $oForm->setFast($fast);

        $deliv_type = $oForm->getTypeDelivList();
        if (!empty($deliv_type)){
            $deliv = key($deliv_type);
            if ($deliv == 2){
                $oOrder->setDelivPrice($aParams['deliv_cost']['deliv_1']);
            } else if ($deliv == 4){
                $oOrder->setDelivPrice($aParams['deliv_cost']['deliv_2']);
            }
        }

        $this->setData('order', $oOrder);

        if ( ( $iUserId = \CurrentUser::getId() ) && ( \CurrentUser::getPolicyId() != \Auth::getDefaultGroupId() ) )
            $oForm->setUserData( $iUserId );

        $oForm->type_form = $type_form;
        $form = array('forms' => $oForm->getForm( 'OrderForm.twig', __DIR__, $aParams ));

        $fast = $this->get('fast');

        echo json_encode($form);
        exit;

    }

    /**
     * @return string
     */
    public static function getOrderDonePath()
    {
        return self::$sOrderDonePath;
    }

    /**
     * @return string
     */
    public static function getWant2PayParamName()
    {
        return self::$sWant2PayParamName;
    }

    /**
     * @return string|null
     */
    private static function getDefaultPayment()
    {
        return \SysVar::get('defaultMailPayment');
    }

} 