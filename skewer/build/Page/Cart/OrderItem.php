<?php

namespace skewer\build\Page\Cart;


class OrderItem {

    /** @var int ID */
    private $id;

    /** @var array Данные товара */
    private $object;

    /** @var int Цена */
    private $price;

    /** @var int Количество */
    private $count = 0;

    /** @var int Сумма */
    private $total = 0;
    private $format_total = 0;

    /**
     * @return int
     */
    public function getId() {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId($id) {
        $this->id = $id;
    }

    /**
     * @return mixed
     */
    public function getCount() {
        return $this->count;
    }

    /**
     * Устанавливает количество
     * Второй параметр управляет складываем старого и нового количества
     * @param mixed $count
     * @param bool $append
     */
    public function setCount($count, $append = true) {

        if ($append) {
            $this->count = $this->count + $count;
        }
        else {
            $this->count = $count;
        }
    }

    /**
     * @return mixed
     */
    public function getObject() {
        return $this->object;
    }

    /**
     * @param mixed $object
     */
    public function setObject($object) {
        $this->object = $object;
    }

    /**
     * Возвращает цену
     * @return mixed
     */
    public function getPrice() {
        return $this->price;
    }

    /**
     * @param mixed $price
     */
    public function setPrice($price) {
        $this->price = $price;
    }

    /**
     *
     * @return int
     */
    public function getTotal() {
        return $this->total;
    }

    /**
     * @param int $total
     */
    public function setTotal($total) {
        $this->total = $total;
    }

}