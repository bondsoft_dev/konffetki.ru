<?php

namespace skewer\build\Page\Cart;

use skewer\build\Component\Catalog;


class Api {

    const SESSION_KEY = '_cart_storage';
    const FAST_SESSION_KEY = '_fast_buy_storage';
    const PRICE_FIELDNAME = 'price';

    private static $bHidePriceFractional = false;

    private static $bFormatPriceUse = false;

    /**
     * Форматирование вывода копеек
     * @param $price
     * @return int|float
     */
    public static function priceFormat( $price ){
        if (!self::$bFormatPriceUse){
            self::$bHidePriceFractional = (bool)\SysVar::get('catalog.hide_price_fractional');
            self::$bFormatPriceUse = true;
        }
        return (self::$bHidePriceFractional)?(int)$price:(float)$price;
    }

    /**
     * Форматирование вывода копеек для запросов через ajax
     * @param $price
     * @return int|float
     */
    public static function priceFormatAjax( $price ){
        if (!self::$bFormatPriceUse){
            self::$bHidePriceFractional = (bool)\SysVar::get('catalog.hide_price_fractional');
            self::$bFormatPriceUse = true;
        }
        return (self::$bHidePriceFractional)?(int)$price:number_format((float)$price, 2, '.', ' ');
    }

    /**
     * Добавление торава в корзину
     * Если уже есть, количество складывается
     * @param $objectId int ID товара
     * @param $count int Количество
     * @param $fast bool быстрый заказ
     */
    public static function setItem($objectId, $count, $fast = false) {

        $order = self::getOrder($fast);
        $object = Catalog\GoodsSelector::get($objectId,1);//fixme #GET_CATALOG_GOODS


        if ($object['fields']['measure']['value'] == \SysVar::get('dict_kgm')) {
            $price = $object['fields']['price']['value'];
            $price_new = ($price*1.0)/10;
            $object['fields']['price']['value_full'] = $price_new;
            $object['fields']['price']['value'] = $price_new;
            $object['fields']['price']['html'] = $price_new .' '.$object['fields']['price']['attrs']['measure'];
        }



        $orderItem = $order->getExistingOrNew($objectId);
        $orderItem->setId($objectId);
        $orderItem->setCount($count);
        $orderItem->setObject($object);
        $orderItem->setPrice(self::getObjectPrice($object));

        $orderItem->setTotal($orderItem->getCount() * self::priceFormat($orderItem->getPrice()));

        $order->setItem($orderItem);

        self::setOrder($order, $fast);
        if(isset($_GET["turbo"])){
            header('Location: https://www.konffetki.ru/cart/');
            exit;
        }
    }

    /**
     * Возвращает цену товара
     * @param $object array Данные товара
     * @return int Цена
     */
    public static function getObjectPrice($object) {
        if (empty($object)) return false;
        $price = (isset($object['fields']['price']['value'])) ? $object['fields']['price']['value'] : 0;

        return self::priceFormat($price);
    }

    /**
     * Сохраняет объект заказа в сессию
     * @param Order $order Объект заказа
     * @param $fast bool быстрый заказ
     */
    public static function setOrder(Order $order, $fast = false) {
        if ($fast){
            $_SESSION[self::FAST_SESSION_KEY] = serialize($order);
        }else{
            $_SESSION[self::SESSION_KEY] = serialize($order);
        }
    }

    /**
     * Возвращает объект заказа
     * @param $fast bool быстрый заказ
     * @return Order Объект заказа
     */
    public static function getOrder($fast = false) {
        if ($fast){
            return isset($_SESSION[self::FAST_SESSION_KEY]) ? unserialize($_SESSION[self::FAST_SESSION_KEY]) : new Order();
        }else{
            return isset($_SESSION[self::SESSION_KEY]) ? unserialize($_SESSION[self::SESSION_KEY]) : new Order();
        }
    }

    /**
     * Возвращает заказ в виде массива
     * @param $fast bool быстрый заказ
     * @return array Заказ
     */
    public static function getOrderAsArray($fast) {

        $order = self::getOrder($fast);
        $orderArr = array();

        $orderArr['items'] = $order->getItemsAsArray();
        $orderArr['lastItem'] = $order->getItemsLast();
        $orderArr['count'] = $order->getTotalCount();
        $orderArr['total'] = self::priceFormatAjax($order->getTotalPrice());
        $orderArr['deliv_cost'] = $order->getDelivPrice();

        return $orderArr;
    }

    /**
     * Отсылает заказ в JSON
     * @param $fast bool быстрый заказ
     * @return void
     */
    public static function sendJSON($fast = false){
        echo json_encode(self::getOrderAsArray($fast));
        exit;
    }
}