<?php

$aLanguage = array();

$aLanguage['ru']['Cart.Page.tab_name'] = 'Корзина';

$aLanguage['ru']['checkout'] = 'Завершить оформление';
$aLanguage['ru']['title_checkout'] = 'Оформление заказа';
$aLanguage['ru']['back_to_cart'] = 'Вернуться в корзину';

$aLanguage['ru']['person'] = 'Контактное лицо';
$aLanguage['ru']['postcode'] = 'Почтовый индекс';
$aLanguage['ru']['address'] = 'Адрес доставки';
$aLanguage['ru']['phone'] = 'Телефон';
$aLanguage['ru']['mail'] = 'E-mail';
$aLanguage['ru']['tp_deliv'] = 'Способ доставки';
$aLanguage['ru']['tp_pay'] = 'Способ оплаты';
$aLanguage['ru']['source_info'] = 'Как вы узнали о нас?';
$aLanguage['ru']['accepts_the_licen'] = 'Я согласен с условиями лицензионного соглашения';
$aLanguage['ru']['text'] = 'Дополнительные пожелания';

$aLanguage['ru']['tp_deliv_1'] = 'Курьером';
$aLanguage['ru']['tp_deliv_2'] = 'Самовывоз';
$aLanguage['ru']['tp_deliv_3'] = 'Транспортная компания';

$aLanguage['ru']['paymentTypeList'] = 'Варианты оплаты';

$aLanguage['ru']['tp_pay_1'] = 'Оплата on-line';
$aLanguage['ru']['tp_pay_2'] = 'Наличными';
$aLanguage['ru']['tp_pay_3'] = 'Безналичный расчет';

$aLanguage['ru']['incor_email'] = 'Некорректно заполнено поле';
$aLanguage['ru']['accepts_the_licen_detail'] = 'Подробнее';

$aLanguage['ru']["title"] = "Наименование товара";
$aLanguage['ru']["photo"] = "Фото";
$aLanguage['ru']["article"] = "Артикул";
$aLanguage['ru']["price"] = "Цена";
$aLanguage['ru']["count"] = "Количество";
$aLanguage['ru']["count_cart"] = "Кол-во";
$aLanguage['ru']["measure_cart"] = "Ед. изм";
$aLanguage['ru']["sum"] = "Сумма";
$aLanguage['ru']["delete"] = "Удалить";
$aLanguage['ru']["back"] = "Вернуться к покупкам";
$aLanguage['ru']["checkout"] = "Оформить заказ";
$aLanguage['ru']["clear"] = "Очистить";
$aLanguage['ru']["cart_empty"] = "Корзина пуста";
$aLanguage['ru']["message_pay"] = "Для оплаты нажмите кнопку ниже.";
$aLanguage['ru']["message_pay2"] = "Заказ успешно отправлен! В ближайшее время с вами свяжется менеджер.";
$aLanguage['ru']["to_main"] = "На главную";

$aLanguage['ru']["msg_count_gt_zero"] = "Количество должно быть целым положительным числом";
$aLanguage['ru']["msg_dell_all"] = "Вы действительно хотите удалить все товары?";

$aLanguage['en']['Cart.Page.tab_name'] = 'Cart';

$aLanguage['en']['checkout'] = 'Proceed to checkout';
$aLanguage['en']['title_checkout'] = 'Proceed to checkout';
$aLanguage['en']['back_to_cart'] = 'Back to shopping cart';

$aLanguage['en']['person'] = 'Contact person';
$aLanguage['en']['postcode'] = 'Postcode';
$aLanguage['en']['address'] = 'Address';
$aLanguage['en']['phone'] = 'Phone';
$aLanguage['en']['mail'] = 'E-mail';
$aLanguage['en']['tp_deliv'] = 'Delivery method';
$aLanguage['en']['tp_pay'] = 'Payment method';
$aLanguage['en']['source_info'] = 'Source info';
$aLanguage['en']['accepts_the_licen'] = 'I have read and accepted the terms and conditions.';
$aLanguage['en']['text'] = 'Additional requests';

$aLanguage['en']['tp_deliv_1'] = 'Courier';
$aLanguage['en']['tp_deliv_2'] = 'Pickup';
$aLanguage['en']['tp_deliv_3'] = 'Transport company';

$aLanguage['en']['paymentTypeList'] = 'Payment options';

$aLanguage['en']['tp_pay_1'] = 'Оплата on-line';
$aLanguage['en']['tp_pay_2'] = 'Наличными';
$aLanguage['en']['tp_pay_3'] = 'Безналичный расчет';

$aLanguage['en']['incor_email'] = 'Field filled correctly';
$aLanguage['en']['accepts_the_licen_detail'] = 'More info';

$aLanguage['en']["title"] = "Title of product";
$aLanguage['en']["photo"] = "Photo";
$aLanguage['en']["article"] = "Article";
$aLanguage['en']["price"] = "Price";
$aLanguage['en']["count"] = "Quantity";
$aLanguage['en']["count_cart"] = "Quantity";
$aLanguage['en']["measure_cart"] = "Units";
$aLanguage['en']["sum"] = "Total";
$aLanguage['en']["delete"] = "Delete";
$aLanguage['en']["back"] = "Back";
$aLanguage['en']["checkout"] = "Proceed to checkout";
$aLanguage['en']["clear"] = "Delete all items";
$aLanguage['en']["cart_empty"] = "Your shopping cart is empty";
$aLanguage['en']["message_pay"] = "For payment, click the button below.";
$aLanguage['en']["message_pay2"] = "Your order has been successfully sent! One of our managers will contact you promptly.";
$aLanguage['en']["to_main"] = "Home";
$aLanguage['en']["msg_count_gt_zero"] = "Quantity must be a positive integer";
$aLanguage['en']["msg_dell_all"] = "Are you sure you want to delete all items?";

return $aLanguage;