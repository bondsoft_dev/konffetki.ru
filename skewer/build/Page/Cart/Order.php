<?php

namespace skewer\build\Page\Cart;


class Order {

    /** @var array Массив позиций заказа */
    private $items = array();
    private $deliv_price = 0;

    /**
     * Последний
     * @var int
     */
    private $lastItem = 0;

    /**
     * Возвращает существующую позицию или новую
     * @param $objectId int ID объекта
     * @return OrderItem Позиция заказа
     */
    public function getExistingOrNew($objectId) {
        return (isset($this->items[$objectId])) ? $this->items[$objectId] : new OrderItem();
    }

    /**
     * Устанавливает позицию заказа
     * @param OrderItem $item Позиция заказа
     */
    public function setItem(OrderItem $item) {
        $this->items[$item->getId()] = $item;
        $this->lastItem = $item->getId();
    }

    /**
     * Удаляет позицию заказа
     * @param $itemId int ID позиции
     */
    public function unsetItem($itemId) {
        if (isset($this->items[$itemId])) {
            unset($this->items[$itemId]);
        }
    }

    /**
     * Возвращает массив позиций заказа
     * @return array Позиции заказа
     */
    public function getItems() {
        return $this->items;
    }

    /**
     * Возвращает позиции заказа в виде массива
     * @return array Позиции заказа в виде массива
     */
    public function getItemsAsArray() {

        $output = array();

        /** @var OrderItem $item */
        foreach ($this->items as $item) {
            $output[] = array(
                'id' => $item->getId(),
                'count' => $item->getCount(),
                'price' => Api::priceFormatAjax($item->getPrice()),
                'total' => Api::priceFormatAjax($item->getTotal())
            );
        }

        return $output;
    }

    /**
     * Возвращает общую сумму заказа
     * @return int Итого
     */
    public function getTotalPrice() {

        $total = 0;
        /** @var OrderItem $item */
        foreach ($this->items as $item) {
            $total += (Api::getObjectPrice($item->getObject()) * $item->getCount() );
        }

        return $total;

    }

    /**
     * Красит цену
     * @param $price mixed Цена
     * @return string Цена
     */
    public static function parsePrice($price) {
        return $price;
    }

    /**
     * Возвращает объект позиции по ID
     * @param $id int ID позиции
     * @return OrderItem|bool
     */
    public function getItemById($id) {
        return isset($this->items[$id]) ? $this->items[$id] : false;
    }

    /**
     * Удаляет все позиции заказа
     * @return void
     */
    public function unsetAll() {
        $this->items = array();
    }

    /**
     * Возвращает общее количество товаров с учетом количества
     * @return int
     */
    public function getTotalCount() {

        $count = 0;

        /** @var OrderItem $item */
        foreach ($this->items as $item) {
            $count += $item->getCount();
        }

        return $count;
    }

    /**
     * Возвращает последнюю позицию в виде массива
     * @return array|bool
     */
    public function getItemsLast() {

        if (!isset($this->items[$this->lastItem])){
            return false;
        }

        /** @var OrderItem $item */
        $item = $this->items[$this->lastItem];

        $oObject = $item->getObject();

        $output = array(
            'id' => $item->getId(),
            'title' => (isset($oObject['title']))?$oObject['title']:'',
            'count' => $item->getCount(),
            'price' => Api::priceFormatAjax($item->getPrice()),
            'total' => Api::priceFormatAjax($item->getTotal())
        );

        return $output;
    }

    /**
     *
     * @return int
     */
    public function getDelivPrice() {
        return $this->deliv_price;
    }

    /**
     * @param int $deliv_price
     */
    public function setDelivPrice($deliv_price) {
        $this->deliv_price = $deliv_price;
    }

    /**
     * Возвращает общую сумму заказа c доставкой
     * @return int Итого
     */
    public function getTotalPriceDeliv() {

        $total = 0;
        /** @var OrderItem $item */
        foreach ($this->items as $item) {
            $total += (Api::getObjectPrice($item->getObject()) * $item->getCount() );
        }
        if($this->deliv_price != 0){
            $total+=$this->deliv_price;
        }

        return $total;

    }

} 