<?php

$aConfig['name']     = 'Forms';
$aConfig['title']     = 'Конструктор форм';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Модуль конструктора форм';
$aConfig['revision'] = '0002';
$aConfig['layer']     = \Layer::PAGE;
$aConfig['useNamespace'] = true;

return $aConfig;
