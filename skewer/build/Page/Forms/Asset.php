<?php

namespace skewer\build\Page\Forms;


use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle {
    
    public $sourcePath = '@skewer/build/Page/Forms/web/';

    public $css = [
        'css/custom.css',
    ];

    public $js = [
        'js/jquery.inputmask.min.js',
        'js/formValidator.js',
        'js/jquery.validate.min.js',
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public $depends = [
        'skewer\assets\JqueryAsset',
        'skewer\assets\FancyboxAsset',
        'skewer\build\Page\Main\Asset'
    ];

    public function init(){

        $this->js[] = 'js/message_' . \Yii::$app->language . '.js';

        parent::init();

    }

}