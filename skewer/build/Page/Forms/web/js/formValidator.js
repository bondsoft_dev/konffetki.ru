var bFormSubmitAllow = [];

$(function(){

    /* применяем правила валидации к полям */
    $('form').each( function() {
        var formId = this.id;
        if ( /form_(.+?)/ , formId ) { // отсекли левые формы типа поиска
            updateFromValidator( formId );
        }
    });

    /* релоад каптчи */
    $(document).on( 'click', '.img_captcha', function() {
        return reloadImg( this );
    });

    /* calendar - инициализация календарика */
    if ( typeof $.datepicker !== 'undefined' ) {
        $.datepicker.setDefaults({
            dateFormat: 'dd.mm.yy'
        });

        $('.js_init_datepicker').datepicker();

        $('body').on('click', '.js_ui-datepicker-trigger', function() {
            $( '.js_init_datepicker', $(this).parent('div') ).focus();
        });
    }

    jQuery.validator.addMethod( 'date', function( value ) {
        if ( value == '' ) return true;
        var parts = value.split('.');
        if ( parts.length != 3 ) return false;
        if( parseInt(parts[2]) < 1000 || parseInt(parts[2]) > 9999 ) return false;
        if( parseInt(parts[1]) < 1 || parseInt(parts[1]) > 12 ) return false;
        if( parseInt(parts[0]) < 1 || parseInt(parts[0]) > 31 ) return false;
        var tmpDate = new Date( parts[2], parseInt(parts[1]) - 1, parts[0], 12 );

        return !/Invalid|NaN/.test(tmpDate);
    });

    $.validator.addMethod('filesize', function(value, element, param) {
        if ($(element).attr('type') != "file"){
            return true;
        }
        if (element.files.length > 0){
            return this.optional(element) || (element.files[0].size <= param[0])
        }
        return true;
    });

    $('.agreed_readmore').fancybox();
});

function updateFromValidator ( formId ) {

    var form = $( '#' + formId );
    var sRules = $('._rules', form).val();

    if ( sRules ) {

        sRules = $.parseJSON( sRules );
        bFormSubmitAllow[ formId ] = false;

        sRules.errorPlacement = function( error, element ) {
            if (element.is('.agreed', form)) {
                $(element).next().next().after(error);
            } else {

                if ( $( '.js_errordiv_' + $(element).attr('name') ).size() )
                    $( '.js_errordiv_' + $(element).attr('name') ).html( error );
                else
                    $(element).after(error);
            }
        };


        $( form ).unbind( 'submit' );

        var oValidator = $(form).validate(sRules);

        $( form ).submit( function() {

            if ( bFormSubmitAllow[ formId ] ) return true;

            if ( oValidator.errorList.length != 0 )
                return false;

            if ( $( ".img_captcha", $(form) ).size() ) {

                var sCode = $( "input[name=captcha]", $(form) ).val();
                var sHash = $(form).attr('id');
                if ( sHash )
                    sHash = sHash.substr(5);

                $.post( '/ajax/ajax.php',{
                        moduleName: 'Forms',
                        cmd: 'captcha_ajax',
                        code: sCode,
                        hash: sHash,
                        language: $('#current_language').val()
                    },
                    function ( mCaptchaResponse ) {

                        if ( !mCaptchaResponse ) {
                            alert( 'Error: message not sent.' );
                            return false;
                        }

                        var oResponse = $.parseJSON( mCaptchaResponse );
                        var sResponse = oResponse.data.out;

                        if ( sResponse == '1' ) {

                            if ( oValidator.errorList.length != 0 )
                                return false;

                            bFormSubmitAllow[ formId ] = true;

                            if ( $( form ).attr( 'ajaxForm' ) == 1 ) {

                                sendAjaxForm( form );

                            } else {

                                $( form ).submit();
                            }
                        } else {

                            $('.img_captcha').each( function() {
                                reloadImg( this );
                            });

                            oValidator.showErrors({"captcha": sResponse});
                        }

                        return true;
                    });

                return false;

            } else {

                bFormSubmitAllow[ formId ] = true;

                if ( $( form ).attr( 'ajaxForm' ) == 1 ) {

                    sendAjaxForm( form );

                    return false;

                } else {

                    $( form ).submit();
                }

            }

            return false;
        })
    }
}

function sendAjaxForm( form ) {

    var v = $( form ).validate( {sendForm: false} );

    if ( v.errorList.length == 0 ) {

        $.ajax({
            url: '/ajax/ajax.php' + '?cmd=send' + '&moduleName=Forms' + '&ajaxForm=1'+ '&language=' + $('#current_language').val(),
            type: 'POST',
            data: new FormData(form[0]), // Добавить форму с файлами
            cache: false,
            dataType: 'json',
            processData: false,
            contentType: false,
            success: function ( mFormResponse ) {
                if ( mFormResponse ) {
                    var fancyboxDiv = $( form ).parents( '.b-form' );
                    fancyboxDiv.hide();
                    fancyboxDiv.html( '' );
                    var oResponse = mFormResponse;
                    fancyboxDiv.html( oResponse.html );
                    fancyboxDiv.show();
                    setTimeout(closeFancybox, 2000);
                }
            }
        });
    }
}

function closeFancybox() {
    $.fancybox.close();
    $('.img_captcha').each( function() {
        reloadImg( this );
    });
}

function maskInit(el){
    $('input', el).each(function(){
        switch ($(this).attr('mask')){
            case "phone":
                $(this).inputmask("mask", {"mask": "+7(999) -999- 99-99", "placeholder": "+7(___) -___- __-__"});
                break;
        }
    });
}

$(document).ready(function(){
    maskInit();
});
