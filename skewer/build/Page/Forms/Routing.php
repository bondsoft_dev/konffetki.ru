<?php

namespace skewer\build\Page\Forms;

use skewer\build\Component\Router\RoutingInterface;


/**
 * Class Routing
 * @package skewer\build\Page\Forms
 */
class Routing implements RoutingInterface {
    /**
     * Возвращает паттерны разбора URL
     * @static
     * @return bool | array
     */
    public static function getRoutePatterns() {

        return array(
            '/*response/',
        );
    }

}
