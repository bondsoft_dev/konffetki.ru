<?php

namespace skewer\build\Page\Forms;

use skewer\build\Component\ReachGoal;
use skewer\build\Component\Forms;
use skewer\build\Component\Section\Tree;
use skewer\build\Component\Site;

/**
 * Модуль вывода формы
 * Class Module
 * @package skewer\build\Page\Forms
 */
class Module extends \skModule {

    public $FormTemplate = 'form.twig';         // Шаблон отображения формы
    public $AnswersTemplate = 'answers.twig';   // Шаблон отображения ответов
    private $LetterTemplate = 'letter.twig';    // Шаблон письма
    private $ErrorTemplate = 'error.twig';      // Шаблон отображения ошибок

    public $SectionId = 0;
    public $FormId = 0;
    public $FormName = '';
    public $AjaxForm = 0;
    public $titleShow = 0;
    public $tplDir;

    public function init() {

        $this->SectionId = (int)$this->getEnvParam('sectionId');

        if ($this->tplDir) $this->oContext->setModuleDir($this->tplDir);

        $this->setData( 'moduleWebPath', $this->getModuleWebDir() );
    }

    public function execute() {

        $sCmd       = $this->getStr( 'cmd' );
        $iObjectId  = $this->getInt( 'objectId' );
        $iFormId    = $this->getInt( 'form_id' );

        // ajax обработка
        $ajaxShow = $this->get( 'ajaxShow', 0 );                                // Показывать форму через ajax
        $ajaxForm = $this->get( 'ajaxForm', $this->AjaxForm );                  // Отправлять форму через ajax
        $section = $ajaxShow ? $this->get( 'section', 0 ) : $this->SectionId;   // раздел с формой

        // Учитываем метку формы, если будет несколько форм из 1 раздела на странице
        $label = $ajaxForm ? 'out' : $this->oContext->getLabel();

        // Обработаем отправку формы только в соответствующей ей метке
        if ( $sCmd == 'send' && $label != $this->get( 'label' ) )
            $sCmd = 'show';

        switch ( $sCmd ) {

            case 'send':

                try {

                    if ($ajaxForm)
                        $section = $this->get( 'section', 0 );

                    if ( !$iFormId )
                        throw new \Exception( 'form_not_found_error' );

                    $oForm = Forms\Table::build( $iFormId, $_POST );
                    $formHash = $oForm->getHash( $section, $label , $ajaxShow );

                    if ( !$oForm->validate( $formHash ) )
                        throw new \Exception( $oForm->getError() );

                    switch ( $oForm->getHandlerType() ) {

                        case 'toMail':

                            $bRes = $oForm->send2Mail( $this->LetterTemplate, $this->getModuleDir() . $this->getTplDirectory() );

                            break;

                        case 'toMethod':
                            $bRes = $oForm->send2Method();
                            break;

                        case 'toBase':

                            $oForm->send2Base( $this->SectionId );
                            $bRes = $oForm->send2Mail( $this->LetterTemplate, $this->getModuleDir() . $this->getTplDirectory() );
                            break;

                        default:
                            throw new \Exception('handler_not_found');# обработчик неизвестен - выходим
                            break;
                    }

                    // вывести сообщение об успешной / неуспешной отправке
                    if ( !$ajaxForm && $oForm->getFormRedirect() ) {
                        \Yii::$app->getResponse()->redirect($oForm->getFormRedirect(),'301');
                        return false;// todo ???
                    }

                    // добавление обработчика ReachGoal
                    if ( $bRes ) {
                        $sTarget = $oForm->getTarget();
                        if ( $sTarget and ReachGoal\Yandex::isActive() ) {
                            $this->setData('yandexReachGoal', array(
                                'id' => rand(0, 1000),
                                'cnt' => ReachGoal\Yandex::getCounter(),
                                'target' => $sTarget
                            ));
                        }
                    }

                    $sData = $bRes ? 'success' : 'error';
                    if ( $bRes && $ajaxForm )
                        $sData = 'successAjax';

                    $this->setData( $sData, 1 );
                    $this->setData( 'form_section', $section );

                } catch ( \Exception $e ) {

                    $this->setData( $e->getMessage(), 1 );
                    $this->setTemplate( $this->ErrorTemplate );
                    return psComplete;
                }

                $this->setTemplate( $this->AnswersTemplate );
                break;

            case 'captcha_ajax':

                if ( \Captcha::check( $this->getStr( 'code' ), $this->getStr( 'hash' ), false ) ) {

                    $this->setData( 'out', 1 );

                } else {

                    $sMessage = \Yii::t('forms', 'wrong_captcha' );
                    $this->setData( 'out', $sMessage );
                }

                return psRendered;
                break;

            case 'show':
            default:

                $sDetailUrl = '';
                $oContent = Site\Page::getMainModule();
                if ( $oContent ) {
                    if( !$oContent->isComplete() )
                        return psWait;
                    $sDetailUrl = $oContent->getUsedURL();
                }

                if ( $this->FormName )
                    $oFormRow = Forms\Table::getByName( $this->FormName );
                else
                    $oFormRow = Forms\Table::get4Section( $section );

                if ( !( $oFormRow instanceof Forms\Row ) ) {

                    //$this->setData( 'form_not_found_error', 1 );
                    //$this->setTemplate( $this->ErrorTemplate );

                    return psBreak;

                } else {

                    /** @var Forms\Entity $oForm */
                    $oForm = Forms\Table::build( $oFormRow->form_id );

                    if ( $iObjectId )
                        $oForm->fillGoodsFields( $iObjectId );

                    $this->setData( 'oForm', $oForm );
                    $this->setData( 'iRandVal', rand(0, 1000) );
                    $this->setData( 'section', $section );
                    $this->setData( 'form_section_path', '' );

                    $this->setData( 'form_section_path', Tree::getSectionAliasPath($section, true) . $sDetailUrl );

                    $this->setTemplate( $this->FormTemplate );

                    $this->setData('titleShow', $this->titleShow);
                    if ( $ajaxShow ){
                        $this->setData( 'ajaxShow', true );
                        $this->setData('titleShow', true);
                    }

                    if ( $ajaxForm ) {

                        $this->setData( 'ajaxForm', true );
                        $this->setData( 'label', 'out' );

                    } else {

                        // Установим метку для отправления формы. 'forms' по умолчанию, если ничего не передано
                        $labelOut = $this->get( 'label', $label == 'out' ? 'forms' : $label );
                        $label = $labelOut;
                        $this->setData( 'label', $labelOut );

                    }

                    $this->setData( 'formHash', $oForm->getHash( $section, $label, $ajaxShow ) );

                }

                break;
        }


        return psComplete;
    }
}