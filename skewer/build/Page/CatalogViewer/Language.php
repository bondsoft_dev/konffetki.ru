<?php

$aLanguage = array();

$aLanguage['ru']['CatalogViewer.Page.tab_name'] = 'Просмотр каталога';
$aLanguage['ru']['param_on_page'] = 'Кол-во товарных позиций';
$aLanguage['ru']['param_title_on_main'] = 'Заголовок на главной';
$aLanguage['ru']['param_title'] = 'Заголовок';

$aLanguage['ru']['in_basket'] = 'В корзину';
$aLanguage['ru']['buy'] = 'Купить'; // todo удалить в 21+
$aLanguage['ru']['order'] = 'Заказать';

$aLanguage['ru']['all_description'] = 'Все описание';

$aLanguage['ru']['back_to_products'] = 'Вернуться к списку';

$aLanguage['ru']['empty_section'] = 'Приносим свои извинения, раздел находится в стадии наполнения';
$aLanguage['ru']['not_found_by_filter'] = 'По вашему запросу ничего не найдено. Измените параметры запроса.';

$aLanguage['ru']['filter_order_by'] = 'Сортировать';
$aLanguage['ru']['filter_order_default'] = 'Сбросить сортировку';

$aLanguage['ru']['view_gallery'] = 'Галерея';
$aLanguage['ru']['view_list'] = 'Список';

$aLanguage['ru']['showSort'] = 'Показывать сортировку в списке';
$aLanguage['ru']['showFilter'] = 'Показывать фильтр в списке';

$aLanguage['ru']['relatedTpl'] = 'Шаблон для сопутствующих товаров';
$aLanguage['ru']['includedTpl'] = 'Шаблон для вывода комплектов';
$aLanguage['ru']['pre_order'] = 'Под заказ';

/* en */
$aLanguage['en']['pre_order'] = 'Pre-order';
$aLanguage['ru']["related_product"] = "Сопутствующие товары";
$aLanguage['ru']["included_product"] = "В комплекте";
$aLanguage['ru']["count"] = "Кол-во";
$aLanguage['ru']["reviews"] = "Отзывы";


$aLanguage['en']['CatalogViewer.Page.tab_name'] = 'Catalog viewer';
$aLanguage['en']['param_on_page'] = 'Count on page';
$aLanguage['en']['param_title_on_main'] = 'Title on main ';
$aLanguage['en']['param_title'] = 'Title ';
$aLanguage['en']['order'] = 'Order';


$aLanguage['en']['buy'] = 'Buy'; // todo удалить в 21+

$aLanguage['en']['buy_in_click'] = 'Buy in one click';
$aLanguage['en']['all_description'] = 'All description';

$aLanguage['en']['back_to_products'] = 'Back to list';

$aLanguage['en']['empty_section'] = 'We apologize section is under construction';
$aLanguage['en']['not_found_by_filter'] = 'Search no results. Modify the query.';

$aLanguage['en']['filter_order_by'] = 'Order by';
$aLanguage['en']['filter_order_default'] = 'Reset sort Order';

$aLanguage['en']['view_gallery'] = 'Gallery';
$aLanguage['en']['view_list'] = 'List';

$aLanguage['en']['in_basket'] = 'Add to cart';

$aLanguage['en']["related_product"] = "Related products";
$aLanguage['en']["included_product"] = "Included products";
$aLanguage['en']["count"] = "Quantity";
$aLanguage['en']["reviews"] = "Feedback";

$aLanguage['en']['showSort'] = 'Show Sort the list';
$aLanguage['en']['showFilter'] = 'Show filter in the list';

$aLanguage['en']['relatedTpl'] = 'Template for related items';
$aLanguage['en']['includedTpl'] = 'Template for included items';

return $aLanguage;