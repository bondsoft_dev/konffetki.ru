<?php

namespace skewer\build\Page\CatalogViewer\State;

use skewer\build\Component\Catalog;
use skewer\build\Component\Site;
use skewer\build\Component\SEO\Api;
use skewer\build\libs\ft;
use yii\helpers\ArrayHelper;


/**
 * Объект вывода списка товарных позиций для старницы бренда
 * Class CollectionPage
 * @package skewer\build\Page\CatalogViewer\State
 */
class CollectionPage extends ListPage {

    /** @var int Идентификатор позиции в коллекции */
    protected $collection = 0;

    /** @var bool Имя карточки коллекции */
    private $card = false;

    /** @var bool Имя поля в карточке товара */
    private $field = false;

    /** @var array */
    private $obj = [];

    /** @var string Шаблон вывода */
    protected $sMainTpl = 'CollectionPage.twig';


    /**
     * Инициализация объекта коллекции по идентификатору
     * @param int|string $collection id or alias
     * @return bool
     */
    public function setCollectionId( $collection ) {

        try {

            list($this->card, $this->field) = explode( ':', $this->getModuleField( 'collectionField' ) );

            $this->obj = Catalog\ObjectSelector::get( $collection, $this->card );

            if ( !$this->obj )
                return false;// todo exception

            $this->collection = ArrayHelper::getValue( $this->obj, 'id', 0 );

        } catch ( \Exception $e ) {
            return false;
        }

        return true;
    }


    /**
     * Получение списка товарных позиций для текущей страницы
     * @return bool
     */
    protected function getGoods() {

        try {

            $oSelector = Catalog\GoodsSelector::getList4Collection( $this->card, $this->field, $this->collection );

            if ( !$oSelector ) return false;

            $this->list = $oSelector
                ->condition( 'active', 1 )
                ->sort( $this->sSortField, ($this->sSortWay == 'down' ? 'DESC' : 'ASC') )
                ->limit( $this->iCount, $this->iPageId, $this->iAllCount )
                ->parse()
            ;

        } catch ( \Exception $e ) {
            return false;
        }

        return true;
    }


    public function build() {

        if ( !$this->obj )
            return false;// todo exception

        $this->getModule()->setData( 'useMainSection', 1 );

        // todo придется перенести код и адаптировать под текущую механику
        parent::build();

        //$this->showNearItems();

        $this->getModule()->setData( 'collection', $this->obj );
        $this->getModule()->setData( 'view', $this->sTpl );

        $this->getModule()->setTemplate( $this->sMainTpl );

        $this->setSEOLabels( $this->obj );
        Site\Page::setTitle( $this->obj['title'] );
        Site\Page::setAddPathItem( $this->obj['title'] );

        return true;
    }


    private function toLower( $str ) {
        $str = trim($str);
        $str = explode( ' ', $str );
        $str[0] = mb_convert_case( $str[0], MB_CASE_LOWER );
        $str = implode( ' ', $str );
        return $str;
    }

    /**
     * Добавление SEO парамертов
     * @param $aObject
     * @return int
     */
    private function setSEOLabels( $aObject ) {

        // Убрать статический контент
        $oPage = Site\Page::getRootModule();
        if ( !$oPage->isComplete() )
            return psWait;
        Site\Page::clearStaticContent();
        Site\Page::clearStaticContent2();

        // Метатеги для списка новостей
        $SEOData = $this->oModule->getEnvParam('SEOTemplates', []);

        $sSEOTpl = Api::TPL_CATALOG_COLLECTION_ELEMENT;

        $aSEOData = [
            'title' => ArrayHelper::getValue($aObject, 'fields.tag_title.value', ''),
            'description' => strip_tags( ArrayHelper::getValue($aObject, 'fields.description.value', '') ),
            'keywords' => ArrayHelper::getValue($aObject, 'fields.keywords.value', '')
        ];

        $SEOData[ $sSEOTpl ] = $aSEOData;

        $this->oModule->setEnvParam('SEOTemplates', $SEOData);
        $SEOVars = $this->oModule->getEnvParam( 'SEOVars', [] );
        $SEOVars['label_collection_title_upper'] = $aObject['title'];
        $SEOVars['label_collection_element_title_upper'] = $aObject['title'];
        $SEOVars['label_collection_element_title_lower'] = $this->toLower( $aObject['title'] );

        $sTitle = Catalog\Card::getTitle( $this->card );

        $SEOVars['label_collection_title_lower'] = $sTitle;
        $SEOVars['label_collection_title_upper'] = $sTitle;
        $SEOVars['label_collection_title_lower'] = $this->toLower( $sTitle                          );

        $this->oModule->setEnvParam('SEOVars', $SEOVars);


        Site\Page::reloadSEO();

        return true;
    }


    /**
     * Вывод PathLine
     */
    protected function setPathLine() {

        // todo и все таки параметры должны браться из запросника после обработки
        $aURLParams = $_GET;
        unSet( $aURLParams['page'] );
        unSet( $aURLParams['url'] );

        $aURLParams['goods-alias'] = ArrayHelper::getValue( $this->obj, 'alias', 0 );// fixme изза этой строчку дублировал метод

        foreach( $aURLParams as $sKey=>$mParam )
            if ( is_array( $mParam ) ) {

                foreach( $mParam as $sWKey=>$mValue )
                    $aURLParams[ $sKey . '[' .$sWKey . ']' ] = $mValue;

                unSet( $aURLParams[$sKey] );
            }

        if ( $this->sSortField )
            $aURLParams['sort'] = $this->sSortField;

        if ( $this->sSortWay )
            $aURLParams['way'] = $this->sSortWay;

        if ( $this->sTpl )
            $aURLParams['view'] = $this->sTpl;

        $this->getModule()->getPageLine(
            $this->iPageId,
            $this->iAllCount,
            $this->getModuleField( 'iCurrentSection' ),
            $aURLParams,
            ['onPage' => $this->iCount]
        );

    }

    protected function showNearItems() {

        $aData = [
            'section' => $this->getModuleField( 'iCurrentSection' ),
            'next' => Catalog\ObjectSelector::getNext( $this->collection, $this->card ),
            'prev' => Catalog\ObjectSelector::getPrev( $this->collection, $this->card )
        ];

        $this->oModule->setData( 'nearItems', $aData );
    }


}