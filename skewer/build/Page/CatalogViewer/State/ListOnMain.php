<?php

namespace skewer\build\Page\CatalogViewer\State;


use skewer\build\Component\Catalog;
use skewer\build\Component\Section\Page;
use skewer\build\libs\ft;
use skewer\build\Page\CatalogViewer;
use skewer\build\Component\Site;
use yii\helpers\ArrayHelper;


class ListOnMain extends Prototype {

    /** @var array Набор товарных позиций для вывода */
    private $aGoods = array();

    /** @var int Всего найдено товарных позиций */
    private $iAllCount = 0;

    /** @var int Кол-во позиций на страницу */
    private $iCount = 3;

    /** @var string Шаблон для вывода */
    private $sTpl = 'gallery';

    /** @var array Набор шаблонов для каталога */
    private $aTemplates = [
        'list' => 'OnMain.list.twig',
        'gallery' => 'OnMain.gallery.twig',
    ];


    public function init() {

        $this->iCount = $this->getModuleField( 'onPage', $this->iCount );

        $sTpl = Page::getVal( 'catalog', 'type_catalog_view' );

        if ($sTpl && $sTpl == 'list'){
            $this->sTpl = 'OnMain.list.twig';
        }

        // получение набора товаров
        if ( $this->iCount > 0 )
            $this->getGoods();

    }


    public function build() {

        // парсинг
        $this->oModule->setData( 'aObjectList', $this->aGoods );
        $this->oModule->setData( 'titleOnMain', $this->getModuleField( 'titleOnMain' ) );
        $this->oModule->setData( 'form_section', $this->getModuleField( 'buyFormSection' ) );
        $this->oModule->setData( 'useCart', Site\Type::isShop() );
        $this->oModule->setData( 'moduleGroup', $this->getModuleGroup() );

        // шаблон
        $this->sTpl = $this->getModuleField( 'listTemplate' );
        $this->sTpl = ArrayHelper::getValue( $this->aTemplates, $this->sTpl, $this->aTemplates['list'] );
        $this->oModule->setTemplate( $this->sTpl );
    }


    /**
     * Получение списка товарных позиций для текущей страницы
     * @return bool
     */
    private function getGoods() {

        $field = $this->getModuleField( 'onMain' );

        if ( !in_array( $field, ['hit', 'new', 'on_main'] ) )
            $field = 'on_main';

        $this->aGoods = Catalog\GoodsSelector::getList( Catalog\Card::DEF_BASE_CARD )
            ->condition( 'active', 1 )
            ->condition( $field, 1 )
            ->limit( $this->iCount, 1, $this->iAllCount )
            ->sortByRand()
            ->parse()
        ;

        return true;
    }


}