<?php

namespace skewer\build\Page\CatalogViewer\State;


use skewer\build\Component\Catalog;
use skewer\build\Component\Gallery\Photo;
use skewer\build\Component\SEO\Api;
use skewer\build\Page\CatalogViewer;
use skewer\build\Component\Site;
use skewer\build\Component\Section\Tree;
use skewer\build\Page\CatalogViewer as CatView;
use skewer\build\Tool\Review;
use skewer\build\libs\ft;


class DetailPage extends Prototype {

    /** @var \skewer\build\Component\Catalog\GoodsRow AR товарной позиции */
    private $oGoods = null;

    /** @var string Шаблон вывода */
    private $sTpl = 'SimpleDetail.twig';

    /** @var string Псевдоним текущего товара */
    private $sGoodsAlias = '';

    /** @var int ID текущего товара */
    private $iGoodsId = 0;

    protected $bShowFilter = false;


    public function init() {

        $sBaseCardName = Catalog\Card::DEF_BASE_CARD; // fixme переделать при нескольких базовых карточек

        $this->oGoods = Catalog\GoodsSelector::get( $this->sGoodsAlias ? $this->sGoodsAlias : $this->iGoodsId , $sBaseCardName );

        // проверка на существование и активность
        if ( empty($this->oGoods) || !$this->oGoods['active'] )
            throw new CatView\Exception('Не найдена товарная позиция');

        // проверка на раздел
        $aSectionList = Catalog\Section::getList4Goods( $this->oGoods['main_obj_id'], $this->oGoods['base_card_id'] );
        if ( !is_array($aSectionList) || !in_array( $this->getSection(), $aSectionList ) )
            throw new CatView\Exception('Не найдена товарная позиция');

        $this->getModule()->addJSFile(\Yii::$app
            ->getAssetManager()
                ->getBundle( CatalogViewer\Asset::className() )
                ->baseUrl . '/js/detail.js'
        );

    }


    public function build() {

        $iObjectId = $this->oGoods['id'];
        $iMainObjId = isSet( $this->oGoods['main_obj_id'] ) && $this->oGoods['main_obj_id'] ? $this->oGoods['main_obj_id']: $iObjectId;

        //картинки для стран
        if (isset($this->oGoods['fields']['strana']) && !empty($this->oGoods['fields']['strana']['item'])){
            foreach ($this->oGoods['fields']['strana']['item'] as $key=>$item) {
                $oGallery = Photo::getFromAlbum($item['foto'], false, 1);
                if (isset($oGallery[0]['images_data']['country']))
                    $country[$key]['photo'] =  $oGallery[0]['images_data']['country']['file'];
            }
        }

        //Берем ссылку на родительскую секцию и ее название в данный модуль, для SEO-текстов
        $bsParentTemp = Tree::getSection($this->getSection(), true);
        if(isset($bsParentTemp['title']) && !empty($bsParentTemp['title']) && isset($bsParentTemp['alias_path']) && !empty($bsParentTemp['alias_path'])) {
            $this->oGoods['fields']['bsseo']['bsSectionTitle'] = $bsParentTemp['title'];
            $this->oGoods['fields']['bsseo']['bsSectionHref'] = $bsParentTemp['alias_path'];
        }
        
        if ($this->oGoods['fields']['measure']['value'] == \SysVar::get('dict_kgm')) {
            $price = $this->oGoods['fields']['price']['value'];
            $price_new = ($price*1.0)/10;
            $this->oGoods['fields']['price']['value_full'] = $price_new;
            $this->oGoods['fields']['price']['value'] = $price_new;
            $this->oGoods['fields']['price']['html'] = $price_new .' '.$this->oGoods['fields']['price']['attrs']['measure'];
        }


        if ( true ) // модуль ближайших товаров
            $this->showNearItems( $iObjectId );

        // товары аналоги
        if ( \SysVar::get('catalog.goods_analogs') )
            $this->showAnalogItems( $iMainObjId, $iObjectId );

        // товары идущие в комплекте товаров
        if ( \SysVar::get('catalog.goods_include') )
            $this->showIncludedItems( $iObjectId );

        // модуль сопутствующих товаров
        if ( \SysVar::get('catalog.goods_analog') )
            $this->showRelatedItems( $iObjectId );

        // отдаем данные на парсинг
        $this->oModule->setData( 'aObject', $this->oGoods );
        if (isset($country))  $this->oModule->setData( 'country', $country );
        $this->oModule->setData( 'aTabs', $this->buildTabs() );
        $this->oModule->setData( 'form_section', $this->getModuleField( 'buyFormSection' ) );
        $this->oModule->setData( 'useCart', Site\Type::isShop() );
        $this->oModule->setData( 'isMainObject', $iMainObjId == $iObjectId );
        $this->oModule->setData( 'hide2lvlGoodsLinks', \SysVar::get('catalog.hide2lvlGoodsLinks') );
        $this->oModule->setData( 'hideBuy1lvlGoods', \SysVar::get('catalog.hideBuy1lvlGoods') );



        //Получаем сопутствующие товары (самопал)
        $curSection = $this->getModuleField( 'iCurrentSection' );
        $this->oModule->setData( 'relatedProducts', Catalog\GoodsSelector::getSimilarToProducts($curSection));

        $this->oModule->setTemplate( $this->sTpl );

        $this->setSEOLabels( $this->oGoods );
        Site\Page::setTitle( $this->oGoods['title'] );
        Site\Page::setAddPathItem( $this->oGoods['title'] );

    }


    /**
     * Поиск товара по идентификаторам
     * @param int $iGoodsId
     * @param string $sGoodsAlias
     * @return bool
     */
    public function findGoods( $iGoodsId, $sGoodsAlias = '' ) {

        $this->iGoodsId = $iGoodsId;
        $this->sGoodsAlias = $sGoodsAlias;

        return true;
    }


    /**
     * Добавление SEO парамертов
     * @param $aObject
     * @return int
     */
    private function setSEOLabels( $aObject ) {

        // Убрать статический контент
        $oPage = Site\Page::getRootModule();
        if ( !$oPage->isComplete() )
            return psWait;
        Site\Page::clearStaticContent();
        Site\Page::clearStaticContent2();

        // Метатеги для списка новостей
        $SEOData = $this->oModule->getEnvParam('SEOTemplates', array());

        $sCardName = isSet($aObject['card']) ? $aObject['card'] : false;
        $sSEOTpl = Api::TPL_CATALOG;

        $aSEOData = array(
            'title' => isSet( $aObject['seo']['title'] ) ? $aObject['seo']['title'] : '',
            'description' => isSet( $aObject['seo']['description'] ) ? strip_tags($aObject['seo']['description']) : '',
            'keywords' => isSet( $aObject['seo']['keywords'] ) ? $aObject['seo']['keywords'] : ''
        );

        $SEOData[ $sSEOTpl ] = $aSEOData;
        $SEOData[ $sSEOTpl . ':' . $sCardName ] = $aSEOData;

        // если товар - модификация, нужно добавить шаблон аналогов
        if ( $aObject['main_obj_id'] != $aObject['id'] )
            $SEOData[ Api::TPL_CATALOG2LAYER ] = $aSEOData;

        $this->oModule->setEnvParam('SEOTemplates', $SEOData);
        $SEOVars = $this->oModule->getEnvParam( 'SEOVars', array() );
        $SEOVars['label_catalog_title_upper'] = $aObject['title'];

        $sTitle = $aObject['title'];
        if ($sTitle){
            $sTitle = trim($sTitle);
            $aTitle = explode( ' ', $sTitle );
            $aTitle[0] = mb_convert_case( $aTitle[0], MB_CASE_LOWER );
            $sTitle = implode( ' ', $aTitle );
        }

        $SEOVars['label_catalog_title_lower'] = $sTitle;
        //$SEOVars['Название категории'] = $this->oModule->viewCategory;
        //$SEOVars['название категории'] = mb_strtolower($SEOVars['Название категории']);

        // добавление всех полей
        if ( isSet( $aObject['fields'] ) && $aObject['fields'] )
            foreach ( $aObject['fields'] as $sFieldName => $aField )
                if ( isSet( $aField['title'] ) && isSet( $aField['html'] ) )
                    $SEOVars[ $aField['title'] ] = strip_tags($aField['html']);

        $this->oModule->setEnvParam('SEOVars', $SEOVars);

        if (isset($aObject['canonical_url'])){
            $oPage->setData('canonical_url', $aObject['canonical_url']);
        }

        Site\Page::reloadSEO();

        return true;
    }

    /**
     * Выводит модуль перехода на ближайшие элементы
     * @param $iObjectId
     * todo протащить поле для сотритовки и фильтр
     */
    protected function showNearItems( $iObjectId ) {

        $aData = array(
            'section' => $this->getModuleField( 'iCurrentSection' ),
            'next' => Catalog\GoodsSelector::getNext( $iObjectId, $this->getModuleField( 'iCurrentSection' ) ),
            'prev' => Catalog\GoodsSelector::getPrev( $iObjectId, $this->getModuleField( 'iCurrentSection' ) )
        );

        $this->oModule->setData('nearItems', $aData );
    }


    /**
     * Вывод модщуля сопутствующих товаров
     * @param $iObjectId
     */
    private function showRelatedItems( $iObjectId ) {

        $sTpl = $this->getModuleField( 'relatedTpl' );
        if ( !in_array( $sTpl, array( 'list', 'gallery', 'table' ) ) )
            $sTpl = 'gallery';

        $aObjectList = Catalog\GoodsSelector::getRelatedList( $iObjectId )
            ->condition( 'active', 1 )
            ->parse()
        ;

        if ( count($aObjectList) ) {
            $aData['section'] = $this->getModuleField( 'iCurrentSection' );

            $this->oModule->setData( 'relatedTpl', $sTpl );
            $this->oModule->setData( 'relatedItems', $aData );
            $this->oModule->setData( 'aRelObjList', $aObjectList );
        }

    }


    /**
     * Вывод товаров из комплекта
     * @param $iObjectId
     */
    private function showIncludedItems( $iObjectId ) {

        $sTpl = $this->getModuleField( 'includedTpl' );
        if ( !in_array( $sTpl, array( 'list', 'gallery', 'table' ) ) )
            $sTpl = 'gallery';

        $aObjectList = Catalog\GoodsSelector::getIncludedList( $iObjectId )
            ->condition( 'active', 1 )
            ->parse()
        ;

        if ( count($aObjectList) ) {
            $aData['section'] = $this->getModuleField( 'iCurrentSection' );

            $this->oModule->setData( 'includedTpl', $sTpl );
            $this->oModule->setData( 'includedItems', $aData );
            $this->oModule->setData( 'aIncObjList', $aObjectList );
        }

    }


    /**
     * Вывод товаров аналогов
     * @param int $iMainObjId Ид основного товара
     * @param int $iObjectId Ид текущего товара
     */
    private function showAnalogItems( $iMainObjId, $iObjectId ) {

        $aObjectList = Catalog\GoodsSelector::getModificationList( $iMainObjId, $iObjectId )
            ->condition( 'active', 1 )
            ->parse()
        ;

        if ( count($aObjectList) ) {
            $aData['section'] = $this->getModuleField( 'iCurrentSection' );

            $this->oModule->setData( 'analogsItems', $aData );
            $this->oModule->setData( 'aAngObjList', $aObjectList );
        }

    }


    /**
     * Вывод отзывов к товару
     * @return array
     */
    private function showReviews() {

        $oReviews = new \skContext( 'GoodsReviewsModule', 'GuestBook', ctModule, array( 'className' => Review\Api::GoodReviews, 'objectId' => $this->oGoods['id'],'actionForm'=>'#tabs-reviews' ) );
        $oReviewsProcess = $this->oModule->addChildProcess( $oReviews );
        $oReviewsProcess->execute();
        $oReviewsProcess->render();

        return array(
            'name' => 'reviews',
            'title' => \Yii::t('catalog', 'reviews'),
            'html' =>  $oReviewsProcess->getOut()
        );
    }


    private function buildTabs() {

        $aTabs = array();

        foreach( $this->oGoods['fields'] as $oField )
            if ( !in_array( $oField['type'], [ft\Editor::GALLERY] ) )// fix убираем поле, пока непонятно как его выводить
            if ( isSet( $oField['attrs']['show_in_tab'] ) && $oField['attrs']['show_in_tab'] && $oField['value']  )
                $aTabs[] = array(
                    'name' => $oField['name'],
                    'title' => $oField['title'],
                    'html' => $oField['html'],
                );

        // модуль вывода отзывов
        if ( $this->oModule->getModuleField( 'showReviews' ) || \SysVar::get('catalog.guest_book_show') )
            $aTabs[] = $this->showReviews();

        return $aTabs;
    }

}