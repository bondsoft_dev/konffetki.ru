<?php

namespace skewer\build\Page\CatalogViewer\State;

use skewer\build\Component\Site;
use skewer\build\Component\Catalog;
use skewer\build\Component\SEO\Api;
use skewer\build\libs\ft;
use yii\helpers\ArrayHelper;


class CollectionList extends ListPage {

    /** @var array Набор позиций для вывода */
    protected $list = [];

    /** @var string Шаблон для вывода */
    private $sMainTpl = 'CollectionList.twig';

    /** @var int  */
    private $card = 0;

    /** @var int  */
    private $field = 0;


    public function init() {

        // постраничный
        $this->iPageId = $this->getModule()->getInt( 'page', 1 );
        $this->iCount = $this->getModuleField( 'onPage', $this->iCount );

        list($this->card, $this->field) = explode( ':', $this->getModuleField( 'collectionField' ) );

        try {

            // получение позиций
            $oSelector = Catalog\ObjectSelector::getCollections( $this->card, $this->field );

            if (!$oSelector) return false;

            $this->list = $oSelector
                ->condition( 'active', 1 )
                //            ->sort( $this->sSortField, ($this->sSortWay == 'down' ? 'DESC' : 'ASC') )
                //            ->limit( $this->iCount, $this->iPageId, $this->iAllCount )
                ->parse()
            ;

        } catch ( \Exception $e ) {

            return false;
        }

        return true;
    }


    public function build() {

        if ( empty($this->list) ) {

            if ( $this->bFilterUsed ) {
                $this->getModule()->setTemplate( 'NotFound.twig' );
                return;
            }

            $oPage = Site\Page::getRootModule();
            $aStaticContent = $oPage->getData('staticContent');
            $sText = ArrayHelper::getValue( $aStaticContent, 'text', '');
            if ( \Html::hasContent($sText) ) {
                return;
            }

            $this->getModule()->setTemplate( 'Empty.twig' );
            return;
        }


        // парсинг
        $this->getModule()->setData( 'section', $this->getModule()->getEnvParam('sectionId') );
        $this->getModule()->setData( 'aObjectList', $this->list );
        $this->getModule()->setData( 'form_section', $this->getModuleField( 'buyFormSection' ) );
        $this->getModule()->setData( 'useCart', Site\Type::isShop() );

        // шаблон
        $this->getModule()->setTemplate( $this->sMainTpl );

        // постраничник
        //$this->setPathLine();

        $this->setSEOLabels();
    }


    private function toLower( $str ) {
        $str = trim($str);
        $str = explode( ' ', $str );
        $str[0] = mb_convert_case( $str[0], MB_CASE_LOWER );
        $str = implode( ' ', $str );
        return $str;
    }


    /**
     * Добавление SEO парамертов
     * @return int
     */
    private function setSEOLabels() {

        // Убрать статический контент
        $oPage = Site\Page::getRootModule();
        if ( !$oPage->isComplete() )
            return psWait;
        //Site\Page::clearStaticContent();
        //Site\Page::clearStaticContent2();

        // Метатеги для списка новостей
        $SEOData = $this->oModule->getEnvParam('SEOTemplates', []);

        $sSEOTpl = Api::TPL_CATALOG_COLLECTION;

        // todo возможно сюда нужно записать данные из раздела
        $aSEOData = [
            'title' => '',
            'description' => '',
            'keywords' => ''
        ];

        $SEOData[ $sSEOTpl ] = $aSEOData;

        $this->oModule->setEnvParam('SEOTemplates', $SEOData);
        $SEOVars = $this->oModule->getEnvParam( 'SEOVars', [] );

        $sTitle = Catalog\Card::getTitle( $this->card );

        $SEOVars['label_collection_title_upper'] = $sTitle;
        $SEOVars['label_collection_title_lower'] = $this->toLower( $sTitle );

        $this->oModule->setEnvParam('SEOVars', $SEOVars);


        Site\Page::reloadSEO();

        return true;
    }




}