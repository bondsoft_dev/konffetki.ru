<?php

namespace skewer\build\Page\CatalogViewer\State;

use \skewer\build\Page\CatalogViewer;

abstract class Prototype {

    /** @var \skewer\build\Page\CatalogViewer\Module  */
    protected $oModule = null;

    /** @var bool Флаг вывода формы фильтрации */
    protected $bShowFilter = true;

    function __construct( CatalogViewer\Module $oModule ) {
        $this->oModule = $oModule;
    }


    protected function getModuleField( $sFieldName, $mDef = null ) {

        return $this->oModule->getModuleField( $sFieldName, $mDef );
    }


    /**
     * Получение имени группы в которой находится метка модуля
     * @return string
     */
    protected function getModuleGroup() {
        return $this->oModule->getLabel();
    }

    protected function getModule() {
        return $this->oModule;
    }


    abstract public function init();


    abstract public function build();


    final public function show() {
        $this->init();
        $this->build();
    }

    protected function getSection() {
        return $this->getModule()->iCurrentSection;
    }

    public function showFilter() {
        return $this->bShowFilter;
    }

} 