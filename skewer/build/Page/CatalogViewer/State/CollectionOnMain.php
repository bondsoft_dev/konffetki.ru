<?php

namespace skewer\build\Page\CatalogViewer\State;

use skewer\build\Component\Catalog;
use skewer\build\libs\ft\Cache;
use skewer\models\Parameters;
use yii\helpers\ArrayHelper;


class CollectionOnMain extends Prototype {

    protected $sTpl = 'list';

    protected $list = [];

    /** @var array Набор шаблонов для каталога */
    private $aTemplates = [
        'list' => 'CollectionOnMain.list.twig',
        'slider' => 'CollectionOnMain.slider.twig',
    ];


    public function init() {

        // fixme задублировано из парсера
        $query = Parameters::find()->where(['name' => 'collectionField']);
        foreach ( $query->each() as $param ) {
            list($card, $field) = explode( ':', $param->value );
            if ( !($model = Cache::softGet( $card )) )
                continue;
            $this->oModule->setData( 'section', $param->parent );

            $this->list = Catalog\ObjectSelector::getCollections( $card )
                ->condition('on_main', 1)
                ->condition('active', 1)
                ->parse();
        }

        // todo обработка нескольких коллекций
    }


    public function build() {

        // парсинг
        $this->oModule->setData( 'aObjectList', $this->list );
        $this->oModule->setData( 'titleOnMain', $this->getModuleField( 'titleOnMain' ) );
        $this->oModule->setData( 'moduleGroup', $this->getModuleGroup() );

        // шаблон
        $this->sTpl = $this->getModuleField( 'listTemplate' );
        $this->sTpl = ArrayHelper::getValue( $this->aTemplates, $this->sTpl, $this->aTemplates['list'] );
        $this->oModule->setTemplate( $this->sTpl );
    }
}