<?php

namespace skewer\build\Page\CatalogViewer\State;


use skewer\build\Component\Gallery\Photo;
use skewer\build\libs\ft;
use skewer\build\Page\CatalogViewer;
use skewer\build\Component\Site;
use skewer\build\Component\Cache;
use skewer\build\Component\Catalog;


/**
 * Объект вывода списка товарных позиций
 * Class ListPage
 * @package skewer\build\Page\CatalogViewer\State
 */
class ListPage extends Prototype {

    /** @var array Набор товарных позиций для вывода */
    protected $list = [];

    /** @var int Всего найдено товарных позиций */
    protected $iAllCount = 0;

    /** @var int Номер страницы */
    protected $iPageId = 1;

    /** @var int Кол-во позиций на страницу */
    protected $iCount = 12;

    /** @var int Название категории */
    private $iCategory = '';

    /** @var string Шаблон для вывода */
    protected $sTpl = '';

    /** @var string Поле для сортировки */
    protected $sSortField = '';

    /** @var string Напровление сортировки */
    protected $sSortWay = 'up';

    /** @var array Набор шаблонов для каталога */
    private $aTemplates = [
        'list' => 'SimpleList.twig',
        'gallery' => 'GalleryList.twig',
        'table' => 'TableList.twig',
        //'extgallery' => 'ExtGalleryList.twig',
    ];

    /** @var bool Факт использования фильтрации */
    protected $bFilterUsed = false;


    /**
     * Набор шаблонов для вывода в пользовательской части
     * @return array
     */
    public static function getTemplates() {

        $aList = ['list', 'gallery', 'table'];

        $aOut = [];
        foreach ($aList as $sName)
            $aOut[$sName] = \Yii::t( 'catalog', 'tpl_'.$sName);

        return $aOut;

    }

    public function init() {

        // категория
        $this->iCategory = $this->getModuleField( 'viewCategory' );

        // постраничный
        $this->iPageId = $this->getModule()->getInt( 'page', 1 );
        $this->iCount = $this->getModuleField( 'onPage', $this->iCount );

        // сортировка
        $this->sSortField = $this->getModuleField( 'listSortField' );
        if ( $sSortField = $this->getModule()->get('sort') )
            $this->sSortField = $sSortField;

        if ( $sSortWay = $this->getModule()->get('way') )
            $this->sSortWay = ( $sSortWay=='down' ? 'down' : 'up' );

        // выбор шаблона для вывода
        $this->getTpl();

        // получение набора товаров
        $this->getGoods();
    }


    public function build() {

        if ( empty($this->list) ) {

            if ( $this->bFilterUsed ) {
                $this->getModule()->setTemplate( 'NotFound.twig' );
                return;
            }

            $oPage = Site\Page::getRootModule();
            /*$aStaticContent = $oPage->getData('staticContent');
            $sText = (is_array($aStaticContent) and isset($aStaticContent['text'])) ? $aStaticContent['text'] : '';
            if ( \Html::hasContent($sText) ) {
                return;
            }*/

            $this->getModule()->setTemplate( 'Empty.twig' );
            return;
        }

        /** @todo продвиженцы пока не определились, как выводить на постраничных */
        //$oPage->setData('canonical_url', $this->getCanonical());

        // парсинг
        $this->getModule()->setData( 'section', $this->getModule()->getEnvParam('sectionId') );
        $this->getModule()->setData( 'aObjectList', $this->list );
        $this->getModule()->setData( 'form_section', $this->getModuleField( 'buyFormSection' ) );
        $this->getModule()->setData( 'useCart', Site\Type::isShop() );
        $this->getModule()->setData( 'hideBuy1lvlGoods', \SysVar::get('catalog.hideBuy1lvlGoods') );

        // панель сортировки для списка
        if ( $this->getModuleField( 'showSort' ) and $this->list )
            $this->showSortPanel();


        if (!empty($this->list)) {
            //Если есть товары - то
            parse_str($_SERVER["QUERY_STRING"], $params);
            
            if($params["page"]){ //Прячем текст на страницах пагинации
                Site\Page::getRootModule()->setData('staticContent2', array("text" => ""));
            }

            //Если есть товары и был использован фильтр - то ...
            if($this->bFilterUsed) { 


                //Соответсвие фильтров файлам с текстом
                $textRoot = array(
                    "/poleznye-sladosti/?strana%5B%5D=2" => "poleznye-sladosti-strana2.php",      
                );


                if($textRoot[$_SERVER["REQUEST_URI"]] && file_exists($_SERVER["DOCUMENT_ROOT"] . "/web/filter_text/" . $textRoot[$_SERVER["REQUEST_URI"]])){ //Если есть текст для фильтра ставим его
                    $newText = file_get_contents($_SERVER["DOCUMENT_ROOT"] . "/web/filter_text/" . $textRoot[$_SERVER["REQUEST_URI"]]);
                    Site\Page::getRootModule()->setData('staticContent2', array("text" => $newText));
                } else {    //Если нет, убираем основной текст раздела
                    Site\Page::getRootModule()->setData('staticContent2', array("text" => ""));
                }
               
            }
            //Костыль для seo-текста
            include( ROOTPATH . "seo_titles_inc.php");
             
            $url = $_SERVER["REQUEST_URI"];
            if(isset($seoMetaData[$url]["SEO_TEXT"])){
                Site\Page::getRootModule()->setData('staticContent2', array("text" => '<div class="bs-seo-text">' . $seoMetaData[$url]["SEO_TEXT"] . '</div>'));
            }
        }



        // шаблон
        $this->getModule()->setTemplate( $this->aTemplates[$this->sTpl] );

        // постраничник
        $this->setPathLine();
    }


    /**
     * Получение списка товарных позиций для текущей страницы
     * @return bool
     */
    protected function getGoods() {

        $oSelector = Catalog\GoodsSelector::getList4Section( $this->getSection() );

        if (!$oSelector) return false;

        if ( $this->getModuleField( 'showFilter' ) )
            $this->bFilterUsed = $oSelector->applyFilter();

        $this->list = $oSelector
            ->condition( 'active', 1 )
            ->sort( $this->sSortField, ($this->sSortWay == 'down' ? 'DESC' : 'ASC') )
            ->limit( $this->iCount, $this->iPageId, $this->iAllCount )
            ->parse()
        ;

        foreach ($this->list as $key=>$item){
            if (isset($item['fields']['strana']) && !empty($item['fields']['strana']['item'])){
                $country = array();
                foreach ($item['fields']['strana']['item'] as $key2=>$item2) {
                    $oGallery = Photo::getFromAlbum($item2['foto'], false, 1);
                    if (isset($oGallery[0]['images_data']['country']))
                        $country[$key2]['photo'] =  $oGallery[0]['images_data']['country']['file'];
                }
            if (isset($country)) $this->list[$key]['country']=$country;
            }
        }

        foreach ($this->list as $key=>$item){
            if ($item['fields']['measure']['value'] == \SysVar::get('dict_kgm')) {
                $price = $item['fields']['price']['value'];
                $price_new = ($price*1.0)/10;
                $this->list[$key]['fields']['price']['value_full'] = $price_new;
                $this->list[$key]['fields']['price']['value'] = $price_new;
                $this->list[$key]['fields']['price']['html'] = $price_new .' '.$this->list[$key]['fields']['price']['attrs']['measure'];
            }
        }

        return true;
    }


    /**
     * Вывод плашки для выбора сотрировки и типа отображения списка
     */
    protected function showSortPanel() {

        /** @var ft\model\Field[] $aFieldList */
        $aFieldList = Cache\Api::get( 'Catalog.Fields' );

        $aSortFieldArr = array();
        foreach ( $aFieldList as $oField ) {

            if ( ! $oField->getAttr( 'show_in_sortpanel' ) )
                continue;

            $aSortFieldArr[] = array(
                'name' => $oField->getName(),
                'title' => $oField->getTitle(),
                'sel' => ( $this->sSortField == $oField->getName() ) ? $this->sSortWay : false
            );
        }

        $this->getModule()->setData('filter', array('aFields' => $aSortFieldArr) );
        $this->getModule()->setData('viewState', $this->sTpl );
        $this->getModule()->setData('sortState', $this->sSortField );
        $this->getModule()->setData('defSortState', $this->getModuleField( 'listSortField' ) );
        $this->getModule()->setData('sortWay', $this->sSortWay );
    }


    /**
     * Установка шаблона для вывода
     */
    private function getTpl() {

        // берем из параметров раздела
        $this->sTpl = $this->getModuleField( 'listTemplate' );

        // проверяем перекрытие из GET
        if ( $sView = $this->getModule()->get('view') )
            $this->sTpl = $sView;

        // убеждаемся в наличии
        if ( !isSet( $this->aTemplates[$this->sTpl] ) )
            $this->sTpl = 'list';
    }


    /**
     * Вывод PathLine
     */
    protected function setPathLine() {

        // todo и все таки параметры должны браться из запросника после обработки
        $aURLParams = $_GET;
        unSet( $aURLParams['page'] );
        unSet( $aURLParams['url'] );

        foreach( $aURLParams as $sKey=>$mParam )
            if ( is_array( $mParam ) ) {

                foreach( $mParam as $sWKey=>$mValue )
                    $aURLParams[ $sKey . '[' .$sWKey . ']' ] = $mValue;

                unSet( $aURLParams[$sKey] );
            }

        if ( $this->sSortField )
            $aURLParams['sort'] = $this->sSortField;

        if ( $this->sSortWay )
            $aURLParams['way'] = $this->sSortWay;

        if ( $this->sTpl )
            $aURLParams['view'] = $this->sTpl;

        $this->getModule()->getPageLine(
            $this->iPageId,
            $this->iAllCount,
            $this->getModuleField( 'iCurrentSection' ),
            $aURLParams,
            array( 'onPage' => $this->iCount )
        );

    }

//    /**
//     * Канонический урл страницы
//     * @return string
//     */
//    private function getCanonical(){
//
//        return \Site::httpDomain().\Yii::$app->router->rewriteURL('[' . $this->getModule()->getEnvParam('sectionId') . ']');
//
//    }

}