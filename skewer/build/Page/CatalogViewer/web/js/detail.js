$(window).load(function () {
    $('.catalogbox__galbox ul').carouFredSel({
        auto: false,
        prev: '.catalogbox__back',
        next: '.catalogbox__next',
        width: '100%'
    });

    $(".js-tabs").tabs();
});