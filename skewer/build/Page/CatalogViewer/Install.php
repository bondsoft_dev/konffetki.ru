<?php

namespace skewer\build\Page\CatalogViewer;


use skewer\build\Component\Catalog;
use skewer\build\Component\I18N\Languages;
use skewer\build\Component\orm\Query;
use \skewer\build\libs\ft;
use \skewer\build\Component\Forms;
use \skewer\build\Component\Site;
use \skewer\build\Component\SEO;
use skewer\build\Component\Gallery;


/**
 * Class Install
 * @package skewer\build\Page\CatalogViewer
 */
class Install extends \skModuleInstall {


    public function init() {
        return true;
    }// func


    public function install() {

        /** установка таблиц в бд */
        $this->buildTables();

        /** сборка данных и базовой карточки */
        $iBaseCard = Catalog\Generator::genBaseCard();

        /** Расширенная карточка */
        $card = Catalog\Generator::createExtCard( $iBaseCard, 'dopolnitelnye_parametry', \Yii::t('data/catalog', 'cart_ext_title', [], \Yii::$app->language ) );
        Catalog\Generator::createField(
            $card->id,
            [
                'name' => 'proizvoditel',
                'title' => \Yii::t('data/catalog', 'field_producer_title', [], \Yii::$app->language ),
                'editor' => 'string',
            ]
        );
        $card->updCache();

        $aLanguages = Languages::getAllActiveNames();

        $iOrderFormSection = 0;
        foreach($aLanguages as $sName){

            /** Создаем форму */
            $iFormId = $this->addOrderForm($sName);

            /** Добавление раздела с формой */
            $iOrderFormSection = $this->addSection4OrderForm($iFormId, $sName);

            /** Каталог на главной */
            $this->addCatalogOnMain($iOrderFormSection, $sName);

        }

        /** Профиль галереи */
        $this->addGalleryProfiles();

        /** SEO template */
        $this->addSeoTemplate();

        /** Шаблон каталожного раздела */
        /** @todo запомнится последняя форма! */
        $this->addCatalogSectionTemplate($iOrderFormSection);

        /** соц кнопки */
        $this->addSocialButton();

        /** sysvars */
        $this->updateSysVars();

        /** Должен сбросить кэш */
        \CacheUpdater::setUpdFlag();

        /** Добавление доступов в каталог для админов */
        $this->setPolicy();

        return true;
    }// func


    public function uninstall() {

        $this->fail("Нельзя удалить каталог");

        return true;
    }// func


    /**
     * Изменение базы данных
     */
    private function buildTables() {

        /** Атрибуты */
        Catalog\model\FieldAttrTable::rebuildTable();

        /** Группы полей */
        Catalog\model\FieldGroupTable::rebuildTable();

        /** Поля */
        Catalog\model\FieldTable::rebuildTable();

        /** Валидаторы */
        Catalog\model\ValidatorTable::rebuildTable();

        /** Сущности */
        Query::dropTable( 'c_entity' );
        Catalog\model\EntityTable::rebuildTable();


        /** Связи товаров */
        Catalog\model\SectionTable::rebuildTable();
        Catalog\model\GoodsTable::rebuildTable();
        Catalog\model\SemanticTable::rebuildTable();

    }


    /**
     * Создаем форму
     * @param $sLanguage
     * @return int
     */
    private function addOrderForm($sLanguage){

        $aFormData = array(
            'form_name' => 'forma-zakaza-tovara',
            'form_title' => \Yii::t('data/order', 'order_form_title', [], $sLanguage),
            'form_handler_type' => 'toBase',
            'form_is_template' => 1,
            'form_answer' => 1,

        );

        $iFormId = 0;
        $oForm = Forms\Table::getNewRow($aFormData);
        if ($oForm){
            /** Нужно для создания таблицы, в которую будут сохраняться заказы */
            $oForm->save();
            $oForm->preSave();
            $iFormId = $oForm->getId();
        }

        if (!$iFormId){
            $this->fail('Не удалось создать форму!');
        }

        /**
         * Поля
         */
        $aFields = array(
            array(
                'param_section_id' => 0,
                'param_name' => 'person',
                'param_title' => \Yii::t('data/order', 'order_form_field_person', [], $sLanguage),
                'param_description' => '',
                'param_type' => 1,
                'param_required' => 0,
                'param_default' => '',
                'param_maxlength' => 255,
                'param_validation_type' => 'text',
                'param_depend' => '',
                'param_man_params' => '',
                'param_priority' => 3,
                'label_position' => 'top',
                'new_line' => 1,
                'width_factor' => 1
            ),
            array(
                'param_section_id' => 0,
                'param_name' => 'phone',
                'param_title' => \Yii::t('data/order', 'order_form_field_phone', [], $sLanguage),
                'param_description' => '',
                'param_type' => 1,
                'param_required' => 1,
                'param_default' => '',
                'param_maxlength' => 255,
                'param_validation_type' => 'text',
                'param_depend' => '',
                'param_man_params' => '',
                'param_priority' => 4,
                'label_position' => 'top',
                'new_line' => 0,
                'width_factor' => 1
            ),
            array(
                'param_section_id' => 0,
                'param_name' => 'email',
                'param_title' => \Yii::t('data/order', 'order_form_field_email', [], $sLanguage),
                'param_description' => '',
                'param_type' => 1,
                'param_required' => 1,
                'param_default' => '',
                'param_maxlength' => 255,
                'param_validation_type' => 'email',
                'param_depend' => '',
                'param_man_params' => '',
                'param_priority' => 5,
                'label_position' => 'top',
                'new_line' => 0,
                'width_factor' => 1
            ),
            array(
                'param_section_id' => 0,
                'param_name' => 'text',
                'param_title' => \Yii::t('data/order', 'order_form_field_text', [], $sLanguage),
                'param_description' => '',
                'param_type' => 2,
                'param_required' => 0,
                'param_default' => '',
                'param_maxlength' => 255,
                'param_validation_type' => 'text',
                'param_depend' => '',
                'param_man_params' => '',
                'param_priority' => 7,
                'label_position' => 'top',
                'new_line' => 1,
                'width_factor' => 1
            ),
            array(
                'param_section_id' => 0,
                'param_name' => 'naimenovanie-tovara',
                'param_title' => \Yii::t('data/order', 'order_form_field_goods_name', [], $sLanguage),
                'param_description' => '',
                'param_type' => 1,
                'param_required' => 0,
                'param_default' => '',
                'param_maxlength' => 255,
                'param_validation_type' => 'text',
                'param_depend' => '',
                'param_man_params' => '',
                'param_priority' => 1,
                'label_position' => 'top',
                'new_line' => 1,
                'width_factor' => 1
            )
        );

        foreach($aFields as $aField){
            $aField['form_id'] = $iFormId;
            $oFormField = Forms\FieldTable::getNewRow($aField);
            $oFormField->save();
        }

        if ($oForm){
            /** Нужно для создания таблицы, в которую будут сохраняться заказы */
            $oForm->preSave();
        }

        /** Письмо-автоответ */
        $this->executeSQLInsert("INSERT INTO `forms_add_data`
        (`form_id`, `answer_title`, `answer_body`, `agreed_title`, `agreed_text`)
        VALUES (:form_id, :answer_title, :answer_body, :agreed_title, :agreed_text)",
            array(
                'form_id' => $iFormId,
                'answer_title' => \Yii::t('data/order', 'order_form_add_answer_title', [], $sLanguage),
                'answer_body' => \Yii::t('data/order', 'order_form_add_answer_body', [], $sLanguage),
                'agreed_title' => \Yii::t('data/order', 'order_form_add_agreed_title', [], $sLanguage),
                'agreed_text' => \Yii::t('data/order', 'order_form_add_agreed_text', [], $sLanguage)
            ));

        /** Связи с полями карточки */

        /**
         * Создаем таблицу руками, по другому падает с ошибкой
         */
        $sQuery = "CREATE TABLE IF NOT EXISTS `forms_links` (
                        `link_id` INT( 11 ) NOT NULL AUTO_INCREMENT ,
                        `form_id` INT( 11 ) DEFAULT NULL ,
                        `form_field` VARCHAR( 255 ) DEFAULT NULL ,
                        `card_field` VARCHAR( 255 ) DEFAULT NULL ,
                        PRIMARY KEY (`link_id`)
                    ) ENGINE = MYISAM DEFAULT CHARSET = utf8;";

        $this->executeSQLQuery($sQuery);

        $this->executeSQLInsert("INSERT INTO `forms_links`
        (`link_id`, `form_id`, `form_field`, `card_field`)
        VALUES (null, :form_id, :form_field, :card_field )",
            array(
                'form_id' => $iFormId,
                'form_field' => 'naimenovanie-tovara',
                'card_field' => 'title'
            ));

        return $iFormId;
    }

    /**
     * Добавление раздела с формой
     * @param $iForm
     * @param $sLanguage
     * @throws \Exception
     * @throws \UpdateException
     * @return bool|int
     */
    private function addSection4OrderForm($iForm, $sLanguage){
        $iTplSection = $this->addSectionByTemplate(
            \Yii::$app->sections->getValue('tools', $sLanguage),
            \Yii::$app->sections->tplNew(),
            'zayavka-na-tovar',
            \Yii::t('data/app', 'section_orderForm', [], $sLanguage),
            1
        );

        if (!$iTplSection){
            throw new \Exception('Не удалось создать раздел для формы заказа!');
        }

        /** раздел должен быть неудаляемый! */
        $this->setParameter($iTplSection, '_break_delete', '.', '1', '', 'Системный (неудаляемый) раздел', 0);

        /**
         * добавить связь раздела с формой
         */
        Forms\Table::link2Section($iForm, $iTplSection);

        $this->setServiceSections('orderForm', \Yii::t('app', 'orderForm', [], $sLanguage), $iTplSection, $sLanguage);

        return $iTplSection;
    }

    /**
     * Добавление профиля галлереи для каталога
     * @return bool
     */
    private function addGalleryProfiles(){
        /**
         * Кажется, в каталоге должен быть профиль с номером 7 и где-то это было вбито в код
         */
        if (Gallery\Profile::getById(7)) return;

        Gallery\Profile::setProfile([
            'id' => 7,
            'title' => \Yii::t('data/gallery', 'profile_catalog_name', [], \Yii::$app->language ),
            'name' => 'catalog',
            'active' => 1
        ]);

        Gallery\Format::setFormat([
            'profile_id' => 7,
            'title' => \Yii::t('data/gallery', 'format_catalog_mini_name', [], \Yii::$app->language ),
            'name' => 'mini',
            'width' => 80,
            'height' => 80,
            'resize_on_larger_side' => 1,
            'scale_and_crop' => 0,
            'use_watermark' => 0,
            'watermark' => '',
            'watermark_align' => 84,
            'active' => 1,
            'position' => 1
        ]);

        Gallery\Format::setFormat([
                'profile_id' => 7,
                'title' => \Yii::t('data/gallery', 'format_catalog_small_name', [], \Yii::$app->language ),
                'name' => 'small',
                'width' => 180,
                'height' => 180,
                'resize_on_larger_side' => 1,
                'scale_and_crop' => 0,
                'use_watermark' => 0,
                'watermark' => '',
                'watermark_align' => 84,
                'active' => 1,
                'position' => 2
        ]);

        Gallery\Format::setFormat([
                'profile_id' => 7,
                'title' => \Yii::t('data/gallery', 'format_catalog_medium_name', [], \Yii::$app->language ),
                'name' => 'medium',
                'width' => 320,
                'height' => 320,
                'resize_on_larger_side' => 1,
                'scale_and_crop' => 0,
                'use_watermark' => 0,
                'watermark' => '',
                'watermark_align' => 84,
                'active' => 1,
                'position' => 3
        ]);

        Gallery\Format::setFormat([
                'profile_id' => 7,
                'title' => \Yii::t('data/gallery', 'format_catalog_big_name', [], \Yii::$app->language ),
                'name' => 'big',
                'width' => 800,
                'height' => 0,
                'resize_on_larger_side' => 1,
                'scale_and_crop' => 0,
                'use_watermark' => 0,
                'watermark' => '',
                'watermark_align' => 84,
                'active' => 1,
                'position' => 4
        ]);
    }

    /**
     * Добавим шаблон SEO для каталожных страниц
     */
    private function addSeoTemplate(){

        /**
         * @todo lang сео на языках?
         */

        if (!SEO\Template::find()->where('sid', SEO\Api::TPL_CATALOG)->getOne()){
            SEO\Template::getNewRow(
                array(
                    'sid' => SEO\Api::TPL_CATALOG,
                    'name' => 'Страница товара в каталоге',
                    'title' => '[Название товара]. [Название страницы]. [Цепочка разделов до главной] [Название сайта]',
                    'description' => '[Название сайта], [Цепочка разделов до главной], [Название страницы], [Название товара]',
                    'keywords' => '[название товара], [название страницы], [цепочка разделов до главной], [Название сайта]',
                    'info' => '[Название товара] - название товара
                [название товара] - название товара с маленькой буквы'
                )
            )
                ->save();
        }

        if (!SEO\Template::find()->where('sid', SEO\Api::TPL_CATALOG2LAYER)->getOne()){
            SEO\Template::getNewRow(
                array(
                    'sid' => SEO\Api::TPL_CATALOG2LAYER,
                    'name' => 'Страница аналогов',
                    'title' => '[Название товара]. [Название страницы]. [Цепочка разделов до главной] [Название сайта]',
                    'description' => '[Название сайта], [Цепочка разделов до главной], [Название страницы], [Название товара]',
                    'keywords' => '[название товара], [название страницы], [цепочка разделов до главной], [Название сайта]',
                    'info' => "[Название товара] - название товара \n [название товара] - название товара с маленькой буквы"
                )
            )
                ->save();
        }

    }

    /**
     * Добавление шаблона каталожного раздела
     * @param $iOrderFormSection
     * @throws \Exception
     * @return int
     */
    private function addCatalogSectionTemplate($iOrderFormSection){

        $iTplSection = $this->addSectionByTemplate(
            \Yii::$app->sections->templates(),
            \Yii::$app->sections->tplNew(),
            'katalog',
            \Yii::t('data/catalog', 'template_catalog_section', [], \Yii::$app->language ),
            1
        );

        if (!$iTplSection){
            throw new \Exception('Не удалось создать шаблон каталожного раздела');
        }

        /**
         * накидаем параметры
         */

        /** для дизайнерского */
        $this->setParameter($iTplSection, '.title', '.layout', 'Каталог', '', 'Название шаблона расположения', 0);
        $this->setParameter($iTplSection, '.order', '.layout', '10', '', 'вес при сортировке', 0);
        $this->setParameter($iTplSection, 'content', '.layout', 'pathLine,bannerContentTop,title,inc.staticContent,CategoryViewer,content,socialButtons,forms,staticContent2', '', 'Контент (центр)', 0);

        $this->setParameter($iTplSection, 'title', 'CatalogFilter', '', '', \Yii::t('catalogFilter', 'BlockTitle', [], \Yii::$app->language ), 1);
        $this->setParameter($iTplSection, 'layout', 'CatalogFilter', 'content,left,right,head', '', '', 0);
        $this->setParameter($iTplSection, '.title', 'CatalogFilter', 'Фильтр для каталога', '', '', 0);

        $this->setParameter($iTplSection, 'relatedTpl', 'content', 'gallery', '', '', 0);
        $this->setParameter($iTplSection, 'includedTpl', 'content', 'gallery', '', '', 0);
        $this->setParameter($iTplSection, 'showFilter', 'content', 0, '', \Yii::t('catalog', 'showFilter', [], \Yii::$app->language ), 0);
        $this->setParameter($iTplSection, 'buyFormSection', 'content', $iOrderFormSection, '', \Yii::t('catalog', 'template_catalog_order_form', [], \Yii::$app->language ), 0);
        $this->setParameter($iTplSection, 'listTemplate', 'content', 'list', '', \Yii::t('catalog', 'template_catalog_list_template', [], \Yii::$app->language ), 0);
        $this->setParameter($iTplSection, 'object', 'content', 'CatalogViewer', '', '', 0);
        $this->setParameter($iTplSection, 'objectAdm', 'content', 'Catalog', '', '', 0);
        $this->setParameter($iTplSection, 'onPage', 'content', 12, '', \Yii::t('catalog', 'template_catalog_on_page'), 0);
        $this->setParameter($iTplSection, 'showSort', 'content', 0, '', \Yii::t('catalog', 'showSort', [], \Yii::$app->language ), 0);
        $this->setParameter($iTplSection, 'viewCategory', 'content', '', '', \Yii::t('catalog', 'view_category', [], \Yii::$app->language ), 0);
    }

    /**
     * Добавление каталога на главной
     * @param $iFormSection
     * @param $sLanguage
     */
    private function addCatalogOnMain($iFormSection, $sLanguage){

        $iMainSection = \Yii::$app->sections->getValue('main', $sLanguage);

        $this->setParameter($iMainSection, '.title', 'catalog', 'Каталог', '', 'Название шаблона расположения', 0);
        $this->setParameter($iMainSection, 'layout', 'catalog', 'content', '', '', 0);
        $this->setParameter($iMainSection, 'object', 'catalog', 'CatalogViewer', '', '', 0);
        $this->setParameter($iMainSection, 'onMain', 'catalog', 'on_main', '', 'Вывод на главной', 0);
        $this->setParameter($iMainSection, 'onPage', 'catalog', '3', '', \Yii::t('catalog', 'param_on_page', [], \Yii::$app->language ), 0);
        $this->setParameter($iMainSection, 'titleOnMain', 'catalog', 'Каталог', '', \Yii::t('catalog', 'param_title_on_main', [], $sLanguage), 0);
        $this->setParameter($iMainSection, 'buyFormSection', 'catalog', $iFormSection, '', 'Раздел с формой заказа', 0);
        $this->setParameter($iMainSection, 'listTemplate', 'catalog', 'gallery', 'list '.\Yii::t('editor', 'type_catalog_list', [], $sLanguage)."\n"
            .'gallery '.\Yii::t('editor', 'type_catalog_gallery', [], $sLanguage), \Yii::t('editor', 'type_catalog_view', [], $sLanguage), '-9');

        $this->setParameter($iMainSection, '.title', 'catalog2', 'Каталог (хиты)', '', 'Название шаблона расположения', 0);
        $this->setParameter($iMainSection, 'layout', 'catalog2', 'content', '', '', 0);
        $this->setParameter($iMainSection, 'object', 'catalog2', 'CatalogViewer', '', '', 0);
        $this->setParameter($iMainSection, 'onMain', 'catalog2', 'hit', '', 'Вывод на главной', 0);
        $this->setParameter($iMainSection, 'onPage', 'catalog2', '3', '', \Yii::t('catalog', 'param_on_page', [], \Yii::$app->language ), 0);
        $this->setParameter($iMainSection, 'titleOnMain', 'catalog2', 'Hits', '', \Yii::t('catalog', 'param_title_on_main', [], $sLanguage), 0);
        $this->setParameter($iMainSection, 'buyFormSection', 'catalog2', $iFormSection, '', 'Раздел с формой заказа', 0);
        $this->setParameter($iMainSection, 'listTemplate', 'catalog2', 'gallery', 'list '.\Yii::t('editor', 'type_catalog_list', [], $sLanguage)."\n"
            .'gallery '.\Yii::t('editor', 'type_catalog_gallery', [], $sLanguage), \Yii::t('editor', 'type_catalog_view', [], $sLanguage), '-9');

        $this->setParameter($iMainSection, '.title', 'catalog3', 'Каталог (новинки)', '', 'Название шаблона расположения', 0);
        $this->setParameter($iMainSection, 'layout', 'catalog3', 'content', '', '', 0);
        $this->setParameter($iMainSection, 'object', 'catalog3', 'CatalogViewer', '', '', 0);
        $this->setParameter($iMainSection, 'onMain', 'catalog3', 'new', '', 'Вывод на главной', 0);
        $this->setParameter($iMainSection, 'onPage', 'catalog3', '3', '', \Yii::t('catalog', 'param_on_page', [], \Yii::$app->language ), 0);
        $this->setParameter($iMainSection, 'titleOnMain', 'catalog3', 'News', '', \Yii::t('catalog', 'param_title_on_main', [], $sLanguage), 0);
        $this->setParameter($iMainSection, 'buyFormSection', 'catalog3', $iFormSection, '', 'Раздел с формой заказа', 0);
        $this->setParameter($iMainSection, 'listTemplate', 'catalog3', 'gallery', 'list '.\Yii::t('editor', 'type_catalog_list', [], $sLanguage)."\n"
            .'gallery '.\Yii::t('editor', 'type_catalog_gallery', [], $sLanguage), \Yii::t('editor', 'type_catalog_view', [], $sLanguage), '-9');

    }

    /**
     * Добавляем значения нужных полей в sysvar
     */
    private function updateSysVars(){
        \SysVar::set( 'syte_type', Site\Type::catalog );
    }

    /**
     * Добавление параметра о соц. кнопке
     */
    private function addSocialButton(){
        $this->setParameter(\Yii::$app->sections->root(), 'soclinkGoods', 'socialButtons', '', '', 'SocialButtons.social_catalog', -5);
    }

    /**
     * Добавление доступа в панель каталога
     */
    private function setPolicy(){
        $aFilter = array(
            'where_condition' =>
                array(
                    'access_level' => array(
                        'sign' => '!=',
                        'value' => 0
                    ),
                    'area' => array(
                        'sign' => '=',
                        'value' => 'admin'
                    ),
                )
        );

        $aPolicy = \Policy::getPolicyList( $aFilter );

        if (isset($aPolicy['items']) && count($aPolicy['items'])){
            foreach ($aPolicy['items'] as $aItem){
                \Policy::setGroupActionParam( $aItem['id'], 'skewer\build\Catalog\LeftList\Module', 'useCatalog', 1, \Yii::t('adm', 'useCatalog'));
            }
        }
    }

}// class