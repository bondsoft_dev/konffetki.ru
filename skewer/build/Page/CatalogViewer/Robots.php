<?php

namespace skewer\build\Page\CatalogViewer;


class Robots implements \skRobotsInterface{

    public static function getRobotsDisallowPatterns(){
        return array(
            '/*view=',
            '/*sort=',
           // '/*?page=' //@todo нужно ли это убирать?
        );
    }

    public static function getRobotsAllowPatterns(){
        return false;
    }
} 