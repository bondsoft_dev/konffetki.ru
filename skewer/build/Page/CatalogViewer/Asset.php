<?php

namespace skewer\build\Page\CatalogViewer;

use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle {
    
    public $sourcePath = '@skewer/build/Page/CatalogViewer/web/';

    public $css = [
        'css/catalog.css',
        'css/filter.css',
        'css/tab.css'
    ];

    public $js = [
        'js/catalog.js'
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public $depends = [
        'skewer\build\Page\GalleryViewer\Asset',
        'skewer\assets\JqueryUiAsset'
    ];


}
