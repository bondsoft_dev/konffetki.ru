<?php

namespace skewer\build\Page\CatalogViewer;
use yii\web\NotFoundHttpException;


/**
 * Модуль вывода каталога в пользовательской части
 * @class CatalogViewer
 * @extends skModule
 * @project Skewer
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date $Date: $
 */
class Module extends \skModule {

    /** @var string Шаблон для вывода списка */
    public $listTemplate = '';

    /** @var string Категория для показа */
    public $viewCategory = '';

    /** @var string раздел формы заказа */
    public $buyFormSection = '';

    /** @var string Имя поля для сотрировки списка */
    public $listSortField = '';

    /** @var string Флаг вывода отзывов */
    public $showReviews = false;

    /** @var int Раздел вывода  */
    public $iCurrentSection;

    /** @var int Кол-во позиций на страницу */
    public $onPage = 3;

    /** @var bool Вывод на главной */
    public $onMain = false;

    /** @var bool Вывод коллекции на главной */
    public $onMainCollection = false;

    /** @var bool Вывод поисковой страницы */
    public $searchCard = false;

    /** @var bool Вывод страницы коллекции */
    public $collectionField = false;

    /** @var bool Показывать панель фильтра для списка */
    public $showFilter = false;

    /** @var bool Показывать плашку сортировки в списке */
    public $showSort = false;

    /** @var State\DetailPage|State\ListOnMain|State\ListPage Состояние вывода каталога */
    private $oState = null;

    /** @var string Заголовок страницы на главной */
    public $titleOnMain = '';

    /** @var string Шаблон для вывода списка связанных товаров в детальной */
    public $relatedTpl = '';

    /** @var string Шаблон для вывода списка товаров из комплекта в детальной*/
    public $includedTpl = '';



    /**
     * Инициализация модуля
     * @return bool
     */
    public function init() {

        $this->iCurrentSection = $this->getEnvParam('sectionId');

        $sGoodsAlias  = urldecode( $this->getStr( 'goods-alias', '' ) );

        $iGoodsId = $this->getInt( 'item', 0 );

        if ( $this->getModuleField( 'onMain' ) ) {

            $this->oState = new State\ListOnMain( $this );

        } elseif ( $this->getModuleField( 'onMainCollection' ) ) {

            $this->oState = new State\CollectionOnMain( $this );

        } elseif ( $this->getModuleField( 'searchCard' ) ) {

            $this->oState = new State\SearchPage( $this );

        } elseif ( $this->getModuleField( 'collectionField' ) ) {

            if ( $iGoodsId || $sGoodsAlias ) {

                $this->oState = new State\CollectionPage( $this );
                $this->oState->setCollectionId( $sGoodsAlias ? $sGoodsAlias : $iGoodsId );

            } else {
                $this->oState = new State\CollectionList( $this );
            }

        } elseif ( $iGoodsId || $sGoodsAlias ) {

            $this->oState = new State\DetailPage( $this );
            $this->oState->findGoods( $iGoodsId, $sGoodsAlias );

        } else {

            $this->oState = new State\ListPage( $this );
        }

        return true;
    }


    public function execute() {

        if ( $this->oState->showFilter() ) {

            // Откладываем запуск если не отработал модуль фильтра
            $oFilterModule = $this->getProcess( 'out.CatalogFilter', psAll );
            if ( ($oFilterModule instanceof \skProcess ) && !$oFilterModule->isComplete() )
                return psWait;

        } else {

            // удаляем процесс из вывода
            $oFilterModule = $this->getProcess( 'out.CatalogFilter', psAll );
            if ( $oFilterModule instanceof \skProcess )
                $oFilterModule->setStatus( psBreak );

        }


        try {

            $this->oState->show();

        } catch ( Exception $e ) {

            // не найдены позиции
            throw new NotFoundHttpException();

        } catch ( \Exception $e ) {

            // внутренняя ошибка при выполнении
            \skLogger::error( $e->getMessage() );

            throw new NotFoundHttpException();

        }

        return psComplete;
    }

}