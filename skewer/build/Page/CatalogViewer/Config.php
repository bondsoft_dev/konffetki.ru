<?php


/* main */
$aConfig['name']     = 'CatalogViewer';
$aConfig['title']    = 'Просмотр каталога';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Модуль вывода каталога';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::PAGE;
$aConfig['languageCategory']     = 'catalog';

$aConfig['dependency'] = array(
    array('CatalogFilter', \Layer::PAGE),

    array('Order', \Layer::ADM),
    array('Catalog', \Layer::ADM),

    array('CardEditor', \Layer::CATALOG),
    array('Dictionary', \Layer::CATALOG),
    array('Filters', \Layer::CATALOG),
    array('Goods', \Layer::CATALOG),
    array('LeftList', \Layer::CATALOG),
);

return $aConfig;
