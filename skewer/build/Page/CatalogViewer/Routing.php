<?php

namespace skewer\build\Page\CatalogViewer;

use skewer\build\Component\Router\RoutingInterface;


/**
 * Класс задание шаблонов роутинга для ЧПУ
 * Class Routing
 * @package skewer\build\Page\CatalogViewer
 */
class Routing implements RoutingInterface {

    /**
     * Возвращает паттерны разбора URL
     * @static
     * @return bool | array
     */
    public static function getRoutePatterns() {

        return array(
            '/goods-alias/',
            '/!response/',
            '/goods-alias/!response/',
        );
    }// func
} 