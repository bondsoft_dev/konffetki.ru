<?php

$aLanguage = array();

$aLanguage['ru']['tab_name'] = 'Вопрос-ответ';
$aLanguage['ru']['back'] = 'Назад';
$aLanguage['ru']['ans_success'] = 'Спасибо за Ваш вопрос. После прохождения модерации он появится на сайте';
$aLanguage['ru']['ans_error'] = 'Ошибка! Данные Не были отправлены.';
$aLanguage['ru']['ans_captcha_error'] = 'Ошибка! Числа с картинки введены неверно.';
$aLanguage['ru']['ans_validation'] = 'Ошибка валидации!';
$aLanguage['ru']['ans_not_found'] = 'Ошибка! Такая форма не существует.';
$aLanguage['ru']['ans_not_exists'] = 'Ошибка! Необходимо создать и настроить форму.';
$aLanguage['ru']['ans_data_error'] = 'Ошибка данных!';
$aLanguage['ru']['ans_upload'] = 'Ошибка загрузки файла!';
$aLanguage['ru']['ans_maxsize'] = 'Ошибка! Превышен максимально допустимый размер загружаемого файла.';
$aLanguage['ru']['ans_format'] = 'Ошибка! Недопустимый формат файла.';
$aLanguage['ru']['ans_handler'] = 'Ошибка! Обработчик не найден.';
$aLanguage['ru']['question'] = 'Вопрос';
$aLanguage['ru']['answer'] = 'Ответ';
$aLanguage['ru']['message_not_found'] = 'Сообщение не найдено';
$aLanguage['ru']['letter_content'] = 'На адрес сайта оставлен новый вопрос. Чтобы ответить на него, зайдите в систему управления сайтом.';
$aLanguage['ru']['letter_subject'] = 'Вопрос с сайта';
$aLanguage['ru']['label_sitename'] = 'название сайта';
$aLanguage['ru']['label_url'] = 'адрес сайта';
$aLanguage['ru']['answer_title_template'] = 'Вопрос с сайта';
$aLanguage['ru']['asnwer_body_tempalte'] = 'На адрес сайта оставлен новый вопрос. Чтобы ответить на него, зайдите в систему управления сайтом.';

$aLanguage['en']['tab_name'] = 'FAQ';
$aLanguage['en']['back'] = 'Back';
$aLanguage['en']['ans_success'] = 'Thank you for your question. After passing the moderation it will appear on the site';
$aLanguage['en']['ans_error'] = 'Error! The data have not been sent.';
$aLanguage['en']['ans_captcha_error'] = 'Error! You have entered incorrect code.';
$aLanguage['en']['ans_validation'] = 'Validation error!';
$aLanguage['en']['ans_not_found'] = 'Error! This form does not exist.';
$aLanguage['en']['ans_not_exists'] = 'Error! You must create and customize the form.';
$aLanguage['en']['ans_data_error'] = 'Data error!';
$aLanguage['en']['ans_upload'] = 'Upload error!';
$aLanguage['en']['ans_maxsize'] = 'Error! Exceeded the maximum file upload size.';
$aLanguage['en']['ans_format'] = 'Error! Invalid file format.';
$aLanguage['en']['ans_handler'] = 'Error! The handler is not found.';
$aLanguage['en']['question'] = 'Question';
$aLanguage['en']['answer'] = 'Answer';
$aLanguage['en']['message_not_found'] = 'Message not found';
$aLanguage['en']['letter_content'] = 'On the website address by a new question. To answer this question, go to the content management system.';
$aLanguage['en']['letter_subject'] = 'The issue with the site';
$aLanguage['en']['label_sitename'] = 'site name';
$aLanguage['en']['label_url'] = 'url';
$aLanguage['en']['answer_title_template'] = 'Question from site';
$aLanguage['en']['asnwer_body_tempalte'] = 'On the website address by a new question. To answer this question, go to the content management system.';

return $aLanguage;