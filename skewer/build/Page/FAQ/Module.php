<?php

namespace skewer\build\Page\FAQ;


use skewer\build\Adm\FAQ as AdmFAQ;
use skewer\build\Component\SEO\Api as SEOApi;
use skewer\build\Component\Site;
use yii\web\NotFoundHttpException;

/**
 * Class Module
 * @package skewer\build\Page\FAQ
 */
class Module extends \skModule{

    var $iSectionId;
    var $altTitle = '';

    public $onPage = 10;
    private $iCurrentSection;

    private $oFAQRow = null;

    /** @var int */
    public $revert = 0;

    public function init(){

        $this->iSectionId = $this->getEnvParam('sectionId');
        $this->iCurrentSection = $this->getEnvParam('sectionId');
        $this->setParser(parserTwig);
    }

    protected function dateToText($sDate) {

        $arr = explode(' ',$sDate);
        $res = array();
        $res['date'] = $arr[0]; $res['time'] = $arr[1];
        list($res['year'],$res['month'],$res['day']) = explode('-',$arr[0]);
        list($res['hour'],$res['min'],$res['sec']) = explode(':',$arr[1]);

        $sNewDate = $res['day'].' '.\Yii::t('app', 'month_'.$res['month']).' '.$res['year'];
        return $sNewDate;
    }

    public function execute(){

        $sAlias = $this->get('alias', '');
        $iId = $this->getInt('id', 0);

        if ($sAlias || $iId){

            if ($sAlias){
                $this->oFAQRow = Api::getFAQByAlias($sAlias);
            }else{
                $this->oFAQRow = Api::getFAQById($iId);
            }

            return $this->showDetail();
        }else{
            return $this->showList();
        }

    }

    /**
     * Вывод детальной
     * @return int
     * @throws NotFoundHttpException
     */
    private function showDetail(){

        if (!$this->oFAQRow)
            throw new NotFoundHttpException();

        if (!isset($this->oFAQRow['parent']) || $this->oFAQRow['parent'] != $this->iSectionId)
            throw new NotFoundHttpException();

        $this->setData('list', array('items' => array($this->oFAQRow)));

        $this->setTemplate('detal.twig');

        // Убрать статический контент
        if ( !Site\Page::rootModuleComplete() )
            return psWait;
        Site\Page::clearStaticContent();


        /** H1 */
        if (isset($this->oFAQRow['content']))
            Site\Page::setTitle($this->oFAQRow['content']);

        /** Pathline */
        Site\Page::setAddPathItem( strip_tags($this->oFAQRow['content']) );

        /** SEO */
        $this->setSEOLabels($this->oFAQRow);

        return psComplete;
    }

    /**
     * Вывод списка
     * @return int
     */
    private function showList(){

        $iPage = $this->getInt('page', 1);

        $iCount = 0;
        $aItems = Api::getItems($this->iSectionId, $iPage, $this->onPage, $iCount);

        if ($iPage != 1 && (!$aItems || !$iCount))
            throw new NotFoundHttpException();

        // расставить отформатированииые даты в записях
        foreach($aItems as $iKey=>$aItem){
            $aItem['date_time'] = date("d.m.Y", strtotime($aItem['date_time']));
            $aItem['content'] = nl2br(strip_tags($aItem['content']));
            $aItem['full_content'] = $aItem['content'];

            $aItem['full_answer'] = $aItem['answer'];
            $aItem['answer'] = nl2br(strip_tags($aItem['answer']));

            // обрезаем слишком длинные тексты до 40 слов
            if (count(explode(' ',$aItem['content']))>40){
                $aItem['content'] = implode(' ',array_slice(explode(' ',$aItem['content']),0,39))."...";
            }

            if (count(explode(' ',$aItem['answer']))>40){
                $aItem['answer'] = implode(' ',array_slice(explode(' ',$aItem['answer']),0,39))."...";
            }

            $aItems[$iKey] = $aItem;
        }

        $this->getPageLine($iPage, $iCount, $this->iCurrentSection, array(), array('onPage'=>$this->onPage));

        $this->setData('list', array('items' => $aItems));
        $this->setData('section', $this->iSectionId);

        $this->showForm();

        $this->setTemplate('view.twig');
        return psComplete;
    }

    /**
     * Вывод формы
     */
    public function showForm() {

        $oCommentForm = new Form();

        $this->setData('revert',$this->revert);

        if ($oCommentForm->load($this->getPost()) && $oCommentForm->isValid() && $oCommentForm->sendComment()) {

            $this->setData('msg', \Yii::t('faq', 'ans_success')); // todo нужное сообщение
            $this->setData('back_link', 1);

        } else {

            $aParams = array(
                'page' => $this->iSectionId,
            );

            $oCommentForm->parent = $this->iSectionId;

            $this->setData('form', $oCommentForm->getForm('form.twig', __DIR__, $aParams));
        }

    }

    /**
     * Добавление SEO парамертов
     * @param $aObject
     * @return int
     */
    private function setSEOLabels( $aObject ) {

        $SEOData = $this->getEnvParam( 'SEOTemplates', array() );
        $SEOData[ SEOApi::TPL_FAQ ] = array( 'title' => '', 'description' => '', 'keywords' => '' );
        if ( $oDataRow = SEOApi::get( 'faq', $aObject['id'] ) )
            $oDataRow->setParams( $SEOData, SEOApi::TPL_FAQ );

        $this->setEnvParam( 'SEOTemplates', $SEOData );
        $SEOVars = $this->getEnvParam( 'SEOVars', array() );
        $SEOVars['label_faq_title_upper'] = strip_tags($aObject['content']);
        $SEOVars['label_faq_title_lower'] = mb_strtolower( strip_tags($aObject['content']) );
        $this->setEnvParam( 'SEOVars', $SEOVars );

        Site\Page::reloadSEO();

        return true;
    }
}