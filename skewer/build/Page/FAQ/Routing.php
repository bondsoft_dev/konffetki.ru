<?php

namespace skewer\build\Page\FAQ;

use skewer\build\Component\Router\RoutingInterface;

class Routing implements RoutingInterface {
    /**
     * Возвращает паттерны разбора URL
     * @static
     * @return bool | array
     */
    public static function getRoutePatterns() {

        return array(
            '/alias/',
            '/id(int)/',
            '/*page/page(int)/',
            '!response'
        );
    }// func
}
