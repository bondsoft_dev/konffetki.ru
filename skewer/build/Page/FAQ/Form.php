<?php

namespace skewer\build\Page\FAQ;


use skewer\build\Component\I18N\ModulesParams;
use skewer\build\Component\orm;
use skewer\build\Adm\FAQ as AdmFAQ;

class Form extends orm\FormRecord {

    public $name = '';
    public $email = '';
    public $city = '';
    public $content = '';

    public $captcha = '';

    public $cmd = 'sendComment';
    public $parent = 0;

    public function rules() {
        return array(
            array( array('name','email','content','captcha'), 'required', 'msg'=>\Yii::t('forms', 'err_empty_field' ) ),
            array( array('email',), 'email', 'msg'=>\Yii::t('forms', 'err_incorrect_email' ) ),
            array( array('captcha'), 'captcha' ),
        );
    }

    public function getLabels() {
        return array(
            'name' => \Yii::t('faq', 'name' ),
            'email' => \Yii::t('faq', 'email' ),
            'city' => \Yii::t('faq', 'city' ),
            'content' => \Yii::t('faq', 'content' ),
        );
    }

    public function getEditors() {
        return array(
            'password' => 'password',
            'rating' => 'hidden',
            'cmd' => 'hidden',
            'captcha' => 'captcha',
            'content' => 'text',
            'parent' => 'hidden',
        );
    }

    public function sendComment() {

        $aData = $this->getData();
        foreach( $aData as &$mValue )
            $mValue = strip_tags( $mValue );


        $aFields = $this->getData();

        $aFields['date_time'] = date('Y-m-d H:i:s');
        $aFields['status'] = AdmFAQ\Api::statusNew;

        $oFAQRow = AdmFAQ\ar\FAQ::getNewRow($aFields);
        if (!$oFAQRow->save())
            return false;

        AdmFAQ\Api::sendMailToAdmin( $oFAQRow->email );
        AdmFAQ\Api::sendMailToClient( $oFAQRow->email );

        return true;
    }

}