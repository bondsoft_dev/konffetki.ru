<?php

namespace skewer\build\Page\SocialButtons;

use skewer\build\Component\Gallery;
use skewer\build\Component\Site;

class Module extends \skModule {

    public $soclinkContent = 0;
    public $soclinkNews = 0;
    public $soclinkGoods = 0;
    public $soclinkGallery = 0;

    /**
     * Первичная инициализация
     */
    public function init(){

    }

    /**
     * Выполнение модуля
     * @return int
     */
    public function execute() {

        $bShow = false;

        $contentModule = Site\Page::getMainModule();
        if ( $contentModule ) {

            if( !$contentModule->isComplete() )
                return psWait;

            switch($contentModule->getModuleClass()){
                case 'skewer\build\Page\News\Module':
                    $newsAlias = -1;
                    if($contentModule->oRouter->getStr('news_alias', $newsAlias) ||
                        $contentModule->oRouter->getStr('news_id', $newsAlias)){

                        if($this->soclinkNews) $bShow = true;

                    }

                break;

                case 'skewer\build\Page\Gallery\Module':

                    $bShow = $this->getEnvParam('gallery_photos') && $this->soclinkGallery;

                break;

                case 'skewer\build\Page\CatalogViewer\Module':
                    $catalogAlias = -1;

                    if($contentModule->oRouter->getStr('goods-alias', $catalogAlias) ||
                        $contentModule->oRouter->getStr('goods_id', $catalogAlias)){

                        if($this->soclinkGoods) $bShow = true;
                    }
                    break;

            }


        }elseif($this->soclinkContent){

            $bShow = true;

        }



        $this->setTemplate('block.twig');

        $this->setData('show', $bShow);

        return psComplete;
    }// func


}
