<?php

/* main */
$aConfig['name']     = 'SocialButtons';
$aConfig['title']     = 'Блок социальных кнопок';
$aConfig['version']  = '1.0';
$aConfig['description']  = '';
$aConfig['revision'] = '0002';
$aConfig['layer']     = \Layer::PAGE;
$aConfig['languageCategory']     = 'page';

return $aConfig;
