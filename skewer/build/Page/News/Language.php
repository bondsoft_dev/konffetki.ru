<?php

$aLanguage = array();

$aLanguage['ru']['News.Page.tab_name'] = 'Новости';
$aLanguage['ru']['back'] = 'Назад';
$aLanguage['ru']['news_title'] = 'Новости';
$aLanguage['ru']['all'] = 'Все новости';
$aLanguage['ru']['modified'] = 'Дата последнего изменения';
$aLanguage['ru']['param_on_page'] = 'Новостей в разделе';
$aLanguage['ru']['section_all'] = 'Id раздела всех новостей';
$aLanguage['ru']['all_section_link'] = 'Все новости';
$aLanguage['ru']['no_news'] = 'Новостей нет';

$aLanguage['en']['News.Page.tab_name'] = 'News';
$aLanguage['en']['back'] = 'Back';
$aLanguage['en']['news_title'] = 'News';
$aLanguage['en']['all'] = 'All news';
$aLanguage['en']['modified'] = 'Last modified date';
$aLanguage['en']['param_on_page'] = 'News on section';
$aLanguage['en']['section_all'] = 'Id section with all the news';
$aLanguage['en']['all_section_link'] = 'All the news';
$aLanguage['en']['no_news'] = 'No news';

return $aLanguage;