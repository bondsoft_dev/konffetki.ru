<?php

namespace skewer\build\Page\News;

use skewer\build\Component\Router\RoutingInterface;


/**
 * @class: NewsRouting
 *
 * @Author: ArmiT, $Author$
 * @version: $Revision$
 * @date: $Date$

 */
class Routing implements RoutingInterface {
    /**
     * Возвращает паттерны разбора URL
     * @static
     * @return bool | array
     */
    public static function getRoutePatterns() {

        return array(
            '/news_alias/',
            '/id(int)/',
            '/*page/page(int)/*date/date/',
            '/*page/page(int)/*date/date/!response/',
            '/*date/date/',
            '/*page/page(int)/',
            '/*page/page(int)/!response/',
            '/news_alias/!response/',
            '/id(int)/!response/',
            '/!response/'
        );
    }// func
}
