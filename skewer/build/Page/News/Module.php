<?php

namespace skewer\build\Page\News;

use skewer\build\Component\Section\Page;
use skewer\models\News;
use skewer\build\Component\SEO;
use skewer\build\Adm\News as NewsAdm;
use Yii;
use skewer\build\Component\Site;
use yii\web\NotFoundHttpException;

/**
 * Публичный модуль вывода новостей
 * Class Module
 * @package skewer\build\Page\News
 */
class Module extends \skModule {

    public $parentSections;
    public $onPage = 10;
    public $listTemplate = 'list.php';
    public $detailTemplate = 'detail_page.php';
    public $titleOnMain = 'Новости';
    public $showArchive;
    public $showFuture;
    public $showOnMain;  // отменяет фильтр по разделам
    public $allNews;
    public $sortNews;
    public $showList;
    public $onMainShowType = 'list';
    // public $usePageLine = 1;

    private static $showDetailLink = null;

    private $iCurrentSection;

    public $aParameterList = array(
        'order'    => 'DESC',
        'future'   => '',
        'byDate'   => '',
        'on_page'  => '',
        'on_main'  => '',
        'section'  => '',
        'archive'  => '',
        'all_news' => '',
        'show_list'=> '',
    );

    public $section_all = 0;

    protected function onCreate() {
        if ( $this->showList )
            $this->setUseRouting( false );
    }

    public function init() {
        $this->setParser(parserPHP);

        $this->iCurrentSection = $this->getEnvParam('sectionId');
        $this->aParameterList['on_page'] = $this->onPage;

        if ( $this->allNews ) $this->aParameterList['all_news'] = 1;
        if ( $this->showOnMain ) $this->aParameterList['all_news'] = 1;

        if ( $this->parentSections ) $this->aParameterList['section'] = $this->parentSections;
        else $this->aParameterList['section'] = $this->iCurrentSection;

        if ( $this->iCurrentSection == \Yii::$app->sections->main() ) $this->aParameterList['on_main'] = 1;
        if ( $this->showArchive ) $this->aParameterList['archive'] = 1;
        if ( $this->showFuture ) $this->aParameterList['future'] = 1;
        if ( $this->sortNews ) $this->aParameterList['order'] = $this->sortNews;

        if ( $this->showList ) $this->aParameterList['show_list'] = 1;

        return true;

    }// func


    public function execute() {
        $iNewsId     = $this->getInt( 'news_id', 0 );
        $sNewsAlias  = $this->getStr( 'news_alias', '' );
        $sDateFilter = $this->getStr( 'date' );
        $this->aParameterList['page'] = $this->getInt( 'page', 1 );

        if ( !empty($sDateFilter) ) {
            $sDateFilter = date('Y-m-d', strtotime($sDateFilter));
            $this->aParameterList['byDate'] = $sDateFilter;
        }
        /*
         * Если в запросе передается ID новости или её алиас, значит необходимо вывести конкретную новость;
         * если нет - выводим список новостей, разбитый по страницам.
         */
        if ( ($iNewsId || $sNewsAlias) && !$this->showList ) {
            /** @var News $news */
            if ( $sNewsAlias )
                $news = News::getPublicNewsByAlias($sNewsAlias);
            else
                $news = News::getPublicNewsById($iNewsId);

            if ( !$news )
                throw new NotFoundHttpException();

            // Убрать статический контент
            if ( !Site\Page::rootModuleComplete() )
                return psWait;
            Site\Page::clearStaticContent();


            //Метатеги для списка новостей
            $this->setSEO( $news );

            // меняем заголовок
            Site\Page::setTitle( $news->title );

            // добавляем элемент в pathline
            Site\Page::setAddPathItem( $news->title );

            $hideDate = Page::getVal( 'content', 'hideDate' );

            if ( $hideDate )
                $this->setData( 'hideDate', true );

            $this->setData( 'news', $news );
            $this->setTemplate($this->detailTemplate);

        } else {
            $iAllSection = 0;
            if ($this->showOnMain){
                $iAllSection = $this->section_all;
            }

            if ($iAllSection){
                $this->aParameterList['all_news'] = 0;
                $this->aParameterList['section'] = $iAllSection;
            }

            // Получаем список новостей
            $dataProvider = News::getPublicList($this->aParameterList);

            $this->setData('dataProvider',$dataProvider);
            $this->setData( 'titleOnMain', $this->titleOnMain );

            if ($iAllSection){
                $this->setData('section_all', $iAllSection);
            }
            // todo исправить в базе
            $this->setTemplate($this->listTemplate);
            $this->setData('showDetailLink', self::hasShowDetailLink());
            $this->setData('onMainShowType', $this->onMainShowType);
            $this->setData('zone', $this->zone);

            // если ничего не нашлось, то 404
            /*if ($this->oContext->getParams()['layout']=='content' && $dataProvider->count==0){
                $this->error404();
            }*/

        }

        // Откладываем запуск (перезапускаем) SEOMetatags после себя
        Site\Page::reloadSEO();

        return psComplete;
    }// func


    public function shutdown() {}


    /**
     * Метатеги для списка новостей
     * @param News $oNewsRow
     */
    public function setSEO( News $oNewsRow ) {

        $SEOData = $this->getEnvParam( 'SEOTemplates', array() );
        $SEOData[ SEO\Api::TPL_NEWS ] = array( 'title' => '', 'description' => '', 'keywords' => '' );
        if ( $oDataRow = SEO\Api::get( 'news', $oNewsRow->id ) )
            $oDataRow->setParams( $SEOData, SEO\Api::TPL_NEWS );
        $this->setEnvParam( 'SEOTemplates', $SEOData );
        $SEOVars = $this->getEnvParam( 'SEOVars', array() );
        $SEOVars['label_news_title_upper'] = $oNewsRow->title;
        $SEOVars['label_news_title_lower'] = mb_strtolower( $oNewsRow->title );
        $this->setEnvParam( 'SEOVars', $SEOVars );

        Site\Page::reloadSEO();

    }


    /**
     * Флаг вывода ссылки "Подробнее"
     * @return bool
     */
    public static function hasShowDetailLink()
    {
        if (is_null(self::$showDetailLink))
            self::$showDetailLink = (bool)\SysVar::get('News.showDetailLink');

        return self::$showDetailLink;
    }

} 