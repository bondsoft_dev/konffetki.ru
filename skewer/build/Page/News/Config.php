<?php

/* main */
$aConfig['name']     = 'News';
$aConfig['version']  = '2.0';
$aConfig['title']    = 'Новости';
$aConfig['description']  = 'Модуль новостной системы';
$aConfig['revision'] = '0002';
$aConfig['layer']     = \Layer::PAGE;
$aConfig['useNamespace'] = true;

$aConfig['dependency'][] = array("PathLine", \Layer::PAGE);
$aConfig['dependency'][] = array("Title", \Layer::PAGE);
$aConfig['dependency'][] = array("SEOMetatags", \Layer::PAGE);

/* Функциональные политики */
$aConfig['policy'][] = array(
    'name'    => 'allowNewsListRead',
    'title'   => 'Разрешить чтение списка новостей',
    'default' => 1
);

$aConfig['policy'][] = array(
    'name'    => 'allowNewsDetailRead',
    'title'   => 'Разрешить чтение детальной новости',
    'default' => 1
);

return $aConfig;
