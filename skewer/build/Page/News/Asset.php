<?php

namespace skewer\build\Page\News;

use Yii;
use yii\helpers\FileHelper;
use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle {

    public $sourcePath = '@skewer/build/Page/News/web/';

    public $css = [
        'css/main.css'
    ];

}
