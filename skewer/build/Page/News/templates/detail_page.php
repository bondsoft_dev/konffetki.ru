<?php
/* @var $this yii\web\View */
/* @var $news \skewer\models\News */
?>
<div class="b-news" sktag="modules.news">
    <? if (isset($news)): ?>
        <p class="news__date" sktag="modules.news.date"><?= Yii::$app->getFormatter()->asDate($news->publication_date,'php:d.m.Y') ?></p>
        <div class="b-editor" sktag="editor">
            <?= $news->full_text ?>
        </div>
        <p class="news__linkback"><a rel="nofollow" href="#" onclick="history.go(-1);return false;">
                <?= \Yii::t('page', 'back') ?>
            </a>
        </p>
    <? endif ?>
</div>
