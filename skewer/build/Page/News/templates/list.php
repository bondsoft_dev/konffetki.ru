<?php
/**
 * @var skewer\models\News $item
 * @var yii\data\ActiveDataProvider $dataProvider
 * @var string $_objectId
 */
?>
<?php if ($dataProvider->count>0): ?>
<div class="b-news b-news_list"<? if (Design::modeIsActive()): ?> sktag="modules.news" sklabel="<?= $_objectId ?>"<? endif ?>>
    <? foreach($dataProvider->getModels() as $item): ?>
        <?php
        /* @var \skewer\models\News $item */
        $hrefParam = ($item->news_alias)?"news_alias={$item->news_alias}":"news_id={$item->id}";
        $href = ($item->hyperlink)?$item->hyperlink:("[{$item->parent_section}][News?".$hrefParam."]");
        ?>
        <div class="news__item">
            <div class="news__title">
                <? if ($item->full_text || $item->hyperlink):?>
                    <a sktag="modules.news.title" href="<?= $href ?>"><?= $item->title ?></a>
                <? else: ?>
                    <span sktag="modules.news.normal"><?= $item->title ?></span>
                <? endif ?>
            </div>
            <div class="news__date" sktag="modules.news.date"><?= Yii::$app->getFormatter()->asDate($item->publication_date,'php:d.m.Y') ?></div>
            <div class="b-editor" sktag="editor">
                <?= $item->announce ?>
            </div>
            <div class="g-clear"></div>
            <? if ($showDetailLink && ($item->full_text || $item->hyperlink)): ?>
                <p class="news__linkback">
                    <a href="<?= $href ?>"><?= \Yii::t('page', 'readmore') ?></a>
                </p>
            <? endif ?>
        </div>
    <? endforeach ?>
</div>
<? // @todo надо сверстать пагинатор на ul - li ?>
<?= ($dataProvider->getModels())?yii\widgets\LinkPager::widget(['pagination'=>$dataProvider->getPagination(),'firstPageLabel'=>\Yii::t('page', 'page_first'),'lastPageLabel'=>\Yii::t('page', 'page_last'), 'options'=>['class'=>'b-pageline']]):""  ?>
<?php else: ?>
    <div><?= \Yii::t('news', 'no_news') ?></div>
<?php endif ?>