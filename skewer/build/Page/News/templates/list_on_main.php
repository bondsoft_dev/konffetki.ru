<?php
/**
 * @var $this yii\web\View
 * @var $item \skewer\models\News
 * @var $dataProvider yii\data\ActiveDataProvider
 * @var string $titleOnMain
 * @var bool $showDetailLink
 * @var string $onMainShowType
 * @var string $zone
 */

if ( $onMainShowType == 'column' and $zone == 'content' )
    $sAddClass = ' b-news-main';
else
    $sAddClass = '';

?>
<? if (($titleOnMain || Design::modeIsActive()) && count($dataProvider->getModels())):?>
    <h2 sktag="editor.h2" skeditor="news/titleOnMain"><?= $titleOnMain ?></h2>
<? endif ?>
<div class="b-news b-news_list<?= $sAddClass ?>" sktag="modules.news">
    <? foreach($dataProvider->getModels() as $item): ?>
    <?
        $hrefParam = ($item->news_alias)?"news_alias={$item->news_alias}":"news_id={$item->id}";
        $href = ($item->hyperlink)?$item->hyperlink:("[{$item->parent_section}][News?".$hrefParam."]");
    ?>
    <div class="news__item">
        <div class="news__title">
            <? if ($item->full_text || $item->hyperlink):?>
                <a class="news-title" sktag="modules.news.title" href="<?= $href ?>"><?= $item->title ?></a>
            <? else: ?>
                <span class="news-title" sktag="modules.news.normal"><?= $item->title ?></span>
            <? endif ?>
        </div>
        <p class="news__date" sktag="modules.news.date"><?= Yii::$app->getFormatter()->asDate($item->publication_date,'php:d.m.Y') ?></p>
        <div class="b-editor" sktag="editor">
            <?= $item->announce ?>
        </div>
        <div class="g-clear"></div>
        <? if ($showDetailLink && ($item->full_text || $item->hyperlink)): ?>
            <p class="news__linkback">
                <a href="<?= $href ?>"><?= \Yii::t('page', 'readmore') ?></a>
            </p>
        <? endif ?>
    </div>
    <? endforeach ?>
</div>
<? if ((isset($section_all)) and count($dataProvider->getModels())): ?>
    <a href="[<?=$section_all ?>]"><?= \Yii::t('News', 'all_section_link') ?></a>
<? endif ?>
