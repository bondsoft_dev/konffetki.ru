<?php

namespace skewer\build\Page\Articles\Model;


use skewer\build\Component\orm;
use skewer\build\libs\ft;
use \yii\base\ModelEvent;
use skewer\build\Component;
use skewer\build\Component\Section\Tree;

class Articles extends orm\TablePrototype {

    protected static $sTableName = 'articles';

    protected static function initModel() {

        ft\Entity::get( 'articles' )
            ->clear()
            ->setTablePrefix('')
            ->setNamespace( __NAMESPACE__ )
            ->addField( 'articles_alias', 'varchar(255)', 'articles.field_alias' )
            ->addField( 'parent_section', 'varchar(255)', 'articles.field_parent' )
            ->addField( 'author', 'varchar(255)', 'articles.field_author' )
            ->addField( 'publication_date', 'date', 'articles.field_date' )
            ->setDefaultVal( 'now' )
            ->addField( 'title', 'varchar(255)', 'articles.field_title' )
            ->addField( 'announce', 'text', 'articles.field_preview' )
            ->addField( 'full_text', 'text', 'articles.field_fulltext' )
            ->addField( 'active', 'int(1)', 'articles.field_active' )
            ->addField( 'archive', 'int(1)', 'articles.field_archive' )
            ->addField( 'on_main', 'int(1)', 'articles.field_on_main' )
            ->addField( 'hyperlink', 'varchar(255)', 'articles.field_hyperlink' )
            ->addModificator('link')
            ->addField( 'last_modified_date', 'date', 'articles.field_modifydate' )
            ->save()
            //->build()
        ;
    }


    public static function getNewRow( $aData = array() ) {

        $oRow = new ArticlesRow();

        $oRow->title = \Yii::t( 'articles', 'new_article' );
        $oRow->publication_date = date( "Y-m-d H:i:s", time() );
        $oRow->active = 1;

        if ( $aData )
            $oRow->setData($aData);

        return $oRow;
    }


    /**
     * Метод по выборке списка статей из базы
     * @static
     * @param int $iPage Страница для показа
     * @param array $aParams Параметры для фильтрации
     * @param int $iCnt Параметры для фильтрации
     * @return ArticlesRow[]
     */
    public static function getPublicList( $iPage, $aParams, &$iCnt=0 ) {

        $oQuery = self::find()
            ->where( 'active', 1 )
            ->order( 'publication_date', $aParams['order'] )
            ->limit( (int)$aParams['on_page'], ($iPage-1) * $aParams['on_page'] );
        ;

        /* Если есть фильтр по дате */
        if( isSet($aParams['byDate']) && !empty($aParams['byDate']) )
            $oQuery->where( 'publication_date BETWEEN ?', array( $aParams['byDate'].' 00:00:00', $aParams['byDate'].' 23:59:59') );

        if ( !$aParams['all_articles'] && $aParams['section'] ) // todo IN ?
            $oQuery->where( 'parent_section', $aParams['section'] );
        else{
            $aSections = Tree::getAllSubsection(\Yii::$app->sections->languageRoot());
            $oQuery->where('parent_section',$aSections);
        }

        if ( $aParams['future'] )
            $oQuery->where( 'publication_date > ?', date('Y-m-d H:i:s', time()) );

        $aItems = $oQuery->setCounterRef( $iCnt )->getAll();

        //$aItems = Mapper::getItems($aFilter);

        /** @var ArticlesRow $oItem */
        foreach( $aItems as &$oItem ) {

            $oItem->announce = str_replace(
                'data-fancybox-group="button"',
                'data-fancybox-group="articles'. $oItem->id .'"',
                $oItem->announce
            );

            $oItem->publication_date = date( "d.m.Y", strtotime( $oItem->publication_date ) );
        }

        return $aItems;
    }



    public static function getPublicById( $iArticlesId ) {
        return self::find( $iArticlesId );
    }


    public static function getPublicByAlias( $sAlias ) {
        return self::find()->where( 'articles_alias', $sAlias )->getOne();
    }


    /**
     * Удаление статей принадлежащих разделу
     * @param ModelEvent $event
     * @throws \Exception
     */
    public static function removeSection( ModelEvent $event ) {

        $sQuery = sprintf(
            "DELETE FROM `%s` WHERE `parent_section`=:section;",
            static::$sTableName
        );

        orm\Query::SQL(
            $sQuery,
            array( 'section' => $event->sender->id )
        )->affectedRows();

    }

    /**
     * Класс для сборки списка автивных поисковых движков
     * @param Component\Search\GetEngineEvent $event
     */
    public static function getSearchEngine( Component\Search\GetEngineEvent $event ) {
        $event->addSearchEngine( \skewer\build\Adm\Articles\Search::className() );
    }

}
