<?php

namespace skewer\build\Page\Articles\Model;

use skewer\build\Adm\Articles\Search;
use skewer\build\Component\orm;


class ArticlesRow extends orm\ActiveRecord {

    public $id = 0;
    public $articles_alias = '';
    public $parent_section = 0;
    public $author = '';
    public $publication_date = '';
    public $title = 'articles.new_article';
    public $announce = '';
    public $full_text = '';
    public $active = 1;
    public $archive = 0;
    public $on_main = 0;
    public $hyperlink = '';
    public $last_modified_date = '';

    function __construct() {
        $this->setTableName( 'articles' );
        $this->setPrimaryKey( 'id' );
    }


    /**
     * Форматирование данных перед сохранением
     */
    public function preSave() {

        // last modification date
        $this->last_modified_date = date( "Y-m-d H:i:s", time() );

        // generate uniq alias
        $this->checkAlias();

        // format wyswyg fields
        if ( $this->full_text && $this->parent_section )
            $this->full_text = \ImageResize::wrapTags( $this->full_text, $this->parent_section );

        if ( $this->announce && $this->parent_section  )
            $this->announce = \ImageResize::wrapTags( $this->announce, $this->parent_section );

        if( $this->hyperlink && (strpos($this->hyperlink,'http') === false) and ($this->hyperlink[0] !== '/') and ($this->hyperlink[0] !== '[') )
            $this->hyperlink = 'http://' . $this->hyperlink;

        if ( !$this->publication_date || ($this->publication_date == 'null') )
            $this->publication_date = date("Y-m-d H:i:s", time());

    }


    /**
     * Генерация и проверка уникального псевдонима
     * @param int $iNum Счетчик итератора
     * @return bool
     * @throws \Exception
     */
    public function checkAlias( $iNum = 0 ) {

        // generate
        if ( !$this->articles_alias )
            $this->articles_alias = \skTranslit::change( $this->title );
        else
            $this->articles_alias = \skTranslit::change( $this->articles_alias );

        if ( preg_match( "/^[0-9]+$/i", $this->articles_alias ) )
            $this->articles_alias = 'articles-' . $this->articles_alias;

        if ( $iNum )
            $this->articles_alias .= $iNum;

        $this->articles_alias = \skTranslit::changeDeprecated( $this->articles_alias );
        $this->articles_alias = \skTranslit::mergeDelimiters( $this->articles_alias );
        $this->articles_alias = trim($this->articles_alias, '-');

        // find
        $oResult = orm\Query::SQL(
            sprintf(
                "SELECT `id` FROM `%s` WHERE `articles_alias` = ?;",
                $this->getTableName()
            ),
            $this->articles_alias
        );


        while ( $aRow = $oResult->fetchArray() ) {

            if ( $aRow['id'] != $this->id )
                return $this->checkAlias( ++$iNum );

        }

        return true;
    }

    public function save()
    {
        $bRes = parent::save();

        $oSearch = new Search();
        $oSearch->updateByObjectId($this->id);

        return $bRes;
    }

    public function delete()
    {

        $id = $this->id;

        $bRes = parent::delete();

        $search = new Search();
        $search->deleteByObjectId( $id );

        return $bRes;
    }

}