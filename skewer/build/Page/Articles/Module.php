<?php

namespace skewer\build\Page\Articles;

use skewer\build\Component\Section\Page;
use skewer\build\Component\SEO;
use skewer\build\Component\Site;
use yii\web\NotFoundHttpException;


/**
 * Модуль системы статей
 * Class Module
 * @package skewer\build\Page\Articles
 */
class Module extends \skModule {

    public $parentSections;
    public $onPage = 10;
    public $listTemplate = 'list.twig';
    public $detailTemplate = 'detail_page.twig';
    public $titleOnMain = 'Статьи';
    public $showFuture;
    public $showOnMain;  // отменяет фильтр по разделам
    public $allArticles;
    public $sortArticles;
    public $showList;
    // public $usePageLine = 1;

    private $iCurrentSection;

    public $aParameterList = array(
        'order'    => 'DESC',
        'future'   => '',
        'byDate'   => '',
        'on_page'  => '',
        'on_main'  => '',
        'section'  => '',
        'active'  => '',
        'all_articles' => '',
        'show_list'=> '',
    );

    public function init() {

        $this->setParser(parserTwig);
        $this->iCurrentSection = $this->getEnvParam('sectionId');
        $this->aParameterList['on_page'] = $this->onPage;

        if ( $this->allArticles ) $this->aParameterList['all_articles'] = 1;
        if ( $this->showOnMain ) $this->aParameterList['all_articles'] = 1;

        if ( $this->parentSections ) $this->aParameterList['section'] = $this->parentSections;
        else $this->aParameterList['section'] = $this->iCurrentSection;

        if ( $this->showFuture ) $this->aParameterList['future'] = 1;
        if ( $this->sortArticles ) $this->aParameterList['order'] = $this->sortArticles;

        if ( $this->showList ) $this->aParameterList['show_list'] = 1;

        return true;

    }// func

    protected function onCreate() {
        if ( $this->showList )
            $this->setUseRouting( false );
    }

    public function execute() {

        $iPage       = $this->getInt('page', 1);
        $iArticlesId     = $this->getInt('articles_id', 0);
        $sArticlesAlias  = $this->getStr('articles_alias', '');
        $sDateFilter = $this->getStr('date');

        if ( !empty($sDateFilter) ) {
            $sDateFilter = date('Y-m-d', strtotime($sDateFilter));
            $this->aParameterList['byDate'] = $sDateFilter;
        }

        /*
         * Если в запросе передается ID или её алиас, значит необходимо вывести конкретную статью;
         * если нет - выводим список, разбитый по страницам.
         */
        if ( ($iArticlesId || $sArticlesAlias) && !$this->showList ) {

            /** @var Model\ArticlesRow $oArticlesRow */
            if ( $sArticlesAlias )
                $oArticlesRow = Model\Articles::getPublicByAlias( $sArticlesAlias );
            else
                $oArticlesRow = Model\Articles::getPublicById( $iArticlesId );

            if ( !$oArticlesRow )
                throw new NotFoundHttpException();

            // Убрать статический контент
            if ( !Site\Page::rootModuleComplete() )
                return psWait;
            Site\Page::clearStaticContent();

            $aRow = $oArticlesRow->getData();
            $aRow['publication_date'] = date( "d.m.Y", strtotime( $oArticlesRow->publication_date ) );

            $bHideDate = Page::getVal('content', 'hideDate');
            if( $bHideDate && $oArticlesRow->last_modified_date ) // todo ??
                unSet( $aRow['last_modified_date'] );
            else
                $aRow['last_modified_date'] = date( "d.m.Y H:i", strtotime($oArticlesRow->last_modified_date) );

            $this->setData( 'aArticlesItem', $aRow );

            $this->setSEO( $aRow );

            // меняем заголовок
            Site\Page::setTitle( $aRow['title'] );

            // добавляем элемент в pathline
            Site\Page::setAddPathItem( $aRow['title'] );

            $this->setTemplate( $this->detailTemplate );

        } else {

            // Получаем список новостей
            $iCount = 0;
            $aArticlesList = Model\Articles::getPublicList( $this->showList ? 1 : $iPage, $this->aParameterList, $iCount );

            if ( $iCount ) {

                $this->setData( 'aArticlesList', $aArticlesList );
                $this->setData( 'articles_section', $this->parentSections );

                $aURLParams = array();

                if( !empty($sDateFilter) )
                    $aURLParams['date'] = $sDateFilter;

                $this->getPageLine(
                    $iPage,
                    $iCount,
                    $this->iCurrentSection,
                    $aURLParams,
                    array( 'onPage' => $this->aParameterList['on_page'] )
                );

            }

            $this->setData('titleOnMain',$this->titleOnMain);

            $this->setTemplate($this->listTemplate);

        }

        Site\Page::reloadSEO();

        return psComplete;
    }// func

    public function shutdown() {}

    /**
     * Метатеги для списка новостей
     * @param $aArticlesItem
     */
    private function setSEO($aArticlesItem) {

        $SEOData = $this->getEnvParam( 'SEOTemplates', array() );
        $SEOData[ SEO\Api::TPL_ARTICLES ] = array( 'title' => '', 'description' => '', 'keywords' => '' );
        if ( $oDataRow = SEO\Api::get( 'articles', $aArticlesItem['id'] ) )
            $oDataRow->setParams( $SEOData, SEO\Api::TPL_ARTICLES );
        $this->setEnvParam( 'SEOTemplates', $SEOData );
        $SEOVars = $this->getEnvParam( 'SEOVars', array() );
        $SEOVars['label_article_title_upper'] = $aArticlesItem['title'];
        $SEOVars['label_article_title_lower'] = mb_strtolower( $aArticlesItem['title'] );
        $this->setEnvParam( 'SEOVars', $SEOVars );

        Site\Page::reloadSEO();
    }

}// class
