<?php

/* main */
$aConfig['name']     = 'Articles';
$aConfig['version']  = '1.0';
$aConfig['title']    = 'Статьи';
$aConfig['description']  = 'Модуль статей';
$aConfig['revision'] = '0002';
$aConfig['layer']     = \Layer::PAGE;
$aConfig['useNamespace'] = true;


/* Функциональные политики */
$aConfig['policy'][] = array(
    'name'    => 'allowArticlesListRead',
    'title'   => 'Разрешить чтение списка статей',
    'default' => 1
);

$aConfig['policy'][] = array(
    'name'    => 'allowArticlesDetailRead',
    'title'   => 'Разрешить чтение детальной статей',
    'default' => 1
);

return $aConfig;
