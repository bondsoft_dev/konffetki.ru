$(function(){
    
 
    
    var bDebug = false;

    if(bDebug){

        console.log('Document rendered: %s',new Date());
    }//is debug

    $('.js_use_resize').fancybox({
        openEffect: 'fade',
        closeEffect: 'fade',
        nextEffect: 'none',
        prevEffect: 'none',
        arrows    : true,
        nextClick : true,
        mouseWheel: true
    });

    $(".callback").click(function(){
        var section = $(this).attr('section');
        var ajaxForm = $(this).attr('ajaxForm');
        var label = $(this).attr('label');
        var link = $(this);

        var module = $(this).attr('module');
        var cmd = $(this).attr('cmd');
        var idObj = $(this).attr('idObj');
        var count = 1;

        if (!module) module = 'Forms';
        if (!cmd) cmd = 'show';
        if (!idObj) idObj = 0;
        else {
            var countInput = $(".js_count[data-id='"+idObj+"']");

            if (parseInt(countInput.val())>1){
                count = parseInt(countInput.val());
            }
        }

        $.post( '/ajax/ajax.php', {
                moduleName: module,
                cmd: cmd,
                section: section,
                ajaxForm: ajaxForm,
                ajaxShow: 1,
                label: label,
                idObj: idObj,
                count:count,
                language: $('#current_language').val()
            },
            function ( mResponse ) {

                if (!mResponse) return false;

                var oResponse = $.parseJSON( mResponse );

                var formDiv = $( '#callbackForm' );

                if (oResponse.css && oResponse.css.length){
                    $(oResponse.css).each(function(){
                        if (!$('link [href="' + this.filePath + '"]').size()){
                            var fileRef=document.createElement("link");
                            fileRef.setAttribute("rel", "stylesheet");
                            fileRef.setAttribute("type", "text/css");
                            fileRef.setAttribute("href", this.filePath) ;
                            document.getElementsByTagName("head")[0].appendChild(fileRef);
                        }
                    });
                }

                if (oResponse.js && oResponse.js.length){
                    $(oResponse.js).each(function(){
                        if (!$('script [src="' + this.filePath + '"]').size()){
                            var fileRef=document.createElement("script");
                            fileRef.setAttribute("type", "text/javascript");
                            fileRef.setAttribute("charset", "utf-8");
                            fileRef.setAttribute("src", this.filePath) ;
                            document.getElementsByTagName("head")[0].appendChild(fileRef);
                        }
                    });
                }

                formDiv.html( oResponse.html );
                var formId = formDiv.find('form:first' ).attr( 'id' );

                if ( typeof formId != 'undefined' ) {

                    maskInit(formDiv);

                    updateFromValidator(formId);

                    /* calendar - инициализация календарика */
                    if ( typeof $.datepicker !== 'undefined' ) {
                        $.datepicker.setDefaults({
                            dateFormat: 'dd.mm.yy'
                        });

                        $( '.js_init_datepicker', '#' + formId ).datepicker();
                    }
                }

                var max_width = link.attr('js_max_width');
                if ( !max_width ){
                    max_width = 9999
                }
                else{
                    formDiv.css( { width: max_width } );
                }

                var max_height= link.attr('js_max_height');
                if ( !max_height ){
                    max_height = 9999
                }else{
                    formDiv.css( { height: max_height } );
                }

                $.fancybox.open('#callbackForm',{
                    dataType : 'html',
                    autoSize: true,
                    autoCenter: true,
                    width:max_width,
                    height:max_height,
                    openEffect: 'none',
                    closeEffect: 'none',
                    'afterClose': function(){
                        $('.img_captcha').each( function() {
                            reloadImg( this );
                        });
                    },
                    'onStart':function(){
                        $("#callbackForm").show();
                    },
                    'onClosed':function(){
                        $("#callbackForm").hide();
                    }
                });

                return true;
            });

        return false;
    });

});

function reloadImg( obj ) {

    if ( !obj ) return false;
    obj = $( obj );

    var src = obj.attr('src');
    var pos = src.indexOf('?');

    if ( pos >= 0 )
        src = src.substr(0, pos);

    var hash = '';
    var form = $(obj).parents('form');
    if ( form.size() && form.attr('id') )
        hash = form.attr('id').substr(5);

    var date = new Date();
    obj.attr( 'src', src + '?h=' + hash + '&v=' + date.getTime() );

    return false;
}
