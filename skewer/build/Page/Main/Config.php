<?php

/* main */
$aConfig['name']     = 'Main';
$aConfig['title']    = 'Страница сайта';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Модуль сборки типовой страницы сайта по параметрам раздела';
$aConfig['revision'] = '0002';
$aConfig['layer']     = \Layer::PAGE;
$aConfig['useNamespace'] = true;
$aConfig['languageCategory']     = 'page';

/* Функциональные политики */
$aConfig['policy'][] = array(
    'name'    => 'allowPageRead',
    'title'   => 'Разрешить чтение страницы',
    'default' => 1
);

$aConfig['policy'][] = array(
    'name'    => 'allowPageWrite',
    'title'   => 'Разрешить запись страницы',
    'default' => 1
);

return $aConfig;
