<?php

namespace skewer\build\Page\Main;

use Yii;
use yii\helpers\FileHelper;
use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle {

    public $sourcePath = '@skewer/build/Page/Main/web/';

    public $css = [
        'css/param.css',
        'css/main.css',
        'css/superfast.css',
        //'css/shcart.css',
        'css/typo.css'
    ];

    public $js = [
        'js/accordion.js',
        'js/jquery.carouFredSel-6.2.1-packed.js',
        'js/pageInit.js'
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public function init()
    {

        if (\Design::modeIsActive())
            array_unshift( $this->css, 'css/design.css' );

        parent::init();
        $this->publishOptions['beforeCopy'] = function ($from, $to) {
            if (is_dir($from) && basename($from)=='images'){
                FileHelper::copyDirectory($from,WEBPATH.'images/');
            }
            return true;
        };
    }

}
