<?php

namespace skewer\build\Page\Main;


use skewer\build\Component\Section\Page;
use skewer\build\Component\Section\Tree;
use skewer\build\Component\Section\Parameters;
use skewer\build\Design\Zones\Api;
use skewer\build\Component\SEO;
use yii\helpers\ArrayHelper;
use yii\web\ServerErrorHttpException;


/**
 * Модуль построения раздела публичной части сайта
 */
class Module extends \skModule {
    /**
     * Id текущего раздела
     * @var int
     */
    public $pageId = 0;

    /** @var string Шаблон вывода */
    public $templateFile = '';

    /**
     * Зоны
     * @var array
     */
    protected $aZonesByGroup = [];

    /**
     * параметр если установлен - раздел скрывется из поиска и sitemap
     * Называеться должен как значение skewer\build\Component\Section\Tree::removeFromSearchParam
     * @var mixed
     */
    public $removeFromSearch = false;

    public function init() {

        $this->setParser(parserPHP);

        if ( !$this->templateFile )
            throw new ServerErrorHttpException( 'Root process template is not set' );

    }// func

    /**
     * Исполнение модуля
     * @return int
     */
    public function execute() {

        $this->pageId =  $this->getInt('pageId', $this->pageId);

        /**
         * #fix404
         * на 404 может перекинуть после отработки нескольких модулей, надо сбросить уже установленные параметры окружения
         */
        if ($this->pageId == \Yii::$app->sections->page404()){
            \Yii::$app->environment->clear();
            // и не учитывать неразобранный хвост URL
            \Yii::$app->router->setUriParsed();
        }

        /**
         * todo #stab_del вот от этой константы в окружении нужно обязательно отказаться и сделать
         *      другой механтизм распространения id текущей страницы
         */
        $this->setEnvParam('sectionId', $this->pageId);
        $this->setData('sectionId',$this->pageId);

        $this->setData('mainId', \Yii::$app->sections->main());

        if ( \Design::modeIsActive() ) {
            $this->setData('designMode', \Design::getDirList() );
        }

        foreach( Page::getByGroup(Parameters::settings) as $aParamsSet){
            $this->setData($aParamsSet['name'], ['value' => $aParamsSet['value'], 'text' => $aParamsSet['show_val']]);
        }

        $this->setLayout();

        $this->setProcess();

        if($version = $this->getStr('version') == 'print')
        {
            $this->setParser(parserTwig);
            $this->templateFile = 'print.twig';
        }

        $this->setSEO();

        $this->setData('SiteName', \Site::domain());

        $this->setData('canonical_url', false);

        $this->setTemplate( $this->templateFile );

        return psComplete;

    }

    /**
     * Обрабатываем шаблон расположения (layout)
     */
    protected function setLayout(){

        $aZones = [];
        $this->aZonesByGroup = [];
        foreach (Page::getByGroup(Api::layoutGroupName) as $sZone => $sZoneVal){

            if ($sZone != '.title' && $sZoneVal['value']){
                $aZoneSource = explode(',', $sZoneVal['value']);
                foreach ($aZoneSource as $label){

                    if( mb_substr($sZone, 0, 1) === '.' )
                        continue;

                    // добавляем инклуды объектных меток
                    if (Page::getVal($label, '.layoutInclude')) {

                        $aZones[$sZone][] = array(
                            'templateFile' => trim(Page::getVal($label, '.layoutInclude')),
                            'templateDir' => $this->getModuleDir() . 'templates/'

                        );
                    }

                    // обрабатываем прямую вставку
                    else {
                        $aZones[$sZone][] = trim($label);
                        $this->aZonesByGroup[trim($label)] = $sZone;
                    } // else

                } // foreach

            }// if

        } // foreach

        $this->setData(Api::layoutGroupName, $aZones);

    }


    /**
     * Формирование списка процессов
     * В список будут включены модули:
     *  * активированные в дизайнерском режиме
     *  * не содержащие параметра layout (системные)
     *  * имеющие флаг force_include
     */
    protected function setProcess(){

        foreach(Page::getGroups() as $sGroupName) {

            if ($sGroupName == Parameters::settings)
                continue;
            
            if (Page::getVal($sGroupName, Parameters::object)) {

                // флаг добавления процесса в список на обработку
                $bAdd = false;
                
                $aParams = Page::getByGroup($sGroupName);
                $aParams = ArrayHelper::map($aParams, 'name', 'value');

                // добавление зоны, если есть
                if ( isset($this->aZonesByGroup[$sGroupName]) ) {
                    $aParams['zone'] = $this->aZonesByGroup[$sGroupName];
                    $bAdd = true;
                }
                
                // если нет метки зоны вывода, то модуль системный - добавляем принудительно
                if ( !isset($aParams['layout']) )
                    $bAdd = true;

                // флаг принудаительно включения модйля в список на обработку
                if (isset($aParams['force_include']) and $aParams['force_include'])
                    $bAdd = true;

                // Добавление процесса, если есть флаг
                if ( $bAdd )
                    $this->addChildProcess(new \skContext($sGroupName, Page::getVal($sGroupName, Parameters::object), ctModule, $aParams));

            }

        }// each

    }


    /**
     * @return array
     * todo нуждается в рефакторинге
     */
    public function setSEO() {

        // SEO Метатеги для страницы
        $SEOData = $this->getEnvParam( 'SEOTemplates', array() );

        $SEOData['text'] = [
            'title' => '',
            'description' => '',
            'keywords' => '',
            'none_index' => false,
            'add_meta' => '',
        ];
        if ( $oDataRow = SEO\Api::get( 'section', $this->pageId ) )
            $oDataRow->setParams( $SEOData, 'text' );

        $this->setEnvParam( 'SEOTemplates', $SEOData );
        $SEOVars = $this->getEnvParam( 'SEOVars', array() );

        $SEOVars['label_page_title_upper'] = Tree::getSectionTitle( $this->pageId, true  );
        $SEOVars['label_page_title_lower'] = mb_strtolower($SEOVars['label_page_title_upper']);

        // get path line
        $stopSections = Page::getVal('pathLine', 'stopSections');
        if ($stopSections)
            $stopSections = explode(',', $stopSections);
        else
            $stopSections = [];

        $stopSections[] = \Yii::$app->sections->topMenu();
        $stopSections[] = \Yii::$app->sections->leftMenu();
        $stopSections[] = \Yii::$app->sections->serviceMenu();
        $stopSections[] = \Yii::$app->sections->tools();


        $SEOVars['label_path_to_main_upper'] = '';
        $SEOVars['label_path_to_page_upper'] = '';

        $cur = $this->pageId;
        while ( $cur = Tree::getSectionParent( $cur, true ) ) {

            if ( in_array($cur, $stopSections) )
                break;

            $SEOVars['label_path_to_main_upper'] .= ' ' . Tree::getSectionTitle( $cur, true ) . '.';
            $SEOVars['label_path_to_page_upper'] = Tree::getSectionTitle( $cur, true ) . '. ' . $SEOVars['label_path_to_page_upper'];
        }

        $SEOVars['label_path_to_main_lower'] = mb_strtolower($SEOVars['label_path_to_main_upper']);
        $SEOVars['label_path_to_page_lower'] = mb_strtolower($SEOVars['label_path_to_page_upper']);
        // Название сайта

        $SEOVars['label_site_name'] = \Site::getSiteTitle();

        $this->setEnvParam('SEOVars', $SEOVars);
    }

}
