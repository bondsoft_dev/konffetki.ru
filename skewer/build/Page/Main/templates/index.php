<?php
/**
 * @var $this \yii\web\View
 * @var string $SEOTitle
 * @var string $SEOKeywords
 * @var string $SEODescription
 * @var string $canonical_url
 */

use \yii\helpers\Html;
                                 
skewer\assets\JqueryAsset::register($this);
skewer\assets\JqueryUiAsset::register($this);
skewer\assets\FancyboxAsset::register($this);
skewer\assets\DatepickerAsset::register($this);

\Design::assetsRegister( $this, \Layer::PAGE );

$pageBundle = \skewer\build\Page\Main\Asset::register($this);

if (isset($designMode)){
    \skewer\assets\DesignAsset::register($this);
}

//$b = Yii::$app->getAssetManager()->bundles;
//include( ROOTPATH . "seo_titles_inc.php");
?>
<?php $this->beginPage() ?>

<? 
include( ROOTPATH . "seo_titles_inc.php");

$url = $_SERVER["REQUEST_URI"];
if( isset($seoMetaData[$url]) ) {
    $SEOTitle = $seoMetaData[$url]["TITLE"];
    $SEODescription = $seoMetaData[$url]["DESCRIPTION"];
    $SEOKeywords = $seoMetaData[$url]["KEYWORDS"];
}

?>

<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= \Yii::$app->language ?>" >
<head>
    <meta charset="utf-8"  content="text/html" />
    <meta name='yandex-verification' content='52b4d51980f5c9ff' />
    <meta name="google-site-verification" content="VHaF3znc4j5ZNi2KlmikpSWjEDkVyPtIqbWSI1TnNi8" />
    <? // todo SEO переписать по Yii-ому ?>
    <title><?= Html::encode($SEOTitle) ?></title>
    <meta name="description" content="<?= Html::encode($SEODescription) ?>" />
    <meta name="keywords" content="<?=Html::decode($SEOKeywords) ?>" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?$curPage = Yii::$app->request->PathInfo;?>
    <?if(($curPage === 'interesnaya-informaciya/') || (strpos($curPage, 'interesnaya-informaciya/page/') !== false)):?>
        <link rel="canonical" href="http://www.konffetki.ru/interesnaya-informaciya" />
    <?endif?>
    <link rel="shortcut icon" href="<?= Design::get('page','favicon') ?>" type="image/png" />
    <? if (isSet($SEONonIndex) && $SEONonIndex): ?><meta name="robots" content="none"/><? endif ?>
    <? if (isSet($SEOAddMeta) && $SEOAddMeta): ?><?=Html::decode($SEOAddMeta) ?><? endif ?>
	
	<?
	
	// Заплатка для страниц пагинацции
	if( strpos( $_SERVER["REQUEST_URI"] , "page=" ) ) {
		
		$ex = explode('?',$_SERVER["REQUEST_URI"]);
		$canonical_url = "http://".$_SERVER["HTTP_HOST"].$ex[0];
		
	}

	?>
	
	

    <? if ($canonical_url): ?>
        <link rel="canonical" href="<?= $canonical_url ?>" />
    <? endif ?>

    <? if (isset($aLangLinks)): ?>
        <? foreach ($aLangLinks as $links): ?>
            <meta rel="alternate" hreflang="<?=Html::decode($links['hreflang']) ?>" href="<?=Html::decode($links['href']) ?>" />
        <? endforeach ?>
    <? endif ?>

    <?php $this->head() ?>

    <?php
        // todo #stab_fix перенести в отдельный Asset и цеплять в самом низу, если получится
        $sFileName = \Design::getAddCssFilePath();
        $lastUpdatedTime = Design::getLastUpdatedTime();
        if ( file_exists($sFileName) )
            echo Html::tag('link', '', [
                'rel' => 'stylesheet',
                'href' => str_replace(WEBPATH,'/', $sFileName).'?v='.$lastUpdatedTime,
                'type' => 'text/css',
                'media' => 'screen, projection, all'
            ]);
    ?>

    <? $jsItems = Design::assetJs(); ?>
    <? foreach ($jsItems as $js): ?>
        <?= $js['line'] ?>
    <? endforeach ?>

    <? # дополнительный блок в заголовке ?>
    <? if (isSet($addHead['text']) && $addHead['text']): ?><?=Html::decode($addHead['text']) ?><? endif ?>

    <? # должен быть непосредственно перед закрывающим тегом </head> ?>
    <? if (isSet($gaCode['text']) && $gaCode['text']): ?><?=Html::decode($gaCode['text']) ?><? endif ?>
	
	<link href="/css/jquery.mmenu.all.css" rel="stylesheet">
	<link href="/css/mobile2.css?v=2" rel="stylesheet">

</head>

    <body cur-page="<?=$url;?>" class="<?= $page_class['value'] ?>" sktag="page" <? if (isset($specMenu_bodyFontSize)): ?> style="font-size: <?= $specMenu_bodyFontSize?>px;"<? endif ?> <? if (isset($designMode)): ?> sectionid="<?= $sectionId ?>"<? endif ?>>
	<div id="body-inner">
    <?php $this->beginBody() ?>
    <input type="hidden" id="current_language" value="<?= \Yii::$app->language ?>">
        <div class="l-container">
            <div class="container__page">
                <div class="container__content">
				
					<div class="header_mobile">
						<div class="row">
							<div class="col-md-12">
									
									<a class="call" href="tel:+79671245610"></a>
									<div class="logo_m">
										<a class="logo__link" href="/">
											<img alt="" class="logo-image" src="/images/logo.png">
										</a>
										<span class="logo__text">
											<div class="sl">Конфетки</div>
										</span>
									</div>	
									<div class="mobile_menu" id="open_meny">
									Меню
									</div>

							</div>
						</div>
						
						<div style="display:none;">
						<div id="for_mobile_meny">
							<ul id="fmm"></ul>
						</div>
						</div>
									
					</div>
				
				
				
				
                    <div class="b-pilot" sktag="page.head" sklayout="head">
                        <div class="b-logo <? if (isset($designMode)): ?>g-ramaborder js-designDrag-left<? endif ?>" sktag="page.head.logo"><a href="<?= '[' . $mainId . ']' ?>"><img src="<?= Design::get('page.head.logo','logo')?>"></a>
                        <? if (isset($designMode)): ?>
                            <div class="b-desbtn"><span>logo</span><ins></ins></div>
                        <? endif ?>
                        </div>
                        <div class="pilot__1 <? if (isset($designMode)): ?>g-ramaborder js-designDrag-<?= Design::get('page.head.pilot1','h_position')?>" sktag="page.head.pilot1" skeditor="./headtext1<? endif ?>"><?= $headtext1['text'] ?>
                            <? if (isset($designMode)): ?>
                            <div class="b-desbtn"><span>txt1</span><ins></ins></div>
                            <? endif ?>
                        </div>
                        <div class="pilot__2 <? if (isset($designMode)): ?>g-ramaborder js-designDrag-<?= Design::get('page.head.pilot2','h_position')?>" sktag="page.head.pilot2" skeditor="./headtext2<? endif ?>"><?= $headtext2['text'] ?>
                            <? if (isset($designMode)): ?>
                            <div class="b-desbtn"><span>txt2</span><ins></ins></div>
                            <? endif ?>
                        </div>
                        <div class="pilot__3 <? if (isset($designMode)): ?>g-ramaborder js-designDrag-<?= Design::get('page.head.pilot3','h_position')?>" sktag="page.head.pilot3" skeditor="./headtext3<? endif ?>"><?= $headtext3['text'] ?>
                            <? if (isset($designMode)): ?>
                                <div class="b-desbtn"><span>txt3</span><ins></ins></div>
                            <? endif ?>
                        </div>
                        <div class="pilot__4 <? if (isset($designMode)): ?>g-ramaborder js-designDrag-<?= Design::get('page.head.pilot4','h_position')?>" sktag="page.head.pilot4" skeditor="./headtext4<? endif ?>"><?= $headtext4['text'] ?>
                            <? if (isset($designMode)): ?>
                                <div class="b-desbtn"><span>txt4</span><ins></ins></div>
                            <? endif ?>
                        </div>
                        <div class="pilot__5 <? if (isset($designMode)): ?>g-ramaborder js-designDrag-<?= Design::get('page.head.pilot5','h_position')?>" sktag="page.head.pilot5" skeditor="./headtext5<? endif ?>"><?= $headtext5['text'] ?>
                            <? if (isset($designMode)): ?>
                                <div class="b-desbtn"><span>txt5</span><ins></ins></div>
                            <? endif ?>
                        </div>
                    </div>
					
					<div class="top-mobile">
                        <div class="topbar__logo">
                            <img src="/files/design/group.png">
							<div class="b-search" sktag="modules.search">
								<form method="GET" class="js-search" action="/search/">
									<button type="submit"></button>
									<div class="search_inputbox"><div><input value="" name="search_text" id="search_text" /></div></div>
								</form>
							</div>
							
                        </div>

                        <div class="topbar__b1">
                            <div class="b-headwork">
<p>Режим работы оператора</p>

<p><strong>10.00–18.00</strong> (ежедневно)</p>
</div>
                        
                            <div class="b-headphone">
<p>+7 (967) 124-56-10</p>
</div>
                        </div>
                        <div class="topbar__b2" style="display: none;">
                            <div class="b-headphone">
<p>+7 (967) 124-56-10</p>
</div>
                        </div>                      
                        <noindex>
    <div class="b-basketmain">
        <div class="basketmain__wrap minicart__content minicart__content-hidden">
            <p><a href="/cart/">Товаров: <span class="goods-count">0</span><br><span class="goods-total">0.00</span> руб</a></p>
        </div>
        <div class="basketmain__wrap minicart__empty">
            <p><a href="/cart/">Корзина пуста</a></p>
        </div>
    </div>

    <div class="b-editor js_cart_fancy_add" style="display: none;">
    <div class="b-basket-notice">
        <div class="basket-title">Товар добавлен в корзину</div>
        <div class="basket-name"></div>
        <div class="basket-btn"><a class="b-btnbox b-btnboxfull" href="/cart/">Перейти в корзину</a></div>
        <div class="basket-link"><a class="js-basket-close" href="#">Продолжить покупки</a></div>
    </div>
</div>
</noindex>
                    </div>

                    <?= (isset($layout['head']))?$layout['head']:'' ?>

                    <div class="column <? if (($hide_right_column['value'] == 1) || !isset($layout['right'])): ?>column_lc<? endif ?> <? if (!isset($layout['left'])): ?>column_cr<? endif ?>">
                        <div class="column__center">
                            <div class="column__center-indent" sklayout="content">
                                <?= (isset($layout['content']))?$layout['content']:'' ?>
                            </div>
                        </div>
                        <? if (isset($layout['left'])):?>
                        <div class="column__left" sklayout="left">
                            <div class="column__left-indent">
                                <?= $layout['left'] ?>
                            </div>
                        </div>
                        <? endif ?>
                        <? if ($hide_right_column['value'] != 1 && isset($layout['right'])): ?>
                        <div class="column__right" sklayout="right">
                            <div class="column__right-indent">
                                <?= $layout['right'] ?>
                            </div>
                        </div>
                        <? endif ?>
                        <div class="column__center-bg">
                            <div class="column__center-inside"></div>
                        </div>
                        <? if (isset($layout['left'])): ?>
                        <div class="column__left-bg">
                            <div class="column__left-inside"></div>
                        </div>
                        <? endif ?>
                        <? if ($hide_right_column['value'] != 1): ?>
                        <div class="column__right-bg">
                            <div class="column__right-inside"></div>
                        </div>
                        <? endif ?>
                    </div>
                    <div class="l-footerbox-stop"></div>
                </div>
            </div>
            <div class="container__opera">
                <div class="container__field">
                    <div class="container__wrapper">
                        <div class="container__left"></div>
                        <div class="container__right"></div>
                    </div>
                </div>
            </div>
            <div class="l-footerbox" sktag="page.footer">
                <div class="footerbox__wrapper">

                    <div class="l-grid">
                        <div class="grid__item1<? if (isset($designMode)): ?> g-ramaborder js-designDrag-<?= Design::get('page.footer.grid1','h_position')?>" sktag="page.footer.grid1" skeditor="./copyright<? endif ?>">
                            <? if (isset($designMode)): ?>
                            <div class="b-desbtn"><span>copyright</span><ins></ins></div>
                            <? endif ?>
                            <?= str_replace('[Year]', date('Y'),$copyright['text']) ?>
                            <?= $copyright_dev['text'] ?>
                        </div>
                        <div class="grid__item2<? if (isset($designMode)): ?> g-ramaborder js-designDrag-<?= Design::get('page.footer.grid2','h_position')?>" sktag="page.footer.grid2" skeditor="./counters<? endif ?>">
                            <? if (isset($designMode)): ?>
                            <div class="b-desbtn"><span>counters</span><ins></ins></div>
                            <? endif ?>
                            <div class="b-counter">
                                <noindex><?= (isset($counters) && isset($counters['text']))?$counters['text']:""?></noindex>
                            </div>
                        </div>
                        <div class="grid__item3<? if (isset($designMode)): ?> g-ramaborder js-designDrag-<?= Design::get('page.footer.grid3','h_position')?>" sktag="page.footer.grid3" skeditor="./contacts<? endif ?>">
                            <? if (isset($designMode)): ?>
                            <div class="b-desbtn"><span>contacts</span><ins></ins></div>
                            <? endif ?>
                            <?= $bottomMenu ?>
                            <?= $contacts['text'] ?>
                        </div>
                        <div class="grid__item4<? if (isset($designMode)): ?> g-ramaborder js-designDrag-<?= Design::get('page.footer.grid4','h_position')?>" sktag="page.footer.grid4" skeditor="./footertext4<? endif ?>">
                            <?= $footertext4['text'] ?>
                            <? if (isset($designMode)): ?>
                            <div class="b-desbtn"><span>txt</span><ins></ins></div>
                            <? endif ?>
                        </div>
                    </div>
                    <div class="footerbox__left"></div>
                    <div class="footerbox__right"></div>

                </div>

            </div>
            <div class="b-topbar js-topbar">
                <div class="container__page">
                    <div class="container__content">
                        <div class="topbar__logo">
                            <img src="<?= Design::get('page.head.logo','logo')?>">
                        </div>
                        <div class="topbar__b1">
                            <?= $headtext3['text'] ?>
                        </div>
                        <div class="topbar__b2">
                            <?= $headtext2['text'] ?>
                        </div>                      
                        <?= (isset($layout['topbar']))?$layout['topbar']:'' ?>
                    </div>
                </div>                
            </div>
        </div>
        <?= (isset($countersCode['text']))?$countersCode['text']:'' ?>
        <div id="callbackForm" style="display: none;"></div>
        <?php $this->endBody() ?>


        <!-- Yandex.Metrika counter -->
        <script type="text/javascript">
            (function (d, w, c) {
                (w[c] = w[c] || []).push(function() {
                    try {
                        w.yaCounter36941965 = new Ya.Metrika({
                            id:36941965,
                            clickmap:true,
                            trackLinks:true,
                            accurateTrackBounce:true,
                            webvisor:true
                        });
                    } catch(e) { }
                });

                var n = d.getElementsByTagName("script")[0],
                    s = d.createElement("script"),
                    f = function () { n.parentNode.insertBefore(s, n); };
                s.type = "text/javascript";
                s.async = true;
                s.src = "https://mc.yandex.ru/metrika/watch.js";

                if (w.opera == "[object Opera]") {
                    d.addEventListener("DOMContentLoaded", f, false);
                } else { f(); }
            })(document, window, "yandex_metrika_callbacks");
        </script>
        <noscript><div><img src="https://mc.yandex.ru/watch/36941965" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
        <!-- /Yandex.Metrika counter -->
	</div>
    </body>
	
	<script src="/js/jquery.mmenu.all.js"></script>
	<script src="/js/mobile10.js"></script>

  
    <script>
(function () {
	var d = document;
	var w = window;

	function l() {
		var s = document.createElement('script');
		s.type = 'text/javascript';
		s.async = true;
		s.src = '/popup-bs/assets/script-bs.js?v=20';
		var ss = document.getElementsByTagName('script')[0];
		ss.parentNode.insertBefore(s, ss);
	}
	if (d.readyState == 'complete') {
		l();
	} else {
		if (w.attachEvent) {
			w.attachEvent('onload', l);
		} else {
			w.addEventListener('load', l, false);
		}
	}
})();
</script>

</html>
<?php $this->endPage() ?>
