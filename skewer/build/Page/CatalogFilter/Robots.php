<?php

namespace skewer\build\Page\CatalogFilter;


use skewer\build\Component\Catalog\Attr;
use skewer\build\Component\orm\Query;

class Robots implements \skRobotsInterface{

    public static function getRobotsDisallowPatterns(){

        /**
         * собираем список полей, выводимых в фильтре
         * @todo наверное можно по-другому
         */
        $oQuery = Query::SQL('
             SELECT field.`name`
             FROM `c_field` as `field` LEFT JOIN `c_field_attr` AS attr
             ON attr.`field`=`field`.`id`
             WHERE attr.`tpl`=:tpl
             AND attr.`value`=1;', ['tpl' => Attr::SHOW_IN_FILTER])
        ;

        $aResult = array();

        while ($aRow = $oQuery->fetchArray()){
            $aResult[] = '/*' . $aRow['name'] . '=';
        }

        return $aResult;
    }

    public static function getRobotsAllowPatterns(){
        return false;
    }
}