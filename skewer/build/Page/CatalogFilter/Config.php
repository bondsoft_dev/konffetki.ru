<?php

$aConfig['name']     = 'CatalogFilter';
$aConfig['title']    = 'Фильтр каталожных позиций';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Модуль для вывода фильтра каталожных позиций';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::PAGE;
$aConfig['languageCategory'] = 'catalogFilter';

return $aConfig;