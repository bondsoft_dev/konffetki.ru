<?php

namespace skewer\build\Page\CatalogFilter;


use skewer\build\Component\Cache;
use skewer\build\Component\Catalog;
use skewer\build\Component\Section\Page;
use skewer\build\Component\Section\Parameters;
use skewer\build\Component\Site;
use skewer\build\Component\SEO\Api;
use skewer\build\libs\ft;


/**
 * Модуль вывода формы поиска/фильтрации товарных позиций в каталоге
 * Class Module
 * @package skewer\build\Page\CatalogFilter
 */
class Module extends \skModule {

    /** @var string Заголовок в форме */
    public $title = '';

    /** @var string Шаблон для вывода формы */
    public $tpl = 'filter.twig';

    /** @var int Раздел из которого будет взята форма */
    public $linkedSection = 0;

    /** @var int Идентификатор текущего раздела */
    private $iCurrentSection;


    public function init() {
        $this->iCurrentSection = $this->getEnvParam('sectionId');
    }


    public function execute() {

        $aOut = [];

        if ( $this->linkedSection ) {

            // вывод формы поиска с другой станицы
            if ( $sSearchCard = $this->getSearchCard( $this->linkedSection ) ) {

                $aOut = Catalog\Filters::get4Card( $sSearchCard );

                $this->setData( 'action', \Yii::$app->router->rewriteURL( "[" . $this->linkedSection . "]" ) );
            }

        } elseif ( $sSearchCard = $this->getSearchCard( $this->iCurrentSection ) ) {

            // форма поиска
            $aOut = Catalog\Filters::get4Card( $sSearchCard );

        } else {

            // форма фильтра
            if ( $this->isShowFilter() )
                $aOut = Catalog\Filters::get4Section( $this->iCurrentSection );

        }


        if ( $aOut ) {
            $bsFilterAr = array();
            $bsCounter = 0;
            //Обходим по фильтрам
            foreach ($aOut as $keyFilter => $valueFilter) {
                //Обходим по полям фильтра
                foreach ($valueFilter['items'] as $keyFilterField => $valueFilterField) {
                    if($valueFilterField['check']){
                        $bsFilterAr[$bsCounter]['catName'] = $valueFilter['title'];
                        $bsFilterAr[$bsCounter]['catUrl'] = $valueFilter['name'];
                        $bsFilterAr[$bsCounter]['id'] = $valueFilterField['id'];
                        $bsFilterAr[$bsCounter]['title'] = $valueFilterField['title'];
                        $bsCounter++;
                    }
                }
            }
            if($bsCounter == 1) {
                $pageProperties = \Yii::$app->environment->getAll();
                $bsSeoTitle = trim($pageProperties['SEOVars']['label_page_title_upper']);
                if($bsFilterAr[0]['catUrl'] === 'proizvoditel') {
                    $bsSeoTitle .= '. '.trim($bsFilterAr[0]['catName']) . ' – фабрика ' . trim($bsFilterAr[0]['title']);
                } elseif ($bsFilterAr[0]['catUrl'] === 'strana') {
                    $bsSeoTitle .= '. '.trim($bsFilterAr[0]['catName']) . ' – ' . trim($bsFilterAr[0]['title']);
                } elseif ($bsFilterAr[0]['catUrl'] === 'nachinka') {
                    $bsSeoTitle .= '. '.trim($bsFilterAr[0]['catName']) . ' – ' . trim($bsFilterAr[0]['title']);
                }

                \Yii::$app->environment->set(
                        'SEOTemplates',
                        array('text' => array(
                                'title' => $bsSeoTitle .': купить в розницу по выгодной цене в Москве - интернет-магазин Конффетки',
                                'description' => $bsSeoTitle .'. Порадуйте себя и своих близких — закажите сладости в интернет-магазине Конффетки!',
                                'keywords' => $bsSeoTitle
                        ))
                );
                \skewer\build\Component\Site\Page::setTitle($bsSeoTitle);
            }
            $this->setData( 'flex', $this->zone == 'content' );
            $this->setData( 'Field4Filter', $aOut );
            $this->setData( 'title', $this->title ? $this->title : \Yii::t('catalogFilter', 'selection') );
            $this->setTemplate( $this->tpl );
        }
        if(strpos($_SERVER['REQUEST_URI'], '?') !== false) {
            Site\Page::getRootModule()->setData('staticContent2', array());
        }

        return psComplete;
    }


    private function isShowFilter() {

        return Page::getVal('content', 'showFilter');

    }


    private function getSearchCard( $section ) {

        return Parameters::getValByName( $section, 'content', 'searchCard');

    }


}