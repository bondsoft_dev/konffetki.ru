<?php

namespace skewer\build\Page\CatalogFilter;


class Install extends \skModuleInstall {

    public function init() {
        return true;
    }// func

    public function install() {

        $this->setParameter(\Yii::$app->sections->tplNew(), '.title', 'CatalogFilter', 'Фильтр для каталога', '', '', 0);
        $this->setParameter(\Yii::$app->sections->tplNew(), 'layout', 'CatalogFilter', 'left', '', '', 0);
        $this->setParameter(\Yii::$app->sections->tplNew(), 'object', 'CatalogFilter', 'CatalogFilter', '', '', 0);

        return true;
    }// func

    public function uninstall() {
        return true;
    }// func

}// class
