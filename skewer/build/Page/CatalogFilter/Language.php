<?php

$aLanguage = array();

$aLanguage['ru']['CatalogFilter.Page.tab_name'] = 'Фильтр каталожных позиций';
$aLanguage['ru']['isActive'] = 'Выводить фильтр для товаров';
$aLanguage['ru']['selection'] = 'Подбор';
$aLanguage['ru']['find'] = 'Найти';
$aLanguage['ru']['clear'] = 'Сбросить';
$aLanguage['ru']['BlockTitle'] = 'Заголовок для формы фильтра';

$aLanguage['en']['CatalogFilter.Page.tab_name'] = 'Filter catalog items';
$aLanguage['en']['isActive'] = 'Output filter for goods';
$aLanguage['en']['selection'] = 'Selection';
$aLanguage['en']['find'] = 'Find';
$aLanguage['en']['clear'] = 'Clear';
$aLanguage['en']['BlockTitle'] = 'Title for the filter form';

return $aLanguage;