<?php

namespace skewer\build\Page\CategoryViewer;

use skewer\build\Component\Site;

class Install extends \skModuleInstall {

    public function init() {
        return true;
    }

    public function install() {

        /**
         * @todo А КАК ИЗ КОНФИГА ВЫТАЩИТЬ???
         */
        $sGroupParam = 'CategoryViewer';

        $iMainTplId = \Yii::$app->sections->tplNew();

        $this->addParameter( $iMainTplId, 'object', 'CategoryViewer', '', $sGroupParam );
        $this->addParameter( $iMainTplId, '.title', 'Список разделов', '', $sGroupParam );
        $this->addParameter( $iMainTplId, 'layout', 'content', '', $sGroupParam );

        return true;
    }

    public function uninstall() {

        $sGroupParam = 'CategoryViewer';

        $iMainTplId = \Yii::$app->sections->tplNew();

        $this->removeParameter( $iMainTplId, 'object', $sGroupParam );
        $this->removeParameter( $iMainTplId, '.title', $sGroupParam );
        $this->removeParameter( $iMainTplId, 'layout', $sGroupParam );

        return true;
    }
}