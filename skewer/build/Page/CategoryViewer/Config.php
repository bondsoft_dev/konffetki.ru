<?php

$aConfig['name']     = 'CategoryViewer';
$aConfig['title']    = 'Категории на главной';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Модуль вывода категорий на главной';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::PAGE;

$aConfig['param_group'] = 'CategoryViewer'; // Группа параметров

return $aConfig;
