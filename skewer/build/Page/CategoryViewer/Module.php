<?php

namespace skewer\build\Page\CategoryViewer;


use skewer\build\Component\Section\Parameters;
use yii\helpers\ArrayHelper;
use skewer\build\Component\Section\Tree;

class Module extends \skModule {

    public $iSectionId = 0;

    /** @var int нужно выводить подразделы */
    public $category_parent = 0;

    /** @var int раздел из которого выводить. 0 - текущий */
    public $category_from = 0;

    public function init() {
        $this->iSectionId = (int)$this->getEnvParam('sectionId');
    }

    public function execute() {

        if ( !$this->category_parent )
            return psComplete;

        $iFromSection = $this->category_from ? $this->category_from : $this->iSectionId;

        /** @var array[] $aSections полный набор разделов */
        $aSections = Tree::getSubSections( $iFromSection, true );

        foreach ($aSections as $sKey => $aSection) {
            if (!ArrayHelper::getValue($aSection, 'visible', 0)){
                unset($aSections[$sKey]);
            }
        }

        if ( empty( $aSections ) )
            return psComplete;


        /** @var int[] $aSectionList полный набор идентификаторов разделов */
        $aSectionList = array_keys($aSections);

        if ( !$aSectionList )
            return psComplete;

        /** @var string $sParamGroup Группа параметров модуля */
        $sParamGroup = $this->getConfigParam('param_group');

        $aShowValList = Parameters::getList( $aSectionList )->group( $sParamGroup)->name('category_show')->asArray()->get();
        $aShowValList = ArrayHelper::map( $aShowValList, 'parent', 'value' );
        $aImgList = Parameters::getList( $aSectionList )->group( $sParamGroup)->name('category_img')->asArray()->get();
        $aImgList = ArrayHelper::map( $aImgList, 'parent', 'value' );

        $aOutList = array();
        // перебираем все разделы
        foreach ( $aSections as $aSect ) {

            $iSectId = (int)$aSect['id'];

            // проверяем флаг активности
            if ( !isset($aShowValList[$iSectId]) or !(int)$aShowValList[$iSectId] )
                continue;

            // добавляем изображение, если есть
            $aSect['img'] = isset($aImgList[$iSectId]) ? $aImgList[$iSectId] : '';

            $aSect['href'] = ($aSect['link']) ? $aSect['link'] : '['.$iSectId.']';

            $aOutList[] = $aSect;
        }

        $this->setData( 'list', $aOutList );

        $this->setTemplate( 'gallery.twig' );

        return psComplete;
    }
}