<?php

namespace skewer\build\Page\Menu;

use skewer\build\Component\Section\Parameters;
use skewer\build\Component\Section\Tree;
use skewer\build\Component\Section;
use yii\helpers\ArrayHelper;


class Module extends \skModule {

    public $templateFile = 'leftMenu.twig';
    public $parentSection = 3;
    public $customSections = '';
    public $openAll = '';

    private static $icons = [];

	
	public function execute() {

        $to = $this->getEnvParam('sectionId');
        $iMainSection = \Yii::$app->sections->main();
        $from = $this->parentSection;

        $mode = 'normal';
        if($this->openAll==1) $mode = 'openAll';
        if($this->openAll==2) $mode = 'openSecond';
        if($this->customSections) $mode = 'custom';

        $items = array();

        switch($mode){
            case 'normal':
                $items = Tree::getUserSectionTree( $from, $to, 1 );
                break;
            case 'openAll':
                $items = Tree::getUserSectionTree( $from, $to );
                break;
            case 'openSecond':
                $items = Tree::getUserSectionTree( $from, $to, 2 );
                break;

            case 'custom':

                $list = explode( ',', $this->customSections );
                $sections = Tree::getCachedSection();
                $items = [];
                foreach ( $list as $id )
                    if ( isSet( $sections[$id] ) ) {
                        $section = $sections[$id];
                        $section['show'] = in_array($section['visible'], Section\Visible::$aShowInMenu);
                        $section['href'] = $section['link'] ?: '['.$id.']';
                        $section['selected'] = ( $id == $to );
                        $items[] = $section;
                    }

                break;

        } // switch

        $this->setData('items', $items);

        if ( \SysVar::get('Menu.ShowIcons'))
            $this->setData('icon', self::getIcons());

        if ( $iMainSection != $to ) $this->setData('hideMenu', 1);
        $this->setTemplate($this->templateFile);

        return psComplete;
    }


    /**
     * Иконки для разделов
     * @return array
     */
    private static function getIcons(){

        if (!self::$icons){
            self::$icons = Parameters::getList()
                ->asArray()
                ->fields(['value', 'parent'])
                ->group(Parameters::settings)
                ->name('category_icon')
                ->get();

            self::$icons = ArrayHelper::map(self::$icons, 'parent', 'value');

            self::$icons = array_filter(self::$icons, function( $s ){
               return (bool)$s;
            });
        }

        return self::$icons;

    }

}
