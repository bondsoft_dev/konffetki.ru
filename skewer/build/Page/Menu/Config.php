<?php

/* main */
$aConfig['name']     = 'Menu';
$aConfig['title']    = 'Меню';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Модуль меню';
$aConfig['revision'] = '0002';
$aConfig['layer']     = \Layer::PAGE;
$aConfig['languageCategory'] = 'page';

return $aConfig;
