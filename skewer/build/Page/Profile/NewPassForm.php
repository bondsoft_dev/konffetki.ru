<?php

namespace skewer\build\Page\Profile;


use skewer\build\Adm\Auth\ar\Users;
use skewer\build\Adm\Auth\ar\UsersRow;
use skewer\build\Component\orm;

class NewPassForm extends orm\FormRecord {

    public $oldpass = '';
    public $pass = '';
    public $wpass = '';
    public $cmd = 'Settings';

    public function rules() {
        return array(
            array( array('oldpass','pass','wpass'), 'required', 'msg' => \Yii::t('auth', 'err_empty_field' ) ),
            array( array('pass', 'wpass'), 'minlength', 'length' => 6, 'msg'=>\Yii::t('auth', 'err_short_pass' ) ),
            array( array('wpass'), 'compare', 'compareField'=>'pass', 'msg'=>\Yii::t('auth', 'err_pass_not_mutch' ) ),
            array( array('oldpass'), 'check', 'method'=>'checkOldPass', 'msg' => \Yii::t('auth', 'incorrect_old_pass' ) ),
        );
    }

    public function getLabels() {
        return array(
            'oldpass' =>\Yii::t('auth', 'old_pass' ),
            'pass' => \Yii::t('auth', 'new_pass' ),
            'wpass' => \Yii::t('auth', 'wpassword' ),
        );
    }

    public function getEditors() {
        return array(
            'cmd' => 'hidden',
        );
    }


    public function checkOldPass() {

        if ( !\CurrentUser::isLoggedIn() || \CurrentUser::getPolicyId() == \Auth::getDefaultGroupId() )
            return false;

        /** @var UsersRow $oUser */
        $oUser = Users::find( \CurrentUser::getId() );

        return $oUser->pass == \Auth::buildPassword( $oUser->login, $this->oldpass );
    }

} 