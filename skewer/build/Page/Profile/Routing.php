<?php

namespace skewer\build\Page\Profile;

use skewer\build\Component\Router\RoutingInterface;


class Routing implements RoutingInterface {
    /**
     * Возвращает паттерны разбора URL
     * @static
     * @return bool | array
     */
    public static function getRoutePatterns() {

        return array(
            '/cmd/',
            '/!response/'
        );
    }// func
}
