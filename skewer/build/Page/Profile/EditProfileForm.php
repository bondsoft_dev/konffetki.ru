<?php

namespace skewer\build\Page\Profile;


use skewer\build\Component\orm;
use skewer\build\Adm\Auth\ar\UsersRow;
use skewer\build\Adm\Auth\ar\Users;

class EditProfileForm extends orm\FormRecord {

    public $fio = '';
    public $postcode = '';
    public $address = '';
    public $contact_phone = '';

    public $cmd = 'info';

    public function __construct( $id = null ) {

        if ( !is_null( $id ) ) {

            /** @var UsersRow $oUser */
            $oUser = Users::find( \CurrentUser::getId() );

            $this->fio = $oUser->name;
            $this->postcode = $oUser->postcode;
            $this->address = $oUser->address;
            $this->contact_phone = $oUser->phone;
        }

    }

    public function rules() {
        return array(
            //array( array('fio','address','contact_phone'), 'required', 'msg'=>\Yii::t('auth', 'err_empty_field' ) ),
        );
    }

    public function getLabels() {
        return array(
            'login' => \Yii::t('auth', 'login_mail' ),
            'fio' => \Yii::t('auth', 'fio' ),
            'postcode' => \Yii::t('auth', 'postcode' ),
            'address' => \Yii::t('auth', 'address' ),
            'contact_phone' => \Yii::t('auth', 'contact_phone' ),
        );
    }

    public function getEditors() {
        return array(
            'cmd' => 'hidden',
        );
    }

    public function save() {

        if ( $this->isValid() ) {

            /** @var UsersRow $oUser */
            $oUser = Users::find( \CurrentUser::getId() );
            $oUser->name = $this->fio;
            $oUser->postcode = $this->postcode;
            $oUser->address = $this->address;
            $oUser->phone = $this->contact_phone;

            return $oUser->save();
        }

        return false;
    }

} 