<?php

namespace skewer\build\Page\Profile;


use skewer\build\Component\I18N\Languages;
use skewer\build\Component\Section\Tree;
use skewer\build\Component\Section\Visible;
use skewer\build\Component\Site;
use yii\helpers\ArrayHelper;

class Install extends \skModuleInstall {

    public function init() {
        return true;
    }// func

    public function install() {

        $this->createProfileSections();

        return true;
    }// func

    public function uninstall() {
        return true;
    }

    protected function createProfileSections()
    {
        $iNewPageSection = \Yii::$app->sections->tplNew();
        foreach (\Yii::$app->sections->getValues('tools') as $sLang => $iSection) {

            $oSection = Tree::addSection($iSection, \Yii::t('data/auth', 'profile_sections_title', [], $sLang), $iNewPageSection, 'profile', Visible::HIDDEN_FROM_MENU);
            $this->setParameter( $oSection->id, 'object', 'content', 'Profile' );
            $this->setParameter( $oSection->id, 'hide_right_column', '.', '1' );

            \Yii::$app->sections->setSection('profile', \Yii::t('site', 'profile', [], $sLang), $oSection->id, $sLang);

        }

    }// func

}// class
