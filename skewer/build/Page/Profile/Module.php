<?php

namespace skewer\build\Page\Profile;

use skewer\build\Adm\Auth\ar\Users;
use skewer\build\Adm\Auth\ar\UsersRow;
use skewer\build\Adm\Order\ar;
use skewer\build\Adm\Order\model\ChangeStatus;
use skewer\build\Adm\Order\model\Status;
use skewer\build\Component\Catalog\GoodsSelector;
use skewer\build\Component\I18N\ModulesParams;
use skewer\build\Page\Auth\Api;
use skewer\build\Tool\Payments as Payments;
use yii\web\NotFoundHttpException;


class Module extends \skModule {

    public $prm = 0;
    private $aOrders = array();
    private $aGoods = array();

    private $aStatusList = array();
    private $newStatus = null;

    /**
     * Метод - исполнитель функционала
     */
    public function execute() {

        $this->setData('profile_url',  Api::getProfilePath());
        $this->setData('auth_url',  Api::getAuthPath());

        /**
         * отключаем счетчики в личном кабинете
         * если надо будет где еще, вынести в отдельный метод
         */
        $this->oContext->getParentProcess()->setData('counters','');
        $this->oContext->getParentProcess()->setData('countersCode','');

        $this->aStatusList = Status::getListTitle();

        $sCmd = $this->getStr('cmd', 'init');

        $this->setData('page', $this->getEnvParam('sectionId'));

        $sActionName = 'action' . ucfirst($sCmd);

        if (method_exists($this, $sActionName))
            $this->$sActionName();
        else
            throw new NotFoundHttpException();

        return psComplete;
    }

    /**
     * Список заказов у пользователя
     * @param int $iUserId id User
     * @param int $iOrderId id заказа, если нет взять все товары пользователя
     * @param string $sToken токен
     * @return array
     */
    private function getOrders($iUserId,$iOrderId = 0,$sToken = '') {

        if ($sToken){
            $this->aOrders = ar\Order::find()->where('token', $sToken)->asArray()->getAll();
        } else
        if ($iOrderId){
            $this->aOrders = ar\Order::find()->where('auth', $iUserId)->where('id',$iOrderId)->order('id','DESC')->asArray()->getAll();
        } else
            $this->aOrders = ar\Order::find()->where('auth', $iUserId)->order('id','DESC')->asArray()->getAll();

        $aClay = array();

        foreach ($this->aOrders as $k=>$item) {
            $this->aOrders[$k]['text_status'] = $this->getStatusText($item['status']);
            $aClay[] = $item['id'];
        }

        $aGoods = array();
        if (!empty($aClay)){
            $aGoods = ar\Goods::find()->where('id_order', $aClay)->asArray()->getAll();
        }

        // переберем все товары и посчитаем сумму
        foreach ($aGoods as $item) {
            // проверим, есть ли связь с каталогом
            if ($item['id_goods']){
                $oCatalogObject = GoodsSelector::get($item['id_goods'],1);//fixme #GET_CATALOG_GOODS
                $item['object'] = $oCatalogObject;
            }

            $this->aGoods[$item['id_order']]['goods'][] = $item;
            if (!isset($this->aGoods[$item['id_order']]['total'])) $this->aGoods[$item['id_order']]['total'] = 0;
            $this->aGoods[$item['id_order']]['total'] += $item['total'];
        }

        foreach ($this->aOrders as $k=>$item){
            $total = $this->aGoods[$item['id']]['total'];
            $this->aOrders[$k]['totalDelivPrice'] = (int)$total+$item['deliv_cost'];

        }

        $aPayments = array();
        // установим кнопку оплаты
        foreach ($this->aOrders as $k=>$item) {
            if ($item['status'] == $this->getNewStatusId()) {

                /**
                 * @var $oPaymentType ar\TypePaymentRow
                 */
                $oPaymentType = ar\TypePayment::find( $item['type_payment'] );

                if ($oPaymentType && $oPaymentType->payment){
                    /**
                     * @var Payments\Payment $oPayment
                     */
                    if (!isset($aPayments[$oPaymentType->payment]) || !$aPayments[$oPaymentType->payment])
                        $oPayment = Payments\Api::make($oPaymentType->payment);
                    else
                        $oPayment = $aPayments[$oPaymentType->payment];
                    if ($oPayment){
                        $oPayment->setOrderId($item['id']);
                        $oPayment->setSum($this->aOrders[$k]['totalDelivPrice']/*$this->aGoods[$item['id']]['total']*/);

                        $this->aOrders[$k]['paymentForm'] = $oPayment->getForm();
                    }
                }

            }
        }
    }

    /**
     * Форма инфы по юзеру
     */
    public function actionInfo() {

        if (\CurrentUser::isLoggedIn() && \CurrentUser::getPolicyId() != \Auth::getDefaultGroupId()) {

            $oForm = new EditProfileForm( \CurrentUser::getId() );

            if ( $oForm->load( $this->getPost() ) && $oForm->save() ) {
                $this->setData( 'msg', \Yii::t('auth', 'msg_save') );
            }

            $this->setData( 'form', $oForm->getForm( 'EditProfileForm.twig', __DIR__ ) );
            $this->setData('cmd', 'info');
            $this->setTemplate('info.twig');

        } else
            $this->setTemplate('no_auth.twig');
    }

    /**
     * Форма смены пароля
     */
    public function actionSettings() {

        if ( \CurrentUser::isLoggedIn() && \CurrentUser::getPolicyId() != \Auth::getDefaultGroupId() ) {

            $oNewPassForm = new NewPassForm();

            $oNewPassForm->oldpass = $this->getStr('oldpass');
            $oNewPassForm->pass = $this->getStr('pass');
            $oNewPassForm->wpass = $this->getStr('wpass');

            if ( $oNewPassForm->load( $this->getPost() ) && $oNewPassForm->isValid() ) {

                /** @var UsersRow $oItem */
                $oItem = Users::find( \CurrentUser::getId() );
                $oItem->pass = \Auth::buildPassword( $oItem->login, $oNewPassForm->pass );

                if ( $oItem->save() ) {

                    $this->setData( 'msg', \Yii::t('auth', 'msg_pass_save') );

                    \Mailer::sendMail( $oItem->email, ModulesParams::getByName('auth', 'mail_title_new_pass'),
                        ModulesParams::getByName('auth', 'mail_new_pass')
                    );

                    $this->setTemplate('settings.twig');
                    return;

                } else
                    $this->setData('msg', \Yii::t('auth', 'msg_pass_no_save'));
            }

            $iSectionId = $this->getEnvParam('sectionId');

            $this->setData('cmd', 'settings');
            $this->setData('url', \Yii::$app->router->rewriteURL( "[$iSectionId][Auth?cmd=settings]" ) );
            $this->setData( 'forms', $oNewPassForm->getForm('new_pass.twig', __DIR__) );
            $this->setTemplate('settings.twig');

        } else
            $this->setTemplate('no_auth.twig');
    }


    /**
     * Форма по дефолту заказа
     */
    public function actionInit() {
        // проверить авторизацию
        if (\CurrentUser::isLoggedIn() && \CurrentUser::getPolicyId() != \Auth::getDefaultGroupId()) {
            $this->setData('current_user', \CurrentUser::getUserData());
            $this->getOrders(\CurrentUser::getId());
            $this->setData('orders', $this->aOrders);
            $this->setData('goods', $this->aGoods);
            $this->setData('cmd', 'order');
            $this->setTemplate('view.twig');
        } else {
            $this->setTemplate('no_auth.twig');
        }
    }

    public function actionDetail(){
        $iOrderId = $this->getInt('id');
        $sToken = $this->getStr('token','');

        $this->getOrders(\CurrentUser::getId(),$iOrderId,$sToken);

        if (!isset($this->aOrders[0]))
            throw new NotFoundHttpException();

        $aOrders = $this->aOrders[0];
        $aOrders['type_payment_text'] = ar\TypePayment::getValue($aOrders['type_payment']);
        $aOrders['type_delivery_text'] = ar\TypeDelivery::getValue($aOrders['type_delivery']);

        if (!isset($this->aGoods[$aOrders['id']]))
            throw new NotFoundHttpException();

        $aGoods = $this->aGoods[$aOrders['id']];

        if ($sToken) $this->setData('token', $sToken);

        $aHistoryList = ChangeStatus::find()->where(['id_order' => $aOrders['id']])->asArray()->all();
        if ($aHistoryList){
            foreach($aHistoryList as $k=>$item){
                $aHistoryList[$k]['title_old_status'] = $this->getStatusText($item['id_old_status']);
                $aHistoryList[$k]['title_new_status'] = $this->getStatusText($item['id_new_status']);
            }
            $this->setData('historyList',$aHistoryList);
        }

        $this->setData('order', $aOrders);
        //var_dump($aOrders);
        $this->setData('items', $aGoods);

        $this->setTemplate('detail.twig');
    }

    /**
     * Отдает название статуса по id
     * @param $mId
     * @return string
     */
    private function getStatusText( $mId ) {
        if ( isset($this->aStatusList[$mId]) )
            return $this->aStatusList[$mId];
        else
            return '---';
    }

    /**
     * Ид статуса нового заказа
     * @return int
     */
    private function getNewStatusId()
    {
        if (is_null($this->newStatus))
            $this->newStatus = Status::getIdByNew();

        return $this->newStatus;
    }

}