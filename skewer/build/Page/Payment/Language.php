<?php

$aLanguage = array();

$aLanguage['ru']['tab_name'] = 'Системы оплат';
$aLanguage['ru']['success_text'] = 'Заказа №{0, number} успешно оплачен';
$aLanguage['ru']['fail_text'] = 'Заказ №{0, number}. Вы отказались от оплаты';

$aLanguage['en']['tab_name'] = 'Payment';
$aLanguage['en']['success_text'] = 'Order #{0, number} paid successfully';
$aLanguage['en']['fail_text'] = 'Order #{0, number}. You refused to pay';

return $aLanguage;