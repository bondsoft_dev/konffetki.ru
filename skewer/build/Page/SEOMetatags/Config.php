<?php

/* main */
$aConfig['name']     = 'SEOMetatags';
$aConfig['title']     = 'СЕО метатеги';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'СЕО метатеги';
$aConfig['revision'] = '0002';
$aConfig['useNamespace'] = true;
$aConfig['layer']     = \Layer::PAGE;
$aConfig['languageCategory']     = 'SEO';

return $aConfig;
