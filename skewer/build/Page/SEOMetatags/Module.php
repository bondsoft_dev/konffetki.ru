<?php

namespace skewer\build\Page\SEOMetatags;

use skewer\build\Component\SEO;
use skewer\build\Component\Site;


/**
 * Модуль вывода метотегов для страницы
 * Class Module
 * @package skewer\build\Page\SEOMetatags
 */
class Module extends \skModule {


    /**
     * Список шаблонов
     * @return array
     */
    private function getTplList() {

        $aOut = array();

        if ( Site\Type::hasCatalogModule() ) {

            $aOut[] = SEO\Api::TPL_CATALOG_COLLECTION_ELEMENT;
            $aOut[] = SEO\Api::TPL_CATALOG_COLLECTION;

            $aOut[] = SEO\Api::TPL_CATALOG2LAYER;

            $aCatalogTpl = SEO\Template::find()
                ->where( 'sid LIKE ?', SEO\Api::TPL_CATALOG . '%' )
                ->order( 'sid' )
                ->getAll();

            foreach ( $aCatalogTpl as $sTpl )
                $aOut[] = $sTpl->sid;
        }

        // todo нужно както их брать из базы
        $aOut[] = SEO\Api::TPL_GALLERY;
        $aOut[] = SEO\Api::TPL_NEWS;
        $aOut[] = SEO\Api::TPL_FAQ;
        $aOut[] = SEO\Api::TPL_ARTICLES;

        return $aOut;
    }


    private static function getFieldList() {
        return array(
            'title' => 'SEOTitle',
            'description' => 'SEODescription',
            'keywords' => 'SEOKeywords',
            'none_index' => 'SEONonIndex',
            'add_meta' => 'SEOAddMeta',
        );
    }


    /**
     * Выполнение модуля
     * @return int
     */
    public function execute() {

        // ждем выполнения всех основных контентных модулей
        $page = Site\Page::getRootModule();
        if ( !$page->isComplete() )
            return psWait;

        // собираем информацию от контентных модулей
        $SEOData = $this->getEnvParam( 'SEOTemplates', array() );
        $SEOVars = $this->getEnvParam( 'SEOVars', array() );

        // выбор приоритетного шаблона для парсинга
        $aTpls = array_keys( $SEOData );
        $sTpl = 'text';

        foreach( $this->getTplList() as $sCurTpl )
            if ( in_array( $sCurTpl, $aTpls ) )
                $sTpl = $sCurTpl;

        // парсинг параметров
        if ( isset( $SEOData[$sTpl] ) and $SEOData[$sTpl] ) {

            $oMainTpl = SEO\Template::getByName( 'text' ); //@todo text ?
            $oTpl = SEO\Template::getByName( $sTpl );

            foreach ( static::getFieldList() as $sField => $sLabel ) {
                if ( isset( $SEOData[$sTpl][$sField] ) && $SEOData[$sTpl][$sField] ) {
                    $page->setData( $sLabel, $SEOData[$sTpl][$sField] );
                } elseif( $oTpl && isSet($oTpl->$sField) && $oTpl->$sField ) {
                    $page->setData( $sLabel, $oTpl->parseTpl( $sField, $SEOVars ) );
                } elseif( $oMainTpl && isSet($oMainTpl->$sField) && $oMainTpl->$sField ) {
                    $page->setData( $sLabel, $oMainTpl->parseTpl( $sField, $SEOVars ) );
                }

            }

        }

        return psRendered;
    }


}
