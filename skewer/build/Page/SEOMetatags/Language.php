<?php

$aLanguage = array();

$aLanguage['ru']['SEOMetatags.Page.tab_name'] = 'SEO метатеги';
$aLanguage['ru']['group_title'] = 'SEO данные';
$aLanguage['ru']['frequency'] = 'Частота обновления';
$aLanguage['ru']['priority'] = 'Приоритет';
$aLanguage['ru']['meta_title'] = 'Название страницы';
$aLanguage['ru']['meta_keywords'] = 'Ключевые слова';
$aLanguage['ru']['meta_description'] = 'Описание страницы';
$aLanguage['ru']['data_group'] = 'Группа данных';
$aLanguage['ru']['row_id'] = 'Id целевой записи';
$aLanguage['ru']['not_defined'] = '-Не задано-';
$aLanguage['ru']['never'] = 'Никогда';
$aLanguage['ru']['daily'] = 'Каждый день';
$aLanguage['ru']['weekly'] = 'Каждую неделю';
$aLanguage['ru']['always'] = 'Всегда';
$aLanguage['ru']['none_index'] = 'Отключить индексирование страницы';
$aLanguage['ru']['add_meta'] = 'Верификация и служебные метатеги';

$aLanguage['en']['SEOMetatags.Page.tab_name'] = 'SEO metatags';
$aLanguage['en']['group_title'] = 'SEO data';
$aLanguage['en']['frequency'] = 'Frequency';
$aLanguage['en']['priority'] = 'Priority';
$aLanguage['en']['meta_title'] = 'Page title';
$aLanguage['en']['meta_keywords'] = 'Keywords';
$aLanguage['en']['meta_description'] = 'Page description';
$aLanguage['en']['data_group'] = 'Group data';
$aLanguage['en']['row_id'] = 'Id target entry';
$aLanguage['en']['not_defined'] = '-Not defined-';
$aLanguage['en']['never'] = 'Never';
$aLanguage['en']['daily'] = 'Daily';
$aLanguage['en']['weekly'] = 'Weekly';
$aLanguage['en']['always'] = 'Always';
$aLanguage['en']['none_index'] = 'Disable Indexing page';
$aLanguage['en']['add_meta'] = 'Verification and service metatags';

return $aLanguage;