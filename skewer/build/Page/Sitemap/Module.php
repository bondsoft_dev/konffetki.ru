<?php

namespace skewer\build\Page\Sitemap;

use skewer\build\Component\Section\Tree;


/**
 * @todo модуль надо рефакторить. Нерациональная рекеурсивная выборка + в шаблоне только 3 уровня
 * Class Module
 * @package skewer\build\Page\Sitemap
 */
class Module extends \skModule {
    /**
     * Список id разделов, от которых строить дерево разделов
     * @var string|array
     */
    var $rootSections = '';

    /**
     * Фильтр выборки разделов
     * @var array
     */
    protected $aDenySections = array();


    /**
     * Выполнение модуля
     * @return int
     */
    public function execute() {

        $this->rootSections = explode(',',$this->rootSections);

        $this->rootSections[] = \Yii::$app->sections->topMenu();
        $this->rootSections[] = \Yii::$app->sections->leftMenu();

        if(!count($this->rootSections)) return psRendered;

        $iMainSection = \Yii::$app->sections->main();

        $aItems[$iMainSection] = Tree::getSection( $iMainSection, true );
        $aItems[$iMainSection]['href'] = $aItems[$iMainSection]['link'] ?: '['.$aItems[$iMainSection]['id'].']';

        /* Если есть запрещенные политикой разделы - исключаем из выборки */
        $this->aDenySections = \Auth::getDenySections('public') ?: [];

        foreach($this->rootSections as $iSectionId) {

            $aSubItems = Tree::getSubSections( $iSectionId );// $this->oTree->getSubItems($iSectionId, $this->aDenySections);

            if ( !$aSubItems ) continue;

            foreach ( $aSubItems as $section ) {

                if ( in_array( $section->id, $this->aDenySections ) )
                    continue;

                if ( $aRow = $this->makeSiteMap( $section->id ) )
                    if( $section->id != $iMainSection )
                        $aItems[] = $aRow;

            }

        }

        $this->setData('items', $aItems);

        $this->setTemplate('view.twig');
        return psComplete;
    }// func

    /**
     * Возвращает дерево(массив) разделов
     * @param $iRootSection
     * @return array|bool
     * todo ref
     */
    protected function makeSiteMap($iRootSection) {

        $aItems = Tree::getSection( $iRootSection, true );

        if ( $aItems['visible'] != 1 ) return false;

        $aItems['href'] = $aItems['link'] ?: '['.$aItems['id'].']';

        $aSubItems = Tree::getSubSections( $iRootSection );

        if ( $aSubItems )
            foreach( $aSubItems as $section ) {

                if ( in_array( $section->id, $this->aDenySections ) )
                    continue;

                if ( $aRows = $this->makeSiteMap( $section->id ) )
                    $aItems['items'][] = $aRows;

            }

        return $aItems;
    }// func

}// class
