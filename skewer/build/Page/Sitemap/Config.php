<?php

/* main */
$aConfig['name']     = 'Sitemap';
$aConfig['title']     = 'Карта сайта';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Карта сайта';
$aConfig['revision'] = '0002';
$aConfig['layer']     = \Layer::PAGE;

return $aConfig;
