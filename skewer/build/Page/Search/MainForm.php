<?php

namespace skewer\build\Page\Search;


use skewer\build\Component\Forms\Field as FormFields;
use skewer\build\Component\Import\Field\Section;
use skewer\build\Component\orm;
use skewer\build\Component\Search;
use skewer\build\Component\Section\Tree;
use \skewer\build\Component\Site;

class MainForm extends orm\FormRecord {

    public $search_text = '';
    public $search_type = -1;
    public $search_section = '';

    public function __construct(){
        $this->search_type = (int)\SysVar::get('Search.default_type');
    }

    public function rules() {
        return array(
            array( array('search_text'), 'required', 'msg'=>\Yii::t('forms', 'err_empty_field' ) ),
        );
    }

    public function getLabels() {
        return array(
            'search_text' => \Yii::t( 'search', 'phrase' ),
            'search_type' => \Yii::t( 'search', 'criteria' ),
            'search_section' => \Yii::t( 'search', 'search_section' ),
        );
    }


    public function getEditors() {
        return array(
            'search_type' => array( 'select', 'method'=>'getTypeList' ),
            'search_section' => array( 'select', 'method'=>'getSectionList' ),
        );
    }


    public function getTypeList() {
        return array(
            Search\Type::allWords => \Yii::t('search', 'all_words'),
            Search\Type::anyWord => \Yii::t('search', 'any_words'),
            Search\Type::exact => \Yii::t('search', 'phrase_criteria'),
        );
    }


    public function getSectionList() {

        $iPolicyId = \Auth::getPolicyId( 'public' );
        $aSections = array( \Yii::t('search', 'all_site') );

        foreach ( Tree::getSectionList( \Yii::$app->sections->topMenu(), $iPolicyId ) as $aSection ){
            if (isset($aSection['visible']) && $aSection['visible'] > 0){
                $aSections[$aSection['id']] = $aSection['title'];
            }
        }

        foreach ( Tree::getSectionList( \Yii::$app->sections->leftMenu(), $iPolicyId ) as $aSection ){
            if (isset($aSection['visible']) && $aSection['visible'] > 0){
                $aSections[$aSection['id']] = $aSection['title'];
            }
        }

        return $aSections;
    }

}