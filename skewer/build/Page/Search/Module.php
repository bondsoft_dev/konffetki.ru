<?php

namespace skewer\build\Page\Search;


use skewer\build\Component\Search\Type;


/**
 * Модуль контекстного поиска по сайту
 * Class Module
 * @package skewer\build\Page\Search
 */
class Module extends \skModule {

    public $iCurrentSection;
    public $onPage = 10;

    /** @var string Тип шаблона мини-формы */
    public $miniFormTemplate = 'mini_form.twig';

    /** @var string Тип шаблона для поиска по каталогу */
    public $sCatalogListTemplate = 'list';

    /** @var bool Выводить список типов */
    public $showTypeSelect = false;

    /** @var bool Выводить список разделов */
    public $showSectionSelect = false;

    /** @var int Тип поиска по области */
    public $search_type = 0;

    /** @var int Тип поиска */
    public $type = 1;

    /** @var int Раздел */
    public $search_section = 0;

    /** @var bool Поиск по подразделам */
    public $bSubsection = true;

    /** @var int Форма заказа товаров */
    public $form_section = 0;

    public function init(){

        $this->type = $this->getInt( 'search_type', -1 );
        if ($this->type < 0){
            $this->type = (int)\SysVar::get('Search.default_type');
        }

        $this->iCurrentSection = $this->getEnvParam('sectionId');

    }

    public function execute() {

        if ( $this->iCurrentSection == \Yii::$app->sections->getValue('search') ) {

            $this->showForm();

            switch ($this->search_type){
                case Type::inAll:
                case Type::inInfo:
                    default:
                    $oSelector = new Selector\InfoSelector( $this );
                    break;

                case Type::inCatalog:
                    $oSelector = new Selector\CatalogSelector( $this );
                    break;
            }

            $oSelector->execute();

            return psComplete;

        } else {
            // mini bar
            $this->setTemplate($this->miniFormTemplate);
            $this->setData('search_section', \Yii::$app->sections->getValue('search'));

        }

        return psComplete;
    }

    /**
     * Вывод формы поиска
     */
    private function showForm(){
        $oSearchForm = new MainForm();

        $oSearchForm->leftMenuId = \Yii::$app->sections->leftMenu();
        $oSearchForm->topMenuId = \Yii::$app->sections->topMenu();

        $oSearchForm->search_text = $this->getStr( 'search_text', '' );
        $oSearchForm->search_type = $this->getStr( 'search_type', $oSearchForm->search_type );
        $oSearchForm->search_section = $this->getStr( 'search_section', '' );

        $this->search_section = $oSearchForm->search_section;

        $this->setData( 'form', $oSearchForm->getForm( 'form.twig', __DIR__ ,
            array('search_section' => $this->getEnvParam('sectionId'), 'showTypeSelect' => $this->showTypeSelect, 'showSectionSelect' => $this->showSectionSelect)
        ) );
        $this->setData( 'domain', \skewer\build\Tool\Domains\Api::getCurrentDomain() );
    }


    public function shutdown(){

    }

}//class