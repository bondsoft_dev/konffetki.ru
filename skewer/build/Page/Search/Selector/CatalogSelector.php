<?php

namespace skewer\build\Page\Search\Selector;


use skewer\build\Component\Catalog\Card;
use skewer\build\Component\Catalog\GoodsSelector;
use skewer\build\Component\Search\Type;
use skewer\build\Component\Site;
use skewer\build\Component\Search\Selector;
use skewer\build\libs\ft;
use skewer\build\Component\Cache;

/**
 * Выборка из каталога
 * Class CatalogSelector
 * @package skewer\build\Page\Search\Selector
 */
class CatalogSelector extends SelectorPrototype{

    public function init(){
        $this->getModule()->setTemplate( 'CatalogResults.twig' );
    }

    public function select(){

        $iPage = $this->getInt('page', 1);
        $sSearchText = $this->getStr('search_text');

        $search_type = $this->getParam( 'type' );
        $search_section = $this->getParam( 'search_section' );
        $iOnPage = (int)$this->getParam( 'onPage', 10 );

        if ( !empty( $sSearchText ) ) {

            /* Если есть запрещенные политикой разделы - исключаем из выборки */
            $aDenySections = ( $res = \Auth::getDenySections('public') ) ? $res : array();

            /** Делаем выборку из поисковой таблицы */
            $aItems = Selector::create()
                ->searchText( $sSearchText )
                ->limit( $iOnPage, $iPage )
                ->type( Type::inCatalog )
                ->searchType( $search_type  )
                ->type( $this->getParam( 'search_type' ) )
                ->section( $search_section )
                ->denySection( $aDenySections )
                ->subsections( $this->getParam('bSubsection') )
                ->find();

            if ( is_array($aItems) && isSet($aItems['count']) && $aItems['count'] ) {

                /** Делаем выборка из каталога */
                $aObjects = \yii\helpers\BaseArrayHelper::map($aItems['items'], 'object_id', 'object_id');
                $aResult = $this->getGoods( $aObjects );
                if (!$aResult){
                    $this->setModuleData('not_found', 1);
                    return false;
                }

                $aResult['count'] = $aItems['count'];

                /** Отправляем данные в модуль */
                $this->setModuleData( 'result_count', $aItems['count'] );

                /**
                 * @todo Есть ли способ инклудить в твиг шаблоны не из папки другого модуля?
                 */
                $this->setModuleData( 'result',
                    \skParser::parseTwig(
                        $this->getTpl(),
                        array( 'aObjectList' => $aResult['items'], 'useMainSection' => true, 'useCart' => Site\Type::isShop(),
                            'form_section' => $this->getParam( 'form_section' ) //@todo получается, что у товаров в поиске - одинаковая форма заказа
                        ),
                        RELEASEPATH. 'build/Page/CatalogViewer/templates/'
                    )
                );

                $this->getModule()->getPageLine($iPage, $aItems['count'], $this->getParam( 'iCurrentSection' ),
                    array('search_text' => $sSearchText, 'search_type' => $search_type, 'search_section' => $search_section),
                    array('onPage' => $iOnPage));

            }
            else {
                $this->setModuleData('not_found', 1);
            }

        }

    }

    /**
     * Получениу товаров
     * @param array $aObjects
     * @return array|bool
     */
    private function getGoods( $aObjects = array() ){
        $aGoods = GoodsSelector::getList( Card::DEF_BASE_CARD )
            ->condition( 'id IN ?',$aObjects )
            // @todo Возможен случай, если товар уже неактивен, но в поиске еще присутствует. На странице будет меньше товаров!
            ->condition( 'active', 1 )
            ->parse();

        if (!$aGoods){
            $this->setModuleData('not_found', 1);
            return false;
        }
        $aGoods = \yii\helpers\BaseArrayHelper::index( $aGoods, 'id' );

        $aResult = array();
        /** Сортировка в соответствии с результатами поиска */
        foreach ( $aObjects as $iObjectId ){
            if ( isSet($aGoods[$iObjectId]) ) {
                $aResult['items'][] = $aGoods[$iObjectId];
                unset($aGoods[$iObjectId]);
            }
        }

        foreach ($aResult['items'] as $key=>$item){
            if ($item['fields']['measure']['value'] == \SysVar::get('dict_kgm')) {
                $price = $item['fields']['price']['value'];
                $price_new = ($price*1.0)/10;
                $aResult['items'][$key]['fields']['price']['value_full'] = $price_new;
                $aResult['items'][$key]['fields']['price']['value'] = $price_new;
                $aResult['items'][$key]['fields']['price']['html'] = $price_new .' '.$aResult['items'][$key]['fields']['price']['attrs']['measure'];
            }
        }

        return $aResult;
    }

    /**
     * Установка шаблона для вывода поиска по каталогу
     * @return string
     */
    private function getTpl() {

        $aTemplates = array(
            'list' => 'SimpleList.twig',
            'gallery' => 'GalleryList.twig',
            'table' => 'TableList.twig',
        );

        $sTpl = $this->getParam( 'sCatalogListTemplate' );

        // проверяем перекрытие из GET
        if ( $sView = $this->getStr('view') )
            $sTpl = $sView;

        // убеждаемся в наличии
        if ( !isSet( $aTemplates[$sTpl] ) )
            $sTpl = 'list';

        return  $aTemplates[$sTpl];
    }

}