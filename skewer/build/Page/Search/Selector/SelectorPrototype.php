<?php

namespace skewer\build\Page\Search\Selector;

/**
 * Прототип класса для выборки
 * Class SelectorPrototype
 * @package skewer\build\Page\Search\Selector
 */
abstract class SelectorPrototype{

    /**
     * @var \skewer\build\Page\Search\Module
     */
    private $oModule = null;

    public function __construct( \skModule $oModule ){
        $this->oModule = $oModule;
    }

    /**
     * Возвращает экземпряр модуля
     * @return \skModule
     */
    protected function getModule(){
        return $this->oModule;
    }

    /**
     * Инициализация
     */
    abstract function init();

    /**
     * Выборка
     */
    abstract function select();

    /**
     * Запуск выборки
     */
    final public function execute(){
        $this->init();
        $this->select();
    }

    /**
     * Получение параметра модуля
     * @param string $sParamName
     * @param string $mDefault
     * @return mixed|string
     */
    final protected function getParam( $sParamName = '', $mDefault = '' ){
        if (!$sParamName){
            return $mDefault;
        }

        try{
            return $this->oModule->$sParamName;
        }
        catch (\Exception $e){
            return $mDefault;
        }

    }

    /**
     * Получение параметра из get/post
     * @param $sParamName
     * @param string $sDefault
     * @return string
     */
    protected function getStr( $sParamName, $sDefault = '' ){
        return $this->oModule->getStr( $sParamName, $sDefault );
    }

    /**
     * Получение параметра из get/post
     * @param $sParamName
     * @param int $iDefault
     * @return int
     */
    protected function getInt( $sParamName, $iDefault = 0 ){
        return $this->oModule->getInt( $sParamName, $iDefault );
    }

    /**
     * Добавляет данные для вывода в шаблонизатор модуля
     * @final
     * @param string $sLabel Метка для вставки данных
     * @param mixed $mData Данные
     * @return bool
     */
    final protected function setModuleData( $sLabel, $mData ){
        $this->oModule->setData( $sLabel, $mData );
    }

}