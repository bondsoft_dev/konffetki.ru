<?php

namespace skewer\build\Page\Search\Selector;


use skewer\build\Component\Search\Api;
use skewer\build\Component\Search\Selector;

/**
 * Информационная выборка (поиск старого типа)
 * Class InfoSelector
 * @package skewer\build\Page\Search\Selector
 */
class InfoSelector extends SelectorPrototype{

    /**
     * Максимальна длина текста, оображаемого в результатах поиска
     * @var int
     */
    private static $iLength = 500;

    public function init(){
        $this->getModule()->setTemplate('results.twig');
    }

    public function select(){

        $iPage = $this->getInt('page', 1);
        $sSearchText = $this->getStr('search_text');

        $search_type = $this->getParam( 'type' );
        $search_section = $this->getParam( 'search_section' );
        $iOnPage = (int)$this->getParam( 'onPage', 10 );

        if ( !empty( $sSearchText ) ) {

            /* Если есть запрещенные политикой разделы - исключаем из выборки */
            $aDenySections = ( $res = \Auth::getDenySections('public') ) ? $res : array();

            /** Делаем выборку */
            $aItems = Selector::create()
                ->searchText( $sSearchText )
                ->limit( $iOnPage, $iPage )
                ->searchType( $search_type )
                ->type( $this->getParam( 'search_type' ) )
                ->section( $search_section )
                ->denySection( $aDenySections )
                ->subsections( $this->getParam('bSubsection') )
                ->find();

            if ( is_array($aItems) && isSet($aItems['count']) && $aItems['count'] ) {

                foreach($aItems['items'] as $iKey=>&$aItem){
                    $aItem['number'] = (int)$iKey+1+($iPage-1)*$iOnPage;
                    $this->parseItem( $aItem );
                }

                /** Тут еще был кусок для pda, выпилил, сейчас вроде бы это не используется */

                /** Отправляем данные в модуль */
                $this->setModuleData( 'result_count', $aItems['count'] );
                $this->setModuleData( 'aItems', $aItems['items'] );
                $this->getModule()->getPageLine($iPage, $aItems['count'], $this->getParam( 'iCurrentSection' ),
                    array('search_text' => $sSearchText, 'search_type' => $search_type, 'search_section' => $search_section),
                    array('onPage' => $iOnPage));

            }
            else {
                $this->setModuleData('not_found', 1);
            }

        }
    }

    private function parseItem( &$aItem = array() ){
        $aItem['module_title'] = static::getModuleTitle( $aItem['class_name'] );

        if ( isset($aItem['text']) && strlen($aItem['text']) > self::$iLength ){
            $aItem['text'] =  substr($aItem['text'], 0, self::$iLength);
            $aItem['text'] = substr($aItem['text'], 0, strrpos($aItem['text'], ' ' )).' ...';
        }
    }

    /**
     * Отдает имя класса по имени модуля
     * @param string $sClassName имя модуля
     * @return string
     */
    private static function getModuleTitle( $sClassName ) {
        $oEngine = Api::getSearch( $sClassName );
        return $oEngine ? $oEngine->getModuleTitle() : '-';
    }

}