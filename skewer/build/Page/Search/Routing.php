<?php

namespace skewer\build\Page\Search;

use skewer\build\Component\Router\RoutingInterface;


class Routing implements RoutingInterface {

    public static function getRoutePatterns() {

        return array(
            '/*page/page(int)/*date/date/',
            '/*date/date/',
            '/*page/page(int)/',
            '/!response/'
        );
    }

}