<?php

namespace skewer\build\Page\Title;

use skewer\build\Component\Section\Tree;
use skewer\build\Component\Site;

class Module extends \skModule{

    var $iSectionId;
    var $hideTitle;
    var $altTitle = '';

    public function init(){

        $this->iSectionId = $this->getEnvParam('sectionId');
        if ( $sImpTitle = $this->getEnvParam('title4section') )
            $this->altTitle = $sImpTitle;
        $this->setParser(parserTwig);
    }

    public function execute(){

        $oProcessContent = Site\Page::getMainModule();
        if ( $oProcessContent ) {

            if ( $oProcessContent->getStatus() == psNew )
                return psWait;

        }

        //Кастыль для установки заголовка H1 из нашего волшебного файла
        include( ROOTPATH . "seo_titles_inc.php");
        $url = $_SERVER["REQUEST_URI"];
        if($seoMetaData[$url]["H1"]){
            $this->setData('title', $seoMetaData[$url]["H1"]);
        }elseif(!empty($this->altTitle)){
            $this->setData('title', $this->altTitle);
        }else {
            $oSection = Tree::getSection( $this->iSectionId );
            $this->setData('title', $oSection ? $oSection->title : '');
        }
        
        $this->setData('hideTitle', $this->hideTitle);
        $this->setTemplate('view.twig');

        return psComplete;
    }

}//class