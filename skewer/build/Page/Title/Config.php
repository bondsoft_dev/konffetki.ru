<?php

/* main */
$aConfig['name']     = 'Title';
$aConfig['title']    = 'Модуль заголовка';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Модуль заголовка';
$aConfig['revision'] = '0002';
$aConfig['layer']     = \Layer::PAGE;
$aConfig['languageCategory']     = 'page';

return $aConfig;
