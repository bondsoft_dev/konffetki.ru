<?php

namespace skewer\build\Page\Subscribe;


use skewer\build\Component\orm;
use skewer\build\Page\Subscribe\ar\SubscribeUser;
use skewer\build\Page\Subscribe\ar\SubscribeUserRow;

class SubscribeForm  extends orm\FormRecord{

    public $person = '';
    public $email = '';
    public $city = '';
    public $cmd = 'subscribe';

    public function rules() {
        return array(
            array( array('email'), 'required', 'msg'=>\Yii::t('subscribe', 'required_fields' ) ),
            array( array('email'), 'check', 'method'=>'checkLogin', 'msg'=>\Yii::t('subscribe', 'duplicate_email' ) ),
            array( array('email'), 'check', 'method'=>'checkEmail', 'msg'=>\Yii::t('subscribe', 'no_valid' ) )

        );
    }

    public function getLabels() {
        return array(
            //'person' => \Yii::t('subscribe', 'person' ),
            'email' => \Yii::t('subscribe', 'email' ),
            //'city' => \Yii::t('subscribe', 'city' )
        );
    }

    public function getEditors() {
        return array(
            'cmd' => 'hidden'
        );
    }


    public function checkEmail(){
        if ( !filter_var( $this->email, FILTER_VALIDATE_EMAIL ) )
            return false;

        return true;
    }

    /**
     * проверка на совпадение email
     * @return bool
     */
    public function checkLogin() {
        $oItem =  SubscribeUser::find()->where( 'email', $this->email )->get();

        if ( $oItem ) {
            $this->setFieldError( 'email', \Yii::t('subscribe', 'duplicate_email') );
            return false;
        }
        return true;
    }

    public function save(){

        /**
         * @var SubscribeUserRow $oRow
         */
        $oRow = SubscribeUser::getNewRow();

        $oRow->city = $this->city;
        $oRow->person = $this->person;
        $oRow->email = $this->email;

        return $oRow->save();



    }

} 