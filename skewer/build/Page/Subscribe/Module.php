<?php

namespace skewer\build\Page\Subscribe;


class Module extends \skModule {

    var $AnswersTemplate = 'answers.twig';
    var $sTpl = 'form.twig';
    var $sMiniTpl = 'mini.twig';
    var $iSectionId = 0;

    var $mini = 0;
    var $enable = 0;

    public function execute() {

        $this->iSectionId = $this->getEnvParam('sectionId');

        $sCmd = $this->getStr( 'cmd' );

        $sActionName = 'action' . ucfirst( $sCmd );

        if ( method_exists( $this, $sActionName ) )
            $this->$sActionName();
        else
            $this->actionInit();

        return psComplete;
    }

    public function actionInit(){

        $this->showForm();
    }

    /*
    public function showMiniForm(){
        $this->setTemplate( 'mini.twig' );

        $this->setData( 'forms', $subscribeForm->getForm( $this->sTpl, __DIR__, $aParams ) );
    }
    */

    public function showForm(){

        $this->setTemplate( 'detail.twig' );

        $subscribeForm = new  SubscribeForm();

        if ( $subscribeForm->load( $this->getPost() ) && $subscribeForm->isValid() && $subscribeForm->save()) {
            $this->setData( 'msg', \Yii::t( 'subscribe', 'success' ) );
        } else {
            $aParams = array(
                'page' => $this->iSectionId,
                'url' => \Yii::$app->router->rewriteURL( "[$this->iSectionId]" )
            ) ;

            if ($this->mini){

                $idSubscribe = \Yii::$app->sections->getValue('subscribe');

                $aParams['url'] = \Yii::$app->router->rewriteURL( "[$idSubscribe]" );

                $this->setData( 'forms', $subscribeForm->getForm( 'miniForm.twig', __DIR__, $aParams ) );
            } else {
                $this->setData( 'forms', $subscribeForm->getForm( $this->sTpl, __DIR__, $aParams ) );
            }
        }
    }

    public function actionUnsubscribe(){

        $this->setTemplate($this->AnswersTemplate);

        $sEmail = $this->getStr('email');
        $sToken = $this->getStr('token');

        $bEmail = Api::checkEmail($sEmail);

        if ( md5('unsub'.$sEmail.'010') != $sToken || !$bEmail ){
            $this->setData('subscriber_not_delete', 1);
        }else{
            $this->setData('subscriber_delete', Api::delSubscriber($sEmail));
        }
    }

    public function actionUnsubscribe_ajax(){
        $sEmail = $this->getStr('email');

        $bEmail = Api::checkEmail($sEmail);

        if ( !$bEmail )
            $this->setData('out', 0);
        else
            $this->setData('out', Api::delSubscriber($sEmail));

        return psRendered;
    }

    public function shutdown(){

    }

} //class