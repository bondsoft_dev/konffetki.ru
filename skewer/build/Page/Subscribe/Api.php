<?php

namespace skewer\build\Page\Subscribe;


use skewer\build\Page\Subscribe\ar\SubscribeUser;
use skewer\build\Page\Subscribe\ar\SubscribeUserRow;

class Api {

    /**
     * @static Метод для проверки наличия адреса e-mail в базе
     * @param $sEmail
     * @return int
     */
    public static function checkEmail($sEmail){

        /** @var SubscribeUserRow $res */
        $res = SubscribeUser::find()->where('email',$sEmail)->getOne();
        if (!$res) return 0;

        return $res->id;
    }

    /**
     * @static Метод удаления подписчика
     * @param $sEmail
     * @return int
     */
    public static function delSubscriber( $sEmail ){
        return SubscribeUser::delete()->where('email',$sEmail)->get();
    }

}//class