<?php

/* main */
$aConfig['name']     = 'Subscribe';
$aConfig['title']    = 'Рассылка';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Модуль рассылки';
$aConfig['revision'] = '0002';
$aConfig['layer']     = \Layer::PAGE;

return $aConfig;
