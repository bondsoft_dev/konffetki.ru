<?php

namespace skewer\build\Page\Subscribe\ar;

use skewer\build\Component\orm;
use skewer\build\libs\ft;
use skewer\build\Adm\Params;

class SubscribeTemplate extends orm\TablePrototype {

    protected static $sTableName = 'subscribe_templates';
    protected static $sKeyField = 'id';


    protected static function initModel() {

        ft\Entity::get('subscribe_templates')
            ->clear(false)
            ->setPrimaryKey(self::$sKeyField)
            ->setTablePrefix('')
            ->setNamespace(__NAMESPACE__)
            ->addField('title', 'varchar(255)', 'title')
            ->addField('content', 'text', 'content')
            ->save();
    }

    public static function getNewRow($aData = array()) {
        $oRow = new SubscribeTemplateRow();

        if ($aData)
            $oRow->setData($aData);

        return $oRow;
    }
}