<?php

namespace skewer\build\Page\Subscribe\ar;
use skewer\build\Component\orm;

class SubscribePostingRow extends orm\ActiveRecord {

    public $id = 'NULL';
    public $list = '';
    public $state = '';
    public $post_date = '';
    public $last_pos = 0;
    public $id_body = 0;
    public $id_from = 0;

    function __construct() {
        $this->setTableName( 'subscribe_posting' );
        $this->setPrimaryKey( 'id' );
    }

}