<?php

namespace skewer\build\Page\Subscribe\ar;
use skewer\build\Component\orm;

class SubscribeMessageRow extends orm\ActiveRecord {

    public $id = 'NULL';
    public $title = '';
    public $text = '';
    public $template = 0;
    public $status = 0;

    function __construct() {
        $this->setTableName( 'subscribe_msg' );
        $this->setPrimaryKey( 'id' );
    }

}