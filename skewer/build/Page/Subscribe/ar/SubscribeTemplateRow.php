<?php

namespace skewer\build\Page\Subscribe\ar;
use skewer\build\Component\orm;

class SubscribeTemplateRow extends orm\ActiveRecord {

    public $id = 'NULL';
    public $title = '';
    public $content = '';

    function __construct() {
        $this->setTableName( 'subscribe_templates' );
        $this->setPrimaryKey( 'id' );
    }

}