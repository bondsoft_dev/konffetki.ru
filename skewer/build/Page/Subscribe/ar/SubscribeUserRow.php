<?php

namespace skewer\build\Page\Subscribe\ar;
use skewer\build\Component\orm;

class SubscribeUserRow extends orm\ActiveRecord {

    public $id = 'NULL';
    public $email = '';
    public $person = '';
    public $city = '';
    public $ticket = '';

    function __construct() {
        $this->setTableName( 'subscribe_users' );
        $this->setPrimaryKey( 'id' );
    }

}