<?php

$aLanguage = array();

$aLanguage['ru']['GalleryViewer.Page.tab_name'] = 'Галерея на страницу';
$aLanguage['ru']['gallery_to_show'] = 'Галереи для вывода на страницу';
$aLanguage['ru']['gallery_main_title'] = 'Заголовок для галереи';

$aLanguage['en']['GalleryViewer.Page.tab_name'] = 'Gallery on page';
$aLanguage['en']['gallery_to_show'] = 'Galleries to show on page';
$aLanguage['en']['gallery_main_title'] = 'Title gallery';

return $aLanguage;