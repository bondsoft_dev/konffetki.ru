<?php
namespace skewer\build\Page\GalleryViewer;

use skewer\build\Component\Gallery;

class Module extends \skModule {

    public $showGallery = 0;
    public $titleOnMain = 'Галерея';

    /**
     * Метод - исполнитель функционала
     */
    public function execute() {

        if (!$this->showGallery) return psRendered;
        
        $aData = Gallery\Photo::getFromAlbum(explode(',', $this->showGallery), true);

        $this->setData('list', $aData);
        $this->setData('titleOnMain', $this->titleOnMain);

        $this->setTemplate('slider.twig');

        return psComplete;
    }
}
