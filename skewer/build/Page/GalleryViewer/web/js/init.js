$(document).ready(function() {

    $('a.single_3').fancybox({
        openEffect: 'fade',
        nextEffect: 'elastic',
        prevEffect: 'elastic',
        helpers : {
            title : {
                type : 'inside'
            }
        },
        arrows    : true,
        nextClick : true,
        mouseWheel: true,
        closeBtn: true,
        beforeShow: function () {
            var imgAlt = $(this.element).find("img").attr("alt");
            var dataAlt = $(this.element).data("alt");
            if (imgAlt) {
                $(".fancybox-image").attr("alt", imgAlt);
            } else if (dataAlt) {
                $(".fancybox-image").attr("alt", dataAlt);
            }
        }
    });


    $('.gallery_slider').each(function(){

        var block = $(this).data('block');

        $(this).carouFredSel({
            circular: false,
            auto 	: false,
            width : '100%',
            align : 'center',
            prev	: {
                button	: '#foo2_prev_' + block,
                key		: "left",
                items: 1
            },
            next	: {
                button	: '#foo2_next_' + block,
                key		: "right",
                items: 1
            }

        });
    });

});
