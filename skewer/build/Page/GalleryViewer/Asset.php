<?php

namespace skewer\build\Page\GalleryViewer;

use yii\web\AssetBundle;
use yii\web\View;

class Asset extends AssetBundle {
    
    public $sourcePath = '@skewer/build/Page/GalleryViewer/web/';

    public $css = [
        'css/skin.css'
    ];

    public $js = [
        'js/jquery.carouFredSel-6.2.1-packed.js',
        'js/init.js'
    ];

    public $jsOptions = [
        'position'=>View::POS_HEAD
    ];

    public $depends = [
        'skewer\assets\JqueryAsset'
    ];

}