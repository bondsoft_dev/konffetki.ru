<?php

use skewer\build\Component\orm\Query;


/**
 * Класс работы с таблицей css параметров - css_data_params
 *
 * @class: CssParamsMapper
 *
 * @Author: User, $Author: sapozhkov $
 * @version: $Revision: 2732 $
 * @date: $Date: 2013-12-18 16:19:12 +0400 (Ср., 18 дек. 2013) $
 *
 */

class CssParamsMapper extends skMapperPrototype {

    /**
     * @var string Имя таблицы, с которой работает маппер
     */
    protected  static $sCurrentTable = 'css_data_params';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {

        return self::$sCurrentTable;
    }

    /**
     * @static Метод возвращает список полей рабочей таблицы
     * @return array
     */
    public static function getParametersList() {

        return self::$aParametersList;
    }

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'id' => 'i:hide:Номер',
        'name' => 's:str:Псевдоним',
        'group' => 'i:int:Группа',
        'layer' => 's:str:Слой',
        'title' => 's:str:Название',
        'type' => 's:str:Тип',
        'value' => 's:str:Значение',
        'default_value' => 's:str:По умолчанию',
        'range' => 's:str:Диапазон',
        'updated_at' => 's:date:Дата изменения',
    );

    /**
     * Отдает набор параметров по id группы
     * @static
     * @param $iGroupId
     * @return mixed
     */
    public static function getParamListByGroupId( $iGroupId ) {

        // Собираем массив фильтра
        $aFilter = array(
            'select_fields' => array('id','title','value','type'),
            'where_condition' => array(
                'group' => array(
                    'sign' => '=',
                    'value' => $iGroupId
                )
            ),
            'order' => array('field' => 'id', 'way' => 'ASC')
        );

        // запросить значения
        $aResponse = static::getItems($aFilter);

        // отдать
        return $aResponse['items'];

    }

    /**
     * Отдает набор параметров по id группы с зависимостями
     * @param $groupId
     * @return array
     */
    public static function getParamListByGroupIdWthRefs($groupId){

        $query = "SELECT `cdp`.`id`,
                         `cdp`.`title`,
                         `cdp`.`value`,
                         `cdp`.`type`,
                         `cdr`.`active`
                    FROM `css_data_params` AS `cdp`
               LEFT JOIN `css_data_references` AS `cdr`
                      ON `cdr`.`descendant`=`cdp`.`name`
                   WHERE `group`=?
                ORDER BY `id` ASC;";

        $result = Query::SQL( $query, $groupId );
        $items = array();

        while ($row = $result->fetchArray() ) {
            $items[] = $row;
        }

        return $items;
    }

    public static function getGroupByParam( $iParamId ) {

        // Собираем массив фильтра
        $aFilter = array(
            'select_fields' => array('group'),
            'where_condition' => array(
                'id' => array(
                    'sign' => '=',
                    'value' => $iParamId
                )
            )
        );

        // запросить значения
        $aResponse = static::getItems($aFilter);

        // отдать
        return isset($aResponse['items'][0]['group']) ? $aResponse['items'][0]['group'] : false;

    }

    /**
     * @static Метод сохранения записи, без обновления поля value
     * @param array $aInputData
     * @return bool
     */
    public static function insertItem( $aInputData = array() ){

        if ( !$aInputData ) return false;

        $aData      = array();
        $aFields    = array();
        $aUpdateFields = array();
        /*
         * Проходимся по массиву переданных извне параметров и, при совпадении варианта switch с параметром входного массива,
         * добавляем соответствующий элемент к массиву плейсхолдеров и к строке запроса
         */
        foreach( $aInputData as $sKey=>$sValue ){

            $aFieldParams = static::getParameter($sKey);

            if ( isset($aFieldParams) && sizeof($aFieldParams) ){

                $aFields[] = "`$sKey`=:$sKey";
                if ( $sKey!='value' )
                    $aUpdateFields[] = "`$sKey`=:$sKey";
                $aData[$sKey] = $sValue;
            }

        }//foreach $aNewsData

        if ( sizeof($aFields) ){

            // Если массив с элементами запроса не пуст - собираем из него строку
            $sFields = implode(',', $aFields);
            $sUpdateFields = implode(',', $aUpdateFields);

            $sTableName = static::getCurrentTable();

            $sQuery = "
                INSERT INTO
                    `$sTableName`
                SET
                    $sFields
                ON DUPLICATE KEY UPDATE
                    $sUpdateFields;";

            // выполнение запроса
            $oResult = Query::SQL( $sQuery, $aData );

            return $oResult->affectedRows() || $oResult->lastId();
        }

        return false;
    }

    /**
     * @inheritdoc
     */
    public static function saveItem( $aInputData = array() ) {
        $aInputData['updated_at'] = date('Y-m-d H:i:s');
        return parent::saveItem( $aInputData );
    }

    public static function clearTable(){

        return Query::SQL( "TRUNCATE TABLE `css_data_params`" );
    }

    /**
     * Выбирает параметры с учетом наследования
     * @return array Возвращает массив выбранных значений либо Исключение в случае ошибки
     * @throws \ErrorException
     */
    public static function getParamsWithRef() {

        $sQuery = "
            SELECT
              `cdp`.`name`,
              `cdp`.`layer`,
              IF(`cdpi`.`value` IS NULL OR `cdi`.`active`=0, `cdp`.`value`, `cdpi`.`value`) AS `value`
            FROM  `css_data_params` AS  `cdp`
            LEFT JOIN  `css_data_references` AS  `cdi` ON cdi.`descendant`=`cdp`.`name`
            LEFT JOIN  `css_data_params` AS  `cdpi` ON cdpi.name=cdi.ancestor
            WHERE
              cdp.group!=0;
        ";

        $result = Query::SQL( $sQuery );

        $out['count'] = 0;
        $out['items'] = array();

        while ( $item = $result->fetchArray() ) {
            $out['items'][] = $item;
            $out['count']++;
        }

        return $out;
    }

    public static function getParamWithRef($name, $layer) {

        $sQuery = "
            SELECT
              `cdp`.`name`,
              `cdp`.`layer`,
              `cdp`.`group`,
              IF(`cdpi`.`value` IS NULL OR `cdi`.`active`=0, `cdp`.`value`, `cdpi`.`value`) AS `value`
            FROM  `css_data_params` AS  `cdp`
            LEFT JOIN  `css_data_references` AS  `cdi` ON cdi.`descendant`=`cdp`.`name`
            LEFT JOIN  `css_data_params` AS  `cdpi` ON cdpi.name=cdi.ancestor
            WHERE
              cdp.name=:param_name AND
              cdp.layer=:layer AND
              cdp.group!=0;
        ";

        $result = Query::SQL( $sQuery, array(
            'param_name' => $name,
            'layer' => $layer,
        ));

        $out['count'] = 0;
        $out['items'] = array();

        while ( $item = $result->fetchArray() ) {
            $out['items'][] = $item;
            $out['count']++;
        }

        return $out;
    }


    /**
     * @todo рефакторить
     * Сохранение связей
     * @param array $aItems
     */
    public static function saveReferences( array $aItems ){

        /**
         * Запрос существующих
         * Связей не много, запросим все разом, вводить около сотни условий в запрос не хочется
         */
        $aCurItems = Query::SelectFrom('css_data_references')->getAll();

        foreach( $aItems as $key=>$item){
            /** Убираем те, что уже есть */
            if ($aCurItems){
                foreach($aCurItems as $aRecord){
                    if ($item[0] == $aRecord['ancestor'] && $item[1] == $aRecord['descendant'])
                        unset($aItems[$key]);
                }
            }
            /** и дубли уберем */
            foreach($aItems as $ikey=>$aItem){
                if ($ikey == $key)
                    continue;
                if ($item[0] == $aItem[0] && $item[1] == $aItem[1])
                    unset($aItems[$key]);
            }
        }

        if (count($aItems)){
            $aData = [];
            $sQuery = "
            INSERT INTO `css_data_references`(`ancestor`, `descendant`, `active`) VALUES
        ";
            $i = 0;
            foreach( $aItems as $aItem ){
                $i++;
                $sQuery .= "( :a$i, :d$i, 0),";
                $aData['a' . $i] = $aItem[0];
                $aData['d' . $i] = $aItem[1];
            }
            $sQuery = trim($sQuery, ',');

            Query::SQL( $sQuery, $aData );
        }

    }


    /**
     * @todo deprecated - много запросов, нужно использовать более оптимальные методы
     * Добавляет либо обновляет ссылку на родителя с именем параметра $ancestorName для параметра $descendantName
     * Возможно явное указание active. В случае, если один из параметров не существует - запись добавлена не будет
     * @param $ancestorName Полное имя параметра предка
     * @param $descendantName Полное имя параметра наследника
     * @param null $ancestorLayer
     * @param null $descendantLayer
     * @param int $active 1/0 todo не используется
     * @throws ErrorException
     * @return bool Возвращает true в случае успешного добавления ссылки либо выбрасывает исключение ErrorException
     * в случае ошибки
     */
    public static function saveReference($ancestorName, $descendantName, $ancestorLayer = null, $descendantLayer = null, $active = 1) {

        $aData = array(
            'ancestor'          => $ancestorName,
            'descendant'        => $descendantName,
            //'active'            => $active
        );


        if (!is_null($ancestorLayer)){
            $ancestorCondition = '  AND cdp.layer=:ancestorLayer ';
            $aData['ancestorLayer'] = $ancestorLayer;
        } else $ancestorCondition   ='';


        if (!is_null($descendantLayer)){
            $descendantCondition = '  AND jcdp.layer=:descendantLayer ';
            $aData['descendantLayer'] = $descendantLayer;
        } else $descendantCondition = '';

        $sQuery = "
            INSERT
                `css_data_references`
                (ancestor, descendant)
                SELECT
                    cdp.name,
                    jcdp.name
                FROM
                    `css_data_params` as cdp
                INNER JOIN `css_data_params` as `jcdp` ON jcdp.name=:descendant $descendantCondition
                WHERE
                    cdp.name=:ancestor $ancestorCondition LIMIT 0, 1
                ON DUPLICATE KEY UPDATE
                    ancestor=cdp.name,
                    descendant=jcdp.name;";

        Query::SQL($sQuery, $aData);

        return true;
    }

}
