<?php
/**
 * Класс работы с таблицей групп css параметров - css_data_groups
 *
 * @class: CssGroupsMapper
 *
 * @Author: User, $Author$
 * @version: $Revision$
 * @date: $Date$
 *
 */

class CssGroupsMapper extends skMapperPrototype {

    /**
     * @var string Имя таблицы, с которой работает маппер
     */
    protected  static $sCurrentTable = 'css_data_groups';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {

        return self::$sCurrentTable;
    }

    /**
     * @static Метод возвращает список полей рабочей таблицы
     * @return array
     */
    public static function getParametersList() {

        return self::$aParametersList;
    }

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'id' => 'i:hide:Номер',
        'name' => 's:str:Псевдоним',
        'title' => 's:str:Название',
        'parent' => 'i:int:Родительская группа',
        'layer' => 's:str:Слой',
        'visible' => 'i:check:Видимость',
        'priority' => 'i:int:Положение'
    );

    /**
     * Список id групп
     * @param array $sGroupNames
     * @param string $sLayerKey
     * @return array
     *          name => id
     */
    public static function getGroupsIdByName( $sGroupNames = [], $sLayerKey='default' ){

        $aFilter = array(
            'select_fields' => array(
                'id', 'name'
            ),
            'where_condition' => array(
                'name' => array(
                    'sign' => 'IN',
                    'value' => $sGroupNames
                ),
                'layer' => array(
                    'sign' => '=',
                    'value' => $sLayerKey
                )
            )
        );

        $aTempParent = self::getItems($aFilter);

        return \yii\helpers\ArrayHelper::map( $aTempParent['items'], 'name', 'id' );

    }

    public static function clearTable(){

        return \skewer\build\Component\orm\Query::SQL("TRUNCATE TABLE `css_data_groups`");
    }

}
