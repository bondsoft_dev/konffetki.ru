<?php
/**
 * Created by JetBrains PhpStorm.
 * User: user
 * Date: 30.04.13
 * Time: 16:45
 * To change this template use File | Settings | File Templates.
 */

class PageViewsMapper extends skMapperPrototype {


    /**
     * @var string Имя текущей таблицы, с которой работает маппер
     */
    protected static $sCurrentTable = 'page_views';

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(

        'class_name'    => 's:str:класс',
        'object_id'     => 'i:int:id',
        'views'         => 'i:int:Кол-во просмотров',

    );



}
