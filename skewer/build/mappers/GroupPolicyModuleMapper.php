<?php

use skewer\build\Component\orm\Query;


/**
* @class GroupPolicyModuleMapper
* @extends skModule
* @project Skewer
* @package kernel
*
* @author kolesnikov, $Author:  $
* @version $Revision: $
* @date $Date: $
*
*/

class GroupPolicyModuleMapper extends skMapperPrototype {


    /**
     * @var string Имя текущей таблицы, с которой работает маппер
     */
    protected static $sCurrentTable = 'group_policy_module';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {

        return static::$sCurrentTable;
    }

    /**
     * @static Метод возвращает список полей рабочей таблицы
     * @return array
     */
    public static function getParametersList() {

        return static::$aParametersList;
    }

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'policy_id'     => 'i:hide:policy.policy_id',
        'module_name'   => 's:str:policy.module_name',
        'title'         => 's:str:policy.title'
    );


    /**
     * @static Выборка функциональных политик
     * @param $aFilter
     * @return array|bool
     */
    public static function getGroupModuleData($aFilter=array()){

        return static::getItems($aFilter);
    }


    public static function addModule($iPolicyId, $sModuleClassName, $sModuleClassTitle = '') {

        // собрать запрос
        $sQuery = '
            INSERT INTO `group_policy_module`
                ( `policy_id`, `module_name`, `title` )
                VALUES (:policy_id, :module_name, :title)
            ON DUPLICATE KEY UPDATE
                `title` = :title
            ;
        ';

        $aData = array(
            'policy_id' => $iPolicyId,
            'module_name' => $sModuleClassName,
            'title' => $sModuleClassTitle
        );

        return Query::SQL( $sQuery, $aData )->affectedRows();
    }

    /**
     * Удаляет модуль $sModuleClassName из списка для политики $iPolicyId
     * @param $iPolicyId
     * @param $sModuleClassName
     * @return bool
     */
    public static function removeModule($iPolicyId, $sModuleClassName) {

        // собрать запрос
        $sQuery = '
            DELETE
            FROM `group_policy_module`
            WHERE
              `policy_id`=:policy_id AND
              `module_name`=:module_name
            ;
        ';

        $aData = array(
            'policy_id' => $iPolicyId,
            'module_name' => $sModuleClassName
        );

        return Query::SQL( $sQuery, $aData )->affectedRows() > 0;
    }



}