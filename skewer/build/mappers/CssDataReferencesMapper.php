<?php
/**
 * Заглушка под класс
 * Создана как быстрое решение для очистки таблицы
 */
class CssDataReferencesMapper {

    /** @var string Таблица */
    protected static $sCurrentTable = 'css_data_references';

    /** @var array Конфигурация полей таблицы */
    protected static $aParametersList = array(
        'ancestor' => 'i:int',
        'descendant' => 'i:int',
        'active' => 'i:int',
    );

    public static function clearTable(){

        return \skewer\build\Component\orm\Query::SQL( "TRUNCATE TABLE `css_data_references`" );
    }

}
