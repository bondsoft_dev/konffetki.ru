<?php

use skewer\build\Component\orm\Query;


/**
 * @class AuthUsersMapper Класс-маппер, обслуживающий таблицы user, user_policy_data, user_policy_func
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author andymitrich, $Author$
 * @version $Revision$
 * @date 12.12.11 10:32 $
 *
 */

class AuthUsersMapper extends skMapperPrototype {

    private static $sDefaultLogin = 'default';
    /**
     * @var string Имя таблицы, с которой работает маппер
     */
    protected static $sCurrentTable = 'users';

    /**
     * @static Метод возвращает список полей рабочей таблицы
     * @return array
     */
    public static function getParametersList() {

        return self::$aParametersList;
    }

    /**
     * @var array Конфигурация полей таблицы
     */
    protected static $aParametersList = array(
        'id' => 'i:hide:auth.id',
        'global_id' => 'i:hide:auth.global_id',
        'login' => 's:str:auth.login',
        'pass' => 's:pass:auth.pass',
        'group_policy_id' => 's:str:auth.group_policy_id',
        'active' => 's:check:auth.active',
        'lastlogin' => 's:str:auth.lastlogin',
        'name' => 's:str:auth.name',
        'email' => 's:str:auth.email',
        'cache' => 's:html:auth.cache',
        'version' => 's:str:auth.version'
    );

    public static function checkUser( $aInputData, $bAuto = false ){

        if ( !$aInputData ) return false;

        $sQuery = sprintf("
            SELECT
                u.*,
                gp.id AS policy_id,
                gp.alias AS policy_alias
            FROM
                `users` AS u
            INNER JOIN `group_policy` AS gp ON u.`group_policy_id`=gp.`id`
            WHERE
                u.`login`=:login AND %s %s %s gp.`area`=:login_area;",
            ( !$bAuto ? " (u.`pass`=:password OR u.`global_id`>0 ) AND" : '' ),
            ( isset($aInputData['active']) ? "u.`active`=:active AND" : '' ),
            /* Только активные политики, либо политика системного администратора */
            ( isset($aInputData['active']) ? '(gp.`active`='.Policy::stActive.' OR gp.`active`='.Policy::stSys.') AND':'' )
        );

        $oResult = Query::SQL( $sQuery, $aInputData );

        if ( $aRow = $oResult->fetchArray() ) {
            unSet( $aRow['pass'] );
        }

        return $aRow;
    }

    /**
     * @static Получаем информацию о пользователе по его ID
     * @param $iUserId
     * @param array $aFileds - набор требуемых полей
     * @return array|bool
     */
    public static function getUserData($iUserId, $aFileds=array()){

        if ( !$iUserId ) return false;

        $sQuery = sprintf(
            "SELECT %s FROM `users` WHERE id = ? LIMIT 0,1;",
            $aFileds ? '`'.implode('`,`',$aFileds).'`' : '*'
        );

        $aResult = Query::SQL( $sQuery, $iUserId );

        return $aResult->fetchArray();
    }// function getUserData()

    /**
     * @static Получаем информацию о пользователе по его логину
     * @param $sUserName
     * @return array|bool
     */
    public static function getUserDataByName($sUserName){

        if ( !$sUserName ) return false;

        $sQuery = "SELECT * FROM `users` WHERE login = ? LIMIT 0,1;";

        $oResult = Query::SQL( $sQuery, $sUserName );

        return $oResult->fetchArray();
    }// function getUserData()

    /**
     * @static Метод для получения информации из таблицы для дефолтного пользователя по логину default
     * @return array|bool
     */
    public static function getDefaultUserData(){

        $sQuery = "SELECT * FROM `users` WHERE login = ? LIMIT 0,1;";

        $oResult = Query::SQL( $sQuery, self::$sDefaultLogin );

        return $oResult->fetchArray();
    }// function getUserData()

    /**
     * @static Выбираем информацию по групповой политике доступа
     * @param $iGroupPolicyId
     * @return array|bool
     */
    public static function getGroupPolicy($iGroupPolicyId){

        if ( !$iGroupPolicyId ) return false;

        $sQuery = "
            SELECT
                *
            FROM
                `group_policy` AS gp
            INNER JOIN `group_policy_data` AS gpd ON gpd.policy_id = gp.id
            WHERE
                gp.id = :policy_id
            LIMIT
                0,1;";

        $oResult = Query::SQL( $sQuery, $iGroupPolicyId );

        return $oResult->fetchArray();
    }//function getGroupPolicy()

    /**
     * Возвращает список пользователей с возможность фильрации
     * @static
     * @param $aFilter - массив для фильтрации
     *      limit - ограничение по количеству - массив с 2 элементами
     *          start - с какого
     *          count - сколько
     *      order - порядок - массив с 2 элементами
     *          field - имя поля
     *          direction - направление ( ASC / DESC )
     *      policy - int id политики
     *      active - int активность записей
     *      search - str полнотекстовый поиск по полям login, name и email
     *      has_pass - bool наличие пароля
     * @return array
     */
    public static function getUsersList($aFilter=array()){

        if ( !is_array($aFilter) )
            $aFilter = array();

        $aData = array();

        $sLimitCondition = '';
        $sWhereCondition = '';
        $sOrderCondition = '';

        // Обрабатываем ограничение по количеству выбираемых элементов
        if ( isset($aFilter['limit']) && $aFilter['limit'] ){

            $aData['start_limit'] = $aFilter['limit']['start'];
            $aData['count_limit'] = $aFilter['limit']['count'];
            $sLimitCondition = " LIMIT :start_limit, :count_limit";
        }

        /* Сортировки */
        if ( isset($aFilter['order']) && $aFilter['order'] ){

            $sOrderCondition = " ORDER BY ".$aFilter['order']['field']." ".$aFilter['order']['direction'];
        }

        /* Фильтр по групповой политике */
        if ( isset($aFilter['policy']) && $aFilter['policy'] > 0 ){

            $aData['policy_id'] = $aFilter['policy'];

            $sWhereCondition .= " AND u.group_policy_id=:policy_id";
        }

        /* Фильтр по статусу */
        if ( isset($aFilter['active']) AND $aFilter['active'] > -1 ){

            $aData['active'] = $aFilter['active'];

            $sWhereCondition .= " AND u.active=:active";
        }

        /* Полнотекстовый поиск по полю */
        if ( isset($aFilter['search']) AND !empty($aFilter['search']) ){

            $aData['search'] = '%' . $aFilter['search'] . '%';

            $sWhereCondition .= " AND ( `login` LIKE :search OR  `email` LIKE :search OR  `name` LIKE :search)";
        }

        /* Только с паролем */
        if ( isset($aFilter['has_pass']) AND $aFilter['has_pass'] ){
            $sWhereCondition .= " AND `pass`!='' ";
        }

//  todo разобраться с эмит куском - походу эторудимент от админинстрации
//        if ( !CurrentAdmin::isAdminPolicy() ){
//
//            $aData['policy_id'] = 1;
//            /* todo Нужно переделать таким образом, чтобы можно было определять какие политики являются админскими */
//            $sWhereCondition .= " AND (u.alias!='admin' OR u.alias!='sysadmin') ";
//        }

        $sQuery = "
            SELECT
            SQL_CALC_FOUND_ROWS
                u.`id`,
                u.`login` AS `login`,
                u.`lastlogin` AS `lastlogin`,
                u.`name` AS `name`,
                u.`active` AS `active`,
                u.`email` AS `email`,
                gp.`title` AS `policy`
            FROM
                `users` AS u
            LEFT JOIN `group_policy` AS gp ON u.group_policy_id=gp.id
            WHERE 1
            $sWhereCondition
            $sOrderCondition
            $sLimitCondition;";

        $rResult = Query::SQL( $sQuery, $aData );


        // Собирается результирующий массив
        $aItems = array( 'items' => array() );

        while ( $aRow = $rResult->fetchArray() ) {

            $aItems['items'][] = $aRow;
        }

        // Получаем общее число записей из таблицы
        $aItems['count'] = Query::SQL( "SELECT FOUND_ROWS() as `rows`;" )->getValue( 'rows' );

        return $aItems;
    }

    public static function getUserDetail($iUserId){

        $sQuery = "
            SELECT
                u.`id`,
                u.`login`,
                u.`lastlogin`,
                u.`group_policy_id`,
                u.`name`,
                u.`active`,
                u.`email`,
                gp.`title` AS `policy`
            FROM
                `users` AS u
            LEFT JOIN `group_policy` AS gp ON u.group_policy_id=gp.id
            WHERE
                u.`id`=?;";

        $oResult = Query::SQL( $sQuery, $iUserId );

        return $oResult->fetchArray();
    }

    public static function delUser($iUserId){

        $sQuery = "
            DELETE
            FROM
                `users`
            WHERE
                `id`=? AND
                `del_block`<>1;";

        return Query::SQL( $sQuery, $iUserId )->affectedRows();
    }

}//class
