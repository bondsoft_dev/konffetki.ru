<?php

namespace skewer\build\Adm\FAQ\ar;


use skewer\build\Component\orm;
use skewer\build\libs\ft;
use \yii\base\ModelEvent;
use skewer\build\Component;
use skewer\build\Adm\FAQ\Search;

class FAQ extends orm\TablePrototype{

    /** @var string Имя таблицы */
    protected static $sTableName = 'faq';

    protected static $sKeyField = 'id';

    public static function getNewRow($aData = array()) {
        $oRow = new FAQRow();
        if ($aData)
            $oRow->setData($aData);

        return $oRow;
    }

    protected static function initModel(){
        ft\Entity::get( static::$sTableName )
            ->clear()
            ->setTablePrefix('')
            ->setNamespace( __NAMESPACE__ )

            ->addField('parent', 'int(11)', \Yii::t('faq', 'parent'))
            ->addField('date_time', 'datetime', \Yii::t('faq', 'date_time'))
            ->setDefaultVal( 'now' )

            ->addField('name', 'varchar(255)', \Yii::t('faq', 'name'))
            ->addField('email', 'varchar(255)', \Yii::t('faq', 'email'))
            ->addField('content', 'text', \Yii::t('faq', 'content'))
            ->addField('status', 'int(3)', \Yii::t('faq', 'status'))
            ->addField('city', 'varchar(255)', \Yii::t('faq', 'city'))
            ->addField('answer', 'text', \Yii::t('faq', 'answer'))

            ->addField('alias', 'varchar(255)', \Yii::t('faq', 'alias'))

            ->addDefaultProcessorSet()
            ->addColumnSet(
                'list',
                array( 'name','date_time','content', 'answer', 'status' )
            )

            ->save()
        ;
    }

    /**
     * Удаление всех вопросов для раздела
     * @param ModelEvent $event
     */
    public static function removeSection( ModelEvent $event ) {
        FAQ::delete()->where( 'parent', $event->sender->id )->get();
    }

    /**
     * Класс для сборки списка автивных поисковых движков
     * @param Component\Search\GetEngineEvent $event
     */
    public static function getSearchEngine( Component\Search\GetEngineEvent $event ) {
        $event->addSearchEngine( Search::className() );
    }

    /**
     * Получаем алиас
     * @param $sAlias
     * @param $iId
     * @return bool|string
     */
    public static function checkAlias( $sAlias, $iId ) {

        if ( !($sAlias) ) $sAlias = date('d-m-Y-H-i');

        $aItems = FAQ::find()->where( 'alias', $sAlias )->get();
        $iSize = count( $aItems );

        if ( !$iSize || ($iSize==1 && ($iId == $aItems[0]->id) ) )
            return $sAlias;

        return self::checkAlias( $sAlias.++$iSize, $iId );
    }

}