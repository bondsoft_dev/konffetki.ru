<?php

namespace skewer\build\Adm\FAQ\ar;


use skewer\build\Component\orm;
use skewer\build\Component\Search;
use skewer\build\libs\ft;

class FAQRow extends orm\ActiveRecord{

    public $id = 0;
    public $parent = 0;
    public $date_time = '';
    public $name = '';
    public $email = '';
    public $content = '';
    public $status = 0;
    public $city = '';
    public $answer = '';

    public $alias = '';

    function __construct() {
        $this->setTableName( 'faq' );
        $this->setPrimaryKey( 'id' );
    }

    /**
     * @inheritdoc
     */
    public function preSave()
    {
        if ( !$this->date_time || ($this->date_time == 'null') )
            $this->date_time = date("Y-m-d H:i:s", time());

        if (!$this->alias){
            $sValue = \skTranslit::change( strip_tags(html_entity_decode($this->content)) );
        }else{
            $sValue = \skTranslit::change( $this->alias );
        }

        // к числам прибавляем префикс
        if ( is_numeric($sValue) ){
            $sValue = 'faq-' . $sValue;
        }

        // приводим к нужному виду
        $sValue = \skTranslit::changeDeprecated( $sValue );
        $sValue = \skTranslit::mergeDelimiters( $sValue );
        $sValue = trim($sValue,'-');

        $this->alias = FAQ::checkAlias($sValue, $this->id);

        parent::preSave();
    }

}