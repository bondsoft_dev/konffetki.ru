<?php

namespace skewer\build\Adm\FAQ;


use skewer\build\Component\I18N\ModulesParams;

class Api {

    /** статус "новый" */
    const statusNew = 0;

    /** статус "одобрен" */
    const statusApproved = 1;

    /** статус "отклонен" */
    const statusRejected = 2;

    /**
     * Отдает набор доступных статусов
     * @return array
     */
    public static function getStatusList() {
        return array(
            static::statusNew => \Yii::t('faq', 'new'),
            static::statusApproved => \Yii::t('faq', 'approved'),
            static::statusRejected => \Yii::t('faq', 'rejected')
        );
    }

    /**
     * Сохранение записи
     * @param array $aData
     * @return bool
     */
    public static function saveItem(array $aData){

        $id = isset($aData['id'])?$aData['id']:false;
        if ($id){
            $oFAQRow = ar\FAQ::find($id);
        }else{
            $oFAQRow = ar\FAQ::getNewRow(array('status' => Api::statusNew));
        }

        if( !isset($aData['date_time']) || !$aData['date_time'] )
            $aData['date_time'] = date('Y-m-d H:i:s');

        if (!isset($aData['status'])){
            $aData['status'] = static::statusNew;
        }

        /** @var array $aModulesData */
        $aModulesData = ModulesParams::getByModule('faq');

        /**
         * если статус сменился - отправляем письмо
         */
        if (isset($aModulesData['onNotif']) && $aModulesData['onNotif'] && $oFAQRow->status != $aData['status']){
            $sEmail = (isset($aData['email']))?$aData['email']:'';
            /** одобрение */
            if ($aData['status'] == static::statusApproved){
                $sTitle = isset($aModulesData['notifTitleApprove'])?$aModulesData['notifTitleApprove']:'';

                $sTitle = ($sTitle) ? $sTitle : \Yii::t('faq', 'answer_title_template').
                    " [".\Yii::t('app', 'site_label')."]";

                $sContent = isset($aModulesData['notifContentApprove'])?$aModulesData['notifContentApprove']:'';
                $sContent = ($sContent) ? $sContent :
                    \Yii::t('faq', 'answer_approve_content_template', \Yii::t('app', 'site_label'));

                \Mailer::sendMail( $sEmail, $sTitle, $sContent, []);
            }
            /** отклонение */
            if ($aData['status'] == static::statusRejected){
                $sTitle = isset($aModulesData['notifTitleReject'])?$aModulesData['notifTitleReject']:'';

                $sTitle = ($sTitle) ? $sTitle : \Yii::t('faq', 'answer_title_template').
                    " [".\Yii::t('app', 'site_label')."]";

                $sContent = isset($aModulesData['notifContentReject'])?$aModulesData['notifContentReject']:'';
                $sContent = ($sContent) ? $sContent :
                    \Yii::t('faq', 'answer_reject_content_template', \Yii::t('app', 'site_label'));

                \Mailer::sendMail( $sEmail, $sTitle, $sContent, []);
            }
        }

        $oFAQRow->setData($aData);

        return $oFAQRow->save();
    }

    /**
     * Отправляем письмо на мыло админа
     * @param string $sEmailClient
     * @return bool
     */
    public static function sendMailToAdmin( $sEmailClient ){

        // берем заголовок письма из базы
        $sTitle = ModulesParams::getByName('faq', 'title_admin');

        $aParams = [];
        $aParams['email'] = $sEmailClient;

        $sContent = ModulesParams::getByName('faq', 'content_admin');

        return \Mailer::sendMailAdmin($sTitle, $sContent, $aParams);

    }

    //автоответ посетителю
    public static function sendMailToClient( $sClientEmail ) {

        $sTitle = ModulesParams::getByName( 'faq', 'title_user' );
        $sContent = ModulesParams::getByName( 'faq', 'content_user' );

        return \Mailer::sendMail( $sClientEmail, $sTitle, $sContent, []);

    }

}
