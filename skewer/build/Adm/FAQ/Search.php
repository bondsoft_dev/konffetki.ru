<?php


namespace skewer\build\Adm\FAQ;


use skewer\build\Component\orm\Query;
use skewer\build\Component\Search\Prototype;
use skewer\build\Component\Search\Row;
use skewer\build\Component\Section\Parameters;
use skewer\build\Component\Section\Tree;

class Search extends Prototype {
    /**
     * отдает имя идентификатора ресурса для работы с поисковым индексом
     * @return string
     */
    public function getName() {
        return 'FAQ';
    }

    /**
     * @inheritdoc
     */
    protected function update(Row $oSearchRow) {

        $oSearchRow->class_name = $this->getName();

        if (!$oSearchRow->object_id)
            return false;

        /** @var ar\FAQRow $oFAQRow */
        $oFAQRow = ar\FAQ::find($oSearchRow->object_id);
        if (!$oFAQRow)
            return false;

        // нет данных - не добавлять в индекс
        if (!$oFAQRow->content)
            return false;

        if ($oFAQRow->status != Api::statusApproved)
            return false;

        $oSearchRow->search_text = $this->stripTags($oFAQRow->content);
        $oSearchRow->search_title = $this->stripTags($oFAQRow->content);
        $oSearchRow->section_id = $oFAQRow->parent;
        $oSearchRow->status = 1;
        $oSearchRow->use_in_search = true;

        $oSearchRow->language = Parameters::getLanguage($oFAQRow->parent);

        // проверка существования раздела и реального url у него
        $oSection = Tree::getSection( $oSearchRow->section_id );
        if ( !$oSection || !$oSection->hasRealUrl() )
            return false;

        $oSearchRow->href  = $sURL = \Yii::$app->router->rewriteURL(sprintf(
            '[%s][FAQ?%s=%s]',
            $oFAQRow->parent,
            $oFAQRow->alias ? 'alias' : 'id',
            $oFAQRow->alias ? $oFAQRow->alias : $oFAQRow->id
        ));

        $oSearchRow->save();
        return true;
    }

    public function restore() {
        $sql = "INSERT INTO search_index(`status`,`class_name`,`object_id`)  SELECT '0','{$this->getName()}',id  FROM faq";
        Query::SQL($sql);
    }

} 