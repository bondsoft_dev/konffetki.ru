<?php

namespace skewer\build\Adm\FAQ;


use skewer\build\Component\I18N\Languages;
use skewer\build\Component\I18N\ModulesParams;
use skewer\build\Component\UI;
use skewer\build\Adm;
use skewer\build\libs\ExtBuilder;
use skewer\build\Component\SEO;
use yii\base\UserException;
use yii\helpers\ArrayHelper;

/**
 * Class Module
 * @package skewer\build\Adm\FAQ
 */
class Module extends Adm\Tree\ModulePrototype {

    // число элементов на страницу
    public  $onAdmPage = 20;

    // текущий номер страницы ( с 0, а приходит с 1 )
    protected $iPage = 0;

    protected $iStatusFilter = Api::statusNew;

    protected $sLanguageFilter = '';

    private $iSectionId = 0;

    private $paramKeys = [
        'title_admin', 'content_admin',
        'title_user', 'content_user', 'onNotif',
        'notifTitleApprove', 'notifContentApprove', 'notifTitleReject',
        'notifContentReject'
    ];

    /**
     * Метод, выполняемый перед action меодом
     * @throws UserException
     * @return bool
     */
    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt('page');

        // id текущего раздела
        $this->iSectionId = $this->getInt('sectionId');

        $this->iStatusFilter = $this->get('filter_status', false);

        $this->sLanguageFilter = $this->get('filter_language', \Yii::$app->language);

        // проверить права доступа
        if ( !\CurrentAdmin::canRead($this->iSectionId) )
            throw new UserException( 'accessDenied' );

    }

    /**
     * Установка служебных данных
     * @param UI\State\BaseInterface $oIface
     */
    protected function setServiceData( UI\State\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData( array(
            'sectionId' => $this->iSectionId,
            'page' => $this->iPage,
            'filter_status' => $this->iStatusFilter,
            'filter_language' => $this->sLanguageFilter,
        ) );

    }

    protected function actionInit(){
        $this->actionList();
    }

    /**
     * list
     */
    protected function actionList(){

        $oFormBuilder = UI\StateBuilder::newList();

        /** Фильтр по статусу */
        $oFormBuilder->addFilterSelect( 'filter_status', Api::getStatusList(), $this->iStatusFilter, \Yii::t('faq', 'status') );

        /** Добавляем поля для списка */
        $oFormBuilder
            ->fieldString('name', \Yii::t('faq', 'name'))
            ->fieldString('date_time', \Yii::t('faq', 'date_time'))
            ->fieldString('content', \Yii::t('faq', 'content'), ['listColumns' => ['flex' => 3]])
            ->fieldString('answer', \Yii::t('faq', 'answer'), ['listColumns' => ['flex' => 3]])
            ->fieldString('status', \Yii::t('faq', 'status'))
            ;

        /** для статуса */
        $oFormBuilder->addWidget( 'status', 'skewer\\build\\Adm\\FAQ\\Service', 'getStatusValue' );

        /** кнопки в записи */
        $oFormBuilder->buttonRowUpdate('edit');
        $oFormBuilder->buttonRowDelete('delete');

        /** кнопки общие */
        $oFormBuilder->button(\Yii::t('faq', 'add'), 'new', 'icon-add', 'init');
        $oFormBuilder->button(\Yii::t('faq', 'settings'), 'settings', 'icon-edit', 'init');

        $aItems = ar\FAQ::find();
        if ($this->iStatusFilter !== false){
            $aItems = $aItems->where('status', $this->iStatusFilter);
        }

        $iCount = 0;
        $aItems = $aItems
            ->where('parent', $this->iSectionId)
            ->order('date_time', 'DESC')
            ->setCounterRef( $iCount )
            ->limit($this->onAdmPage, $this->iPage * $this->onAdmPage)
            ->asArray()
            ->getAll();

        $oFormBuilder->setValue($aItems, $this->onAdmPage, $this->iPage, $iCount);

        $this->setInterface($oFormBuilder->getForm());
    }

    /**
     * edit
     */
    protected function actionEdit(){
        $this->showForm($this->getInDataValInt('id'));
    }

    /**
     * new
     */
    protected function actionNew(){
        $this->showForm();
    }

    /**
     * Форма редактирования
     * @param null $id
     */
    private function showForm($id = null){

        $oForm = UI\StateBuilder::newEdit();

        // добавление кнопок
        $oForm->addButtonSave('save');
        $oForm->addButtonCancel('list');

        /**
         * @var $oFAQRow ar\FAQRow
         */
        if ($id){
            $oFAQRow = ar\FAQ::find($id);

            // кнопки модерации
            if ( $oFAQRow->status == Api::statusNew ) {

                $oForm->buttonSeparator('-');

                /** Одобрить */
                $oForm->addExtButton( \ExtDockedAddBtn::create()
                        ->setAction('save')
                        ->setTitle(\Yii::t('faq', 'approve'))
                        ->setIconCls('icon-commit')
                        ->setAddParam( 'setStatus', Api::statusApproved )
                        ->unsetDirtyChecker()
                );

                /** отклонить */
                $oForm->addExtButton( \ExtDockedAddBtn::create()
                        ->setAction('save')
                        ->setTitle(\Yii::t('faq', 'reject'))
                        ->setIconCls('icon-stop')
                        ->setAddParam( 'setStatus', Api::statusRejected )
                        ->unsetDirtyChecker()
                );
            }

            // кнопка удаления
            $oForm->buttonSeparator('->');
            $oForm->button(\Yii::t('adm','del'), 'delete', 'icon-delete', 'list');
        }else{
            $oFAQRow = ar\FAQ::getNewRow(array('status' => Api::statusNew));
            $oFAQRow->date_time = date('Y-m-d H:i:s');
        }

        /** Добавим поля */
        $oForm
            ->fieldHide('id', 'id')
            ->fieldString('name', \Yii::t('faq', 'name'))
            ->fieldString('email', \Yii::t('faq', 'email'))
            ->fieldString('city', \Yii::t('faq', 'city'))
            ->field('date_time', \Yii::t('faq', 'date_time'), 's', 'datetime')
            ->field('content', \Yii::t('faq', 'content'), 's', 'wyswyg')
            ->field('answer', \Yii::t('faq', 'answer'), 's', 'wyswyg')
            ->addSelectField('status', \Yii::t('faq', 'status'), 'i', Api::getStatusList())

            ->fieldString('alias', \Yii::t('faq', 'alias'));

        $oForm->setValue($oFAQRow->getData());

        // добавление SEO блока полей
        $oSearch = new Search();
        SEO\Api::appendExtForm( $oForm->getForm(), $oSearch->getName(), $oFAQRow->id );

        $this->setInterface($oForm->getForm());
    }

    /**
     * save
     */
    protected function actionSave(){

        $aData = $this->getInData();
        if( !isset($aData['parent']) )
            $aData['parent'] = $this->iSectionId;

        // перекрытие статуса
        $iStatus = $this->get( 'setStatus', null );
        if ( !is_null( $iStatus ) )
            $aData['status'] = $iStatus;

        $iId = Api::saveItem($aData);

        $oSearch = new Search();
        $oSearch->updateByObjectId($iId);

        // сохранение SEO данных
        $oSearch = new Search();
        SEO\Api::saveJSData( $oSearch->getName(), $iId, $aData );

        SEO\Api::setUpdateSitemapFlag();

        $this->actionList();
    }

    /**
     * delete
     */
    protected function actionDelete(){
        $id = $this->getInDataValInt('id');
        if ($id){
            ar\FAQ::delete($id);
            $oSearch = new Search();
            $oSearch->deleteByObjectId($id);
        }
        $this->actionList();
    }


    /**
     * Форма настроек модуля
     */
    protected function actionSettings(){

        $oForm = UI\StateBuilder::newEdit();

        $oForm->addButtonSave('saveSettings');
        $oForm->addButtonCancel('init');

        $aLanguages = Languages::getAllActive();
        $aLanguages = ArrayHelper::map($aLanguages, 'name', 'title');

        if (count($aLanguages) > 1) {
            $oForm->addFilterSelect('filter_language', $aLanguages, $this->sLanguageFilter, \Yii::t('adm','language'), ['set' => true]);
            $oForm->addFilterAction('settings');
        }

        $oForm
            ->field('info', '', 's', 'show', ['hideLabel' => true])
            ->fieldString('title_admin', \Yii::t('faq', 'mail_title'))
            ->field('content_admin', \Yii::t('faq', 'mail_body'), 's', 'wyswyg', ['listColumns.flex' => 1, 'growMax' => 500, 'grow' => true])
            ->fieldString('title_user', \Yii::t('faq', 'mail_title_user'))
            ->field('content_user', \Yii::t('faq', 'mail_body_user'), 's', 'wyswyg', ['listColumns.flex' => 1, 'growMax' => 500, 'grow' => true])
            ->field('onNotif', \Yii::t('faq', 'enable_notifications'), 'i', 'check')
            ->fieldString('notifTitleApprove', \Yii::t('faq', 'email_approve_subject'))
            ->field('notifContentApprove', \Yii::t('faq', 'email_approve_text'), 's', 'wyswyg', ['listColumns.flex' => 1, 'growMax' => 500, 'grow' => true])
            ->fieldString('notifTitleReject', \Yii::t('faq', 'email_reject_subject'))
            ->field('notifContentReject', \Yii::t('faq', 'email_reject_text'), 's', 'wyswyg', ['listColumns.flex' => 1, 'growMax' => 500, 'grow' => true])
        ;

        $aModulesData = ModulesParams::getByModule('faq', $this->sLanguageFilter);
        $this->setInnerData('languageFilter', $this->sLanguageFilter);

        $aItems = [];
        $aItems['info'] = \Yii::t('faq', 'head_mail_text', [\Yii::t('app', 'site_label'), \Yii::t('app', 'url_label')]);

        foreach( $this->paramKeys as $sKey ){
            $aItems[$sKey] = ArrayHelper::getValue($aModulesData, $sKey, '');
        }

        $oForm->setValue($aItems);

        // вывод данных в интерфейс
        $this->setInterface( $oForm->getForm() );
    }
    
    /**
     * Сохраняем настройки формы
     */
    protected function actionSaveSettings(){

        $aData = $this->getInData();

        $sLanguage = $this->getInnerData('languageFilter');
        $this->setInnerData('languageFilter', '');

        if ($sLanguage){
            foreach( $aData as $sName => $sValue ){

                if (!in_array($sName, $this->paramKeys))
                    continue;

                ModulesParams::setParams( 'faq', $sName, $sLanguage, $sValue);
            }
        }

        $this->actionInit();
    }

}
