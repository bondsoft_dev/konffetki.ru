<?php

use skewer\models\TreeSection;
use skewer\build\Component\Search;

/* main */
$aConfig['name']     = 'FAQ';
$aConfig['title']    = 'Модуль вопросов и ответов';
$aConfig['version']  = '2.000a';
$aConfig['description']  = 'Модуль вопросов и ответов';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::ADM;
$aConfig['useNamespace'] = true;
$aConfig['languageCategory']     = 'faq';

$aConfig['events'][] = [
    'event' => TreeSection::EVENT_BEFORE_DELETE,
    'eventClass' => TreeSection::className(),
    'class' => skewer\build\Adm\FAQ\ar\FAQ::className(),
    'method' => 'removeSection',
];
$aConfig['dependency'] = [
    ['FAQ', \Layer::PAGE],
];

$aConfig['events'][] = [
    'event' => Search\Api::EVENT_GET_ENGINE,
    'class' => skewer\build\Adm\FAQ\ar\FAQ::className(),
    'method' => 'getSearchEngine',
];

return $aConfig;
