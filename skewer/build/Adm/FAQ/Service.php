<?php

namespace skewer\build\Adm\FAQ;


use skewer\build\Component\orm;

class Service extends \ServicePrototype{

    /**
     * @param orm\ActiveRecord $oItem
     * @return string
     */
    public static function getStatusValue($oItem){
        $aStatulList = Api::getStatusList();

        if (isset($aStatulList[$oItem['status']])){
            return $aStatulList[$oItem['status']];
        } else return '';
    }
}