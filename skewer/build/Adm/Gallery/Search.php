<?php


namespace skewer\build\Adm\Gallery;

use skewer\build\Component\orm\Query;
use skewer\build\Component\Search\Prototype;
use skewer\build\Component\Search\Row;
use skewer\build\Component\Gallery;
use skewer\build\Component\Section\Parameters;
use skewer\build\Component\Section\Tree;

class Search extends Prototype {

    /**
     * отдает имя идентификатора ресурса для работы с поисковым индексом
     * @return string
     */
    public function getName() {
        return "Gallery";
    }

    /**
     * @inheritdoc
     */
    protected function update(Row $oSearchRow) {

        $oSearchRow->class_name = $this->getName();

        if (!$oSearchRow->object_id)
            return false;

        /** @var Gallery\models\Albums $oAlbum */
        if (!$oAlbum = Gallery\models\Albums::findOne($oSearchRow->object_id))
            return false;

        if ( !Gallery\Photo::getCountByAlbum( $oAlbum->id ) )
            return false;

        // нет описания - не добавлять в индекс
        if (!$oAlbum->description)
            return false;

        if (!$oAlbum->visible)
            return false;

        /** Если стоит галочка выводить только фото, то не добавляем в поиск */
        if ($oAlbum->section_id && Parameters::getValByName($oAlbum->section_id, 'content', 'openAlbum'))
            return false;

        $oSearchRow->search_text = $this->stripTags($oAlbum->description);
        $oSearchRow->search_title = $this->stripTags($oAlbum->title);
        $oSearchRow->section_id = $oAlbum->section_id;
        $oSearchRow->status = 1;
        $oSearchRow->use_in_search = true;

        // проверка существования раздела и реального url у него
        $oSection = Tree::getSection( $oSearchRow->section_id );
        if ( !$oSection || !$oSection->hasRealUrl() )
            return false;

        $oSearchRow->language = Parameters::getLanguage($oAlbum->section_id);

        $oSearchRow->href  = $sURL = \Yii::$app->router->rewriteURL(sprintf(
            '[%s][Gallery?%s=%s]',
            $oAlbum->section_id,
            'alias',
            $oAlbum->alias
        ));

        $oSearchRow->save();
        return true;

    }

    /**
     *  воссоздает полный список пустых записей для сущности, отдает количество добавленных
     * @return int
     */
    public function restore() {
        $sql = "INSERT INTO search_index(`status`,`class_name`,`object_id`) SELECT '0', '{$this->getName()}', id FROM photogallery_albums WHERE profile_id=6";
        Query::SQL($sql);
    }

}
