<?php

$aLanguage = array();

// Albums
$aLanguage['ru']['Gallery.Adm.tab_name'] = 'Галерея';
$aLanguage['ru']['owner'] = 'Модуль-владелец фотоальбома';
$aLanguage['ru']['title'] = 'Название альбома';
$aLanguage['ru']['alias'] = 'Псевдоним';
$aLanguage['ru']['visible'] = 'Выводить на сайте';
$aLanguage['ru']['album_id'] = 'id альбома';
$aLanguage['ru']['priority'] = 'Индекс сортировки';
$aLanguage['ru']['profile_id'] = 'Профиль обработки изображений';
$aLanguage['ru']['section_id'] = 'Id раздела';
$aLanguage['ru']['description'] = 'Описание';
$aLanguage['ru']['creation_date'] = 'Дата создания';
$aLanguage['ru']['albums'] = ' Альбомы';
$aLanguage['ru']['album'] = ' Альбом';
$aLanguage['ru']['openAlbum'] = ' Только фотографии';
$aLanguage['ru']['galleryNoAlbums'] ='Нет добавленных альбомов';


// Formats
$aLanguage['ru']['formats_title'] = 'Название формата';
$aLanguage['ru']['formats_name'] = 'Техническое имя';
$aLanguage['ru']['formats_width'] = 'Ширина(px)';
$aLanguage['ru']['formats_height'] = 'Высота(px)';
$aLanguage['ru']['formats_active'] = 'Использовать формат';
$aLanguage['ru']['formats_format_id'] = 'ID';
$aLanguage['ru']['formats_resize_on_larger_side'] = 'Ресайз по большей стороне';
$aLanguage['ru']['formats_scale_and_crop'] = 'Вписывать изображение';
$aLanguage['ru']['formats_use_watermark'] = 'Использовать водяные знаки';
$aLanguage['ru']['formats_watermark'] = 'Текст водяного знака';
$aLanguage['ru']['formats_profile_id'] = 'id профиля';
$aLanguage['ru']['formats_watermark_align'] = 'Выравнивание водяного знака';
$aLanguage['ru']['formats_priority'] = 'Позиция при сортировки';

// Images
$aLanguage['ru']['images_title'] = 'Название фотографии';
$aLanguage['ru']['images_alt_title'] = 'Альтернативный заголовок';
$aLanguage['ru']['image'] = 'Изображение';
$aLanguage['ru']['images_source'] = 'Исходное изображение';
$aLanguage['ru']['images_visible'] = 'Выводить';
$aLanguage['ru']['images_priority'] = 'Индекс сортировки';
$aLanguage['ru']['images_image_id'] = 'ID изображения';
$aLanguage['ru']['images_album_id'] = 'ID альбома';
$aLanguage['ru']['images_thumbnail'] = 'Миниатюра для списка';
$aLanguage['ru']['images_images_data'] = 'Массив списка изображений по форматам';
$aLanguage['ru']['images_description'] = 'Описание';
$aLanguage['ru']['images_creation_date'] = 'Дата создания';

$aLanguage['ru']['galleryActive'] = 'Активность';
$aLanguage['ru']['galleryDelImg'] = 'Удалить изображение?';
$aLanguage['ru']['galleryMultiDelImg'] = 'Удалить изображения';
$aLanguage['ru']['galleryDeleteConfirm'] = 'Подтверждение удаления';
$aLanguage['ru']['galleryNoImages'] ='Нет изображений в альбоме';
$aLanguage['ru']['galleryNoItems'] ='Не выбрано элементов для удаления';

$aLanguage['ru']['galleryDeleteAlbum']  = 'Удалить альбом?';
$aLanguage['ru']['galleryDeleteAlbums'] = 'Удалить альбомы';
$aLanguage['ru']['galleryDeleteMeasure']= 'шт.';
$aLanguage['ru']['galleryUploadingImage']= 'Идет загрузка изображения';


//Profiles
$aLanguage['ru']['profiles_title'] = 'Название профиля';
$aLanguage['ru']['profiles_active'] = 'Выводить в списке форматов';
$aLanguage['ru']['profiles_profile_id'] = 'ID';

//Module
$aLanguage['ru']['module_addUpdAlbum'] = 'Редактировать';
$aLanguage['ru']['module_getAlbums'] = 'В список';
$aLanguage['ru']['module_loadImage'] = 'Загрузить';
$aLanguage['ru']['module_images'] = 'Изображения';

$aLanguage['ru']['module_showInAlbum'] = 'Выводить в альбоме';
$aLanguage['ru']['module_title'] = 'Название';
$aLanguage['ru']['module_alt_title'] = 'Альтернативный заголовок';
$aLanguage['ru']['module_description'] = 'Описание';
$aLanguage['ru']['module_creation_date'] = 'Дата создания';
$aLanguage['ru']['module_edit'] = 'Изменить';
$aLanguage['ru']['module_add'] = 'Добавление/Редактирование альбома';
$aLanguage['ru']['module_load'] = 'Загрузка изображения';
$aLanguage['ru']['module_load_subtext'] = 'Вы можете выбрать размер кадрирования фотографии, потянув за рамки фотографии';
$aLanguage['ru']['module_editImage'] ='Редактирование изображения';

//tools
$aLanguage['ru']['tools_profileImageSettings'] ='Профили настроек изображений';
$aLanguage['ru']['tools_addProfile'] ='Добавление профиля настроек';
$aLanguage['ru']['tools_editProfile'] ='Редактирование профиля настроек';
$aLanguage['ru']['tools_formats'] ='Форматы';
$aLanguage['ru']['tools_formatsImage'] ='Форматы изображений';
$aLanguage['ru']['tools_backToProfile'] ='К профилю';
$aLanguage['ru']['tools_addFormat'] ='Добавление формата изображения';
$aLanguage['ru']['tools_editFormat'] ='Редактирование формата изображения';
$aLanguage['ru']['tools_saveFormatMessage'] ='Формат сохранен';




$aLanguage['ru']['albumError'] ='Ошибка: Альбом не определен!';
$aLanguage['ru']['loadNotice'] ='Вы можете загрузить фото не превышающее {0, number} Мб по объему, {1, number} пикселей по большей из сторон и только следующих форматов: {2}.';
$aLanguage['ru']['noImageError'] ='Ошибка: Изображение не найдено!';
$aLanguage['ru']['badImageError'] ='Ошибка: Изображение повреждено!';
$aLanguage['ru']['badProfileError'] ='Ошибка: Видимо, профиль для этого альбома был удален или не существует!';
$aLanguage['ru']['noSaveImage'] ='Ошибка: Изображение не сохранено!';
$aLanguage['ru']['noDeleteImage'] ='Ошибка: Изображение не удалено!';
$aLanguage['ru']['deleteImage'] ='Удаление фото';
$aLanguage['ru']['deleteImagesPro'] ='Удалено изображений: {0, number} из {1, number}';
$aLanguage['ru']['photoId'] ='id фото';
$aLanguage['ru']['loadDataError'] ='Ошибка входных данных!';
$aLanguage['ru']['noAlbumError'] ='Альбом не определен!';
$aLanguage['ru']['badData'] ='Неверные данные!';
$aLanguage['ru']['editImageTab'] ='Изменение изображения ';
$aLanguage['ru']['badFormat'] ='Ошибка: неверно задан формат изображения!';
$aLanguage['ru']['noDataToSave'] ='Отсутствуют данные для сохранения фото.';
$aLanguage['ru']['noLoadImage'] ='Изображение не было загружено!';
$aLanguage['ru']['noSaveError'] ='Изображение не сохранено!';
$aLanguage['ru']['saveImageNotice'] ='Сохранение изображения';
$aLanguage['ru']['noUploadData'] ='Ошибка: Данные не были отправлены!';
$aLanguage['ru']['saveAlbumReport'] ='Сохранение альбома';
$aLanguage['ru']['deleteAlbumReport'] ='Удаление альбома';
$aLanguage['ru']['deleteAlbumName'] ='Альбом удален: ';
$aLanguage['ru']['noSort'] ='Не заданы параметры для сортировки';
$aLanguage['ru']['deleteAlbumsPro'] ='Удалено альбомов: {0, number} из {1, number}';
$aLanguage['ru']['albumDeleting'] ='Удаление альбомов';





// Albums
$aLanguage['en']['Gallery.Adm.tab_name'] = 'Gallery';
$aLanguage['en']['owner'] = 'Owner';
$aLanguage['en']['title'] = 'Title';
$aLanguage['en']['alias'] = 'Alias';
$aLanguage['en']['visible'] = 'Visible';
$aLanguage['en']['album_id'] = 'ID';
$aLanguage['en']['priority'] = 'Sort';
$aLanguage['en']['profile_id'] = 'Profile ID';
$aLanguage['en']['section_id'] = 'Section ID';
$aLanguage['en']['description'] = 'Description';
$aLanguage['en']['creation_date'] = 'Creation date';
$aLanguage['en']['albums'] = ' Albums';
$aLanguage['en']['album'] = ' Album';
$aLanguage['en']['openAlbum'] = ' Photos only';
$aLanguage['en']['galleryNoAlbums'] ='No albums';

// Formats
$aLanguage['en']['formats_title'] = 'Format title';
$aLanguage['en']['images_alt_title'] = 'Alternative title';
$aLanguage['en']['formats_name'] = 'Name';
$aLanguage['en']['formats_width'] = 'Width(px)';
$aLanguage['en']['formats_height'] = 'Height(px)';
$aLanguage['en']['formats_active'] = 'Active';
$aLanguage['en']['formats_format_id'] = 'ID';
$aLanguage['en']['formats_resize_on_larger_side'] = 'Resize on larger side';
$aLanguage['en']['formats_scale_and_crop'] = 'Scale and crop';
$aLanguage['en']['formats_use_watermark'] = 'Use watermark';
$aLanguage['en']['formats_watermark'] = 'Watermark text';
$aLanguage['en']['formats_profile_id'] = 'Profile ID';
$aLanguage['en']['formats_watermark_align'] = 'Watermark align';
$aLanguage['en']['formats_priority'] = 'Sort';

// Images
$aLanguage['en']['images_title'] = 'Image title';
$aLanguage['en']['image'] = 'Image';
$aLanguage['en']['images_source'] = 'Original image';
$aLanguage['en']['images_visible'] = 'Visible';
$aLanguage['en']['images_priority'] = 'Sort';
$aLanguage['en']['images_image_id'] = 'Image ID';
$aLanguage['en']['images_album_id'] = 'Album ID';
$aLanguage['en']['images_thumbnail'] = 'Thumbnail';
$aLanguage['en']['images_images_data'] = 'An array of image list on formats';
$aLanguage['en']['images_description'] = 'Description';
$aLanguage['en']['images_creation_date'] = 'Creation date';

$aLanguage['en']['galleryActive'] = 'Activity';
$aLanguage['en']['galleryDelImg'] = 'Delete image?';
$aLanguage['en']['galleryMultiDelImg'] = 'Delete images';
$aLanguage['en']['galleryDeleteConfirm'] = 'Delete confirm';
$aLanguage['en']['galleryNoImages'] ='No images in album';
$aLanguage['en']['galleryNoItems'] ='No elements selected for deleteng';

$aLanguage['en']['galleryDeleteAlbum']  = 'Delete album?';
$aLanguage['en']['galleryDeleteAlbums'] = 'Delete albums';
$aLanguage['en']['galleryDeleteMeasure']= 'pcs';
$aLanguage['en']['galleryUploadingImage']= 'Uploading image';

//Profiles
$aLanguage['en']['profiles_title'] = 'Profile title';
$aLanguage['en']['profiles_active'] = 'Show in the lists of formats';
$aLanguage['en']['profiles_profile_id'] = 'ID';

//Module
$aLanguage['en']['module_addUpdAlbum'] = 'Edit';
$aLanguage['en']['module_getAlbums'] = 'Back';
$aLanguage['en']['module_loadImage'] = 'Load';
$aLanguage['en']['module_images'] = 'Images';
$aLanguage['en']['module_showInAlbum'] = 'Show in album';
$aLanguage['en']['module_title'] = 'Title';
$aLanguage['en']['module_alt_title'] = 'Alternative title';
$aLanguage['en']['module_description'] = 'Description';
$aLanguage['en']['module_creation_date'] = 'Creation date';
$aLanguage['en']['module_edit'] = 'Edit';
$aLanguage['en']['module_add'] = 'Add/Edit album';
$aLanguage['en']['module_load'] = 'Load image';
$aLanguage['en']['module_load_subtext'] = 'You can choose the size of the framing pictures, pulling the frame photos';
$aLanguage['en']['module_editImage'] ='Edit image';

//tools
$aLanguage['en']['tools_profileImageSettings'] ='Image settings';
$aLanguage['en']['tools_addProfile'] ='Add profile settings';
$aLanguage['en']['tools_editProfile'] ='Edit profile settings';
$aLanguage['en']['tools_formats'] ='Formats';
$aLanguage['en']['tools_formatsImage'] ='Image formats';
$aLanguage['en']['tools_backToProfile'] ='Back to profile';
$aLanguage['en']['tools_addFormat'] ='Add images format';
$aLanguage['en']['tools_editFormat'] ='Edit images format';
$aLanguage['en']['tools_saveFormatMessage'] ='Format is saved';



$aLanguage ['en'] ['albumError'] = ' Error : The album is not defined ';
$aLanguage ['en'] ['loadNotice'] = ' You can upload a photo does not exceed {0, number} MB on volume , {1, number} pixels at most of the parties , and only the following formats : {2}.';
$aLanguage ['en'] ['noImageError'] = ' Error: Image not found ';
$aLanguage ['en'] ['badImageError'] = ' Error: The picture is damaged ';
$aLanguage ['en'] ['badProfileError'] = ' Error : Apparently , the profile for this album has been deleted or does not exist ';
$aLanguage ['en'] ['noSaveImage'] = ' Error: The image is not saved ';
$aLanguage ['en'] ['noDeleteImage'] = ' Error: The image is not removed ';
$aLanguage ['en'] ['deleteImage'] = ' Delete photo ';
$aLanguage ['en'] ['deleteImagesPro'] = ' Deleted images : {0, number} of {1, number}';
$aLanguage ['en'] ['photoId'] = 'id picture ';
$aLanguage ['en'] ['loadDataError'] = ' Error input ';
$aLanguage ['en'] ['noAlbumError'] = ' Album is not defined ';
$aLanguage ['en'] ['badData'] = ' Invalid data ';
$aLanguage ['en'] ['editImageTab'] = ' Change Image ';
$aLanguage ['en'] ['badFormat'] = ' Error: the format is not true image ';
$aLanguage ['en'] ['noDataToSave'] = ' Nothing to save the photo. ';
$aLanguage ['en'] ['noLoadImage'] = ' No image was uploaded ';
$aLanguage ['en'] ['noSaveError'] = ' Image is not saved ';
$aLanguage ['en'] ['saveImageNotice'] = ' Save Image ';
$aLanguage ['en'] ['noUploadData'] = ' Error: No data was sent ';
$aLanguage ['en'] ['saveAlbumReport'] = ' Save the album' ;
$aLanguage ['en'] ['deleteAlbumReport'] = ' Delete the book ';
$aLanguage ['en'] ['deleteAlbumName'] = ' Album deleted :';
$aLanguage ['en'] ['noSort'] = ' Do not set the parameters for sorting ';
$aLanguage ['en'] ['deleteAlbumsPro'] = ' Deleted Album : {0, number} of {0, number}';
$aLanguage ['en'] ['albumDeleting'] = ' Delete Album ';



return $aLanguage;