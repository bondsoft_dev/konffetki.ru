<?php

namespace skewer\build\Adm\Gallery;

use skewer\build\Component\Gallery;

/**
 * API работы с админской частью галереи
 */
class Api {

    /**
     * id формата для копа
     */
    const sCropFormatName = 'crop_min';

    /**
     * Высота элемента для кропа
     */
    const cropHeight = 400;

    /**
     * Список ошибок
     * @var array
     */
    private static $aErrorUploadList = array();

    /**
     * @return array
     */
    public static function getErrorUploadList(){
        return static::$aErrorUploadList;
    }

    /**
     * Загружает изображения и перемещает в целевую директорию
     * @static
     * @param $iSectionId
     * @param $prefix
     * @return bool|string
     * @throws \Exception
     */
    public static function uploadFile( $iSectionId, $prefix = '' ) {

        // параметры загружаемого файла
        $aFilter = array();
        $aFilter['size']            = \Yii::$app->params['upload']['maxsize'];
        $aFilter['allowExtensions'] = \Yii::$app->params['upload']['allow']['images'];
        $aFilter['imgMaxWidth']     = \Yii::$app->params['upload']['images']['maxWidth'];
        $aFilter['imgMaxHeight']    = \Yii::$app->params['upload']['images']['maxHeight'];

        $aValues = \skewer\build\Adm\Files\Api::getLanguage4Uploader();
        \skUploadedFiles::loadErrorMessages($aValues);

        // загрузка
        $oFiles = \skUploadedFiles::get($aFilter, FILEPATH.$prefix, PRIVATE_FILEPATH);

        // если ни один файл не загружен - выйти
        if (!$oFiles->count())
            throw new \Exception ($oFiles->getError());

        /*
         * пока берется только один файл, но делался задел на загрузку
         * большего количества
         */

        // имя файла исходника
        $sSourceFN = array();

        $aErrorMessages = array();

        /** @noinspection PhpUnusedLocalVariableInspection */
        foreach ($oFiles as $file) {

            try{
                // проверка ошибок
                if($sError = $oFiles->getError())
                    throw new \Exception ($sError);

                /* @todo Должна быть добавлена проверка на защиту директории (protected: true/false) */
                $sNewFile = $oFiles->UploadToSection($prefix.$iSectionId, 'gallery/sources/', false, false, true);

                // отрезать корневую папку от пути
                $sNewFile = substr( $sNewFile, strlen(WEBPATH)-1 );
                $sSourceFN[$file['name']] = $sNewFile;
                
            } catch (\Exception $e) {

                $aErrorMessages[] = $e->getMessage();
            }

        }

        static::$aErrorUploadList = $aErrorMessages;

        // проверка наличия имени файла
        if( !count($sSourceFN) )
            throw new \Exception( \Yii::t('gallery', 'noLoadImage') );

        reset($sSourceFN);
        return ( count($sSourceFN) == 1 && !count($aErrorMessages)) ? $sSourceFN[key($sSourceFN)] : $sSourceFN;
    }

    /**
     * Создает миниатюру для режима обрезки
     * @static
     * @param string $sSourceFN имя исходного файла
     * @param int $iSectionId if раздела
     * @param $prefix
     * @throws \Exception
     * @return array
     */
    public static function createCropMin( $sSourceFN, $iSectionId, $prefix = '' ) {

        $bProtected = false;
        $sSourceFN = WEBPATH.$sSourceFN;

        /* Путь к корневой директории галереи в текущем разделе */
        $sImagePath = \skFiles::getFilePath( $prefix.$iSectionId, Gallery\Photo::$sModuleDirName );

        /* Загрузка исходного изображения для дальнейшей обработки */
        $aValues = \skewer\build\Adm\Files\Api::getLanguage4Image();
        \skImage::loadErrorMessages($aValues);
        $oImage = new \skImage();

        // загрузить изображение
        if(!$oImage->load($sSourceFN))
            throw new \Exception('Crop. Image processing error: Image not loaded!');

        // привести высоту к приемлемому размеру
        $iHeight = min( $oImage->getSrcHeight(), self::cropHeight );

        // изменить размер
        $oImage->resize(0, $iHeight);

        $sSavedFile = \skFiles::generateUniqFileName($sImagePath.self::sCropFormatName.DIRECTORY_SEPARATOR, basename($sSourceFN));

        $sSavedFile = str_replace(\skFiles::getRootUploadPath($bProtected), '', $sSavedFile);

        $sDir = \skFiles::createFolderPath(dirname($sSavedFile), $bProtected);

        if(!$sDir) throw new \Exception('Crop. Image processing error: Directory is not created!');

        $sNewFilePath = $sDir.basename($sSavedFile);

        /* Сохранить измененное изображение */
        if(!$oImage->save($sNewFilePath)) throw new \Exception('Crop. Image processing error: Image do not saved!');

        list($iWidth, $iHeight) = $oImage->getSize();
        return array(
            'file'   => \skFiles::getWebPath($sNewFilePath, false),
            'width'  => $iWidth,
            'height' => $iHeight,
            'koef'   => $oImage->getSrcHeight()/self::cropHeight
        );

    }

    /**
     * Заменяем миниатюры в изображении $iImageId
     * todo В этом методе наверняка есть лишний код
     * @param $iImageId
     * @param $mProfile
     * @param $mThumbnailPath
     * @return bool|int
     */
    public static function replaceImageReCropFormat($iImageId, $mProfile, $mThumbnailPath = false) {

        if (!$aImage = Gallery\Photo::getImage($iImageId)) return false;

        $aImagesData = json_decode($aImage['images_data'], true);

        foreach($mProfile as $sFormatName=>$aFormat) {

            // удаление старых(земененных) изображений
            \skFiles::remove(WEBPATH.$aImagesData[$sFormatName]['file']);

            // замена миниатюры на новую
            $aImagesData[$sFormatName] = $aFormat;
        }

        $aData['images_data'] = json_encode($aImagesData);
        // изменяем миниатюры для админки
        if( $mThumbnailPath ){
            /**
             * удаление старой миниатюры
             */
            \skFiles::remove(ROOTPATH.$aImage['thumbnail']);
            $aData['thumbnail']   = $mThumbnailPath;
        }

        return Gallery\Photo::setImage($aData, $iImageId);
    }

    /**
     * Валидирует набор id для альбома
     * todo Нужен ли этот метод вообще?
     * @static
     * @param $aIdList
     * @param $iAlbumId
     * @return array
     */
    public static function validateIdList($aIdList, $iAlbumId) {

        // набор приведенных к типу id на удаление
        $aIntIdList = array();
        foreach ($aIdList as $mId)
            if ($iId = (int)$mId) $aIntIdList[] = $iId;

        // набор точно присутствующих в альбоме id
        $aValidIdList = array();

        // изображения в альбоме
        if ( $aAlbumItems = Gallery\Photo::getFromAlbum($iAlbumId, false) )
            foreach ($aAlbumItems as $aItem)
                if ( in_array($aItem['id'], $aIntIdList) )
                    $aValidIdList[] = $aItem['id'];

        return $aValidIdList;
    }

    /**
     * Валидирует набор id альбомов для раздела
     * @static
     * @param array $aIdList
     * @param int $iSectionId
     * @return array
     */
    public static function validateIdAlbumsList( $aIdList, $iSectionId ) {

        // набор приведенных к типу id на удаление
        $aIntIdList = array();
        foreach ($aIdList as $mId)
            if ($iId = (int)$mId) $aIntIdList[] = $iId;

        // набор точно присутствующих в разделе id альбомов
        $aValidIdList = array();

        if ($aAlbumItems = Gallery\Album::getBySection($iSectionId, false)) // альбомы в разделе
            foreach ($aAlbumItems as $aItem)
                if ( in_array($aItem['id'], $aIntIdList) )
                    $aValidIdList[] = $aItem['id'];

        return $aValidIdList;
    }
}