<?php

namespace skewer\build\Adm\Gallery;

use skewer\build\Component\Gallery;

class Service extends \ServicePrototype {

    /** Чистка исходных изображений фотогалерей. Используется в кроне.
     */
    public static function findOldSourceImages() {

        $aImages = Gallery\Photo::getOlderPhotoWithSourse();

        // Удаление исходного файла фотографии, если прошла неделя
        foreach ($aImages as $aImage)
            if ($aImage['source']) {

                // delete source file
                if (file_exists(WEBPATH . $aImage['source'])) unlink(WEBPATH . $aImage['source']);

                // update DB row
                $aImage['source'] = '';
                Gallery\Photo::setImage($aImage, $aImage['id']);
            }

        return true;
    }
}