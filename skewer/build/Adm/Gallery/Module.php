<?php

namespace skewer\build\Adm\Gallery;


use skewer\build\Component\Section\Parameters;
use skewer\build\Component\UI;
use skewer\build\Component\SEO;
use skewer\build\Adm;
use skewer\build\Component\Gallery;
use yii\base\UserException;

/**
 * Система администрирования для модуля фотогаллереи
 */
class Module extends Adm\Tree\ModulePrototype {

    /**
     * Id текущего раздела
     * @var int
     */
    protected $iSectionId = 0;

    /**
     * Количество записей на страницу
     * @var int
     */
    protected $iOnPage = 10;

    /**
     * Массив полей, выводимых колонками в списке альбомов
     * @var array
     */
    protected $aAlbumsListFields = array('id', 'title', 'visible');

    /**
     * Номер текущей страницы
     * @var int
     */
    protected $iPage = 0;

    /**
     * Id текущего альбома
     * @var int
     */
    protected $iCurrentAlbumId = 0;

    /**
     * Максимально допустимый размер для загружаемых изображений
     * @var int
     */
    protected $iMaxUploadSize = 0;

    /**
     * Название, присваиваемое загруженному изображению по-умолчанию
     * @var string
     */
    protected $sDefaultImageTitle = 'Gallery.image';

    /*
     * Массив данных для ресайза
     * заполняется при загрузке файла
     */
    protected $aUploadedData = array();

    /**
     * Префикс, добавляемый к числовым алиасам альбомов
     * @var string
     */
    protected $sAlbumAliasPrefix = 'album-';

    /** @var bool Выводить только интерфейс редактированья фотографий */
    protected $onlyAlbumEditor = false;

    /** @var bool Создавать альбом */
    protected $createAlbum = false;

    /** @var bool Флаг всплывающего окна */
    protected $popup = false;

    /* Methods */

    /**
     * Иницализация
     */
    protected function preExecute() {

        /* текущая страница постраничного */
        $this->iPage = $this->getInt('page');

        /* Id текущего раздела */
        $this->iSectionId = $this->getInt('sectionId', $this->getEnvParam('sectionId'));

        /* Максимально допустимый размер для загружаемых изображений */
        $this->iMaxUploadSize = \Yii::$app->params['upload']['maxsize'];

        /* Восстанавливаем текущий альбом (не нужно перекрывать если задан в подпроцессе) */
        if ( !$this->iCurrentAlbumId )
            $this->iCurrentAlbumId = $this->getInt('currentAlbumId');

        if ( !$this->iSectionId && $this->iCurrentAlbumId )
            $this->iSectionId = $this->iCurrentAlbumId;

        //if ( !CurrentAdmin::canRead($this->iSectionId) ) throw new ModuleAdminErrorException( 'accessDenied' );

    }// func

    /**
     * Установка служебных данных
     * @param UI\State\BaseInterface $oIface
     */
    protected function setServiceData( UI\State\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData( array(
            'sectionId' => $this->iSectionId,
            'page' => $this->iPage,
            'currentAlbumId' => $this->iCurrentAlbumId,
        ) );

    }// func

    /**
     * Вызывается в случае отсутствия явного обработчика
     * @return int
     */
    protected function actionInit() {

        $this->setModuleLangValues(array(
                'galleryNoAlbums',
                'galleryNoImages',
                'galleryNoItems',
                'galleryDeleteAlbum',
                'galleryDeleteAlbums',
                'galleryDeleteMeasure',
                'galleryDeleteConfirm',
                'galleryUploadingImage',
            )
        );

        if ( $this->onlyAlbumEditor && $this->iCurrentAlbumId )
            return $this->actionShowAlbum();
        elseif ( $this->onlyAlbumEditor && $this->createAlbum )
            return $this->actionNonAlbum();
        else
            return $this->actionGetAlbums();

    }

    /**
     * запустить сборщик мусора
     */
    protected function startScavenger() {

        // запросить
        $aRows = TmpModule::getOldRows();
        if ( !$aRows )
            return;

        // набор id для удаления
        $aIdList = array();

        // перебрать все записи
        foreach ( $aRows as $aRow ) {
            // id в список удаления
            $aIdList[] = $aRow['id'];

            // проверить наличие записей с откатом разделов
            if ( strpos($aRow['value'], '..') )
                continue;

            // удаление файла
            if( is_file( WEBPATH.$aRow['value'] ) )
                unlink(WEBPATH.$aRow['value']);

        }

        // удалние записей по списку
        TmpModule::delById( $aIdList );

    }

    /**
     * Возвращает список альбомов
     * @throws \Exception
     * @return int
     */
    protected function actionGetAlbums() {

        /* список альбомов текущего раздела */
        $this->iCurrentAlbumId = 0;
        $this->setPanelName(\Yii::t('gallery', 'albums'),true);

        /* Строим список альбомов */
        $oList = new \ExtUserFile( 'PhotoAlbumList' );

        if(!$this->iSectionId)
            $this->iSectionId = $this->getEnvParam('sectionId');

        /* Выбираем данные для списка */
        $aAlbums = Gallery\Album::getBySection($this->iSectionId, false);
        $aAlbums = Gallery\Album::setCountsAndPreview($aAlbums);

        foreach ($aAlbums as &$aAlbum) {
            $aAlbum['url']      = (isset($aAlbum['album_img']) && $aAlbum['album_img']) ? $aAlbum['album_img'] : $this->getModuleWebDir().'/img/no_photo.png';
            $aAlbum['name']     = $aAlbum['title'];
            $aAlbum['lastmod']  = $aAlbum['creation_date'];
            $aAlbum['active']   = $aAlbum['visible'] ? 'checked' : '';
            $aAlbum['size']     = 0;
        }// each picture

        /* Записываем данные на отправку */
        $this->setData('albums', $aAlbums);
        /* Добавление библиотек для работы */
        $this->addLibClass('PhotoSorter');
        $this->addLibClass('PhotoAlbumListView');

        $oList->addExtButton( \ExtDockedAddBtn::create()
            ->setAction('addUpdAlbum')
        );
        $oList->addBtnSeparator('->');
        $oList->addExtButton( \ExtDockedDelBtn::create()
            ->setAction('')
            ->setState('del_selected')
        );

        /* Добавляем css файл для */
        $this->addCssFile('gallery.css');

        /* php событие при клике */
        $this->setData('clickAction', 'showAlbum');

        $this->setCmd('show_albums_list');

        $this->setInterface($oList);

        return psComplete;
    }// func

    /**
     * Изменение видимости фотографии
     */
    public function actionAlbumActiveChange() {
        Gallery\Album::toggleActiveAlbum( $this->get('data') );
    }

    /**
     * Описание альбома, список изображений в нем.
     * @param array $aShowData
     * @return int
     * @throws \Exception
     */
    protected function actionShowAlbum($aShowData = []) {

        /** Сообщения */
        if (count($aShowData)) {

            $time = 2000 + count($aShowData['errors'])*2000;

                if ( $aShowData['iUpload'] ){
                    $sMsg = \Yii::t('Files', 'loadingPro', [$aShowData['iUpload'], $aShowData['iCount']] );
                    if (count($aShowData['errors'])){
                        $sMsg .= "<br>" . implode('<br>', $aShowData['errors']);
                    }

                    $this->addMessage( $sMsg , '', $time);
                }
                else{
                    $sMsg = \Yii::t('files', 'noLoaded', [$aShowData['iUpload'], $aShowData['iCount']] );
                    if (count($aShowData['errors'])){
                        $sMsg .= "<br>" . implode('<br>', $aShowData['errors']);
                    }
                    $this->addError($sMsg);
                }

        }

        // очистить контейнер загруки
        //$this->clearUploadedDataWithSrc();
        $this->clearUploadedData();

        /* Строим список изображений */
        $oList = new \ExtUserFile('PhotoList');

        $aData = $this->get('data');

        if ($this->iCurrentAlbumId)
            $aData['id'] = $this->iCurrentAlbumId;

        if (!(int)$iAlbumId = $aData['id']) throw new \Exception(\Yii::t('gallery', 'albumError'));

        $aAlbum = Gallery\Album::getById($iAlbumId);

        if (!$aAlbum)
            throw new \Exception('Альбом не найден');

        $this->iCurrentAlbumId = $iAlbumId;

        /* Устанавливаем название вкладки  */
        $this->setPanelName(sprintf(\Yii::t('gallery', 'album').' "%s" [#%s]',$aAlbum['title'],$iAlbumId),true);

        /* Выбираем данные для списка */
        $aItems = Gallery\Photo::getFromAlbum($iAlbumId);

        $aImages = array();

        if($aItems)
            foreach ($aItems as $aImage) {
                $aImages[] = array(
                    'url'      => $aImage['thumbnail'],
                    'name'     => $aImage['title'],
                    'size'     => 0,
                    'lastmod'  => $aImage['creation_date'],
                    'id'       => $aImage['id'],
                    'album_id' => $aImage['album_id'],
                    'active'   => $aImage['visible'] ? 'checked' : ''
                );
            }// each picture

        /* Записываем данные на отправку */
        $this->setData('images',$aImages);

        /* Добавление библиотек для работы */
        $this->addLibClass('PhotoSorter');
        $this->addLibClass('PhotoAddField');
        $this->addLibClass('PhotoListView');

        // дополнительный текст для списка
        $this->setData('addText',
            sprintf(\Yii::t('gallery', 'loadNotice'),
                \skFiles::getMaxUploadSize()/1024/1024,
                \skImage::getMaxLineSize(),
                implode(', ',\skImage::getAllowImageTypes())
            )
        );

        if (!$this->onlyAlbumEditor) {

            $oList->addDockedItem(array(
                'text' => \Yii::t('gallery', 'module_addUpdAlbum'),
                'iconCls' => 'icon-edit',
                'state' => 'init',
                'action' => 'addUpdAlbum',
            ));
            $oList->addDockedItem(array(
                'text' => \Yii::t('gallery', 'module_getAlbums'),
                'iconCls' => 'icon-cancel',
                'state' => 'init',
                'action' => 'getAlbums',
            ));
            $oList->addBtnSeparator('');
        }

        if ($this->popup){
            // кнопка сохранения (закрывает окно)
            $oList->addBtnSave('', 'closeWindow');
        }

        $oList->setModuleLangValues([
            'galleryActive',
            'galleryDelImg',
            'galleryNoItems',
            'galleryDeleteConfirm',
            'galleryMultiDelImg',
            'galleryNoImages',
            'galleryUploadingImage'
        ]);

        // кнопка загрузки
        $oList->addExtButton( \ExtDockedByUserFile::create(\Yii::t('gallery', 'module_loadImage'),'PhotoAddField')
                ->setIconCls( \ExtDocked::iconAdd )
        );

        // кнопка удаления
        $oList->addBtnSeparator('->');
        $oList->addBtnDelete('', 'del_selected');

        /* Добавляем css файл для */
        $this->addCssFile('gallery.css');

        /* php событие при клике */
        $this->setData('clickAction','showImage');

        $this->setCmd('show_photos_list');

        $this->setInterface( $oList );

        return psComplete;
    }// func

    protected  function actionShowImage() {

        $aData = $this->get('data');

        if(!$iImageId = $aData['id']) throw new \Exception(\Yii::t('gallery', 'noImageError'));

        /* Получить изображение */
        $aImage = Gallery\Photo::getImage($iImageId);
        if(!$aImage) throw new \Exception(\Yii::t('gallery', 'noImageError'));

        $this->iCurrentAlbumId = $aImage['album_id'];

        if(!$aImage['images_data'] = json_decode($aImage['images_data'], true))
            throw new \Exception(\Yii::t('gallery', 'badImageError'));

        /* Получить Набор форматов для изображения */
        $iProfileId = Gallery\Album::getProfileId($this->iCurrentAlbumId);
        $aFormats   = Gallery\Format::getByProfile($iProfileId);

        if(!$aFormats) throw new \Exception(\Yii::t('gallery', 'badProfileError'));

        /* Собираем массив данных по изображению */
        $aTabs = [];
        $i = 0;
        foreach ($aFormats as $aFormat) {
            if ( isSet($aImage['images_data'][$aFormat['name']]) && $aFormat['active'] ) {
                ++$i;
                $aImageItem['src']    = $aImage['images_data'][$aFormat['name']]['file'];
                $aImageItem['name']   = $aFormat['name'];
                $aImageItem['title']  = ($aFormat['title'])? $aFormat['title']: "Размер ($i)";
                $aImageItem['width']  = ($aImage['images_data'][$aFormat['name']]['width'])?  $aImage['images_data'][$aFormat['name']]['width']: '*';
                $aImageItem['height'] = ($aImage['images_data'][$aFormat['name']]['height'])? $aImage['images_data'][$aFormat['name']]['height']: '*';
                $aTabs[] = $aImageItem;
            }
        }
        // дополнительная библиотека для отображения
        $this->addLibClass('PhotoImg');

        /* Подключить автоматический генератор форм */
        $oFormBuilder = UI\StateBuilder::newEdit();

        /* Установить заголовок панели */
        if(!empty($aImage['title']))
            $this->setPanelName(sprintf(\Yii::t('gallery', 'module_editImage').' "%s"',$aImage['title']), true);
        else
            $this->setPanelName(\Yii::t('gallery', 'module_editImage'), true);

        /** @todo Добавить проверку на наличие фоток согласно формату (Не удалять )*/

        $oFormBuilder
            ->addSpecField('formats', \Yii::t('gallery', 'module_images'), 'PhotoShowFormatsField', $aTabs)
            ->field('visible', \Yii::t('gallery', 'module_showInAlbum'), 'i', 'check')
            ->fieldString('title', \Yii::t('gallery', 'module_title'))
            ->fieldString('alt_title', \Yii::t('gallery', 'module_alt_title'))
            ->field('description', \Yii::t('gallery', 'module_description'), 's', 'text')
            ->fieldString('creation_date', \Yii::t('gallery', 'module_creation_date'), ['disabled' => true])
            ->fieldHide('id', '');

        /* Установить значения для элементов */
        $oFormBuilder->setValue($aImage);

        /* Кнопки боковой панели */
        $oFormBuilder->addButtonSave('updateImage');
        $oFormBuilder->addButtonCancel('showAlbum');
        $oFormBuilder->buttonSeparator('-');

        $oFormBuilder->getForm()->addDockedItem([
            'text' => 'Re-crop',
            'iconCls' => 'icon-edit',
            'state' => 'init',
            'action' => 'reCropForm'
        ]);

        // кнопка загрузки новой миниатюры
        $oFormBuilder->addExtButton( \ExtDockedByUserFile::create(\Yii::t('gallery', 'module_edit'),'PhotoAddToFormatField')
                ->setIconCls( \ExtDocked::iconEdit )
                ->setAddParam('imageId',$iImageId)
        );

        $oFormBuilder->buttonSeparator('->');
        $oFormBuilder->getForm()->addBtnDelete('deleteImage');

        /* Построить интерфейс */
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    } // func

    /**
     * Обновляет заголовочные данные изображения
     * @return int
     * @throws \Exception
     */
    protected function actionUpdateImage() {

        $aData = $this->get( 'data' );

        if(!count($aData)) throw new \Exception (\Yii::t('gallery', 'noSaveImage'));
        if(!(int)$iImageId = $aData['id']) throw new \Exception (\Yii::t('gallery', 'noSaveImage'));

        /* Обновляем данные изображения */
        Gallery\Photo::setImage([
            'title'       => $aData['title'],
            'visible'     => ($aData['visible']) ? 1 : 0,
            'description' => $aData['description'],
            'alt_title'   => $aData['alt_title'],
        ], $iImageId);

        /* вывод списка */
        return $this->actionShowAlbum();

    }// func

    protected function actionDeleteImage() {

        $aData = $this->get( 'data' );

        if(!count($aData)) throw new \Exception (\Yii::t('gallery', 'noDeleteImage'));
        if(!(int)$iImageId = $aData['id']) throw new \Exception (\Yii::t('gallery', 'noDeleteImage'));

        /* Обновляем данные изображения */

        $mError = '';
        $bRes = Gallery\Photo::removeImage($iImageId, $mError);

        if(!$bRes) throw new \Exception ($mError);

        $this->addNoticeReport(\Yii::t('gallery', 'deleteImage'), \Yii::t('gallery', 'photoId')." $iImageId", \skLogger::logUsers, "Adm\\Gallery\\Module");

        return $this->actionShowAlbum();

    }// func

    /**
     * Групповое удаление изображений
     */
    protected function actionGroupDel() {

        // набор входных данных для удаления
        $aInList = $this->get( 'delItems' );

        // проверить принадлежность целевому альбому
        $aDelList = Api::validateIdList( $aInList, $this->iCurrentAlbumId );

        // удалить по списку
        $iCnt = 0;

        foreach ($aDelList as $iId) {
            if ( Gallery\Photo::removeImage($iId) )
                $iCnt++;
        }

        $this->addMessage( \Yii::t('gallery', 'deleteImagesPro', [$iCnt, count($aInList)]) );
        $this->addNoticeReport(\Yii::t('gallery', 'deleteImage'), "<pre>".print_r($aDelList, true)."</pre>", \skLogger::logUsers, "Adm\\Gallery\\Module");
        $this->actionShowAlbum();
    }

    /**
     * Изменение видимости фотографии
     */
    public function actionPhotoActiveChange() {

        $iPhotoId = $this->get('data');
        Gallery\Photo::toggleActivePhoto($iPhotoId);
    }

    /**
     * Загрузка нового изображения для определенного формата
     */
    protected function actionLoadNewImageForFormat(){

        $this->setPanelName(\Yii::t('gallery', 'module_loadImage'), true);

        // Обработка входных данных
        $sFormat = $this->get('formatName');
        $iImageId = $this->get('imageId');

        if( !$sFormat || !$iImageId )
            throw new \Exception(\Yii::t('gallery', 'loadDataError'));

        if(!$iAlbumId = (int)$this->iCurrentAlbumId) throw new \Exception(\Yii::t('gallery', 'noAlbumError'));

        $iProfileId = Gallery\Album::getProfileId($iAlbumId);

        if(!$iProfileId) throw new \Exception(\Yii::t('gallery', 'badData'));

        // Загрузка изображений, перемещение в целевую директорию
        if ($iProfileId == 7) $sSourceFN = Api::uploadFile($this->iSectionId, 'catalog/');
        else $sSourceFN = Api::uploadFile($this->iSectionId);

        $this->actionReCropForm($iImageId, $sFormat, $sSourceFN);
    }

    /**
     * Изменений кропинга для определенного формата изображения
     */
    protected function actionReCropForm($iImageId = 0, $sFormatName = '', $sSourceFN = '') {

        // Обработка входных данных
        $aData = $this->get('data');

        $sFormat = isSet($aData['selectedFormat']) ? $aData['selectedFormat'] : $sFormatName;
        $iImageId = isSet($aData['id']) ? $aData['id'] : $iImageId;
        if (!$sFormat OR !$iImageId)
            throw new \Exception(\Yii::t('gallery', 'loadDataError'));

        $this->setPanelName(\Yii::t('gallery', 'editImageTab'), true);

        // получение информации о текущем формате
        $aFormat = Gallery\Format::getByName($sFormat);

        if (!$aFormat)
            throw new \Exception(\Yii::t('gallery', 'badFormat'));

        // получение исходного изображения
        if (!$sSourceFN) {
            $aImage = Gallery\Photo::getImage($iImageId);
            if ( $aImage['source'] AND file_exists(WEBPATH.trim($aImage['source'], '/')) ) {
                $sSourceFN = $aImage['source'];
            }else{
                //todo здесь можно переделать согласно новой модели Photo
                //попытаемся вытащить больший формат
                $aImagesData = json_decode($aImage['images_data'], true);
                if (isset($aImagesData['big'])){
                    $sSourceFN = $aImagesData['big']['file'];
                }else if (isset($aImagesData['med'])){
                    $sSourceFN = $aImagesData['med']['file'];
                }else if (isset($aImagesData['colmax'])){
                    $sSourceFN = $aImagesData['colmax']['file'];
                }
            }
        }

        if (!$sSourceFN){
            throw new UserException(\Yii::t('gallery', 'noImageError'));
        }

        // создать миниатюру
        $aCropMin = Api::createCropMin($sSourceFN, $this->iSectionId);

        // добавить в сессию запись о загруженном файле и о миниатюре и получить ключ
        $sCropFN = $aCropMin['file'];
        $this->aUploadedData = [
            'crop' => $sCropFN,
            'source' => $sSourceFN,
            'crop_id' => TmpModule::create('crop', $sCropFN),
            'source_id' => TmpModule::create('source', $sSourceFN)
        ];

        // данные о миниатюре для отображения кроп интерфейса
        $this->setData('cropData', $aCropMin);
        $this->setData('formatsData', $aFormat);

        // Сборка интерфейса

        // Подключить автоматический генератор форм
        $oFormBuilder = UI\StateBuilder::newEdit();

        // Установить набор элементов формы

        // Собираем массив данных по изображению
        $aTabs['name']  = 'formats';
        $aTabs['title'] = \Yii::t('gallery', 'module_images');
        $aTabs['value'] = ['cropData' => $aCropMin, 'formatsData' => $aFormat];
        $aTabs['cropData'] = $aCropMin;
        $aTabs['formatsData'] = $aFormat;
        $aTabs['subtext'] = \Yii::t('gallery', 'module_load_subtext');

        $this->addLibClass('PhotoResizer');

        $oFormBuilder
            ->addSpecField('formats', \Yii::t('gallery', 'module_images'), 'PhotoResizerList', $aTabs)
            ->fieldHide('id', '', 'i', ['value' => $iImageId]); // передача идентификатора изображения

        // Установить значения для элементов
        $oFormBuilder->setValue([]);

        // Кнопки боковой панели

        // добавить кнопку запуска обработки
        $oFormBuilder->addButtonSave('saveReCropImage');

        // и кнопку отмены (возврата к альбому)
        $oFormBuilder->addButtonCancel('showAlbum');

        $this->addJSFile('jquery.min.js');
        $this->addJSFile('jquery.Jcrop.min.js');
        $this->addCSSFile('jquery.Jcrop.min.css');

        // Построить интерфейс
        $this->setInterface($oFormBuilder->getForm());
    }

    /**
     * Сохранение перекроппинного изображения для определенного формата
     * @throws \Exception
     */
    protected function actionSaveReCropImage() {

        // id отображаемого альбома
        $iAlbumId = (int)$this->iCurrentAlbumId;
        if( !$iAlbumId )
            throw new \Exception(\Yii::t('gallery', 'noAlbumError'));

        // id профиля для альбома
        $iProfileId = Gallery\Album::getProfileId($iAlbumId);
        if(!$iProfileId)
            throw new \Exception(\Yii::t('gallery', 'badData'));

        // получить ключ кропа
        $aCrop = array();
        $aData = $this->get('data');
        if( is_array($aData) )
            foreach( $aData as $sKey=>$aVal ) {
                if(strpos($sKey,'cropData_') !== false)
                    $aCrop[substr($sKey,9)] = $aVal;
            }

        if ( !($iImageId = isSet($aData['id']) ? $aData['id'] : false) )
            throw new \Exception(\Yii::t('gallery', 'badData'));

        $aData = $this->aUploadedData;

        if ( !$aData or !count($aCrop) )
            throw new \Exception(\Yii::t('gallery', 'noDataToSave'));

        // запросить файл
        $sImagePath = $aData['source'];
        $sImageFullPath = WEBPATH.$sImagePath;

        /* Обработка изображения согласно профилю настроек, перемещение созданных файлов в целевые директории */
        if(!$sImagePath) throw new \Exception(\Yii::t('gallery', 'noLoadImage'));

        $sError = false;

        $mProfileId = [
            'crop' => $aCrop,
            'iProfileId' => $iProfileId
        ];
        $aUpdImage = Gallery\Photo::processImage($sImageFullPath, $mProfileId, $this->iSectionId, false, false, $sError, Api::cropHeight);

        if(!$aUpdImage OR $sError) throw new \Exception($sError);

        $sThumbnail   = (isSet($aUpdImage['thumbnail'])) ? $aUpdImage['thumbnail']: false;

        unSet($aUpdImage['thumbnail']);

        /* Сохранение измененных миниатюр в БД */
        $bReplaceImage = Api::replaceImageReCropFormat($iImageId, $aUpdImage, $sThumbnail);

        // очистить контейнер загруки
        $this->clearUploadedData();

        if(!$bReplaceImage) throw new \Exception(\Yii::t('gallery', 'noSaveError'));

        $this->set('data', ['id' => $iImageId]);

        $this->addNoticeReport(\Yii::t('gallery', 'saveImageNotice'), \Yii::t('gallery', 'photoId')." = $iImageId", \skLogger::logUsers, "Adm\\Gallery\\Module");

        $this->actionShowImage();
    }


    /**
     * Форма  Добавления / редактирования описания альбома
     * @return int
     * @throws \Exception
     */
    protected function actionAddUpdAlbum() {

        $oFormBuilder = UI\StateBuilder::newEdit();

        $this->setPanelName(\Yii::t('gallery', 'module_add'),true);
        $iAlbumId = false;

        try {
            if ($this->iCurrentAlbumId) $iAlbumId = $this->iCurrentAlbumId;

            /* Получаем данные формата или заготовку под новый формат */
            $aValues = $iAlbumId ? Gallery\Album::getById($iAlbumId) : Gallery\Album::getAlbumBlankValues();

            /* установить набор элементов формы */
            $oFormBuilder
                ->fieldHide('owner', \Yii::t('gallery', 'owner'), 's')
                ->fieldString('title', \Yii::t('gallery', 'title'))
                ->fieldString('alias', \Yii::t('gallery', 'alias'))
                ->field('visible', \Yii::t('gallery', 'visible'), 'i', 'check')
                ->fieldHide('id', \Yii::t('gallery', 'album_id'))
                ->fieldHide('priority', \Yii::t('gallery', 'priority'))
                ->fieldHide('profile_id', \Yii::t('gallery', 'profile_id'))
                ->fieldHide('section_id', \Yii::t('gallery', 'section_id'))
                ->field('description', \Yii::t('gallery', 'description'), 's', 'text');

            // Если не новый альбом
            if ($iAlbumId) $oFormBuilder->fieldString('creation_date', \Yii::t('gallery', 'creation_date'), ['disabled' => true]);

            /* Если профиль по-умолчанию не задан - ставим первый активный */
            if (!$aValues['profile_id']) $aValues['profile_id'] = Gallery\Profile::getFirstActiveProfileId();

            /* установить значения для элементов */
            $oFormBuilder->setValue($aValues);

            /* добавление кнопок */
            $oFormBuilder->addButtonSave('saveAlbum');

            // добавление SEO блока полей
            SEO\Api::appendExtForm($oFormBuilder->getForm(), 'gallery', $iAlbumId);

        } catch (\Exception $e) {
            $oFormBuilder->getForm()->addError($e->getMessage());
        }

        /* Если Создается новый альбом, то "canсel" ведет на список альбомов а не детальную конкретного  */
        if ($iAlbumId)
            $oFormBuilder->addButtonCancel('showAlbum');
        else
            $oFormBuilder->addButtonCancel('getAlbums');

        $oFormBuilder->getForm()->setModuleLangValues(['galleryUploadingImage']);

        // вывод данных в интерфейс
        $this->setInterface($oFormBuilder->getForm());
        return psComplete;
    }// func

    /**
     * Сохраняет Описание альбома
     * @throws \Exception
     */
    protected function actionSaveAlbum() {

        $aData = $this->get('data');

        if(!count($aData)) throw new \Exception (\Yii::t('gallery', 'noUploadData'));

        // добавляем алиас
        if(!isset($aData['alias']) || !$aData['alias']){
            $aData['alias'] = \skFiles::makeURLValidName($aData['title'],false);
        } else {
            $aData['alias'] = \skFiles::makeURLValidName($aData['alias'],false);

            /* Если передали в алиасе число - добавляем префикс */
            $mAlias = $aData['alias'];
            if((int)$mAlias)
                $aData['alias'] = $this->sAlbumAliasPrefix.$aData['alias'];
        }


        // проверка на дубли алиса
        $sAlias = $aData['alias'];
        $iNum = 0;
        do {
            $bFlag = true;
            $sCurAlias = $sAlias;
            if ($iNum) $sCurAlias .= $iNum;

            if (!$aAlbum = Gallery\Album::getByAlias($sCurAlias, $this->iSectionId)) {
                $aData['alias'] = $sCurAlias;
                $bFlag = false;
            } else {
                if ( isSet($aData['id']) && $aAlbum['id'] == $aData['id'] ) {
                    $aData['alias'] = $sCurAlias;
                    $bFlag = false;
                }
            }
            $iNum++;
            if($iNum > 300) $bFlag = false;
        } while( $bFlag );


        $iAlbumId = ($aData['id'])? $aData['id']: false;

        /* Установка дополнительных значений */
        $aData['owner'] = 'section';              // владельца
        $aData['section_id'] = $this->iSectionId; // родительского раздела

        /* Добавляем либо обнавляем данные альбома */
        $iNewId = Gallery\Album::setAlbum($aData, $iAlbumId);

        // сохранение SEO данных
        SEO\Api::saveJSData( 'gallery', $iNewId, $aData );

        /* Если все нормально, устанавливаем в кач. текущего альбома, тот, который был определен */
        if($iNewId)  $this->iCurrentAlbumId = $iNewId;

        $this->addNoticeReport(\Yii::t('gallery', 'saveAlbumReport'), \Yii::t('gallery', 'album_id')." = $iNewId", \skLogger::logUsers, "Adm\\Gallery\\Module");

        SEO\Api::setUpdateSitemapFlag();

        /* вывод изображений альбома */
        $this->actionShowAlbum();

    }// func

    /**
     * Автоматический кроп фотографий при мультифайловой загрузки
     * @param array $aFiles
     * @throws \Exception
     * @return void
     */
    protected function actionMultiUploadImages($aFiles = []) {

        // id отображаемого альбома
        $iAlbumId = (int)$this->iCurrentAlbumId;
        if( !$iAlbumId )
            throw new \Exception(\Yii::t('gallery', 'noAlbumError'));

        // id профиля для альбома
        $iProfileId = Gallery\Album::getProfileId($iAlbumId);
        if (!$iProfileId)
            throw new \Exception(\Yii::t('gallery', 'badData'));

        // набор форматов альбома todo ?
        $aFormats   = Gallery\Format::getByProfile($iProfileId);
        if(!$aFormats) throw new \Exception(\Yii::t('gallery', 'badProfileError'));
        foreach ($aFormats as $iKey=>$aFormat)
            if (!$aFormat['active']) unset($aFormats[$iKey]);
        unset($aFormats['thumbnail']);

        $aErrors = Api::getErrorUploadList();
        $iCount = count($aFiles) + count($aErrors);

        foreach($aFiles as $k => $sSourceFN) {

            try {
                $aCrop = [];
                if (count($aFormats))
                    foreach ($aFormats as $iKey=>$aFormat) {
                        //if(strpos($sKey,'cropData_') !== false)
                        $aCrop[$aFormat['name']] = array('x'=>0,'y'=>0,'width'=>0,'height'=>0);
                    }

                $sTitle = '';
                $sAltTitle = '';
                $sDescription = '';

                if ( !count($aCrop) )
                    throw new \Exception(\Yii::t('gallery', 'noDataToSave'));

                // запросить файл
                $sImagePath = $sSourceFN;

                $sImageFullPath = WEBPATH.$sImagePath;

                /* Обработка изображения согласно профилю настроек, перемещение созданных файлов в целевые директории */
                if(!$sImagePath) throw new \Exception(\Yii::t('gallery', 'noLoadImage'));

                $sError = false;

                $mProfileId = [
                    'crop' => $aCrop,
                    'iProfileId' => $iProfileId
                ];

                $aNewImage = Gallery\Photo::processImage($sImageFullPath, $mProfileId, $this->iSectionId, false, true, $sError, Api::cropHeight);

                if (!$aNewImage OR $sError) throw new \Exception($sError);

                $sThumbnail   = (isSet($aNewImage['thumbnail']))? $aNewImage['thumbnail']: '';

                unSet($aNewImage['thumbnail']);

                /* Сохранение сущности в БД */
                $iImageId = Gallery\Photo::setImage([
                    'title'   => $sTitle,
                    'alt_title'   => $sAltTitle,
                    'visible'     => 1,
                    'album_id'    => $iAlbumId,
                    'thumbnail'   => $sThumbnail,
                    'description' => $sDescription,
                    'images_data' => json_encode($aNewImage),
                    'source' => $sImagePath
                ]);

                // очистить контейнер загруки todo
                $this->clearUploadedData();

                if(!$iImageId) throw new \Exception(\Yii::t('gallery', 'noSaveError'));
            }
            catch (\Exception $e) {
                $aErrors[] = $k . ': ' . $e->getMessage();
            }

        }

        $aData = [
            'errors' => $aErrors,
            'iCount' => $iCount,
            'iUpload' => $iCount - count($aErrors)
        ];

        $this->actionShowAlbum($aData);
    }


    /**
     * Загружает изображение на сервер и обрабатывает согласно профилю
     * @return bool|int
     * @throws \Exception
     */
    protected function actionUploadImage() {

        // сборщик мусора
        if (TmpModule::allowStartScavenger()) $this->startScavenger();

        if (!$iAlbumId = (int)$this->iCurrentAlbumId)
            throw new \Exception(\Yii::t('gallery', 'noAlbumError'));

        $iProfileId = Gallery\Album::getProfileId($iAlbumId);
        if (!$iProfileId)
            throw new \Exception(\Yii::t('gallery', 'badData'));

        // набор форматов альбома
        $aFormats   = Gallery\Format::getByProfile($iProfileId);
        if(!$aFormats) 
            throw new \Exception(\Yii::t('gallery', 'badProfileError'));
        foreach( $aFormats as $iKey=>$aFormat)
            if( !$aFormat['active'] ) unset($aFormats[$iKey]);
        unset( $aFormats['thumbnail'] );

        $this->setPanelName(\Yii::t('gallery', 'module_load'), true);


        // Загрузка изображений, перемещение в целевую директорию
        if ($iProfileId == 7) $sSourceFN = Api::uploadFile($this->iSectionId, 'catalog/');
        else $sSourceFN = Api::uploadFile($this->iSectionId);

        // #35829 принудительно включен режим полностью автоматической загрузки
        if ( is_string($sSourceFN) )
            $sSourceFN  = [$sSourceFN];
        $this->actionMultiUploadImages($sSourceFN);
        return false;

        // если загружено несколько файлов - делаем автоматический кроппинг
//        if ( count($sSourceFN) > 1 || count(Api::getErrorUploadList()) ) {
//            $this->actionMultiUploadImages($sSourceFN);
//            return false;
//        }

        // создать миниатюру
        $aCropMin = Api::createCropMin( $sSourceFN, $this->iSectionId, 'catalog/' );

        // добавить в сессию запись о загруженном файле и о миниатюре и получить ключ
        $sCropFN = $aCropMin['file'];
        $this->aUploadedData = [
            'crop' => $sCropFN,
            'source' => $sSourceFN,
            'crop_id' => TmpModule::create('crop', $sCropFN),
            'source_id' => TmpModule::create('source', $sSourceFN)
        ];

        // данные о миниатюре для отображения кроп интерфейса
        $this->setData('cropData', $aCropMin);
        $this->setData('formatsData', $aFormats);

        // Сборка интерфейса

        // Подключить автоматический генератор форм
        $oFormBuilder = UI\StateBuilder::newEdit();

        /* Собираем массив данных по изображению */
        $aTabs['name']  = 'formats';
        $aTabs['title'] = \Yii::t('gallery', 'module_images');
        $aTabs['value'] = array('cropData'=>$aCropMin,'formatsData'=>$aFormats);
        $aTabs['cropData'] = $aCropMin;
        $aTabs['formatsData'] = $aFormats;
        $aTabs['subtext'] = \Yii::t('gallery', 'module_load_subtext');

        $this->addLibClass('PhotoResizer');

        $oFormBuilder
            ->addSpecField('formats', \Yii::t('gallery', 'module_images'), 'PhotoResizerList', $aTabs)
            ->fieldString('title', \Yii::t('gallery', 'module_title'))
            ->fieldString('alt_title', \Yii::t('gallery', 'module_alt_title'))
            ->field('description', \Yii::t('gallery', 'module_description'), 's', 'text');

        // Установить значения для элементов
        $oFormBuilder->setValue([]);

        // Кнопки боковой панели

        // добавить кнопку запуска обработки
        $oFormBuilder->addButtonSave('resizePhoto');
        // и кнопку отмены (возврата к альбому)
        $oFormBuilder->addButtonCancel('showAlbum');

        $this->addJSFile('jquery.min.js');
        $this->addJSFile('jquery.Jcrop.min.js');
        $this->addCSSFile('jquery.Jcrop.min.css');

        // Построить интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }// func

    /**
     * Выполнение ресайза уже загруженного фото
     * @throws \Exception
     */
    protected function actionResizePhoto() {

        // id отображаемого альбома
        $iAlbumId = (int)$this->iCurrentAlbumId;
        if( !$iAlbumId )
            throw new \Exception(\Yii::t('gallery', 'noAlbumError'));

        // id профиля для альбома
        $iProfileId = Gallery\Album::getProfileId($iAlbumId);
        if(!$iProfileId)
            throw new \Exception(\Yii::t('gallery', 'badData'));

        // получить ключ кропа
        $aCrop = array();
        $aData = $this->get('data');
        if( is_array($aData) )
            foreach( $aData as $sKey=>$aVal ) {
                if(strpos($sKey,'cropData_') !== false)
                    $aCrop[substr($sKey,9)] = $aVal;
            }

        $sTitle = isset($aData['title']) ? $aData['title'] : '';
        $sAltTitle = isset($aData['alt_title']) ? $aData['alt_title'] : '';
        $sDescription = isset($aData['description']) ? $aData['description'] : '';
        $aData = $this->aUploadedData;

        if ( !$aData or !count($aCrop) )
            throw new \Exception(\Yii::t('gallery', 'noSataToSave'));

        // запросить файл
        $sImagePath = $aData['source'];
        $sImageFullPath = WEBPATH.$sImagePath;

        /* Обработка изображения согласно профилю настроек, перемещение созданных файлов в целевые директории */
        if(!$sImagePath) throw new \Exception(\Yii::t('gallery', 'noLoadImage'));

        $sError = false;

        $mProfileId = [
            'crop' => $aCrop,
            'iProfileId' => $iProfileId
        ];
        $aNewImage = Gallery\Photo::processImage($sImageFullPath, $mProfileId, $this->iSectionId, false, true, $sError, Api::cropHeight);

        if (!$aNewImage OR $sError) throw new \Exception($sError);

        $sThumbnail   = (isSet($aNewImage['thumbnail']))? $aNewImage['thumbnail']: '';

        unSet($aNewImage['thumbnail']);

        /* Сохранение сущности в БД */
        $iImageId = Gallery\Photo::setImage([
            'title'       => $sTitle,
            'alt_title'   => $sAltTitle,
            'source'      => $sImagePath,
            'visible'     => 1,
            'album_id'    => $iAlbumId,
            'thumbnail'   => $sThumbnail,
            'description' => $sDescription,
            'images_data' => json_encode($aNewImage)
        ]);

        // очистить контейнер загруки todo
        $this->clearUploadedData();

        if(!$iImageId) throw new \Exception(\Yii::t('gallery', 'noSataToSave'));

        $this->set('data',array('id'=>$iImageId));
        $this->addNoticeReport(\Yii::t('gallery', 'loadImageNotice'), \Yii::t('gallery', 'photoId')." = $iImageId", \skLogger::logUsers, "Adm\\Gallery\\Module");

        $this->actionShowImage();
    }

    /**
     * Удаляет файлы и очищает контейнер загрузки
     */
    protected function clearUploadedData() {

        // выйти, если уже очищен
        if (!$this->aUploadedData)
            return;

        // удалить миниатюру и исходник
        unlink( WEBPATH.$this->aUploadedData['crop'] );

        TmpModule::delById( $this->aUploadedData['crop_id'] );
        TmpModule::delById( $this->aUploadedData['source_id'] );

        // очистить контейнер данных загрузки
        $this->aUploadedData = array();
    }

    /**
     * Удаляет выбранный альбом и фотографии к нему
     * @throws \Exception
     */
    protected function actionDelAlbum() {

        /* Данные по альбому */
        $aData = $this->get('data');

        if ( !isSet($aData['id']) OR (!$iAlbumId = (int)$aData['id']) )
            throw new \Exception(\Yii::t('gallery', 'albumError'));

        /* Удаление альбома */
        $mError = false;
        if (!Gallery\Album::removeAlbum($iAlbumId, $mError)) throw new \Exception($mError);

        /* Сброс значения текущго альбома */
        $this->iCurrentAlbumId = false;

        /*Вывод списка альбомов для текущего раздела*/
        $this->actionGetAlbums();

        SEO\Api::setUpdateSitemapFlag();

        $this->addNoticeReport(\Yii::t('gallery', 'deleteAlbumReport'), \Yii::t('gallery', 'deleteAlbumName') .$aData['title'], \skLogger::logUsers, "Adm\\Gallery\\Module");

    }

    /**
     * Сортировка картинок
     */
    protected function actionSortImages(){

        $iItemId = $this->get('itemId');
        $iTargetItemId = $this->get('targetId');
        $sOrderType = $this->get('orderType');

        if ( !$iItemId or !$iTargetItemId or !$sOrderType )
            throw new \Exception(\Yii::t('gallery', 'noSort'));

        $iItemId = (int)str_replace('horizontal_sort', '', $iItemId);
        $iTargetItemId = (int)str_replace('horizontal_sort', '', $iTargetItemId);

        Gallery\Photo::sortImages($iItemId, $iTargetItemId, $sOrderType);
    }

    /**
     * Сортировка альбомов
     */
    protected function actionSortAlbums(){

        $iItemId = $this->get('itemId');
        $iTargetItemId = $this->get('targetId');
        $sOrderType = $this->get('orderType');

        if ( !$iItemId or !$iTargetItemId or !$sOrderType )
            throw new \Exception(\Yii::t('gallery', 'noSort'));

        $iItemId = (int)str_replace('horizontal_sort', '', $iItemId);
        $iTargetItemId = (int)str_replace('horizontal_sort', '', $iTargetItemId);

        Gallery\Album::sortAlbums($iItemId, $iTargetItemId, $sOrderType);
    }


    /**
     * Групповое удаление альбомов
     */
    protected function actionGroupAlbumDel() {

        // набор входных данных для удаления
        $aInList = $this->get('delItems');

        // проверить принадлежность целевому альбому
        $aDelList = Api::validateIdAlbumsList($aInList, $this->iSectionId);

        // удалить по списку
        $iCnt = 0;
        foreach ($aDelList as $iId) {
            if (Gallery\Album::removeAlbum($iId))
                $iCnt++;
        }

        $this->addMessage( \Yii::t('gallery', 'deleteAlbumsPro', [$iCnt, count($aInList)]) );
        $this->addNoticeReport(\Yii::t('gallery', 'albumDeleting'), "<pre>".print_r($aDelList, true)."</pre>", \skLogger::logUsers, "Adm\\Gallery\\Module");
        $this->actionGetAlbums();
    }


    /**
     * Интерфейс с кнопкой создания альбома для раздела
     * @return int
     */
    protected function actionNonAlbum() {

        /* список альбомов текущего раздела */
        $this->setPanelName(\Yii::t('gallery', 'albums'),true);

        /* Строим список альбомов */
        $oList = new \ExtList();

        /* Записываем данные на отправку */
        $this->setData('albums',array());
        /* Добавление библиотек для работы */

        $oList->addExtButton( \ExtDockedAddBtn::create()
            ->setAction('CreateAlbum4Section')
        );

        $this->setInterface( $oList );

        return psComplete;
    }


    /**
     * Создание альбомка для раздела
     * todo перенести в компонент, когда таковой появится
     * todo PS Компонент появился. Есть ли смысл переносить в него одну команду Gallery\Album::setAlbum ...
     */
    protected function actionCreateAlbum4Section() {

        $iNewId = Gallery\Album::setAlbum([
            'owner'      => 'section',  // владелец
            'section_id' => $this->iSectionId, // родительский раздел
            'profile_id' => 6 // Профиль форматов // todo Сделать констанотой
        ]);

        /* Если все нормально, устанавливаем в кач. текущего альбома, тот, который был определен */
        if ($iNewId)  $this->iCurrentAlbumId = $iNewId;

        // todo группа должна браться текущая
        Parameters::setParams($this->iSectionId, 'object', 'iCurrentAlbumId', $iNewId);

        /* вывод изображений альбома */
        $this->actionShowAlbum();
    }
}