/**
 * Модуль для ресайза изображений
 */
Ext.define('Ext.Adm.PhotoResizer',{

    extend: 'Ext.form.field.Base',

    fieldSubTpl: [],
    initialWidth: 0,
    initialHeight: 0,
    imageUrl: '',

    getSubmitData: function() {

        var data = {};
        var cropItem = this;

        // если активны - запросить данные
        if ( cropItem.isActivated() )
            data['cropData_'+cropItem.formatName] = cropItem.getCropData();

        return data;

    },

    // параметры инициализации
    cropInitWidth: 0,
    cropInitHeight: 0,
    cropInitTop: 0,
    cropInitLeft: 0,

    // параметры для ограничения и промежуточных расчетов
    minWidth: 50,
    minHeight: 50,
    preserveRatio: true,
    formatName: '',
    // загружаемая фотка
    crop: {
        file: '',
        width: 0,
        height: 0,
        koef: 1
    },
    cropData: {
        x: 0,
        y: 0,
        height: 0,
        width:0
    },

    // формат
    format: {
        'width': 0,
        'height': 0,
        'resize_on_larger_side': 0,
        'name': ''
    },
    // параметры для сохранения
    outCrop: {},

    cropDriver: null, // crop_api

    getCropData: function() {
        var ScalesBox = this.cropDriver.tellScaled();
        // todo если не задано брать параметры инициализации
        var ScaleResult = {
            x: ScalesBox.x,
            y: ScalesBox.y,
            width: ScalesBox.w,
            height: ScalesBox.h
        };
        return ScaleResult;
    },

    listeners: {
        afterrender: function( self ) {
            self.execute();
        }
    },

    initComponent: function() {

        var me = this;

        // флаг сохранения пропорций
        me.preserveRatio = (parseInt(me.format.width) && parseInt(me.format.height));

        // редактирование ориентации формата
        if( me.format.resize_on_larger_side ) {
            if( ( ( me.crop.width/me.crop.height < 1 ) && ( me.format.width/me.format.height > 1 ) ) ||
                ( (me.crop.width/me.crop.height > 1 ) && (me.format.width/me.format.height < 1) ) ) {
                var cur_size = me.format.width;
                me.format.width = me.format.height;
                me.format.height = cur_size;
            }
        }

        // если стороны - зависимые
        if ( this.preserveRatio ) {

            // отношения сторон вырезки и оригинала
            var kW = me.format.width/me.crop.width;     // соотношение ширин
            var kH = me.format.height/me.crop.height;   // высот
            var kF = me.format.width/me.format.height;  // сторон в выходном форматеы

            // ширина и высота
            if ( kH < kW ) {
                me.cropInitWidth = me.crop.width;
                me.cropInitHeight = parseInt( me.crop.width/kF );
            } else {
                me.cropInitHeight = me.crop.height;
                me.cropInitWidth = parseInt( me.crop.height*kF );
            }

            // сдвиги
            me.cropInitTop = parseInt( (me.crop.height-me.cropInitHeight)/2 );
            me.cropInitLeft = parseInt( (me.crop.width-me.cropInitWidth)/2 );

        } else {
            // независимые - растянуть на все изображение
            me.cropInitTop = 0;
            me.cropInitLeft = 0;
            me.cropInitWidth = me.crop.width;
            me.cropInitHeight = me.crop.height;
        }

        // минимальные значения
        me.minWidth = parseInt( me.format.width/me.crop.koef );
        me.minHeight = parseInt( me.format.height/me.crop.koef );

        // ограничения минимальных значений сверху (если они больше самого кропа)
        if ( me.minWidth && (me.minWidth > me.crop.width) )
            me.minWidth = me.crop.width;
        if ( me.minHeight && (me.minHeight > me.crop.height) )
            me.minHeight = me.crop.height;

        // начальное выходное значение
        me.outCrop = {
            y: me.cropInitTop,
            x: me.cropInitLeft,
            width: me.cropInitWidth,
            height: me.cropInitHeight
        };

        this.callParent();

    },

    active: false,

    isActivated: function() {
        return this.active;
    },

    execute : function(){

        var me = this;

        if ( this.active ) return;
        this.active = true;

        this.cropBg = this.el.insertFirst().setStyle({
            background: 'url('+this.crop.file+') no-repeat left top',
            position: 'absolute',
            left: 0,
            top: 0,
            allowSelect: true
        }).setSize(this.crop.width,this.crop.height);

        this.initNewWrapper();
    },
    initNewWrapper: function() {

        var me = this;

        $(this.cropBg.dom).Jcrop({
            bgFade:     true,
            bgOpacity: .4,
            setSelect: [ me.cropInitLeft,
                        me.cropInitTop,
                        me.cropInitLeft + me.cropInitWidth,
                        me.cropInitTop + me.cropInitHeight]
        } , function() {

            if( me.preserveRatio )
                this.setOptions({ aspectRatio: me.cropInitWidth/me.cropInitHeight });

            this.setOptions({
                minSize: [ me.minWidth, me.minHeight ],
                maxSize: [ me.maxWidth, me.maxHeight ]
            });

            this.focus();

            me.cropDriver = this;
        });

    }
});
