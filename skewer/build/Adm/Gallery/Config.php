<?php

use skewer\models\TreeSection;
use skewer\build\Component\Search;
use skewer\build\Component\Gallery\models\Albums;

/* main */
$aConfig['name']     = 'Gallery';
$aConfig['title']     = 'Галерея (админ)';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Система администрирования фотогаллереи';
$aConfig['revision'] = '0002';
$aConfig['layer']     = Layer::ADM;

$aConfig['events'][] = [
    'event' => TreeSection::EVENT_BEFORE_DELETE,
    'eventClass' => TreeSection::className(),
    'class' => Albums::className(),
    'method' => 'removeSection',
];

$aConfig['events'][] = [
    'event' => Search\Api::EVENT_GET_ENGINE,
    'class' => Albums::className(),
    'method' => 'getSearchEngine',
];

$aConfig['events'][] = [
    'event' => \yii\db\ActiveRecord::EVENT_AFTER_UPDATE,
    'eventClass' => \skewer\models\Parameters::className(),
    'class' => 'skewer\build\Component\Gallery\Album',
    'method' => 'updateSection',
];

return $aConfig;
