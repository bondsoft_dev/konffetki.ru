<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 10.06.14
 * Time: 11:20
 */

namespace skewer\build\Adm\Auth\ar;
use skewer\build\libs\ft;
use skewer\build\Component\orm;
use skewer\build\Adm\Params;
use skewer\build\Component\orm\TablePrototype;

class Users extends TablePrototype {

    /** @var string Имя таблицы */
    protected static $sTableName = 'users';

    public static function getNewRow($aData = array()) {
        $oRow = new UsersRow();
        if ($aData)
            $oRow->setData($aData);
        return $oRow;
    }

    protected static function initModel() {
        ft\Entity::get( static::$sTableName )
            ->clear()
            ->setTablePrefix('')
            ->setNamespace( __NAMESPACE__ )
            ->addField( 'global_id', 'int(11)', 'auth.global_id' )
            ->addField( 'login', 'varchar(40)', 'auth.email' )
            ->addField( 'pass', 'char(32)', 'auth.pass' )
            ->addField( 'group_policy_id', 'int(11)', 'auth.group_policy_id' )
            ->addField( 'active', 'int(11)', 'auth.active' )

            ->addField( 'reg_date', 'datetime', 'auth.reg_date' )

            ->addField( 'lastlogin', 'datetime', 'auth.datetime' )
            ->addField( 'name', 'varchar(40)', 'auth.name' )
            ->addField( 'email', 'varchar(40)', 'auth.email' )
            ->addField( 'postcode', 'varchar(40)', 'auth.postcode' )
            ->addField( 'address', 'varchar(255)', 'auth.address' )
            ->addField( 'phone', 'varchar(20)', 'auth.contact_phone' )
            ->addField( 'cache', 'text', 'auth.cache' )
            ->addField( 'version', 'int(11)', 'auth.version' )
            ->addField( 'del_block', 'tinyint(1)', 'auth.del_block' )

            ->addField('user_info','text','auth.user_info')

            ->addField( 'type_user', 'tinyint(1)', 'auth.type_user' )

            ->addDefaultProcessorSet()
            ->addColumnSet(
                'list',
                array( 'id','name','login','phone','reg_date' )
            )
            ->addColumnSet(
                'add',
                array( 'login','pass', 'type_user')
            )
            ->addColumnSet(
                'edit',
                array( 'id','login','name','postcode','address','phone','reg_date','user_info' ))
            ->save()
            ->build()
        ;
    }
}