<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 11.06.14
 * Time: 12:27
 */

namespace skewer\build\Adm\Auth\ar;


use skewer\build\libs\ft;
use skewer\build\Component\orm;

class UsersRow extends orm\ActiveRecord {

    public $id = 0;
    public $global_id = 0;
    public $login = '';
    public $pass = '';
    public $group_policy_id = 3;
    public $active = 1;

    public $lastlogin = '';
    public $name = "";
    public $email = '';
    public $postcode = '';
    public $address = '';
    public $phone = '';
    public $cache = '';

    public $reg_date = 0;

    public $version = 0;
    public $del_block = 0;

    public $user_info = '';
    public $type_user = '';

    // подтверждение пароля, надо для валидатора
    private $validateError = '';

    function __construct() {
        $this->setTableName( 'users' );
        $this->setPrimaryKey( 'id' );
    }


    public function insert(){
        $this->email = $this->login;
        $this->reg_date = date('Y-m-d H:i:s');
        $this->pass = \Auth::buildPassword($this->login,$this->pass);
        return parent::save();
    }


    /**
     * @return bool
     * перенес в RegForm checkLogin
     */
    public function validate(){

        $oItem =  Users::find()->where('login',$this->login)->get();
        if ($oItem){
            $this->validateError = \Yii::t('auth', 'alredy_taken');
            return false;
        }

        if (!filter_var($this->login,FILTER_VALIDATE_EMAIL)){
            $this->validateError = \Yii::t('auth', 'no_email_valid');
            return false;
        }

        if (!$this->pass){
            $this->validateError = \Yii::t('auth', 'bad_password');
            return false;
        }

        return true;
    }

    public function getValidateErrorMessage(){
        return $this->validateError;
    }
}