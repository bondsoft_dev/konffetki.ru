<?php
/**
 * User: kolesnikiv
 * Date: 31.07.13
 */
$aConfig['name']     = 'Auth';
$aConfig['title']    = 'Регистрация';
$aConfig['version']  = '1.0';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::ADM;
$aConfig['useNamespace'] = true;
$aConfig['languageCategory']     = 'auth';

$aConfig['dependency'] = [
    ['Auth', \Layer::PAGE],
    ['Auth', \Layer::TOOL],
    ['Profile', \Layer::PAGE],
];


return $aConfig;
