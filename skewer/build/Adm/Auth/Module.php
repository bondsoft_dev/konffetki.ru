<?php

namespace skewer\build\Adm\Auth;


use skewer\build\Component\I18N\Languages;
use skewer\build\Component\I18N\ModulesParams;
use skewer\build\Component\Section\Parameters;
use skewer\build\Component\UI;
use skewer\build\Page\Auth\Api;
use skewer\build\Page\CatalogViewer;
use skewer\build\Adm;
use skewer\build\libs\ExtBuilder;
use yii\base\UserException;
use yii\helpers\ArrayHelper;

/**
 * Модуль настройки вывода форм регистрации
 * Class Module
 * @package skewer\build\Adm\Catalog
 */
class Module extends Adm\Tree\ModulePrototype {

    protected $sLanguageFilter = '';

    /** @var array Поля настроек */
    protected $aSettingsKeys =
        [
            'mail_activate',
            'mail_close_ban',
            'mail_banned',
            'mail_admin_activate',
            'mail_user_activate',
            'mail_reset_password',
            'mail_title_admin_newuser',
            'mail_title_user_newuser',
            'mail_title_reset_password',
            'mail_title_new_pass',
            'mail_new_pass',
            'mail_title_mail_activate',
            'mail_title_mail_close_banned',
            'mail_title_mail_banned'
        ];

    /** @var int id текущего раздела */
    protected $iSectionId = 0;
    protected $iStatusFilter = 0;

    // фильтр по тексту
    protected $sSearchNameFilter = '';
    protected $sSearchEmailFilter = '';
    protected $sSearchPhoneFilter = '';

    /**
     * Метод, выполняемый перед action меодом
     * @return bool
     * @throws UserException
     */
    protected function preExecute() {
        // id текущего раздела
        $this->iSectionId = $this->getInt('sectionId');
        $this->iStatusFilter = $this->get('filter_status', false);
        $this->sSearchNameFilter = $this->getStr('search');
        $this->sSearchEmailFilter = $this->getStr('email');
        $this->sSearchPhoneFilter = $this->getStr('phone');

        // проверить права доступа
        if (!\CurrentAdmin::canRead($this->iSectionId))
            throw new UserException('accessDenied');

        $sLanguage = \Yii::$app->language;
        if ($this->pageId){
            $sLanguage = Parameters::getLanguage($this->pageId);
        }

        $this->sLanguageFilter = $this->get('filter_language', $sLanguage);
    }


    /**
     * Первичное состояние
     */
    protected function actionInit() {
        $this->actionList();
    }

    /**
     * Сохраняем состояние
     */
    protected function actionSaveStatement(){

        $iStatus = $this->getInDataValInt('status',0);
        \SysVar::set('auth.activate_status',$iStatus);

        $this->actionList();
    }

    /**
     * Выбор активации пользователя
     */
    protected function actionEditActivateStatement(){
        $oForm = new \ExtForm();

        $oStatusField = new ExtBuilder\Field\SelectByArray(Api::getActivateStatusList());
        $oStatusField->setName('status');
        $oStatusField->setTitle(\Yii::t('auth', 'activate_status'));

        $iActivateStatus = \SysVar::get('auth.activate_status');
        $oStatusField->setValue($iActivateStatus);

        $oForm->addField($oStatusField);

        $oForm->addBtnSave('saveStatement','saveStatement');
        $oForm->addBtnCancel();
        $this->setInterface($oForm);
    }

    protected function actionChangeStatus(){

        $iUserId =  $this->getInDataValInt('id');
        $iActiveId = $this->getInDataValInt('active');

        /**
         * @var Adm\Auth\ar\UsersRow $oUser
         */
        $oUser = Adm\Auth\ar\Users::find($iUserId);

        if ($oUser){

            $prevStatus = $oUser->active;

            $oUser->active = $iActiveId;
            $oUser->save();


            $sMsg = \Yii::t('auth', 'change_status');
            $sSubject = \Yii::t('auth', 'change_status');

            if ($prevStatus == Api::STATUS_NO_AUTH && $iActiveId == Api::STATUS_AUTH){
                $sMsg = Api::getTextMailActivate();
                $sSubject = ModulesParams::getByName('auth', 'mail_title_mail_activate');
            }

            if ($prevStatus == Api::STATUS_BANNED && $iActiveId == Api::STATUS_AUTH){
                $sMsg = Api::getTextMailCloseBan();
                $sSubject = ModulesParams::getByName('auth', 'mail_title_mail_close_banned');
            }

            if ($iActiveId == Api::STATUS_BANNED){
                $sMsg = Api::getTextMailBanned();
                $sSubject = ModulesParams::getByName('auth', 'mail_title_mail_banned');
            }

            \Policy::incPolicyVersion();

            \Mailer::sendMail( $oUser->email, $sSubject, $sMsg);

        }

        $aActiveStatusList = Api::getStatusList();

        $oListVals = new \ExtListRows();
        $oListVals->setSearchField( 'id' );
        $oListVals->addDataRow( array_merge($oUser->getData(),array('text_active'=>$aActiveStatusList[$oUser->active])));
        $oListVals->setData( $this );
    }

    /**
     * Список пользователей в магазине
     */
    protected function actionList() {
        $oList = new \ExtList();

        $aActiveStatusList = Api::getStatusList();

        $oList->addFilterText( 'search', $this->sSearchNameFilter, \Yii::t('auth', 'name') );
        $oList->addFilterText( 'email', $this->sSearchEmailFilter, \Yii::t('auth', 'email') );
        $oList->addFilterText( 'phone', $this->sSearchPhoneFilter, \Yii::t('auth', 'contact_phone') );

        $oList->setFieldsByFtModel(ar\Users::getModel(), 'list');

        // добавляем фильтр по статусам
        $oList->addFilterSelect('filter_status', Api::getStatusList(), $this->iStatusFilter, \Yii::t('auth', 'field_status_active'));

        $oList->setFieldFlex('id', 1);
        $oList->setFieldFlex('login', 3);
        $oList->setFieldFlex('name', 3);


        $aUsersCatalog = ar\Users::find()->where('group_policy_id',3);

        if ($this->iStatusFilter !== false)
            $aUsersCatalog->where('active', $this->iStatusFilter);

        if ( $this->sSearchNameFilter ) {
            $aUsersCatalog->where( 'name LIKE ?', '%' . $this->sSearchNameFilter . '%' );
        }
        if ( $this->sSearchEmailFilter ) {
            $aUsersCatalog->where( 'email LIKE ?', '%' . $this->sSearchEmailFilter . '%' );
        }
        if ( $this->sSearchPhoneFilter ) {
            $aUsersCatalog->where( 'phone LIKE ?', '%' . $this->sSearchPhoneFilter . '%' );
        }

        $aUsersCatalog = $aUsersCatalog->getAll();

        foreach($aUsersCatalog as $k=>$item){
            $aUsersCatalog[$k]->login = htmlentities($aUsersCatalog[$k]->login);
            $aUsersCatalog[$k]->name = htmlentities($aUsersCatalog[$k]->name);
            $aUsersCatalog[$k]->postcode = htmlentities($aUsersCatalog[$k]->postcode);
            $aUsersCatalog[$k]->address = htmlentities($aUsersCatalog[$k]->address);
            $aUsersCatalog[$k]->phone = htmlentities($aUsersCatalog[$k]->phone);
            $aUsersCatalog[$k]->text_active = $aActiveStatusList[$item->active];
        }
        $oList->setValues($aUsersCatalog);

        $oField = new ExtBuilder\Field\String();
        $oField->setName('text_active');
        $oField->setTitle(sprintf('%s', \Yii::t('auth', 'activate_status')));
        $oList->addField($oField);
        $oList->setFieldFlex('text_active');


        $oList->addRowCustomBtn('StatusGroupBtn');

        $oList->addRowBtnUpdate('editUser');
        $oList->addRowBtnDelete();

        // кнопка добавления
        $oList->addBtnAdd('newUser');
        $oList->addDockedItem(array(
            'text' => \Yii::t('auth', 'status_edit'),
            'iconCls' => 'icon-edit',
            'state' => 'editActivateStatement',
            'action' => 'editActivateStatement',
            'addParams' => array()
        ));
        $oList->addDockedItem(array(
                'text' => \Yii::t('auth', 'mail_status_edit'),
                'iconCls' => 'icon-edit',
                'state' => 'showMail',
                'action' => 'showMail',
                'addParams' => array()
            )
        );
        $oList->addDockedItem(array(
            'text' => \Yii::t('auth', 'license_edit'),
            'iconCls' => 'icon-edit',
            'state' => 'editLicense',
            'action' => 'editLicense',
            'addParams' => array()
        ));

        $this->setInterface($oList);
    }

    /**
     * Редактируем письма активации
     */
    protected function actionSaveMail(){

        $aData = $this->getInData();

        $sLanguage = $this->getInnerData('languageFilter');
        $this->setInnerData('languageFilter', '');

        if ($sLanguage) {
            foreach( $aData as $sName => $sValue ){

                if (!in_array($sName, $this->aSettingsKeys))
                    continue;

                ModulesParams::setParams( 'auth', $sName, $sLanguage, $sValue);
            }
        }

        $this->actionInit();
    }

    protected function actionShowMail(){
        // создаем форму
        $oFormBuilder=  UI\StateBuilder::newEdit();

        $oFormBuilder->addButtonSave('saveMail');
        $oFormBuilder->addButtonCancel('init');

        if (!$this->pageId){
            $aLanguages = Languages::getAllActive();
            $aLanguages = ArrayHelper::map($aLanguages, 'name', 'title');

            if (count($aLanguages) > 1) {
                $oFormBuilder->addFilterSelect('filter_language', $aLanguages, $this->sLanguageFilter, \Yii::t('adm','language'), ['set' => true]);
                $oFormBuilder->addFilterAction('showMail');
            }
        }

        $oFormBuilder
            ->field('info', '', 's', 'show', ['hideLabel' => true])

            ->fieldString('mail_title_mail_activate', \Yii::t('auth', 'edit_title_mail_activate'))
            ->field('mail_activate', \Yii::t('auth', 'edit_mail_activate'), 's', 'wyswyg')

            ->fieldString('mail_title_admin_newuser', \Yii::t('auth', 'edit_mail_title_admin_newuser'))
            ->field('mail_admin_activate', \Yii::t('auth', 'edit_main_admin_activate'), 's', 'wyswyg')

            ->fieldString('mail_title_user_newuser', \Yii::t('auth', 'edit_mail_title_user_newuser'))
            ->field('mail_user_activate', \Yii::t('auth', 'edit_main_user_activate'), 's', 'wyswyg')

            ->fieldString('mail_title_new_pass', \Yii::t('auth', 'edit_mail_title_new_pass'))
            ->field('mail_new_pass', \Yii::t('auth', 'edit_mail_new_pass'), 's', 'wyswyg')

            ->fieldString('mail_title_reset_password', \Yii::t('auth', 'edit_mail_title_reset_password'))
            ->field('mail_reset_password', \Yii::t('auth', 'edit_mail_reset_password'), 's', 'wyswyg')

            ->fieldString('mail_title_mail_banned', \Yii::t('auth', 'edit_title_mail_banned'))
            ->field('mail_banned', \Yii::t('auth', 'edit_mail_banned'), 's', 'wyswyg')

            ->fieldString('mail_title_mail_close_banned', \Yii::t('auth', 'edit_title_mail_close_banned'))
            ->field('mail_close_ban', \Yii::t('auth', 'edit_mail_close_ban'), 's', 'wyswyg')
        ;

        $aModulesData = ModulesParams::getByModule('auth', $this->sLanguageFilter);
        $this->setInnerData('languageFilter', $this->sLanguageFilter);

        $aItems = [];
        $aItems['info'] = \Yii::t('auth', 'head_mail_text', [\Yii::t('app', 'site_label'), \Yii::t('app', 'url_label')]);

        foreach( $this->aSettingsKeys as  $key ){
            $aItems[$key] = (isset($aModulesData[$key]))?$aModulesData[$key]:'';
        }

        $oFormBuilder->setValue($aItems);

        $this->setInterface($oFormBuilder->getForm());
    }

    protected function actionDelete(){
        $aData = $this->get('data');
        if (isset($aData['id'])){
            $iItemId = $aData['id'];
            ar\Users::delete()->where('id',$iItemId)->where('group_policy_id',3)->get();
        }
        $this->actionList();
    }


    protected function actionEditUser(){

        $iUserId =  $this->getInDataValInt('id');

        /**
         * @var Adm\Auth\ar\UsersRow $oUser
         */
        $oUser = Adm\Auth\ar\Users::find($iUserId);
        $this->showForm($oUser);
    }


    /**
     * Форма добавления
     */
    protected function actionNewUser() {
        $this->showForm();
    }

    protected function actionSaveUser(){
        // получим данные
        $id = $this->getInDataValInt('id');
        $sLogin = $this->getInDataVal('login');
        $sPassword = $this->getInDataVal('pass');


        // редактирование
        if ($id){
            /**
             * @var ar\UsersRow $oUser
             */
            $oUser = ar\Users::find($id);
            $oUser->name = $this->getInDataVal('name');
            $oUser->postcode = $this->getInDataVal('postcode');
            $oUser->address = $this->getInDataVal('address');
            $oUser->phone = $this->getInDataVal('phone');
            $oUser->user_info = $this->getInDataVal('user_info');

            $oUser->save();
            $this->actionList();
        // новый юзер
        } else {
            // создадим в AR
            $oUser = new ar\UsersRow();
            $oUser->login = $sLogin;
            $oUser->pass =  $sPassword;
            $oUser->phone = $this->getInDataVal('phone');
            $oUser->address = $this->getInDataVal('address');
            $oUser->name = $this->getInDataVal('name');
            $oUser->postcode = $this->getInDataVal('postcode');
            $oUser->active = 1;
            $oUser->user_info = $this->getInDataVal('user_info');

            // отвалидируем и сохраним
            if ($oUser->validate() && $oUser->insert()){
                $this->actionList();
            } else {
                // если ошибка, то болт.
                $this->addMessage($oUser->getValidateErrorMessage());
            }
        }

    }

    protected function actionPass(){

        $oFormBuilder = UI\StateBuilder::newEdit();

        $oFormBuilder->button(\Yii::t('adm','save'),'savePass','icon-save');
        $oFormBuilder->button(\Yii::t('adm','back'),'init','icon-cancel');
        $oFormBuilder
            ->fieldHide('id','id')
            ->field('pass',\Yii::t('auth', 'password'),'s','pass')
            ->field('wpass',\Yii::t('auth', 'wpassword'),'s','pass')
        ;

        $aData['id'] = $this->getInDataValInt('id');
        $oFormBuilder->setValue($aData);

        // вывод данных в интерфейс
        $this->setInterface($oFormBuilder->getForm());
    }

    protected function actionSavePass(){
        $id = $this->getInDataVal('id');
        $sPass = $this->getInDataVal('pass');
        $sWpass = $this->getInDataVal('wpass');

        /**
         * @var ar\UsersRow $oUser
         */
        $oUser = ar\Users::find($id);

        if ($oUser && $sPass!='' && $sPass==$sWpass){
            $oUser->pass = \Auth::buildPassword($oUser->login,$sPass);
            $oUser->save();

            $this->actionInit();
        } else {
            $this->addError('Error in password saving');
        }
    }

    /**
     * Отображение формы добавления/редактирования формы
     * @param Adm\Auth\ar\UsersRow $oItem
     */
    private function showForm($oItem = null) {

        $oFormBuilder = UI\StateBuilder::newEdit();

        $aActiveStatusList = Api::getStatusList();


        $oFormBuilder->button(\Yii::t('adm','save'),'saveUser','icon-save');
        $oFormBuilder->button(\Yii::t('adm','back'),'init','icon-cancel');

        // добавляем поля
        $oFormBuilder
            ->field('id','id','i',($oItem)?'string':'hide',($oItem)?['readOnly' => true]:[])
            ->fieldString('login', \Yii::t('auth', 'email'), ($oItem)?['readOnly' => true]:[]);

        if (!$oItem){
            // для нового пользователя выведем филд пароля
            $oFormBuilder->fieldString('pass',\Yii::t('auth', 'password'));
        } else {
            // кнопка редактирования пароля
            if ( !\skewer\build\Tool\Users\Api::isCurrentSystemUser($oItem->id) ) {
                $oFormBuilder->button(\Yii::t('auth', 'pass'), 'pass', 'icon-edit', ['id'=>$oItem->id]);
            }
        }

        $oFormBuilder
            ->fieldString('name', \Yii::t('auth', 'name'))
            ->fieldString('postcode', \Yii::t('auth', 'postcode'))
            ->fieldString('address', \Yii::t('auth', 'address'))
            ->fieldString('phone', \Yii::t('auth', 'contact_phone'))
            ->field('user_info', \Yii::t('auth', 'user_info') , 's', 'text');

        $aData = array();
        if ($oItem && $oItem->getData()) $aData = $oItem->getData();


        if ($oItem){
            $aData['active'] = $aActiveStatusList[$oItem->active];
            $oFormBuilder
                ->field('reg_date', \Yii::t('auth', 'reg_date') , 's', 'show', ($oItem)?['readOnly' => true]:[])
                ->field('active', \Yii::t('auth', 'activate_status') , 's', 'show', ($oItem)?['readOnly' => true]:[]);
        };

        $aData['pass'] = '';


        // устанавливаем значения
        $oFormBuilder->setValue($aData);

        // вывод данных в интерфейс
        $this->setInterface($oFormBuilder->getForm());
    }


    protected function actionEditLicense() {

        $oFormBuilder = UI\StateBuilder::newEdit();

        $aData['license'] = ModulesParams::getByName('auth', 'reg_license', $this->sLanguageFilter);

        if (!$this->pageId){
            $aLanguages = Languages::getAllActive();
            $aLanguages = ArrayHelper::map($aLanguages, 'name', 'title');

            if (count($aLanguages) > 1) {
                $oFormBuilder->addFilterSelect('filter_language', $aLanguages, $this->sLanguageFilter, \Yii::t('adm','language'), ['set' => true]);
                $oFormBuilder->addFilterAction('editLicense');
            }
        }

        // добавляем поля
        $oFormBuilder
            ->field( 'license', \Yii::t('auth', 'license'), 's', 'wyswyg' )
            ->setValue( $aData )
            ->button( \Yii::t('adm','save'), 'saveLicense', 'icon-save' )
            ->button( \Yii::t('adm','back'), 'list', 'icon-cancel' )
        ;

        // вывод данных в интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

    }


    protected function actionSaveLicense() {

        ModulesParams::setParams('auth', 'reg_license', $this->sLanguageFilter, $this->getInDataVal( 'license' ));

        $this->actionList();

    }

}