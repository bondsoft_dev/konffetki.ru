<?php

namespace skewer\build\Adm\Order;


use skewer\build\Adm\Order\model\Delivery;
use skewer\build\Component\I18N\Languages;
use skewer\build\Component\I18N\ModulesParams;
use skewer\build\Component\Section\Parameters;
use skewer\build\Component\UI;
use skewer\build\Adm;
use skewer\build\Adm\Order\model\Status;
use skewer\build\Component\Catalog\GoodsSelector;
use skewer\build\libs\ExtBuilder;
use skewer\build\Component\Site;
use skewer\build\Page\Auth\UserType;
use yii\base\UserException;
use yii\helpers\ArrayHelper;


/**
 * @todo рефакторить модуль
 * Class Module
 * @package skewer\build\Adm\Order
 */
class Module extends Adm\Tree\ModulePrototype {
    var $iPage = 0;
    var $iOnPage = 0;
    protected $iStatusFilter = 0;

    protected $sLanguageFilter = '';

    protected function preExecute() {
        // номер страницы
        $this->iPage = $this->getInt('page');

        $this->iStatusFilter = $this->getInt('filter_status', 0);

        $sLanguage = \Yii::$app->language;
        if ($this->pageId){
            $sLanguage = Parameters::getLanguage($this->pageId);
        }

        $this->sLanguageFilter = $this->get('filter_language', $sLanguage);

    }

    protected function setServiceData(UI\State\BaseInterface $oIface) {

        parent::setServiceData($oIface);

        // расширение массива сервисных данных
        $oIface->setServiceData(array(
            'filter_status' => $this->iStatusFilter,
            'filter_language' => $this->sLanguageFilter,
        ));

    }


    protected function actionInit() {
        $this->actionList();
    }

    protected function actionDelete() {
        // запросить данные
        $aData = $this->get('data');

        // id записи
        $iItemId = (is_array($aData) and isset($aData['id'])) ? (int)$aData['id'] : 0;

        if (!$iItemId){
            $iItemId = $this->getInnerData('orderId');
        }

        ar\Order::delete($iItemId);
        // вывод списка
        $this->actionInit();
    }

    /**
     * Сохранение заказы
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->get('data');
        $iId = $this->getInnerDataInt('orderId');

        if (!$aData)
            throw new UserException('Empty data');

        if ($iId) {
            /**
             * @var ar\OrderRow $oRow
             */
            $oRow = ar\Order::find($iId);

            if (!$oRow)
                throw new UserException("Not found [$iId]");

            if (isset($aData['status']) && $oRow->status != $aData['status']) {
                Service::sendMailChangeOrderStatus($iId, $oRow->status, $aData['status']);
            }

            $oRow->setData($aData);
        } else {
            $oRow = ar\Order::getNewRow($aData);
        }

        $oRow->save();
        // вывод списка
        $this->actionInit();
    }

    protected function actionShow() {

        // номер заказа
        $aData = $this->get('data');

        $iItemId = (is_array($aData) && isset($aData['id'])) ? (int)$aData['id'] : 0;
        $iItemId = $this->getInDataVal('id_order',$iItemId);

        // -- сборка интерфейса
        $oFormBuilder = UI\StateBuilder::newEdit();

        $statusList = Status::getListTitle();
        $paymentList = ArrayHelper::map(ar\TypePayment::find()->asArray()->getAll(),'id','title');
        $deliveryList = ArrayHelper::map(ar\TypeDelivery::find()->asArray()->getAll(),'id','title');
        $aSourcesInfo = ArrayHelper::map(ar\SourcesInfo::find()->asArray()->getAll(),'id','title');

        $aHistoryList = Adm\Order\model\ChangeStatus::find()->asArray()->where(['id_order' => $iItemId])->all();

        $aOrder = ar\Order::find()->where('id',$iItemId)->asArray()->getOne();

        if (!$aOrder)
            throw new UserException('Item not found');


        if ($aHistoryList){
            foreach($aHistoryList as $k=>$item){
                $aHistoryList[$k]['title_old_status'] = ArrayHelper::getValue($statusList, $item['id_old_status'], sprintf('--- ['.$item['id_old_status'].']'));
                $aHistoryList[$k]['title_new_status'] = ArrayHelper::getValue($statusList, $item['id_new_status'], sprintf('--- ['.$item['id_new_status'].']'));
            }
            $aOrder['history'] = $this->renderTemplate('historyList.twig', array('historyList'=>$aHistoryList));
        }
        $type_form = $aOrder['type_form'];
        if ($aOrder['type_form'] == UserType::TYPE_INDIVIDUAL) {
            $aOrder['type_form'] = UserType::TITLE_INDIVIDUAL;
        }elseif($aOrder['type_form'] == UserType::TYPE_ENTITY){
            $aOrder['type_form'] = UserType::TITLE_ENTITY;
        } elseif($aOrder['type_form'] == UserType::TYPE_INDIVID_ENTREP){
            $aOrder['type_form'] = UserType::TITLE_INDIVID_ENTREP;
        }
        // добавляем поля
        $oFormBuilder
            ->addField('id', 'ID', 'i', 'string', array('readOnly' => 1,'groupTitle' => 'Информация о заказе') )
            ->addField('date', \Yii::t('order', 'field_date'), 's', 'datetime', array('groupTitle' => 'Информация о заказе'))
            ->addField('type_form', \Yii::t('order', 'type_form'), 's', 'string', array('readOnly' => 1,'groupTitle' => 'Информация о заказе') );

        if ($type_form != UserType::TYPE_INDIVIDUAL) {
            $oFormBuilder
            //данные юридического лица
                ->addField('legal_form', \Yii::t('order', 'field_legal_form'), 's', 'string', array('groupTitle' => 'Данные юридического лица'));
            if ($type_form == UserType::TYPE_INDIVID_ENTREP) {
                $oFormBuilder
                    ->addField('organization', \Yii::t('order', 'fio'), 's', 'string', array('groupTitle' => 'Данные юридического лица'));
            } else {
                $oFormBuilder
                    ->addField('organization', \Yii::t('order', 'field_organization'), 's', 'string', array('groupTitle' => 'Данные юридического лица'))
                    ->addField('kpp', \Yii::t('order', 'field_kpp'), 's', 'string', array('groupTitle' => 'Данные юридического лица'));
            }
            $oFormBuilder
                ->addField('inn', \Yii::t('order', 'field_inn'), 's', 'string', array('groupTitle' => 'Данные юридического лица'));
        }
        //контактные данные
        if ($type_form == UserType::TYPE_INDIVIDUAL){
        $oFormBuilder
            ->addField('person', \Yii::t('order', 'fio'), 's', 'string',array('groupTitle'=>'Контактные данные'));
        } else {
        $oFormBuilder
            ->addField('person', \Yii::t('order', 'field_contact_face'), 's', 'string',array('groupTitle'=>'Контактные данные'));}
        $oFormBuilder
            ->addField('phone', \Yii::t('order', 'field_phone'), 's', 'string',array('groupTitle'=>'Контактные данные'))
            ->addField('mail', \Yii::t('order', 'field_mail'), 's', 'string',array('groupTitle'=>'Контактные данные'))

            ->addSelectField('status', \Yii::t('order', 'field_status'), 's',$statusList,array('groupTitle'=>'Дополнительная информация'))
            ->addSelectField('type_payment', \Yii::t('order', 'field_payment'), 's',$paymentList,array('groupTitle'=>'Дополнительная информация'))
            ->addSelectField('type_delivery', \Yii::t('order', 'field_delivery'), 's',$deliveryList,array('groupTitle'=>'Дополнительная информация'))
            ->addSelectField('source_info', \Yii::t('order', 'adm_field_sources_info'), 's',$aSourcesInfo,array('groupTitle'=>'Дополнительная информация'));
        if ($type_form != UserType::TYPE_INDIVIDUAL) {
            $oFormBuilder
                //юридический адрес
                ->addField('legal_region', \Yii::t('order', 'field_region'), 's', 'string', array('groupTitle' => 'Юридический адрес'))
                ->addField('legal_city', \Yii::t('order', 'field_city'), 's', 'string', array('groupTitle' => 'Юридический адрес'))
                ->addField('legal_street', \Yii::t('order', 'field_street'), 's', 'string', array('groupTitle' => 'Юридический адрес'))
                ->addField('legal_house', \Yii::t('order', 'field_house'), 's', 'string', array('groupTitle' => 'Юридический адрес'))
                ->addField('legal_housing', \Yii::t('order', 'field_housing'), 's', 'string', array('groupTitle' => 'Юридический адрес'))
                ->addField('legal_room', \Yii::t('order', 'field_room'), 's', 'string', array('groupTitle' => 'Юридический адрес'))
                //фактический адрес
                ->addField('actual_region', \Yii::t('order', 'field_region'), 's', 'string', array('groupTitle' => 'Фактический адрес'))
                ->addField('actual_city', \Yii::t('order', 'field_city'), 's', 'string', array('groupTitle' => 'Фактический адрес'))
                ->addField('actual_street', \Yii::t('order', 'field_street'), 's', 'string', array('groupTitle' => 'Фактический адрес'))
                ->addField('actual_house', \Yii::t('order', 'field_house'), 's', 'string', array('groupTitle' => 'Фактический адрес'))
                ->addField('actual_housing', \Yii::t('order', 'field_housing'), 's', 'string', array('groupTitle' => 'Фактический адрес'))
                ->addField('actual_room', \Yii::t('order', 'field_room'), 's', 'string', array('groupTitle' => 'Фактический адрес'));
        }

        $oFormBuilder
            //адрес доставки
            ->addField('delivery_region', \Yii::t('order', 'field_region'), 's', 'string',array('groupTitle'=>'Адрес доставки'))
            ->addField('delivery_city', \Yii::t('order', 'field_city'), 's', 'string',array('groupTitle'=>'Адрес доставки'))
            ->addField('delivery_street', \Yii::t('order', 'field_street'), 's', 'string',array('groupTitle'=>'Адрес доставки'))
            ->addField('delivery_house', \Yii::t('order', 'field_house'), 's', 'string',array('groupTitle'=>'Адрес доставки'))
            ->addField('delivery_housing', \Yii::t('order', 'field_housing'), 's', 'string',array('groupTitle'=>'Адрес доставки'))
            ->addField('delivery_room', \Yii::t('order', 'field_room'), 's', 'string',array('groupTitle'=>'Адрес доставки'))

            ->addField('text', \Yii::t('order', 'field_text'), 's', 'text')
            ->addField('notes', \Yii::t('order', 'field_notes'), 's', 'text')
            ->addField('good',\Yii::t('order', 'goods_info'),'s','show',array('labelAlign'=>'top'))

            ->addFieldIf('history',\Yii::t('order', 'history_list'),'s','show',array('labelAlign'=>'top'))
        ;



        $aGoods = ar\Goods::find()->where('id_order',$aOrder['id'])->asArray()->getAll();
        foreach($aGoods as $k=>$item){
            //@TODO исправить это в каталоге
            // наверно это не правильно, но врятли у нас будет > 100 товаров в заказе
            $aGoods[$k]['object'] = GoodsSelector::get($item['id_goods'], 1); //fixme #GET_CATALOG_GOODS
        }
        $totalPrice = Service::getTotalPriceWithoutDelivery($aOrder['id']);
        $deliv_cost = Delivery::getDeliveryPrice($aOrder['type_delivery']);
        $totalDelivPrice = (int)$totalPrice + (int)$deliv_cost;
        $sText = "";
        if ($aGoods && !empty($aGoods)) {
            $sText = $this->renderTemplate('admin.goods.twig', array(
                'id' => $aOrder['id'],
                'aGoods' => $aGoods,
                'totalPrice' => $totalPrice,
                'delivCost' => $deliv_cost,
                'totalDelivPrice' => $totalDelivPrice
            ));
        }



        $aOrder['good'] = $sText;
        $oFormBuilder->setValue($aOrder);

        // добавляем элементы управления
        $oFormBuilder->addButton(\Yii::t('adm','save'), 'save', 'icon-save','save');
        $oFormBuilder->addButton(\Yii::t('adm','back'), 'init', 'icon-cancel');

        if (isset($aOrder['id']) && $aOrder['id']){
            $oFormBuilder->addButton(\Yii::t('order', 'field_goods_title_edit'),'goodsShow','icon-edit');
            $this->setInnerData('orderId', $aOrder['id']);
        }

        $oFormBuilder->addBtnSeparator('->');
        $oFormBuilder->addButton(\Yii::t('adm','del'),'delete','icon-delete');

        $this->setInterface($oFormBuilder->getForm());
    }


    /**
     * Список заказов
     */
    protected function actionList() {

        $aStatusList =  Status::getListTitle();

        // -- сборка интерфейса
        $oFormBuilder = new UI\StateBuilder();

        // создаем форму
        $oFormBuilder->clear()->createFormList();
        // добавляем поля
        $oFormBuilder
            ->addFilterSelect('filter_status', $aStatusList, $this->iStatusFilter, \Yii::t('order', 'field_status'))
            ->addField('id', 'ID', 'i', 'string',array( 'listColumns' => array('flex' => 1) ) )
            ->addField('date', \Yii::t('order', 'field_date'), 's', 'string',array( 'listColumns' => array('flex' => 3) ))
            ->addField('person', \Yii::t('order', 'field_contact_face'), 's', 'string',array( 'listColumns' => array('flex' => 3) ))
            ->addField('mail', \Yii::t('order', 'field_mail'), 's', 'string',array( 'listColumns' => array('flex' => 3) ))
            ->addField('total_price', \Yii::t('order', 'field_goods_total'), 's', 'string',array( 'listColumns' => array('flex' => 2) ))
            ->addField('status', \Yii::t('order', 'field_status'), 's', 'string',array( 'listColumns' => array('flex' => 3) ))
            ->setFields()
            ->addWidget( 'status', 'skewer\\build\\Adm\\Order\\Service', 'getStatusValue' );



        $oQuery = ar\Order::find()->order('id', 'DESC');

        if ($this->iStatusFilter){
            $oQuery->where('status', $this->iStatusFilter);
        }
        $aOrders = $oQuery->asArray()->getAll();

        $aTotalPrice = Service::getArrayTotalPrice();

        foreach ($aOrders as &$item) {
            if (isset($aTotalPrice[$item['id']]))
                $item['total_price'] = $aTotalPrice[$item['id']];
            else $item['total_price'] = "-";
        }

        $oFormBuilder->setValue($aOrders);
        $oFormBuilder->addRowButtonUpdate();
        $oFormBuilder->addRowButtonDelete();

        $oFormBuilder->addButton(\Yii::t('order', 'field_orderstatus_title'),'statusList','icon-edit','statusList');
        $oFormBuilder->addButton(\Yii::t('order', 'field_mailtext'),'emailShow','icon-edit','emailShow');
        $oFormBuilder->addButton(\Yii::t('order', 'field_payment'),'typePaymentList','icon-edit','typePaymentList');
        $oFormBuilder->addButton(\Yii::t('order', 'field_delivery'),'typeDeliveryList','icon-edit','typeDeliveryList');
        $oFormBuilder->addButton(\Yii::t('auth', 'license' ),'editLicense','icon-edit','editLicense');
        $oFormBuilder->addButton(\Yii::t('order', 'field_sources_info'),'sourcesInfo','icon-edit','sourcesInfo');

        $this->setInterface($oFormBuilder->getForm());
    }

    /**
     * CRUD для типа доставки (list)
     */
    protected function actionTypeDeliveryList(){
        // -- сборка интерфейса
        $oFormBuilder = new UI\StateBuilder();
        // создаем форму
        $oFormBuilder->clear()->createFormList();
        // добавляем поля
        $oFormBuilder
            ->addField('id', 'id', 'i', 'string')
            ->addField('title', \Yii::t('order', 'field_delivery'), 's', 'string',array( 'listColumns' => array('flex' => 3) ))
            ->addField('price', \Yii::t('order', 'price'), 'i', 'int',array( 'listColumns' => array('flex' => 3) ))
            ->addField('active', \Yii::t('order', 'active'), 'i', 'check',array( 'listColumns' => array('flex' => 3) ))
            ->setFields();

        $oFormBuilder->setValue(ar\TypeDelivery::find()->asArray()->getAll());

        $oFormBuilder->addRowButtonDelete('DeleteTypeDelivery','DeleteTypeDelivery');
        $oFormBuilder->addButton(\Yii::t('adm','add'), 'AddTypeDelivery', 'icon-add','AddTypeDelivery');
        $oFormBuilder->addButton(\Yii::t('adm','back'), 'init', 'icon-cancel');

        $oFormBuilder->setEditableFields(array('title', 'price', 'active'),'SaveTypeDelivery');

        $this->setInterface($oFormBuilder->getForm());
    }

    /**
     * Список источников информации
     */
    protected function actionSourcesInfo(){
        // -- сборка интерфейса
        $oFormBuilder = new UI\StateBuilder();
        // создаем форму
        $oFormBuilder->clear()->createFormList();
        // добавляем поля
        $oFormBuilder
            ->addField('id', 'id', 'i', 'string')
            ->addField('title', \Yii::t('order', 'field_delivery'), 's', 'string',array( 'listColumns' => array('flex' => 3) ))
            ->setFields();

        $oFormBuilder->setValue(ar\SourcesInfo::find()->asArray()->getAll());

        $oFormBuilder->addRowButtonDelete('DeleteSourcesInfo','DeleteSourcesInfo');
        $oFormBuilder->addButton(\Yii::t('adm','add'), 'AddSourcesInfo', 'icon-add','AddTypeDelivery');
        $oFormBuilder->addButton(\Yii::t('adm','back'), 'init', 'icon-cancel');

        $oFormBuilder->setEditableFields(array('title'),'SaveSourcesInfo');

        $this->setInterface($oFormBuilder->getForm());
    }

    /**
     * Добавление источников информации
     */
    protected function actionAddSourcesInfo(){
        /**
         * @var ar\TypeDeliveryRow $oParameters
         */
        $oParameters = ar\SourcesInfo::getNewRow();


        // -- сборка интерфейса
        $oFormBuilder = new UI\StateBuilder();

        // создаем форму
        $oFormBuilder->clear()->createFormEdit();

        // добавляем поля
        $oFormBuilder
            ->addField('title', \Yii::t('order', 'payment_field_title'), 's', 'string')
            ->setFields();

        $oFormBuilder->setValue($oParameters);

        // добавляем элементы управления
        $oFormBuilder->addButton(\Yii::t('adm','save'), 'saveSourcesInfo', 'icon-save','init');
        $oFormBuilder->addButton(\Yii::t('adm','back'), 'sourcesInfo', 'icon-cancel');

        $this->setInterface($oFormBuilder->getForm());
    }

    /**
     * Сохранение источников информации
     */
    protected function actionSaveSourcesInfo(){
        $oRow = new ar\SourcesInfoRow();
        $oRow->setData($this->getInData());
        $oRow->save();

        $this->actionSourcesInfo();
    }

    /**
     * Удаление источников информации
     */
    protected function actionDeleteSourcesInfo(){
        $iId = $this->getInDataValInt('id');

        if ($iId){
            ar\SourcesInfo::delete($iId);
        }
        $this->actionSourcesInfo();
    }

    /**
     * CRUD для типа доставки (add)
     */
    protected function actionAddTypeDelivery(){

        /**
         * @var ar\TypeDeliveryRow $oParameters
         */
        $oParameters = ar\TypeDelivery::getNewRow();


        // -- сборка интерфейса
        $oFormBuilder = new UI\StateBuilder();

        // создаем форму
        $oFormBuilder->clear()->createFormEdit();

        // добавляем поля
        $oFormBuilder
            ->addField('title', \Yii::t('order', 'payment_field_title'), 's', 'string')
            ->addField('price', \Yii::t('order', 'price'), 'i', 'int')
            ->addField('active', \Yii::t('order', 'active'), 'i', 'check')
            ->setFields();

        $oFormBuilder->setValue($oParameters);

        // добавляем элементы управления
        $oFormBuilder->addButton(\Yii::t('adm','save'), 'saveTypeDelivery', 'icon-save','init');
        $oFormBuilder->addButton(\Yii::t('adm','back'), 'TypeDeliveryList', 'icon-cancel');

        $this->setInterface($oFormBuilder->getForm());
    }

    /**
     * CRUD для типа доставки (delete)
     */
    protected function actionDeleteTypeDelivery(){

        $iId = $this->getInDataValInt('id');

        if ($iId){
            ar\TypeDelivery::delete($iId);
        }
        $this->actionTypeDeliveryList();
    }

    /**
     * CRUD для типа оплаты (save)
     */
    protected function actionSaveTypeDelivery(){

        $oRow = new ar\TypeDeliveryRow();
        $oRow->setData($this->getInData());
        $oRow->save();

        $this->actionTypeDeliveryList();
    }

    /**
     * CRUD для типа оплаты (list)
     */
    protected function actionTypePaymentList(){

        $oFormBuilder = UI\StateBuilder::newList();

        $oFormBuilder
            ->addField('id', 'id', 'i', 'string')
            ->addField('title', \Yii::t('order', 'payment_field_title'), 's', 'string',array( 'listColumns' => array('flex' => 3) ))
            ->addField('payment', \Yii::t('order', 'payment_field_payment'), 's', 'string',array( 'listColumns' => array('flex' => 1) ))
        ;

        $oFormBuilder->addWidget( 'payment', 'skewer\\build\\Tool\\Payments\\Api', 'getPaymentsTitle');

        $oFormBuilder->setValue(ar\TypePayment::find()->asArray()->getAll());

        $oFormBuilder->addRowButtonUpdate('EditTypePayment');
        $oFormBuilder->addRowButtonDelete('DeleteTypePayment','DeleteTypePayment');
        $oFormBuilder->addButton(\Yii::t('adm','add'), 'AddTypePayment', 'icon-add','AddTypePayment');
        $oFormBuilder->addButton(\Yii::t('adm','back'), 'init', 'icon-cancel');

        $this->setInterface($oFormBuilder->getForm());
    }

    protected function actionEditTypePayment(){

        $id = $this->getInDataValInt( 'id' );
        $this->showTypePaymentEditForm( $id );

    }

    /**
     * CRUD для типа оплаты (add)
     */
    protected function actionAddTypePayment(){

        $this->showTypePaymentEditForm();

    }

    private function showTypePaymentEditForm( $id = null ){
        /**
         * @var ar\TypePayment $oParameters
         */
        if ($id){
            $oParameters = ar\TypePayment::find($id);
        }else{
            $oParameters = ar\TypePayment::getNewRow();
        }

        $oFormBuilder = UI\StateBuilder::newEdit();

        $aItems = ArrayHelper::map(\skewer\build\Tool\Payments\Api::getPaymentsList( true ), 'type', 'title');

        // добавляем поля
        $oFormBuilder
            ->addField('id', 'id', 'i', 'hide')
            ->addField('title', \Yii::t('order', 'payment_field_title'), 's', 'string')
        ;

        if ($aItems){
            array_unshift($aItems, '---');
            $oFormBuilder->addSelectField( 'payment', \Yii::t('order', 'payment_field_payment'), 's', $aItems);
        }

        $oFormBuilder->setValue($oParameters);

        // добавляем элементы управления
        $oFormBuilder->addButton(\Yii::t('adm','save'), 'saveTypePayment', 'icon-save','init');
        $oFormBuilder->addButton(\Yii::t('adm','back'), 'TypePaymentList', 'icon-cancel');

        $this->setInterface($oFormBuilder->getForm());
    }

    /**
     * CRUD для типа оплаты (delete)
     */
    protected function actionDeleteTypePayment(){

        $iId = $this->getInDataValInt('id');

        if ($iId){
            ar\TypePayment::delete($iId);
        }
        $this->actionTypePaymentList();
    }

    /**
     * CRUD для типа оплаты (save)
     */
    protected function actionSaveTypePayment(){

        $oRow = new ar\TypePaymentRow();
        $oRow->setData($this->getInData());
        $oRow->save();

        $this->actionTypePaymentList();
    }


    protected function actionEmailShow() {

        // -- сборка интерфейса
        $oFormBuilder = UI\StateBuilder::newEdit();

        if (!$this->pageId){
            $aLanguages = Languages::getAllActive();
            $aLanguages = ArrayHelper::map($aLanguages, 'name', 'title');

            if (count($aLanguages) > 1) {
                $oFormBuilder->addFilterSelect('filter_language', $aLanguages, $this->sLanguageFilter, \Yii::t('adm','language'), ['set' => true]);
                $oFormBuilder->addFilterAction('emailShow');
            }
        }

        $aPaymentTypes = $this->getOnlinePaymentTypes();

        $oFormBuilder
            ->fieldSelect('defaultPayment',\Yii::t('order', 'mail_payment_type'),$aPaymentTypes)
            ->field('info', '', 's', 'show', ['hideLabel' => true])

            ->fieldString('title_user_mail', \Yii::t('order', 'mail_title_to_user'))
            ->field('user_content', \Yii::t('order', 'mail_to_user'), 's', 'wyswyg')

            ->fieldString('title_adm_mail', \Yii::t('order', 'mail_title_to_admin'))
            ->field('adm_content', \Yii::t('order', 'mail_to_admin'), 's', 'wyswyg')

            ->fieldString('title_change_status_mail', \Yii::t('order', 'mail_title_to_change_status'))
            ->field('status_content', \Yii::t('order', 'mail_to_change_status'), 's', 'wyswyg')
          ;

        $aModulesData = ModulesParams::getByModule('order', $this->sLanguageFilter);
        $this->setInnerData('languageFilter', $this->sLanguageFilter);

        $aItems = [];
        $aItems['info'] = \Yii::t('order', 'head_mail_text', [\Yii::t('app', 'site_label'), \Yii::t('app', 'url_label')]);
        $aItems['title_change_status_mail'] = ArrayHelper::getValue($aModulesData, 'title_change_status_mail', '');
        $aItems['status_content'] = ArrayHelper::getValue($aModulesData, 'status_content', '');
        $aItems['title_user_mail'] = ArrayHelper::getValue($aModulesData, 'title_user_mail', '');
        $aItems['user_content'] = ArrayHelper::getValue($aModulesData, 'user_content', '');
        $aItems['title_adm_mail'] = ArrayHelper::getValue($aModulesData, 'title_adm_mail', '');
        $aItems['adm_content'] = ArrayHelper::getValue($aModulesData, 'adm_content', '');
        $aItems['defaultPayment'] = \SysVar::get('defaultMailPayment');

        $oFormBuilder->setValue($aItems);

        $oFormBuilder->addButtonSave('emailSave');
        $oFormBuilder->addButtonCancel('init');

        $this->setInterface($oFormBuilder->getForm());
    }

    protected function actionEmailSave() {

        $aData = $this->getInData();

        if(!empty($aData['defaultPayment'])){
            \SysVar::set('defaultMailPayment',$aData['defaultPayment']);
        }

        $aKeys =
            [
                'title_change_status_mail', 'status_content',
                'title_user_mail', 'user_content',
                'title_adm_mail', 'adm_content'
            ];

        $sLanguage = $this->getInnerData('languageFilter');
        $this->setInnerData('languageFilter', '');

        if ($sLanguage){
            foreach( $aData as $sName => $sValue ){

                if (!in_array($sName, $aKeys))
                    continue;

                ModulesParams::setParams( 'order', $sName, $sLanguage, $sValue);
            }
        }

        $this->actionEmailShow();
    }

    /**
     * Быстрое сохранение прям из листа
     */
    protected function actionEditDetailGoods(){

        // запросить данные
        $aData = $this->get('data');

        $sPrice = str_replace(',','.',$aData['price']);
        $sCount = $aData['count'];

        // проверяем, чтоб админ не ввел бурду
        if (is_numeric($sPrice) && is_numeric($sCount)){

            $iId = $this->getInDataValInt('id');
            if ($iId) {
                /**
                 * @var ar\GoodsRow $oRow
                 */
                $oRow = ar\Goods::find($iId);
                if (!$oRow)
                    throw new UserException("Запись [$iId] не найдена");

                if (!$sPrice) {
                    $sPrice = $oRow->price;
                    $aData['price'] = $oRow->price;
                }

                $total = $sPrice*$sCount;
                if ($total>0)
                    $aData['total'] = $total;

                $oRow->setData($aData);
                $oRow->save();

                $oRow = ar\Goods::find($iId);
                $aData = $oRow->getData();

                $oListVals = new \ExtListRows();
                $oListVals->setSearchField( 'id' );
                $oListVals->addDataRow( $aData );
                $oListVals->setData( $this );
            }
        } else throw new UserException( \Yii::t('order', 'valid_data_error') );
    }

    protected function actionDeleteDetailGoods(){
        // запросить данные
        $aData = $this->get('data');

        // id записи
        $iItemId = (is_array($aData) and isset($aData['id'])) ? (int)$aData['id'] : 0;
        ar\Goods::delete($iItemId);
        $this->actionGoodsShow($aData['id_order']);
    }

    protected function actionGoodsShow($id = 0) {
        // номер заказа
        $iItemId = $this->getInnerDataInt('orderId');

        if (!$iItemId || $id) $iItemId = $id;

        $aItems =  ar\Goods::find()->where('id_order',$iItemId)->asArray()->getAll();
        // -- сборка интерфейса
        $oFormBuilder = new UI\StateBuilder();
        // создаем форму
        $oFormBuilder->clear()->createFormList();
        // добавляем поля
        $oFormBuilder
            ->addField('id', 'ID', 'i', 'string',array( 'listColumns' => array('flex' => 1) ) )
            ->addField('title', \Yii::t('order', 'field_goods_title'), 's', 'string',array( 'listColumns' => array('flex' => 3) ))
            ->addField('count', \Yii::t('order', 'field_goods_count'), 's', 'int',array( 'listColumns' => array('flex' => 3, 'editor' => ['minValue' => 1]) ))
            ->addField('price', \Yii::t('order', 'field_goods_price'), 's', 'money',array( 'listColumns' => array('flex' => 3, 'editor' => ['minValue' => 0]) ))
            ->addField('total', \Yii::t('order', 'field_goods_total'), 's', 'string',array( 'listColumns' => array('flex' => 3) ))
            ->setFields();
        $oFormBuilder->setValue($aItems);
        $oFormBuilder->addRowButtonDelete('deleteDetailGoods','deleteDetailGoods');

        $oFormBuilder->addButton(\Yii::t('adm','back'), 'show', 'icon-cancel','show',array('addParams'=>array('data'=>array('id'=>$iItemId))));

        $oFormBuilder->setEditableFields(array('price','count'),'editDetailGoods');
        $this->setInterface($oFormBuilder->getForm());
    }


    protected function actionStatusList() {

        $oFormBuilder = UI\StateBuilder::newList();

        // добавляем поля
        if (\CurrentAdmin::isSystemMode())
            $oFormBuilder->fieldHide('name', \Yii::t('order', 'status_name'), 's');

        $oFormBuilder->fieldString('title', \Yii::t('order', 'field_status'), ['listColumns' => ['flex' => 3]]);

        $aItems = Status::getList();

        $oFormBuilder->setValue($aItems);

        $oFormBuilder->setEditableFields(['title'], 'statusSave');

        $oFormBuilder->buttonRowUpdate('statusShow','statusShow');
        $oFormBuilder->buttonRowDelete('statusDelete','statusDelete');
        $oFormBuilder->button(\Yii::t('adm','add'), 'statusShow', 'icon-add','statusShow');
        $oFormBuilder->button(\Yii::t('adm','back'), 'init', 'icon-cancel');

        $this->setInterface($oFormBuilder->getForm());

    }

    protected function actionStatusShow(){

        $sName = $this->getInDataVal('name');

        if ($sName) {
            $oStatus = Status::find()
                ->where(['name' => $sName])
                ->multilingual()
                ->one()
            ;
        }
        else {
            $oStatus = new Status();
        }

        $aData = $oStatus->getAllAttributes();

        $oForm = UI\StateBuilder::newEdit();

        if (\CurrentAdmin::isSystemMode())
            $oForm->field('name', \Yii::t('order', 'status_name'), 's', $oStatus->isNewRecord ? 'string' : 'hide');

        $aLanguages = Languages::getAllActive();

        if (count($aLanguages) > 1)
            $sLabel = 'status_title_lang';
        else
            $sLabel = 'status_title';

        foreach($aLanguages as $aLanguage){
            $oForm->fieldString( 'title_' . $aLanguage['name'], \Yii::t('order', $sLabel, [$aLanguage['title']]));
        }

        $oForm->setValue($aData);

        $oForm->button(\Yii::t('adm','save'), 'StatusSave', 'icon-save','init');
        $oForm->button(\Yii::t('adm','back'), 'StatusList', 'icon-cancel');

        $this->setInterface($oForm->getForm());
    }

    protected function actionStatusDelete() {

        $name = ArrayHelper::getValue($this->get('data'), 'name', '');
        if ($name){
            if (!Status::canBeDeleted($name)){
                throw new UserException( \Yii::t('order', 'status_delete_error') );
            }
            Status::deleteAll(['name' => $name]);
        }

        $this->actionStatusList();
    }

    /**
     * Сохранение статуса
     */
    protected function actionStatusSave() {

        $aData = $this->get('data');
        $sName = $this->getInDataVal('name');

        if (!$aData || !$sName)
            throw new UserException(\Yii::t('order', 'valid_data_error'));

        if (isset($aData['title']))
            $aData['title_'.\Yii::$app->language] = $aData['title'];

        /**
         * @var Status $oStatus
         */
        $oStatus = Status::find()
            ->multilingual()
            ->where(['name' => $sName])
            ->one()
        ;

        if (!$oStatus){
            $oStatus = new Status();
            $oStatus->name = $sName;
        }

        $oStatus->setLangData($aData);

        $oStatus->save();

        $this->actionStatusList();
    }


    protected function actionEditLicense() {

        // -- сборка интерфейса
        $oFormBuilder = UI\StateBuilder::newEdit();

        if (!$this->pageId){
            $aLanguages = Languages::getAllActive();
            $aLanguages = ArrayHelper::map($aLanguages, 'name', 'title');

            if (count($aLanguages) > 1) {
                $oFormBuilder->addFilterSelect('filter_language', $aLanguages, $this->sLanguageFilter, \Yii::t('adm','language'), ['set' => true]);
                $oFormBuilder->addFilterAction('editLicense');
            }
        }

        $aData['license'] = ModulesParams::getByName('order', 'license', $this->sLanguageFilter);
        $this->setInnerData('languageFilter', $this->sLanguageFilter);

        // добавляем поля
        $oFormBuilder
            ->field( 'license', \Yii::t('order', 'license_edit'), 's', 'wyswyg' )
            ->setValue( $aData )
            ->addButtonSave( 'saveLicense' )
            ->addButtonCancel( 'list' )
        ;

        // вывод данных в интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

    }


    protected function actionSaveLicense() {

        $sLanguage = $this->getInnerData('languageFilter');
        $this->setInnerData('languageFilter', '');

        if ($sLanguage) {
            ModulesParams::setParams('order', 'license', $sLanguage, $this->getInDataVal('license'));
        }

        $this->actionList();
    }

    /**
     * @return array
     */
    private function getOnlinePaymentTypes(){
        $aPaymentTypes = [];
        $aOnlinePayments = Adm\Order\model\Payments::find()
            ->where("payment not LIKE ''")
            ->asArray()
            ->all();
        foreach ($aOnlinePayments as $value){
            $aPaymentTypes[$value['id']]=$value['payment'];
        }
        return $aPaymentTypes;
    }

}