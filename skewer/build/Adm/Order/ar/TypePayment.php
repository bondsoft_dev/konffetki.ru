<?php
/**
 * User: Max
 * Date: 25.06.14
 */

namespace skewer\build\Adm\Order\ar;
use skewer\build\libs\ft;
use skewer\build\Component\orm;

class TypePayment extends orm\TablePrototype {

    protected static $sTableName = 'orders_payment';
    protected static $sKeyField = 'id';

    protected static function initModel() {

        ft\Entity::get('orders_payment')
            ->clear(false)
            ->setPrimaryKey(self::$sKeyField)
            ->setTablePrefix('')
            ->setNamespace(__NAMESPACE__)
            ->addField('title', 'varchar(255)', 'order.payment_field_title')
            ->addField('payment', 'varchar(64)', 'order.payment_field_payment')
            ->addDefaultProcessorSet()

            ->addColumnSet(
                'list',
                array('id','title')
            )
            ->save()
            //->build()
        ;
    }

    public static function getValue($id){
        /**
         * @var TypePaymentRow $model
         */
        $model = TypePayment::find($id);
        if ($model) return $model->title;
        return '';
    }

    public static function getNewRow($aData = array()) {
        $oRow = new TypePaymentRow();

        if ($aData)
            $oRow->setData($aData);

        return $oRow;
    }
}