<?php
/**
 * User: Max
 * Date: 25.06.14
 */

namespace skewer\build\Adm\Order\ar;
use skewer\build\Component\orm;
use skewer\build\libs\ft;
use skewer\build\Adm\Params;

class TypeDeliveryRow extends orm\ActiveRecord {

    public $id = 'NULL';
    public $title = '';
    public $price = 0;
    public $active = 0;

    function __construct() {
        $this->setTableName( 'orders_delivery' );
        $this->setPrimaryKey( 'id' );
    }
}