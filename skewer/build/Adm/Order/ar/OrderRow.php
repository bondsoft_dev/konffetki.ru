<?php

namespace skewer\build\Adm\Order\ar;

use skewer\build\Component\orm;

class OrderRow extends orm\ActiveRecord {
    public $id = 0;
    public $date = '';
//    public $address = '';
    // фио или контактное лицо
    public $person = '';
    //телефон
    public $phone = '';
    //email
    public $mail = '';
    public $status = '';
//    public $postcode = '';
    public $type_payment = 0;
    public $type_delivery = 0;
    public $source_info = 0;
    public $text = '';
    public $notes = '';
    public $token= '';
    public $auth= '';
    public $deliv_cost = 0;

    //данные для всех 3 типов
    //адрес доставки
    public $delivery_region = '';
    public $delivery_city = '';
    public $delivery_street = '';
    public $delivery_house = '';
    public $delivery_housing = '';
    public $delivery_room = '';

    //для юриков и ип
    //правовая форма
    public $legal_form = '';
    //название организации
    public $organization = '';
    //ИНН
    public $inn = '';
    //КПП
    public $kpp = '';

    //юридический адрес
    public $legal_region = '';
    public $legal_city = '';
    public $legal_street = '';
    public $legal_house = '';
    public $legal_housing = '';
    public $legal_room = '';

    //фактический адрес
    public $actual_region = '';
    public $actual_city = '';
    public $actual_street = '';
    public $actual_house = '';
    public $actual_housing = '';
    public $actual_room = '';

    //тип формы
    public $type_form = 1;



    function __construct() {
        $this->setTableName( 'orders' );
        $this->setPrimaryKey( 'id' );
    }

    public function preSave()
    {
        if (!$this->date || $this->date === 'null'){
            $this->date = date( "Y-m-d H:i:s", time() );
        }

        parent::preSave();
    }


}