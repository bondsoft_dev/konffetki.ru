<?php

namespace skewer\build\Adm\Order\ar;
use skewer\build\libs\ft;
use skewer\build\Component\orm;

class SourcesInfo extends orm\TablePrototype {

    protected static $sTableName = 'orders_sources_info';
    protected static $sKeyField = 'id';

    protected static function initModel() {

        ft\Entity::get('orders_sources_info')
            ->clear(false)
            ->setPrimaryKey(self::$sKeyField)
            ->setTablePrefix('')
            ->setNamespace(__NAMESPACE__)
            ->addField('title', 'varchar(255)', 'order.payment_field_title')
            ->addDefaultProcessorSet()

            ->addColumnSet(
                'list',
                array('id','title')
            )
            ->save()
            //->build()
        ;
    }

    public static function getValue($id){
        /**
         * @var TypePaymentRow $model
         */
        $model = SourcesInfo::find($id);
        if ($model) return $model->title;
        return '';
    }

    public static function getNewRow($aData = array()) {
        $oRow = new SourcesInfoRow();

        if ($aData)
            $oRow->setData($aData);

        return $oRow;
    }
}