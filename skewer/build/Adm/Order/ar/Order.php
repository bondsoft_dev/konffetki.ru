<?php
/**
 * Created by PhpStorm.
 * User: Max
 * Date: 23.04.14
 * Time: 15:54
 */

namespace skewer\build\Adm\Order\ar;
use skewer\build\libs\ft;
use skewer\build\Component\orm;

class Order extends orm\TablePrototype {

    protected static $sTableName = 'orders';
    protected static $sKeyField = 'id';


    /**
     * Объявление сущности
     */
    protected static function initModel() {

        ft\Entity::get( self::$sTableName)
            ->clear(false)
            ->setPrimaryKey(self::$sKeyField)
            ->setTablePrefix('')
            ->setNamespace(__NAMESPACE__)
            ->addField( 'date', 'datetime', 'field_date' )
//            ->addField( 'address', 'varchar(255)', 'field_address' )
            ->addField( 'person', 'varchar(255)', 'field_contact_face' )
            ->addField( 'phone', 'varchar(255)', 'field_phone' )
            ->addField( 'mail', 'varchar(255)', 'field_mail' )
//            ->addField( 'postcode', 'varchar(255)', 'field_postcode' )
            ->addField('status','int(11)','field_status')

            ->addField('type_payment','int(11)','field_payment')
            ->addField('type_delivery','int(11)','field_delivery')
            ->addField('source_info','int(11)','adm_field_sources_info')

            ->addField('text','text','field_text')
            ->addField('token','varchar(255)','field_token')
            ->addField('notes','text','field_notes')
            ->addField('auth','int(11)','field_user_id')

            ->addField('deliv_cost','int(11)','field_deliv_cost')

            //адрес доставки
            ->addField( 'delivery_region', 'varchar(255)', 'field_delivery_region' )
            ->addField( 'delivery_city', 'varchar(255)', 'field_delivery_city' )
            ->addField( 'delivery_street', 'varchar(255)', 'field_delivery_street' )
            ->addField( 'delivery_house', 'varchar(255)', 'field_delivery_house' )
            ->addField( 'delivery_housing', 'varchar(255)', 'field_delivery_housing' )
            ->addField( 'delivery_room', 'varchar(255)', 'field_delivery_room' )

            //для юриков и ип
            ->addField( 'legal_form', 'varchar(255)', 'field_legal_form' )
            ->addField( 'organization', 'varchar(255)', 'field_organization' )
            ->addField( 'inn', 'varchar(255)', 'field_inn' )
            ->addField( 'kpp', 'varchar(255)', 'field_kpp' )

            //юридический адрес
            ->addField( 'legal_region', 'varchar(255)', 'field_legal_region' )
            ->addField( 'legal_city', 'varchar(255)', 'field_legal_city' )
            ->addField( 'legal_street', 'varchar(255)', 'field_legal_street' )
            ->addField( 'legal_house', 'varchar(255)', 'field_legal_house' )
            ->addField( 'legal_housing', 'varchar(255)', 'field_legal_housing' )
            ->addField( 'legal_room', 'varchar(255)', 'field_legal_room' )

            //фактический адрес
            ->addField( 'actual_region', 'varchar(255)', 'field_actual_region' )
            ->addField( 'actual_city', 'varchar(255)', 'field_actual_city' )
            ->addField( 'actual_street', 'varchar(255)', 'field_actual_street' )
            ->addField( 'actual_house', 'varchar(255)', 'field_actual_house' )
            ->addField( 'actual_housing', 'varchar(255)', 'field_actual_housing' )
            ->addField( 'actual_room', 'varchar(255)', 'field_actual_room' )

            ->addField('type_form','int(11)','field_type_form')

            ->addDefaultProcessorSet()
            ->addColumnSet(
                'list',
                array( 'id','date','person','mail','status' )
            )
            ->addColumnSet(
                'edit',
                array( 'id','date','person','postcode','address','phone','mail','status','type_payment','type_delivery','source_info','text','notes' )
            )
            ->addColumnSet(
                'mail',
                array( 'person','postcode','address','phone','mail','type_payment','type_delivery','source_info','text' )
            )
            ->addColumnSet(
                'individial_mail',
                array( 'person','postcode','address','phone','mail','type_payment','type_delivery','source_info','text', 'delivery_region', 'delivery_city', 'delivery_street', 'delivery_house', 'delivery_housing', 'delivery_room'  )
            )
            ->addColumnSet(
                'individ_entrep_mail',
                array( 'person','postcode','address','phone','mail','type_payment','type_delivery','source_info','text', 'legal_form', 'inn' )
            )
            ->addColumnSet(
                'entity_mail',
                array( 'person','postcode','address','phone','mail','type_payment','type_delivery','source_info','text', 'legal_form', 'inn', 'kpp', 'organization' )
            )
            ->addColumnSet(
                'delivery_mail',
                array( 'delivery_region', 'delivery_city', 'delivery_street', 'delivery_house', 'delivery_housing', 'delivery_room'  )
            )
            ->addColumnSet(
                'legal_mail',
                array( 'legal_region', 'legal_city', 'legal_street', 'legal_house', 'legal_housing', 'legal_room' )
            )
            ->addColumnSet(
                'actual_mail',
                array( 'actual_region', 'actualy_city', 'actual_street', 'actual_house', 'actual_housing', 'actual_room' )
            )

            ->save()
            ->build()
        ;
    }

    public static function getNewRow($aData = array()) {
        $oRow = new OrderRow();
        if ($aData)
            $oRow->setData($aData);
        return $oRow;
    }

    public static function delete($id = null){
        /**
         * надо удалить и товары, привязанные к заказу
         */
        if ($id){
            $aGoods = Goods::find()->where('id_order',$id)->getAll();
            if ($aGoods){
                /**
                 * @var $oGoodsRow GoodsRow
                 */
                foreach($aGoods as $oGoodsRow){
                    Goods::delete($oGoodsRow->id);
                }
            }
        }else{
            Goods::delete();
        }

        parent::delete($id);
    }

}