<?php
/**
 * User: Max
 * Date: 25.06.14
 */

namespace skewer\build\Adm\Order\ar;
use skewer\build\libs\ft;
use skewer\build\Component\orm;

class TypeDelivery extends orm\TablePrototype {

    protected static $sTableName = 'orders_delivery';
    protected static $sKeyField = 'id';

    protected static function initModel() {

        ft\Entity::get('orders_delivery')
            ->clear(false)
            ->setPrimaryKey(self::$sKeyField)
            ->setTablePrefix('')
            ->setNamespace(__NAMESPACE__)
            ->addField('title', 'varchar(255)', 'order.payment_field_title')
            ->addDefaultProcessorSet()

            ->addColumnSet(
                'list',
                array('id','title')
            )
            ->save()
            ->build()
        ;
    }

    public static function getNewRow($aData = array()) {
        $oRow = new TypeDeliveryRow();

        if ($aData)
            $oRow->setData($aData);

        return $oRow;
    }

    /**
     * Получаем знчение по id
     * @param $id
     * @return string
     */
    public static function getValue($id){
        /**
         * @var TypeDeliveryRow $model
         */
        $model = TypeDelivery::find($id);
        if ($model) return $model->title;
        return '';
    }

} 