<?php

namespace skewer\build\Adm\Order\ar;
use skewer\build\Component\orm;
use skewer\build\libs\ft;
use skewer\build\Adm\Params;

class SourcesInfoRow extends orm\ActiveRecord {

    public $id = 'NULL';
    public $title = '';

    function __construct() {
        $this->setTableName( 'orders_sources_info' );
        $this->setPrimaryKey( 'id' );
    }
}