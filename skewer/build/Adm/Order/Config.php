<?php

/* main */
$aConfig['name']     = 'Order';
$aConfig['title']    = 'Заказы (админ)';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Админ-интерфейс управления модулем заказов';
$aConfig['revision'] = '0002';
$aConfig['layer']     = Layer::ADM;

return $aConfig;
