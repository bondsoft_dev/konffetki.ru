<?php

namespace skewer\build\Adm\Order\model;

use skewer\build\Page\Cart\Api;
use Yii;

/**
 * This is the model class for table "orders_delivery".
 *
 * @property integer $id
 * @property string $title
 * @property integer $price
 * @property integer $active
 */
class Delivery extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'orders_delivery';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title'], 'required'],
            [['title'], 'string', 'max' => 255],
            [['active','price'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'active' => 'active',
            'price' => 'price',
        ];
    }

    /**
     * возвращает запись о доставке по id
     * @param $iId
     * @return array|\yii\db\ActiveRecord|null
     */
    public static function getById($iId)
    {
        return self::find()
            ->where(['id' => $iId])
            ->asArray()
            ->one();
    }

    /**
     * возвращает список доставок
     * @return array|\yii\db\ActiveRecord[]
     */
    public static function getList()
    {
        $listDelivery = self::find()
            ->where(['active' => 1])
            ->asArray()
            ->all();

        foreach ($listDelivery as &$delivery) {
            $delivery['price'] = Api::priceFormatAjax($delivery['price']);
        }

        return $listDelivery;
    }

    /**
     * возвращает стоимость доставки
     * @param $iId
     * @return int
     */
    public static function getDeliveryPrice($iId)
    {
        return self::getById($iId)['price'];
    }
}
