<?php

$aLanguage = array();

$aLanguage['ru']['order_form_title'] = 'Форма заказа товаров';

$aLanguage['ru']['cart_section_title'] = 'Корзина';

$aLanguage['ru']['order_form_field_goods_name'] = 'Наименование товара';
$aLanguage['ru']['order_form_field_person'] = 'Контактное лицо';
$aLanguage['ru']['order_form_field_phone'] = 'Телефон';
$aLanguage['ru']['order_form_field_email'] = 'E-mail';
$aLanguage['ru']['order_form_field_text'] = 'Дополнительные пожелания';

$aLanguage['ru']['order_form_add_answer_title'] = 'Ваш заказ на [адрес сайта]';
$aLanguage['ru']['order_form_add_answer_body'] = 'Уважаемый пользователь!
Вы оформили заказ на сайте [адрес сайта].
В ближайшее время мы свяжемся с вами по указанным контактным данным.
__
С уважением, администрация сайта [название сайта]';
$aLanguage['ru']['order_form_add_agreed_title'] = '';
$aLanguage['ru']['order_form_add_agreed_text'] = '';

$aLanguage['ru']['status_new'] = 'Новый';
$aLanguage['ru']['status_paid'] = 'Заказ оплачен';
$aLanguage['ru']['status_formed'] = 'Заказ сформирован';
$aLanguage['ru']['status_send'] = 'Заказ отправлен';
$aLanguage['ru']['status_close'] = 'Заказ обработан и закрыт';
$aLanguage['ru']['status_fail'] = 'Заказ не оплачен';
$aLanguage['ru']['status_cancel'] = 'Заказ отменен';

$aLanguage['ru']['title_change_status_mail'] = 'Смена статуса заказа';
$aLanguage['ru']['status_content'] = '<p>​Уважаемый пользователь!<br />
Статус заказа №[order_id] на сайте [site] был изменен c [before_status] на [after_status].<br />
<br />
Чтобы посмотреть заказ, перейдите по [link]<br />
<br />
[order_info]<br />
<br />
__<br />
С уважением, администрация сайта [site]</p>
';
$aLanguage['ru']['title_user_mail'] = 'Ваш заказ';
$aLanguage['ru']['user_content'] = '<p>​Уважаемый пользователь!<br />
Вами был сделан заказ №[order_id] на сайте [site].<br />
<br />
Чтобы посмотреть его, перейдите по [link]<br />
[order_info]<br />
<br />
__<br />
С уважением, администрация сайта [site]</p>
';
$aLanguage['ru']['title_adm_mail'] = 'Новый заказ товара';
$aLanguage['ru']['adm_content'] = '<p>Уважаемый администратор!<br />
На вашем сайте [site] был сделан заказ.<br />
<br />
[order_info]</p>
';

$aLanguage['ru']['license'] = '<h2>Соглашение об обработке персональных данных</h2>

<p>Данное соглашение об обработке персональных данных разработано в соответствии с законодательством Российской Федерации.<br />
Все граждане, заполнившие сведения, составляющие персональные данные, на сайте или на любой его странице, а также разместившие иную информацию обозначенными действиями, подтверждают свое согласие на:</p>

<ul>
	<li>подписку на электронные почтовые рассылки с информационными и рекламными целями;</li>
	<li>обработку персональных данных;</li>
	<li>передачу персональных данных оператору по их обработке.</li>
</ul>

<p><strong>Под персональными данными Гражданина понимается:</strong></p>

<ul>
	<li>общая информация (Ф.И.О);</li>
	<li>электронная почта (e-mail);</li>
	<li>номер телефона;</li>
	<li>компания и должность.</li>
</ul>

<p>Гражданин, принимая настоящее Соглашение, выражает свою заинтересованность и полное согласие, что обработка его персональных данных может включать в себя следующие действия: сбор, систематизацию, накопление, хранение, уточнение (обновление, изменение), использование, уничтожение.</p>

<p><strong>Гражданин гарантирует:</strong></p>

<ul>
	<li>информация, им предоставленная, является полной, точной и достоверной;</li>
	<li>при предоставлении информации не нарушается действующее законодательство Российской Федерации, законные права и интересы третьих лиц;</li>
	<li>вся предоставленная информация заполнена Гражданином в отношении себя лично.</li>
</ul>
';

$aLanguage['en']['order_form_title'] = 'Ordering products';

$aLanguage['en']['cart_section_title'] = 'Cart';

$aLanguage['en']['order_form_field_goods_name'] = 'Name of product';
$aLanguage['en']['order_form_field_person'] = 'The contact person';
$aLanguage['en']['order_form_field_phone'] = 'Phone';
$aLanguage['en']['order_form_field_email'] = 'E-mail';
$aLanguage['en']['order_form_field_text'] = 'Additional wishes';

$aLanguage['en']['order_form_add_answer_title'] = 'Your order on [url]';
$aLanguage['en']['order_form_add_answer_body'] = 'Dear user!
You place your order at [url].
In the near future we will contact you by model.
__
Sincerely, Administration site [site]';
$aLanguage['en']['order_form_add_agreed_title'] = '';
$aLanguage['en']['order_form_add_agreed_text'] = '';

$aLanguage['en']['status_new'] = 'new';
$aLanguage['en']['status_paid'] = 'Order paid';
$aLanguage['en']['status_formed'] = 'Order formed';
$aLanguage['en']['status_send'] = 'The order has been sent';
$aLanguage['en']['status_close'] = 'orders are processed and closed';
$aLanguage['en']['status_fail'] = 'Order has not been paid';
$aLanguage['en']['status_cancel'] = 'Order canceled';

$aLanguage['en']['title_change_status_mail'] = 'Change the order status';
$aLanguage['en']['status_content'] = '<p> Dear user! <br />
Order status № [order_id] site [site] was changed c [before_status] on [after_status]. <br />
<br />
To see the order, go to [link] <br />
<br />
[order_info] <br />
<br />
__ <br />
Sincerely, Administration site [site] </p>
';
$aLanguage['en']['title_user_mail'] = 'Your order';
$aLanguage['en']['user_content'] = '<p> Dear user! <br />
Your order was placed № [order_id] site [site]. <br />
<br />
To see it, go to [link] <br />
[order_info] <br />
<br />
__ <br />
Sincerely, Administration site [site] </p>
';
$aLanguage['en']['title_adm_mail'] = 'A new order of the goods';
$aLanguage['en']['adm_content'] = '<p> Dear administrator! <br />
On your site [site] order was placed. <br />
<br />
[order_info] </p>
';
$aLanguage['en']['license'] = "<h2> The agreement on the processing of personal data</h2>
<p> The agreement on the processing of personal data developed in accordance with the legislation of the Russian Federation. <br />
All citizens who filled the information constituting any personal data on the site or on any of its page and also post other information designated actions confirm their agreement to: </p>
<ul> <li> subscribe to the electronic mailing list with information and advertising purposes; </li> <li> the processing of personal data; </li> <li> the transfer of personal data to the operator's processing. </li>
</ul>
<p> <strong> Under the personal data refers to the Citizen: </strong> </p>
<ul> <li> general information (full name); </li> <li> e-mail (e-mail); </li> <li> phone number; </li> <li> the company and position. </li>
</ul>
<p> Citizen, taking this Agreement, expressed their interest and complete agreement that the processing of personal data may include the following: the collection, systematization, accumulation, storage, clarification (update, change), use, destruction. </p >
<p> <strong> A citizen guarantees: </strong> </p>
<ul> <li> the information they provided is complete, accurate and reliable; </li> <li> When the information does not violate any applicable laws of the Russian Federation, the legitimate rights and interests of third parties; </li> <li> All Citizen of the information provided in the full respect of myself. </li>
</ul>
";

return $aLanguage;