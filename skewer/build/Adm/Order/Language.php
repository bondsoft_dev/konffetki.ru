<?php

$aLanguage = array();

$aLanguage['ru']['Order.Adm.tab_name'] = 'Заказы';
$aLanguage['ru']['field_id'] = '№';
$aLanguage['ru']['field_date'] = 'Дата';
$aLanguage['ru']['field_address'] = 'Адрес';
$aLanguage['ru']['field_contact_face'] = 'Контактное лицо';
$aLanguage['ru']['field_phone'] = 'Телефон';
$aLanguage['ru']['field_mail'] = 'E-mail';
$aLanguage['ru']['field_postcode'] = 'Почтовый индекс';
$aLanguage['ru']['field_status'] = 'Статус';
$aLanguage['ru']['status_name'] = 'Имя статуса';
$aLanguage['ru']['status_title'] = 'Название статуса';
$aLanguage['ru']['status_title_lang'] = 'Название статуса ({0})';
$aLanguage['ru']['field_text'] = 'Дополнительные пожелания';
$aLanguage['ru']['field_notes'] = 'Комментарии менеджера';
$aLanguage['ru']['field_user_id'] = 'User id';
$aLanguage['ru']['field_goods_article'] = 'Артикул';
$aLanguage['ru']['order_number'] = 'Номер заказа';
$aLanguage['ru']['history_list'] = 'История изменения статусов';
$aLanguage['ru']['active'] = 'Активность';

$aLanguage['ru']['field_change_data'] = 'Дата изменения';
$aLanguage['ru']['field_old_status'] = 'Старый статус';
$aLanguage['ru']['field_new_status'] = 'Статус';
$aLanguage['ru']['status_delete_error'] = 'Статус является системным и не может быть удален!';

$aLanguage['ru']['valid_data_error'] = 'Некорректные данные';

$aLanguage['ru']['field_orderstatus_title'] = 'Статусы';

$aLanguage['ru']['field_mailtext'] = 'Редактор писем';

$aLanguage['ru']['field_goods_title'] = 'Наименование товара';
$aLanguage['ru']['field_goods_title_edit'] = 'Редактор заказа';

$aLanguage['ru']['field_goods_count'] = 'Кол-во';
$aLanguage['ru']['field_goods_measure'] = 'Ед. изм';
$aLanguage['ru']['field_goods_total'] = 'Итого';
$aLanguage['ru']['field_deliv_cost'] = 'За доставку';
$aLanguage['ru']['field_deliv_total'] = 'К оплате';
$aLanguage['ru']['field_goods_price'] = 'Цена';
$aLanguage['ru']['field_goods_id_order'] = 'id Товара';

$aLanguage['ru']['field_payment'] = 'Тип оплаты';
$aLanguage['ru']['field_sources_info'] = 'Как вы узнали о нас?';
$aLanguage['ru']['adm_field_sources_info'] = 'Как вы узнали о нас?';
$aLanguage['ru']['field_delivery'] = 'Тип доставки';

$aLanguage['ru']['tp_deliv_1'] = 'Стоимость доставки Самовывозом';
$aLanguage['ru']['tp_deliv_2'] = 'Стоимость доставки Курьером';
$aLanguage['ru']['tp_deliv_3'] = 'Стоимость доставки в пределах МКАД';
$aLanguage['ru']['tp_deliv_4'] = 'Стоимость доставки по Московской области';
$aLanguage['ru']['tp_deliv_5'] = 'Стоимость по Почте России';
$aLanguage['ru']['tp_deliv_6'] = 'Стоимость доставки мой заказ более 5500 рублей';
$aLanguage['ru']['tp_deliv_7'] = 'Стоимость доставки мкр. Московские водники';

$aLanguage['ru']['license_edit'] = 'Лицензионное соглашение';

$aLanguage['ru']['order_description'] = 'Заказ № {0, number}';
$aLanguage['ru']['button_label'] = 'Оплатить';
$aLanguage['ru']['payment_field_title'] = 'Название';
$aLanguage['ru']['payment_field_payment'] = 'Тип оплаты';
$aLanguage['ru']['goods_info'] = 'Описание товара';

$aLanguage['ru']['current_currency'] = 'руб';
$aLanguage['ru']['total'] = 'Итого';
$aLanguage['ru']['payable'] = 'К оплате';
$aLanguage['ru']['deliver'] = 'За доставку';

$aLanguage['ru']['mail_to_user'] = 'Письмо для пользователя';
$aLanguage['ru']['mail_to_admin'] = 'Письмо для админа';
$aLanguage['ru']['mail_to_change_status'] = 'Письмо смены статуса';

$aLanguage['ru']['mail_payment_type'] = 'Тип оплаты в письме';

$aLanguage['ru']['mail_title_to_user'] = 'Заголовок письма для пользователя';
$aLanguage['ru']['mail_title_to_admin'] = 'Заголовок письма для админа';
$aLanguage['ru']['mail_title_to_change_status'] = 'Заголовок письма смены статуса';

$aLanguage['ru']['your_order'] = "Ваш заказ";
$aLanguage['ru']['new_order'] = "Новый заказ";
$aLanguage['ru']['change_status'] = "Смена статуса";

$aLanguage['ru']['order_data'] = "Данные заказа";

$aLanguage['ru']['head_mail_text'] = 'Метки для замены:<br>
[{0}] - Название сайта<br>
[{1}] - Адрес сайта<br>
[link] - Ссылка на заказ<br>
[order_id] - Номер заказа<br>
[before_status] - Информация о заказе<br>
[after_status] - Старый статус<br>
[order_info] - Новый статус<br>
[pay_link] - ссылка для оплаты<br>
';

$aLanguage['en']['Order.Adm.tab_name'] = 'Order';
$aLanguage['en']['field_id'] = '№';
$aLanguage['en']['field_date'] = 'Date';
$aLanguage['en']['field_address'] = 'Address';
$aLanguage['en']['field_contact_face'] = 'Contact face';
$aLanguage['en']['field_phone'] = 'Phone';
$aLanguage['en']['field_mail'] = 'Mail';
$aLanguage['en']['field_status'] = 'Status';
$aLanguage['en']['field_text'] = 'Info';
$aLanguage['en']['field_notes'] = 'Manager comments';
$aLanguage['en']['field_user_id'] = 'User id';
$aLanguage['en']['field_payment'] = 'Payment';
$aLanguage['en']['field_sources_info'] = 'Sources info';
$aLanguage['en']['adm_field_sources_info'] = 'Sources info';
$aLanguage['en']['field_delivery'] = 'Delivery';
$aLanguage['en']['field_postcode'] = 'postcode';
$aLanguage['en']['status_name'] = 'Status name';
$aLanguage['en']['status_title'] = 'Title status';
$aLanguage['en']['status_title_lang'] = 'Title status ({0})';

$aLanguage['en']['field_orderstatus_title'] = 'Status';
$aLanguage['en']['status_delete_error'] = 'Status is a system and can not be deleted!';

$aLanguage['en']['valid_data_error'] = 'No valid data';

$aLanguage['en']['license_edit'] = 'License';

$aLanguage['en']['field_goods_title'] = 'Title';
$aLanguage['en']['field_goods_count'] = 'Count';
$aLanguage['en']['field_goods_article'] = 'Article';
$aLanguage['en']['field_goods_total'] = 'Total';
$aLanguage['en']['field_goods_price'] = 'Price';
$aLanguage['en']['field_goods_id_order'] = 'id Order';
$aLanguage['en']['order_description'] = 'Order # {0, number}';
$aLanguage['en']['button_label'] = 'Paid';
$aLanguage['en']['payment_field_title'] = 'Title';
$aLanguage['en']['payment_field_payment'] = 'Type of payment';
$aLanguage['en']['field_mailtext'] = 'Text of the letter';
$aLanguage['en']['order_number'] = 'Order number';
$aLanguage['en']['goods_info'] = 'Goods';

$aLanguage['en']['current_currency'] = 'RUR';
$aLanguage['en']['total'] = 'Total';
$aLanguage['en']['payable'] = 'Total cost';

$aLanguage['en']['mail_to_user'] = 'Mail to user';
$aLanguage['en']['mail_to_admin'] = 'Mail to admin';
$aLanguage['en']['mail_to_change_status'] = 'Mail to change status';

$aLanguage['en']['mail_title_to_user'] = 'Subject to the user';
$aLanguage['en']['mail_title_to_admin'] = 'Subject to the administrator';
$aLanguage['en']['mail_title_to_change_status'] = 'Subject status change';

$aLanguage['en']['your_order'] = "Your order";
$aLanguage['en']['new_order'] = "New order";
$aLanguage['en']['change_status'] = "Change status";
$aLanguage['en']['field_goods_title_edit'] = 'Order edit';
$aLanguage['en']['history_list'] = 'Status change history';

$aLanguage['en']['field_change_data'] = 'Date change';
$aLanguage['en']['field_old_status'] = 'Old status';
$aLanguage['en']['field_new_status'] = 'New status';

$aLanguage['en']['order_data'] = "Order data";

$aLanguage['en']['head_mail_text'] = 'Tags for replacement: <br>
[{0}] - The name of the site <br>
[{1}] - Site Address <br>
[link] - Link to order <br>
[order_id] - Order <br>
[before_status] - Ordering information <br>
[after_status] - Old status <br>
[order_info] - New status <br>
';

return $aLanguage;