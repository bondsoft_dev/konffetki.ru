<?php
/**
 *
 * @project Skewer
 * @package Modules
 *
 * @author kolesnikov,max $Author: $
 * @version $Revision:  $
 * @date $Date: $
 */

namespace skewer\build\Adm\Order;

use skewer\build\Adm\Order\model\ChangeStatus;
use skewer\build\Adm\Order\model\Delivery;
use skewer\build\Adm\Order\model\Status;
use skewer\build\Component\Catalog;
use skewer\build\Component\orm\ActiveRecord;
use skewer\build\Component\orm\Query;
use skewer\build\Component\Section\Parameters;
use skewer\build\libs;
use skewer\build\Page\Auth\Api;
use skewer\build\Page\Auth\UserType;
use skewer\build\Page\Forms as PageForms;
use skewer\build\Component\Forms;
use skewer\build\Page\Cart as Cart;
use \skewer\build\Component\I18N\ModulesParams;
use SysVar;
use yii\helpers\ArrayHelper;
use skewer\build\Adm\Order as AdmOrder;

/**
 * @todo отрефакторить класс. Добавить языки в заказы (пользователи). Парсинг письма клиенту с нужным языком.
 * Class Service
 * @package skewer\build\Adm\Order
 */
class Service extends \ServicePrototype {

    private static $aStatus = array();

    protected static function buildArrayStatus() {

        static::$aStatus = ArrayHelper::map(Status::getList(false, \Yii::$app->language), 'id', 'title');
    }

    /**
     * Список статусов
     * @return array
     */
    public static function getStatusList() {

        if (empty(static::$aStatus)) {
            static::buildArrayStatus();
        }

        return static::$aStatus;
    }

    /**
     * Метод отправки письма
     * @param string $sMail email кому отправляем
     * @param string $sTitle заголовок письма
     * @param string $sBody текст письма
     * @param array $aOptions массив меток для автозамены
     * @return bool
     */
    public static function sendMail($sMail, $sTitle, $sBody, $aOptions = array()) {

        if (isset($aOptions['token']))
            $aOptions['link'] = '<a href="' . \Site::httpDomain() . Api::getProfilePath() . '?cmd=detail&token=' . $aOptions['token'] . '">'.\Yii::t('adm','by_link').'</a>';

        return $oMailer = \Mailer::sendMail($sMail, $sTitle, $sBody, $aOptions);

    }


    /**
     * Заголовок статуса (для виджета)
     * @param ActiveRecord $oItem
     * @param $sField
     * @return string
     */
    public static function getStatusValue($oItem, $sField){

        $aStatulList = static::getStatusList();

        if (isset($aStatulList[$oItem['status']])){
            return $aStatulList[$oItem['status']];
        } else return '';
    }

    /**
     * Письмо о смене статуса
     * @param int $iOrderId
     * @param int $iBeforeStatus
     * @param int $iAfterStatus
     */
    public static function sendMailChangeOrderStatus($iOrderId, $iBeforeStatus, $iAfterStatus) {

        $aStatus = Status::getListTitle();

        /**
         * @var ar\OrderRow $oRow
         */
        $oRow = ar\Order::find($iOrderId);

        /**
         * @todo какова хрена тут сохранение? рефакторить!
         */

        // тут надо отправить email о смене статуса, сделал в лоб, скорее всего надо будет переписать
        if (isset($aStatus[$oRow->status]) || $aStatus[$iAfterStatus]) {

            $oChangeRowStatus = new ChangeStatus();
            $oChangeRowStatus->change_date = date('Y-m-d H:i:s');
            $oChangeRowStatus->id_old_status = $iBeforeStatus;
            $oChangeRowStatus->id_new_status = $iAfterStatus;
            $oChangeRowStatus->id_order = $iOrderId;
            $oChangeRowStatus->save();

            $aVars = $oRow->getData();

            $aDataOrder = array();

            // передаваемые метки
            $aAllowFields = array(
                'person','postcode','address','phone','mail','type_payment','type_delivery','text'
            );

            foreach ($aAllowFields as $item) {
                // только нужные нам поля

                if (!isset($aVars[$item])) continue;

                $sLangTitle = \Yii::t('order', ar\Order::getModel()->getFiled($item)->getTitle());

                if ($item == 'type_payment'){
                    $aDataOrder[] = array('title' => $sLangTitle, 'value' => ar\TypePayment::getValue($aVars[$item]));
                } else
                    if ($item == 'type_delivery'){
                        $aDataOrder[] = array('title' => $sLangTitle, 'value' => ar\TypeDelivery::getValue($aVars[$item]));
                    } else {
                        $aDataOrder[] = array('title' => $sLangTitle, 'value' => $aVars[$item]);
                    }
            }

            $aGoods = ar\Goods::find()
                ->where('id_order', $iOrderId)
                ->asArray()->getAll();

            $totalPrice = 0;

            $aGoodsListId = array();
            foreach($aGoods as $item){
                $aGoodsListId[] = $item['id_goods'];
                $totalPrice+=$item['total'];
            }

            $webrootpath = \Site::httpDomain();
            if ($aGoodsListId){

                $aGoodsList = Catalog\GoodsSelector::getList( Catalog\Card::DEF_BASE_CARD )
                    ->condition('id IN ?', $aGoodsListId)
                    ->parse()
                ;

                $aGoodsList = \yii\helpers\BaseArrayHelper::index( $aGoodsList, 'id' );

                foreach($aGoods as &$item){
                    $item['object'] = (isset($aGoodsList[$item['id_goods']]))?$aGoodsList[$item['id_goods']]:false;
                    $item['webrootpath'] = $webrootpath;
                }
            }

            /**
             * @var ar\OrderRow $oOrder
             */
            $oOrder = ar\Order::find()->where('id',$iOrderId)->getOne();
            $iDeliveryPrice = self::getOrderDeliveryPrice($iOrderId);
            $totalDelivPrice = (int)$totalPrice+(int)$iDeliveryPrice;
            //@TODO отрефакторить отправку писем

            $out = \skParser::parseTwig('mail.twig', array(
                'totalPrice'=>$totalPrice,
                'delivCost'=>$iDeliveryPrice,
                'totalDelivPrice' => $totalDelivPrice,
                'orderId'=>$iOrderId,
                'items' => $aDataOrder ,
                'date' => $aVars['date'],
                'aGoods' => $aGoods
            ), __DIR__ . '/templates/');

            $sTitle = ModulesParams::getByName('order', 'title_change_status_mail');
            $sBody = ModulesParams::getByName('order', 'status_content');

            Service::sendMail($oRow->mail, $sTitle, $sBody, [
                'order_id' => $iOrderId,
                'before_status' => $aStatus[$iBeforeStatus],
                "order_info" => $out,
                'token' => $oOrder->token,
                'after_status' => $aStatus[$iAfterStatus]
            ]);
        }
    }

    /**
     * @todo что за ужас? рефакторить
     * Меняем статус заказа (для робокассы)
     * @param $iOrderId
     * @param $iStatus
     * @param $iTotal
     * @return bool
     */
    public static function changeStatus($iOrderId, $iStatus, $iTotal) {

        $oResult = Query::SQL(
            "SELECT orders_goods.id_order, SUM(orders_goods.total) as total_price
            FROM orders_goods
            WHERE orders_goods.id_order = :id
            GROUP BY orders_goods.id_order",
            array( 'id' => $iOrderId )
        );

        if ( $aRow = $oResult->fetchArray() ) {

            return Query::SQL(
                "UPDATE orders SET status=:status WHERE id=:order",
                array(
                    'order' => $iOrderId,
                    'status' => ( $aRow['total_price'] <= $iTotal ) ? $iStatus : Status::getIdByFail()
                )
            );

        }

        return false;
    }


    /**
     * Форма сохранения заказа из формбилдера
     * @param Forms\Entity $oForm
     * @return bool
     */
    public function saveOrder( Forms\Entity $oForm ) {

        $oCardForm = new Cart\OrderForm();

        $aFieldList = $oForm->getFields();

        $aFields = array(
            'address' => 'address',
            'person' => 'person',
            'phone' => 'phone',
            'mail' => 'mail',
            'postcode' => 'postcode',
            'text' => 'text',
            'tp_deliv' => 'type_delivery',
            'tp_pay' => 'type_payment',
            'source_info' => 'source_info',
        );

        foreach ($aFieldList as $oItem) {
            if ( array_key_exists( $oItem->param_name, $aFields ) )
                $oCardForm->$aFields[$oItem->param_name] = $oItem->param_default;
        }

        return $this->saveOrderForm( $oCardForm );

    }
    /**
     * Форма сохранения заказа из формбилдера
     * @param $oForm
     * @return int
     */
    public function saveOrderForm( Cart\OrderForm $oForm ) {

        $aFieldList = $oForm->getFields();

        $oOrder = new ar\OrderRow();
        $type_form = $aFieldList['type_form']->value;
        $actual = $aFieldList['legal_actual']->value;
        $delivery = $aFieldList['actual_delivery']->value;

        foreach ($aFieldList as $oItem) {

            switch ($oItem->name) {
                case 'person':
                    $oOrder->person = $oItem->value;
                    break;
                case 'phone':
                    $oOrder->phone = $oItem->value;
                    break;
                case 'mail':
                    $oOrder->mail = $oItem->value;
                    break;
                case 'text':
                    $oOrder->text = $oItem->value;
                    break;
                case 'tp_deliv':
                    $oOrder->type_delivery = $oItem->value;
                    break;
                case 'tp_pay':
                    $oOrder->type_payment = $oItem->value;
                    break;
                case 'source_info':
                    $oOrder->source_info = $oItem->value;
                    break;
                case 'inn':
                    $oOrder->inn = $oItem->value;
                    break;
                case 'kpp':
                    $oOrder->kpp = $oItem->value;
                    break;
                case 'legal_form':
                    $oOrder->legal_form = $oItem->value;
                    break;
                case 'organization':
                    $oOrder->organization = $oItem->value;
                    break;
                case 'type_form':
                    $oOrder->type_form = $oItem->value;
                    break;
            }
        }

        if ($type_form == UserType::TYPE_INDIVIDUAL) {
            foreach ($aFieldList as $oItem) {
                switch ($oItem->name) {
                    case 'delivery_region':
                        $oOrder->delivery_region = $oItem->value;
                        break;
                    case 'delivery_city':
                        $oOrder->delivery_city = $oItem->value;
                        break;
                    case 'delivery_street':
                        $oOrder->delivery_street = $oItem->value;
                        break;
                    case 'delivery_house':
                        $oOrder->delivery_house = $oItem->value;
                        break;
                    case 'delivery_housing':
                        $oOrder->delivery_housing = $oItem->value;
                        break;
                    case 'delivery_room':
                        $oOrder->delivery_room = $oItem->value;
                        break;
                }
            }
        } else {
            foreach ($aFieldList as $oItem) {
                switch ($oItem->name) {
                    case 'legal_region':
                        $oOrder->legal_region = $oItem->value;
                        break;
                    case 'legal_city':
                        $oOrder->legal_city = $oItem->value;
                        break;
                    case 'legal_street':
                        $oOrder->legal_street = $oItem->value;
                        break;
                    case 'legal_house':
                        $oOrder->legal_house = $oItem->value;
                        break;
                    case 'legal_housing':
                        $oOrder->legal_housing = $oItem->value;
                        break;
                    case 'legal_room':
                        $oOrder->legal_room = $oItem->value;
                        break;
                }
            }
        }

        if ($type_form != UserType::TYPE_INDIVIDUAL) {
            if ($actual == 1) {
                foreach ($aFieldList as $oItem) {
                    switch ($oItem->name) {
                        case 'actual_region':
                            $oOrder->actual_region = $oItem->value;
                            break;
                        case 'actual_city':
                            $oOrder->actual_city = $oItem->value;
                            break;
                        case 'actual_street':
                            $oOrder->actual_street = $oItem->value;
                            break;
                        case 'actual_house':
                            $oOrder->actual_house = $oItem->value;
                            break;
                        case 'actual_housing':
                            $oOrder->actual_housing = $oItem->value;
                            break;
                        case 'actual_room':
                            $oOrder->actual_room = $oItem->value;
                            break;
                    }
                }
                if ($delivery == 1) {
                    foreach ($aFieldList as $oItem) {
                        switch ($oItem->name) {
                            case 'delivery_region':
                                $oOrder->delivery_region = $oItem->value;
                                break;
                            case 'delivery_city':
                                $oOrder->delivery_city = $oItem->value;
                                break;
                            case 'delivery_street':
                                $oOrder->delivery_street = $oItem->value;
                                break;
                            case 'delivery_house':
                                $oOrder->delivery_house = $oItem->value;
                                break;
                            case 'delivery_housing':
                                $oOrder->delivery_housing = $oItem->value;
                                break;
                            case 'delivery_room':
                                $oOrder->delivery_room = $oItem->value;
                                break;
                        }
                    }

                } else {
                    $oOrder->delivery_region = $oOrder->actual_region;
                    $oOrder->delivery_city = $oOrder->actual_city;
                    $oOrder->delivery_street = $oOrder->actual_street;
                    $oOrder->delivery_house = $oOrder->actual_house;
                    $oOrder->delivery_housing = $oOrder->actual_housing;
                    $oOrder->delivery_room = $oOrder->actual_room;
                }

            } else {
                $oOrder->actual_region = $oOrder->legal_region;
                $oOrder->actual_city = $oOrder->legal_city;
                $oOrder->actual_street = $oOrder->legal_street;
                $oOrder->actual_house = $oOrder->legal_house;
                $oOrder->actual_housing = $oOrder->legal_housing;
                $oOrder->actual_room = $oOrder->legal_room;
                if ($delivery == 1) {
                    foreach ($aFieldList as $oItem) {
                        switch ($oItem->name) {
                            case 'delivery_region':
                                $oOrder->delivery_region = $oItem->value;
                                break;
                            case 'delivery_city':
                                $oOrder->delivery_city = $oItem->value;
                                break;
                            case 'delivery_street':
                                $oOrder->delivery_street = $oItem->value;
                                break;
                            case 'delivery_house':
                                $oOrder->delivery_house = $oItem->value;
                                break;
                            case 'delivery_housing':
                                $oOrder->delivery_housing = $oItem->value;
                                break;
                            case 'delivery_room':
                                $oOrder->delivery_room = $oItem->value;
                                break;
                        }
                    }
                } else {
                    $oOrder->delivery_region = $oOrder->actual_region;
                    $oOrder->delivery_city = $oOrder->actual_city;
                    $oOrder->delivery_street = $oOrder->actual_street;
                    $oOrder->delivery_house = $oOrder->actual_house;
                    $oOrder->delivery_housing = $oOrder->actual_housing;
                    $oOrder->delivery_room = $oOrder->actual_room;
                }
            }
        }
        $dNow = new \DateTime('NOW');
        $oOrder->date = $dNow->format("Y-m-d H:i:s");
        $oOrder->status = Status::getIdByNew();


        // готовим токен
        $salt = "#*#BaraNeKontritsya322#*#";
        $sha512 = hash('sha512', rand(0, 1000) . $salt . $oOrder->date);
        $oOrder->token = $sha512;

        // если авторизован, сохраняем id клиента
        if (\CurrentUser::isLoggedIn() && \CurrentUser::getPolicyId() != \Auth::getDefaultGroupId()) {
            $oOrder->auth = \CurrentUser::getId();
        }

        $bFast = $oForm->getFast();

        $cart = Cart\Api::getOrder($bFast);
        if ($oOrder->type_delivery == 2){
            $oOrder->deliv_cost  = Parameters::getValByName(3,'.','deliv_1');
        } else if ($oOrder->type_delivery == 4){
            $oOrder->deliv_cost  = Parameters::getValByName(3,'.','deliv_2');
        } else if ($oOrder->type_delivery == 6){
            $oOrder->deliv_cost  = Parameters::getValByName(3,'.','deliv_3');
        } else if ($oOrder->type_delivery == 7){
            $oOrder->deliv_cost  = Parameters::getValByName(3,'.','deliv_4');
        } else if ($oOrder->type_delivery == 11){
            $oOrder->deliv_cost  = Parameters::getValByName(3,'.','deliv_5');
        } else if ($oOrder->type_delivery == 9){
            $oOrder->deliv_cost  = Parameters::getValByName(3,'.','deliv_6');
        } else if ($oOrder->type_delivery == 10){
            $oOrder->deliv_cost  = Parameters::getValByName(3,'.','deliv_7');
        }

        $orderId = $oOrder->save();

        $goods = array();
        /** @var Cart\OrderItem $item */
        foreach ($cart->getItems() as $item) {

            $object = $item->getObject();
            $good = new ar\GoodsRow();
            $good->title = $object['title'];
            $good->id_goods = $object['id'];
            $good->price = $item->getPrice();
            $good->count = $item->getCount();
            $good->id_order = $orderId;
            $good->total = $item->getTotal();
            $good->save();

            $goods[] = array_merge($good->getData(),array('object'=>$object,'webrootpath'=> \Site::httpDomain()));
        }

        Cart\Api::setOrder(new Cart\Order(), $bFast);

        // собираем данные для мыла
        $aOrderMailData = array();

        if ($type_form == UserType::TYPE_INDIVIDUAL){
            $aFields = ar\Order::getModel()->getColumnSet('individial_mail');
            $aOrderMailData[] = array('title' => \Yii::t('order', 'type_form'),
                'value' =>  UserType::TITLE_INDIVIDUAL
            );
        } elseif($type_form == UserType::TYPE_INDIVID_ENTREP){
            $aFields = ar\Order::getModel()->getColumnSet('individ_entrep_mail');
            $aOrderMailData[] = array('title' => \Yii::t('order', 'type_form'),
                'value' =>  UserType::TITLE_INDIVID_ENTREP
            );
        }elseif($type_form == UserType::TYPE_ENTITY){
            $aFields = ar\Order::getModel()->getColumnSet('entity_mail');
            $aOrderMailData[] = array('title' => \Yii::t('order', 'type_form'),
                'value' =>  UserType::TITLE_ENTITY
            );
        }
//        $aFields = ar\Order::getModel()->getColumnSet('mail');
        $aValues = $oOrder->getData();

        foreach ($aFields as $sField) {

            if ($sField == 'type_delivery') $aValues[$sField] = ar\TypeDelivery::getValue($aValues[$sField]);
            if ($sField == 'type_payment') $aValues[$sField] = ar\TypePayment::getValue($aValues[$sField]);
            if ($sField == 'source_info') $aValues[$sField] = ar\SourcesInfo::getValue($aValues[$sField]);

            if (isset($aValues[$sField]))
                $aOrderMailData[] = array('title' => \Yii::t('order', ar\Order::getModel()->getFiled($sField)->getTitle()),
                    'value' => $aValues[$sField]
                );
        }

        // Подготавливаем данные для шаблона письма
        $totalDelivPrice = (int)$cart->getTotalPrice() + Delivery::getDeliveryPrice($oOrder->type_delivery);
        $aDataMail = array(
            'totalPrice'=>$cart->getTotalPrice(),
            'delivCost'=>Delivery::getDeliveryPrice($oOrder->type_delivery),
            'totalDelivPrice' => $totalDelivPrice,
            'aGoods'=>$goods,
            'items' => $aOrderMailData,
            'date' => $oOrder->date,
            'orderId'=>$orderId
        );

        if($type_form != UserType::TYPE_INDIVIDUAL){
            $aFields = ar\Order::getModel()->getColumnSet('delivery_mail');
            $aDeliveryMail = array();
            foreach ($aFields as $sField) {
                if (isset($aValues[$sField]))
                    $aDeliveryMail[] = array('title' => \Yii::t('order', ar\Order::getModel()->getFiled($sField)->getTitle()),
                        'value' => $aValues[$sField]
                    );
            }
            $aFields = ar\Order::getModel()->getColumnSet('legal_mail');
            $aLegalMail = array();
            foreach ($aFields as $sField) {
                if (isset($aValues[$sField]))
                    $aLegalMail[] = array('title' => \Yii::t('order', ar\Order::getModel()->getFiled($sField)->getTitle()),
                        'value' => $aValues[$sField]
                    );
            }
            $aFields = ar\Order::getModel()->getColumnSet('actual_mail');
            $aActualMail = array();
            foreach ($aFields as $sField) {
                if (isset($aValues[$sField]))
                    $aActualMail[] = array('title' => \Yii::t('order', ar\Order::getModel()->getFiled($sField)->getTitle()),
                        'value' => $aValues[$sField]
                    );
            }
            $aDataMail['delivery_address'] = $aDeliveryMail;
            $aDataMail['legal_address'] = $aLegalMail;
            $aDataMail['actual_address'] = $aActualMail;
        }

        $sOut = \skParser::parseTwig(
            'mail.twig',
            $aDataMail,
            __DIR__ . '/templates/'
        );
        $aOptions = array('order_id' => $orderId, 'token' => $sha512, 'order_info' => $sOut);

        // письмо клиенту
        if ($oOrder->mail) {

            $sTitle = ModulesParams::getByName('order', 'title_user_mail');
            $sBody = ModulesParams::getByName('order', 'user_content');
            $aOptions['pay_link'] = $this->getPayment($oOrder);
            static::sendMail($oOrder->mail, $sTitle, $sBody, $aOptions);
        }

        $sTitle = ModulesParams::getByName('order', 'title_adm_mail');
        $sBody = ModulesParams::getByName('order', 'adm_content');

        // письмо админу
        static::sendMail(\Site::getAdminEmail(), $sTitle, $sBody, $aOptions);

        return $orderId;
    }

    /**
     * Получаем итоговую цену по id order с доставкой
     * @param int $iOrderId
     * @return bool|float
     * @throws \yii\db\Exception
     */
    public static function getTotalPrice($iOrderId = 0){

        if ( !$iOrderId )
            return false;

        $oResult = Query::SQL(
            "SELECT SUM(orders_goods.total) as total_price
            FROM orders_goods
            WHERE orders_goods.id_order=:id
            GROUP BY orders_goods.id_order",
            array( 'id' => (int)$iOrderId )
        );

        $iDeliveryPrice = self::getOrderDeliveryPrice($iOrderId);

        if ( $aRow = $oResult->fetchArray() ){
            return round( $aRow['total_price'] + $iDeliveryPrice, 3 );
        }

        return false;

    }

    /**
     * Получаем итоговую цену по id order без доставки
     *
     * @param int $id
     * @return bool|float
     * @throws \yii\db\Exception
     */
    public static function getTotalPriceWithoutDelivery($id = 0){

        if ( !$id )
            return false;

        $oResult = Query::SQL(
            "SELECT SUM(orders_goods.total) as total_price
            FROM orders_goods
            WHERE orders_goods.id_order=:id
            GROUP BY orders_goods.id_order",
            array( 'id' => (int)$id )
        );

        if ( $aRow = $oResult->fetchArray() ){
            return round( $aRow['total_price'], 3 );
        }

        return false;
    }


    /**
     * Получаем массив итоговой цены по всем товарам
     * @return array
     */
    public static function getArrayTotalPrice($bWithDelivery = true)
    {
        $oResult = Query::SQL(
            "SELECT id_order, SUM(total) as total_price
            FROM orders_goods
            GROUP BY id_order"
        );

        $aResult = array();

        while ( $oRow = $oResult->fetchArray() ){
            $iTotalPrice = $oRow['total_price'];
            if ($bWithDelivery){
                $iTotalPrice+= self::getOrderDeliveryPrice($oRow['id_order']);
            }
            if (SysVar::get('catalog.hide_price_fractional')){
                $aResult[ $oRow['id_order'] ] = (int)$iTotalPrice;
            } else {
                $aResult[ $oRow['id_order'] ] = number_format($iTotalPrice, 2, '.', ' ');
            }
        }

        return $aResult;
    }

    /**
     * @param $oOrder
     * @return string
     */
    private function getPayment($oOrder)
    {

        $sOrderDonePath = Cart\Module::getOrderdonePath();
        $sPaymentLink = \Site::httpDomain() . $sOrderDonePath . $oOrder->token;
        $oPaymentType = AdmOrder\ar\TypePayment::find($oOrder->type_payment);

        if (!self::isOnlinePay($oPaymentType)) {
            $sWant2PayParam = self::getWant2PayParam();
            $sPaymentLink .= "&".$sWant2PayParam;
        }
        return $sPaymentLink;

    }

    /**
     * @return string
     */
    private static function getWant2PayParam(){
        $sParamName = Cart\Module::getWant2PayParamName();
        $sParam = $sParamName."=true";
        return $sParam;
    }

    /**
     * @param $oPaymentType
     * @return bool
     */
    private static function isOnlinePay($oPaymentType)
    {
        if ($oPaymentType && $oPaymentType->payment) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * возвращает стоимость доставки для заказа
     * @param $iOrderId
     * @return string
     */
    private static function getOrderDeliveryPrice($iOrderId)
    {
        $oOrder = AdmOrder\ar\Order::find()
            ->where(["id" => $iOrderId])
            ->asArray()
            ->getOne();
        return Delivery::getDeliveryPrice($oOrder['type_delivery']);
    }
}
