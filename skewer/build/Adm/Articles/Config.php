<?php

use skewer\models\TreeSection;
use skewer\build\Component\Search;

/* main */
$aConfig['name']     = 'Articles';
$aConfig['title']    = 'Статьи (админ)';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Админ-интерфейс управления статьями';
$aConfig['revision'] = '0002';
$aConfig['layer']     = Layer::ADM;
$aConfig['useNamespace'] = true;

$aConfig['events'][] = [
    'event' => TreeSection::EVENT_BEFORE_DELETE,
    'eventClass' => TreeSection::className(),
    'class' => skewer\build\Page\Articles\Model\Articles::className(),
    'method' => 'removeSection',
];

$aConfig['events'][] = [
    'event' => Search\Api::EVENT_GET_ENGINE,
    'class' => skewer\build\Page\Articles\Model\Articles::className(),
    'method' => 'getSearchEngine',
];

return $aConfig;
