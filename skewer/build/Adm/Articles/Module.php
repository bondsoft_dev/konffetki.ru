<?php

namespace skewer\build\Adm\Articles;


use skewer\build\Component\UI;
use skewer\build\Component\SEO;
use skewer\build\Adm;
use skewer\build\Page\Articles\Model\Articles;
use skewer\build\Page\Articles\Model\ArticlesRow;
use yii\base\UserException;


/**
 * Class Module
 * @package skewer\build\Adm\Articles
 */
class Module extends Adm\Tree\ModulePrototype {

    // id текущего раздела
    protected $iSectionId = 0;

    // число элементов на страницу
    protected $iOnPage = 20;

    // текущий номер страницы ( с 0, а приходит с 1 )
    protected $iPage = 0;

    /**
     * Метод, выполняемый перед action меодом
     * @return bool
     * @throws UserException
     */
    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt('page');

        // id текущего раздела
        $this->iSectionId = $this->getInt('sectionId');

        // проверить права доступа
        if ( !\CurrentAdmin::canRead($this->iSectionId) )
            throw new UserException( 'accessDenied' );

    }


    protected function actionInit() {

        // вывод списка
        $this->actionList();

    }


    /**
     * Список статей для раздела
     * @throws \Exception
     */
    protected function actionList() {

        $iCount = 0;
        $aItems = Articles::find()
            ->where( 'parent_section', $this->iSectionId )
            ->order( 'publication_date', 'DESC' )
            ->setCounterRef($iCount)
            ->limit( $this->iOnPage, $this->iPage * $this->iOnPage )
            ->getAll();

        /** @var ArticlesRow $oRow */
        foreach ( $aItems as $oRow ) {
            $oRow->active = (bool)$oRow->active;
            $oRow->publication_date = date( "d.m.Y H:i", strtotime( $oRow->publication_date ) );
        }


        $oForm = UI\StateBuilder::newList();

        $oForm
            ->addField( 'id', 'ID', 'i', 'hide' )
            ->addField( 'title', \Yii::t('articles', 'field_title'), 's', 'string', array( 'listColumns' => array('flex' => 3) ))
            ->addField( 'author', \Yii::t('articles', 'field_author'), 's', 'string' )
            ->addField( 'publication_date', \Yii::t('articles', 'field_date'), 's', 'string', array( 'listColumns' => array('flex' => 2) ))
            ->addField( 'active', \Yii::t('articles', 'field_active'), 'i', 'check')
            ->setValue( $aItems, $this->iOnPage, $this->iPage, $iCount )
            ->setEditableFields( array('active'), 'fastSave' )
            ->addRowButtonUpdate()
            ->addRowButtonDelete()
            ->addButton( \Yii::t('adm','add'), 'show', 'icon-add' )
        ;

        $this->setInterface( $oForm->getForm() );
    }


    /**
     * Форма редактирования статьи
     * @throws \Exception
     */
    protected function actionShow() {

        $aData = $this->getInData();
        $iItemId = (is_array($aData) && isset($aData['id'])) ? (int)$aData['id'] : 0;

        /** @var ArticlesRow $oRow */
        $oRow = $iItemId ? Articles::find( $iItemId ) : Articles::getNewRow();

        $oRow->announce = \ImageResize::restoreTags($oRow->announce);
        $oRow->full_text = \ImageResize::restoreTags($oRow->full_text);

        $oForm = UI\StateBuilder::newEdit();

        $oForm
            ->addField( 'id', 'ID', 'i', 'hide' )
            ->addField( 'title', \Yii::t('articles', 'field_title'), 's', 'string' )
            ->addField( 'author', \Yii::t('articles', 'field_author'), 's', 'string' )
            ->addField( 'publication_date', \Yii::t('articles', 'field_date'), 's', 'datetime' )
            ->addField( 'announce', \Yii::t('articles', 'field_preview'), 's', 'wyswyg' )
            ->addField( 'full_text', \Yii::t('articles', 'field_fulltext'), 's', 'wyswyg' )
            ->addField( 'active', \Yii::t('articles', 'field_active'), 'i', 'check' )
            ->addField( 'hyperlink', \Yii::t('articles', 'field_hyperlink'), 's', 'string' )
            ->addField( 'articles_alias', \Yii::t('articles', 'field_alias'), 's', 'string' )

            ->setValue( $oRow->getData() )

            ->addButton( \Yii::t('adm','save'), 'save', 'icon-save' )
            ->addButton( \Yii::t('adm','back'), 'init', 'icon-cancel' )
        ;

        if ( $iItemId ) {

            $oForm
                ->addBtnSeparator( '->' )
                ->addButton( \Yii::t('adm','del'), 'delete', 'icon-delete' )
            ;

        }

        // добавление SEO блока полей
        $oSearch = new Search();
        SEO\Api::appendExtForm( $oForm->getForm(), $oSearch->getName(), $oRow->id );


        $this->setInterface( $oForm->getForm() );
    }


    /**
     * Сохранение статьи
     * @throws UserException
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->getInData();
        $iId = $this->getInDataValInt( 'id' );

        // есть данные - сохранить
        if ( !$aData )
            throw new UserException( 'Empty data' );

        if ( !isset($aData['title']) or !$aData['title'] )
            throw new UserException( \Yii::t('articles', 'error_title') );

        if ( $iId ) {

            if ( !( $oRow = Articles::find( $iId ) ) )
                throw new UserException( "Articles [$iId] not found" );

            $oRow->setData( $aData );
        } else {

            $oRow = Articles::getNewRow( $aData );
        }

        /** @var ArticlesRow $oRow */
        $oRow->parent_section = $this->iSectionId;

        $iId = $oRow->save();

        $oSearch = new Search();

        // сохранение SEO данных
        SEO\Api::saveJSData( $oSearch->getName(), $iId, $aData );

        SEO\Api::setUpdateSitemapFlag();

        // вывод списка
        $this->actionList();
    }


    /**
     * Изменение полей из списка
     * @throws UserException
     */
    protected function actionFastSave() {

        // запросить данные
        $aData = $this->getInData();
        $iId = $this->getInDataValInt( 'id' );

        if ( !$aData || !$iId )
            throw new UserException( 'Empty data' );

        /** @var ArticlesRow $oRow */
        if ( !( $oRow = Articles::find( $iId ) ) )
            throw new UserException( "Articles [$iId] not found" );

        $oRow->active = $aData['active'];
        $oRow->save();

        $this->actionList();
    }


    /**
     * Удаляет запись
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->getInData();

        // id записи
        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        /** @var ArticlesRow $oRow */
        $oRow = Articles::find( $iItemId );

        if ( !$oRow )
            throw new UserException( "Articles [$iItemId] not found!" );

        $oRow->delete();

        // удаление SEO данных
        SEO\Api::del( 'articles', $oRow->id );

        //$this->addNoticeReport("Удаление статьи",$aData);

        SEO\Api::setUpdateSitemapFlag();

        // вывод списка
        $this->actionInit();

    }


    /**
     * Установка служебных данных
     * @param UI\State\BaseInterface $oIface
     */
    protected function setServiceData( UI\State\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData( array(
            'sectionId' => $this->iSectionId,
            'page' => $this->iPage
        ) );

    }

}
