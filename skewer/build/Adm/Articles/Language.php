<?php

$aLanguage = array();

$aLanguage['ru']['tab_name'] = 'Статьи';
$aLanguage['ru']['field_id'] = 'ID';
$aLanguage['ru']['field_alias'] = 'Псевдоним';
$aLanguage['ru']['field_parent'] = 'Родительский раздел';
$aLanguage['ru']['field_author'] = 'Автор';
$aLanguage['ru']['field_title'] = 'Название';
$aLanguage['ru']['field_date'] = 'Дата публикации';
$aLanguage['ru']['field_time'] = 'Время публикации';
$aLanguage['ru']['field_preview'] = 'Анонс';
$aLanguage['ru']['field_fulltext'] = 'Полный текст';
$aLanguage['ru']['field_active'] = 'Активность';
$aLanguage['ru']['field_hyperlink'] = 'Ссылка';
$aLanguage['ru']['field_modifydate'] = 'Дата последнего изменения';
$aLanguage['ru']['title_on_main'] = 'Заголовок статей на главной';
$aLanguage['ru']['on_page'] = 'Статей на странице';
$aLanguage['ru']['on_column'] = 'Статей в колонке';
$aLanguage['ru']['allowArticlesListRead'] = 'Разрешить чтение списка статей';
$aLanguage['ru']['allowArticlesDetailRead'] = 'Разрешить чтение детальной статей';
$aLanguage['ru']['new_article'] = 'Статья';
$aLanguage['ru']['titleOnMain'] = 'Заголовок статей для главной';
$aLanguage['ru']['error_title'] = 'Название не задано';
$aLanguage['ru']['param_on_page'] = 'Статей на главной';
$aLanguage['ru']["author"] = "Автор";

$aLanguage['en']['tab_name'] = 'Articles';
$aLanguage['en']['field_id'] = 'ID';
$aLanguage['en']['field_alias'] = 'Alias';
$aLanguage['en']['field_parent'] = 'Parent section';
$aLanguage['en']['field_author'] = 'Author';
$aLanguage['en']['field_title'] = 'Title';
$aLanguage['en']['field_date'] = 'Publish date';
$aLanguage['en']['field_time'] = 'Publish time';
$aLanguage['en']['field_preview'] = 'Preview';
$aLanguage['en']['field_fulltext'] = 'Full text';
$aLanguage['en']['field_active'] = 'Active';
$aLanguage['en']['field_hyperlink'] = 'Link';
$aLanguage['en']['field_modifydate'] = 'Last modify date';
$aLanguage['en']['title_on_main'] = 'Title of articles on the main';
$aLanguage['en']['on_page'] = 'Articles on page';
$aLanguage['en']['on_column'] = 'Articles on column';
$aLanguage['en']['allowArticlesListRead'] = 'Allow reading the list of articles';
$aLanguage['en']['allowArticlesDetailRead'] = 'Allow reading detailed description of the articles';
$aLanguage['en']['new_article'] = 'Article';
$aLanguage['en']['titleOnMain'] = 'Title articles to main';
$aLanguage['en']['error_title'] = 'Empty title';
$aLanguage['en']['param_on_page'] = 'Articles on main';
$aLanguage['en']["author"] = "Author";


return $aLanguage;