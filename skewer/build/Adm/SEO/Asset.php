<?php

namespace skewer\build\Adm\SEO;


use yii\web\AssetBundle;

class Asset extends AssetBundle {
    public $sourcePath = '@skewer/build/Adm/SEO/web';
}