<?php

$aLanguage = array();

$aLanguage['ru']['Editor.Adm.tab_name'] = 'Редактор';
$aLanguage['ru']['link_text'] = 'главная';
$aLanguage['ru']['hyperlink'] = 'Ссылка';

//Параметры
$aLanguage['ru']['send_email'] = 'E-mail для отправки сообщений';
$aLanguage['ru']['email'] = 'E-mail для приема сообщений';
$aLanguage['ru']['site_name'] = 'Название сайта';
$aLanguage['ru']['copyright'] = 'Копирайт';
$aLanguage['ru']['contacts_footer'] = 'Контакты внизу сайта';
$aLanguage['ru']['text_1'] = 'Контентный блок №1';
$aLanguage['ru']['text_2'] = 'Контентный блок №2';
$aLanguage['ru']['text_3'] = 'Контентный блок №3';
$aLanguage['ru']['text_4'] = 'Контентный блок №4';
$aLanguage['ru']['text_5'] = 'Контентный блок №5';
$aLanguage['ru']['text_footer'] = 'Контентный блок в подвале';
$aLanguage['ru']['counters'] = 'Счетчики в подвале сайта';
$aLanguage['ru']['countersCode'] = 'Счетчики JS код';
$aLanguage['ru']['gaCode'] = 'Google Analytics';
$aLanguage['ru']['google'] = 'Верификация Google';
$aLanguage['ru']['yandex'] = 'Верификация Yandex';
$aLanguage['ru']['developer_copyright'] = 'Разработческий копирайт';
$aLanguage['ru']['static_content'] = 'Текст раздела';
$aLanguage['ru']['static_content2'] = 'Текст раздела 2';
$aLanguage['ru']['hide_right_column'] = 'Скрыть правую колонку';
$aLanguage['ru']['editSectionText'] = 'Редактирование текста раздела';
$aLanguage['ru']['access_denied'] = 'Доступ запрещен';
$aLanguage['ru']['type_catalog_view'] = 'Тип вывода товаров';
$aLanguage['ru']['type_category_view'] = 'Тип вывода элементов коллекции';
$aLanguage['ru']['type_catalog_list'] = 'список';
//$aLanguage['ru']['type_catalog_table'] = 'таблица';
$aLanguage['ru']['type_catalog_gallery'] = 'галерея';
$aLanguage['ru']['type_collection_list'] = 'список';
$aLanguage['ru']['type_collection_slider'] = 'карусель';

$aLanguage['ru']['message_saved'] = 'Данные сохранены';
$aLanguage['ru']['robots'] = 'Robots.txt';

$aLanguage['ru']['wyswyg_format_tag_icons_pdf'] = 'pdf';
$aLanguage['ru']['wyswyg_format_tag_icons_zip'] = 'zip';
$aLanguage['ru']['wyswyg_format_tag_icons_doc'] = 'doc';
$aLanguage['ru']['wyswyg_format_tag_icons_xls'] = 'xls';
$aLanguage['ru']['wyswyg_format_tag_icons_ppt'] = 'ppt';
$aLanguage['ru']['wyswyg_format_tag_icons_disk'] = 'Диск';
$aLanguage['ru']['wyswyg_format_tag_icons_info'] = 'Информация';
$aLanguage['ru']['wyswyg_format_tag_icons_warning'] = 'Предупреждение';
$aLanguage['ru']['wyswyg_format_tag_icons_stickynote'] = 'Заметка';
$aLanguage['ru']['wyswyg_format_tag_icons_download'] = 'Скачать';
$aLanguage['ru']['wyswyg_format_tag_icons_faq'] = 'Вопрос';
$aLanguage['ru']['wyswyg_format_tag_icons_flag'] = 'Флаг';

//------------------------------------------------------------------------------------------

$aLanguage['en']['Editor.Adm.tab_name'] = 'Editor';
$aLanguage['en']['link_text'] = 'main';
$aLanguage['en']['hyperlink'] = 'Hyperlink';

//Параметры
$aLanguage['en']['send_email'] = 'E-mail to send messages';
$aLanguage['en']['email'] = 'E-mail to receive messages';
$aLanguage['en']['site_name'] = 'Site name';
$aLanguage['en']['copyright'] = 'Copyright';
$aLanguage['en']['contacts_footer'] = 'Contacts at the bottom of the site';
$aLanguage['en']['text_1'] = 'Content block №1';
$aLanguage['en']['text_2'] = 'Content block №2';
$aLanguage['en']['text_3'] = 'Content block №3';
$aLanguage['en']['text_4'] = 'Content block №4';
$aLanguage['en']['text_5'] = 'Content block №5';
$aLanguage['en']['text_footer'] = 'Content block in the footer';
$aLanguage['en']['counters'] = 'Counters in the footer';
$aLanguage['en']['countersCode'] = 'Counters JS code';
$aLanguage['en']['gaCode'] = 'Google Analytics';
$aLanguage['en']['google'] = 'Verification Google';
$aLanguage['en']['yandex'] = 'Verification Yandex';
$aLanguage['en']['developer_copyright'] = 'Developer copyright';
$aLanguage['en']['static_content'] = 'Section text';
$aLanguage['en']['static_content2'] = 'Section text 2';
$aLanguage['en']['hide_right_column'] = 'Hide right column';
$aLanguage['en']['editSectionText'] = 'Editing text of section';
$aLanguage['en']['access_denied'] = 'Access denied';
$aLanguage['en']['type_category_view'] = 'Type collection view';
$aLanguage['en']['type_catalog_view'] = 'Type catalog view';
$aLanguage['en']['type_catalog_list'] = 'list';
//$aLanguage['en']['type_catalog_table'] = 'table';
$aLanguage['en']['type_catalog_gallery'] = 'gallery';
$aLanguage['en']['robots'] = 'Robots.txt';
$aLanguage['en']['type_collection_list'] = 'list';
$aLanguage['en']['type_collection_slider'] = 'slider';

$aLanguage['en']['message_saved'] = 'Data saved';

$aLanguage['en']['wyswyg_format_tag_icons_pdf'] = 'pdf';
$aLanguage['en']['wyswyg_format_tag_icons_zip'] = 'zip';
$aLanguage['en']['wyswyg_format_tag_icons_doc'] = 'doc';
$aLanguage['en']['wyswyg_format_tag_icons_xls'] = 'xls';
$aLanguage['en']['wyswyg_format_tag_icons_ppt'] = 'ppt';
$aLanguage['en']['wyswyg_format_tag_icons_disk'] = 'disk';
$aLanguage['en']['wyswyg_format_tag_icons_info'] = 'info';
$aLanguage['en']['wyswyg_format_tag_icons_warning'] = 'warning';
$aLanguage['en']['wyswyg_format_tag_icons_stickynote'] = 'stickynote';
$aLanguage['en']['wyswyg_format_tag_icons_download'] = 'download';
$aLanguage['en']['wyswyg_format_tag_icons_faq'] = 'faq';
$aLanguage['en']['wyswyg_format_tag_icons_flag'] = 'flag';

return $aLanguage;
