<?php

namespace skewer\build\Adm\Editor;

use skewer\build\Component\Section\Parameters;
use skewer\build\Component\Section\Params\Type;
use skewer\build\Component\UI;
use yii\base\UserException;

/**
 * Редактор для единственного параметра
 * Используется в дизайнерском режиме
 */
class SimpleModule extends Module {

    private $sEditorId = '';

    /**
     * Метод, выполняемый перед action меодом
     * @return bool
     */
    protected function preExecute() {
        parent::preExecute();
        $this->sEditorId = $this->getStr('editorId');
    }

    /**
     * Установка служебных данных
     * @param UI\State\BaseInterface $oIface
     */
    protected function setServiceData( UI\State\BaseInterface $oIface ) {

        parent::setServiceData( $oIface );

        // расширение массива сервисных данных
        $aData = $oIface->getServiceData();
        $aData['editorId'] = $this->sEditorId;
        $oIface->setServiceData( $aData );

    }

    /**
     * Запрос доступных параметров
     * @throws UserException
     * @return array
     */
    protected function getAvailItems(){

        // восстанавливаем id параметра по его имени "группа/имя"
        $aParamData = explode('/', $this->sEditorId);

        if(count($aParamData)>1){

            $oParam = Parameters::getByName( $this->iSectionId, $aParamData[0], $aParamData[1], true );

            if ($oParam->access_level == Type::paramLanguage){
                $oParam = Parameters::getByName( \Yii::$app->sections->languageRoot(), $aParamData[0], $aParamData[1] );
            }

            if($oParam) {
                $this->iSectionId = $oParam->parent;

                if(!\CurrentAdmin::canRead($oParam->parent))
                    throw new UserException(\Yii::t('editor', 'access_denied'));
                return [$oParam];
            }
        }
        return [];
    }

    /**
     * Сохранение данных
     * @return bool
     */
    public function actionSave(){
        $this->fireJSEvent('reload_display_form');
        parent::actionSave();
    }

    /**
     * Собирает набор элементов для редактирования
     * @param $aItems
     * @return array
     */
    protected function sortItems($aItems) {
        return $aItems;
    }

    /**
     * Добавление дополнительных элементов
     * @param $aOut
     * @return array
     */
    protected function addAdditionalComponents( $aOut ) {
        return $aOut;
    }

    /**
     * @return array
     */
    protected function getParamsTypes()
    {
        $aTypes =  parent::getParamsTypes();

        $aTypes[Type::paramSystem] = ['type'=>'hide', 'val'=> 'value'];

        return $aTypes;
    }


}
