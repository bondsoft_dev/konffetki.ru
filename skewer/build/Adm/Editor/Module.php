<?php

namespace skewer\build\Adm\Editor;

use skewer\build\Component\Section;
use skewer\build\Component\Section\Tree;
use skewer\build\Component\Section\Parameters;
use skewer\build\Component\Section\Params\ListSelector;
use skewer\build\Component\Section\Params\Type;
use skewer\build\Component\UI;
use skewer\build\Component\SEO;
use skewer\build\libs\ExtBuilder;
use skewer\build\Adm;
use skewer\build\Component\Search;
use yii\base\UserException;
use yii\web\ServerErrorHttpException;


/**
 * Class Module
 * @package skewer\build\Adm\Editor
 */
class Module extends Adm\Tree\ModulePrototype {

    /** id текущего раздела */
    protected $iSectionId = 0;

    /**
     * Метод, выполняемый перед action меодом
     * @throws UserException
     * @return bool
     */
    protected function preExecute() {

        // id текущего раздела
        $this->iSectionId = $this->getInt('sectionId');

        // проверить права доступа
        if ( !\CurrentAdmin::canRead($this->iSectionId) )
            throw new UserException( 'accessDenied' );

    }

    /**
     * Установка служебных данных
     * @param UI\State\BaseInterface $oIface
     */
    protected function setServiceData( UI\State\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData( array(
            'sectionId' => $this->iSectionId
        ) );

    }

    /**
     * Инициализация
     * @return bool
     */
    public function actionInit() {

        return $this->actionLoadItems();

    }

    /**
     * Сохранение данных
     * @return bool
     */
    public function actionSave(){

        // запросить набор редакторов для раздела
        $aNowItems = $this->getAvailItems();

        $aSaveItems = $this->get('data', []);

        // данные по типам
        $aTypes = $this->getParamsTypes();

        // результат операции
        $bAllRes = false;

        $this->checkParams( $aNowItems, $aTypes );

        // сохранить в каждый параметр то, что пришло
        foreach ( $aNowItems as $oItem  ) {

            // если есть такой тип
            $iAbsAccessLevel = abs($oItem->access_level);
            if ( isset($aTypes[$iAbsAccessLevel]) ) {

                // собрать имя пришедшего параметра
                $sName = 'field_' . $oItem->id;

                // если есть такое поле в массиве на сохранение
                if ( isset( $aSaveItems[$sName] ) ) {

                    if ($oItem->hasUseShowVal())
                        $oItem->show_val = $aSaveItems[$sName];
                    else
                        $oItem->value = $aSaveItems[$sName];

                    // для wyswyg сделать оборачивание картинок с размерами
                    if ( $oItem->isWysWyg() ) {
                        $oItem->show_val = \ImageResize::wrapTags($aSaveItems[$sName], $this->iSectionId );
                    }

                    // сохранить
                    $bOneRes = $oItem->save();

                    // что-то, да сохранено
                    if ( $bOneRes )
                        $bAllRes = true;

                }

            }

        }

        SEO\Api::saveJSData( 'section', $this->iSectionId, $aSaveItems );

        SEO\Api::setUpdateSitemapFlag();

        // добавить в лог сообщение о редактировании
        \skLogger::addNoticeReport(\Yii::t('editor', 'editSectionText'), \skLogger::buildDescription(['Результат сохранения' => $bAllRes]), \skLogger::logUsers, $this->getModuleName());

        $search = new Adm\Tree\Search();
        $search->updateByObjectId($this->iSectionId);

        // положить в ответ результат сохранения
        $this->setData('saveResult', $bAllRes);

        $this->addMessage( \Yii::t('editor', 'message_saved') );

        // отдать текущее состояние всех элементов
        return $this->actionLoadItems();

    }


    /**
     * Отмечаем ненужные параметры
     * @param array $aItems
     * @param array $aTypes
     */
    private function checkParams( &$aItems = [], $aTypes = [] ){

        $oInstaller = new \skewer\build\Component\Installer\Api();
        if ($oInstaller->isInstalled('ParamSettings', \Layer::ADM)){
            $oParamSettingsApi = new \skewer\build\Adm\ParamSettings\Api();

            $aParams = $oParamSettingsApi->getParams();
            $aTypesList = array();
            foreach($aTypes as $iKey => $aType){
                $aTypesList[$aType['type']] = $iKey;
            }

            foreach($aParams as $aParam){
                if (is_array($aParam) && count($aParam)){
                    if (!isset($aParam['section'])){
                        $aParam['section'] = 'all';
                    }
                    switch ($aParam['section']){
                        case 'all':
                        default:
                            $iSection = \Yii::$app->sections->languageRoot();
                            break;
                        case 'main':
                            $iSection = \Yii::$app->sections->main();
                            break;
                    }

                    foreach ( $aItems as &$oItemPar  ) {
                        if (!$oItemPar instanceof \skewer\models\Parameters)
                            continue;
                        if ($oItemPar->group == $aParam['group'] && $oItemPar->name == $aParam['name']){
                            if ($oItemPar->parent == $iSection){
                                $oItemPar->access_level = (isset($aTypesList[$aParam['editor']]))?$aTypesList[$aParam['editor']]:0;
                            }
                        }
                    }
                }
            }
        }
    }


    /**
     * Запрос доступных параметров
     * @return \skewer\models\Parameters[]
     */
    protected function getAvailItems(){

        return Parameters::getList( $this->iSectionId )
            ->addOrder('group')
            ->addOrder('name')
            ->level(ListSelector::alEdit)
            ->get();

    }


    /**
     * Загрузка списка
     * @return bool
     */
    public function actionLoadItems() {

        // набор полей текущего раздела
        $aItems = $this->getAvailItems();
        // сортировка полей
        $aItems = $this->sortItems( $aItems );

        // данные по типам
        $aTypes = Type::getParamTypes();

        // создание модели
        $aFields = [];

        $this->checkParams( $aItems, $aTypes );

        // собрать список
        foreach ( $aItems as $oItem  ) {
            if (!$oItem instanceof \skewer\models\Parameters)
                continue;

            // если есть такой тип
            $iAbsAccessLevel = abs($oItem->access_level);

            /** Хак: не выводим эти параметры в редакторе */
            if (in_array($iAbsAccessLevel, [Type::paramServiceSection, Type::paramLanguage]))
                continue;

            if ( isset($aTypes[$iAbsAccessLevel]) ) {

                // тип данных
                $aType = $aTypes[$iAbsAccessLevel];

                // имя поля
                $sFieldName = 'field_' . $oItem->id;

                $sFieldClass = \ExtFT::getPosibleEditorClass( $aType['type'] );

                if ( !class_exists($sFieldClass) )
                    throw new UserException( "Не найден класс редактора поля [$sFieldClass]" );

                $oField = new $sFieldClass();

                if ( !$oField instanceof ExtBuilder\Field\Prototype )
                    throw new UserException( "Класс [$sFieldClass] не наследник ExtBuilder\\Field\\Prototype" );

                $oField->setName( $sFieldName );

                if (($iPoint = strpos($oItem->title, '.')) !== 0){
                    $langVal = \Yii::t(substr($oItem->title, 0, $iPoint), substr($oItem->title, $iPoint+1));
                    if ($langVal !== substr($oItem->title, $iPoint+1))
                        $oItem->title = $langVal;
                }
                $oField->setTitle( $oItem->title );

                $oField->setValue( $oItem->hasUseShowVal() ? $oItem->show_val : $oItem->value );

                $this->modifyField( $oField, $oItem );

                $aFields[] = $oField;

            } else {
                throw new UserException('Неподдерживаемый тип '.$iAbsAccessLevel);
            }
        }

        // группировка полей
        $aFields = $this->groupItems( $aFields, $aItems );

        $aFields = $this->addAdditionalComponents( $aFields );

        $oForm = new \ExtForm();

        $oForm->setFields( $aFields );
        $oForm->useSpecSectionForImages( $this->iSectionId );

        $oForm->addBtnSave();
        $oForm->addBtnCancel();

        // вывод данных в интерфейс
        $this->setInterface( $oForm );

    }

    /**
     * Добавление дополнительных элементов
     * @param $aOut
     * @return array
     */
    protected function addAdditionalComponents( $aOut ) {

        // если есть дополнительные компоненты
        if ( $this->hasAddComponents( $this->iSectionId ) ) {

            // добавить сслку
            $oHrefField = $this->getHrefField();
            if ( $oHrefField )
                array_unshift( $aOut, $oHrefField );

            // добавление блока SEO данных
            $aOut[] = SEO\Api::getJSFields( 'section', $this->iSectionId );
            $this->addLibClass( 'SEOFields', 'Adm', 'SEO' );

        }

        return $aOut;

    }

    /**
     * Собирает набор элементов для редактирования
     * @param $aItems
     * @return array
     */
    protected function sortItems($aItems) {

        $iSectionId = $this->getInt('sectionId');

        $aOldItems = $aItems;

        /* Сортируем поля в редакторе */
        $sOrderConditions = Parameters::getShowValByName( $iSectionId, Parameters::settings, 'field_order', true );

        if ($sOrderConditions) {

            $aOrderConditions = preg_replace('/\x0a+|\x0d+/Uims', '', $sOrderConditions);
            $aOrderConditions = explode(';', trim($aOrderConditions));
            $aOrderConditions = array_diff($aOrderConditions, ['']);

            $aItems = [];

            if(count($aOrderConditions))
                foreach($aOrderConditions as $sCondition){
                    list($sGroup, $sParamName) = explode(':',$sCondition);
                    /** @var \skewer\models\Parameters $oItem */
                    foreach($aOldItems as $iKey=>$oItem) {

                        if( $oItem->group == $sGroup && $oItem->name == $sParamName ) {
                            $aItems[] = $oItem;
                            unSet($aOldItems[$iKey]);
                        }
                    }
                }
            $aItems = array_merge( $aItems, $aOldItems);
        }

        return $aItems;

    }

    /**
     * Группирует поля
     * @param $aFields
     * @param $aItems
     * @return mixed
     */
    protected function groupItems( $aFields, $aItems ) {

        // собрать наименования групп
        $aGroups = [];
        foreach ( $aItems as $iKey => $aItem ) {
            $aGroups[ $aItem['group'] ][] = $iKey;
        }

        // запросить имена для групп
        $aGroupTitleList = Parameters::getList( $this->iSectionId )->name(Parameters::groupName)->fields(['name', 'value'])->get();
        foreach ( $aGroupTitleList as $oGroupTitle ) {

            // проверяем наличие полей
            $sGroupName = $oGroupTitle->group;
            if ( !array_key_exists( $sGroupName, $aGroups ) )
                continue;

            // первый элемент заменяем
            $iFirstKey = $aGroups[$sGroupName][0];

            // собираем набор полей для группы
            $aGroupItems = array();
            foreach( $aGroups[$sGroupName] as $iOldKey ) {
                /** @var ExtBuilder\Field\Field $oField  */
                $oField = $aFields[$iOldKey];
                // удаляем все кроме первой
                if ( $iOldKey !== $iFirstKey )
                    unset($aFields[$iOldKey]);
                $aGroupItems[] = \ExtForm::getFieldDesc( $oField );
            }

            $sTitle = \Yii::t('editor', $oGroupTitle->value);
            if (($pos = strpos($oGroupTitle->value, '.')) !== false){
                $category = substr($oGroupTitle->value, 0, $pos);
                $message = substr($oGroupTitle->value, $pos+1);
                $sTitle = \Yii::t($category, $message);
            }

            // создаем поле-группу на месте первого вхождения
            $aFields[$iFirstKey] = array(
                'name' => 'group_'.$sGroupName,
                'collapsed' => false,
                'type' => 'specific',
                'view' => 'specific',
                'extendLibName' => 'FieldGroup',
                'layerName' => 'sk',
                'title' => $sTitle,
                'group_name' => $sGroupName,
                'row_id' => '1',
                'data' => self::get($sGroupName, 1),
                'fieldConfig' => $aGroupItems
            );

        }

        return $aFields;

    }

    /**
     * Отдает массив для поля - ссылки на страницу
     * @return ExtBuilder\Field\Field|null
     */
    protected function getHrefField() {

        // формирование поля
        $oField = new ExtBuilder\Field\Link();
        $oField->setName('page_href');
        $oField->setTitle(\Yii::t('editor', 'hyperlink'));

        // запросить запись раздела
        $oSection = Tree::getSection( $this->iSectionId );
        if ( !$oSection )
            return null;

        // если скрыт из пути
        if ( $oSection->visible == Section\Visible::HIDDEN_FROM_PATH )
            return null;

        $sHref = $oSection->link ?: \Yii::$app->router->rewriteURL( "[$this->iSectionId]" );

        // составить текст ссылки
        $sText = $sHref==='/' ? \Yii::t('editor', 'link_text') : $sHref;

        // добавить параметры
        $oField->setLink($sText,$sHref);

        return $oField;

    }

    /**
     * Возвращает флаг наличия дополнительных элементов
     * @param int $iSectionId
     * @return bool
     */
    protected function hasAddComponents( $iSectionId ) {

        // данные раздела
        $oSection = Tree::getSection( $iSectionId );

        if ( !$oSection )
            return false;

        // для разделов без url не выводим
        if ( !$oSection->hasRealUrl() )
            return false;

        // id родительского раздела
        $iParentSectionId = $oSection->parent;

        // если не наследники подчиненных/корневого раздела
        $bIsSysSection = in_array( $iParentSectionId, array(
            \Yii::$app->sections->root(),
            \Yii::$app->sections->templates(),
            \Yii::$app->sections->library()
        ) );

        return !$bIsSysSection;

    }

    /**
     * Дополнение поля параметрами, если требуется
     * @param ExtBuilder\Field\Prototype $oField
     * @param \skewer\models\Parameters $oItem
     * @throws ServerErrorHttpException
     * @throws \yii\base\InvalidConfigException
     */
    private function modifyField( ExtBuilder\Field\Prototype $oField, $oItem ) {

        /** Висивиг */
        if ($oItem->isWysWyg()){
            if ( !$oField instanceof ExtBuilder\Field\Wyswyg )
                throw new ServerErrorHttpException( 'Поле Wyswyg должно быть типа ExtBuilder\Field\Wyswyg' );

            // отменить оборачивание картинок с размерами
            // todo -> editor
            $oField->setValue( \ImageResize::restoreTags($oField->getValue()) );

            // стилевой класс для body в wysiwyg
            $addCssClassName = 'b-editor b-wyswyg';

            // извлекаем дополнительные параметры висивига из поля value
            $aWysiwygParams = $this->extractWysiwygParams( $oItem->value );

            if($aWysiwygParams['cssClass'])
                $addCssClassName = $aWysiwygParams['cssClass'];

            $oField->setCssFiles( array(
                \yii::$app->getAssetManager()->getBundle(\skewer\build\Page\Main\Asset::className())->baseUrl.'/css/typo.css',
                \yii::$app->getAssetManager()->getBundle(\skewer\build\Cms\Frame\Asset::className())->baseUrl.'/css/wyswyg.css'
            ) );

            $oField->setHeight( $aWysiwygParams['height'] ? $aWysiwygParams['height'] : 300 );

            $oField->setContentHtmlClass( $addCssClassName );

            $oField->setAddLangParams(Api::getAddLangParams4Wyswyg());
        }
        /** Текстовые */
        else if ($oItem->hasTextField()){
            if ( !$oField instanceof ExtBuilder\Field\Text )
                throw new ServerErrorHttpException( 'Поле Text должно быть типа ExtBuilder\Field\Text' );

            $iVal = (int)$oItem->value;
            if ( $iVal )
                $oField->setHeight($iVal);
        }
        /** Список */
        else if ($oItem->isSelect()){
            if ( !$oField instanceof ExtBuilder\Field\Select )
                throw new ServerErrorHttpException( 'Поле select должно быть типа ExtBuilder\Field\Select' );

            $aList = array();
            if ( $oItem->show_val ) {
                foreach ( explode( "\n", $oItem->show_val ) as $sRow ) {
                    if ( strpos( $sRow, ' ' ) !== false )
                        list( $sKey, $sVal ) = explode( ' ', $sRow, 2 );
                    else
                        $sKey = $sVal = $sRow;
                    $aList[$sKey] = $sVal;
                }
            }

            $oField->setList( $aList );
        }

    }

    /**
     * Извлекает длинну, класс и шаблон из поля value для wysiwyg редактора
     * @static
     * @param string $sValue
     * @return array
     */
    protected function extractWysiwygParams( $sValue ) {

        // формат значения - height:cssClass:parserInclude
        //
        // 1. одно значение интерпретируется в зависимости от типа как height или как cssClass
        // 2. несколько значений интерпретируется в порядке: height, cssClass, parserInclude
        //    если необходимо пропустить значения необходимо сохранить необходимое количество
        //    разделителей (например ::content.twig - пропускает значения height и cssClass)


        $aParamKeys = array('height', 'cssClass'/*, 'parserInclude'*/);

        $aRow = array();
        foreach($aParamKeys as $sKey) $aRow[$sKey]='';

        if(strpos($sValue, ':')){

            $aData = explode(':', $sValue);
            while(count($aData) and count($aParamKeys))
                $aRow[array_shift($aParamKeys)] = trim(array_shift ($aData));

            // задать высоту
            if($aRow['height']) $aRow['height'] = (int)$aRow['height'];

        }

        else {

            $iIsNumeric = (int)$sValue;
            // если в значении число - взять за высоту
            if( $iIsNumeric ) $aRow['height'] = $iIsNumeric;
            elseif ( $sValue )
                $aRow['cssClass'] = trim($sValue);

        }

        return $aRow;

    }

    /**
     * @return array
     */
    protected function getParamsTypes()
    {
        return Type::getParamTypes();
    }

}
