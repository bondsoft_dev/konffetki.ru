<?php

$aConfig['name']     = 'Editor';
$aConfig['title']    = 'Редактор';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Редактор контента для админки';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::ADM;
$aConfig['useNamespace'] = true;

return $aConfig;
