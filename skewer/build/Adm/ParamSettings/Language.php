<?php

$aLanguage = array();

$aLanguage['ru']['ParamSettings.Adm.tab_name'] = 'Настройка параметров';

$aLanguage['ru']['groups_news'] = 'Параметры вывода новостей';
$aLanguage['ru']['groups_articles'] = 'Параметры вывода статей';
$aLanguage['ru']['groups_Review'] = 'Параметры вывода отзывов';
$aLanguage['ru']['groups_catalog'] = 'Каталог на главной';
$aLanguage['ru']['groups_catalog2'] = 'Каталог на главной (Хиты)';
$aLanguage['ru']['groups_catalog3'] = 'Каталог на главной (Новинки)';
$aLanguage['ru']['groups_catalog_filter'] = 'Фильтр каталога на главной';
$aLanguage['ru']['section_cat_filter_form'] = 'Раздел с формой';
$aLanguage['ru']['groups_catalog_collection'] = 'Коллекции каталога на главной';

$aLanguage['en']['ParamSettings.Adm.tab_name'] = 'Setting';

$aLanguage['en']['groups_news'] = 'Output Options News';
$aLanguage['en']['groups_articles'] = 'Output Options articles';
$aLanguage['en']['groups_Review'] = 'Output Options reviews';
$aLanguage['en']['groups_catalog'] = 'Catalogue Home';
$aLanguage['en']['groups_catalog2'] = 'Catalogue Home (Hits)';
$aLanguage['en']['groups_catalog3'] = 'Catalogue Home (News)';
$aLanguage['en']['groups_catalog_filter'] = 'Catalog filter Home';
$aLanguage['en']['section_cat_filter_form'] = 'Form section';
$aLanguage['en']['groups_catalog_collection'] = 'Catalog collection on main';

return $aLanguage;