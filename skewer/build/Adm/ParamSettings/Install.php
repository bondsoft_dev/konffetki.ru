<?php

namespace skewer\build\Adm\ParamSettings;

use skewer\build\Component\Section\Parameters;

class Install extends \skModuleInstall {

    public function init() {
        return true;
    }

    public function install() {

        /** Обойдем все параметры, вынесенные для редактирования в модуле и скроем их из редакторов */
        $oApi = new Api();

        $aParams = $oApi->getParams();

        if (is_array($aParams) && count($aParams)){
            foreach($aParams as $aParam){
                if (!isset($aParam['section'])){
                    $aParam['section'] = 'all';
                }
                switch ($aParam['section']){
                    case 'all':
                    default:
                        $iSection = \Yii::$app->sections->tplNew();
                        break;
                    case 'main':
                        $iSection = \Yii::$app->sections->main();
                        break;
                }

                Parameters::setParams( $iSection, $aParam['name'], $aParam['group'], null, null, null, 0 );

            }
        }

        return true;
    }

    public function uninstall() {
        return true;
    }
}