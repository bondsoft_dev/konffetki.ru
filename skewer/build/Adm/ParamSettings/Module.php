<?php

namespace skewer\build\Adm\ParamSettings;


use skewer\build\Component\UI;
use skewer\build\Adm;
use skewer\build\Component\Site;

class Module extends Adm\Tree\ModulePrototype {

    /**
     * Шаблон нового раздела
     * @var int
     */
    protected $iNewTpl = 0;

    /**
     *  Метод, выполняемый перед action меодом
     * @return void
     */
    protected function preExecute() {

        $this->iNewTpl = \Yii::$app->sections->tplNew();

    }

    protected function actionInit(){
        $this->actionEdit();
    }

    protected function actionEdit(){
        $oInterface = new \ExtForm();

        $oApi = new Api();

        $oApi->addForm($oInterface);

        $oInterface->addBtnSave('save');
        $oInterface->addBtnCancel('edit');

        $this->setInterface($oInterface);
    }

    protected function actionSave(){

        $oApi = new Api();

        $oApi->saveParams($this->getInData());

        $this->actionEdit();
    }

}