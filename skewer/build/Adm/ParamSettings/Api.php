<?php

namespace skewer\build\Adm\ParamSettings;


use skewer\build\Component\Section\Parameters;
use skewer\build\Component\UI;
use skewer\build\Adm\Params;
use skewer\build\Component\Site;
use skewer\build\Component\Catalog\Section;

/**
 * @deprecated #37372
 */
class Api{

    /**
     * Список параметров
     * @var array
     */
    private $aParams = array(
        /** news */
        array(
            'name' => 'titleOnMain',
            'group' => 'news',
            'title' => 'news.title_on_main',
            'editor' => 'string'
        ),
        array(
            'name' => 'onPage',
            'group' => 'news',
            'title' => 'news.on_page',
            'editor' => 'int'
        ),
        array(
            'name' => 'onPage',
            'group' => 'news',
            'title' => 'news.on_page_main',
            'section' => 'main',
            'editor' => 'int'
        ),
        array(
            'name' => 'section_all',
            'group' => 'news',
            'title' => 'news.section_all',
            'editor' => 'string'
        ),
        array(
            'name' => 'onMainShowType',
            'group' => 'news',
            'title' => 'news.on_main_show_type',
            'editor' => 'select_news_on_main_type'
        ),
        /** articles */
        array(
            'name' => 'titleOnMain',
            'group' => 'articles',
            'title' => 'articles.titleOnMain',
            'editor' => 'string'
        ),
        array(
            'name' => 'onPage',
            'group' => 'articles',
            'title' => 'articles.on_column',
            'editor' => 'int'
        ),
        /** review */
        array(
            'name' => 'titleOnMain',
            'group' => 'Review',
            'title' => 'news.title_on_main',
            'editor' => 'string'
        ),
        array(
            'name' => 'onPage',
            'group' => 'Review',
            'title' => 'review.show_on_page',
            'section' => 'all',
            'editor' => 'int'
        ),
        array(
            'name' => 'maxLen',
            'group' => 'Review',
            'title' => 'review.maxLen',
            'editor' => 'int'
        ),
        array(
            'name' => 'section_id',
            'group' => 'Review',
            'title' => 'review.section_id_editor',
            'editor' => 'int'
        ),
        /** catalog */
        array(
            'name' => 'titleOnMain',
            'group' => 'catalog',            
            'title' => 'catalog.param_title',
            'section' => 'main',
            'editor' => 'string'
        ),
        array(
            'name' => 'onPage',
            'group' => 'catalog',
            'title' => 'catalog.param_on_page',
            'section' => 'main',
            'editor' => 'int'
        ),
        array(
            'name' => 'listTemplate',
            'group' => 'catalog',
            'title' => 'editor.type_catalog_view',
            'section' => 'main',
            'editor' => 'select'
        ),
        /** catalog 2 */
        array(
            'name' => 'titleOnMain',
            'group' => 'catalog2',
            'title' => 'catalog.param_title',
            'section' => 'main',
            'editor' => 'string'
        ),
        array(
            'name' => 'onPage',
            'group' => 'catalog2',
            'title' => 'catalog.param_on_page',
            'section' => 'main',
            'editor' => 'int'
        ),
        array(
            'name' => 'listTemplate',
            'group' => 'catalog2',
            'title' => 'editor.type_catalog_view',
            'section' => 'main',
            'editor' => 'select'
        ),
        /** catalog 3 */
        array(
            'name' => 'titleOnMain',
            'group' => 'catalog3',
            'title' => 'catalog.param_title',
            'section' => 'main',
            'editor' => 'string'
        ),
        array(
            'name' => 'onPage',
            'group' => 'catalog3',
            'title' => 'catalog.param_on_page',
            'section' => 'main',
            'editor' => 'int'
        ),
        array(
            'name' => 'listTemplate',
            'group' => 'catalog3',
            'title' => 'editor.type_catalog_view',
            'section' => 'main',
            'editor' => 'select'
        ),
        /** CatalogFilter */
        array(
            'name' => 'linkedSection',
            'group' => 'CatalogFilter',
            'title' => 'params.section_cat_filter_form',
            'section' => 'main',
            'editor' => 'select_cat_search_sections'
        ),
        /** collection */
        array(
            'name' => 'titleOnMain',
            'group' => 'collection',
            'title' => 'collections.param_title',
            'section' => 'main',
            'editor' => 'string'
        ),
        array(
            'name' => 'listTemplate',
            'group' => 'collection',
            'title' => 'Editor.type_category_view',
            'section' => 'main',
            'editor' => 'select'
        ),
    );

    /**
     * Группы
     * @var array
     */
    private $aGroups = array(
        'news' => 'groups_news',
        'articles' => 'groups_articles',
        'Review' => 'groups_Review',
        'catalog' => 'groups_catalog',        
        'catalog2' => 'groups_catalog2',
        'catalog3' => 'groups_catalog3',
        'CatalogFilter' => 'groups_catalog_filter',
        'collection' => 'groups_catalog_collection',
    );

    /**
     * Получаем набор параметров
     */
    public function getParams(){
        return $this->aParams;
    }

    /**
     * Добавляем форму
     * @param \ExtForm $oInterface
     */
    public function addForm(\ExtForm $oInterface){

        $oForm = new UI\Form();

        $aValues = array();

        $bCatalogSite = Site\Type::hasCatalogModule();

        $oApi = new \skewer\build\Component\Installer\Api();

        $bCollectionInstall = $oApi->isInstalled('Collections', \Layer::CATALOG);

        foreach($this->aParams as $aParam){

            if (!$bCatalogSite && in_array( $aParam['group'], ['CatalogFilter','catalog','catalog2','catalog3'])){
                continue;
            }

            if (!$bCollectionInstall && $aParam['group'] == 'collection')
                continue;

            if (!isset($aParam['section'])){
                $aParam['section'] = 'all';
            }
            switch ($aParam['section']){
                case 'all':
                default:
                    $iSection = \Yii::$app->sections->languageRoot();
                    break;
                case 'main':
                    $iSection = \Yii::$app->sections->main();
                    break;
            }

            $oParam = Parameters::getByName( $iSection, $aParam['group'], $aParam['name'] );

            if (!$oParam){
                $oParam = Parameters::createParam([
                    'parent' => $iSection, 'group' => $aParam['group'], 'name' => $aParam['name'],
                    'access_level' => 0
                ]);
                $oParam->save();
            }

            if (!$oParam->id){
                continue;
            }

            $aValues['param_'.$oParam->id] = $oParam->value;

            if (strpos($aParam['title'], '.') !== false){
                list($sCategory, $sTitle) = explode('.', $aParam['title'], 2);
                $aParam['title'] = \Yii::t($sCategory, $sTitle);
            }

            switch ($aParam['editor']){
                case 'select':
                    $aData = array();
                    if ($oParam->show_val){
                        foreach ( explode( "\n", $oParam->show_val ) as $sRow ) {
                            if ( strpos( $sRow, ' ' ) !== false )
                                list( $sKey, $sVal ) = explode( ' ', $sRow, 2 );
                            else
                                $sKey = $sVal = $sRow;

                            if (($iPoint = strpos($sVal, '.')) !== 0){
                                $langVal = \Yii::t(substr($sVal, 0, $iPoint), substr($sVal, $iPoint+1));
                                if ($sVal !== substr($sVal, $iPoint+1))
                                    $sVal = $langVal;
                            }

                            $aData[$sKey] = $sVal;
                        }
                    }
                    $oField = new UI\Form\Select('param_'.$oParam->id, $aParam['title']);
                    $oField->setItems($aData);
                    break;

                case 'select_cat_search_sections':
                    $aData = Section::getSearchList();
                    $oField = new UI\Form\Select('param_'.$oParam->id, $aParam['title']);
                    $oField->setItems($aData);
                    break;

                case 'select_news_on_main_type':
                    $aData = [
                        'list' => \Yii::t('news', 'show_type_list'),
                        'column' => \Yii::t('news', 'show_type_columns')
                    ];
                    $oField = new UI\Form\Select('param_'.$oParam->id, $aParam['title']);
                    $oField->setItems($aData);
                    break;

                default:
                    $oField = new UI\Form\Field('param_'.$oParam->id, $aParam['title'], $aParam['editor']);
                    break;
            }

            $oField->setGroupTitle( \Yii::t( 'params', $this->aGroups[$oParam->group]) );
            $oForm->addField($oField);

        }

        // задание набора полей для интерфейса
        $oInterface->setFieldsByUiForm( $oForm );

        // контент
        $oInterface->setValues( $aValues );
    }

    /**
     * Сохранение параметров
     * @param array $aData
     */
    public function saveParams( array $aData ){

        foreach($aData as $sKey => $sValue){
            if (!preg_match('/param_(\d+)/', $sKey)){
                continue;
            }
            $iParamId = (int)substr($sKey, 6);

            $oParams = Parameters::getById( $iParamId );
            if ($oParams){
                $oParams->value = $sValue;
                $oParams->save();
            }
        }

    }

}