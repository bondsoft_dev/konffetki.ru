<?php

namespace skewer\build\Adm\HTMLBanners;

use skewer\build\Component\UI;
use skewer\build\Adm;
use skewer\build\Adm\HTMLBanners\models\Banners as Banners;
use Yii;
use yii\base\UserException;
use yii\helpers\ArrayHelper;
use \skewer\build\Adm\HTMLBanners\Api as BannerApi;

/**
 * Class Module
 * @package skewer\build\Adm\Auth
 */
class Module extends Adm\Tree\ModulePrototype{

    var $iSectionId;
    var $iPage = 0;
    var $iOnPage = 50;

    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt('page');

        // id текущего раздела
        $this->iSectionId = $this->getEnvParam('sectionId');
    }

    protected function actionInit() {

        $this->actionList();
    }

    /**
     * Список баннеров
     */
    protected function actionList() {

        $sort = $this->getInnerData('sort','id');
        if ($sort !== 'id')
            $banners = Banners::find()
                ->orderBy([$sort=>SORT_DESC])
                ->asArray()
                ->all();
        else
            $banners = Banners::find()
                ->asArray()
                ->all();

        if ($sort == 'location')
            usort($banners, function($banner1, $banner2) {
                return (Api::getBannerLocationsPos()[$banner1['location']] > Api::getBannerLocationsPos()[$banner2['location']]);
            });

        $oFormBuilder = new UI\StateBuilder();
        $oFormBuilder = $oFormBuilder::newList();
        $oFormBuilder
            ->field('title',\Yii::t('HTMLBanners' ,'field_title'),'s','string',['listColumns' => ['flex' => 3]])
            ->field('location',\Yii::t('HTMLBanners' ,'field_location'),'s','string',['listColumns' => ['flex' => 2]])
            ->field('active',\Yii::t('HTMLBanners' ,'field_active'),'i','check',['listColumns' => ['flex' => 1]])
            ->field('on_main',\Yii::t('HTMLBanners' ,'field_onmain'),'i','check',['listColumns' => ['flex' => 1]])
            ->field('on_allpages',\Yii::t('HTMLBanners' ,'field_allpages'),'i','check',['listColumns' => ['flex' => 1]])
            ->field('on_include',\Yii::t('HTMLBanners' ,'field_include'),'i','check',['listColumns' => ['flex' => 1]])
            ->buttonRowUpdate()
            ->buttonRowDelete()
            ->button(\Yii::t('adm', 'add'),'new','icon-add')
            ->button(\Yii::t('adm', 'sort'),'sort','icon-add')
           // ->enableDragAndDrop('sortItems')
        ;

        $oFormBuilder->addWidget('location', '\\skewer\\build\\Adm\\HTMLBanners\\Api', 'getBannerLocation');

        $oFormBuilder->setValue($banners);
        $oFormBuilder->setEditableFields(array('active','on_main','on_allpages','on_include'),'saveFromList');

        $this->setInterface($oFormBuilder->getForm());

    }
    /*
     * Драг энд дроп
     * */
    protected function actionsortItems(){
        //TODO обработчик драг энд дропа добавить.
    }
    /*
     * Сохранение из списка
     * */
    protected function  actionSaveFromList(){

        $iId = $this->getInDataValInt( 'id' );

        $sFieldName = $this->get('field_name');

        $oRow = Banners::findOne(['id'=>$iId]);
        /** @var Banners $oRow */
        if ( !$oRow )
            throw new UserException( "Запись [$iId] не найдена" ); //@todo языковая метка

        $oRow->$sFieldName = $this->getInDataVal( $sFieldName );

        $oRow->save();

        $this->actionInit();

    }



    /**
     * Отображение формы
     */
    protected function actionShow() {

        $aData = $this->get('data');
        $iItemId = ArrayHelper::getValue( $aData, 'id', 0);
        /** @var Banners $oBannersRow */
        $oBannersRow = Banners::findOne(['id'=>$iItemId]);
        $this->showForm($oBannersRow);
    }

    protected function actionSort(){
        $this->setInnerData('sort','location');
        $this->actionList();
    }

    /**
     * Форма добавления
     */
    protected function actionNew() {
        $this->showForm( Banners::getBlankBanner() );
    }

    /**
     * Отображение формы добавления/редактирования Баннера
     * @param Banners $oItem
     * @throws UserException
     */
    private function showForm( Banners $oItem ) {

        if ( !$oItem )
            throw new UserException('Item not found');

        // -- сборка интерфейса
        $oFormBuilder = new UI\StateBuilder();
        // создаем форму
        $oFormBuilder = $oFormBuilder::newEdit();
        $oFormBuilder->field('id', 'ID', 'i', 'hide')
            ->field('title',\Yii::t('HTMLBanners' ,'field_title'),'s')
            ->field('content',\Yii::t('HTMLBanners' ,'field_content'),'s','wyswyg')
            ->fieldSelect('location',\Yii::t('HTMLBanners' ,'field_location'),BannerApi::getBannerLocations())
            ->fieldSelect('section',\Yii::t('HTMLBanners' ,'field_section'),BannerApi::getSectionList())
            ->field('sort',\Yii::t('HTMLBanners' ,'field_sort'),'s','string')
            ->field('active',\Yii::t('HTMLBanners' ,'field_active'),'i','check')
            ->field('on_main',\Yii::t('HTMLBanners' ,'field_onmain'),'i','check')
            ->field('on_allpages',\Yii::t('HTMLBanners' ,'field_allpages'),'i','check')
            ->field('on_include',\Yii::t('HTMLBanners' ,'field_include'),'i','check')
        ;

        $oFormBuilder->button(\Yii::t('adm', 'save'),'save','icon-save');
        $oFormBuilder->button(\Yii::t('adm', 'back'),'init','icon-cancel');

        $oFormBuilder->setValue($oItem->getAttributes());
        $this->setInterface($oFormBuilder->getForm());


    }

    /**
     * Сохранение баннера
     */
    protected function actionSave() {

        $aData = $this->get( 'data' );
        
        $iId = $this->getInDataValInt( 'id' );

        if ( !$aData )
            throw new UserException( 'Empty data' );

        if ( $iId ) {
            $oBannersRow = Banners::findOne(['id'=>$iId]);
            if ( !$oBannersRow )
                throw new UserException( "Запись [$iId] не найдена" );
        } else {
            $oBannersRow = Banners::getNewRow();

        }
        $oBannersRow->setAttributes($aData);

        $oBannersRow->save();

        \skLogger::addNoticeReport(\Yii::t('HTMLBanners', 'addBanner'),\skLogger::buildDescription($aData),\skLogger::logUsers,$this->getModuleNameAdm());

        // вывод списка
        $this->actionInit();

    }

    /**
     * Удаляет запись
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        Banners::deleteAll(['id' => $aData['id']]);

        \skLogger::addNoticeReport(\Yii::t('HTMLBanners', 'deleteBanner'), \skLogger::buildDescription($aData), \skLogger::logUsers, $this->getModuleNameAdm());

        // вывод списка
        $this->actionList();

    }

    /**
     * Установка служебных данных
     * @param UI\State\BaseInterface $oIface
     */
    protected function setServiceData( UI\State\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData( array(
            'sectionId' => $this->iSectionId,
            'page' => $this->iPage
        ) );

    }
}// class