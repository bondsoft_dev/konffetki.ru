<?php

namespace skewer\build\Adm\HTMLBanners;
use skewer\build\Component\I18N\Languages;
use skewer\build\Component\Section\Page;
use skewer\build\Component\Section\Tree;
use skewer\build\Component\Site\Section;
use yii\helpers\ArrayHelper;



/**
 * Class Api
 * @package skewer\build\Adm\HTMLBanners
 */
class Api {

    /**
     * Конфигурационный массив для положений
     * @var array
     */
    protected static $aBannerLocations = array(
        'left' => array(
            'name' => 'left',
            'title' => 'position_left',
            'pos' => 1),
        'right' => array(
            'name' => 'right',
            'title' => 'position_right',
            'pos' => 2),
        'content_top' => array(
            'name' => 'content_top',
            'title' => 'position_top',
            'pos' => 3),
        'content_bottom' => array(
            'name' => 'content_bottom',
            'title' => 'position_bottom',
            'pos' => 4)
    );

    /*
     * извлекает по дереву разделов все секции
     * */
    public static function getSectionList() {

        $iPolicyId = \Auth::getPolicyId( 'public' );
        $aSections = array( \Yii::$app->sections->root() => \Yii::t('search', 'all_site')  );

        if ($aLanguages = Languages::getAllActiveNames()){

            if ($aLanguages){
                foreach($aLanguages as $sLang){

                    if (count($aLanguages) > 1)
                        $aSections[\Yii::$app->sections->getValue(Page::LANG_ROOT, $sLang)] = Tree::getSectionTitle(\Yii::$app->sections->getValue(Page::LANG_ROOT, $sLang), true);

                    foreach ( Tree::getSectionList( \Yii::$app->sections->getValue('topMenu', $sLang), $iPolicyId ) as $aSection ){
                        if (isset($aSection['visible']) && $aSection['visible'] > 0){
                            $aSections[$aSection['id']] = $aSection['title'];
                        }
                    }

                    foreach ( Tree::getSectionList( \Yii::$app->sections->getValue('leftMenu', $sLang), $iPolicyId ) as $aSection ){
                        if (isset($aSection['visible']) && $aSection['visible'] > 0){
                            $aSections[$aSection['id']] = $aSection['title'];
                        }
                    }

                }
            }

        }


        return $aSections;
    }


    /**
     * @static
     * @return array
     */
    public static function getBannerLocations(){

        return ArrayHelper::map(self::$aBannerLocations, 'name', function($banner){
            return \Yii::t('HTMLBanners', $banner['title']);
        });

    }

    /**
     * @static
     * @return array
     */
    public static function getBannerLocation($banner){

        return \Yii::t('HTMLBanners', self::$aBannerLocations[$banner['location']]['title']);

    }

    /**
     * @static
     * @return array
     */
    public static function getBannerLocationsPos(){

        return ArrayHelper::map(self::$aBannerLocations, 'name', 'pos');

    }

    /**
     * @static
     * @return array
     */
    public static function getSectionTitle(){
        return Tree::getSectionsTitle( \Yii::$app->sections->root(), true );
    }

}// class