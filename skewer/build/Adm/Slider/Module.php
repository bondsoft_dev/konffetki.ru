<?php

namespace skewer\build\Adm\Slider;

use skewer\assets\JqueryAsset;
use skewer\assets\JqueryUiAsset;
use skewer\build\Component\orm\Query;
use skewer\build\Component\UI;
use skewer\build\Adm;
use skewer\build\Tool;
use skewer\build\Cms;
use Yii;


class Module extends Cms\Tabs\ModulePrototype implements Adm\Tree\ModuleInterface {

    const section = \Layer::ADM;
    const tool = \Layer::TOOL;

    public $pageId = 0;

    /**
     * Отдает режим работы модуля
     * @return string
     */
    protected function getWorkMode() {
        return self::section;
    }

    protected function isSectionMode() {
        return $this->getWorkMode()===self::section;
    }

    protected function isToolMode() {
        return $this->getWorkMode()===self::tool;
    }

    function getPageId() {
        // id не нужен
        return $this->pageId;
    }

    public function __construct( \skContext $oContext ) {
        parent::__construct( $oContext );
        $oContext->setModuleLayer( 'Adm' );
        $oContext->setModuleWebDir( '/skewer/build/Adm/Slider' );
    }

    protected $iCurrentBanner = 0;
    protected $iCurrentSlide = 0;

    /** @var int Параметр из раздела */
    protected $currentBanner = 0;


    protected function preExecute() {

        if ( $this->currentBanner )
            $this->iCurrentBanner = $this->currentBanner;

    }


    protected function actionInit() {

        // вывод списка
        if ( $this->currentBanner )
            $this->actionSlideList();
        else
            $this->actionBannerList();

        $this->addCssFile(\Yii::$app->getAssetManager()->getBundle(\skewer\build\Adm\Slider\Asset::className())->baseUrl.'/css/SlideShower.css');
        $this->addJSFile(\Yii::$app->getAssetManager()->getBundle(\skewer\build\Adm\Slider\Asset::className())->baseUrl.'/js/SlideLoadImage.js');
        $this->addJSFile(\Yii::$app->getAssetManager()->getBundle(\skewer\build\Adm\Slider\Asset::className())->baseUrl.'/js/SlideShower.js');

    }


    /**
     * Список баннеров
     */
    protected function actionBannerList() {

        $oFormBuilder = new UI\StateBuilder();

        $this->iCurrentBanner = 0;

        // создаем форму
        $oFormBuilder->createFormList();

        // добавляем поля
        $oFormBuilder
            ->addField( 'title', \Yii::t('slider', 'title'), 's', 'string' )
            ->addField( 'preview_img', '', 's', 'addImg', array('listColumns' => array('flex' => 1)) )
            ->addField( 'active', \Yii::t('slider', 'active'), 'i', 'check' )
            ->setFields()
            ->addWidget( 'preview_img', 'skewer\\build\\Adm\\Slider\\Banner', 'getPreviewImg' )
            ->setEditableFields( array( 'active' ), 'saveBanner' );
        ;

        $aItems = Banner::find()->getAll();

        // добавляем данные
        $oFormBuilder->setValue( $aItems );

        // элементы управления
        $oFormBuilder
            ->addButton( \Yii::t('slider', 'addBanner'), 'editBannerForm', 'icon-add' )
            ->addButton( \Yii::t('slider', 'displaySettings'), 'toolsForm', 'icon-edit' )
            ->addRowButtonUpdate( 'SlideList' )
            ->addRowButtonDelete( 'delBanner' )
        ;

        $this->setInnerData('currentBanner', false);

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );
    }


    /**
     * Форма редактирования баннера
     * todo default fields
     */
    protected function actionEditBannerForm() {

        $iBannerId = $this->getInnerData('currentBanner');

        if ( $iBannerId ) {
            $oBannerRow = Banner::find( $iBannerId );
        } else {
            $oBannerRow = Banner::getNewRow();
            $oBannerRow->setVal( 'title', \Yii::t('slider', 'new_banner') );
            $oBannerRow->setVal( 'active', true );
        }


        $oFormBuilder = new UI\StateBuilder();

        // создаем форму
        $oFormBuilder->clear()->createFormEdit();//->addHeadText( $sHeadText );

        // добавляем поля
        //id	title	section	on_include
        $oFormBuilder
            ->addField( 'id', 'id', 'i', 'hide' )
            ->addField( 'title', \Yii::t('slider', 'title'), 's', 'string', array('listColumns' => array('flex' => 1)) )
            ->addSelectField( 'section', \Yii::t('slider', 'section'), 's', Banner::getSectionTitle() )
            ->addField( 'on_include', \Yii::t('slider', 'on_include'), 'i', 'check' )
            ->addField( 'bullet', \Yii::t('slider', 'bullet'), 'i', 'check' )
            ->addField( 'scroll', \Yii::t('slider', 'scroll'), 'i', 'check' )
            ->addField( 'active', \Yii::t('slider', 'active'), 'i', 'check' )
            ->setFields();

        // устанавливаем значения
        $oFormBuilder->setValue( $oBannerRow );

        // добавляем элементы управления
        $oFormBuilder->addButton( \Yii::t('adm','save'), 'saveBanner', 'icon-save' );
        if ( $iBannerId )
            $oFormBuilder->addButton( \Yii::t('slider', 'editSlides'), 'slideList', 'icon-edit' );
        $oFormBuilder->addButton( \Yii::t('adm','cancel'), 'bannerList', 'icon-cancel' );


        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }


    /**
     * Добавление/обновление баннера
     * todo отлов ошибок
     */
    protected function actionSaveBanner() {

        try {

            $aData = $this->getInData();

            $oRow = Banner::getNewRow();
            $oRow->setData( $aData );

            $oRow->save();

            if ( $oRow->getError() )
                throw new \Exception( $oRow->getError() );

            $this->actionBannerList();

        } catch ( \Exception $e ) {
            $this->addError( $e->getMessage() );
        }

    }


    /**
     * Удаление баннера
     */
    protected function actionDelBanner() {

        try {

            $aData = $this->getInData();

            $iBannerId = iSset( $aData['id'] ) ? $aData['id'] : false;

            if ( !$iBannerId )
                throw new \Exception("not found banner id=$iBannerId");

            $oRow = Banner::find( $iBannerId );

            if ( !$oRow )
                throw new \Exception("not found banner id=$iBannerId");

            // удаление слайдов от банера
            Slide::removeBanner( $iBannerId );

            $oRow->delete();

            $this->actionBannerList();

        } catch ( \Exception $e ) {

            $this->addError( $e->getMessage() );
        }

    }


    /**
     * Список слайдов для баннера
     */
    protected function actionSlideList() {

        $aData = $this->getInData();
        if ( !$this->iCurrentBanner && isSet($aData['id']) && $aData['id'] )
            $this->iCurrentBanner = $aData['id'];

        $this->iCurrentSlide = 0;

        $this->setInnerData('currentBanner', $this->iCurrentBanner);
        if ( !$this->iCurrentBanner )
            throw new \Exception( \Yii::t('slider', 'noFindBanner') );

        $oFormBuilder = new UI\StateBuilder();

        // создаем форму
        $oFormBuilder->createFormList();

        // добавляем поля
        $oFormBuilder
            ->addField( 'preview_img', '', 's', 'addImg', array('listColumns' => array('flex' => 1)) )
            ->addField( 'active', \Yii::t('slider', 'active'), 'i', 'check' )
            ->addField( 'slide_link', \Yii::t('slider', 'slides_link'), 's', 'string' )
            ->setFields()
            ->addWidget( 'preview_img', 'skewer\\build\\Adm\\Slider\\Slide', 'getSlideImg' )
            ->setEditableFields( array( 'active' ), 'saveSlide' )
            ->enableDragAndDrop( 'sortSlideList' )
        ;

        $aItems = Slide::find()->where( 'banner_id', $this->iCurrentBanner )->order( 'position' )->getAll();

        //@TODO Активность приходит строкой и приведеним типов в JS превращается в true
        foreach( $aItems as $oItem )
            $oItem->active = (bool)$oItem->active;

        // добавляем данные
        $oFormBuilder->setValue( $aItems );

        // элементы управления
        $oFormBuilder
            ->addButton( \Yii::t('slider', 'addSlide'), 'editSlideForm', 'icon-add' );

        if ( !$this->currentBanner )
            $oFormBuilder
                ->addButton( \Yii::t('slider', 'editBanner'), 'editBannerForm', 'icon-edit' )
                ->addButton( \Yii::t('adm','back'), 'bannerList', 'icon-cancel' );

        $oFormBuilder
            ->addRowButtonUpdate( 'editSlideForm' )
            ->addRowButtonDelete( 'delSlide' )
        ;

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;

    }


    protected function actionSortSlideList() {

        $aData = $this->get( 'data' );
        $aDropData = $this->get( 'dropData' );
        $sPosition = $this->get( 'position' );

        if( !isSet($aData['id']) || !$aData['id'] ||
            !isSet($aDropData['id']) || !$aDropData['id'] || !$sPosition )
            $this->addError( \Yii::t('slider', 'sortError') );

        if( ! Slide::sort( $aData['id'], $aDropData['id'], $sPosition ) )
            $this->addError( \Yii::t('slider', 'sortError') );

    }


    /**
     * Форма редактиорвания слайда
     * @return int
     */
    protected function actionEditSlideForm() {

        $aData = $this->getInData();
        $iItemId = isSet($aData['id']) ? $aData['id'] : $this->iCurrentSlide;
        $this->iCurrentSlide = $iItemId;

        $sUploadImage = isSet($aData['upload_image']) ? $aData['upload_image'] : '';

        $this->addJSFile( \Yii::$app->getAssetManager()->getBundle(JqueryAsset::className())->baseUrl.'/jquery.js' );
        $this->addJSFile( \Yii::$app->getAssetManager()->getBundle(JqueryUiAsset::className())->baseUrl.'/jquery-ui.min.js' );

        if ( $iItemId )
            $oSlideRow = Slide::find( $iItemId );
        else {
            $oSlideRow = Slide::getNewRow();
            $oSlideRow->setVal( 'active', true );
        }


        if ( $sUploadImage )
            $oSlideRow->setVal( 'img', $sUploadImage );

        if ( !$oSlideRow->getVal( 'img' ) )
            $oSlideRow->setVal( 'img', Slide::getEmptyImgWebPath() );

        $oFormBuilder = new UI\StateBuilder();

        // создаем форму
        $oFormBuilder->clear()->createFormEdit();

        // добавляем поля
        $oFormBuilder
            ->addField( 'id', 'id', 'i', 'hide' )
            ->addField( 'img', '', 's', 'hide' )
            ->addSpecField( 'canv', \Yii::t('slider', 'editSlide'), 'SlideShower', $oSlideRow )
            ->addField( 'active', \Yii::t('slider', 'active'), 'i', 'check' )
            ->addField( 'slide_link', \Yii::t('slider', 'slides_link'), 's', 'string' )
            ->setFields();

        // устанавливаем значения
        $oFormBuilder->setValue( $oSlideRow );

        /* элементы управления */
        //$this->addLibClass( 'SlideLoadImage' );


        // добавляем элементы управления
        $oFormBuilder->addButton( \Yii::t('adm','save'), 'saveSlide', 'icon-save', 'save' );
        $oFormBuilder->addExtButton(
            \ExtDockedByUserFile::create( \Yii::t('slider', 'backLoad'), 'SlideLoadImage' )
            ->setIconCls( \ExtDocked::iconAdd )
            ->setAddParam( 'slideId', $iItemId )
        );
        if ( $iItemId )
            $oFormBuilder->addButton( \Yii::t('slider', 'editSlideText'), 'editSlideText', 'icon-edit' );
        $oFormBuilder->addButton( \Yii::t('adm','cancel'), 'slideList', 'icon-cancel' );

        $this->addInitParam('lang', ['galleryUploadingImage' => \Yii::t('gallery', 'galleryUploadingImage')]);

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }


    /**
     * Форма расширенного редактирования текстов для слайда
     * @return int
     * @throws \Exception
     */
    protected function actionEditSlideText() {

        if ( !$this->iCurrentSlide )
            throw new \Exception( \Yii::t('slider', 'noFindSlide') );

        $oSlideRow = Slide::find( $this->iCurrentSlide );


        $oFormBuilder = new UI\StateBuilder();

        // создаем форму
        $oFormBuilder->clear()->createFormEdit();

        // добавляем поля
        $oFormBuilder
            ->addField( 'id', 'id', 'i', 'hide' )
            ->addFieldWithValue( 'back2edit', '', 'i', 'hide', true )
            ->addField( 'text1', \Yii::t('slider', 'slides_text1'), 's', 'wyswyg' )
            ->addField( 'text2', \Yii::t('slider', 'slides_text2'), 's', 'wyswyg' )
            ->addField( 'text3', \Yii::t('slider', 'slides_text3'), 's', 'wyswyg' )
            ->addField( 'text4', \Yii::t('slider', 'slides_text4'), 's', 'wyswyg' )
            ->setFields();

        // устанавливаем значения
        $oFormBuilder->setValue( $oSlideRow );

        // добавляем элементы управления
        $oFormBuilder->addButton( \Yii::t('adm','save'), 'saveSlide', 'icon-save', 'save' );
        $oFormBuilder->addButton( \Yii::t('adm','cancel'), 'EditSlideForm', 'icon-cancel' );

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }


    /**
     * Обработка загруженного изображения для фона слайда
     * @return mixed
     */
    protected function actionUploadImage() {

        $iItemId = (int) $this->get( 'slideId' );

        $sSourceFN = Slide::uploadFile();

        $this->set(
            'data',
            array(
                'id' => $iItemId,
                'upload_image'=>$sSourceFN
            )
        );

        $this->actionEditSlideForm();
    }


    /**
     * Состояние сохранения слайда
     */
    protected function actionSaveSlide() {

        $aData = $this->getInData();
        $bBackToEdit = isSet( $aData['back2edit'] ) ? $aData['back2edit'] : false;

        try {

            if( !$this->iCurrentBanner )
                throw new \Exception( \Yii::t('slider', 'noFindBanner') );

            $iSlideId = (isSet($aData['id']) && $aData['id']) ? $aData['id'] : false;

            if ( $iSlideId )
                $oSlideRow = Slide::find( $iSlideId );
            else
                $oSlideRow = Slide::getNewRow();

            // при обновлении из списка приходят неактуальные данные
            unset( $aData['position'] );

            $oSlideRow->setData( $aData );

            $oSlideRow->setVal( 'banner_id', $this->iCurrentBanner );

            // не сохраняем картинку, если это загрушка
            if ( $oSlideRow->getVal( 'img' ) == Slide::getEmptyImgWebPath() )
                $oSlideRow->setVal( 'img', '' );

            if ( !$oSlideRow->getVal( 'position' ) )
                $oSlideRow->setVal( 'position', Slide::getMaxPos4Banner($this->iCurrentBanner)+1 );
            // todo position - перенести в модификаор и проверить механику с исключением текущего слайда по id

            $oSlideRow->save();

        } catch ( \Exception $e ) {

            $this->addError( $e->getMessage() );
        }

        $this->set( 'data', false );

        if ( $bBackToEdit )
            $this->actionEditSlideForm();
        else
            $this->actionSlideList();
    }


    /**
     * Состояние удаления слайда
     */
    protected function actionDelSlide() {

        $aData = $this->getInData();

        try {

            if( !$this->iCurrentBanner )
                throw new \Exception( \Yii::t('slider', 'noFindBanner') );

            $iSlideId = (isSet($aData['id']) && $aData['id']) ? $aData['id'] : false;

            if ( !$iSlideId )
                throw new \Exception( \Yii::t('slider', 'noFindSlide') );

            $oSlideRow = Slide::find( $iSlideId );

            if ( !$oSlideRow )
                throw new \Exception( \Yii::t('slider', 'noFindSlide') );

            $oSlideRow->delete();

        } catch ( \Exception $e ) {

            $this->addError( $e->getMessage() );
        }

        $this->actionSlideList();
    }


    /**
     * Форма настройки параметров показа баннеров
     * @return int
     */
    protected function actionToolsForm() {

        $aToolData = array();
        $aItems = Query::SelectFrom( 'banners_tools' )->getAll();
        foreach ( $aItems as $aItem )
            $aToolData[ $aItem['bt_key'] ] = $aItem['bt_value'];

        $oFormBuilder = new UI\StateBuilder();

        // создаем форму
        $oFormBuilder->clear()->createFormEdit();

        // добавляем поля
        $oFormBuilder
            ->addSelectField( 'mode', \Yii::t('slider', 'animtype'), 's',
                array(
                    'horizontal'=>'horizontal slide',
                  //  'vertical' => 'vertical slide',
                    'fade'=>'fade',
                )
            )
            ->addField( 'pause', \Yii::t('slider', 'animduration'), 'i', 'str' ) //def:450
            ->addField( 'speed', \Yii::t('slider', 'animspeed'), 'i', 'str' ) //def:4000
            ->addField( 'autoHover', \Yii::t('slider', 'hoverpause'), 'i', 'check' ) //def:true
            ->addField( 'auto', \Yii::t('slider', 'autostart'), 'i', 'check' ) //def:true
            ->addField( 'infiniteLoop', \Yii::t('slider', 'infiniteLoop'), 'i', 'check' ) // true
            ->addField( 'maxHeight', \Yii::t('slider', 'banner_h'), 'i', 'str' ) // 330
            ->addField( 'responsive', \Yii::t('slider', 'responsive'), 'i', 'check' ) // 330
            ->setFields();

        // устанавливаем значения
        $oFormBuilder->setSimpleValue( $aToolData );

        // добавляем элементы управления
        $oFormBuilder->addButton( \Yii::t('adm','save'), 'saveTools', 'icon-save' );
        $oFormBuilder->addButton( \Yii::t('adm','cancel'), 'bannerList', 'icon-cancel' );

        // сформировать интерфейс
        $this->setInterface( $oFormBuilder->getForm() );

        return psComplete;
    }


    /**
     * Состояние сохранения параметров показа баннеров
     */
    protected function actionSaveTools() {

        $aData = $this->get('data');

        foreach ( $aData as $sKey => $sValue ) {
            Query::InsertInto( 'banners_tools' )
                ->set( 'bt_key', $sKey )
                ->set( 'bt_value', $sValue )
                ->onDuplicateKeyUpdate()
                ->set( 'bt_value', $sValue )
                ->get();
        }

        $this->actionBannerList();
    }

}