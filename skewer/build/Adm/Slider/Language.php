<?php

$aLangauge = array();

$aLangauge['ru']['tab_name'] = 'Слайдер';
$aLangauge['ru']['addBanner'] = 'Добавить баннер';
$aLangauge['ru']['displaySettings'] = 'Настройки показа';
$aLangauge['ru']['editSlides'] = 'Редактировать слайды';
$aLangauge['ru']['editBanner'] = 'Настройки баннера';
$aLangauge['ru']['addSlide'] = 'Добавить слайд';
$aLangauge['ru']['editSlide'] = 'Настройка слайда';
$aLangauge['ru']['id'] = 'Идентификатор баннера';
$aLangauge['ru']['title'] = 'Название баннера';
$aLangauge['ru']['section'] = 'Разделы для показа';
$aLangauge['ru']['active'] = 'Активность';
$aLangauge['ru']['on_include'] = 'Показывать на внутренних страницах';
$aLangauge['ru']['bullet'] = 'Показывать буллет';
$aLangauge['ru']['scroll'] = 'Показывать прокрутку вперед/назад';
$aLangauge['ru']['slides_id'] = 'Идентификатор слайда';
$aLangauge['ru']['slides_banner_id'] = 'Баннер';
$aLangauge['ru']['slides_active'] = 'Активность';
$aLangauge['ru']['slides_link'] = 'Ссылка';
$aLangauge['ru']['slides_position'] = 'Порядковый номер';
$aLangauge['ru']['slides_img'] = 'Фоновое изображение';
$aLangauge['ru']['slides_text1'] = 'Текстовый блок 1';
$aLangauge['ru']['slides_text2'] = 'Текстовый блок 2';
$aLangauge['ru']['slides_text3'] = 'Текстовый блок 3';
$aLangauge['ru']['slides_text4'] = 'Текстовый блок 4';
$aLangauge['ru']['editSlideText'] = 'Тексты';
$aLangauge['ru']['noFindBanner'] = 'Ошибка! Баннер не найден';
$aLangauge['ru']['noFindSlide'] = 'Ошибка! Слайд не найден';
$aLangauge['ru']['sortError'] = 'Ошибка! Неверно заданы параметры сортировки';
$aLangauge['ru']['backLoad'] = 'Загрузить фон';
$aLangauge['ru']['animtype'] = 'Тип анимации';
$aLangauge['ru']['animspeed'] = 'Время анимации смены слайда, мс';
$aLangauge['ru']['animduration'] = 'Время задержки между слайдами, мс';
$aLangauge['ru']['hoverpause'] = 'Останавливать автоматическую смену слайдов при наведении курсора';
$aLangauge['ru']['banner_w'] = 'Ширина баннера';
$aLangauge['ru']['banner_h'] = 'Высота баннера';
$aLangauge['ru']['new_banner'] = 'Новый баннер';

$aLangauge['ru']['autostart'] = 'Автостарт';
$aLangauge['ru']['infiniteLoop'] = 'Зациклить';
$aLangauge['ru']['responsive'] = 'Адаптивность';

$aLangauge['en']['tab_name'] = 'Slider';
$aLangauge['en']['addBanner'] = 'Add banner';
$aLangauge['en']['displaySettings'] = 'Display settings';
$aLangauge['en']['editSlides'] = 'Edit slides';
$aLangauge['en']['editBanner'] = 'Edit banner';
$aLangauge['en']['addSlide'] = 'Add slide';
$aLangauge['en']['editSlide'] = 'Edit slide';
$aLangauge['en']['id'] = 'ID';
$aLangauge['en']['title'] = 'Title';
$aLangauge['en']['section'] = 'Sections';
$aLangauge['en']['active'] = 'Active';
$aLangauge['en']['on_include'] = 'Show On internal pages';
$aLangauge['en']['bullet'] = 'Show bullet';
$aLangauge['en']['scroll'] = 'Show scroll forward/backward';
$aLangauge['en']['slides_id'] = 'Slide ID';
$aLangauge['en']['slides_banner_id'] = 'Banner';
$aLangauge['en']['slides_active'] = 'Active';
$aLangauge['en']['slides_position'] = 'Position';
$aLangauge['en']['slides_link'] = 'Link';
$aLangauge['en']['slides_img'] = 'Background image';
$aLangauge['en']['slides_text1'] = 'Text block 1';
$aLangauge['en']['slides_text2'] = 'Text block 2';
$aLangauge['en']['slides_text3'] = 'Text block 3';
$aLangauge['en']['slides_text4'] = 'Text block 4';
$aLangauge['en']['editSlideText'] = 'Texts';
$aLangauge['en']['noFindBanner'] = 'Error! No find banner';
$aLangauge['en']['noFindSlide'] = 'Error! No find slide';
$aLangauge['en']['sortError'] = 'Error! Bad parameters for sort';
$aLangauge['en']['backLoad'] = 'Upload background';
$aLangauge['en']['animtype'] = 'Type of animation';
$aLangauge['en']['animspeed'] = 'Animation slide transition, ms';
$aLangauge['en']['animduration'] = 'Delay between slides, ms';
$aLangauge['en']['hoverpause'] = 'stop animation on mouseover hover';
$aLangauge['en']['banner_w'] = 'Banner width';
$aLangauge['en']['banner_h'] = 'Banner height';
$aLangauge['en']['new_banner'] = 'New banner';

$aLangauge['en']['autostart'] = 'Autostart';
$aLangauge['en']['infiniteLoop'] = 'Infinite Loop';
$aLangauge['en']['responsive'] = 'Adaptibility';



return $aLangauge;