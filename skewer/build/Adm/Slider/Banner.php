<?php

namespace skewer\build\Adm\Slider;


use skewer\build\Component\orm;
use skewer\build\Component\Section\Tree;

use skewer\build\libs\ft;


/**
 * Объект для работы с сущностью - Баннер
 * Class Banner
 * @package skewer\build\Adm\Slider
 */
class Banner extends orm\TablePrototype {

    protected static $sTableName = 'banners_main';

    protected static function initModel() {

        ft\Entity::get( 'banners_main' )
            ->clear()
            ->setTablePrefix('')
            ->setNamespace( __NAMESPACE__ )
            ->addField( 'title', 'varchar(255)', 'slider.title' )
                ->setDefaultVal( \Yii::t('slider', 'new_banner') )
            ->addField( 'section', 'int', 'slider.section' )
            ->addField( 'active', 'int(1)', 'slider.active' )
                ->setDefaultVal( 1 )
            ->addField( 'on_include', 'int(1)', 'slider.on_include' )
            ->addField( 'bullet', 'int(1)', 'slider.bullet' )
            ->addField( 'scroll', 'int(1)', 'slider.scroll' )
            ->addColumnSet(
                'list',
                array( 'title', 'active' )
            )
            ->addColumnSet(
                'edit',
                array( 'id','title' )
            )
            ->addColumnSet(
                'list_edit',
                array( 'active' )
            )
            ->save()
        ;
    }


    /*
     * Получаем поличество слайдов в банере
     * */
    public static function getSlidesCount4Banner( $iBannerId ){

        $aSlides = orm\Query::SelectFrom( 'banners_slides' )
            ->where( 'active' )
            ->andWhere( 'banner_id', $iBannerId )
            ->order( 'position' )
            ->getCount();

        return $aSlides;
    }


    public static function getBanner4Section( $iSectionId ) {

        //$aBanners = \MainBanner2BannersToolMapper::getBannersForSection( $iSectionId );

        $aTreeSection = Tree::getSectionParents( $iSectionId );

        $tempBanners = orm\Query::SelectFrom( 'banners_main' )
            ->where( 'active' )
            ->andWhere( 'section', $iSectionId )
            ->orWhere( 'on_include' )
            ->andWhere( 'section', $aTreeSection )
            ->andWhere( 'active' )
            ->getAll();

        // ищем баннеры заданные для раздела
        $bUseParentBanners = true;

        $aBanners = [];
        foreach ( $tempBanners as $tBanner ){
            if( $tBanner['section'] == $iSectionId )
                $bUseParentBanners = false;
            //проверим есть ли слайды в этом банере
            if (self::getSlidesCount4Banner($tBanner['id'])>0){
                $aBanners[] = $tBanner;
            }

        }

        // если нашли - удаляем баннеры родителей
        if ( !$bUseParentBanners ) {

            foreach($aBanners as $iKey=>$aBanner)
                if( $aBanner['section'] != $iSectionId )
                    unset($aBanners[$iKey]);

            $aBanners = array_values($aBanners);
        }

        return $aBanners;
    }


    public static function getAllTools() {

        $aToolData = array();
        $aItems = orm\Query::SelectFrom( 'banners_tools' )->getAll();

        foreach ( $aItems as $aItem ) {
            $mVal = $aItem['bt_value'];
            if (is_numeric($mVal))
                $mVal = (int)$mVal;
            $aToolData[$aItem['bt_key']] = $mVal;
        }

        return $aToolData;
    }


    /**
     * Виджет для картинки превью в списке баннеров
     * @param orm\ActiveRecord $oItem
     * @param $sField
     * @return string
     */
    public static function getPreviewImg( $oItem, $sField  ) {

        $iBannerId = $oItem->getVal( 'id' );

        if ( $iBannerId ) {

            /** @var orm\ActiveRecord $oRow */
            $oRow = Slide::find()->where( 'banner_id', $iBannerId )->where( 'active' )->order( 'position' )->getOne();

            if ( $oRow && ( $img = $oRow->getVal( 'img' ) ) ) {

                return $img;
            }
        }


        return Slide::getEmptyImgWebPath();
    }


    /**
     * Получение дерева разделов в виде списка
     * @return array
     */
    public static function getSectionTitle(){
        return Tree::getSectionsTitle( \Yii::$app->sections->root(), true );
    }


    public static function get4Section( $iSectionId ) {


        return array();
    }


} 