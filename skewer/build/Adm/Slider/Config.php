<?php
/* main */
$aConfig['name']     = 'Slider';
$aConfig['title']    = 'Слайдер';
$aConfig['version']  = '2.0';
$aConfig['description']  = 'Админ-интерфейс управления слайдером';
$aConfig['revision'] = '0002';
$aConfig['layer']     = \Layer::ADM;

return $aConfig; 
