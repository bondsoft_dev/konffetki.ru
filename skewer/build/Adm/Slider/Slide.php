<?php

namespace skewer\build\Adm\Slider;


use skewer\build\Component\orm;

use skewer\build\libs\ft;


/**
 * Объект для работы с сущностью - Слайд
 * Class Slide
 * @package skewer\build\Adm\Slider
 */
class Slide extends orm\TablePrototype {

    protected static $sTableName = 'banners_slides';

    /** Папка для слайдов */
    const path = 'slider';

    /**
     * Отдает путь до изображения
     * @throws \yii\base\InvalidConfigException
     */
    public static function getEmptyImgWebPath() {
        return \Yii::$app->getAssetManager()->getBundle(\skewer\build\Adm\Slider\Asset::className())->baseUrl.'/img/noimg.png';
    }

    protected static function initModel() {

        ft\Entity::get( 'banners_slides' )
            ->clear()
            ->setTablePrefix('')
            ->setNamespace( __NAMESPACE__ )
            ->addField( 'banner_id', 'int', 'slider.slides_banner_id' )
            ->addField( 'active', 'int(1)', 'slider.slides_active' )
                ->setDefaultVal( 1 )
            ->addField( 'slide_link', 'varchar(255)', 'slider.slides_link' )
            ->addField( 'position', 'int', 'slider.slides_position' )
            ->addField( 'img', 'varchar(255)', 'slider.slides_img' )

            ->addField( 'text1', 'text', 'slider.slides_text1' )
            ->addField( 'text1_h', 'int' )
            ->addField( 'text1_v', 'int' )

            ->addField( 'text2', 'text', 'slider.slides_text1' )
            ->addField( 'text2_h', 'int' )
            ->addField( 'text2_v', 'int' )

            ->addField( 'text3', 'text', 'slider.slides_text1' )
            ->addField( 'text3_h', 'int' )
            ->addField( 'text3_v', 'int' )

            ->addField( 'text4', 'text', 'slider.slides_text1' )
            ->addField( 'text4_h', 'int' )
            ->addField( 'text4_v', 'int' )
            ->save()
        ;
    }


    /**
     * Получение набора слайдов для баннера
     * @param $iBannerId
     * @return mixed
     */
    public static function getSlides4Banner( $iBannerId ) {

        $aSlides = orm\Query::SelectFrom( 'banners_slides' )
            ->where( 'active' )
            ->andWhere( 'banner_id', $iBannerId )
            ->order( 'position' )
            ->getAll();

        $return = array();


        foreach ($aSlides as $aSlide) 
            if ($aSlide['img'] != '') $return[] = $aSlide;
        

        return $return;
    }


    /**
     * Получение изображения для вывода в подложке слайда
     * @param orm\ActiveRecord $oItem
     * @param $sField
     * @return string
     */
    public static function getSlideImg( $oItem, $sField  ) {

        $img = $oItem->getVal( 'img' );

        return $img ? $img : self::getEmptyImgWebPath();
    }


    /**
     * Загружает изображения и перемещает в целевую директорию
     * @static
     * @return bool|string
     * @throws \Exception
     */
    public static function uploadFile() {

        // параметры загружаемого файла
        $aFilter = array();
        $aFilter['size']            = \Yii::$app->params['upload']['maxsize'];
        $aFilter['allowExtensions'] = \Yii::$app->params['upload']['allow']['images'];
        $aFilter['imgMaxWidth']     = \Yii::$app->params['upload']['images']['maxWidth'];
        $aFilter['imgMaxHeight']    = \Yii::$app->params['upload']['images']['maxHeight'];

        $aValues = \skewer\build\Adm\Files\Api::getLanguage4Uploader();
        \skUploadedFiles::loadErrorMessages($aValues);

        // загрузка
        $oFiles = \skUploadedFiles::get( $aFilter, FILEPATH, PRIVATE_FILEPATH );

        // если ни один файл не загружен - выйти
        if ( !$oFiles->count() )
            throw new \Exception( $oFiles->getError() );

        /*
         * пока берется только один файл, но делался задел на загрузку
         * большего количества
         */

        // имя файла исходника
        $sSourceFN = false;

        /** @noinspection PhpUnusedLocalVariableInspection */
        foreach ( $oFiles  as  $file ) {

            // проверка ошибок
            if ( $sError = $oFiles->getError() )
                throw new \Exception( $sError );

            /* @todo Должна быть добавлена проверка на защиту директории (protected: true/false) */
            $iSectionId = \Yii::$app->sections->main();
            $sSourceFN = $oFiles->UploadToSection( $iSectionId, static::path, false );

        }

        // проверка наличия имени файла
        if( !$sSourceFN )
            throw new \Exception( \Yii::t('gallery', 'noLoadImage') );

        // отрезать корневую папку от пути
        $sSourceFN = substr( $sSourceFN, strlen(WEBPATH)-1 );
        return $sSourceFN;
    }


    public static function getCount4Banner( $iBannerId ) {
        $iCount = orm\Query::SelectFrom( static::$sTableName )
            ->where( 'banner_id', (int)$iBannerId )
            ->getCount();
        return $iCount;
    }


    /**
     * Получение максимальной позиции
     * @param $iBannerId
     * @return int
     */
    public static function getMaxPos4Banner( $iBannerId ) {

        // todo реализовать max() дла запросов
        $aItems = orm\Query::SelectFrom( static::$sTableName )
            ->where( 'banner_id', (int)$iBannerId )
            ->getAll();

        $iPos = 0;
        foreach ( $aItems as $aItem ) {
            $iCurPos = $aItem['position'];
            if ( $iPos < $iCurPos )
                $iPos = $iCurPos;
        }

        return $iPos;
    }


    public static function sort( $aItemId, $aTargetId, $sOrderType='before' ) {

        $oItem = self::find( $aItemId );
        $oTarget = self::find( $aTargetId );

        if ( !$oItem || !$oTarget )
            return false;

        $sSortField = 'position';

        // должны быть в одной форме
        if( $oItem->getVal('banner_id') != $oTarget->getVal('banner_id') )
            return false;

        $iItemPos = $oItem->getVal($sSortField);
        $iTargetPos = $oTarget->getVal($sSortField);

        // выбираем напрвление сдвига
        if ( $iItemPos > $iTargetPos ) {

            $iStartPos = $iTargetPos;
            if( $sOrderType=='before' ) $iStartPos--;
            $iEndPos = $iItemPos;
            $iNewPos = $sOrderType=='before' ? $iTargetPos : $iTargetPos + 1;
            self::shiftPosition( $oItem->getVal('banner_id') , $iStartPos, $iEndPos, '+' );
            self::changePosition( $oItem->getVal('id') ,$iNewPos );

        } else {

            $iStartPos = $iItemPos;
            $iEndPos = $iTargetPos;
            if( $sOrderType=='after' ) $iEndPos++;
            $iNewPos = $sOrderType=='after' ? $iTargetPos : $iTargetPos - 1;
            self::shiftPosition( $oItem->getVal('banner_id') , $iStartPos, $iEndPos, '-' );
            self::changePosition( $oItem->getVal('id') ,$iNewPos );

        }

        return true;

    }


    private static function shiftPosition ( $iFormId,$iStartPos,$iEndPos,$sSign = '+' ) {

        orm\Query::UpdateFrom( static::$sTableName )
            ->set( 'position=position'.$sSign.'?', 1 )
            ->where( 'banner_id', (int)$iFormId )
            ->where( 'position>?', (int)$iStartPos )
            ->where( 'position<?', (int)$iEndPos )
            ->get();
    }


    private static function changePosition ( $iParamId, $iPos ) {

        orm\Query::UpdateFrom( static::$sTableName )
            ->set( 'position', (int)$iPos )
            ->where( 'id', (int)$iParamId )
            ->get();
    }


    /**
     * Удаление слайдов банера
     * @param int $iBannerId Ид банера
     * @return mixed
     */
    public static function removeBanner( $iBannerId ) {
        $res = orm\Query::DeleteFrom( static::$sTableName )
            ->where( 'banner_id', (int)$iBannerId )
            ->get();
        return $res;
    }


} 