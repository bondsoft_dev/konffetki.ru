<?php

namespace skewer\build\Adm\Slider;

/**
 * Class Install
 * @package skewer\build\Adm\Slider
 * @project Skewer
 *
 * @author kolesnikov, $Author: $
 * @version $Revision: $
 * @date $Date: $
 */
class Install extends \skModuleInstall {

    public function init() {
        return true;
    } // func

    public function install() {
        return true;
    } // func

    public function uninstall() {
        return true;
    } // func

} //class