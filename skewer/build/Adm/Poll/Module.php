<?php

namespace skewer\build\Adm\Poll;


use skewer\build\Component\UI;
use skewer\build\Adm;


/**
 * Class Module
 * @package skewer\build\Adm\Poll
 */
class Module extends Adm\Tree\ModulePrototype{

    var $iSectionId;
    var $iPage = 0;
    var $iOnPage = 0;
    var $iCurrentPoll = 0;

    var $oPollApi = NULL;

    protected function preExecute() {

        // номер страницы
        $this->iPage = $this->getInt('page');
        // id текущего раздела
        $this->iSectionId = $this->getEnvParam('sectionId');
        $this->iCurrentPoll = $this->getInt('poll_id', 0);
    }

    protected function actionInit() {

        $this->actionList();
    }

    /**
     * Список опросов
     */
    protected function actionList() {

        // объект для построения списка
        $oList = new \ExtList();

        $aModel = Api::getPollModel(Api::getPollListFields());

        // задать модель данных для вывода
        $oList->setFields( $aModel );

        // число записей на страницу
        $oList->setOnPage( $this->iOnPage );

        // добавление набора данных
        $aFilter = array(
            'limit' => $this->iOnPage ? array (
                'start' => $this->iPage*$this->iOnPage,
                'count' => $this->iOnPage
            ) : false,
        );

        // добавление набора данных
        $aItems = Api::getPollList($aFilter);

        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );

        // вывод данных в интерфейс
        $this->setInterface( $oList );

        /**
         * Интерфейс
         */
        // сортировка
        //$oList->addSorter('sort');
        //$oList->addSorter('locationTitle');

        // группировка
        $oList->setGroupField('locationTitle');

        // добавление кнопок
        $oList->addRowBtnUpdate();
        $oList->addRowBtnDelete();

        // кнопка добавления
        $oList->addBtnAdd('show');

        // вывод данных в интерфейс
        $this->setInterface( $oList );

    }

    /**
     * Отображение формы
     */
    protected function actionShow() {

        // подключить автоматический генератор форм
        $oForm = new \ExtForm();

        // номер новости
        $aData = $this->get('data');
        $iItemId = (is_array($aData) && isset($aData['id'])) ? (int)$aData['id'] : 0;
        if ( !$iItemId ) $iItemId = $this->iCurrentPoll;

        // запись новости
        $aItem = $iItemId ? Api::getPollById($iItemId): Api::getPollBlankValues();

        // установить набор элементов формы
        $oForm->setFields( Api::getPollModel(Api::getPollDetailFields()));

        // установить значения для элементов
        $oForm->setValues( $aItem );

        // добавление кнопок
        $oForm->addBtnSave();
        $oForm->addBtnCancel();

        if ( $iItemId ){
            $oForm->addDockedItem(array(
                'text' => \Yii::t('poll', 'answersTitle'),
                'iconCls' => 'icon-edit',
                'state' => 'answerList',
                'action' => 'answerList'
            ));

            $this->iCurrentPoll = $iItemId;

            $oForm->addBtnSeparator('->');
            $oForm->addBtnDelete();
        }

        // вывод данных в интерфейс
        $this->setInterface( $oForm );
    }

    /**
     * Список опросов
     */
    protected function actionAnswerList() {

        // объект для построения списка
        $oList = new \ExtList();

        $aModel = Api::getAnswerModel(Api::getAnswerListFields());

        // задать модель данных для вывода
        $oList->setFields( $aModel );

        $this->setAnswerListItems($oList);

        // вывод данных в интерфейс
        $this->setInterface( $oList );

        // добавление кнопок
        $oList->addRowBtnArray(array(
            'tooltip' => \Yii::t('adm','upd'),
            'iconCls' => 'icon-edit',
            'action' => 'showAnswerForm',
            'state' => 'edit_form'
        ));

        $oList->addRowBtnArray(array(
            'text' => '_delete',
            'iconCls' => 'icon-delete',
            'state' => 'answerDelete',
            'action' => 'answerDelete'
        ));

        // кнопка добавления
        $oList->addBtnAdd('showAnswerForm');
        $oList->addDockedItem(array(
            'text' => \Yii::t('poll', 'back'),
            'iconCls' => 'icon-cancel',
            'state' => 'edit_form',
            'action' => 'show'
        ));

        // вывод данных в интерфейс
        $this->setInterface( $oList );

    }

    /**
     * @param \ExtList $oList
     */
    protected function setAnswerListItems( \ExtList &$oList ) {

        $aData = $this->get( 'data' );

        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        if ( !$iItemId )
            $iItemId = (isset($aData['parent_poll']) ) ? (int)$aData['parent_poll'] : $iItemId;

        if ( !$iItemId )
            $iItemId = $this->iCurrentPoll;

        // добавление набора данных
        $aItems = Api::getAnswerList($iItemId);

        $oList->setValues( $aItems['items'] );
        $oList->setTotal( $aItems['count'] );

    }

    /**
     * Отображение формы
     */
    protected function actionShowAnswerForm() {

        // подключить автоматический генератор форм
        $oForm = new \ExtForm();

        // номер новости
        $aData = $this->get('data');

        $iItemId = (is_array($aData) && isset($aData['answer_id'])) ? (int)$aData['answer_id'] : 0;

        // запись новости
        $aItem = $iItemId ? Api::getAnswerById($iItemId): Api::getAnswerBlankValues();

        // установить набор элементов формы
        $oForm->setFields( Api::getAnswerModel(Api::getAnswerDetailFields()), $iItemId );

        // установить значения для элементов
        $oForm->setValues( $aItem );

        $oForm->addBtnSave('answerSave','answerSave');
        $oForm->addBtnCancel('answerList','answerList');

        if ( $iItemId ) {
            $oForm->addBtnSeparator('->');
            $oForm->addDockedItem(array(
                'text' => \Yii::t('poll', 'delete'),
                'iconCls' => 'icon-delete',
                'state' => 'answerDelete',
                'action' => 'answerDelete'
            ));
        }

        // вывод данных в интерфейс
        $this->setInterface( $oForm );
    }

    /**
     * Сохранение опроса
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->get( 'data' );

        // есть данные - сохранить
        if ( $aData )
            Api::updPoll( $aData );

        if ( isset($aData['id']) && $aData['id'] )
            \skLogger::addNoticeReport(\Yii::t('poll', 'editPoll'),\skLogger::buildDescription($aData),\skLogger::logUsers,$this->getModuleNameAdm());
        else {
            unset($aData['id']);
            \skLogger::addNoticeReport(\Yii::t('poll', 'addPoll'),\skLogger::buildDescription($aData),\skLogger::logUsers,$this->getModuleNameAdm());
        }

        // вывод списка
        $this->actionList();

    }

    /**
     * Сохранение варианта ответа
     */
    protected function actionAnswerSave() {

        // запросить данные
        $aData = $this->get( 'data' );

        $aData['parent_poll'] = $this->iCurrentPoll;

        // есть данные - сохранить
        if ( $aData )
            Api::updAnswer( $aData );

        if ( isset($aData['id']) && $aData['id'] )
            \skLogger::addNoticeReport(\Yii::t('poll', 'editPollAnswer'),\skLogger::buildDescription($aData),\skLogger::logUsers,$this->getModuleNameAdm());
        else {
            unset($aData['id']);
            \skLogger::addNoticeReport(\Yii::t('poll', 'addPollAnswer'),\skLogger::buildDescription($aData),\skLogger::logUsers,$this->getModuleNameAdm());
        }

        // вывод списка
        $this->actionAnswerList();

    }

    /**
     * Удаляет запись
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        // id записи
        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        // удаление
        Api::delPoll( $iItemId );

        \skLogger::addNoticeReport(\Yii::t('poll', 'deletePoll'),\skLogger::buildDescription($aData),\skLogger::logUsers,$this->getModuleNameAdm());

        // вывод списка
        $this->actionList();

    }

    /**
     * Удаляет запись
     */
    protected function actionAnswerDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        $aData['parent_poll'] = $this->iCurrentPoll;

        // удаление
        Api::delAnswer( $aData );

        \skLogger::addNoticeReport(\Yii::t('poll', 'deletePollAnswer'),\skLogger::buildDescription($aData),\skLogger::logUsers,$this->getModuleNameAdm());

        // вывод списка
        $this->actionAnswerList();

    }


    /**
     * Установка служебных данных
     * @param UI\State\BaseInterface $oIface
     */
    protected function setServiceData( UI\State\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData( array(
            'sectionId' => $this->iSectionId,
            'page' => $this->iPage,
            'poll_id' => $this->iCurrentPoll
        ) );

    }

}// class