<?php

namespace skewer\build\Adm\Poll;


use skewer\build\Component\Section\Tree;

use skewer\build\Page\Poll\AnswersMapper;
use skewer\build\Page\Poll\Mapper;


/**
 * Class Api
 * @package skewer\build\Adm\Poll
 */
class Api {

    /**
     * Конфигурационный массив для положений
     * @var array
     */
    protected static $aPollLocations = array(
        'left' => array('name' => 'left', 'title' => 'left_column', 'pos' => 1),
        'right' => array('name' => 'right', 'title' => 'right_column', 'pos' => 2)
    );

    /**
     * @static
     * @return array
     */
    public static function getPollLocations(){

        $aLocations = array();
        foreach(self::$aPollLocations as $aLocation){

            $aLocations[$aLocation['name']] = \Yii::t('poll', $aLocation['title']);
        };

        return $aLocations;
    }

    /**
     * Имя области, не найденной в конфинурационном массиве
     * @var string
     */
    protected static $sOthersTitle = 'Неактивные опросы';

    /**
     * @static
     * @return array
     */
    public static function getSectionTitle(){
        return Tree::getSectionsTitle( \Yii::$app->sections->root(), true );
    }

    /**
     * Метод возвращает список полей, выбираемых при отображении списка опросов
     * @return array
     */
    public static function getPollListFields(){
        return array(
            'id',
            'title',
            'question',
            'location',
            'locationTitle',
            'section',
            'sort',
            'active',
            'on_main',
            'on_allpages',
            'on_include'
        );
    }// function getListFields()

    /**
     * Метод возвращает список полей, выбираемых при отображении списка вариантов ответа для опроса
     * @return array
     */
    public static function getAnswerListFields(){
        return array(
            'answer_id',
            'title',
            'parent_poll',
            'value',
            'sort'
        );
    }// function getAnswersListFields()

    /**
     * Метод возвращает список полей, выбираемых при отображении формы редактирования опроса
     * @return array
     */
    public static function getPollDetailFields(){
        return array(
            'id',
            'title',
            'question',
            'location',
            'section',
            'active',
            'on_main',
            'on_include',
            'on_allpages',
            'sort'
        );
    }// function getDetailFields()

    /**
     * @return array
     */
    public static function getAnswerDetailFields(){
        return array(
            'answer_id',
            'title',
            'parent_poll',
            'value',
            'sort'
        );
    }// function getAnswersFields()

    /**
     * @static
     * @param $aFields
     * @return array
     */
    public static function getPollModel($aFields){

        return Mapper::getFullParamDefList($aFields);
    }

    /**
     * @static
     * @param $aFields
     * @return array
     */
    public static function getAnswerModel($aFields){

        return AnswersMapper::getFullParamDefList($aFields);
    }

    /**
     * @static
     * @return array
     */
    public static function getPollList(){

        $aFields = Mapper::getItems();

        foreach ( $aFields['items'] as &$aField ){

            // если элемент активен и есть такое положение в конфигурации
            if ( $aField['active'] and isset( self::$aPollLocations[$aField['location']] ) ) {

                // добавить соответствующие записи
                $aField['locationTitle'] = self::$aPollLocations[$aField['location']]['pos'].' - '. \Yii::t('poll', self::$aPollLocations[$aField['location']]['title']);

            } else {

                // нет - отнести к группе остальных
                $aField['location'] = '';
                $aField['locationTitle'] = self::$sOthersTitle;

            }

            // расстановка имен разедлов
            $iSection = $aField['section'];

            if ( $iSection ) {

                if ( !isset($aSections[$iSection]) )
                    $aSections[$iSection] = Tree::getSectionsTitle( $iSection ); // fixme делать запрос для каждого раздела жирновато

                $aField['section'] = $aSections[$iSection];
            }
        }

        return $aFields;
    }

    /**
     * @static
     * @param $iItemId
     * @return array|bool
     */
    public static function getAnswerList($iItemId){

        if ( !$iItemId ) return false;

        $aFilter['where_condition'] = array(
            'parent_poll' => array(
                'sign' => '=',
                'value' => $iItemId
            )
        );

        $aAnswers = AnswersMapper::getItems($aFilter);

        return $aAnswers;
    }

    /**
     * @static
     * @param $iPollId
     * @return array+
     */
    public static function getPollById($iPollId){

        $aFilter['where_condition'] = array(
            'id' => array(
                'sign' => '=',
                'value' => $iPollId
            )
        );

        $aPoll = Mapper::getItem($aFilter);

        return $aPoll;
    }

    /**
     * @static
     * @param $iAnswerId
     * @return array
     */
    public static function getAnswerById($iAnswerId){

        $aFilter['where_condition'] = array(
            'answer_id' => array(
                'sign' => '=',
                'value' => $iAnswerId
            )
        );

        $aAnswer = AnswersMapper::getItem($aFilter);

        return $aAnswer;
    }

    /**
     * @static
     * @param $aData
     * @return bool
     */
    public static function updPoll($aData){

        return Mapper::updPoll($aData);
    }

    /**
     * @static
     * @param $iPollId
     * @return bool
     */
    public static function delPoll($iPollId){

        return Mapper::delPoll($iPollId);
    }

    /**
     * @static
     * @param $aData
     * @return bool|int
     */
    public static function updAnswer($aData){

        return AnswersMapper::updAnswer($aData);
    }

    /**
     * @param $aInputData
     * @return bool|\skewer\build\Component\orm\service\DataBaseAdapter
     */
    public static function delAnswer($aInputData){

        return AnswersMapper::delAnswer($aInputData);
    }

    /**
     * @static
     * @return array
     */
    public static function getPollBlankValues(){
        return array(
            'title' => \Yii::t('poll', 'new_poll'),
            'active' => 1,
            'location' => 'left',
            'section' => 3,
            'sort' => ''
        );
    }

    /**
     * @static
     * @return array
     */
    public static function getAnswerBlankValues(){
        return array(
            'title' => \Yii::t('poll', 'new_answer'),
            'value' => '0'
        );
    }


}// class