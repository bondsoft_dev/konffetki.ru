<?php

/* main */
$aConfig['name']     = 'Poll';
$aConfig['title']    = 'Голосование (админ)';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Админ-интерфейс управления модулем голосования';
$aConfig['revision'] = '0002';
$aConfig['layer']     = Layer::ADM;
$aConfig['useNamespace'] = true;

return $aConfig;
