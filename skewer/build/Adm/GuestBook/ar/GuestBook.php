<?php
/**
 * User: Max
 * Date: 25.07.14
 */

namespace skewer\build\Adm\GuestBook\ar;

use skewer\build\Component\orm;
use skewer\build\libs\ft;

class GuestBook extends orm\TablePrototype {

    protected static $sTableName = 'guest_book';
    protected static $sKeyField = 'id';

    /** статус "новый" */
    const statusNew = 0;

    /** статус "одобрен" */
    const statusApproved = 1;

    /** статус "отклонен" */
    const statusRejected = 2;

    protected static function initModel() {

        ft\Entity::get(self::$sTableName)
            ->clear(false)
            ->setPrimaryKey(self::$sKeyField)
            ->setTablePrefix('')
            ->setNamespace(__NAMESPACE__)

            ->addField('rating','int(2)','review.field_rating')
            ->addField( 'parent', 'int(11)', 'review.field_parent' )
            ->addField( 'parent_class', 'varchar(20)', 'review.field_parent_class' )
            ->addField( 'date_time', 'datetime', 'review.field_date_time' )
            ->addField( 'name', 'varchar(128)', 'review.field_name' )
            ->addField( 'email', 'varchar(128)', 'review.field_email' )
            ->addField( 'content', 'text', 'review.field_content' )
            ->addField( 'status', 'int(1)', 'review.field_status' )
            ->addField( 'city', 'varchar(255)', 'review.field_city' )
            ->addField('on_main','int(1)','review.field_on_main')
            ->save();
    }

    public static function getNewRow($aData = array()) {
        $oRow = new GuestBookRow();
        if ($aData)
            $oRow->setData($aData);
        return $oRow;
    }
} 