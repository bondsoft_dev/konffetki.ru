<?php
/**
 * User: Max
 * Date: 25.07.14
 */

namespace skewer\build\Adm\GuestBook\ar;
use skewer\build\Component\orm;

class GuestBookRow extends orm\ActiveRecord {
    public $id = 0;
    public $parent = 0;
    public $parent_class = '';
    public $date_time = 0;
    public $name = '';
    public $email = '';
    public $content = '';
    public $status = 0;
    public $city = '';
    public $on_main = 0;
    public $rating = 0;

    function __construct() {
        $this->setTableName( 'guest_book' );
        $this->setPrimaryKey( 'id' );
    }


    /**
     * @inheritdoc
     */
    public function preSave()
    {
        if ( !$this->date_time || ($this->date_time == 'null') )
            $this->date_time = date("Y-m-d H:i:s", time());

        parent::preSave();
    }

} 