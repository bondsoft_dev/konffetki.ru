<?php

/* main */
$aConfig['name']     = 'GuestBook';
$aConfig['title']     = 'Отзывы';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Система администрирования Отзывов';
$aConfig['revision'] = '0002';
$aConfig['layer']     = Layer::ADM;
$aConfig['languageCategory']     = 'review';

$aConfig['dependency'] = [
    ['GuestBook', \Layer::PAGE],
    ['Review', \Layer::TOOL],
];


return $aConfig;
