<?php

use skewer\models\TreeSection;
use skewer\build\Component\Search;

/* main */
$aConfig['name']     = 'Tree';
$aConfig['title']    = 'Дерево';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Дерево разделов';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::ADM;
$aConfig['useNamespace'] = true;

$aConfig['events'][] = [
    'event' => TreeSection::EVENT_BEFORE_DELETE,
    'eventClass' => TreeSection::className(),
    'class' => TreeSection::className(),
    'method' => 'onSectionDelete',
];

$aConfig['events'][] = [
    'event' => Search\Api::EVENT_GET_ENGINE,
    'class' => TreeSection::className(),
    'method' => 'getSearchEngine',
];

return $aConfig;
