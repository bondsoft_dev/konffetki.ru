<?php


namespace skewer\build\Adm\Tree;


use skewer\build\Component\orm\Query;
use skewer\build\Component\Search\SearchIndex;
use skewer\build\Component\Search\Prototype;
use skewer\build\Component\Search\Row;
use skewer\build\Component\Section\Page;
use skewer\build\Component\Section;
use skewer\build\Component\Section\Tree;
use skewer\build\Component\Section\Parameters;
use skewer\build\Component\SEO\Service;
use skewer\build\Component\Site;
use skewer\build\Component\SEO;


class Search extends Prototype {

    /**
     * Флаг необходимости рекурсивного сброса данных по дереву разделов
     * @var bool
     */
    private $bRecursiveReset = false;

    /**
     * отдает имя идентификатора ресурса для работы с поисковым индексом
     * @return string
     */
    public function getName() {
        return 'Page';
    }

    /**
     * @inheritDoc
     */
    public function getModuleTitle() {
        return \Yii::t('page', 'tab_name');
    }

    /**
     * @inheritdoc
     */
    protected function update(Row $oSearchRow) {

        $oSearchRow->class_name = $this->getName();

        if (!$oSearchRow->object_id)
            return false;

        if ( $this->bRecursiveReset ){
            $this->resetSectionRecursive($oSearchRow->object_id);
            Service::updateSearchIndex();
        }

        /** Исключаем корневые разделы языковых версий */
        if (in_array($oSearchRow->object_id, \Yii::$app->sections->getValues(Page::LANG_ROOT)))
            return false;

        $oContent = Parameters::getByName( $oSearchRow->object_id, Parameters::settings, 'staticContent', true);
        $sText = $oContent ? $oContent->show_val : '';

        // проверим на лейдинг пейдж, а вдруг?
        if ( \Yii::$app->sections->landingPageTpl() ){
            $aTemplate = Parameters::getByName( $oSearchRow->object_id, Parameters::settings, Parameters::template, true);
            if ($aTemplate && $aTemplate['value'] == \Yii::$app->sections->landingPageTpl()){
                $sText = Tree::getSectionsTitle( $oSearchRow->object_id );
            }
        }

        // нет текста - в поиск не заносим
        if ( !$sText )
            return false;

        $sTitle = Tree::getSectionsTitle( $oSearchRow->object_id );

        $oSection = Tree::getSection( $oSearchRow->object_id );

        // проверка существования раздела и реального url у него
        if ( !$oSection || !$oSection->hasRealUrl() )
            return false;

        // проверка флага исключения в seo
        $oSEOData = SEO\Api::get( 'section', $oSearchRow->object_id );
        if ( $oSEOData && $oSEOData->none_index )
            return false;

        /** В случае если раздел виден и имеет ссылку, т.е. есть редирект куда-то он не должен попадать в поиск. */
        if ( $oSection->link )
            return false;

        $oSearchRow->language = Parameters::getLanguage($oSearchRow->object_id);
        
        $oSearchRow->href = \Yii::$app->router->rewriteURL('['.$oSearchRow->object_id.']');

        $oSearchRow->section_id = $oSearchRow->object_id;
        $oSearchRow->search_text = $this->stripTags($sText);
        $oSearchRow->search_title = $sTitle;
        $oSearchRow->status = 1;
        $oSearchRow->use_in_search = true;

        $oSearchRow->save();
        return true;
    }

    /**
     * @param $id
     * рекурсивный сброс индекса раздела и подчиненных ему
     */
    private function resetSectionRecursive($id){

        // сбросить статус для сущностей, привязанных к этому разделу
        SearchIndex::update()
            ->set('status', 0)
            ->where('section_id', $id)
            ->get()
        ;

        // найти подразделы
        $section = Tree::getSection( $id );
        $aSubSections = $section->getSubSections();

        // выполнить сброс для подразделов
        foreach ( $aSubSections as $item ) {
            $this->resetToId( $item->id );
            $this->resetSectionRecursive( $item->id );
        }

    }

    /**
     *  воссоздает полный список пустых записей для сущности, отдает количество добавленных
     * @return int
     */
    public function restore() {
        $sql = "INSERT INTO search_index(`status`,`class_name`,`object_id`)  SELECT '0','{$this->getName()}',id  FROM tree_section WHERE parent>3";
        Query::SQL($sql);

    }

    /**
     * Устанавливает флаг рекурсивного сброса поискового индекса по дереву разделов
     */
    public function setRecursiveResetFlag() {
        $this->bRecursiveReset = true;
    }

}