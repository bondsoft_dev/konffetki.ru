<?php

namespace skewer\build\Adm\Tree;

use skewer\build\Cms;
use skewer\build\Component\Section\Tree;



/**
 * Класс для обработки ветки библиотек
 * Class LibModule
 * @package skewer\build\Adm\Tree
 */
class LibModule extends Module {

    /** @var string заместитель основной JS библиотеки */
    protected $sMainJSClass = 'Tree4Lib';

    /** @var bool Флаг наличия нескольких деревьев */
    protected $bMultiTree = true;

    /**
     * Отдает id родительского раздела
     * @return int
     */
    protected function getStartSection() {
        return (int)\Yii::$app->sections->library();
    }

    /**
     * Устанавливаем список шаблонов для библиотек
     * @param int $iTemplateId
     * @param $iSectionId
     * @return array
     */
    protected function getTemplateList(&$iTemplateId, $iSectionId){
        $aResult = array();
        
        // добавляем фиктивный раздел Папка
         $aResult[] = array(
                 'id' => Tree::tplDirId,
                 'title' => '"'.\Yii::t('tree','folder').'"'
         );
        
        return $aResult;
    }

    /**
     * Возвращает заголовок дерева
     * @return bool|mixed|string
     */
    protected function getTreeTitle() {
        return \Yii::t('tree','lib_tree_title');
    }

//    // для именования разделов по именам основных модулей
//    //   использовалось при безнеймспейсовох модулях, сейчас не актуально
//    protected function markLeafs($aItems) {
//
//        foreach ($aItems as $iKey => $item) {
//            $alias = mb_strtolower($item['alias']);
//            if (strpos($alias, 'AdmModule')) {
//                /** @var Cms\Tabs\ModulePrototype $module */
//                $module = new $alias(new \skContext($alias,$alias,ctModule,array()));
//                if ($module) {
//                    $item['title'] = $module->getTitle();
//                }
//            }
//            $aItems[$iKey] = $item;
//        }
//        return parent::markLeafs($aItems);
//    }


}
