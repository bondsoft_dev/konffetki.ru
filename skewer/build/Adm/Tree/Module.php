<?php

namespace skewer\build\Adm\Tree;


use skewer\build\Cms;
use skewer\build\Component\Section;
use skewer\build\Component\SEO;
use skewer\models\TreeSection;
use yii\base\UserException;
use yii\helpers\ArrayHelper;


/**
 * Class Module
 * @package skewer\build\Adm\Tree
 */
class Module extends Cms\LeftPanel\ModulePrototype {

    /**
     * Отдает класс-родитель, насдедники которого могут быть добавлены в дерево процессов
     * в качестве вкладок
     * @return string
     */
    public function getAllowedChildClassForTab() {
        return 'skewer\build\Adm\Tree\ModuleInterface';
    }

    /**
     * Отдает инициализационный массив для набора вкладок
     * @param int|string $mRowId идентификатор записи
     * @return string[]
     */
    public function getTabsInitList( $mRowId ) {

        $section = Section\Tree::getSection( $mRowId );
        if ( !$section )
            return [];


        // выбор действия по типу раздела
        switch ( $section->type ) {

            // обычный раздел
            case Section\Tree::typeSection:

                // загрузить модули раздела
                return self::initSectionModules( $mRowId );

            // папка
            case Section\Tree::typeDirectory:

                // загрузить только файловый менеджер
                return [ 'files' => 'skewer\\build\\Adm\\Files\\Module' ];

            default:
                return [];

        }

    }

    /**
     * Задает дополнительные параметры для вкладок
     * @static
     * @param $mRowId
     * @return array
     */
    public function getTabsAddParams( $mRowId ) {

        // выходной массив
        $aOut = [];

        /** @var array[] $aParamsList */
        $aParamsList = Section\Parameters::getList( $mRowId )->fields(['value'])->groups()->asArray()->rec()->get();

        // если есть параметры
        if(count($aParamsList)) {

            // обойти все
            foreach($aParamsList as $aGroups) {
                $sModuleName = '';

                foreach($aGroups as $k=>$aParams) {

                    // если есть инициализация админского
                    if ($aParams['name'] == Section\Parameters::objectAdm) {

                        // имя модуля
                        $sModuleName = str_replace( '\\', '_', $aParams['value'] );
                        unset( $aParamsList[$k] );

                    }
                }

                if ($sModuleName){
                    $aParameters = ArrayHelper::map($aGroups, 'name', 'value');
                    $aParameters['pageId'] = (int)$mRowId;
                    $aOut['obj'.$sModuleName] = $aParameters;
                }

            }

        }

        return $aOut;

    }


    /**
     * Отдает набор объектов для раздела
     * @static
     * @param $mRowId
     * @return array
     */
    protected function initSectionModules( $mRowId ) {

        $aOut = array();

        /**
         * админские модули
         */

        if ( !Section\Parameters::getValByName($mRowId, Section\Parameters::settings, Section\Parameters::HideEditor, true))
            $aOut['editor'] = 'skewer\build\Adm\Editor\Module';

        if( \CurrentAdmin::isSystemMode() )
            $aOut['params'] = 'skewer\build\Adm\Params\Module';

        /**
         * обычные модули
         */

        // запрос упрощенного списка параметров
        $aParamsList = Section\Parameters::getList( $mRowId )->name(Section\Parameters::objectAdm)->fields(['value'])
            ->rec()->asArray()->get();

        // если есть параметры
        if(count($aParamsList)) {

            // обойти все
            foreach($aParamsList as $aParams) {

                // имя модуля
                $sModuleAlias = str_replace( '\\', '_', $aParams['value'] );
                $sModuleName = $aParams['value'];

                if ($sModuleName)
                    $aOut['obj'.$sModuleAlias] = \Module::getClassOrExcept($sModuleName, \Layer::ADM);

            }

        }

        return $aOut;

    }

    /**
     * Стартовый раздел
     * @var int
     */
    protected $iStartSection = null;

    /** @var bool Флаг наличия нескольких деревьев */
    protected $bMultiTree = false;

    /** @var bool Флаг не отображения дополнительных интерфейсов */
    protected $bDropView = false;

    /** @var string заместитель основной JS библиотеки */
    protected $sMainJSClass = '';

    public function init(){

        // вызвать инициализацию, прописанной в родительском классе
        parent::init();

        // если задан заместитель основной JS библиотеки
        if ( $this->sMainJSClass ) {

            // изменение стандартного имени модуля
            $this->setModuleName( $this->sMainJSClass );

            // подцепить основной модуль как подчиненный
            $this->addLibClass( 'Tree', 'Adm', 'Tree' );

        }

    }// func

    /**
     * Внутренняя функция. Отдает подразделы заданного раздела
     * @param $iId
     * @return array
     */
    protected function getSubSections( $iId ) {

        $iId = (int)$iId;

        $this->setModuleLangValues(
            array(
                'treeDelRowHeader'=>'treeDelRowHeader',
                'treeDelRow'=>'treeDelRow',
                'treeDelMsg'=>'treeDelMsg',
                'treePanelHeader'=>'treePanelHeader',
                'treeErrorNoParent'=>'treeErrorNoParent',
                'treeErrorOnDelete'=>'treeErrorOnDelete',
                'add'=>'add',
                'treeErrorParentNotSelected'=>'treeErrorParentNotSelected',
                'treeNewSection'=>'treeNewSection',
                'siteSettings'=>'siteSettings',
                'treeFormHeaderAdd'=>'treeFormHeaderAdd',
                'treeFormTitleTitle'=>'treeFormTitleTitle',
                'treeFormTitleAlias'=>'treeFormTitleAlias',
                'treeFormTitleParent'=>'treeFormTitleParent',
                'treeFormTitleTemplate'=>'treeFormTitleTemplate',
                'treeFormTitleLink'=>'treeFormTitleLink',
                'treeTitleVisible'=>'treeTitleVisible',
                'visibleHiddenFromMenu'=>'visibleHiddenFromMenu',
                'visibleVisible'=>'visibleVisible',
                'visibleHiddenFromPath'=>'visibleHiddenFromPath',
                'visibleHiddenFromIndex'=>'visibleHiddenFromIndex',
                'paramFormSaveUpd'=>'paramFormSaveUpd',
                'paramFormClose'=>'paramFormClose'
            )
        );

        // запрос подразделов
        $aAllowedSection = \CurrentAdmin::getReadableSections();
        $list = Section\Tree::getSubSections( $iId );
        $aItems = [];

        foreach ( $list as $section )
            if ( in_array( $section->id, $aAllowedSection ) )
                $aItems[$section->id] = $section->getAttributes();

        // обозначить разделы без наследников
        // раскрасить элементы
        $aItems = $this->markLeafs( $aItems );

        $this->setData('cmd','loadItems');

        return array_values($aItems);

    }

    /**
     * Проверка доступа к заданному разделу
     * @param $iSectionId
     * @throws \Exception
     * @return bool
     */
    protected function testAccess( $iSectionId ) {

        // сделать проверку прав доступа
        // + проверка доступа к 0 разделу на запись (для parent)
        //$bRes = (bool)$iSectionId;

        // если нет доступа
        if( !\CurrentAdmin::canRead( $iSectionId ) )
            throw new \Exception('authError');

    }

    /**
     * Проверка на неудаляемый системный раздел
     * @param $iSectionId
     * @throws \Exception
     * @return bool
     */
    protected function testBreakDelete( $iSectionId ) {

        /** @todo _break_delete - в константу куда-нибудь */
        if( $aParam = Section\Parameters::getByName($iSectionId, Section\Parameters::settings, '_break_delete', true) )
            if( $aParam['value'] )
                throw new \Exception('Ошибка! Нельзя удалить системный раздел.');

        return true;
    }

    /**
     * Отдает id родительского раздела
     * @return int
     */
    protected function getStartSection() {
        return (int)\Yii::$app->sections->root();
    }

    /**
     * @throws \Exception
     * Состояние. Выбор корневого набора разделов
     * @return bool
     */
    protected function actionInit() {

        // родительский раздел
        $iId = $this->getStartSection();

        // проверять доступность корневого раздела
        if( !\Auth::isReadable('admin',$iId) && !\CurrentAdmin::isSystemMode() )
            return psBreak;

        // загрузка элементов
        $this->setData('items', $this->getSubSections( $iId ));

        // установка корневого раздела
        $this->addInitParam( 'rootSection',$iId );
        $this->addInitParam( 'multiTree', $this->bMultiTree );

        // блокировка некоторых параметров
        $this->addInitParam( 'showButtons', !$this->bDropView );

        // название
        $this->addInitParam( 'title', $this->getTreeTitle() );

        return true;
    }

    /**
     * @throws \Exception
     * Состаяние. Выбор корневого набора разделов
     * @return bool
     */
    protected function actionReloadTree() {

        // родительский раздел
        $iId = $this->getStartSection();

        // проверять доступность корневого раздела
        if( !\Auth::isReadable('admin',$iId) && !\CurrentAdmin::isSystemMode() )
            return psBreak;

        // загрузка элементов
        $this->setData('items', $this->getSubSections( $iId ));

        $this->setData('dropAll', true);
        $this->setData('sectionId', $this->getInt('sectionId'));
        $this->setCmd('loadTree');

        return psComplete;

    }

    /**
     * Возвращает название дерева
     * @return bool|mixed|string
     */
    protected function getTreeTitle() {
        return \Yii::t('adm', 'section_'. $this->getStartSection());
    }

    /**
     * Состояние. Выбор подразделов заданного раздела
     * @return bool
     */
    protected function actionGetSubItems() {

        // заданный раздел
        $iId = $this->getInt('node');

        $this->setData('items', $this->getSubSections( $iId ));

        return true;

    }


    /**
     * Запрос параметров для построения формы добавления / редактирования
     * @throws UserException
     */
    protected function actionGetForm() {

        // добавить библиотеку отображения
        $this->addLibClass( 'TreeForm', \Layer::ADM, 'Tree' );

        // запрос пришедших переменных
        $iSectionId = $this->getInt('selectedId');
        $aRow = $this->get('item');
        $iParentId = (int)ArrayHelper::getValue( $aRow, 'parent', 0 );
        //is_array($aRow) and isset($aRow['parent']) and $aRow['parent']) ? (int)$aRow['parent'] : 0;

        // если нужно добавлять в родительский и
        if ( !$iSectionId and   // новый раздел и
            $iParentId and  // и задан родительский
            Section\Parameters::getValByName( $iParentId, Section\Parameters::settings, Section\Parameters::AddToParent, true ) )
        {
            $iParentId = Section\Tree::getSectionParent( $iParentId );
            $aRow['parent'] = $iParentId;
        }

        $iTemplateId = 0;
        
        // если дополнение и есть блокировка дополнительных интерфейсов
        if ( !$iSectionId and $this->bDropView )
            throw new UserException('adding is not allowed');

        // приведение к нужному типу
        if ( !is_array($aRow) ) $aRow = array();

        if ( $iSectionId ) {

            $section = Section\Tree::getSection( $iSectionId );
            if ( !$section )
                throw new UserException('notFound');

            // проверка прав доступа
            $this->testAccess( $iSectionId );

            // запросить основную строку
            $aRow = $section->getAttributes();

            // запросить шаблон для данного раздела
            $iTemplateId = Section\Parameters::getTpl( $iSectionId );

        }

        // устанавливаем дефолтное значение, если не задано
        if(isset($aRow['type']) and $aRow['type'] == Section\Tree::typeDirectory)
            $iTemplateId = Section\Tree::tplDirId;

        // набор шаблонов
        $aRow['template_list'] = $this->getTemplateList($iTemplateId, $iParentId);

        // сохранение ссылки на шаблон
        $aRow['template'] = $iTemplateId;

        $aOutParentSections = array();

        // собрать родительские разделы
        $bHideParents = (bool)Section\Parameters::getValByName($iParentId, Section\Parameters::settings, Section\Parameters::HideParents, true);
        if ( !$bHideParents ) {

            $policy = \CurrentAdmin::isSystemMode() ? false : Section\Tree::policyAdmin;

            /** @var [] $aDenySections массив из раздела и его подразделов*/
            $aDenySections = $iSectionId ? ArrayHelper::map( Section\Tree::getSectionList( $iSectionId, $policy ), 'id', 'id' ) : [];

            /** @var [] $aParentList массив всех разделов */
            $aParentList = Section\Tree::getSectionList( $this->getStartSection(), $policy );
            if ( $aParentList ) {
                foreach ( $aParentList as $aSection ) {
                    if (isset($aSection['id']) && !isset($aDenySections[$aSection['id']])){
                        $aOutParentSections[(int)$aSection['id']] = $aSection['title'];
                    }
                }
            }
            $aOutParentSections = \ExtForm::markUniqueValue($aOutParentSections);
        } else {
            $aOutParentSections = [
                $iParentId => Section\Tree::getSectionsTitle( $iParentId )
            ];
        }

        $aRow['parent_list'] = array();
        foreach ( $aOutParentSections as $iKey => $sTitle ) {
            $aRow['parent_list'][] = [
                'id' => $iKey,
                'title' => $sTitle
            ];
        }

        // отдать параметры
        $this->setCmd( 'createForm' );
        $this->setData( 'form', $aRow );

    }

    /**
     * Устанавливаем список шаблонов для каждого блока разделов
     * @param $iSectionId
     * @param int $iTemplateId
     * @return array
     */
    protected function getTemplateList(&$iTemplateId, $iSectionId){

        $bSubTpl = (bool)Section\Parameters::getValByName( $iSectionId, Section\Parameters::settings, Section\Parameters::SubSectonTpl, true );

        // если выводим подшаблоны шаблона для раздела в который добавляем
        if ( $bSubTpl )
            $iTplId = Section\Parameters::getTpl( $iSectionId );
        else
            $iTplId = \Yii::$app->sections->templates();

        return Section\Tree::getTplSection( $iTemplateId, $iTplId );

    }
    

    /**
     * Сохранение/добавление раздела
     * @throws UserException
     * @return bool
     */
    protected function actionSaveSection() {

        // массив на сохранение
        $aData = $this->get('item');

        // id раздела
        $iSectionId = $this->getInt('sectionId');

        /** @var bool $iIsNew флаг "Новая запись" */
        $iIsNew = !$iSectionId;

        // состояние системы
        $this->setCmd('saveItem');

        // id родительского раздела
        $iParentId = isset($aData['parent']) ? (int)$aData['parent'] : 0;
        $this->testAccess( $iParentId );


        if ( $iParentId && $iSectionId && $iSectionId == $iParentId )
            throw new UserException('You can not make yourself the parent partition or section!');

        // проверка прав доступа
        if ( $iSectionId ) {

            // проверить права доступа
            $this->testAccess( $iSectionId );

        } else {

            // если есть блокировка дополнительных интерфейсов
            if ( $this->bDropView )
                throw new UserException('adding is not allowed');

        }

        if ( !$aData ) {
            $this->setData( 'saveResult', false );
            return false;
        }

        // сохраняемы шаблон
        $iTemplateId = isset($aData['template']) ? (int)$aData['template'] : 0;
        $bIsDirectory = $iTemplateId === Section\Tree::tplDirId;

        // тип раздела
        $aData['type'] = ( $iIsNew and $bIsDirectory ) ? Section\Tree::typeDirectory : Section\Tree::typeSection;

        $section = $iSectionId ? Section\Tree::getSection( $iSectionId ) : (new TreeSection());
        $section->setAttributes( $aData );

        // если существующий раздел и родитель поменялся - перегрузить дерево
        if ( $iSectionId and ($section->isAttributeChanged('parent') ) )
            $this->fireJSEvent( 'reload_tree' );

        if ( !$section->save() )
            return false;

        $this->addNoticeReport(
            $iIsNew ? \Yii::t('tree', 'section_creating') : \Yii::t('tree', 'section_editing'),
            $section->getAttributes(),
            \skLogger::logUsers,
            'Tree'  // пришлось вписать вручную, т.к. пробрасывалось значение из класса наследника
        );


        // статус сохранения
        $this->setData( 'saveResult', $section->id );
        // выдать в ответ текущие данные в базе
        $this->setData( 'item', $section->getAttributes() );

        // для нового раздела выполнить дополнительные действия
        if ( $iIsNew and !$bIsDirectory )
            $section->setTemplate( $iTemplateId );

        SEO\Service::updateSiteMap();

        return true;

    }

    /**
     * Удаление раздела
     */
    protected function actionDeleteSection() {

        // id раздела
        $iSectionId = $this->getInt('sectionId');

        // проверка прав доступа
        $this->testAccess( $iSectionId );

        $this->testLangRoot($iSectionId);

        //проверка на системный раздел
        $this->testBreakDelete( $iSectionId );

        $oSection = Section\Tree::getSection( $iSectionId );

        // удаление раздела
        $bRes = Section\Tree::removeSection( $iSectionId );

        SEO\Api::del('section',$iSectionId);

        /* запускаем событие если удаление прошло нормально */
        //if($bRes) $this->fireEvent('removeSection', array('sectionId' => $iSectionId));

        //\skLogger::addNoticeReport("section_deleting",\skLogger::buildDescription(array('Section ID'=>$iSectionId)),\skLogger::logUsers,$this->getModuleName());
        $this->addNoticeReport(
            \Yii::t('tree', 'section_deleting'),
            $oSection ? $oSection->getAttributes() : ['Section ID'=>$iSectionId],
            \skLogger::logUsers,
            'Tree'  // пришлось вписать вручную, т.к. пробрасывалось значение из класса наследника
        );

        // возврат результата
        $this->setCmd('deleteSection');
        $this->setData( 'deletedId', $bRes ? $iSectionId : 0 );

    }

    /**
     * Поветочно открывает дерево до нужного раздлела
     * @param int $iFromSection - корневая вершина
     * @param int $iToSection - целевая вершина
     * @param array &$aParents - набор радителей
     * @param array $aTail - набор подчиненных разделов
     * @return array
     */
    protected function getSectionsTree( $iFromSection=0, $iToSection = 0, &$aParents, $aTail = array() ) {

        if ( !$iToSection ) $iToSection = $this->getInt('sectionId');

        // родительский раздел
        $iParentId = Section\Tree::getSectionParent( $iToSection );

        // если есть родитель и до конца не добрались
        if( $iParentId and $iFromSection != $iToSection ){

            // составление набора родительских элементов
            $aParents[] = $iParentId;

            // запросить набор элементов этого уровня
            $aAllowedSection = \CurrentAdmin::getReadableSections();
            $list = Section\Tree::getSubSections( $iParentId );
            $aItems = [];

            foreach ( $list as $section )
                if ( in_array( $section->id, $aAllowedSection ) )
                    $aItems[$section->id] = [
                        'id' => $section->id,
                        'title' => $section->title,
                        'parent' => $section->parent,
                        'visible' => $section->visible,
                        'type' => $section->type,
                        'link' => $section->link,
                    ];


            // обобначить разделы без наследников
            $aItems = $this->markLeafs( $aItems );

            // рекурсивно спуститься ниже
            $aItems = array_merge($this->getSectionsTree($iFromSection, $iParentId, $aParents, $aItems),$aTail);

            return $aItems;
        } // if parent
        else {
            return $aTail;
        } // else

    } // func

    /**
     * Возвращает в поток дерево разделов, открытое до определенной вершины
     */
    protected function actionGetTree() {

        // целевой раздел
        $iToSection = $this->getInt('sectionId');

        // запросить дерево
        $aParents = array();
        $aItems = $this->getSectionsTree( 0, $iToSection, $aParents );

        // отдать в вывод, если найдено
        if ( $aItems ) {
            $this->setCmd('loadTree');
            $this->setData('sectionId',$iToSection);
            $this->setData( 'items', $aItems );
            $this->setData( 'parents', array_reverse($aParents) );
        }

    }

    /**
     * Просто возвращает данные для выбора раздела
     */
    protected function actionSelectNode(){

        // целевой раздел
        $iToSection = $this->getInt('sectionId');

        // отдать в вывод, если найдено
        $this->setCmd('selectNode');
        $this->setData('sectionId',$iToSection);

    }

    /**
     * Событие изменения положения раздела
     * @throws UserException
     */
    protected function actionChangePosition() {

        // направление
        $sDirection = $this->getStr('direction');
        // id переносимого элемента
        $iItemId = $this->getInt('itemId');
        // id элемента относительного которого идет перемещение
        $iOverId = $this->getInt('overId');

        // проверка наличия параменных
        if ( !$iItemId or !$iOverId or !$sDirection )
            throw new UserException('badData');

        // запросить записи элементов
        $oSection = Section\Tree::getSection( $iItemId );
        $oOverSection = Section\Tree::getSection( $iOverId );

        // наличие разделов обязательно
        if ( !$oSection or !$oOverSection )
            throw new UserException('loadSectionError');

        // проверка прав доступа
        if ( !$oSection->testAdminAccess() || !$oOverSection->testAdminAccess() )
            throw new UserException('authError');

        $oSection->changePosition( $oOverSection, $sDirection );

    }

    /**
     * Раскрашивает список элементов
     * @param array $aItems
     * @return array
     * fixme перенес из TreePrototype::markLeafs может не работать
     */
    protected function markLeafs( $aItems ) {

        if ( !$aItems ) return [];

        // набор id разделов
        $aIdList = array_keys($aItems);

        // сборка запроса
        $aHasChild = [];
        foreach ( TreeSection::find()->where(['parent' => $aIdList])->each() as $section )
            $aHasChild[] = $section->parent;

        // добавить пустой контейнер тем, у кого наследников нет
        foreach ( $aIdList as $iId ) {
            if ( !in_array($iId,$aHasChild) )
                $aItems[$iId]['children'] = array();
        }

        return $aItems;
    }

    /**
     * Проверка, является ли раздел главным в какой-либо языковой ветке
     * @param $iSectionId
     * @throws UserException
     */
    protected function testLangRoot($iSectionId)
    {
        if (in_array($iSectionId, \Yii::$app->sections->getValues(Section\Page::LANG_ROOT))) {
            throw new UserException(\Yii::t('tree', 'error_lang_root_delete'));
        }
    }


}// class
