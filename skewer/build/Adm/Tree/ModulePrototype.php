<?php

namespace skewer\build\Adm\Tree;

use skewer\build\Adm;
use skewer\build\Cms;

/**
 * Родительский класс для модулей относящихся к дереву разделов
 */
class ModulePrototype extends Cms\Tabs\ModulePrototype implements Adm\Tree\ModuleInterface {

    /** @var int id раздела */
    protected $pageId = 0;

    function getPageId() {
        return $this->pageId;
    }

}
