<?php

namespace skewer\build\Adm\Tree;


/**
 * Дерево разеделов для панели выбора файлов
 * Class FileBrowserModule
 * @package skewer\build\Adm\Tree
 */
class FileBrowserModule extends Module {

    /** @var string заместитель основной JS библиотеки */
    protected $sMainJSClass = 'Tree2FileBrowser';

    /**
     * Возвращает заголовок дерева
     * @return bool|mixed|string
     */
    protected function getTreeTitle() {
        return \Yii::t('tree','file_tree_title');
    }

}
