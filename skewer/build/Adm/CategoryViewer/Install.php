<?php

namespace skewer\build\Adm\CategoryViewer;


class Install extends \skModuleInstall {

    public function init() {
        return true;
    }// func

    public function install() {

        /**
         * @todo А КАК ИЗ КОНФИГА ВЫТАЩИТЬ???
         */
        $sGroupParam = 'CategoryViewer';

        $this->addParameter( \Yii::$app->sections->tplNew(), 'objectAdm', 'CategoryViewer', '', $sGroupParam );
        return true;
    }// func

    public function uninstall() {

        $sGroupParam = 'CategoryViewer';

        $this->removeParameter( \Yii::$app->sections->tplNew(), 'objectAdm', $sGroupParam );
        return true;
    }// func

}//class