<?php

$aLanguage = array();

$aLanguage['ru']['CategoryViewer.Adm.tab_name'] = 'Список разделов';
$aLanguage['ru']['param_show'] = 'Выводить раздел в разводку';
$aLanguage['ru']['param_image'] = 'Изображение подраздела';
$aLanguage['ru']['param_parent'] = 'Выводить подразделы';
$aLanguage['ru']['param_from'] = 'Выводить из раздела';
$aLanguage['ru']['param_icon'] = 'Иконка раздела';

$aLanguage['en']['CategoryViewer.Adm.tab_name'] = 'list of sections';
$aLanguage['en']['param_show'] = 'show in the list of sections';
$aLanguage['en']['param_image'] = 'Subsection image';
$aLanguage['en']['param_parent'] = 'Show subsections';
$aLanguage['en']['param_from'] = 'Show from section';
$aLanguage['en']['param_icon'] = 'Icon section';

return $aLanguage;
