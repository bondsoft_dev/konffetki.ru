<?php

/* main */
$aConfig['name']     = 'CategoryViewer';
$aConfig['title']    = 'CategoryViewer';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Админ-интерфейс управления конструктором форм';
$aConfig['revision'] = '0002';
$aConfig['layer']     = Layer::ADM;
$aConfig['useNamespace'] = true;

$aConfig['param_group'] = 'CategoryViewer'; // Группа параметров

return $aConfig;
