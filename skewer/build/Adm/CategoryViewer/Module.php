<?php

namespace skewer\build\Adm\CategoryViewer;


use skewer\build\Adm;
use skewer\build\Component\Section\Parameters;
use skewer\build\Component\UI;
use yii\base\UserException;
use yii\helpers\ArrayHelper;


class Module extends Adm\Tree\ModulePrototype {

    public $category_parent = 0;
    public $category_show = 0;
    public $category_img = '';
    public $category_from = 0;
    public $category_icon = '';


    public function actionInit() {

        $this->category_icon = (string)Parameters::getValByName($this->pageId, Parameters::settings, 'category_icon');

        $this->actionShow();
    }


    protected function actionShow() {

        $aData = array(
            'category_parent' => $this->category_parent,
            'category_show' => $this->category_show,
            'category_img' => $this->category_img,
            'category_from' => $this->category_from,
            'category_icon' => $this->category_icon,
        );

        $oForm = UI\StateBuilder::newEdit();

        $oForm
            ->fieldCheck( 'category_parent', \Yii::t('categoryViewer', 'param_parent') )
            ->fieldCheck( 'category_show', \Yii::t('categoryViewer', 'param_show') )
            ->field( 'category_img', \Yii::t('categoryViewer', 'param_image'), 's', 'imagefile' )
            ->fieldInt( 'category_from', \Yii::t('categoryViewer', 'param_from') );

        if ( \SysVar::get('Menu.ShowIcons'))
            $oForm->addField( 'category_icon', \Yii::t('categoryViewer', 'param_icon'), 's', 'imagefile' );

        $oForm
            ->setValue( $aData )
            ->button( \Yii::t('adm','save'), 'save', 'icon-save' )
            ->button( \Yii::t('adm','back'), 'show', 'icon-cancel' )
        ;


        $this->setInterface( $oForm->getForm()  );
    }


    protected function actionSave() {

        $aData = $this->getInData();

        $sGroupName = $this->getConfigParam('param_group');

        // на случай если не найден параметр.
        if ( !$sGroupName )
            throw new UserException( 'Can not find group name' );

        $this->category_parent  = ArrayHelper::getValue($aData, 'category_parent', '');
        $this->category_show    = ArrayHelper::getValue($aData, 'category_show', '');
        $this->category_img     = ArrayHelper::getValue($aData, 'category_img',  '');
        $this->category_from    = ArrayHelper::getValue($aData, 'category_from', '');
        $this->category_icon    = ArrayHelper::getValue($aData, 'category_icon', '');

        Parameters::setParams( $this->pageId, $sGroupName, 'category_parent', $this->category_parent);
        Parameters::setParams( $this->pageId, $sGroupName, 'category_show', $this->category_show);
        Parameters::setParams( $this->pageId, $sGroupName, 'category_img', $this->category_img);
        Parameters::setParams( $this->pageId, $sGroupName, 'category_from', $this->category_from);

        if ( \SysVar::get('Menu.ShowIcons'))
            Parameters::setParams( $this->pageId, Parameters::settings, 'category_icon', $this->category_icon);

        $this->actionShow();
    }


} 