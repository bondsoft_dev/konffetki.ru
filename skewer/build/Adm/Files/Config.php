<?php

/* main */
$aConfig['name']     = 'Files';
$aConfig['title']    = 'Files';
$aConfig['version']  = '1.000a';
$aConfig['description']  = 'Files';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::ADM;
$aConfig['useNamespace'] = true;

return $aConfig;