<?php

namespace skewer\build\Adm\Files;


/**
 * Панель отображения набора файлов для панели выбора файлов
 * Class BrowserModule
 * @package skewer\build\Adm\Files
 */
class BrowserModule extends Module {

    // возможность выбирать файлы
    protected $bCanSelect = true;

    protected $sListBuilderClass = 'ExtListModule';

    public function init(){

        // вызвать инициализацию, прописанной в родительском классе
        parent::init();

        // установить имя для используемого модуля
        $this->addLibClass('FileBrowserFiles');

    }

}
