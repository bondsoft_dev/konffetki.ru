<?php

namespace skewer\build\Adm\Files;


use skewer\build\Component\UI;
use skewer\build\Adm;
use yii\base\UserException;


/**
 * Класс для работы с набором файлов раздела
 * Class Module
 * @package skewer\build\Adm\Files
 */
class Module extends Adm\Tree\ModulePrototype {

    // id текущего раздела
    protected $iSectionId = 0;

    // возможность выбирать файлы
    protected $bCanSelect = false;

    // набор сообщений
    protected $aSysMessages = array();

    /**
     * @var \ExtList
     */
    protected $sListBuilderClass = 'ExtList';

    /**
     * Метод, выполняемый перед action меодом
     * @throws UserException
     * @return bool
     */
    protected function preExecute() {

        // id текущего раздела
        $this->iSectionId = $this->getInt('sectionId');

        // проверить права доступа
        if ( $this->iSectionId and !\CurrentAdmin::canRead($this->iSectionId) )
            throw new UserException( 'accessDenied' );

        // составление набора допустимых расширений для изображений
        $this->aImgExt = \Yii::$app->params['upload']['allow']['images'];

        $this->aSysMessages = array();

    }

    /**
     * Отдает id директории для записи
     * @return int
     */
    protected function getSectionId() {
        return $this->iSectionId;
    }

    /**
     * Первичная загрузка
     */
    protected function actionInit() {

        $this->actionList();

    }

    /**
     * Загрузка списка
     */
    protected function actionList() {

        $this->setPanelName( \Yii::t('Files', 'filesList') );

        // запрос файлов раздела
        $aItems = Api::getFiles($this->getSectionId());

        if ( $this->hasImages($aItems) ) {
            $this->actionPreviewList( $aItems );
        } else {
            $this->actionSimpleList( $aItems );
        }

        // обработка сообщений
        if ( isset( $this->aSysMessages['loadResult'] ) ) {
            $iTotal = $this->aSysMessages['loadResult']['total'];
            $iLoaded = $this->aSysMessages['loadResult']['loaded'];
            $aErrors = $this->aSysMessages['loadResult']['errors'];

            if ( !$iTotal ) {
                $this->addError(\Yii::t('Files', 'loadingError'));
            } else {

                $time = 2000 + count($aErrors)*2000;

                if ( $iLoaded ){
                    $sMsg = \Yii::t('Files', 'loadingPro', [$iLoaded, $iTotal] );
                    if (count($aErrors)){
                        $sMsg .= "<br>" . implode('<br>', $aErrors);
                    }

                    $this->addMessage( $sMsg , '', $time);
                }
                else{
                    $sMsg = \Yii::t('Files', 'noLoaded' );
                    if (count($aErrors)){
                        $sMsg .= "<br>" . implode('<br>', $aErrors);
                    }
                    $this->addError($sMsg);
                }

            }

        }

    }

    /**
     * Отображение обычного списка фалов
     * @param array $aItems набор файлов
     */
    private function actionSimpleList( $aItems ) {

        // установить имя для используемого модуля
        $this->addLibClass('FileBrowserFiles');

        /**
         * объект для построения списка (может быть перекрыт)
         */
        $oList = new ExtListModule();
        $oList->allowSorting();

        /**
         * Модель данных
         */

        // фильтр по полям
        $aFieldFilter = Api::getListFields();

        // описания полей
        $aListModel = Api::getFullParamDefList( $aFieldFilter );

        // задать модель данных для вывода
        $oList->setFields( $aListModel );

        /**
         * Данные
         */

        // добавление набора данных
        $oList->setValues($aItems);

        /**
         * Интерфейс
         */

        // сортировка
        $oList->addSorter('ext');
        $oList->addSorter('name');

        // группировка
        $oList->setGroupField('ext');

        // добавление кнопок
        $this->addListButtons( $oList );

        // кнопка в строке удаления
        $oList->addRowBtnDelete();

        // вывод данных в интерфейс
        $this->setInterface( $oList );

    }

    /**
     * Добавление кнопок к интерфейсу "список"
     * @param \ExtPrototype $oList
     */
    private function addListButtons( $oList ) {

        // кнопка выбора файла, если доступна  \Yii::t('files', 'noLoaded')
        if ( $this->bCanSelect ) {
            $oList->addDockedItem(array(
                'text' => \Yii::t('Files', 'select'),
                'iconCls' => 'icon-commit',
                'state' => 'selectFile'
            ));
            $oList->addBtnSeparator();

        }

        // кнопка добавления
        $oList->addDockedItem(array(
            'text' => \Yii::t('Files', 'load'),
            'iconCls' => 'icon-add',
            'action' => 'addForm',
        ));

    }

    /**
     * Отображение списка фалов с миниатюрами
     * @param array $aItems набор файлов
     */
    private function actionPreviewList( $aItems ) {

        // объект для построения списка
        $oIface = new \ExtUserFile( 'FileBrowserImages' );

        // Добавление библиотек для работы
        $this->addLibClass( 'FileImageListView' );

        // добавление миниатюр в спиок файллов
        $aItems = $this->makePreviewListArray( $aItems );

        // сортировка файлов по имени
        usort( $aItems, array( $this, 'sortFIlesByName' ) );

        // добавление кнопок
        $this->addListButtons( $oIface );

        // добавление кнопки удаления
        $oIface->addBtnSeparator('->');
        $oIface->addDockedItem(array(
            'text' => \Yii::t('adm','del'),
            'iconCls' => 'icon-delete',
            'state' => 'delete',
            'action' => ''
        ));

        // добавляем css файл для
        $this->addCssFile('files.css');

        // задать команду для обработки
        $this->setCmd('load_list');

        // задать список файлов
        $this->setData('files',$aItems);

        $this->setModuleLangValues(
            array(
                'delRowNoName',
                'delCntItems',
                'delRowHeader',
                'delRow'
            )
        );

        // вывод данных в интерфейс
        $this->setInterface( $oIface );

    }

    /**
     * Подготавливает список файлов для в виде миниатюр
     * @param array $aItems входной список фалйов
     * @return array
     */
    private function makePreviewListArray($aItems){

        $aOut = array();

        // перебор файлов
        foreach ( $aItems as $aItem ) {

            // флаг наличия миниатюры
            $bThumb = false;

            // если изображение
            if ( $this->isImage($aItem) ) {

                // и есть миниатюра
                $sThumbName = Api::getThumbName( $aItem['webPathShort'] );
                if ( file_exists(WEBPATH.$sThumbName) ) {
                    $aItem['preview'] = $sThumbName;
                    $aItem['thumb'] = 1;
                    $bThumb = true;
                }
            }

            // если нет миниатюры
            if ( !$bThumb ) {
                $aItem['preview'] = $this->getModuleWebDir().'/img/file.png';
                $aItem['thumb'] = 0;
            }
            $aOut[] = $aItem;
        }

        return $aOut;

    }

    /**
     * сортировка файлов по имени
     * @param array $a1 первый элемент
     * @param array $a2 второй элемент
     * @return int
     */
    protected function sortFIlesByName( $a1, $a2 ) {

        // заначения
        $s1 = $a1['name'];
        $s2 = $a2['name'];

        // сравнение
        if ($s1 == $s2)
            return 0;
        return ($s1 < $s2) ? -1 : 1;

    }

    /**
     * Удаляет файл
     * @throws UserException
     */
    protected function actionDelete() {

        // запросить данные
        $sName = $this->getInDataVal( 'name' );

        // удаление одного файла
        if ( $sName ) {

            // проверка наличия данных
            if ( !$sName )
                throw new UserException('no file name provided');

            // удалить файл
            $bRes = Api::deleteFile( $this->getSectionId(), $sName );

            if ( $bRes ) {
                \skLogger::addNoticeReport(\Yii::t('files', 'deleting'), $sName, \skLogger::logUsers, $this->getModuleName());
                $this->addMessage( \Yii::t('Files', 'delete'));
            } else {
                $this->addError( \Yii::t('Files', 'noDelete') );
            }


        } else {

            $aData = $this->get( 'delItems' );

            // проверка наличия данных
            if ( !is_array($aData) or !$aData )
                throw new UserException('badData');

            // счетчики
            $iTotal = count($aData);
            $iCnt = 0;

            // удаление файлов
            foreach ( $aData as $sFileName ) {
                // удалить файл
                $iCnt += (int)(bool)Api::deleteFile( $this->getSectionId(), $sFileName );
            }

            // сообщения
            if ( $iCnt ) {
                $this->addMessage( \Yii::t('Files', 'deletingPro', [$iCnt, $iTotal]) );
                \skLogger::addNoticeReport(\Yii::t('files', 'deleteFiles'), array('section'=>$this->getSectionId(),'files'=>$aData), \skLogger::logUsers, $this->getModuleName());
            } else {
                $this->addError( \Yii::t('Files', 'noDeleteFiles') );
            }

        }

        // показать список
        $this->actionList();

    }

    /**
     * Отображает форму добавления
     */
    protected function actionAddForm() {

        // объект для построения списка
        $oIface = new \ExtUserFile( 'FileAddForm' );

        $this->setPanelName( \Yii::t('Files', 'loadFiles') );

        // добавить кнопки
        // загрузка
        $oIface->addDockedItem(array(
            'text' => \Yii::t('Files', 'load'),
            'iconCls' => 'icon-commit',
            'state' => 'upload'
        ));
//        $oIface->addBtnSeparator();
//        // еще один файл
//        $oIface->addDockedItem(array(
//            'text' => 'Добавить поле',
//            'iconCls' => 'icon-add',
//            'state' => 'addField'
//        ));
        $oIface->addBtnSeparator();
        // отмена
        $oIface->addBtnCancel();

        $this->setModuleLangValues(
            array(
                'fileBrowserFile',
                'fileBrowserSelect',
                'fileBrowserNoSelection'
            )
        );

        // вывод данных в интерфейс
        $this->setInterface( $oIface );

    }

    /**
     * Установка служебных данных
     * @param UI\State\BaseInterface $oIface
     */
    protected function setServiceData( UI\State\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData( array(
            'sectionId' => $this->getSectionId()
        ) );

    }

    protected function actionUpload() {

        // отдать правильный заголовок
        header('Content-Type: text/html');

        // загрузить файлы
        $aRes = Api::uploadFiles( $this->getSectionId() );

        \skLogger::addNoticeReport(\Yii::t('files', 'loadFile') ,\skLogger::buildDescription($aRes),\skLogger::logUsers,$this->getModuleName());

        $this->aSysMessages['loadResult'] = $aRes;

        $this->setData('loadedFiles',$aRes['files']);

        $this->setModuleLangValues(
            array(
                'fileBrowserNoSelection'
            )
        );

        // вызвать состояние "список"
        $this->actionList();

    }

    /** @var array список расширений, считающихся картинками */
    private $aImgExt = array();

    /**
     * Определяет, относится ли расширение к картинкам
     * @param string|array $mExt расширение файла/описание файла
     * @return bool
     */
    private function isImage( $mExt ){
        return in_array(is_array($mExt)?$mExt['ext']:$mExt, $this->aImgExt);
    }

    /**
     * Вычисляет, есть ли среди списка файлов картинки
     * @param $aItems
     * @return bool
     */
    private function hasImages( $aItems ) {

        // перебор записей
        foreach ( $aItems as $aItem ) {
            // есть найдена картинка
            if ( $this->isImage($aItem) )
                return true;
        }

        // нет картинок
        return false;

    }

}
