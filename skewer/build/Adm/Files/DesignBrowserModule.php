<?php

namespace skewer\build\Adm\Files;


/**
 * Панель отображения набора файлов для панели выбора файлов дизайнерского режима
 * Class DesignBrowserModule
 * @package skewer\build\Adm\Files
 */
class DesignBrowserModule extends BrowserModule {

    /**
     * Отдает id директории для записи
     * @return int
     */
    protected function getSectionId() {
        return \Design::imageDirName;
    }


}
