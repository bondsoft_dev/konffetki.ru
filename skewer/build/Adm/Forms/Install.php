<?php

namespace skewer\build\Adm\Forms;

/**
 * @class Install
 * @extends skModule
 * @project Skewer
 * @package adm
 *
 * @author Andy Mitrich, $Author$
 * @version $Revision$
 * @date 07.02.12 17:46 $
 *
 */

class Install extends \skModuleInstall {

    public function init() {
        return true;
    }// func

    public function install() {
        return true;
    }// func

    public function uninstall() {
        return true;
    }// func

}//class