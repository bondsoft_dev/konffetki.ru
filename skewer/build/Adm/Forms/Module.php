<?php

namespace skewer\build\Adm\Forms;


use skewer\build\Component\Section\Parameters;
use skewer\build\Component\UI;
use skewer\build\Adm;
use skewer\build\Component\Forms;


/**
 * Модуль добавления формы для раздела
 * @class Module
 * @extends skModule
 * @project Skewer
 * @package kernel
 *
 * @author kolesnikov, $Author: sapozhkov $
 * @version $Revision:  $
 * @date $Date: $
 * todo перевести на namespace и переименовать в линкер или чекер
 */
class Module extends Adm\Tree\ModulePrototype {

    public $iSectionId;
    public $iCurrentForm = 0;
    public $enableSettings = 0;

    protected function preExecute() {

        // id текущего раздела
        $this->iCurrentForm = $this->getInt('form_id');

        if ( $this->getInt('sectionId') )
            $this->iSectionId = $this->getInt('sectionId');

        if ( !$this->iSectionId )
            $this->iSectionId = $this->getEnvParam('sectionId');
    }

    protected function actionInit() {

        $this->setPanelName( \Yii::t('forms', 'select_form') );
        $this->actionPreList();
    }

    // форма задания формы для раздела
    protected function actionPreList() {

        $oFormRow = Forms\Table::get4Section( $this->iSectionId );
        $iForm = $oFormRow ? $oFormRow->getId() : 0;

        $aFormList = Forms\Table::find()
            //->where( 'form_handler_type<>?', 'toMethod' )
            ->asArray()->getAll();
        $aTemplateForms = array( 0 => ' -- ' . (\Yii::t('forms', 'form_not_selected')) . ' --' );
        foreach ( $aFormList as $aItem )
            $aTemplateForms[$aItem['form_id']] = $aItem['form_title'];


        // -- сборка интерфейса
        $oFormBuilder = new UI\StateBuilder();
        $oFormBuilder->clear()->createFormEdit();
        $oFormBuilder
            ->addSelectField( 'new_form', \Yii::t('forms', 'select_form'), 's', $aTemplateForms )
            ->setFields()
            ->setValue( array( 'new_form' => $iForm ) )
            ->addButton( \Yii::t('adm', 'save'), 'linkFormToSection', 'icon-save' )
            //->addButton( \Yii::t('forms', 'add_new_form'), 'addNewForm', 'icon-add' )
        ;

        // вывод данных в интерфейс
        $this->setInterface( $oFormBuilder->getForm()  );
    }

    protected function actionLinkFormToSection() {

        $aData = $this->get('data');

        $iNewForm = ( isset($aData['new_form']) && $aData['new_form'] )? $aData['new_form']: 0;
        Forms\Table::link2Section( $iNewForm, $this->iSectionId );

        $oFormRow = Forms\Table::find( $iNewForm );

        $sParamGroup = 'forms.content'; //возможно вынести в константы

        if ($oFormRow && $oFormRow->form_handler_type=='toBase'){

            Parameters::setParams( $this->iSectionId, $sParamGroup, 'objectAdm', 'Tool\FormOrders' );
            Parameters::setParams( $this->iSectionId, $sParamGroup, 'formId', $iNewForm );
        } else {
            Parameters::removeByName( $this->iSectionId, $sParamGroup, 'objectAdm' );
            Parameters::removeByName( $this->iSectionId, $sParamGroup, 'formId' );
        }

        $this->fireJSEvent( 'reload_section' );

        $this->addNoticeReport( \Yii::t('forms', 'editingFormInSection'), \Yii::t('forms', 'section_id') .": $this->iSectionId", \skLogger::logUsers, "Forms");
        
        $this->actionPreList();

    }


    /**
     * Установка служебных данных
     * @param UI\State\BaseInterface $oIface
     */
    protected function setServiceData( UI\State\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData( array(
            'section' => $this->iSectionId,
            'form_id' => $this->iCurrentForm,
            'enableSettings' => $this->enableSettings
        ) );

    }

}
