<?php

/* main */
$aConfig['name']     = 'Forms';
$aConfig['title']    = 'Конструктор форм (админ)';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Админ-интерфейс управления конструктором форм';
$aConfig['revision'] = '0002';
$aConfig['layer']     = Layer::ADM;
$aConfig['useNamespace'] = true;

return $aConfig;
