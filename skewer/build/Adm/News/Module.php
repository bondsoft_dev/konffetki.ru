<?php

namespace skewer\build\Adm\News;


use skewer\models\News;
use skewer\build\Component\UI;
use skewer\build\Component\SEO;
use skewer\build\Adm;
use Yii;
use yii\base\UserException;


/**
 * Class Module
 * @package skewer\build\Adm\News
 */
class Module extends Adm\Tree\ModulePrototype {

    // число элементов на страницу
    protected $iOnPage = 20;

    // текущий номер страницы ( с 0, а приходит с 1 )
    protected $iPageNum = 0;

    /**
     * Метод, выполняемый перед action меодом
     * @throws UserException
     * @return bool
     */
    protected function preExecute() {

        // номер страницы
        $this->iPageNum = $this->getInt('page');

        // проверить права доступа
        if ( !\CurrentAdmin::canRead($this->pageId) )
            throw new UserException( 'accessDenied' );

    }


    /**
     * Первичное состояние - спосок новостей для раздела
     */
    protected function actionInit() {
        // -- сборка интерфейса

        $news = News::find()
            ->where(['parent_section'=>$this->pageId])
            ->orderBy(['publication_date'=>SORT_DESC])
            ->limit($this->iOnPage)
            ->offset($this->iPageNum * $this->iOnPage)
            ->asArray()
            ->all();

        $iCount = News::find()
            ->where(['parent_section'=>$this->pageId])
            ->count();

        /**
         * @var News[] $news
         */

        $oFormBuilder = new UI\StateBuilder();
        // создаем форму

        $oFormBuilder = $oFormBuilder::newList();
        $oFormBuilder
            ->addField('id', 'ID', 'i', 'string',array( 'listColumns' => array('flex' => 1) ) )

            ->addField('title', \Yii::t('news', 'field_title'), 's', 'string',array( 'listColumns' => array('flex' => 3) ))
            ->addField('publication_date', \Yii::t('news', 'field_date'), 's', 'datetime',array( 'listColumns' => array('flex' => 2) ))
            ->addField('active', \Yii::t('news', 'field_active'), 'i', 'check')
            ->addField('on_main', \Yii::t('news', 'field_onmain'), 'i','check')

            ->addRowButtonUpdate()
            ->addRowButtonDelete()

            ->addButton(\Yii::t('adm','add'),'new','icon-add')
        ;

        //@TODO Активность приходит строкой и приведеним типов в JS превращается в true
        /*
        foreach( $news as &$oItem ){
            $oItem['active'] = (bool)$oItem['active'];
            $oItem['on_main'] = (bool)$oItem['on_main'];
        }
        */

        $oFormBuilder->setValue($news, $this->iOnPage, $this->iPageNum, $iCount);
        $oFormBuilder->setEditableFields(array('active','on_main'),'saveFromList');

        $this->setInterface($oFormBuilder->getForm());
    }

    /**
     * Сохраняет записи из спискового интерфейса
     */
    protected function actionSaveFromList() {

        $iId = $this->getInDataValInt( 'id' );

        $sFieldName = $this->get('field_name');

        $oRow = News::findOne(['id'=>$iId]);
        /** @var News $oRow */
        if ( !$oRow )
            throw new UserException( "Запись [$iId] не найдена" );

        $oRow->$sFieldName = $this->getInDataVal( $sFieldName );

        $oRow->save();

        $this->actionInit();
    }

    /**
     * Форма добавления
     */
    protected function actionNew() {
        $this->showForm( News::getNewRow() );
    }

    /**
     * Форма редактирования
     */
    protected function actionShow() {

        $aData = $this->get('data');
        $iItemId = isset($aData['id']) ? (int)$aData['id'] : 0;

        /**
         * @var News $oNewsRow
         */

        $oNewsRow = News::findOne(['id'=>$iItemId]);
        $this->showForm($oNewsRow);
    }

    /**
     * Отображение формы добавления/редактирования новости
     * @param News|null $oItem
     * @throws UserException
     */
    private function showForm( News $oItem ) {

        if ( !$oItem )
            throw new UserException('Item not found');

        // -- сборка интерфейса
        $oFormBuilder = new UI\StateBuilder();
        // создаем форму

        $oFormBuilder = $oFormBuilder::newEdit();
        $oFormBuilder->addField('id', 'ID', 'i', 'hide')
            ->addField('title', \Yii::t('news', 'field_title'), 's', 'string')
            ->addField('publication_date', \Yii::t('news', 'field_date'), 's', 'datetime')
            ->addField('active', \Yii::t('news', 'field_active'), 'i', 'check')
            ->addField('on_main', \Yii::t('news', 'field_onmain'), 'i', 'check')
            ->addField('announce', \Yii::t('news', 'field_preview'), 's', 'wyswyg')
            ->addField('full_text', \Yii::t('news', 'field_fulltext'), 's', 'wyswyg')
            ->addField('hyperlink', \Yii::t('news', 'field_hyperlink'), 's', 'string')
            ->addField('news_alias', \Yii::t('news', 'field_alias'), 's', 'string')
            //->addField('last_modified_date', \Yii::t('news', 'field_modifydate'), 's', 'datetime')
        ;

        $oFormBuilder->addButton(\Yii::t('adm','save'),'save','icon-save');
        $oFormBuilder->addButton(\Yii::t('adm','back'),'init','icon-cancel');

        if ( $oItem->id ) {
            $oFormBuilder->addBtnSeparator('->');
            $oFormBuilder->addButton(\Yii::t('adm','del'),'delete','icon-delete');
        }

        $oItem->announce = \ImageResize::restoreTags($oItem->announce);
        $oItem->full_text = \ImageResize::restoreTags($oItem->full_text);

        $oFormBuilder->setValue($oItem->getAttributes());

        // добавление SEO блока полей
        $oSearch = new Search();
        SEO\Api::appendExtForm( $oFormBuilder->getForm(), $oSearch->getName(), $oItem->id );

        $this->setInterface($oFormBuilder->getForm());
    }


    /**
     * Сохранение новости
     */
    protected function actionSave() {

        // запросить данные
        $aData = $this->get( 'data' );
        $iId = $this->getInDataValInt( 'id' );

        if ( !$aData )
            throw new UserException( 'Empty data' );

        if ( $iId ) {
            $oNewsRow = News::findOne(['id'=>$iId]);
            if ( !$oNewsRow )
                throw new UserException( "Запись [$iId] не найдена" );
        } else {
            $oNewsRow = News::getNewRow();

        }
        $oNewsRow->setAttributes($aData);
        $oNewsRow->parent_section = $this->pageId;

        $oNewsRow->save();

        $oSearch = new Search();

        // сохранение SEO данных
        SEO\Api::saveJSData( $oSearch->getName(), $oNewsRow->id, $aData );

        if ( $oNewsRow->id )
            $this->addModuleNoticeReport(\Yii::t('news', 'editNews'),$aData);
        else {
            $aData['id'] = $oNewsRow->id;
            $this->addModuleNoticeReport(\Yii::t('news', 'addNews'),$aData);
        }

        // вывод списка
        $this->actionInit();

    }


    /**
     * Удаляет запись
     */
    protected function actionDelete() {

        // запросить данные
        $aData = $this->get( 'data' );

        // id записи
        $iItemId = ( is_array($aData) and isset($aData['id']) ) ? (int)$aData['id'] : 0;

        News::deleteAll(['id'=>$iItemId] );

        // удаление SEO данных
        SEO\Api::del( 'news', $iItemId );

        $oSearch = new Search();
        $oSearch->deleteByObjectId($iItemId);

        $this->addModuleNoticeReport( \Yii::t('news', 'deleteNews'), $aData );

        // вывод списка
        $this->actionInit();

    }


    /**
     * Установка служебных данных
     * @param UI\State\BaseInterface $oIface
     */
    protected function setServiceData( UI\State\BaseInterface $oIface ) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData( array(
            'page' => $this->iPageNum
        ) );

    }

}
