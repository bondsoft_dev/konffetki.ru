<?php

use skewer\models\TreeSection;
use skewer\build\Component\Search;


$aConfig['name']     = 'News';
$aConfig['title']    = 'Новости (админ)';
$aConfig['version']  = '2.0';
$aConfig['description']  = 'Админ-интерфейс управления новостной системой';
$aConfig['revision'] = '0002';
$aConfig['layer']     = Layer::ADM;

$aConfig['events'][] = [
    'event' => TreeSection::EVENT_BEFORE_DELETE,
    'eventClass' => TreeSection::className(),
    'class' => skewer\models\News::className(),
    'method' => 'removeSection',
];

$aConfig['events'][] = [
    'event' => Search\Api::EVENT_GET_ENGINE,
    'class' => skewer\models\News::className(),
    'method' => 'getSearchEngine',
];

return $aConfig;
