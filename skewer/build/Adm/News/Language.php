<?php

$aLanguage = array();

$aLanguage['ru']['News.Adm.tab_name'] = 'Новости';
$aLanguage['ru']['field_id'] = 'ID';
$aLanguage['ru']['field_alias'] = 'Псевдоним';
$aLanguage['ru']['field_parent'] = 'Родительский раздел';
$aLanguage['ru']['field_title'] = 'Название';
$aLanguage['ru']['field_date'] = 'Дата публикации';
$aLanguage['ru']['field_time'] = 'Время публикации';
$aLanguage['ru']['field_preview'] = 'Анонс';
$aLanguage['ru']['field_fulltext'] = 'Полный текст';
$aLanguage['ru']['field_active'] = 'Активность';
$aLanguage['ru']['field_archive'] = 'Архив';
$aLanguage['ru']['field_onmain'] = 'На главной';
$aLanguage['ru']['field_hyperlink'] = 'Ссылка';
$aLanguage['ru']['field_modifydate'] = 'Дата последнего изменения';
$aLanguage['ru']['title_on_main'] = 'Название для главной';
$aLanguage['ru']['on_page'] = 'Записей в колонке';
$aLanguage['ru']['on_page_main'] = 'Записей на главной';
$aLanguage['ru']['on_main_show_type'] = 'Тип вывода новостей';
$aLanguage['ru']['show_type_list'] = 'список';
$aLanguage['ru']['show_type_columns'] = 'колонки ';
$aLanguage['ru']['editNews'] = 'Редактирование новости';
$aLanguage['ru']['addNews'] = 'Добавление новости';
$aLanguage['ru']['deleteNews'] = 'Удаление новости';
$aLanguage['ru']['allowNewsListRead'] = 'Разрешить чтение списка новостей';
$aLanguage['ru']['allowNewsDetailRead'] = 'Разрешить чтение детальной новости';
$aLanguage['ru']['new_news'] = 'Новость';

$aLanguage['en']['News.Adm.tab_name'] = 'News';
$aLanguage['en']['field_id'] = 'ID';
$aLanguage['en']['field_alias'] = 'Alias';
$aLanguage['en']['field_parent'] = 'Parent section';
$aLanguage['en']['field_title'] = 'Title';
$aLanguage['en']['field_date'] = 'Publish date';
$aLanguage['en']['field_time'] = 'Publish time';
$aLanguage['en']['field_preview'] = 'Preview';
$aLanguage['en']['field_fulltext'] = 'Full text';
$aLanguage['en']['field_active'] = 'Active';
$aLanguage['en']['field_archive'] = 'Archive';
$aLanguage['en']['field_onmain'] = 'On main';
$aLanguage['en']['field_hyperlink'] = 'Link';
$aLanguage['en']['field_modifydate'] = 'Last modify date';
$aLanguage['en']['title_on_main'] = 'Title on main';
$aLanguage['en']['on_page'] = 'News on page';
$aLanguage['en']['on_page_main'] = 'News on main';
$aLanguage['en']['on_main_show_type'] = 'Showing type';
$aLanguage['en']['show_type_list'] = 'list';
$aLanguage['en']['show_type_columns'] = 'columns ';
$aLanguage['en']['editNews'] = 'Edit news';
$aLanguage['en']['addNews'] = 'Add news';
$aLanguage['en']['deleteNews'] = 'Delete news';
$aLanguage['en']['allowNewsListRead'] = 'Allow reading the list of news';
$aLanguage['en']['allowNewsDetailRead'] = 'Allow reading the detailed description of news';
$aLanguage['en']['new_news'] = 'News';

return $aLanguage;