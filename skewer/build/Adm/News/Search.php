<?php


namespace skewer\build\Adm\News;


use skewer\build\Component\orm\Query;
use skewer\build\Component\Search\Prototype;
use skewer\build\Component\Search\Row;
use skewer\build\Component\Section\Parameters;
use skewer\build\Component\Section\Tree;
use skewer\models;

class Search extends Prototype{
    /**
     * отдает имя идентификатора ресурса для работы с поисковым индексом
     * @return string
     */
    public function getName() {
       return 'News';
    }

    /**
     * @inheritdoc
     */
    protected function update(Row $oSearchRow) {

        $oSearchRow->class_name = $this->getName();

        if (!$oSearchRow->object_id)
            return false;

        /** @var models\News $news */
        $news = models\News::findOne(['id' => $oSearchRow->object_id]);
        if (!$news)
            return false;

        // нет данных - не добавлять в индекс
        if (!$news->full_text)
            return false;

        if (!$news->active)
            return false;

        $oSearchRow->search_text = $this->stripTags($news->full_text);
        $oSearchRow->search_title = $this->stripTags($news->title);
        $oSearchRow->status = 1;
        $oSearchRow->use_in_search = true;
        $oSearchRow->section_id = $news->parent_section;
        $oSearchRow->language = Parameters::getLanguage($news->parent_section);

        // проверка существования раздела и реального url у него
        $oSection = Tree::getSection( $oSearchRow->section_id );
        if ( !$oSection || !$oSection->hasRealUrl() )
            return false;

        $oSearchRow->href  =
            ( !empty($news->news_alias) ?
                \Yii::$app->router->rewriteURL( sprintf(
                    '[%s][News?news_alias=%s]', // todo проверить урл(имя модуля) после переезда на неймспейс
                    $news->parent_section,
                    $news->news_alias
                )):
                \Yii::$app->router->rewriteURL( sprintf(
                    '[%s][News?id=%s]',
                    $news->parent_section,
                    $news->id
                ))
            );

        $oSearchRow->save();

        return true;
    }

    /**
     *  воссоздает полный список пустых записей для сущности, отдает количество добавленных
     * @return int
     */
    public function restore() {
        $sql = "INSERT INTO search_index(`status`,`class_name`,`object_id`)  SELECT '0','{$this->getName()}',id  FROM news";
        Query::SQL($sql);

    }


} 