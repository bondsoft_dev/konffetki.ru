<?php

/* main */
$aConfig['name']     = 'Params';
$aConfig['title']    = 'Параметры';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Параметры';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::ADM;

$aConfig['events'][] = [
    'event' => \skewer\models\TreeSection::EVENT_AFTER_DELETE,
    'eventClass' => \skewer\models\TreeSection::className(),
    'class' => \skewer\models\Parameters::className(),
    'method' => 'removeSection',
];

return $aConfig;