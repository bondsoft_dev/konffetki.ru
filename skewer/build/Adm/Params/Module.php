<?php

namespace skewer\build\Adm\Params;

use skewer\build\Component\Section\Parameters;
use skewer\build\Component\Section\Params\Type;
use skewer\build\Component\UI;
use skewer\build\Adm;
use skewer\build\Design\Zones;
use yii\base\UserException;

/**
 * Class Module
 * @package skewer\build\Adm\Params
 */
class Module extends Adm\Tree\ModulePrototype {

    // id текущего раздела
    protected $iSectionId = 0;

    protected $sFilter = '';

    /**
     * Метод, выполняемый перед action меодом
     * @throws UserException
     * @return bool
     */
    protected function preExecute() {

        // id текущего раздела
        $this->iSectionId = $this->getInt('sectionId');
        $this->sFilter = $this->getStr('filter', '');

        // проверить права доступа
        if (!\CurrentAdmin::canRead($this->iSectionId))
            throw new UserException('accessDenied');

    }

    /**
     * Установка служебных данных
     * @param UI\State\BaseInterface $oIface
     */
    protected function setServiceData(UI\State\BaseInterface $oIface) {

        // установить данные для передачи интерфейсу
        $oIface->setServiceData(array(
            'sectionId' => $this->iSectionId,
            'filter' => $this->sFilter
        ));

    }

    /**
     * Инициализация
     */
    public function actionInit() {

        // заголовок
        $this->setPanelName(\Yii::t('params', 'paramList'));

        $oList = UI\StateBuilder::newList();

        $oList
            ->fieldString( 'name', \Yii::t('params', 'name'), ['listColumns' => ['flex' => 5], 'sorted' => true] )
            ->fieldString( 'title', \Yii::t('params', 'title'), ['listColumns' => ['flex' => 5]] )
            ->fieldString( 'value', \Yii::t('params', 'value'), ['listColumns' => ['flex' => 5]] )
            ->fieldString( 'id', 'ID', ['listColumns' => ['flex' => 1]] )
            ->fieldString( 'parent', \Yii::t('params', 'parent'), ['listColumns' => ['flex' => 1]] )
        ;

        $oList->addFilterText('filter', $this->sFilter, \Yii::t('params', 'filter'));

        $aItems = Parameters::getList( $this->iSectionId  )
            ->fields(['id', 'name', 'value', 'parent', 'title'])
            ->rec()
            ->asArray()
            ->get();

        if ($this->sFilter != '')
            $aItems = Api::filterParams($aItems, $this->sFilter);

        $oList->setValue($aItems);

        // Редактируемое поле
        $oList->setEditableFields(['value'], 'save');

        // Группировка
        $oList->setGroups('group');

        // Сортировка
        $oList->addSorter('name');

        // Кнопки
        $oList
            ->addRowCustomBtn('ParamsAddObjBtn')
            ->addRowCustomBtn('ParamsEditBtn')
            ->addRowCustomBtn('ParamsDelBtn')
            ->button( \Yii::t('adm','add'), 'show', 'icon-add' )
            ->button( \Yii::t('adm','del'), 'delete', 'icon-delete' )
        ;

        $oList = $oList->getForm();
        $oList->setModuleLangValues(
            array(
                'del',
                'upd',
                'paramDelRowHeader',
                'paramDelRow',
                'paramAddForSection',
                'paramCopyToSection'
            )
        );

        // вывод данных в интерфейс
        $this->setInterface($oList);

    }


    /**
     * Добавить параметр по шаблону
     * @throws \Exception
     */
    protected function actionAddParam(){

        $this->setPanelName(\Yii::t('params', 'addParam'));

        $iItemId = $this->getInDataValInt('id');

        $oTemplateParameters = Parameters::getById( $iItemId );

        if (!$oTemplateParameters) {
            throw new \Exception(\Yii::t('params', 'noFindParam'));
        }

        $aParamList = $this->getParams4Module($this->iSectionId, $oTemplateParameters->group);

        $oParameters = Parameters::createParam();

        $oParameters->group = $oTemplateParameters->group;
        $oParameters->parent = $this->iSectionId;
        $oParameters->access_level = '0';

        // -- сборка интерфейса
        $oFormBuilder = UI\StateBuilder::newEdit();

        $aData = $oParameters->getAttributes();
        $aData['group_show'] = $oParameters->group;
        $aData['class'] = $this->getInnerData('class');

        // добавляем поля
        $oFormBuilder
            ->fieldHide('id', 'id')
            ->fieldHide('parent', \Yii::t('params', 'parent'))
            ->fieldHide('group', \Yii::t('params', 'group'))
            ->fieldString('group_show', \Yii::t('params', 'group'), ['disabled' => true])
            ->fieldString('class', \Yii::t('params', 'class'), ['disabled' => true])
            ->fieldSelect('name', \Yii::t('params', 'name'), $aParamList, [
                'forceSelection' => false,
                'allowBlank' => false,
                'editable' => true,
                'onUpdateAction' => 'getValueParams'
            ])

            ->fieldString('value', \Yii::t('params', 'value'))
            ->fieldString('title', \Yii::t('params', 'title'))
            ->fieldSelect('access_level', \Yii::t('params', 'access_level'), Type::getParametersList())
            ->field('show_val', \Yii::t('params', 'show_val'), 's', 'text')
        ;
        // устанавливаем значения
        $oFormBuilder->setValue($aData);

        // добавляем элементы управления
        $oFormBuilder->button(\Yii::t('adm','save'), 'save', 'icon-save', 'save');
        $oFormBuilder->button(\Yii::t('adm','back'), 'init', 'icon-cancel', 'init');

        $this->setInterface($oFormBuilder->getForm());

    }


    /**
     * Загружает форму для редактирования
     */
    protected function actionShow() {
        // номер записи
        $iItemId = $this->getInDataValInt('id');

        // запись параметра
        if ($iItemId) {
            $oParameters = Parameters::getById( $iItemId );

            if (!$oParameters) {
                throw new \Exception(\Yii::t('params', 'noFindParam'));
            }

            // если нельзя читать раздел
            if (!\CurrentAdmin::canRead($oParameters->parent))
                throw new \Exception(\Yii::t('params', 'authError'));

            // если параметр не принадлежит данному разделу - отвязать от id
            if ((int)$oParameters->parent !== (int)$this->iSectionId) {
                $oParameters->id = 0;
                $this->setPanelName(\Yii::t('params', 'cloneParam'));
            } else {
                $this->setPanelName(\Yii::t('params', 'editParam'));
            }

        } else {

            $this->setPanelName(\Yii::t('params', 'addParam'));

            $oParameters = Parameters::createParam([
                'group' => '.',
                'parent' => $this->iSectionId,
                'access_level' => 0
            ]);

            $oParameters->id = 0;
        }

        // -- сборка интерфейса
        $oFormBuilder = UI\StateBuilder::newEdit();

        $aData = $oParameters->getAttributes();
        $aData['class'] = $this->getInnerData('class');

        // добавляем поля
        $oFormBuilder
            ->fieldHide('id', 'id')
            ->fieldHide('parent' ,\Yii::t('params', 'parent'))
            ->fieldSelect('group', \Yii::t('params', 'group'), Api::getAllGroups($this->iSectionId), [
                'forceSelection' => false,
                'allowBlank' => false,
                'editable' => true,
                'onUpdateAction' => 'getModuleParams'
            ])
            ->fieldString('class', \Yii::t('params', 'class'), ['disabled' => true])
            ->fieldSelect('name', \Yii::t('params', 'name'), $this->getParams4Module( $oParameters->parent, $oParameters->group ), [
                'forceSelection' => false,
                'allowBlank' => false,
                'editable' => true,
                'onUpdateAction' => 'getValueParams'
            ]);

            if ($this->getInDataVal('type')=='obj'){
                $oParameters->name = 'object';
                $oParameters->value = '';
            } else {
                $oFormBuilder->field('value', \Yii::t('params', 'value'), 'string');
            }

            $oFormBuilder
                ->fieldString('title', \Yii::t('params', 'title'), 'string')
                ->fieldSelect('access_level', \Yii::t('params', 'access_level'), Type::getParametersList())
                ->field('show_val', \Yii::t('params', 'show_val'), 's', 'text')
            ;

        // устанавливаем значения
        $oFormBuilder->setValue($aData);

        // добавляем элементы управления
        $oFormBuilder->button(\Yii::t('adm','save'), 'save', 'icon-save', 'save');
        $oFormBuilder->button(\Yii::t('adm','back'), 'init', 'icon-cancel', 'init');

        $this->setInterface($oFormBuilder->getForm());
    }

    /**
     * Сохранение параметров
     * @throws \Exception
     * @return bool
     */
    public function actionSave() {

        // массив на сохранение
        $aData = $this->get('data');
        if (!$aData)
            throw new \Exception(\Yii::t('params', 'noSaveData'));

        // id элемента
        $iId = $this->getInDataValInt('id');

        $aRowInBase = false;

        // если задан id
        if ($iId) {

            // проверить совпадение раздела
            $oParentParam = Parameters::getById($iId);

            // перекрываем все данные пришедшими
            if ($oParentParam && (int)$oParentParam->parent == $this->iSectionId) {

                $aRowInBase = $oParentParam;

            }

        }

        if (!$aRowInBase)
            $aRowInBase = Parameters::getByName( $this->iSectionId,
                isset($aData['group'])?$aData['group']:'',
                isset($aData['name'])?$aData['name']:'');
        if (!$aRowInBase)
            $aRowInBase = Parameters::createParam();

        unset($aData['id']);
        $aRowInBase->setAttributes($aData);
        $aRowInBase->parent = $this->iSectionId;

        // сохранить параметр
        $iRes = $aRowInBase->save();

        if ($iRes) {
            $this->addMessage(\Yii::t('params', 'saveParam'));
        } else {
            $this->addError(\Yii::t('params', 'saveParamError'));
        }

        // отдать результат сохранения
        $this->setData('saveResult', $iRes);

        // отдать назад список параметров
        $this->actionInit();

    }

    /**
     * Удаление набора параметров
     * @throws \Exception
     * @return bool
     */
    public function actionDelete() {

        // список/id параметра
        $iId = $this->getInDataValInt('id');

        if (!$iId)
            throw new \Exception(\Yii::t('params', 'noParamDelete'));

        // составление набора id для удаления
        $aItem = Parameters::getById($iId);
        if (!$aItem)
            throw new \Exception(\Yii::t('params', 'noFindParam'));

        if ((int)$aItem->parent !== $this->iSectionId)
            throw new \Exception(\Yii::t('params', 'notDeleteCloneParam'));
            
        if ($aItem->group == Zones\Api::layoutGroupName && $aItem->parent == \Yii::$app->sections->root())
            throw new \Exception(\Yii::t('params', 'notDeleteParam'));
    
        // выполнение запроса на удаление
        $iRes = $aItem->delete();

        if ($iRes) {
            $this->addMessage(\Yii::t('params', 'deleteParam'));
        } else {
            $this->addError(\Yii::t('params', 'deleteParamError'));
        }

        // отдать назад список параметров
        $this->actionInit();

    }

    /**
     * Создать копию параметра для заданного раздел
     * @throws \Exception
     * @return array
     */
    public function actionClone() {

        // id параметра
        $iId = $this->getInDataValInt('id');

        // запросить дублируемое поле
        $aSrcRow = Parameters::getById($iId);

        if (!$aSrcRow)
            throw new \Exception(\Yii::t('params', 'noFindString'));

        $oNewParam = Parameters::copyToSection($aSrcRow, $this->iSectionId);

        // отдать результат только в случае ошибки
        if (!$oNewParam)
            throw new \Exception(\Yii::t('params', 'cloneError'));

        // отдать назад список параметров
        $this->actionInit();

    }


    /**
     * Подстановка в форму возможных параметров для выбранной метки
     */
    public function actionGetModuleParams(){

        $aFormData = $this->get('formData', []);

        $iParent = isset($aFormData['parent'])?$aFormData['parent']:0;
        $sGroupName = isset($aFormData['group'])?$aFormData['group']:0;

        $aParams = $this->getParams4Module( $iParent, $sGroupName );
        $aParams = array_combine($aParams, $aParams);

        $oListVals = new \ExtFormRows();
        $oListVals->addFieldInput( 'class', $this->getInnerData('class'), true );
        $oListVals->addFieldSelect( 'name', $aParams, [
                'forceSelection' => false,
                'allowBlank' => false,
                'editable' => true,
                'onUpdateAction' => 'getValueParams'
            ]);

        $oListVals->setData( $this );
    }


    /**
     * Подстановка в форму значения
     */
    public function actionGetValueParams(){

        $aFormData = $this->get('formData', []);

        $iParent = isset($aFormData['parent'])?$aFormData['parent']:0;
        $sGroupName = isset($aFormData['group'])?$aFormData['group']:'';
        $sName = isset($aFormData['name'])?$aFormData['name']:'';

        if ($iParent && $sGroupName && $sName){
            $aParam = Parameters::getByName( $iParent, $sGroupName, $sName, true );

            if ($aParam){
                $oListVals = new \ExtFormRows();
                $oListVals->addFieldInput( 'value', $aParam->value );
                $oListVals->addFieldInput( 'access_level', $aParam->access_level );
                $oListVals->addFieldInput( 'show_val', $aParam->show_val );

                $oListVals->setData( $this );
            }

        }

    }


    /**
     * Список параметров для модуля по разделу и метке
     * @param $iParent
     * @param $sGroupName
     * @return array
     * @throws \Exception
     */
    protected function getParams4Module( $iParent, $sGroupName ){

        // если нельзя читать раздел
        if (!\CurrentAdmin::canRead($iParent))
            throw new \Exception(\Yii::t('params', 'authError'));

        $oParamObject = Parameters::getByName( $iParent, $sGroupName, Parameters::object, true );

        if (!$oParamObject)
            return [];

        $sObjectName = $oParamObject->value;

        $sLayer = \Layer::PAGE;
        switch($oParamObject->name){
            case Parameters::object:
                $sLayer = \Layer::PAGE;
                break;
            case Parameters::objectAdm:
                $sLayer = \Layer::ADM;
                break;
        }

        $oMainModule = Parameters::getByName( $this->iSectionId, Parameters::settings, Parameters::object, true );
        if (strpos($oMainModule->value, \Layer::LANDING_PAGE) === 0)
            $sLayer = \Layer::LANDING_PAGE;
        if (strpos($oMainModule->value, \Layer::LANDING_ADM) === 0)
            $sLayer = \Layer::LANDING_ADM;

        $sObjectFullName = '\skewer\build\\' . $sLayer . '\\' . substr($sObjectName, strrpos($sObjectName, '/')) . '\\Module';

        $this->setInnerData('class', $sLayer . '\\' . substr($sObjectName, strrpos($sObjectName, '/')) );

        return Parameters::getParamTplList($sObjectFullName);
    }

}
