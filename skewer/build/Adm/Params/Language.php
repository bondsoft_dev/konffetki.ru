<?php

$aLanguage = array();

$aLanguage['ru']['Params.Adm.tab_name'] = 'Параметры';

$aLanguage['ru']['paramList'] = 'Список параметров';
$aLanguage['ru']['noFindParam'] = 'Параметр не найден';
$aLanguage['ru']['authError'] = 'Доступ к параметру запрещен';
$aLanguage['ru']['cloneParam'] = 'Клонирование родительского параметра';
$aLanguage['ru']['editParam'] = 'Редактирование параметра';
$aLanguage['ru']['addParam'] = 'Добавление параметра';
$aLanguage['ru']['noSaveData'] = 'Нет данных для сохранения';
$aLanguage['ru']['saveParam'] = 'Параметр сохранен';
$aLanguage['ru']['saveParamError'] = 'Возникла ошибка при сохранении';
$aLanguage['ru']['noParamDelete'] = 'Параметр для удаления не выбран';
$aLanguage['ru']['notDeleteCloneParam'] = 'Нельзя удалять параметры из родительского раздела';
$aLanguage['ru']['deleteParam'] = 'Параметр удален';
$aLanguage['ru']['deleteParamError'] = 'При удалении возникла ошибка';
$aLanguage['ru']['noFindString'] = 'Исходная строка не найдена';
$aLanguage['ru']['cloneError'] = 'Возникла ошибка при клонировании';
$aLanguage['ru']['filter'] = 'Фильтр';
$aLanguage['ru']['payment_field_title'] = 'Название';


$aLanguage['ru']['type_system'] = 'Системный';
$aLanguage['ru']['type_string'] = 'Строка';
$aLanguage['ru']['type_text'] = 'Текст';
$aLanguage['ru']['type_wyswyg'] = 'HTML редактор';
$aLanguage['ru']['type_check'] = 'Галочка';
$aLanguage['ru']['type_imagefile'] = 'Изображение';
$aLanguage['ru']['type_file'] = 'Файл';
$aLanguage['ru']['type_html'] = 'Упрощенный HTML редактор';
$aLanguage['ru']['type_select'] = 'Выпадающий список';
$aLanguage['ru']['type_int'] = 'Число';
$aLanguage['ru']['type_float'] = 'Дробное число';
$aLanguage['ru']['type_date'] = 'Дата';
$aLanguage['ru']['type_time'] = 'Время';
$aLanguage['ru']['type_datetime'] = 'ДатаВремя';
$aLanguage['ru']['type_inherit'] = 'Наследуемое';
$aLanguage['ru']['type_service_section'] = 'Системный раздел';

/**
 * @todo это параметр, значение которого для вывода в Page слое
 * будет тянуться из родительского раздела для языка.
 * Не могу придумать название, которое будет отражать это
 */
$aLanguage['ru']['type_language'] = 'Языковой параметр';

$aLanguage['ru']['type_text_html'] = 'HTML-code редактор';
$aLanguage['ru']['type_text_js'] = 'JS редактор';
$aLanguage['ru']['type_text_css'] = 'CSS редактор';

$aLanguage['ru']['type_string_local'] = 'Строка (локальное поле)';
$aLanguage['ru']['type_text_local'] = 'Текст (локальное поле)';
$aLanguage['ru']['type_wyswyg_local'] = 'HTML редактор (локальное поле)';
$aLanguage['ru']['type_check_local'] = 'Галочка (локальное поле)';
$aLanguage['ru']['type_file_local'] = 'Файл (локальное поле)';
$aLanguage['ru']['type_imagefile_local'] = 'Изображение (локальное поле)';
$aLanguage['ru']['type_html_local'] = 'Упрощенный HTML редактор (локальное поле)';
$aLanguage['ru']['type_select_local'] = 'Выпадающий список (локальное поле)';
$aLanguage['ru']['type_int_local'] = 'Число (локальное поле)';
$aLanguage['ru']['type_float_local'] = 'Дробное число (локальное поле)';
$aLanguage['ru']['type_date_local'] = 'Дата (локальное поле)';
$aLanguage['ru']['type_time_local'] = 'Время (локальное поле)';
$aLanguage['ru']['type_datetime_local'] = 'ДатаВремя (локальное поле)';
$aLanguage['ru']['type_inherit_local'] = 'Наследуемое (локальное поле)';

$aLanguage['ru']['type_text_html_local'] = 'HTML-code редактор (локальное поле)';
$aLanguage['ru']['type_text_js_local'] = 'JS редактор (локальное поле)';
$aLanguage['ru']['type_text_css_local'] = 'CSS редактор (локальное поле)';

$aLanguage['ru']['upd'] = 'Редактировать';
$aLanguage['ru']['del'] = 'Удалить';
$aLanguage['ru']['paramDelRowHeader'] = ' Удаление записи ';
$aLanguage['ru']['paramDelRow'] = ' Удалить запись "';
$aLanguage['ru']['paramAddForSection'] = 'Исправить для раздела';
$aLanguage['ru']['paramCopyToSection'] = 'Скопировать в данный раздел';

$aLanguage['ru']['class'] = 'Модуль';
$aLanguage['ru']['parent'] = 'Раздел';
$aLanguage['ru']['group'] = 'Метка';
$aLanguage['ru']['name'] = 'Имя';
$aLanguage['ru']['value'] = 'Значение';
$aLanguage['ru']['title'] = 'Название';
$aLanguage['ru']['access_level'] = 'Тип';
$aLanguage['ru']['show_val'] = 'Текстовое поле';

$aLanguage['ru']['notDeleteParam'] = 'Нельзя удалить системный параметр!';



$aLanguage['en']['Params.Adm.tab_name'] = 'Parameters';

$aLanguage ['en'] ['paramList'] = ' Parameter List ';
$aLanguage ['en'] ['noFindParam'] = ' Parameter not found';
$aLanguage ['en'] ['authError'] = ' Parameter access denied';
$aLanguage ['en'] ['cloneParam'] = ' Cloning parent parameter';
$aLanguage ['en'] ['editParam'] = ' Edit parameter ';
$aLanguage ['en'] ['addParam'] = ' Add parameter ';
$aLanguage ['en'] ['noSaveData'] = ' No data to save';
$aLanguage ['en'] ['saveParam'] = ' parameter is saved';
$aLanguage ['en'] ['saveParamError'] = ' An error occurred while saving ';
$aLanguage ['en'] ['noParamDelete'] = 'Option to remove non-selected ';
$aLanguage ['en'] ['notDeleteCloneParam'] = ' You can not delete parameters from the parent section ';
$aLanguage ['en'] ['deleteParam'] = ' Delete option ';
$aLanguage ['en'] ['deleteParamError'] = ' The delete failed ';
$aLanguage ['en'] ['noFindString'] = ' The original string is not found ';
$aLanguage ['en'] ['cloneError'] = ' An error occurred while cloning ';

$aLanguage['en']['type_system'] = 'System';
$aLanguage['en']['type_string'] = 'String';
$aLanguage['en']['type_text'] = 'Text';
$aLanguage['en']['type_wyswyg'] = 'Wyswyg';
$aLanguage['en']['type_check'] = 'Checkbox';
$aLanguage['en']['type_file'] = 'File';
$aLanguage['en']['type_imagefile'] = 'Image';
$aLanguage['en']['type_html'] = 'HTML';
$aLanguage['en']['type_select'] = 'Select';
$aLanguage['en']['type_int'] = 'Int';
$aLanguage['en']['type_float'] = 'Float';
$aLanguage['en']['type_date'] = 'Date';
$aLanguage['en']['type_time'] = 'Time';
$aLanguage['en']['type_datetime'] = 'Datetime';
$aLanguage['en']['type_inherit'] = 'Inherit';
$aLanguage['en']['type_service_section'] = 'Service section';

$aLanguage['en']['type_language'] = 'Language params';

$aLanguage['en']['type_text_html'] = 'HTML-code editor';
$aLanguage['en']['type_text_js'] = 'JS editor';
$aLanguage['en']['type_text_css'] = 'CSS editor';

$aLanguage['en']['type_string_local'] = 'String (local)';
$aLanguage['en']['type_text_local'] = 'Text (local)';
$aLanguage['en']['type_wyswyg_local'] = 'Wyswyg (local)';
$aLanguage['en']['type_check_local'] = 'Checkbox (local)';
$aLanguage['en']['type_file_local'] = 'File (local)';
$aLanguage['en']['type_imagefile_local'] = 'Image (local)';
$aLanguage['en']['type_html_local'] = 'HTML (local)';
$aLanguage['en']['type_select_local'] = 'Select (local)';
$aLanguage['en']['type_int_local'] = 'Int (local)';
$aLanguage['en']['type_float_local'] = 'Float (local)';
$aLanguage['en']['type_date_local'] = 'Date (local)';
$aLanguage['en']['type_time_local'] = 'Time (local)';
$aLanguage['en']['type_datetime_local'] = 'Datetime (local)';
$aLanguage['en']['type_inherit_local'] = 'Inherit (local)';
$aLanguage['en']['upd'] = 'Edit';
$aLanguage['en']['del'] = 'Delete';
$aLanguage['en']['paramDelRowHeader'] = ' Deleting ';
$aLanguage['en']['paramDelRow'] = ' Delete record " ';
$aLanguage['en']['paramAddForSection'] = ' Fix for the section ';
$aLanguage['en']['paramCopyToSection'] = 'Copy to this section';
$aLanguage['en']['filter'] = 'Filter';

$aLanguage['en']['class'] = 'Module';
$aLanguage['en']['parent'] = 'Parent';
$aLanguage['en']['group'] = 'Group';
$aLanguage['en']['name'] = 'Name';
$aLanguage['en']['value'] = 'Value';
$aLanguage['en']['title'] = 'Title';
$aLanguage['en']['access_level'] = 'Access level';
$aLanguage['en']['show_val'] = 'Show val';

$aLanguage['en']['payment_field_title'] = 'Title';
$aLanguage['en']['type_text_html_local'] = 'HTML-code editor (local)';
$aLanguage['en']['type_text_js_local'] = 'JS editor (local)';
$aLanguage['en']['type_text_css_local'] = 'CSS editor (local)';

$aLanguage['en']['notDeleteParam'] = 'You can not delete a system parameter!';

return $aLanguage;