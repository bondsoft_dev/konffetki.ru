<?php

$aLanguage = array();

$aLanguage['ru']['Catalog.Adm.tab_name'] = 'Каталог';

$aLanguage['ru']['category'] = 'Категория';
$aLanguage['ru']['brand'] = 'Бренд';
$aLanguage['ru']['listTpl'] = 'Шаблон вывода списка';
$aLanguage['ru']['relatedTpl'] = 'Шаблон сопутствующих товаров';
$aLanguage['ru']['includedTpl'] = 'Шаблон товаров из комплекта';
$aLanguage['ru']['listCnt'] = 'Кол-во позиций на страницу';
$aLanguage['ru']['showListFilters'] = 'Показывать панель фильтра в списке';
$aLanguage['ru']['showListSort'] = 'Показывать панель сортировки в списке';
$aLanguage['ru']['formSection'] = 'Раздел с формой заказа';
$aLanguage['ru']['formSectionError'] = 'Заданный раздел не существует {0}';

$aLanguage['ru']['template_catalog_order_form'] = 'Раздел с формой заказа';
$aLanguage['ru']['template_catalog_list_template'] = 'Шаблон для списка';
$aLanguage['ru']['template_catalog_on_page'] = 'на странице';
$aLanguage['ru']['view_category'] = 'Категория товаров для просмотра';

$aLanguage['ru']['tpl_'] = '<не менять>';
$aLanguage['ru']['tpl_list'] = 'список';
$aLanguage['ru']['tpl_gallery'] = 'галерея';
$aLanguage['ru']['tpl_table'] = 'таблица';

$aLanguage['ru']['errorCategorySelect'] = 'Ошибка при выборе параметра категории';
$aLanguage['ru']['error_no_card_for_category'] = 'Нельзя напрямую добавить товар - не задана карточка товара для выбранной категории каталога.';
$aLanguage['ru']['btn_settings'] = 'Настройки';
$aLanguage['ru']['goods_list'] = 'Список товаров';
$aLanguage['ru']['good_editor'] = 'Редактор товара';
$aLanguage['ru']['main_section'] = 'Основной раздел';
$aLanguage['ru']['settings'] = 'Настройки';
$aLanguage['ru']['good_card'] = 'Карточка товара';
$aLanguage['ru']['goods_card_select'] = 'Установка карточки для создаваемых товаров';
$aLanguage['ru']['card'] = 'Карточка';
$aLanguage['ru']['category'] = 'Категория';

$aLanguage['ru']['globalChangeTpl'] = 'Глобальный тип вывода';
$aLanguage['ru']['globalTpl_info'] = 'Внимание: будет изменен тип вывода списка для ВСЕХ разделов с каталогом!';
$aLanguage['ru']['globalTpl_ok'] = 'Шаблон успешно изменен';

/************************/

$aLanguage['en']['Catalog.Adm.tab_name'] = 'Catalog';

$aLanguage['en']['category'] = 'Category';
$aLanguage['en']['brand'] = 'Brand';
$aLanguage['en']['listTpl'] = 'List template';
$aLanguage['en']['relatedTpl'] = 'Template of related items';
$aLanguage['en']['includedTpl'] = 'Template of included items';
$aLanguage['en']['listCnt'] = 'Items per page';
$aLanguage['en']['showListFilters'] = 'Show filter panel in the list';
$aLanguage['en']['showListSort'] = 'Show sorting panel in the list';
$aLanguage['en']['formSection'] = 'Section with order form';
$aLanguage['en']['formSectionError'] = 'The specified key does not exist {0}';

$aLanguage['en']['template_catalog_order_form'] = 'Section with the order form';
$aLanguage['en']['template_catalog_list_template'] = 'The template for the list';
$aLanguage['en']['template_catalog_on_page'] = 'on the page';
$aLanguage['en']['view_category'] = 'Category to view products';

$aLanguage['en']['tpl_'] = '<do not change>';
$aLanguage['en']['tpl_list'] = 'list';
$aLanguage['en']['tpl_gallery'] = 'gallery';
$aLanguage['en']['tpl_table'] = 'table';

$aLanguage['en']['errorCategorySelect'] = 'Error in category selection';
$aLanguage['en']['error_no_card_for_category'] = 'Can not directly add goods - сard was not set for linked catalog category';
$aLanguage['en']['btn_settings'] = 'Settings';
$aLanguage['en']['goods_list'] = 'Goods list';
$aLanguage['en']['good_editor'] = 'Good editor';
$aLanguage['en']['main_section'] = 'Main section';
$aLanguage['en']['settings'] = 'Settings';
$aLanguage['en']['good_card'] = 'Goods card';
$aLanguage['en']['goods_card_select'] = 'Goods card setup';
$aLanguage['en']['card'] = 'Card';
$aLanguage['en']['category'] = 'Category';

$aLanguage['en']['globalChangeTpl'] = 'Global output type';
$aLanguage['en']['globalTpl_info'] = 'Warning: show type for list state of catalog will be changed for ALL sections!';
$aLanguage['en']['globalTpl_ok'] = 'Template successfully changed';

return $aLanguage;