<?php
/**
 * User: kolesnikiv
 * Date: 31.07.13
 */
$aConfig['name']     = 'Catalog';
$aConfig['title']    = 'Каталог';
$aConfig['version']  = '1.000a';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::ADM;
$aConfig['useNamespace'] = true;

$aConfig['events'][] = [
    'event' => \skewer\models\TreeSection::EVENT_AFTER_CREATE,
    'eventClass' => \skewer\models\TreeSection::className(),
    'class' => 'skewer\build\Component\Catalog\Api',
    'method' => 'addSection',
];

return $aConfig;
