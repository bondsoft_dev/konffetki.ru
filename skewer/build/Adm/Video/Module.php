<?php

namespace skewer\build\Adm\Video;

use skewer\build\Adm;
use skewer\core\Component\Config\Exception;


/**
 * Модуль фиктивный. Используется для создания директории в браузере файлов.
 * Class Module
 * @package skewer\build\Adm\Video
 */
class Module extends Adm\Tree\ModulePrototype {

    public function execute(){
        throw new Exception('Module Video is not allowed to execute.');
    }
}