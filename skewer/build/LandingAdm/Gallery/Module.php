<?php

namespace skewer\build\LandingAdm\Gallery;

use skewer\build\Component\UI;
use skewer\build\libs\LandingPage as LP;
use skewer\build\Adm;
use skewer\build\Component\Gallery;

/**
 * Класс для построения интерфейса типового редактора для LandingPage блока
 * Class Module
 * @package skewer\build\Adm\LandingPageEditor
 * @deprecated НЕ ИСПОЛЬЗУЕТСЯ ПОКА
 */
class Module extends Adm\Tree\ModulePrototype {


}