<?php

namespace skewer\build\LandingAdm\Form;


use skewer\build\Component\UI;
use skewer\build\libs\LandingPage as LP;
use skewer\build\Component\Forms;
use skewer\build\Adm;


/**
 * Класс для построения интерфейса типового редактора для LandingPage блока
 * Class Module
 */
class Module extends Adm\Tree\ModulePrototype {

    protected $currentForm = 0;

    /**
     * Инициализация
     */
    protected function actionInit() {

        $oForm = Forms\Table::get4Section( $this->pageId );
        if ( $oForm )
            $this->currentForm = $oForm->getId();

        $aFormList = Forms\Table::find()->where( 'form_handler_type<>?', 'toMethod' )->asArray()->getAll();
        $aTemplateForms = array( 0 => ' -- ' . (\Yii::t( 'forms', 'form_not_selected')) . ' --' );
        foreach ( $aFormList as $aItem )
            $aTemplateForms[$aItem['form_id']] = $aItem['form_title'];


        // -- сборка интерфейса
        $oFormBuilder = new UI\StateBuilder();
        $oFormBuilder->clear()->createFormEdit();
        $oFormBuilder
            ->addSelectField( 'new_form', \Yii::t( 'forms', 'select_form'), 's', $aTemplateForms )
            ->setFields()
            ->setValue( array( 'new_form' => $this->currentForm ) )
            ->addButton( \Yii::t('adm','save'), 'linkFormToSection', 'icon-save' )
        ;

        // вывод данных в интерфейс
        $this->setInterface( $oFormBuilder->getForm()  );
    }


    /**
     * Состояние добавления ссылки на форму
     */
    protected function actionLinkFormToSection() {

        $aData = $this->getInData();

        $iNewForm = ( isset($aData['new_form']) && $aData['new_form'] )? $aData['new_form']: 0;

        Forms\Table::link2Section( $iNewForm, $this->pageId );

        $this->currentForm = $iNewForm;

        $this->actionInit();
    }
//
//    /**
//     * Список полей формы
//     * @return int
//     */
//    protected function actionFieldList() {
//
//        if ( !$this->currentForm ) {
//            $this->addError( 'Не найдена форма' );
//            return psComplete;
//        }
//
//        $oBlock = Forms\State::getFrom4FieldList( $this->currentForm );
//
//        $this->setInterface( $oBlock );
//
//        return psComplete;
//    }
//
//
//    /**
//     * Форма редактирования поля формы
//     * @return int
//     */
//    protected function actionFieldEdit() {
//
//        $aData = $this->getInData();
//        $iFieldId = ( is_array($aData) && isset($aData['param_id']) ) ? (int)$aData['param_id'] : 0;
//
//        $oBlock = Forms\State::getForm4EditField( $iFieldId );
//
//        $this->setInterface( $oBlock );
//
//        return psComplete;
//    }
//
//    /**
//     * Состояние сохранения поля
//     */
//    protected function actionFieldSave() {
//
//        if ( !$this->currentForm ) {
//            $this->addError( 'Не найдена форма для сохранения' );
//            return psComplete;
//        }
//
//        // todo не вижу валидацию формы
//
//        $aData = $this->getInData();
//
//        Forms\Api::saveField( $aData, $this->currentForm );
//
//        $this->actionFieldList();
//    }
//
//
//    /**
//     * Состояние удаления поля формы
//     */
//    protected function actionFieldDel() {
//
//        $aData = $this->getInData();
//
//        $iFieldId = isSet( $aData['param_id'] ) ? $aData['param_id'] : 0;
//
//        if ( $iFieldId )
//            Forms\Api::delField( $iFieldId );
//        else
//            $this->addError( 'Не найдено поле для удаления' );
//
//        $this->actionFieldList();
//    }
//
//
//    protected function actionFormEdit() {
////        // номер опроса
////        $aData = $this->get('data');
////        if( is_array($aSubData) && !empty($aSubData) ) $aData = array_merge($aData, $aSubData);
////
////        $iItemId = (is_array($aData) && isset($aData['form_id'])) ? (int)$aData['form_id'] : 0;
////
////        if ( !$iItemId ) $iItemId = $this->iCurrentForm;
//
//        $oBlock = Forms\State::getFrom4EditForm();
//
//        $this->setInterface( $oBlock );
//
//        return psComplete;
//    }


    protected function actionFormSave() {

        // todo добавление id формы в параметры
        return psComplete;
    }

}