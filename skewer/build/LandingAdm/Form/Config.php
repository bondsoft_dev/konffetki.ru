<?php

$aConfig['name']     = 'Form';
$aConfig['title']    = 'LandingPage. Формы';
$aConfig['version']  = '1.000a';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::LANDING_ADM;
$aConfig['useNamespace'] = true;
$aConfig['languageCategory']     = 'forms';

return $aConfig;
