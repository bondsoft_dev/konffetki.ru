<?php

$aConfig['name']     = 'Slider';
$aConfig['title']    = 'LandingPage. Слайдер';
$aConfig['version']  = '1.000a';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::LANDING_ADM;
$aConfig['useNamespace'] = true;
$aConfig['languageCategory']     = 'slider';

return $aConfig;
