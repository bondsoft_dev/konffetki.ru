<?php

namespace skewer\build\LandingAdm\Slider;

use skewer\build\Component\Section\Parameters;
use skewer\build\Component\UI;
use skewer\build\libs\LandingPage as LP;
use skewer\build\Adm;
use skewer\build\Component\orm;


/**
 * Класс для построения интерфейса типового редактора для LandingPage блока
 * Class Module
 * @package skewer\build\Adm\LandingPageEditor
 */
class Module extends Adm\Slider\Module {// implements Tool\LeftList\ModuleInterface extends Adm\Tree\ModulePrototype

    protected $currentBanner = 0;

    function getName() {
        return $this->getModuleName();
    }

    protected function getWorkMode() {
        return self::tool;
    }

    /**
     * Инициализация
     */
    protected function actionInit() {

        if ( $this->currentBanner ) {
            $this->iCurrentBanner = $this->currentBanner;
            $this->actionSlideList();
        } else {
            $this->actionNonBanner();
        }

        $this->addCssFile(\Yii::$app->getAssetManager()->getBundle(\skewer\build\Adm\Slider\Asset::className())->baseUrl.'/css/SlideShower.css');
        $this->addJSFile(\Yii::$app->getAssetManager()->getBundle(\skewer\build\Adm\Slider\Asset::className())->baseUrl.'/js/SlideLoadImage.js');
        $this->addJSFile(\Yii::$app->getAssetManager()->getBundle(\skewer\build\Adm\Slider\Asset::className())->baseUrl.'/js/SlideShower.js');
    }


    /**
     * Интерфейс создания баннера
     */
    protected function actionNonBanner() {

        $oList = new \ExtList();

        $oList->addExtButton(
            \ExtDockedAddBtn::create()->setAction('CreateBanner')
        );

        $this->setInterface( $oList );
    }


    /**
     * Состояние создания нового банера
     */
    protected function actionCreateBanner() {

        $this->currentBanner =
            $this->iCurrentBanner =
                $this->actionNewBanner();

        // todo группа должна браться текущая
        Parameters::setParams( $this->getPageId(), 'object', 'currentBanner', $this->currentBanner );

        $this->actionSlideList();
    }


    public function actionNewBanner() {

        $iBannerId = orm\Query::InsertInto( 'banners_main' )
            ->set( 'title', 'LandingPage' )
            ->set( 'active', 1 )
            ->get();

        return $iBannerId;
    }

} 