<?php

namespace skewer\build\LandingAdm\Editor;


use skewer\build\libs\LandingPage as LP;

/**
 * AR Класс для задания полей формы редактирования шаблонов
 */

class TplField {

    /** @var string имя поля */
    public $name;

    /** @var string название поля */
    public $title;

    /** @var string тип поля */
    public $type = 'text';

    /** @var string группа для сохранения */
    public $group = LP\Api::groupMain;

    /** @var array дополнительные данные для отображения */
    public $addParams = array();

    /** @var string значение */
    public $text = '';

}