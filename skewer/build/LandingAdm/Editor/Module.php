<?php

namespace skewer\build\LandingAdm\Editor;


use skewer\build\Component\Section\Parameters;
use skewer\build\libs\LandingPage as LP;
use skewer\build\Component\UI;
use skewer\build\Adm;
use yii\web\ServerErrorHttpException;


/**
 * Класс для построения интерфейса типового редактора для LandingPage блока
 * Class Module
 * @package skewer\build\Adm\LandingPageEditor
 */
class Module extends Adm\Tree\ModulePrototype {

    /**
     * Инициализация
     */
    protected function actionInit() {

        $this->actionShowDataForm();

    }

    protected function canBeParent() {
        return true;
    }

    protected function getAllowedChildClass() {
        return 'skewer\build\libs\LandingPage\PagePrototype';
    }


    /**
     * Отдает набор полей для редактирования шаблона
     * @return TplField[]
     */
    protected function getTplFields() {

        $aOut = array();

        /*
         * CSS
         */
        $oCss = new TplField();
        $oCss->name = LP\Css::paramName;
        $oCss->title = 'CSS';
        $oCss->type = 'text_css';
        $oCss->text = $this->getCss();
        $oCss->addParams = array(
            'height' => 300,
            'subtext' => LP\Css::classLabel." - ".\Yii::t('editor', 'msg_helper')//класс для текущего блока"
        );

        $aOut[$oCss->name] = $oCss;

        /*
         * Шаблон
         */
        $oTpl = new TplField();
        $oTpl->name = LP\Tpl::paramName;
        $oTpl->title = \Yii::t('editor', 'template');
        $oTpl->text = $this->getTpl();
        $oTpl->type = 'text_html';
        $oTpl->addParams = array(
            'height' => 300,
            'subtext' => $this->getTplDescription()
        );

        $aOut[$oTpl->name] = $oTpl;

        return $aOut;

    }


    /**
     * Отображение формы для редактирования шаблона
     */
    protected function actionShowTplForm() {

        $oForm = new \ExtForm();

        $oForm->setPanelTitle( 'Редактирование шаблона' );

        $oFormModel = new UI\Form();

        foreach ( $this->getTplFields() as $oField ) {

            $oUIField = new UI\Form\Field(
                $oField->name,
                $oField->title,
                $oField->type,
                $oField->addParams
            );
            $oUIField->setValue( $oField->text );

            $oFormModel->addField( $oUIField );

        }

        // установить набор элементов формы
        $oForm->setFieldsByUiForm( $oFormModel );

        $oForm->addBtnSave( 'saveTpl' );
        $oForm->addBtnCancel( 'ShowTplForm' );

        $oForm->addBtnSeparator('->');
        $oForm->addExtButton( \ExtDocked::create(\Yii::t('editor', 'data') )->setAction( 'showDataForm' ) );

        $this->setInterface( $oForm );

    }

    /**
     * Сохраняет шаблон
     */
    protected function actionSaveTpl() {

        foreach ( $this->getTplFields() as $oField ) {

            $oParam = Parameters::getByName( $this->pageId, $oField->group, $oField->name );
            if (!$oParam)
                $oParam = Parameters::createParam(['parent' => $this->pageId, 'group' => $oField->group,
                    'name' => $oField->name]);

            $oParam->title = $oField->title;
            $oParam->show_val = $this->getInDataVal( $oField->name, '' );

            $oParam->save();

        }

        $this->actionShowTplForm();

    }

    /**
     * Отрисовывает форму по шаблону
     */
    protected function actionShowDataForm() {

        $oForm = new \ExtForm();

        $oForm->setPanelTitle( 'Редактирование данных' );

        /*
         * Форма
         */

        $oFormModel = new UI\Form();

        $aRowList = LP\Parser::getRowsByTpl( $this->getTpl() );

        foreach ( $aRowList as $oRow ) {

            $oFormModel->addField( new UI\Form\Field(
                $oRow->name,
                $oRow->title,
                $oRow->type
            ) );

        }

        // установить набор элементов формы
        $oForm->setFieldsByUiForm( $oFormModel );

        /*
         * Данные
         */

        $aData = LP\Api::getData( $this->pageId );

        // снять обертку для всплывающих изображений
        foreach ( $aData as $sKey => $sVal )
            $aData[$sKey] = \ImageResize::restoreTags( $sVal );

        $oForm->setValues( $aData );

        if ( empty($aRowList) ) {
            $oForm->setAddText( $this->get('empty') );
        } else {
            $oForm->addBtnSave( 'saveData' );
            $oForm->addBtnCancel( 'ShowDataForm' );
        }

        if ( \CurrentAdmin::isSystemMode() ) {
            $oForm->addBtnSeparator('->');
            $oForm->addExtButton( \ExtDocked::create( \Yii::t('editor', 'template') )->setAction( 'showTplForm' ) );
        }

        $this->setInterface( $oForm );

    }

    /**
     * Сохраняет данные формы
     */
    protected function actionSaveData() {

        // список полей
        $aRowList = LP\Parser::getRowsByTpl( $this->getTpl() );

        // набор имен меток в массиве
        $sAllowedNames = array_keys($aRowList);

        // пришедшие данные с фильтрацией
        $aData = $this->getInData( $sAllowedNames );

        // сохранение
        foreach ( $aData as $sName => $sVal ) {

            // установить обертку для всплывающих изображений
            $sVal = \ImageResize::wrapTags($sVal,$this->pageId);

            $oParam = Parameters::getByName( $this->pageId, LP\Api::groupData, $sName );
            if (!$oParam)
                $oParam = Parameters::createParam(['parent' => $this->pageId, 'group' => LP\Api::groupData,
                    'name' => $sName]);

            $oParam->title = isset($aRowList[$sName]) ? $aRowList[$sName]->title : '';

            $oParam->show_val = $sVal;

            $oParam->save();
        }

        $this->actionShowDataForm();

    }

    /**
     * Отдает шаблон текущей страницы
     */
    protected function getTpl() {
        return LP\Tpl::getForSection( $this->pageId );
    }

    /**
     * Отдает текущие CSS параметры
     */
    private function getCss() {
        return LP\Css::getForSection( $this->pageId );
    }

    /**
     * Отдаёт контент для клиентского модуля
     * @return string
     */
    protected function getPageModuleText() {

        $sSubLabel = 'lp_block';

        // id подраздела
        $iSubSectionId = $this->pageId;

        // параметр объекта
        $aObjParam =  Parameters::getByName( $iSubSectionId, Parameters::settings, Parameters::object, true );
        if ( !$aObjParam )
            throw new ServerErrorHttpException( 'Не найден клиентский корневой модуль' );

        // набор параметров
        $aParams = Parameters::getList( $iSubSectionId )
            ->fields(['value'])
            ->group(Parameters::settings)
            ->rec()
            ->asArray()
            ->get();

        $aParams['pageId'] = $iSubSectionId;

        // имя модуля
        $sClassName = \Module::getClassOrExcept($aObjParam['value'], \Layer::PAGE);

        // добавление подчиненного модуля
        $this->addChildProcess( new \skContext(
            $sSubLabel,
            $sClassName,
            ctModule,
            $aParams
        ));

        $oProcess = $this->getChildProcess( $sSubLabel );

        if ( !$oProcess )
            throw new ServerErrorHttpException( 'Не задан процесс клиентсого корневого модуля' );

        // отклюяаем прописанные напрямую в файле js
        $aJSList = \skLinker::getJSList();

        $oProcess->execute();

        \skLinker::setJSList( $aJSList );

        $oModule = $oProcess->getModule();
        if ( !$oModule )
            throw new ServerErrorHttpException( 'Не задан клиентский корневой модуль' );

        if ( !$oModule instanceof LP\PagePrototype )
            throw new ServerErrorHttpException( sprintf( 'Модуль [%s] должен быть наследником LandingPage\Prototype', $oModule->getModuleName() ) );

        $sOut = $oProcess->getOut();

        $this->removeChildProcess( $sSubLabel );

        unset($oProcess);

        return $sOut;

    }

    /**
     * Отдает описание текстовое описание для шаблона
     * @return string
     */
    private function getTplDescription() {

        $aTplDesc = Parameters::getByName(
            $this->pageId,
            LP\Api::groupMain,
            LP\Tpl::descParamName,
            true
        );

        return $aTplDesc ? $aTplDesc[ 'show_val' ] : '';

    }

}
