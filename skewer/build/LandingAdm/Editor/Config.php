<?php

$aConfig['name']     = 'Editor';
$aConfig['title']    = 'LandingPage. Редактор';
$aConfig['version']  = '1.000a';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::LANDING_ADM;
$aConfig['useNamespace'] = true;
$aConfig['languageCategory']     = 'editor';

return $aConfig;
