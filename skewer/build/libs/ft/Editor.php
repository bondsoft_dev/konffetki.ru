<?php

namespace skewer\build\libs\ft;


/**
 * Класс для работы с редакторами ft-сущностей
 * Class Editor
 * @package skewer\build\libs\ft
 */
class Editor {

    // стандартные редакторы
    const INTEGER = 'int';
    const FLOAT = 'float';
    const MONEY = 'money';
    const STRING = 'string';
    const TEXT = 'text';
    const HTML = 'html';
    const WYSWYG = 'wyswyg';
    const SELECT = 'select';
    const MULTICHECK = 'multicheck';
    const CHECK = 'check';
    const FILE = 'file';
    const GALLERY = 'gallery';
    const DATE = 'date';
    const TIME = 'time';
    const HIDE = 'hide';

    // спец редакторы
    const MULTISELECT = 'multiselect';
    const COLLECTION = 'collection';

    private static $aEditorList = array(
        self::INTEGER => 'int',
        self::FLOAT => 'float',
        self::MONEY => 'decimal',
        self::STRING => 'varchar',
        self::TEXT => 'text',
        self::HTML => 'text',
        self::WYSWYG => 'text',
        self::SELECT => 'varchar',
        self::CHECK => 'int',
        self::FILE => 'varchar',
        self::GALLERY => 'varchar',
        self::DATE => 'date',
        self::TIME => 'time',
        self::HIDE => 'varchar',
    );

    /**
     * Отдает массив пар "псевдоним типа"  => "имя типа"
     * @return array
     */
    public static function getSimpleList() {

        $aList = array();

        foreach ( self::$aEditorList as $sKey => $aItem )
            $aList[$sKey] = \Yii::t('ft', 'field_type_'.$sKey );

        return $aList;
    }

    /**
     * Отдает тип редактора для интерфейса
     * @param string $editor
     * @return string
     */
    public static function getTypeForEditor( $editor ) {

        if ( $editor == self::COLLECTION )
            return self::INTEGER;

        $editor = isset(self::$aEditorList[$editor]) ? $editor : self::STRING;
        return self::$aEditorList[$editor];
    }

}