<?php

namespace skewer\build\libs\ft\converter;

use skewer\build\libs\ft;

/**
 * Класс для преобразования json ft описания в класс skewer\build\libs\ft\Model
 * @package skewer\build\libs\ft\formater
 */
class Json implements ConverterInterface {

    /**
     * Преобрзовывает данные в ft модель
     * @param string $sIn входные данные
     * @return ft\Model
     */
    function dataToFtModel( $sIn ) {
        return new ft\Model( json_decode($sIn, true) );
    }

    /**
     * Преобрзовывает данные в ft модель
     * @param ft\Model $oModel модель данных для экспорта
     * @return string
     */
    function ftModelToData( ft\Model $oModel ) {
        return json_encode( $oModel->getModelArray() );
    }

}