<?php

namespace skewer\build\libs\ft\converter;

use skewer\build\libs\ft;

/**
 * Интерфейс для классов преобразования в ft модель и из нее
 * @package skewer\build\libs\ft\formater
 */
interface ConverterInterface {

    /**
     * Преобрзовывает данные в ft модель
     * @param string $sIn входные данные
     * @return ft\Model
     */
    function dataToFtModel( $sIn );

    /**
     * Преобрзовывает данные в ft модель
     * @param ft\Model $oModel модель данных для экспорта
     * @return string
     */
    function ftModelToData( ft\Model $oModel );

}