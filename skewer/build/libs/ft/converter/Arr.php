<?php

namespace skewer\build\libs\ft\converter;

use skewer\build\libs\ft;

/**
 * Класс для преобразования массивов ft описания в класс skewer\build\libs\ft\Model и обратно
 * @package skewer\build\libs\ft\formater
 */
class Arr implements ConverterInterface {

    /**
     * Преобрзовывает данные в ft модель
     * @param array $aIn входные данные
     * @return ft\Model
     */
    function dataToFtModel( $aIn ) {
        return new ft\Model( $aIn );
    }

    /**
     * Преобрзовывает данные в ft модель
     * @param ft\Model $oModel модель данных для экспорта
     * @return array
     */
    function ftModelToData( ft\Model $oModel ) {
        return $oModel->getModelArray();
    }

}