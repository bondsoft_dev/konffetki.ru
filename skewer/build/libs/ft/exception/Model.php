<?php

namespace skewer\build\libs\ft\exception;

use skewer\build\libs\ft as ft;

/**
 *
 *
 * @class: ftModelException
 *
 * @Author: User, $Author$
 * @version: $Revision$
 * @date: $Date$
 *
 */

class Model extends ft\Exception {

    /**
     * Отдает набор путей для исключения из трассировки
     * @return array
     */
    protected function getSkipList() {
        return array(
            COREPATH,
            BUILDPATH.'libs/ft/',
            BUILDPATH.'classes/ft.php'
        );
    }


}
