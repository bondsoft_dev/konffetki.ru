<?php

namespace skewer\build\libs\ft;

/**
 * Класс помошник для работы с индексами
 * Class Index
 * @package skewer\build\libs\ft
 */
class Index {

    /** обычный индекс */
    const index = 'index';

    /** первичный ключ */
    const primary = 'primary';

    /** уникальный ключ */
    const unique = 'unique';

    /** полнитекстовый индекс */
    const fulltext = 'fulltext';

    /** ключ */
    const key = 'key';

}