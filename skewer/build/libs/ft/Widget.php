<?php

namespace skewer\build\libs\ft;

/**
 * Класс для работы с виджетами
 * Class Widget
 * @package skewer\build\libs\ft
 */
class Widget {

    /** имена констант для флагов использования в состояних */
    const useInShow = '__useInShow';
    const useInEdit = '__useInEdit';
    const useInList = '__useInList';

    const modeShow = 1;
    const modeList = 2;
    const modeEdit = 3;

    /**
     * @param proc\widget\Prototype $oWidget
     * @param int $iMode
     * @throws exception\Model
     * @return bool
     */
    public static function canUseInMode( proc\widget\Prototype $oWidget, $iMode ) {

        switch ( $iMode ) {

            case self::modeEdit:
                return $oWidget->useInEdit();

            case self::modeShow:
                return $oWidget->useInShow();

            case self::modeList:
                return $oWidget->useInList();

            default:
                throw new exception\Model( "Неизвестный режим применение виджетов [$iMode]" );

        }

    }

}
