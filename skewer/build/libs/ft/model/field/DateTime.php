<?php

namespace skewer\build\libs\ft\model\field;

use skewer\build\libs\ft;

/**
 * Класс поля типа datetime
 * Class DateTime
 * @package skewer\build\libs\ft\model\field
 */
class DateTime extends ft\model\Field {

    public function getDefault() {

        $sVal = parent::getDefault();

        if ( $sVal === 'now' )
            $sVal = date( 'Y-m-d H:i:s' );

        return $sVal;

    }

} 