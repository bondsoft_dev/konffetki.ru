<?php

namespace skewer\build\libs\ft\model;

use skewer\build\Component\orm;
use skewer\build\libs\ft as ft;


/**
 * Класс описания поля сущности
 * ver 2.00
 *
 * todo Переписать
 * 1. сделать прототип с моделью попроще (без спец ft полей)
 * 2. этот класс сделать расширением описанного выше
 * 3. отказаться от описаний массивом
 * 4. сделать класс импорта/экспорта ( массив, yaml )
 *
 */

class Field {

    /** @var string имя сущности */
    protected $sName;

    /** @var string название сущности */
    protected $sTitle;

    /** @var array описание поля */
    protected $aFieldOptions = array();

    /** @var ft\Model ссылка на родительскую модель */
    protected $oModel = null;

    /** @var ft\Relation[] набор связей */
    protected $aRelations = array();

    /** @var ft\proc\modificator\Prototype[]|null $aModifList кэш набора модификаторов (null - ещё не заполнялся) */
    private $aModifCache = null;

    /** @var ft\proc\modificator\Prototype[]|null $aWidgetCache кэш набора модификаторов (null - ещё не заполнялся) */
    private $aWidgetCache = null;

    /**
     * Конструктор
     * @param string $sFieldName имя поля
     * @param array $aFieldOptions описание поля
     * @throws ft\Exception
     */
    public function __construct( $sFieldName, $aFieldOptions ) {

        // если формат данных заведомо левый - то выходим
        if(!is_array($aFieldOptions))
            throw new ft\Exception('Неверный тип контейнера описания для поля');

        // сохраняем описание
        $this->aFieldOptions = $aFieldOptions;

        // добавление отношений
        $aRelations = isset($aFieldOptions['relations']) ? $aFieldOptions['relations'] : array();
        if ( !is_array($aRelations))
            throw new ft\exception\Model('Неверный контейнер связей');
        foreach ( $aRelations as $aRel ) {
            $oRel = new ft\Relation(
                isset($aRel['type']) ? $aRel['type'] : '',
                isset($aRel['entity']) ? $aRel['entity'] : '',
                isset($aRel['content']) ? $aRel['content'] : '',
                isset($aRel['inner']) ? $aRel['inner'] : '',
                isset($aRel['external']) ? $aRel['external'] : ''
            );
            $this->aRelations[] = $oRel;
        }

        // имя поля
        $this->sName = (string)$sFieldName;
        if ( !$this->sName )
            throw new ft\Exception('Отсутствует имя поля');

        // название сущности
        $this->sTitle = (isset($aFieldOptions['title']) and $aFieldOptions['title']) ?
            (string)$aFieldOptions['title'] :
            $this->getName();

    }

    /**
     * Отдает базовый набор для генерации поля
     * @param string $sDatatype
     * @param string $sTitle
     * @return array
     */
    public static function getBaseDesc( $sDatatype, $sTitle ) {

        // задан размер
        if ( preg_match( '/^(\w+)[(](\d+)[)]$/i', $sDatatype, $find ) ){
            $sDatatype = $find[1];
            $size = $find[2];
        } else $size = '';

        if ( $sDatatype==='str' )
            $sDatatype = 'varchar';

        // todo сделать проверку доступных типов

        // формирование описания поля
        return array(
            'datatype' => $sDatatype,
            'multilang' => 0,
            'type' => 1,
            'required' => 1,
            'fictitious' => false,
            'size' => $size,
            'title' => $sTitle,
            'default' => '',
            'editor' => '',
            'params' => array(),
            'widget' => array(),
            'validator' => array(),
            'modificator' => array(),
            'relations' => array()
        );

    }

    /**
     * Добавляет атрибут
     * @param $sOptionName
     * @param mixed $mVal
     * @return mixed
     */
    public function setOption( $sOptionName, $mVal ) {
        return $this->aFieldOptions[$sOptionName] = $mVal;
    }

    /**
     * Отдает атрибут
     * @param $sOptionName
     * @return mixed|null
     */
    public function getOption( $sOptionName ) {
        return isset($this->aFieldOptions[$sOptionName]) ? $this->aFieldOptions[$sOptionName] : null;
    }

    /**
     * Отдает массив описания
     * @return array
     */
    public function getModelArray() {

        $aModel = $this->aFieldOptions;

        $aModel['relations'] = array();

        foreach ( $this->aRelations as $oRelation )
            $aModel['relations'][] = $oRelation->getModelArray();

        return $aModel;

    }

    /**
     * Задает значение флага "фиктивное поле"
     * @param bool $mVal
     */
    public function setFictitious( $mVal ) {
        $this->setOption('fictitious',(bool)$mVal);
    }

    /**
     * Задает значение флага "фиктивное поле"
     * @return bool
     */
    public function getFictitious() {
        return (bool)$this->getOption('fictitious');
    }

    /**
     * Флаг фиктивного поля
     * @return bool
     */
    public function isFictitious() {
        return $this->getFictitious();
    }

    /**
     * Отдает имя поля
     * @return string
     */
    public function getName(){
        return $this->sName;
    }

    /**
     * Задает имя поля
     * @param string $sName
     */
    public function setName( $sName ){
        $this->sName = $sName;
    }

    /**
     * Отдает название поля
     * @return string
     */
    public function getTitle(){
        return $this->sTitle;
    }

    /**
     * Отдает тип переменной в базе
     * @return string
     */
    public function getDatatype() {
        return (string)$this->getOption('datatype');
    }

    /**
     * Отдает тип переменной в базе с размерностью в скобках, если есть
     * @return string
     */
    public function getDatatypeFull() {

        // тип
        $sOut = $this->getDatatype();

        // расширить размерностью, если задана
        if ( $this->getSize() )
            $sOut .= sprintf('(%d)',$this->getSize());

        // для int по умолчанию размерность 11
        elseif ( $sOut === 'int' )
            $sOut .= '(11)';

        return $sOut;

    }

    /**
     * Отдает размер переменной в базе
     * @return int
     */
    public function getSize() {
        return (int)$this->getOption('size');
    }

    /**
     * Задает переметр "обязательное"
     * @param $mVal
     * @return string
     */
    public function setRequired( $mVal ) {
        return (string)$this->setOption('required',(int)(bool)$mVal);
    }

    /**
     * Задает значение мультиязычности
     * @param $iVal
     * @return void
     */
    public function setMultilang( $iVal ) {
        $this->setOption('multilang',(int)$iVal);
    }

    /**
     * Отдает значение мультиязычности
     * @return int
     */
    public function getMultilang() {
        return (int)$this->getOption('multilang');
    }

    /**
     * Отдает флаг "мультиязычное"
     * @return bool
     */
    public function isMultilang() {
        return (bool)$this->getMultilang();
    }

    /**
     * Возвращает true, если поле - подчиненная сущность
     * @return bool
     */
    public function isEntity() {
        return false;
    }

    /**
     * Отдает имя редактора
     * @return string
     */
    public function getEditorName() {
        $aEditor = $this->getOption( 'editor' );
        if ( is_array($aEditor) and isset($aEditor['name']) )
            return (string)$aEditor['name'];
        else
            return '';
    }

    /**
     * Отдает параметры редактора
     * @return array
     */
    public function getEditorParams() {
        $aEditor = $this->getOption( 'editor' );
        if ( is_array($aEditor) and isset($aEditor['params']) )
            return $aEditor['params'];
        else
            return array();
    }

    /**
     * Устанавливает имя редактора для поля
     * @param string $sEditorName
     * @param array $aParam
     */
    public function setEditor( $sEditorName, $aParam=array() ) {
        $this->setOption('editor', array(
            'name' => $sEditorName,
            'params' => $aParam
        ));
    }

    /**
     * Отдает массив с набором параметров
     * @return array
     */
    public function getParameterList() {
        return $this->aFieldOptions['params'];
    }

    /**
     * Сохраняет массив с набором параметров
     * @param $aParamList
     */
    public function setParameterList( $aParamList ) {
        $this->aFieldOptions = $aParamList;
    }

    /**
     * Отдает значение параметра
     * @static
     * @param $sParamName
     * @return mixed|null
     */
    public function getParameter( $sParamName ) {
        return isset($this->aFieldOptions['params'][$sParamName]) ? $this->aFieldOptions['params'][$sParamName] : null;
    }

    /**
     * Задает значение параметра
     * @static
     * @param $sParamName
     * @param $mValue
     */
    public function setParameter( $sParamName, $mValue ) {
        $this->aFieldOptions['params'][(string)$sParamName] = $mValue;
    }


    /**
     * Возвращает значение всех атрибутов поля
     * @return string
     */
    public function getAttrs() {
        return isSet($this->aFieldOptions['attrs']) ? $this->aFieldOptions['attrs'] : array();
    }


    /**
     * Возвращает значение атрибута
     * @param string $sAttrName Имя атрибута
     * @return string
     */
    public function getAttr( $sAttrName ) {
        return $this->isSetAttr($sAttrName) ? $this->aFieldOptions['attrs'][$sAttrName] : null;
    }


    /**
     * Устанавливает значение для атрибута
     * @param int $sAttrName Имя атрибута
     * @param bool|string|int $sAttrValue Значение атрибута
     */
    public function setAttr( $sAttrName, $sAttrValue = false ) {
        $this->aFieldOptions['attrs'][$sAttrName] = $sAttrValue;
    }


    /**
     * Проверка наличие атрибута
     * @param string $sAttrName имя атрибута
     * @return bool
     */
    public function isSetAttr( $sAttrName ) {
        return isSet( $this->aFieldOptions['attrs'][$sAttrName] );
    }

    /**
     * Задает ссылку на модель
     * @param ft\Model $oModel
     */
    public function setModel( ft\Model $oModel ) {
        $this->oModel = $oModel;
    }

    /**
     * Отдает ссылку на родительскую модель
     * @return \skewer\build\libs\ft\Model
     */
    public function getModel() {
        return $this->oModel;
    }


    /**
     * Добавить в контейнер описания сущности - уникальное значение
     * @param $sContName
     * @param $sValue
     */
    protected function addToArrayUnique( $sContName, $sValue ) {
        if ( !in_array($sValue, $this->aFieldOptions[$sContName]) )
            $this->aFieldOptions[$sContName][] = $sValue;
    }

    /**
     * Добавление виджета
     * @param $sPName
     * @param array $aParam
     */
    public function addWidget( $sPName, $aParam = array() ) {
        //$this->addToArrayUnique('widget', $sPName);

        // проверка на уникальность
        if ( count( $this->aFieldOptions['widget'] ) )
            foreach ( $this->aFieldOptions['widget'] as $aWidget ) {

                if ( $aWidget['name'] == $sPName )
                    return;
            }

        $this->aFieldOptions['widget'][] = array(
            'name' => $sPName,
            'params' => $aParam
        );
    }

    /**
     * Удаляет виджет по имени
     * @param string $sPName
     */
    public function delWidget( $sPName ) {

        foreach ( $this->aFieldOptions['widget'] as $iKey => $aWidget ) {
            if ( $aWidget['name'] == $sPName )
                unset($this->aFieldOptions['widget'][$iKey]);
        }

    }

    /**
     * Отдает список виджетов
     * @param $oRow
     * @throws \skewer\build\libs\ft\Exception
     * @throws \skewer\build\libs\ft\exception\Model
     * @return ft\proc\widget\Prototype[][]
     * todo перевести на новые AR
     */
    public function getWidgetAsStrList( $oRow=null ) {

    }


    /**
     * Отдает имя первого виджета
     * @return string
     */
    public function getWidgetName() {

        $aWidgetList = $this->aFieldOptions['widget'];

        if ( !$aWidgetList )
            return '';

        return isSet($aWidgetList[0]['name']) ? $aWidgetList[0]['name'] : '';
    }


    /**
     * Отдает список виджетов
     * @param $oRow
     * @throws \skewer\build\libs\ft\Exception
     * @throws \skewer\build\libs\ft\exception\Model
     * @return ft\proc\widget\Prototype[][]
     * todo перевести на новые AR
     */
    public function getWidgetList( $oRow=null ) {

        return array();

        // проверка валидность пришедшей записи (или отсутствия)
        if ( !is_null($oRow) and !$oRow instanceof ft\ArPrototype )
            throw new ft\exception\Model(
                'First param for ft\model\Field::getWidgetList must be ft\ArPrototype or null'
            );

        // заполнить кэш, если не заполнялся еще
        if ( is_null( $this->aWidgetCache ) ) {

            $aList = array();

            foreach ( $this->aFieldOptions['widget'] as $aModif ) {

                $sName =  is_array($aModif) ? ( isSet($aModif['name']) ? $aModif['name'] : '' ) : $aModif;
                if ( strpos( $sName, '\\' ) === false )
                    $sClassName = 'skewer\\build\\libs\\ft\\proc\\widget\\'.\skTranslit::toCamelCase($sName);
                else
                    $sClassName = $sName;

                if ( !class_exists($sClassName) )
                    throw new ft\Exception("Не найден виджет [$sClassName]");

                $oModif = new $sClassName();

                if ( !$oModif instanceof ft\proc\widget\Prototype )
                    throw new ft\exception\Model("Виджет [$sClassName] не является наследником ft\\proc\\widget\\Prototype");

                $oModif->setField( $this );
                $oModif->setModel( $this->getModel() );
                $oModif->setParamList( $aModif['params'] );

                $oModif->checkInit();

                $aList[] = $oModif;

            }

            $this->aWidgetCache = $aList;

        }

        // занести строку в модификатор
        foreach ( $this->aWidgetCache as $oWidget )
            $oWidget->setRow( $oRow );

        return $this->aWidgetCache;

    }

    /**
     * Добавление модификатора
     * @param string $sMName Имя модификатора
     * @param array $aParam Дополнительные парамеры
     */
    public function addModificator( $sMName, $aParam = array() ) {
        $this->aFieldOptions['modificator'][] = array(
            'name' => $sMName,
            'params' => $aParam
        );
    }

    /**
     * Отдает список модификаторов
     * @param ft\ArPrototype $oRow
     * @throws ft\Exception
     * @throws ft\exception\Model
     * @return ft\proc\modificator\Prototype[]
     * todo перевести на новые AR
     */
    public function getModificatorList( /*ft\ArPrototype*/ $oRow ) {

//        // проверка валидность пришедшей записи (или отсутствия)
//        if ( !is_null($oRow) and !$oRow instanceof ft\ArPrototype )
//            throw new ft\exception\Model(
//                'First param for ft\model\Field::getModificatorList must be ft\ArPrototype or null'
//            );

        // заполнить кэш, если не заполнялся еще
        if ( is_null( $this->aModifCache ) ) {

            $aList = array();

            foreach ( $this->aFieldOptions['modificator'] as $aModif ) {

                $sName =  is_array($aModif) ? ( isSet($aModif['name']) ? $aModif['name'] : '' ) : $aModif;
                if ( strpos( $sName, '\\' ) === false )
                    $sClassName = 'skewer\\build\\libs\\ft\\proc\\modificator\\'.\skTranslit::toCamelCase($sName);
                else
                    $sClassName = $sName;

                if ( !class_exists($sClassName) )
                    throw new ft\Exception("Не найден модификатор [$sClassName]");

                $oModif = new $sClassName();

                if ( !$oModif instanceof ft\proc\modificator\Prototype )
                    throw new ft\exception\Model("Модификатор [$sClassName] не является наследником ft\\proc\\modificator\\Prototype");

                $oModif->setField( $this );
                $oModif->setModel( $this->getModel() );
                $oModif->setParamList( $aModif['params'] );

                $oModif->checkInit();

                $aList[] = $oModif;

            }

            $this->aModifCache = $aList;

        }

        // занести строку в модификатор
        foreach ( $this->aModifCache as $oModif )
            $oModif->setRow( $oRow );

        return $this->aModifCache;

    }

    /**
     * Добавление валидатора
     * @param string $sPName
     * @param array $aParam
     */
    public function addValidator( $sPName, $aParam = array() ) {

        // проверка на уникальность
        if ( count( $this->aFieldOptions['validator'] ) )
            foreach ( $this->aFieldOptions['validator'] as $aValidator ) {
                if ( $aValidator['name'] == $sPName )
                    return;
            }

        $this->aFieldOptions['validator'][] = array(
            'name' => $sPName,
            'params' => $aParam
        );

    }

    /**
     * Удаляет валидатор по имени
     * @param string $sPName
     */
    public function delValidator( $sPName ) {

        foreach ( $this->aFieldOptions['validator'] as $iKey => $aValidator ) {
            if ( $aValidator['name'] == $sPName )
                unset($this->aFieldOptions['validator'][$iKey]);
        }

    }

    /**
     * Отдает список валидаторов
     * @param orm\ActiveRecord $oRow
     * @throws ft\Exception
     * @return ft\proc\validator\Prototype[]
     */
    public function getValidatorList( $oRow ) {

        $aList = array();

        foreach ( $this->aFieldOptions['validator'] as $aValidator ) {

            $sName =  is_array($aValidator) ? ( isSet($aValidator['name']) ? $aValidator['name'] : '' ) : $aValidator;
            $sClassName = 'skewer\\build\\libs\\ft\\proc\\validator\\'.ucfirst($sName);

            if ( !class_exists($sClassName) )
                throw new ft\Exception("Не найден валидатор [$sName]");

            /** @var ft\proc\validator\Prototype $oValidator */
            $oValidator = new $sClassName();
            $oValidator->setField( $this );
            $oValidator->setModel( $this->getModel() );
            if ( !is_null($oRow) ) {
//                if ( !$oRow instanceof ft\ArPrototype )
//                    throw new ft\exception\Model(
//                        'First param for ft\model\Field::getValidatorList must be ft\ArPrototype or null'
//                    );
                $oValidator->setRow( $oRow );
            }
            $oValidator->setParamList( $aValidator['params'] );

            $oValidator->checkInit();

            $aList[] = $oValidator;

        }

        return $aList;
    }

    /**
     * Задает значение поля по умолчанию
     * @param mixed $mVal
     */
    public function setDefault( $mVal ) {
        $this->setOption( 'default', $mVal );
    }

    /**
     * Возвращшает значение по-умолчанию для атрибута сущности
     * @return mixed
     */
    public function getDefault() {
        return $this->getOption( 'default' );
    }

    /**
     * Возвращает Описание атрибута сущности
     * @return string
     */
    public function getDescription() {

        return '';
    }

    /**
     * Добавление связи
     * @param ft\Relation $oRelation
     */
    public function addRelation( ft\Relation $oRelation ) {

        $this->aRelations[] = $oRelation;

        if ( $oRelation->getType() == ft\Relation::MANY_TO_MANY )
            $this->buildLinkTable();

    }

    /**
     * Добавление связи
     * @return ft\Relation[]
     */
    public function getRelationList() {
        return $this->aRelations;
    }


    /**
     * Получаем первкю связь поля
     * @return ft\Relation
     */
    public function getFirstRelation() {
        return count($this->aRelations) ? $this->aRelations[0] : null;
    }


    /**
     * Имя таблицы связей (><)
     * @return string
     */
    public function getLinkTableName() {
        // плохое имя так как невозможно повторить из связанной сущности
        return 'cr_' . $this->getModel()->getName() . '__' . $this->getName();
    }


    /**
     * Создание таблицы связей
     * @return bool
     */
    public function buildLinkTable() {

        ft\Entity::get( $this->getLinkTableName() )
            ->clear( false )
            ->setNamespace( __NAMESPACE__ )
            ->addField( ft\Relation::INNER_FIELD, 'int', 'Ид внутренней сущности' )
            ->addField( ft\Relation::EXTERNAL_FIELD, 'int', 'Ид внешней сущности' )
            ->addField( ft\Relation::SORT_FIELD, 'int', 'Вес для сортировки' )
            ->selectFields( ft\Relation::INNER_FIELD . ',' . ft\Relation::EXTERNAL_FIELD )
            ->addIndex('unique')
            ->save()
            ->build()
        ;

        return true;
    }

    /**
     * Набор связанных записей
     * @param int $id Ид записи для которой собираем связи
     * @return array
     */
    public function getLinkRow( $id ) {

        $query = orm\Query::SelectFrom( $this->getLinkTableName() )
            ->where( ft\Relation::INNER_FIELD, $id )
            ->order( ft\Relation::SORT_FIELD )
            ->asArray();

        $out = [];

        while ( $row = $query->each() )
            $out[] = $row[ ft\Relation::EXTERNAL_FIELD ];

        return $out;
    }


    /**
     * Обнавление набора связанных записей
     * @param int $id Ид записи для которой обновляем связи
     * @param int[] $values Набор id связанных записей
     * @return bool
     */
    public function updLinkRow( $id, $values = [] ) {

        // удаляем
        $query = orm\Query::DeleteFrom( $this->getLinkTableName() )
            ->where( ft\Relation::INNER_FIELD, $id )
        ;

        if ( $values )
            $query->andWhere( ft\Relation::EXTERNAL_FIELD . ' NOT IN ?', $values );

        $query->get();

        // добавляем
        foreach ( $values as $val )
            if ( $val )
                orm\Query::InsertInto( $this->getLinkTableName() )
                    ->set( ft\Relation::INNER_FIELD, $id )
                    ->set( ft\Relation::EXTERNAL_FIELD, $val )
//                    ->setInc( ft\Relation::SORT_FIELD, [
//                        ft\Relation::INNER_FIELD => $id,
//                        ft\Relation::EXTERNAL_FIELD => $val
//                    ])
                    ->onDuplicateKeyUpdate()
                    ->set( ft\Relation::SORT_FIELD, 0 )
                    ->get();


        return true;
    }


    /**
     * Удаление всех связей с элементом
     * @param int $id Идентификатор элемента связанной сущности
     * @return bool
     * fixme плохое имя
     */
    public function unLinkAllRow( $id ) {
        orm\Query::DeleteFrom( $this->getLinkTableName() )
            ->where( ft\Relation::EXTERNAL_FIELD, $id )
            ->get();
        return true;
    }

}
