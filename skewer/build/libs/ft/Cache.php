<?php

namespace skewer\build\libs\ft;

use skewer\build\Component\Catalog;
use skewer\build\Component\Command\Exception;
use skewer\build\Component\orm\MagicTable;

/**
 * Класс для работы с кэшем ft
 * ver 2.00
 */

class Cache {

    /** @var array[] Список ft-моделей в виде массивов*/
    private static $aModelList = array();

    /**
     * Проверяет существует ли сущность
     * @static
     * @param string $sEntityName имя сущности
     * @return bool
     */
    public static function exists( $sEntityName ) {
        return isSet( self::$aModelList[$sEntityName] );
    }

    /**
     * Отдает объект-описание сущности
     * @static
     * @param int|string $sEntityName имя сущности
     * @throws Exception
     * @return Model
     */
    static public function get( $sEntityName ) {

        if ( !isSet( self::$aModelList[$sEntityName] ) ) {

            // запросник на сущность

            /** @var Catalog\model\EntityRow $oEntityRow */
            if ( is_numeric( $sEntityName ) ) {

                $oEntityRow = Catalog\model\EntityTable::find( $sEntityName );
            }
            else {

                $sValue = $sEntityName;
                if (strpos($sEntityName, 'c_') === 0)
                    $sValue = substr( $sEntityName, 2 );

                $oEntityRow = Catalog\model\EntityTable::find()
                    ->where('name',$sValue)
                    ->getOne();

            }

            if ( empty($oEntityRow) )
                throw new Exception("Не найдена модель для сущности [$sEntityName]");

            $oModel = $oEntityRow->getModelFromCache();

            self::set( $sEntityName, $oModel );

            return clone $oModel;

        } else {

            $oConverter = new converter\Arr();

            return $oConverter->dataToFtModel( self::$aModelList[$sEntityName] );

        }

    }


    /**
     * Отдает объект-описание сущности или FALSE если не сущность найдена
     * @param int|string $card имя сущности
     * @return bool|Model
     */
    static public function softGet( $card ) {
        try {
            return self::get( $card );
        } catch ( Exception $e ) {
            return false;
        }
    }


    /**
     * Записать в кэш описание модели
     * @static
     * @param $sEntityName
     * @param Model $oModel
     */
    static public function set( $sEntityName, Model $oModel ) {

        $oConverter = new converter\Arr();

        self::$aModelList[$sEntityName] = $oConverter->ftModelToData( $oModel );

    }

    /**
     * Загрузка системный сущностей
     * @param $sEntityName
     * @param $sNameSpace
     */
    public static function loadSystemEntity ( $sEntityName, $sNameSpace ) {

        self::set( $sEntityName, self::get($sNameSpace.'\\'.$sEntityName) );

    }

    /**
     * Отдает CRUD-контроллер для модели
     * @param $sModelName
     * @param string $sNamespace
     * @throws \Exception
     * @return QueryPrototype
     * @deprecated
     */
    static public function getModelTable( $sModelName, $sNamespace = '' ) {

        $sModelName = \skTranslit::toCamelCase( $sModelName );

        $sTableFromCache = "\\cache\\model\\{$sModelName}Table";

        if ( $sNamespace ) {

            $sTableFromNamespace = sprintf(
                "%s\\model\\%sTable",
                $sNamespace,
                $sModelName
            );

            if ( class_exists( $sTableFromNamespace ) )
                return new $sTableFromNamespace();

            $sTableFromNamespace = sprintf(
                "%s\\%sTable",
                $sNamespace,
                $sModelName
            );

            if ( class_exists( $sTableFromNamespace ) )
                return new $sTableFromNamespace();

        }

        // нигде нет - ошибка
        // todo почемуб не перегенерить сущность и попробовать еще раз?
        if ( !class_exists( $sTableFromCache ) )
            throw new \Exception("Не найден Table класс для сущнсти [{$sModelName}]");

        return new $sTableFromCache();


    }

    /**
     * Получение класса для работы с сущностью
     * @param string $sModelName Имя сущности
     * @return \skewer\build\Component\orm\MagicTable
     */
    static public function getMagicTable( $sModelName ) {

        $oModel = self::get( $sModelName );

        // todo запроксировать объекты через хранилище

        $oTable = MagicTable::init( $oModel );

        return $oTable;
    }

}
