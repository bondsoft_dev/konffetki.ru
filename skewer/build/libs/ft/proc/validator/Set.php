<?php

namespace skewer\build\libs\ft\proc\validator;


class Set extends Prototype {

    /**
     * Проверяет данные на соответствие условиям
     * @return bool
     */
    function isValid() {
        return (bool)$this->getValue();
    }

    /**
     * Отдает текст ошибки
     * @return string
     */
    function getErrorText() {
        return \Yii::t('ft', 'error_validator_set');
    }

}