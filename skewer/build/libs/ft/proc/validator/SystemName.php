<?php

namespace skewer\build\libs\ft\proc\validator;


class SystemName extends Prototype {

    /**
     * Проверяет данные на соответствие условиям
     * @return bool
     */
    function isValid() {
        return preg_match('/^[a-z0-9_]*$/i', $this->getValue());
    }

    /**
     * Отдает текст ошибки
     * @return string
     */
    function getErrorText() {
        return \Yii::t('ft', 'error_validator_sys_name');
    }

}