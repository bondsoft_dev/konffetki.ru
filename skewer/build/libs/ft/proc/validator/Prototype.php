<?php

namespace skewer\build\libs\ft\proc\validator;

use skewer\build\libs\ft;
/**
 * Прототип валидатора
 */
abstract class Prototype extends ft\proc\Prototype {

    /**
     * Проверяет данные на соответствие условиям
     * @return bool
     */
    abstract function isValid();

    /**
     * Отдает текст ошибки
     * @return string
     */
    abstract function getErrorText();

}