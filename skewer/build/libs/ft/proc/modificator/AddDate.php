<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 20.03.14
 * Time: 15:38
 */

namespace skewer\build\libs\ft\proc\modificator;

/**
 * задает дату при добавлении
 * Class AddDate
 * @package skewer\build\libs\ft\proc\modificator
 */
class AddDate extends Prototype {

    public function beforeAdd() {
        $this->setValue( date("Y-m-d H:i:s") );
    }

} 