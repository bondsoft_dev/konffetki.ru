<?php

namespace skewer\build\libs\ft\proc\modificator;

/**
 * Протопип обновляющего модификатора
 * Вызывается метод modify при обновлении и добавлении записи
 * Class BoolAsInt
 * @package skewer\build\libs\ft\proc\modificator
 */
abstract class OnUpdPrototype extends Prototype {

    /**
     * Заменяет текущее значение
     * @param mixed $sIn
     * @return mixed
     */
    abstract protected function modify( $sIn );

    public function beforeAdd() {
        $this->setValue( $this->modify( $this->getValue() ) );
    }

    public function beforeUpd() {
        $this->setValue( $this->modify( $this->getValue() ) );
    }

}