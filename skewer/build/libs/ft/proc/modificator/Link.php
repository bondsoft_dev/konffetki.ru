<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 18.03.14
 * Time: 18:06
 */

namespace skewer\build\libs\ft\proc\modificator;

/**
 * Модификатор для правильного преобразования ссылок
 * Class Int
 * @package skewer\build\libs\ft\proc\modificator
 */
class Link extends OnUpdPrototype {

    protected function modify( $sVal ) {

        if( $sVal and
            (strpos($sVal,'http') === false) and
            !in_array( $sVal[0], array('/', '[') )  )
        {
            $sVal = 'http://' . $sVal;
            return $sVal;
        }

        return $sVal;

    }

}