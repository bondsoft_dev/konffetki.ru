<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 18.03.14
 * Time: 18:06
 */

namespace skewer\build\libs\ft\proc\modificator;

/**
 * Приводит значение к типу float
 * Class Int
 * @package skewer\build\libs\ft\proc\modificator
 */
class Float extends OnUpdPrototype {

    protected function modify( $sIn ) {
        return (float)$sIn ;
    }

}