<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 18.03.14
 * Time: 18:06
 */

namespace skewer\build\libs\ft\proc\modificator;

/**
 * Приводит значение к типу int
 * Class Int
 * @package skewer\build\libs\ft\proc\modificator
 */
class Int extends OnUpdPrototype {

    protected function modify( $sIn ) {
        return (int)$sIn;
    }

}