<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 18.03.14
 * Time: 18:06
 */

namespace skewer\build\libs\ft\proc\modificator;

use skewer\build\libs\ft;

abstract class Prototype extends ft\proc\Prototype {

    /**
     * Задает значение для поля
     * @param mixed $mVal
     * @return mixed
     */
    protected function setValue( $mVal ) {
        $sFieldName = $this->oField->getName();
        $this->oRow->$sFieldName = $mVal;
    }

    /**
     * Метод вызываемый перед добавлением
     * Значение поля может быть перекрыто методом setValue
     * @return void
     */
    public function beforeAdd() {
    }

    /**
     * Метод вызываемый после добавления
     * @return void
     */
    public function afterAdd() {
    }

    /**
     * Метод вызываемый перед обновлением
     * Значение поля может быть перекрыто методом setValue
     * @return void
     */
    public function beforeUpd() {
    }

    /**
     * Метод вызываемый после обновления
     * @return void
     */
    public function afterUpd() {
    }

    /**
     * Метод вызываемый перед удалением
     * @return void
     */
    public function beforeDel() {
    }

    /**
     * Метод вызываемый после удаления
     * @return void
     */
    public function afterDel() {
    }

}