<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 18.03.14
 * Time: 18:06
 */

namespace skewer\build\libs\ft\proc\modificator;

/**
 * Сохраняет дату обновления в поле
 * Class Int
 * @package skewer\build\libs\ft\proc\modificator
 */
class UpdDate extends OnUpdPrototype {

    protected function modify( $sIn ) {
        return date("Y-m-d H:i:s");
    }

}