<?php

namespace skewer\build\libs\ft\proc\modificator;

use skewer\build\libs\ft;

/**
 * Обновляется дата при изменении заданного поля
 * Class BoolAsInt
 * @package skewer\build\libs\ft\proc\modificator
 * todo не проверено
 */
abstract class FieldUpdDate extends Prototype {

    public function beforeAdd() {
        $this->setValue( date("Y-m-d H:i:s") );
    }

    public function beforeUpd() {

        // определение целевого поля
        $sField = $this->getParam('field');
        if ( !$sField ) return;

        // ноовое значение
        $iNewValue = $this->getValue();

        // запросить старое значение
        $oOldVal = ft\Cache::getModelTable( $this->oRow->getModel()->getName() )
            ->getRowById( $this->oRow->getPrimaryKeyValue() )
        ;

        if ( $oOldVal ) {

            if ( $oOldVal->$sField !== $iNewValue )
                $this->setValue( date("Y-m-d H:i:s") );

        }

    }

}