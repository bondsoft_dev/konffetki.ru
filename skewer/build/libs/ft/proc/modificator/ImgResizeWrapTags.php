<?php

namespace skewer\build\libs\ft\proc\modificator;

class ImgResizeWrapTags extends OnUpdPrototype {

    protected function modify( $sIn ) {
        return \ImageResize::wrapTags( $sIn );
    }

}