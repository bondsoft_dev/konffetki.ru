<?php

namespace skewer\build\libs\ft\proc\modificator;

/**
 * Приводт к занчению 0 или 1
 * Class BoolAsInt
 * @package skewer\build\libs\ft\proc\modificator
 */
class BoolAsInt extends OnUpdPrototype {

    protected function modify( $sIn ) {
        return $sIn ? 1 : 0 ;
    }

}