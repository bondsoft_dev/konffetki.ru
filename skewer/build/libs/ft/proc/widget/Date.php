<?php

namespace skewer\build\libs\ft\proc\widget;

/**
 * Виджет "Дата"
 * Class Date
 * @package skewer\build\libs\ft\proc\widget
 */
class Date extends Datetime {

    protected function getDefaultFormat() {
        return 'd.m.Y';
    }

} 