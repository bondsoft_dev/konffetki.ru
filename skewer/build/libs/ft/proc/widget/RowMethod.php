<?php

namespace skewer\build\libs\ft\proc\widget;

/**
 * Выводит данные по имени метода
 * Class RowMethod
 * @package skewer\build\libs\ft\proc\widget
 */
class RowMethod extends Prototype {

    /**
     * Модифицирует значение
     * @param mixed $sVal
     * @return mixed
     */
    public function modify($sVal) {

        $sMethodName = $this->getParam(0,'');

        if ( $sMethodName and $this->oRow and method_exists( $this->oRow, $sMethodName ) ) {

            $mVals = $this->$sMethodName();

            if ( is_array($mVals) )
                return isset( $mVals[$this->getValue()] ) ? $mVals[$this->getValue()] : '';

            return $mVals;

        }

        return '';

    }

}