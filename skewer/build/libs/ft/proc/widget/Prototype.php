<?php

namespace skewer\build\libs\ft\proc\widget;

use skewer\build\libs\ft;

abstract class Prototype extends ft\proc\Prototype {

    /**
     * Режим работы ft\Widget::mode = List / Show / Edit
     * @var int
     */
    protected $iMode = 0;

    /**
     * Модифицирует значение
     * @param mixed $sVal
     * @return mixed
     */
    abstract public function modify( $sVal );

    /**
     * Флаг вывода в списковом интерфейсе
     * @return bool
     */
    public function isListMode() {
        return $this->getMode()===ft\Widget::modeList;
    }

    /**
     * Флаг вывода в режиме редактирования
     * @return bool
     */
    public function isEditMode() {
        return $this->getMode()===ft\Widget::modeEdit;
    }

    /**
     * Флаг вывода в режиме отображения формы (без редактирования)
     * @return bool
     */
    public function isShowMode() {
        return $this->getMode()===ft\Widget::modeShow;
    }

    /**
     * Задает режим работы
     * @return int
     */
    public function getMode() {
        return $this->iMode;
    }

    /**
     * Отдает режим работы
     * @param int $iMode
     */
    public function setMode( $iMode ) {
        $this->iMode = $iMode;
    }

    /**
     * Флаг использования при использовании в детальной
     * По умолчанию - true
     * @return bool
     */
    public function useInShow() {
        return (bool)$this->getParam( ft\Widget::useInShow, true );
    }

    /**
     * Флаг использования при использовании в списке
     * По умолчанию - false
     * @return bool
     */
    public function useInList() {
        return (bool)$this->getParam( ft\Widget::useInList, false );
    }

    /**
     * Флаг использования при использовании в форме редактирования
     * По умолчанию - false
     * @return bool
     */
    public function useInEdit() {
        return (bool)$this->getParam( ft\Widget::useInEdit, false );
    }

}