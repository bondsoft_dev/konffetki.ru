<?php
/**
 * Created by PhpStorm.
 * User: User
 * Date: 27.03.14
 * Time: 12:11
 */

namespace skewer\build\libs\ft\proc\widget;

class ImgResizeRestoreTags extends Prototype {

    public function useInShow() {
        return false;
    }

    public function useInEdit() {
        return true;
    }

    /**
     * Модифицирует значение
     * @param mixed $sVal
     * @return mixed
     */
    public function modify( $sVal ) {
        return \ImageResize::restoreTags( $sVal );
    }

}
