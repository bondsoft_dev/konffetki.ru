<?php

namespace skewer\build\libs\ft\proc\widget;

class Datetime extends Prototype {

    /**
     * Отдает формат для отображения
     */
    protected function getDefaultFormat() {
        return 'd.m.Y H:i';
    }

    final protected function getFormat() {
        return $this->getDefaultFormat();
    }

    /**
     * Модифицирует значение
     * @param mixed $sVal
     * @return mixed
     */
    public function modify( $sVal ) {

        // перевести в объект "дата"
        $oDate = \DateTime::createFromFormat( 'Y-m-d H:i:s', $sVal );

        // собрать строку по заданному формату
        $sVal = $oDate ? $oDate->format( $this->getFormat() ) : '';

        return $sVal;

    }

}