<?php

namespace skewer\build\libs\ft\proc\widget;

/**
 * Виджет "Время"
 * Class Date
 * @package skewer\build\libs\ft\proc\widget
 */
class Time extends Datetime {

    protected function getDefaultFormat() {
        return 'H:i:s';
    }

} 