/**
 * Поле для выбора файла
 */
Ext.define('Ext.sk.field.GallerySelector', {
    extend:'Ext.form.field.Picker',//
    alias: ['widget.selectgalleryfield'],
    uses: ['Ext.button.Button'],//, 'Ext.layout.component.field.File'

    // Текст на кнопке выбора
    buttonText: dict.galleryBrowserSelect,

    // Отступ кнопки
    buttonMargin: 3,

    // Флаг "Только чтение"
    readOnly: true,
    hideTrigger: true,
    selectMode: '',

    // компонент вывода
    componentLayout: 'filefield',//buttonfield filefield

    // при генерации
    onRender: function() {
        var me = this;

        // вызвать родительский обработчик для создания поля ввода
        me.callParent(arguments);

        // создать элементы
        me.createButton();

        // деактивация, если нужна
        if (me.disabled) {
            me.disableItems();
        }

    },

    // создает кнопку
    createButton: function() {
        var me = this;
        /** @namespace me.buttonConfig */
        /** @namespace me.bodyEl */
        me.button = Ext.widget('button', Ext.apply({
            ui: me.ui,
            fieldCont: me,
            selectMode: me.selectMode,
            renderTo: me.bodyEl,
            text: me.buttonText,
            cls: Ext.baseCSSPrefix + 'form-file-btn',
            preventDefault: false,
            style: 'margin-left:' + me.buttonMargin + 'px',
            listeners: { click: me.onButtonClick }
        }, me.buttonConfig))

    },

    // при нажатии кнопки выбора
    onButtonClick: function() {

        processManager.fireEvent( 'edit_gallery', {
            scope: this.fieldCont,
            mode: this.selectMode,
            fnc: 'onGallerySelect'
        } );

    },

    // при выборе файла
    onGallerySelect: function ( value ) {
        this.setValue( value );
        this.triggerBlur();

    },

    // при отключении
    onDisable: function(){
        this.callParent();
        this.disableItems();
    },

    // отключение подчиненных элементов
    disableItems: function(){
        var button = this.button;
        if (button) {
            button.disable();
        }
    },

    // при вкдючении
    onEnable: function(){
        var me = this;
        me.callParent();
        me.button.enable();
    },

    // при удалении элемента
    onDestroy: function(){
        Ext.destroyMembers(this, 'button');
        this.callParent();
    }

});
