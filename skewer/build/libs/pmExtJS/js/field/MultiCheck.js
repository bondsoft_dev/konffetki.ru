/**
 * ComboBox с возможностью выбора нескольких элементов
 */
Ext.define('Ext.sk.field.MultiCheck', {
    alias: ['widget.multicheckfield'],
    extend: 'Ext.form.field.ComboBox',
    mode: 'local',
    triggerAction: 'all',
    forceSelection: false,
    allowBlank: true,
    editable: false,
    displayField: 'title',
    valueField:'type_id',
    queryMode: 'local',

    multiSelect: true,

    store: null,

    initComponent: function() {
        var me = this;

        me.callParent();
    },

    getValue: function() {
        return ( ( this.value instanceof Array ) ? this.value.join ( ',' ) : this.value );
    }

});