<?php
/**
 * Кнопка добаления
 */
class ExtDockedAddBtn extends ExtDockedPrototype {

    /**
     * Кнопка добавления
     * @return ExtDockedAddBtn
     */
    public static function create() {
        $oDocked = new ExtDockedAddBtn();
        $oDocked->setTitle( \Yii::t('adm','add') );
        $oDocked->setAction('addForm');
        $oDocked->setState('save');
        $oDocked->setIconCls( ExtDocked::iconAdd );
        return $oDocked;
    }

}
