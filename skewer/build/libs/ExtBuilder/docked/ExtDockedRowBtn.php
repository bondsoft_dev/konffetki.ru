<?php
/**
 * Кнопка для спискового интерфейса в строке
 * todo пересобрать прототип, чтобы были только требуемые методы
 */

class ExtDockedRowBtn extends ExtDockedPrototype {

    /**
     * Кнопка добавления
     * @return ExtDockedRowBtn
     */
    public static function create() {
        return new ExtDockedRowBtn();
    }

}