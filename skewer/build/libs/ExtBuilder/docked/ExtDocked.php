<?php
/**
 * Класс для работы с общими кнопками интерфейса (добавить / удалить / ...)
 */
class ExtDocked extends ExtDockedPrototype {

    /*
     * Набор иконок
     */
    const iconAdd = 'icon-add';
    const iconDel = 'icon-delete';
    const iconEdit = 'icon-edit';
    const iconSave = 'icon-save';
    const iconNext = 'icon-next';
    const iconCancel = 'icon-cancel';
    const iconInstall = 'icon-install';
    const iconReinstall = 'icon-reinstall';
    const iconReload = 'icon-reload';
    const iconConfiguration = 'icon-configuration';
    const iconLanguages = 'icon-languages';

    /**
     *
     * @param string $sTitle подпись
     * @return \ExtDocked
     */
    public static function create( $sTitle ) {
        $oDocked = new ExtDocked();
        $oDocked->setTitle( $sTitle );
        return $oDocked;
    }

}
