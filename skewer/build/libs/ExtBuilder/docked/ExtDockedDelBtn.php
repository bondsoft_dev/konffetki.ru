<?php
/**
 * Кнопка удаления
 */
class ExtDockedDelBtn extends ExtDockedPrototype {

    /**
     * Кнопка удаления
     * @return ExtDockedAddBtn
     */
    public static function create() {
        $oDocked = new ExtDockedDelBtn();
        $oDocked->setTitle( \Yii::t('adm','del') );
        $oDocked->setAction('delete');
        $oDocked->setState('delete');
        $oDocked->setIconCls( ExtDocked::iconDel );
        return $oDocked;
    }

}
