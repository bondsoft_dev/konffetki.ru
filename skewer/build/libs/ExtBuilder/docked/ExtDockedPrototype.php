<?php
/**
 * Класс прототип кнопок интерфейса
 * deprecated может быть замещено skewer\build\Component\UI\Element\Button
 */
abstract class ExtDockedPrototype {

    /** @var string подпись кнопки */
    protected $sTitle = '';

    /** @var string состояние в php, вызываемое по нажатию */
    protected $sAction = '';

    /** @var string состояние в js, вызываемое по нажатию */
    protected $sState = '';

    /** @var string иконка */
    protected $sIconCls = '';

    /** @var string подтверждение */
    protected $sConfirm = '';

    /** @var array доп параметры */
    protected $aAddParam = array();

    /** @var bool флаг проверки наличия изменений в форме */
    protected $bUseDirtyChecker = true;

    /**
     * Закрытый конструктор
     */
    protected function __construct() {}

    /**
     * Отдает инициализационный массив
     * @param ExtPrototype $oExtInterface
     * @return array
     */
    public function getInitArray( ExtPrototype $oExtInterface = null ) {
        return array(
            'text' => $this->getTitle(),
            'iconCls' => $this->getIconCls(),
            'state' => $this->getState(),
            'action' => $this->getAction(),
            'addParams' => $this->getAddParamList(),
            'confirmText' => $this->getConfirm(),
            'unsetFormDirtyBlocker' => !$this->getDirtyChecker()
        );
    }

    /**
     * Возвращает подпись кнопки
     * @return string
     */
    public function getTitle() {
        return $this->sTitle;
    }

    /**
     * Задает подпись кнопки
     * @param string $sTitle
     * @return \ExtDockedPrototype
     */
    public function setTitle( $sTitle ) {
        $this->sTitle = $sTitle;
        return $this;
    }

    /**
     * Отдает php состояние
     * @return string
     */
    public function getAction() {
        return $this->sAction;
    }

    /**
     * Задает php состояние
     * @param string $sAction
     * @return \ExtDockedPrototype
     */
    public function setAction( $sAction ) {
        $this->sAction = $sAction;
        return $this;
    }

    /**
     * Отдает js состояние
     * @return string
     */
    public function getState() {
        return $this->sState;
    }

    /**
     * Задает js состояние
     * @param string $sState
     * @return \ExtDockedPrototype
     */
    public function setState( $sState ) {
        $this->sState = $sState;
        return $this;
    }

    /**
     * Отдает иконку
     * @return string
     */
    public function getIconCls() {
        return $this->sIconCls;
    }

    /**
     * Задает иконку
     * @param string $sIconCls
     * @return \ExtDockedPrototype
     */
    public function setIconCls( $sIconCls ) {
        $this->sIconCls = $sIconCls;
        return $this;
    }


    /**
     * Возвращает доп параметры
     * @return array
     */
    public function getAddParamList() {
        return $this->aAddParam;
    }

    /**
     * Задает набор доп параметры
     * @param array $aList
     * @return \ExtDockedPrototype
     */
    public function setAddParamList( $aList ) {
        $this->aAddParam = $aList;
        return $this;
    }

    /**
     * Возвращает доп параметры
     * @param $sParamName
     * @return array
     */
    public function getAddParam($sParamName) {
        return isSet($this->aAddParam[$sParamName]) ? $this->aAddParam[$sParamName] : false;
    }

    /**
     * Задает подпись кнопки
     * @param $sParamName
     * @param $sValue
     * @return \ExtDockedPrototype
     */
    public function setAddParam( $sParamName, $sValue ) {
        $this->aAddParam[$sParamName] = $sValue;
        return $this;
    }

    /**
     * Запрос подтверждения
     * @return string
     */
    public function getConfirm() {
        return $this->sConfirm;
    }

    /**
     * Установка подтверждения
     * @param string $sConfirm
     * @return \ExtDockedPrototype
     */
    public function setConfirm( $sConfirm ) {
        $this->sConfirm = $sConfirm;
        return $this;
    }

    /*
     * Проверка наличия изменений в форме
     */

    /**
     * Отдает статус наличия проверки изменений в форме
     * @return bool
     */
    public function getDirtyChecker() {
        return $this->bUseDirtyChecker;
    }

    /**
     * Устанавливает флаг проверки изменений в форме
     * @param bool $bVal значение
     * @return \ExtDockedPrototype
     */
    public function setDirtyChecker( $bVal=true ) {
        $this->bUseDirtyChecker = (bool)$bVal;
        return $this;
    }

    /**
     * Снимает флаг проверки изменений в форме
     * @return \ExtDockedPrototype
     */
    public function unsetDirtyChecker() {
        $this->bUseDirtyChecker = false;
        return $this;
    }


}
