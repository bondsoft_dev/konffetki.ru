<?php
/**
 * Created by JetBrains PhpStorm.
 * User: user
 * Date: 26.07.13
 * Time: 15:34
 * To change this template use File | Settings | File Templates.
 */

class ExtDockedBtn extends ExtDockedPrototype {

    /**
     * Кнопка добавления
     * @return ExtDockedBtn
     */
    public static function create() {
        return new ExtDockedBtn();
    }

}