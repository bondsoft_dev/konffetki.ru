<?php

namespace skewer\build\libs\ExtBuilder\Field;

/**
 * Редактор текста CKEditor
 */
class Wyswyg extends Prototype {

    const addConfigParamName = 'addConfig';

    public function getView() {
        return 'wyswyg';
    }

    /**
     * Задает класс для контента
     * @param $sClass
     */
    public function setContentHtmlClass( $sClass ) {
        $this->setAddConfigVar( 'bodyClass', $sClass );
    }

    /**
     * Добавляет дополнительные языковые данные
     * @param $aParams
     */
    public function setAddLangParams( $aParams ){
        $this->setAddConfigVar( 'addLangParams', $aParams );
    }

    /**
     * Задает высоту блока при отображении
     * @param $sClass
     */
    public function setHeight( $sClass ) {
        $this->setDescVal( 'height', $sClass );
    }

    /**
     * Задает набор css файлов для отображения в клиентской части
     * @param array $aFiels
     */
    public function setCssFiles( $aFiels ) {
        $this->setAddConfigVar( 'contentsCss', $aFiels );
    }

    /**
     * Задает параметр дополнительного конфига
     * @param string $sName
     * @param mixed $mVal
     */
    private function setAddConfigVar( $sName, $mVal ) {
        $aAddConfig = $this->getDescVal( self::addConfigParamName, array() );
        $aAddConfig[$sName] = $mVal;
        $this->setDescVal( self::addConfigParamName, $aAddConfig );
    }

}