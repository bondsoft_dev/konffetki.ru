<?php

namespace skewer\build\libs\ExtBuilder\Field;

/**
 * Редактор "дробное число"
 */
class Float extends Prototype {

    function getView() {
        return 'float';
    }

}