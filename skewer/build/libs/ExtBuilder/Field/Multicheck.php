<?php

namespace skewer\build\libs\ExtBuilder\Field;

/**
 * Формирование контента для ComboBox-a с multiselect-ом
 * Class Multicheck
 */
class Multicheck extends Prototype {

    /**
     * Отдает описание элемента для формы
     * @return array
     */
    final function getFormFieldDesc() {

        // если данные уже есть - можно не искать
        $a = $this->aDesc;
        if ( isset($a['store']) and isset($a['store']['data']) and $a['store']['data'] )
            return array();

        // формирование дополнительных данных
        $aData = \ExtForm::getDesc4SelectFromArray( $this->getList() );
        unSet( $aData['view'] );

        return $aData;
    }


    /**
     * Возвращает список доступных значений в формате value => title
     * @return array
     */
    protected function getList() {

        // если есть описание поля
        if ( $this->oField ) {

            /** @var array $aParams набор параметров */
            return $aParams = $this->getParams();
        }

        return array();
    }

    function getView() {
        return 'multicheck';
    }

}