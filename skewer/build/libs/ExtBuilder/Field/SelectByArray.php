<?php

namespace skewer\build\libs\ExtBuilder\Field;


/**
 * Класс для создания списковых элементов в автопостроителе из переданного массива
 */
class SelectByArray extends SelectPrototype {

    /**
     * Создает запись
     * @param array $aList
     * @param mixed $mDefault
     * @throws \Exception
     */
    public function __construct( $aList = null, $mDefault=null ) {

        // переустановить список, если задан
        if ( is_array($aList) ) {
            $this->aList = $aList;
        }

        // задать стандартный элемент
        if ( !is_null($mDefault) )
            $this->mDefault = $mDefault;

        if ( !is_array($this->aList) )
            throw new \Exception('ExtBuilder\Field\SelectByArray has no array list.');


    }

    /** @var array набор доступных значений */
    protected $aList = null;

    /** @var mixed стандартное значение */
    protected $mDefault = null;

    /**
     * Значение по умолчанию
     * @return int
     */
    public function getDefaultVal() {
        if ( !is_null($this->mDefault) )
            return $this->mDefault;
        else
            return parent::getDefaultVal();
    }

    /**
     * Возвращает список доступных значений в формате value => title
     * @return array
     */
    function getList() {
        return $this->aList;
    }

    /**
     * Проверяет доступность заданного значения
     * @return bool
     */
    function isValid() {
        return isset($this->aList[ $this->getValue() ]);
    }

    /**
     * Отдает текстовое название для установленного значения
     * @return string
     */
    function getText() {
        return $this->isValid() ? $this->aList[ $this->getValue() ] : '-не определено-';
    }

    function getView() {
        return 'select';
    }

}
