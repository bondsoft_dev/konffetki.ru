<?php

namespace skewer\build\libs\ExtBuilder\Field;

/**
 * Редактор "Упрощенный html редактор"
 */
class Html extends Text {

    /**
     * Отдает название типа отображения
     * @return string
     */
    function getView() {
        return 'html';
    }

}