<?php

namespace skewer\build\libs\ExtBuilder\Field;

/**
 * Класс для работы с полями автопостроителя
 * todo выпилить и заменить на набор объектов
 */
class ByArray extends Prototype {

    /**
     * Конструктор для первичной инициализации в момент создания
     * @param array $aBaseDesc
     * @param array $aAddDesc
     */
    public function __construct( array $aBaseDesc=null, array $aAddDesc=null ) {

        // если есть базовое описание
        if ( !is_null($aBaseDesc) ) {

            // инициализировать базовое описание
            $this->setBaseDesc( $aBaseDesc );

            // если есть дополнительные параметры
            if ( !is_null($aAddDesc) ) {

                // инициализировать дополнительные параметры
                $this->setAddDesc( $aAddDesc );

            }

        }

    }

    public function getView() {
        return (string)$this->getDescVal('view');
    }

}
