<?php

namespace skewer\build\libs\ExtBuilder\Field;

/**
 * Редактор "строка"
 */
class String extends Prototype {

    function getView() {
        return 'str';
    }

}