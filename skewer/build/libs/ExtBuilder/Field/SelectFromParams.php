<?php

namespace skewer\build\libs\ExtBuilder\Field;

/**
 * Редактор "Выпадающий список"
 * todo не нашел где используется
 */
class SelectFromParams extends SelectPrototype {

    /**
     * Возвращает список доступных значений в формате value => title
     * @return array
     */
    protected function getList() {
        return $this->getParams();
    }

}