<?php

namespace skewer\build\libs\ExtBuilder\Field;

/**
 * Редактор "Ссылка"
 */
class Link extends Prototype {

    function getView() {
        return 'show';
    }

    /**
     * Текст для ссылки
     * и ссылка, если она еще не была задана
     * @param mixed $sText
     * @param $sHref
     * @return mixed|void
     */
    public function setLink( $sText, $sHref ) {
        parent::setValue(sprintf('<a href="%s" target="_blank">%s</a>',$sHref,$sText));
    }

}
