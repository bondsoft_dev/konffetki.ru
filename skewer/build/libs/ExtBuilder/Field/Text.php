<?php

namespace skewer\build\libs\ExtBuilder\Field;

/**
 * Редактор "текст"
 */
class Text extends Prototype {

    /**
     * Отдает название типа отображения
     * @return string
     */
    function getView() {
        return 'text';
    }

    /**
     * Задает высоту блока при отображении
     * @param $sClass
     */
    public function setHeight( $sClass ) {
        $this->setDescVal( 'height', $sClass );
    }

}