<?php

namespace skewer\build\libs\ExtBuilder\Field;

use skewer\build\libs\ft;

/**
 * Редактор "Выпадающий список"
 */
class Select extends SelectPrototype {

    /** @var null|array набор значений */
    protected $aList = null;

    /**
     * Задает набор знеачений
     */
    public function setList( $aList ) {
        $this->aList = $aList;
    }

    /**
     * Возвращает список доступных значений в формате value => title
     * @throws \skewer\build\libs\ft\exception\Inner
     * @throws \skewer\build\libs\ft\exception\Model
     * @return array
     */
    protected function getList() {

        if ( !is_null($this->aList) )
            return $this->aList;

        // если есть описание поля
        if ( $this->oField ) {

            /** @var array $aParams набор параметров */
            $aParams = $this->getParams();

            if ( isset($aParams['list']) )
                return $aParams['list'];

            if ( isset($aParams['rowMethod']) and $aParams['rowMethod'] ) {

                if ( !$this->oRow )
                    throw new ft\exception\Inner( "Для интерфейса поля не задана запись" );

                $sMethodName = $aParams['rowMethod'];
                if ( method_exists( $this->oRow, $aParams['rowMethod'] ) )
                    return $this->oRow->$sMethodName();
                else
                    throw new ft\exception\Model( "Метод запроса списка [$sMethodName] не найден." );

            }

        }

        // если есть описание модели
        if ( $this->oModel ) {
            /** @var ft\Relation $oRelation связь поля */
            $oRelation = $this->oModel->getOneFieldRelation( $this->getName() );
            if ( $oRelation ) {

                /** @var ft\Model $oRelModel внешняя модель*/
                $oRelModel = $oRelation->getModel();

                $oQuery = ft\Cache::getModelTable( $oRelModel->getName(), $oRelModel->getNamespace() );

                $aList = $oQuery->select();
                $aOut = array();
                foreach ( $aList as $oItem ) {
                    $sPk = $oRelation->getExternalFieldName();
                    if ( !isset($oItem->$sPk) )
                        throw new ft\exception\Model("Нет в ответе внешней сущности  поля для связи [$sPk]");
                    $mKey = $oItem->$sPk;
                    $aOut[$mKey] = (string)$oItem;
                }
                return $aOut;

            }
        }

        return $this->oField ? $this->getParams() : array();

    }

    /**
     * Отдает название типа отображения
     * @return string
     */
    function getView() {
        return 'select';
    }

}