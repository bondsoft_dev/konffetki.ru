<?php

namespace skewer\build\libs\ExtBuilder\Field;

/**
 * Редактор "Число"
 */
class Int extends Prototype {

    function getView() {
        return 'num';
    }

} 