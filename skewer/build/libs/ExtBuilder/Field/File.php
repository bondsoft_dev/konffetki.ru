<?php

namespace skewer\build\libs\ExtBuilder\Field;

/**
 * Поле типа "Файл"
 */
class File extends Prototype {

    function getView() {
        return 'file';
    }

}