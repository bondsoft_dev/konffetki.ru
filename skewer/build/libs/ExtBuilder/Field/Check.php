<?php

namespace skewer\build\libs\ExtBuilder\Field;

/**
 * Редактор для поля "галочка"
 */
class Check extends Prototype {

    public function getView() {
        return 'check';
    }

} 