<?php

namespace skewer\build\libs\ExtBuilder\Field;

/**
 * Класс для редактирования поля типа "ДатаВремя"
 */
class Datetime extends Prototype {

    const listFormat = 'd.m.Y H:i';

    /**
     * Отдает формат для отображения списка
     * @return string
     */
    protected function getListFormat() {
        return self::listFormat;
    }

    public function getListShowValue() {

        // взять текущее значение
        $sVal = parent::getListShowValue();

        // перевести в объект "дата"
        $oDate = \DateTime::createFromFormat( 'Y-m-d H:i:s', $sVal );

        // собрать строку по заданному формату
        $sVal = $oDate ? $oDate->format( $this->getListFormat() ) : '';

        return $sVal;
    }

    function getView() {
        return 'datetime';
    }

} 