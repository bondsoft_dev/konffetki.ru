<?php

namespace skewer\build\libs\ExtBuilder\Field;

/**
 * Родительский класс для создания списковых элементов в автопостроителе
 */
abstract class SelectPrototype extends Prototype {

    /**
     * Возвращает список доступных значений в формате value => title
     * @abstract
     * @return array
     */
    abstract protected function getList();

    /**
     * Отдает описание элемента для формы
     * @return array
     */
    final function getFormFieldDesc() {

        // если данные уже есть - можно не искать
        $a = $this->aDesc;
        if ( isset($a['store']) and isset($a['store']['data']) and $a['store']['data'] )
            return array();

        // формирование дополнительных данных
        return \ExtForm::getDesc4SelectFromArray( $this->getList() );

    }

}
