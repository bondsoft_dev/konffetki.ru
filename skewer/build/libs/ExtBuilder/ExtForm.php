<?php

use skewer\build\libs\ft as ft;
use skewer\build\Component\orm;
use skewer\build\Component\UI;
use skewer\build\libs\ExtBuilder;
use skewer\build\Cms;

/**
 * Класс для автоматической сборки админских интерфейсов на ExtJS
 * Класс для построения форм
 *
 * @class: ExtForm
 * @project Skewer
 * @package Build
 *
 * @Author: sapozhkov
 * @version: $Revision$
 * @date: $Date$
 *
 */

class ExtForm extends ExtModelPrototype implements UI\State\EditInterface {

    const CheckSuffix = '__check__';

    /** @var bool флаг использования спец директории для изображений модуля */
    protected $bUseSpecSectionForImages = false;

    /** @var int id спец директории для загружаемых изображений */
    protected $iSpecSectionForImages = 0;

    /**
     * Имя состояния для сохранения
     * @var string
     */
    private $sSaveState = 'save';

    /** @var bool Отслеживать изменения */
    protected $bTrackChanges = true;

    /**
     * Устанавливает флаг использования спец директории для изображений модуля
     */
    public function useSpecSectionForImages( $iId=0 ) {
        $this->bUseSpecSectionForImages = true;
        $this->iSpecSectionForImages = (int)$iId;
    }

    /**
     * Общие Функции
     */

    /**
     * Возвращает имя компонента
     * @return string
     */
    public function getComponentName() {
        return 'Form';
    }

    /**
     * Запрос дополнительных полей для инициализации полей по ft модели
     * @param ft\model\Field $oField
     * @return array
     */
    protected function getAddParamsForFtField( ft\model\Field $oField ) {
        return array(
            'disabled' => false,
            'activeError' => $oField->getParameter('form_error')
        );
    }

    /**
     * Добавляет к текущей модели запись
     * @param ExtBuilder\Field\Prototype $oItem новая запись для модели
     * @return bool
     */
    public function addField( ExtBuilder\Field\Prototype $oItem ) {

        // проверка корректности описания
        if ( !$oItem->getName() ) {
            $this->error( 'Model create. Wrong input - no name field found.', $oItem->getDesc() );
            return false;
        }

        if ( !$oItem->getView() ) {
            $this->error( 'Model create. Wrong input - no view field found.', $oItem->getDesc() );
            return false;
        }

        return parent::addField( $oItem );

    }

    /**
     * Преобразует объект поля в пригодный для ExtJS массив
     * @param ExtBuilder\Field\Prototype $oField
     * @return array
     */
    public static function getFieldDesc( ExtBuilder\Field\Prototype $oField ) {

        // замена инициализационного массива перекрытым для обхекта
        $oField->setAddDesc( $oField->getFormFieldDesc() );

// todo разобрать и удалить
//        // типы для хранилища
//        switch ( $oItem->getView() ) {
//            default:
//            case 'string': +
//                $oItem->setDescVal('type','str');
//                break;
//            case 'int': +
//                $oItem->setDescVal('type','num');
//                break;
//            case 'float': +
//                $oItem->setDescVal('type','float');
//                break;
//            case 'multicheck':
//            case 'check': +
//            case 'file': +
//            case 'hide':
//            case 'text': +
//            case 'wyswyg': +
//            case 'html': +
//            case 'boolean':
//            case 'select': +
//            case 'date': +
//            case 'datetime': +
//            case 'time': +
//            case 'show': +
//            case 'pass':
//            case 'gallery':
//            case 'specific':
//            case 'multi':
//                $oItem->setDescVal('type',$oItem->getView());
//                break;
//        }

        $oField->setDescVal('type',$oField->getView());

        // убрать ненужное поле
        $oField->delDescVal('view');

        // значение поля - обязательное ( может быть false / 0 / null / ... )
        if ( !$oField->hasValue() )
            $oField->setValue('');

        // название - обязательное, если не указано иное
        if ( !$oField->getTitle() and $oField->getTitle()!==false )
            $oField->setTitle( $oField->getName() );

        // преобразование значения поля
        $sValue = $oField->getValue();
        foreach ( $oField->getWidgetList() as $oWidget ) {
            if ( $oWidget->useInEdit() )
                $sValue = $oWidget->modify( $sValue );
        }
        $oField->setValue($sValue);

        $aDesc = $oField->getDesc();

        return $aDesc;

    }

    /**
     * Устанавливает значения для набор элементов
     * @param array|orm\ActiveRecord $aValues - набор пар имя поля - значение
     */
    public function setValues($aValues) {

        // обойти весь пришедший массив
        foreach ( $aValues as $sFieldName => $mValue ) {

            // для найденных элементов установить значения
            if ( $this->hasField($sFieldName) ) {
                $this->aFields[$sFieldName]->setValue($mValue);
            }

        }

    }

    /**
     * Устанавливает значения по умолчанию
     */
    public function setDefaultValues(){

        foreach ( $this->getFields() as $oItem ) {
            $oItem->setValue( $oItem->getDefaultVal() );
        }

    }

    /**
     * Протокол Передачи Данных
     */

    /**
     * Собирает интерфейсный массив для выдачи в JS
     * @return array
     */
    public function getInterfaceArray() {

        // собираем массив описаний
        $aItems = array();
        foreach ( $this->getFields() as $oItem )
            $aItems[] = $this->getFieldDesc( $oItem );

        // выходной массив
        $aOut = array(
            'items' => $aItems,
            'saveStateName' => $this->getSaveState(),
            'trackChanges' => $this->getTrackChanges(),
            'barElements' => $this->getFilters(),
            'actionNameLoad' => $this->getPageLoadActionName(),
        );

        // вывод данных
        return $aOut;

    }

    /**
     * Возввращает массив инициализации специфического поля
     * По сути расширяет массив $aInitParams дополнительными параметрам, которые будут
     *      приняты js кодом и обработаны
     * @param string $sLibName - имя спец класса
     * @param array $aInitParams - параметры для передачи
     * @return array
     */
    public function getSpecificItemInitArray( $sLibName, $aInitParams=array() ){

        // добавить инициализацию библиотеки
        $this->addLibClass( $sLibName );

        // метка спец обработчика
        $aInitParams['view'] = 'specific';

        // имя для библиотеки наследования
        $aInitParams['extendLibName'] = $sLibName;

        return $aInitParams;

    }

    /**
     * Возвращает инициализационный массив для создания элемента типа select в ExtJS
     * Пример
     *  $aData =
     *      0
     *          value = 12
     *          title = декабрь
     *      1
     *          value = 2
     *          title = февраль
     *  $sValName = value
     *  $sTitleName = title
     * @static
     * @param array $aData - набор данных
     * @param string $sValName - имя переменной для сохранения из массива данных
     * @param string $sTitleName - имя переменной для отображения из массива данных
     * @return array
     */
    public static function getDesc4Select( $aData, $sValName, $sTitleName ){

        return array(
            'view' => 'select',
            'valueField' => $sValName,
            'displayField' => $sTitleName,
            'store' => array(
                'fields' => array( $sValName, $sTitleName ),
                'data' => array_values($aData)
            )
        );

    }

    /**
     * Возвращает инициализационный массив для создания набора элементов типа глочки в ExtJS
     * @static
     * @param array $aData пришедшие данные
     * @param string $sName имя элемента
     * @return array массив имен галочек
     * @deprecated вместе с getDesc4MultiSelect
     */
    public static function getMultiSelectValues( $aData, $sName ){

        $aOut = array();

        $sPrefix = sprintf( '%s%s', $sName, self::CheckSuffix );
        $iLen = strlen($sPrefix);

        foreach ( $aData as $sKey => $sVal ) {

            if ( strpos( $sKey, $sPrefix ) === 0 and $sVal )
                $aOut[] = substr($sKey,$iLen);

        }

        return $aOut;

    }

    /**
     * Возвращает инициализационный массив для создания набора элементов типа глочки в ExtJS
     * @static
     * @param array $aSet набор пар имя - название
     * @param array $aValues набор выбранных значений
     * @param string $sName
     * @param string $sTitle
     * @return array
     * @deprecated use skewer\build\Component\UI\Form\MultiCheck
     */
    public static function getDesc4MultiSelect( $aSet, $aValues, $sName, $sTitle ){

        $aOut = array();
        $bFirst = true;

        foreach ( $aSet as $sKey => $sVal ) {

            $aItem = array(
                'view' => 'check',
                'boxLabel' => $sVal,
                'value' => (string)in_array($sKey, $aValues),
                'name' => sprintf( '%s%s%s', $sName, self::CheckSuffix, $sKey ),
            );

            if ( $bFirst ) {
                $aItem['title'] = $sTitle;
            } else {
                $aItem['title'] = false;
                $aItem['hideEmptyLabel'] = false;
            }

            $aOut[] = $aItem;

            $bFirst = false;

        }

        return $aOut;

    }

    /**
     * Возвращает инициализационный массив для создания элемента типа select в ExtJS
     * На вход принимается одномерный массив пар значение => заголовок
     * @static
     * @param $aData
     * @return array
     */
    public static function getDesc4SelectFromArray( $aData ){

        $aData = self::markUniqueValue($aData);

        // сборка массива данных
        $aItems = array();
        foreach ( $aData as $sKey => $mItem ) {
            $aItems[] = array(
                'v' => $sKey,
                't' => $mItem
            );
        }

        // отдать массив с данными
        return static::getDesc4Select( $aItems, 'v', 't' );

    }

    /**
     * Помечает значения элементов массива, если есть одинаковые
     * @param array $aData
     * @return array
     */
    public static function markUniqueValue( $aData = array() ){

        // если есть дублируюшие элементы
        if (count(array_values($aData)) != count(array_values(array_unique($aData)))){
            $aResult = array();
            $aFound = [];
            foreach($aData as $k => $v){
                $val = $v;
                // если был такой элемент - дополнить
                if ( in_array($val, $aFound) ) {
                    $val = sprintf('%s [%s]', $v, $k);
                    // это на всякий пожарный )
                    if ( in_array($val, $aFound) )
                        $val = sprintf('%s [%s/%d]', $v, $k, rand(10000000, 99999999));
                }
                $aFound[] = $val;
                $aResult[$k] = $val;
            }
            return $aResult;
        }
        return $aData;

    }

    /**
     * Отдает описание для создания прописанного в js библиотеке поля
     * @static
     * @param string $sLibName
     * @param array $aAddData
     * @return array
     */
    public static function getDesc4CustomField( $sLibName, $aAddData=array() ) {

        return array_merge(array(
            'customField' => $sLibName
        ),$aAddData);

    }

    /**
     * Задает инициализационный  массив для атопостроителя интерфейсов
     * @param Cms\Frame\ModulePrototype $oModule - ссылка на вызвавший объект
     */
    public function setInterfaceData( Cms\Frame\ModulePrototype $oModule ) {

        // если есть спец флаг
        if ( $this->bUseSpecSectionForImages ) {
            // увязывание файлов для wyswyg в спец директорию
            foreach ( $this->aFields as $oField ) {
                // todo -> editor
                if ( $oField->getView() === 'wyswyg' ) {
                    $aAddConfig = $oField->getDescVal( 'addConfig', array() );
                    $aAddConfig['filebrowserBrowseUrl'] = $this->getFileBrowserUrl( $oModule );
                    $oField->setDescVal( 'addConfig', $aAddConfig );
                }
            }
        }

        // выполняем родительскую часть модуля
        parent::setInterfaceData( $oModule );

    }

    /**
     * Отдает ссылку на интерфейс загрузки файлов с автовыбором директории для модуля
     * @param Cms\Frame\ModulePrototype $oModule
     * @return string
     */
    public function getFileBrowserUrl( Cms\Frame\ModulePrototype $oModule ) {
        $sPattern = '/admin/?mode=fileBrowser&%s=%s&type=file&returnTo=ckeditor';
        if ( $this->iSpecSectionForImages )
            return sprintf( $sPattern, 'section', $this->iSpecSectionForImages );
        else {
            if ( $oModule->useNamespace() )
                $sModuleName = sprintf( '%s_%s', $oModule->getLayerName(), $oModule->getModuleName() );
            else
                $sModuleName = get_class( $oModule );
            return sprintf( $sPattern, 'module', $sModuleName );
        }
    }


    /**
     * Отдает набор полей для вывода по умолчанию
     * @return string
     */
    protected function getDefaultFieldsSet() {
        return '';
    }

    /**
     * Отдает состояние для сохранения
     * @return string
     */
    private function getSaveState() {
        return $this->sSaveState;
    }

    /**
     * задает имя состояния для сохранения
     */
    public function setSaveState( $sState ) {
        $this->sSaveState = $sState;
    }

    /**
     * @param boolean $bTrackChanges
     */
    public function setTrackChanges($bTrackChanges){
        $this->bTrackChanges = $bTrackChanges;
    }

    /**
     * @return boolean
     */
    public function getTrackChanges(){
        return $this->bTrackChanges;
    }

}
