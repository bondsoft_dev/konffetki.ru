<?php

use skewer\build\Cms;

/**
 * Класс для перегрузки нескольких строк в списоковом интерфейсе автопостроителя
 * Служит для изменения / добавления / удаления строк
 *
 * пока реализовано только изменение
 *
 * Class ExtListRows
 */
class ExtListRows {

    /** @var string|array имя поля для сравнения */
    private $mKeyField = '';

    /**
     * Набор строк для добавления / удаления
     * @var array
     */
    private $aUpdRowList = array();

    /**
     * добавляет нужные параметры к модулю (дополняет посылку на сервер)
     * @param Cms\Tabs\ModulePrototype $oModule
     */
    public function setData( Cms\Tabs\ModulePrototype $oModule) {

        $oModule->setData( 'cmd', 'loadItem' );

        $oModule->setData( 'items', $this->aUpdRowList );
        $oModule->setData( 'keyField', $this->mKeyField );

    }

    /**
     * Задет поле по которому будут сравниваться строки при обновлении / доб / удалении
     * @param string|array $mKeyField
     */
    public function setSearchField($mKeyField) {
        $this->mKeyField = $mKeyField;
    }

    /**
     * Добавляет набор (строку) данных для изменения / добавления
     * @param $aRow
     */
    public function addDataRow($aRow) {
        $this->aUpdRowList[] = $aRow;
    }

}