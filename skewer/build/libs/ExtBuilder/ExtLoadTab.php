<?php
use skewer\build\libs\ft;

/**
 * Объект для создания новой пусто вкладки
 */
class ExtLoadTab extends ExtPrototype {

    /**
     * Возвращает имя компонента
     * @return string
     */
    function getComponentName() {
        return '';
    }

    /**
     * Отдает интерфейсный массив для атопостроителя интерфейсов
     * @return array
     */
    function getInterfaceArray() {
        return array();
    }

    /**
     * Задает набор полей по FT модели
     * @param ft\Model $oModel описание модели
     * @param string $sColSet набор колонок для вывода
     * @return void
     */
    public function setFieldsByFtModel( ft\Model $oModel, $sColSet = '' ) {
        // TODO: Implement setFieldsByFtModel() method.
    }
}
