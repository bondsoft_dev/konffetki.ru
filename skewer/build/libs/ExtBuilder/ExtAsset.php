<?php

use yii\web\AssetBundle;

class ExtAsset extends AssetBundle {
    public $sourcePath = '@skewer/build/libs/ExtBuilder/web';

    public $depends = [
        'skewer\assets\ExtJsProcessManagerAsset'
    ];
}