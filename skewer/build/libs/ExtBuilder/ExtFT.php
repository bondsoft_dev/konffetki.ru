<?php

use skewer\build\libs\ft as ft;
use skewer\build\Component\UI;
use skewer\build\libs\ExtBuilder;

/**
 * Набор общих методов для завязки ft и ext автопостроителя
 */

class ExtFT {

    /**
     * Отдает букву типа для запроса по полю
     * @static
     * @param $oField
     * @return string ( s / i )
     */
    public static function getLetterType( ft\model\Field $oField ){
        switch ( $oField->getDatatype() ) {
            case 'tinyint':
            case 'int': return 'i';
            default: return 's';
        }
    }

    /**
     * Отдает возможное имя класса для редактора поля по имени редактора
     * todo перенести
     * @param $sEditorName
     * @return string
     */
    public static function getPosibleEditorClass( $sEditorName ) {
        return 'skewer\build\libs\ExtBuilder\Field\\'.\skTranslit::toCamelCase($sEditorName);
    }

    /**
     * Создает объект поля для вывода в интерфейсе
     * @param array $aParams
     * @param ft\model\Field $oField
     * @param ft\Model $oModel
     * @throws ft\exception\Model
     * @return ExtBuilder\Field\Prototype
     */
    public static function createFieldByFt( $aParams, ft\model\Field $oField, ft\Model $oModel ) {

        // предположительное имя класса обработчика
        $sClassName = self::getPosibleEditorClass($oField->getEditorName());

        // ищем класс
        if ( class_exists($sClassName) ) {
            $oIfaceField = new $sClassName();
        } else {

            // todo выпилить - заменить на объекты из списка self::getSimpleEditorList

            // проверить - возможно осуществима обработка по умолчанию
            if ( !in_array( $oField->getEditorName(), self::getSimpleEditorList() ) )
                throw new ft\exception\Model( sprintf('Нет редактора [%s] для поля [%s]',
                    $oField->getEditorName(),
                    $oField->getName()
                ) );

            $oIfaceField = new ExtBuilder\Field\ByArray();
        }

        if ( !$oIfaceField instanceof ExtBuilder\Field\Prototype )
            throw new ft\exception\Model( sprintf('Класс [%s] должен быть унаследован от [%s]',
                get_class($oIfaceField),
                'ExtBuilder\Field\Prototype'
            ));

        $oIfaceField->setBaseDesc( $aParams );
        $oIfaceField->setDescObj( $oField, $oModel );

        return $oIfaceField;

    }

    /**
     * Создает объект поля для вывода в интерфейсе
     * @param array $aParams
     * @param UI\Form\Field $oField
     * @throws ft\exception\Model
     * @return ExtBuilder\Field\Prototype
     */
    public static function createFieldByUi( $aParams, UI\Form\Field $oField ) {

        // предположительное имя класса обработчика
        $sClassName = self::getPosibleEditorClass($oField->getEditor());

        // ищем класс
        if ( class_exists($sClassName) ) {
            $oIfaceField = new $sClassName();
        } else {

            // проверить - возможно осуществима обработка по умолчанию
            if ( !in_array( $oField->getEditor(), self::getSimpleEditorList() ) )
                throw new ft\exception\Model( sprintf('Нет редактора [%s] для поля [%s]',
                    $oField->getEditor(),
                    $oField->getName()
                ) );

            $oIfaceField = new ExtBuilder\Field\ByArray();
        }

        if ( !$oIfaceField instanceof ExtBuilder\Field\Prototype )
            throw new ft\exception\Model( sprintf('Класс [%s] должен быть унаследован от [%s]',
                get_class($oIfaceField),
                'ExtBuilder\Field\Prototype'
            ));

        $oIfaceField->setBaseDesc( $aParams );

        return $oIfaceField;

    }

    /**
     * Отдает список простых редакторов, которые
     *  могут быть самостоятельно обработаны ExtJS
     *  без подключения дополнительных компонентов
     * @return string[]
     */
    protected static function getSimpleEditorList() {
        return array(
            'show',
            'string',
            'money',
            //'int',
            'float',
            'check',
            'button',
            'hide',
            'text',
            //'date',
            //'time',
            //'datetime',
            'html',
            //'wyswyg',
            //'multicheck',
        );
    }


}
