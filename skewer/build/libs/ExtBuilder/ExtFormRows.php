<?php
use skewer\build\Cms;

/**
 * Класс для перегрузки нескольких строк в форме
 */

class ExtFormRows {

    /**
     * Набор строк для добавления / удаления
     * @var array
     */
    private $aUpdRowList = array();

    /**
     * добавляет нужные параметры к модулю (дополняет посылку на сервер)
     * @param Cms\Tabs\ModulePrototype $oModule
     */
    public function setData( Cms\Tabs\ModulePrototype $oModule) {

        $oModule->setData( 'cmd', 'loadItem' );

        $oModule->setData( 'items', $this->aUpdRowList );

    }

    /**
     * Добавляет набор (строку) данных для изменения / добавления
     * @param string $sName имя поля
     * @param mixed $mVal значение поля
     */
    public function addDataRow($sName, $mVal) {
        $this->aUpdRowList[$sName] = $mVal;
    }

    /**
     * Перезадает значения для выпадающего списка
     * @param string $sName имя поля
     * @param string[] $aValList набор пар ключ-значение для выпадающего списка
     * @param null $mCurrentVal текущее значение
     * @param bool $bDisabled флаг деактивации поля
     */
    public function addFieldSelect($sName, $aValList, $mCurrentVal = null, $bDisabled = false) {

        $aData = array();
        foreach ( $aValList as $mKey => $sVal )
            $aData[] = array(
                'v' => $mKey,
                't' => $sVal
            );

        $aItems = array(
            'type' => 'combo',
            'data' => $aData,
            'disabled' => $bDisabled
        );

        if ( !is_null($mCurrentVal) )
            $aItems['value'] = $mCurrentVal;

        $this->aUpdRowList[$sName] = $aItems;

    }

    /**
     *
     * @param $sName
     * @param $mCurrentVal
     * @param bool $bDisabled
     */
    public function addFieldInput($sName, $mCurrentVal, $bDisabled = false) {

        $aItems = array(
            'type' => 'textfield',
            'disabled' => $bDisabled
        );

        if ( !is_null($mCurrentVal) )
            $aItems['value'] = $mCurrentVal;

        $this->aUpdRowList[$sName] = $aItems;

    }

    /**
     * Добавление поля типа файл
     * @param $sName
     * @param $mCurrentVal
     * @param bool $bDisabled
     */
    public function addFieldFile($sName, $mCurrentVal, $bDisabled = false) {

        $aItems = array(
            'type' => 'file',
            'disabled' => $bDisabled
        );

        if ( !is_null($mCurrentVal) )
            $aItems['value'] = $mCurrentVal;

        $this->aUpdRowList[$sName] = $aItems;

    }

} 