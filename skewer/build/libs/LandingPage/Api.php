<?php

namespace skewer\build\libs\LandingPage;
use skewer\build\Component\Section\Parameters;
use skewer\build\Component\Section\Params\ListSelector;
use yii\helpers\ArrayHelper;

/**
 * API для работы с модулем LandingPage
 * Class Api
 * @package skewer\build\libs\LandingPage
 */
class Api {

    /** имя основной группы параметров */
    const groupMain = 'lp';

    /** имя группы параметров с данными */
    const groupData = 'lp_data';

    /** имя параметра с общим шаблоном для страницы */
    const pageTpl = 'pageTpl';

    /** имя параметка скрытия меню */
    const paramShowMenu = 'lp_show_menu';

    /**
     * Отдает набор сохраненных данных для заданного раздела
     * @param int $iSectionId
     * @return array
     */
    public static function getData( $iSectionId ) {

        $aDataList = Parameters::getList( $iSectionId )
            ->group(self::groupData)
            //->level(ListSelector::alEdit) //@todo как должно быть в лендингах?
            ->get();

        $aRes = [];
        foreach( $aDataList as $aParam ){
            $aRes[$aParam->name] = $aParam->show_val;
        }
        return $aRes;

    }

    /**
     * Отдает набор сохраненных данных для заданного раздела
     * @param int $iSectionId
     * @return Row[]
     */
    public static function getDataRows( $iSectionId ) {

        // набор меток
        $aRowList = Parser::getRowsByTpl( Tpl::getForSection( $iSectionId ) );

        // данные для шаблона
        $aData = self::getData( $iSectionId );

        foreach ( $aRowList as $oRow )
            $oRow->value = isset($aData[$oRow->name]) ? $aData[$oRow->name] : '';

        return $aRowList;

    }

    /**
     * Отдает результат парсинга шаблона и данных для раздела
     * @param $pageId
     * @return string
     */
    public static function getViewForSection( $pageId ) {
        return Parser::render(
            Tpl::getForSection( $pageId ),
            self::getDataRows( $pageId )
        );
    }

}