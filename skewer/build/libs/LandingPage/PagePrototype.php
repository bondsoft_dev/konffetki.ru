<?php

namespace skewer\build\libs\LandingPage;
use skewer\build\Component\Section\Tree;
use skewer\build\Component\Section\Parameters;

/**
 * Прототип модуля для клиентского модуля
 * Class Prototype
 * @package skewer\build\libs\LandingPage
 */
abstract class PagePrototype extends \skModule {

    /** @var int id раздела */
    var $pageId;

    /**
     * Метод - исполнитель функционала
     */
    public function execute() {

        $this->validateProcessCall();

        $aTpl = Parameters::getByName( $this->pageId, Api::groupMain, Api::pageTpl, true );
        $sTpl = $aTpl ? $aTpl[ 'show_val' ] : "no tpl for section [$this->pageId]";

        $aData = array(
            'css' => Css::getParsedText( $this->pageId ),
            'class' => Css::getBlockClass( $this->pageId ),
            'text' => $this->getRenderedText(),
            'alias' => Tree::getSectionAlias( $this->pageId )
        );

        $sContent = \skTwig::renderSource( $sTpl, $aData );

        $this->setOut( $sContent );

        return psRendered;

    }

    /**
     * @todo убрать exit внутри метода
     * Проверяет возможность запуска модуля в текущем окружении
     * нет родителя - запуск блока из корня - редирект на главную LP
     * @return void
     */
    private function validateProcessCall() {

        $response = \Yii::$app->getResponse();

        if ( $this->oContext->getParentProcess() )
            return;

        $iParentId = Tree::getSectionParent( $this->pageId );
        if ( !$iParentId ) {
            $response->setStatusCode(500);
            $response->send();
        }

        $sUrl = \Yii::$app->router->rewriteURL( "[$iParentId]" );
        if ( !$sUrl ) {
            $response->setStatusCode(500);
            $response->send();
        }


        $sAlias = Tree::getSectionAlias( $this->pageId );
        if ( $sAlias )
            $sUrl = sprintf( '%s#%s', $sUrl, $sAlias );

        \Yii::$app->getResponse()->redirect($sUrl,'301')->send();

    }

    /**
     * Отдает полностью сформированный текст блока
     * @return string
     */
    abstract protected function getRenderedText();

}