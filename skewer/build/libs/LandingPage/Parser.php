<?php

namespace skewer\build\libs\LandingPage;
use yii\web\ServerErrorHttpException;

/**
 * Парсер шаблонов для модуля LandingPage
 * Class Parser
 * @package skewer\build\libs\LandingPage
 */
class Parser {

    /**
     * Выбирает из шаблона метки
     * @param string $sTpl шаблон
     * @return Row[]
     */
    public static function getRowsByTpl( $sTpl ) {

        /*
         * [^{] - чтобы отрезать Twig метки типа {{...}}
         * пробелы в начале и конце - из-за предыдущего условия не срабатывают в начале и конце строки
         *      можно попробовать решить подругому
         */
        preg_match_all( '/[^{]{((?<type>[a-z]+)\|)?(?<title>[ \w]+)}[^}]/iu', ' '.$sTpl.' ', $aList );

        if ( !$aList )
            return array();

        $aOut = array();
        foreach ( $aList['title'] as $iKey => $sTitle ) {

            if ( !trim($sTitle) )
                continue;

            $sType = $aList['type'][$iKey];
            $sName = \skTranslit::change( $sTitle );
            $sName = str_replace( ' ', '_', $sName );

            if ( array_key_exists( $sName, $aOut ) )
                continue;

            $aOut[ $sName ] = self::makeRow(
                $sName,
                '',
                $sTitle,
                $sType
            );

        }

        return $aOut;

    }

    /**
     * Парсит шаблон данными и выдает текст
     * @param string $sTpl
     * @param Row[] $aDataList
     * @throws ServerErrorHttpException
     * @return string
     */
    public static function render( $sTpl, $aDataList ) {

//        foreach ( $aDataList as $aLex ) {
//
//            $pattern = "/{((string)\|)?" . $aLex->title . "}/i";
//            $replacement = $aLex->value;
//
//            $sTpl = preg_replace($pattern, $replacement, $sTpl);
//
//        }

        $aLexList = self::getRowsByTpl( $sTpl );
        foreach ( $aLexList as $aLex ) {

            foreach ( $aDataList as $aCurRow ) {
                if ( !$aCurRow instanceof Row )
                    throw new ServerErrorHttpException( 'Данные для обработки шаблона должны быть типа LP\Row' );
                if ( $aLex->name == $aCurRow->name )
                    $aLex->value = $aCurRow->value;
            }

            $pattern = "/{([a-z]+\|)?" . $aLex->title . "}/i";
            $replacement = $aLex->value;

            $sTpl = preg_replace($pattern, $replacement, $sTpl);
        }


        return $sTpl;
    }

    /**
     * Отдает список имен полей для заданного шаблона
     * @param $getTpl
     * @return string[]
     */
    public static function getNamesFromTpl( $getTpl ) {
        $aRowList = self::getRowsByTpl( $getTpl );
        $aOut = array();
        foreach ( $aRowList as $oRow )
            $aOut[] = $oRow->name;
        return $aOut;
    }

    /**
     * Создает объект-элемент для парсинга
     * @param string $sName имя параметра
     * @param string $sValue значение
     * @param string $sTitle название
     * @param string $sType тип
     * @return Row
     */
    public static function makeRow($sName, $sValue, $sTitle='', $sType=null) {
        $oRow = new Row;
        $oRow->name = $sName;
        $oRow->value = $sValue;
        $oRow->title = $sTitle;
        $oRow->type = $sType ? $sType : 'string';
        return $oRow;
    }

} 