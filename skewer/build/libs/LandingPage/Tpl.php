<?php

namespace skewer\build\libs\LandingPage;
use skewer\build\Component\Section\Parameters;


/**
 * Класс для работы с шаблонами
 * Class Tpl
 * @package skewer\build\libs\LandingPage
 */
class Tpl {

    /** имя параметра для шаблона */
    const paramName = 'tpl';

    /** имя параметра текстового описания шаблона */
    const descParamName = 'tplDesc';

    /**
     * Отдает текст шаблона для заданного раздела
     * @param int $pageId
     * @param string $sParamName имя параметра
     * @return string
     */
    public static function getForSection( $pageId, $sParamName = self::paramName ) {

        return Parameters::getShowValByName( $pageId, Api::groupMain, $sParamName, true );

    }

}