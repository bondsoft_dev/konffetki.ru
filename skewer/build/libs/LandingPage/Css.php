<?php

namespace skewer\build\libs\LandingPage;
use skewer\build\Component\Section\Parameters;


/**
 * Класс для работы с css данными
 * Class Css
 * @package skewer\build\libs\LandingPage
 */
class Css {

    /** имя параметра для css данных */
    const paramName = 'css';

    /** префикс для класса блоков */
    const blockClassPrefix = 'block_';

    /** имя метки для класса блока в css */
    const classLabel = '[block_class]';

    /**
     * Отдает класс для блока раздела с заданным id
     * @param int $pageId id раздела блока
     * @return string
     */
    public static function getBlockClass( $pageId ) {
        return self::blockClassPrefix.$pageId;
    }

    /**
     * Отдает текст css параметров для заданного раздела
     * @param int $pageId
     * @return string
     */
    public static function getForSection( $pageId ) {

        $aCss = Parameters::getByName( $pageId, Api::groupMain, self::paramName, true );
        return $aCss ? $aCss[ 'show_val' ] : '';
    }

    /**
     * Отдает скомпилированный текст для раздела
     * (с подставленными значениями в метки)
     * @param int $pageId id раздела
     * @return string
     */
    public static function getParsedText( $pageId ) {

        $sOut = self::getForSection( $pageId );

        // класс для блока
        $sOut = preg_replace( '/\.?'.preg_quote(self::classLabel).'/', '.'.self::getBlockClass($pageId), $sOut );

        return $sOut;

    }

}