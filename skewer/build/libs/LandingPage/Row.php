<?php

namespace skewer\build\libs\LandingPage;


/**
 * Объект - запись для парсера
 * Class Row
 * @package skewer\build\libs\LandingPage
 */
class Row {

    /** @var string Название поля */
    public $title = '';

    /** @var string системное имя поля */
    public $name = '';

    /** @var string значение поля */
    public $value = '';

    /** @var string имя типа поля */
    public $type = '';

} 