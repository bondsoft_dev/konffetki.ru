<?php

$aConfig['name']     = 'Main';
$aConfig['title']    = 'LandingPage. Главный';
$aConfig['version']  = '1.000a';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::LANDING_PAGE;
$aConfig['useNamespace'] = true;
$aConfig['languageCategory']     = 'page';

return $aConfig;
