<?php

namespace skewer\build\LandingPage\Main;

use skewer\build\Component\Section;
use skewer\build\Component\Section\Tree;
use skewer\build\Component\Section\Parameters;

use skewer\build\Component\SEO;
use skewer\build\libs\LandingPage as LP;
use yii\web\ServerErrorHttpException;

/**
 * Модуль сборки набора блоков в LandingPage
 * Class Module
 * @package LandingPage
 */
class Module extends \skModule {

    /** @var int id страницы */
    public $pageId = 0;

    /**
     * Конструктор
     * @param \skContext $oContext
     * @throws ServerErrorHttpException
     */
    public function __construct(\skContext $oContext) {

        parent::__construct( $oContext );

        $aSubSectionList = Tree::getSubSections( $this->pageId );

        foreach ( $aSubSectionList as $section ) {

            if ( !in_array( (int)$section->visible, array(
                Section\Visible::HIDDEN_FROM_PATH,
                Section\Visible::VISIBLE
            ) ) ) continue;

            // id подраздела
            $iSubSectionId = (int)$section->id;

            // параметр объекта
            $aObjParam =  Parameters::getByName( $iSubSectionId, Parameters::settings, Parameters::object, true );
            if ( !$aObjParam )
                throw new ServerErrorHttpException ( 'Не найден корневой модуль для подчиненного блока' );

            // набор параметров
            $aParams = Parameters::getList( $iSubSectionId )
                ->rec()
                ->asArray()
                ->group(Parameters::settings)
                ->get()
            ;

            $aParams['pageId'] = $iSubSectionId;

            // имя модуля
            $sClassName = \Module::getClassOrExcept( $aObjParam['value'], \Layer::PAGE );

            // добавление подчиненного модуля
            $sLabelName = 'block_' . $iSubSectionId;
            $this->addChildProcess( new \skContext(
                $sLabelName,
                $sClassName,
                ctModule,
                $aParams
            ) );

        }

    }

    /**
     * Метод - исполнитель функционала
     */
    public function execute() {

        // если не все дети отработали - ждем
        foreach ( $this->getChildProcesses() as $oProcess ) {
            if ( !in_array( $oProcess->getStatus(), array( psComplete, psRendered, psWait ) ) )
                return psWait;
        }

        $aParamsList = Parameters::getList( $this->pageId )
            ->rec()
            ->asArray()
            ->groups()
            ->fields(['value', 'show_val'])
            ->get();

        /** Метки из . */
        foreach($aParamsList as $sGroupName => $aParams) {
            if ( $sGroupName === Parameters::settings ){
                foreach( $aParams as $aParamsSet){
                    $this->setData($aParamsSet['name'], ['value' => $aParamsSet['value'], 'text' => $aParamsSet['show_val']]);
                }
            }
        }

        $this->setSEO();

        $aOut = array();

        // menu
        if ( Parameters::getValByName( $this->pageId, Parameters::settings, LP\Api::paramShowMenu, true ) ) {

            $list = Tree::getSubSections( $this->pageId );

            $aMenu = [];

            foreach ( $list as $section )
                if ( $section->visible == Section\Visible::VISIBLE )
                    $aMenu[$section->id] = $section->getAttributes();

            $this->setData('menu', $aMenu);
        }

        //$this->addCSSFile( 'main.css' );

        // собираем со всех выходные данные
        foreach ( $this->getChildProcesses() as $oProcess ) {

            if ( $oProcess->getStatus() === psComplete )
                $oProcess->render();

            $oModule = $oProcess->getModule();
            if ( $oModule instanceof LP\PagePrototype )
                $aOut[] = $oProcess->getOut();

        }

        $this->setParser(parserPHP);
        $this->setTemplate( 'main.php' );

        $this->setData('blockList', $aOut);

        $this->setData('css', LP\Css::getParsedText( $this->pageId ));

        $this->oContext->oProcess->setStatus( psComplete );

        /*
         * Счетчики
         */
        $aCodesParam = Parameters::getByName(
            \Yii::$app->sections->root(),
            Parameters::settings,
            'countersCode',
            false
        );

        if ( $aCodesParam )
            $this->setData('countersCode', $aCodesParam['show_val']);

        $this->addChildProcess( new \skContext(
            'seo',
            'skewer\\build\\Page\\SEOMetatags\\Module',
            ctModule,
            array( 'pageId' => $this->pageId )
        ) );

        $oSeoProcess = $this->getChildProcess('seo');
        $oSeoProcess->execute();
        $oSeoProcess->render();

        return psComplete;

    }

    /**
     * Задает SEO данные
     * fixme этот метод дублирует метод в Page\Main\Module
     */
    public function setSEO(){

        // SEO Метатеги для страницы
        $SEOData = $this->getEnvParam( 'SEOTemplates', array() );
        $SEOData['text'] = array('title' => '', 'description' => '', 'keywords' => '');
        if ( $oDataRow = SEO\Api::get( 'section', $this->pageId ) )
            $oDataRow->setParams( $SEOData, 'text' );

        $this->setEnvParam( 'SEOTemplates', $SEOData );
        $SEOVars = $this->getEnvParam( 'SEOVars', array() );

        $SEOVars['label_page_title_upper'] = Tree::getSectionTitle( $this->pageId, true );
        $SEOVars['label_page_title_lower'] = mb_strtolower($SEOVars['label_page_title_upper']);

        $items = array();//$tree->getByIdList($goodParents);

        $SEOVars['label_path_to_main_upper'] = '';
        $SEOVars['label_path_to_page_upper'] = '';

        if (!empty($items))
            foreach ($items as &$item) {
                $SEOVars['label_path_to_main_upper'] .= ' ' . $item['title'] . '.';
                $SEOVars['label_path_to_page_upper'] = $item['title'] . '. ' . $SEOVars['label_path_to_page_upper'];
            } // foreach

        $SEOVars['label_path_to_main_lower'] = mb_strtolower($SEOVars['label_path_to_main_upper']);
        $SEOVars['label_path_to_page_lower'] = mb_strtolower($SEOVars['label_path_to_page_upper']);
        // Название сайта
        $SEOVars['label_site_name'] = \Site::getSiteTitle();

        $this->setEnvParam('SEOVars', $SEOVars);
    }

}