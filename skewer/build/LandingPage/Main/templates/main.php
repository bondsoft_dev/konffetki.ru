<?php
/**
 * @var $this \yii\web\View
 * @var string $SEOTitle
 * @var string $SEOKeywords
 * @var string $SEODescription
 * @var string $canonical_url
 */

use \yii\helpers\Html;

skewer\assets\JqueryAsset::register($this);
skewer\assets\JqueryUiAsset::register($this);
skewer\assets\FancyboxAsset::register($this);
skewer\assets\DatepickerAsset::register($this);
skewer\assets\FancyBoxAsset::register($this);

\Design::assetsRegister( $this, \Layer::LANDING_PAGE );

skewer\build\Page\GalleryViewer\Asset::register($this);

if (isset($designMode)){
    \skewer\assets\DesignAsset::register($this);
}


?>
<?php $this->beginPage() ?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.1//EN" "http://www.w3.org/TR/xhtml11/DTD/xhtml11.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?= \Yii::$app->language ?>" >
<head>
    <meta charset="utf-8"  content="text/html" />
    <title><?= Html::decode($SEOTitle) ?></title>
    <meta name="description" content="<?= Html::decode($SEODescription) ?>" />
    <meta name="keywords" content="<?=Html::decode($SEOKeywords) ?>" />
    <link rel="shortcut icon" href="<?= Design::get('page','favicon') ?>" type="image/png" />

    <? if (isSet($SEONonIndex) && $SEONonIndex): ?><meta name="robots" content="none"/><? endif ?>
    <? if (isSet($SEOAddMeta) && $SEOAddMeta): ?><?=Html::decode($SEOAddMeta) ?><? endif ?>

    <? if (isSet($gaCode['text']) && $gaCode['text']): ?><?=Html::decode($gaCode['text']) ?><? endif ?>
    <?php $this->head() ?>
    <? $jsItems = Design::assetJs(); ?>
    <? foreach ($jsItems as $js): ?>
        <?= $js['line'] ?>
    <? endforeach ?>
</head>
<body>
<?php $this->beginBody() ?>
<input type="hidden" id="current_language" value="<?= \Yii::$app->language ?>">
    <div class="l-container">
        <a class="page_top"><?= \Yii::t('page', 'back_to_top') ?></a>
        <script type="text/javascript">
            var menu_height = $(".b-sevice").height();
            $('.sevice-stopper').height(menu_height);

            $(function() {

                $(window).scroll(function() {
                    if($(this).scrollTop() != 0) {
                        $('.page_top').fadeIn();
                    } else {
                        $('.page_top').fadeOut();
                    }
                });

                $('.page_top').click(function() {
                    $('body,html').animate({scrollTop:0},800);
                });

            });
        </script>
        <? if (isset($css)): ?>
            <style type="text/css">
                <?= $css ?>
            </style>
        <? endif ?>
        <? if (isset($menu) && is_array($menu)): ?>
            <div class="sevice-stopper"></div>
            <div class="b-sevice">
                <ul class="level-1">
                    <? foreach($menu as $item): ?>
                        <?php if ($item['visible']): ?>
                            <li class="item-1 <?=(isset($item['selected']))?"on-1":"" ?> <?= (isset($item['last']))?"last":"" ?>">
                                <span><a href="#<?= $item['alias']?>"><ins></ins><?= $item['title'] ?></a></span></li>
                        <? endif ?>
                    <? endforeach ?>
                </ul>
            </div>
        <? endif ?>
        <?php foreach($blockList as $block): ?>
            <?= $block ?>
        <? endforeach ?>
    </div>

    <?= $countersCode ?>

    <div id="callbackForm" style="display: none;"></div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>