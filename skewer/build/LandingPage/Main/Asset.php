<?php

namespace skewer\build\LandingPage\Main;


use yii\web\AssetBundle;

class Asset extends AssetBundle {
    public $sourcePath = '@skewer/build/LandingPage/Main/web';

    /**
     * @inheritDoc
     * todo #stab_check 2 js файла тянутся из основной клиентской части. либо свест подключание к
     *      наследованию, либо сделать свои без избыточночти
     */
    public function init()
    {
        parent::init();
        $sMainPath = \Yii::$app->getAssetManager()->getBundle(
            \skewer\build\Page\Main\Asset::className()
        )->baseUrl;
        $this->js = [
            $sMainPath.'/js/pageInit.js',
            $sMainPath.'/js/accordion.js'
        ];
    }


    public $css = [
        'css/main.css'
    ];

    /*
     * todo #stab_check можно подключать в модуле баннеров, а не корневом
     */
    public $depends = [
        '\skewer\build\Page\MainBanner\Asset'
    ];

}