<?php

namespace skewer\build\LandingPage\Slider;


use yii\web\AssetBundle;

class Asset extends AssetBundle {
    public $sourcePath = '@skewer/build/LandingPage/Slider/web';

    public $css = [
        'css/bxslider.css'
    ];


}