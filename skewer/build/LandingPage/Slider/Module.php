<?php

namespace skewer\build\LandingPage\Slider;

use skewer\assets\JqueryAsset;
use skewer\build\Component\Section\Parameters;
use skewer\build\libs\LandingPage as LP;
use skewer\build\Adm\Slider;

/**
 * Class Module
 * @package skewer\build\Page\LandingPage\Editor
 */
class Module extends LP\PagePrototype {


    /**
     * Отдает полностью сформированный текст блока
     * @return string
     */
    protected function getRenderedText() {

        $sContent = LP\Api::getViewForSection( $this->pageId );

        $iBannerId = Parameters::getValByName( $this->pageId, 'object', 'currentBanner', false );

        $aBanner = Slider\Banner::find( $iBannerId );

        $aSlides = Slider\Slide::getSlides4Banner( $iBannerId );

        if (!$aSlides) return false;

        $aBannerTools = Slider\Banner::getAllTools();
        if ( isSet( $aBanner->bullet ) && isSet( $aBanner->scroll ) ) {
            $aBannerTools['pager'] = (float)$aBanner->bullet;
            $aBannerTools['controls'] = (float)$aBanner->scroll;
        }

        $aData = array(
            'banner' => $aSlides,
            'tools' => $aBannerTools,
            'config' => json_encode($aBannerTools)
        );

        return \skTwig::renderSource( $sContent, $aData );
        //return psRendered;

    }

}