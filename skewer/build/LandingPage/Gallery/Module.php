<?php

namespace skewer\build\LandingPage\Gallery;


use skewer\build\Component\Section\Parameters;
use skewer\build\libs\LandingPage as LP;
use skewer\build\Component\Gallery;

/**
 * Class Module
 * @package skewer\build\Page\LandingPage\Gallery
 */
class Module extends LP\PagePrototype {

    /**
     * Отдает собранный html код для сборки блока
     * @return string
     */
    protected function getRenderedText() {

        $sContent = LP\Api::getViewForSection( $this->pageId );

        $iGalleryId = Parameters::getValByName( $this->pageId, Parameters::object, 'iCurrentAlbumId', false );

        if (!$iGalleryId) return '';

        $aData = Gallery\Photo::getFromAlbum(explode(',', $iGalleryId), true);

        // НАЧАЛО: Операция для совместимости с шаблоном в БД. todo переделать шаблон, см \skewer\build\Page\GalleryViewer\templates\slider.twig
        if ($aData){
            $iCount = count($aData);

            for ($i=0; $i<$iCount; $i++) {
                if ( (!isset($aData[$i]['images_data']['preview']['file']) OR empty($aData[$i]['images_data']['preview']['file'])) OR
                    (!isset($aData[$i]['images_data']['med']['file']) OR empty($aData[$i]['images_data']['med']['file'])) ) unset($aData[$i]);
                /** @var \skewer\build\Component\Gallery\models\Photos [] $aData */
                $aData[$i] = $aData[$i]->toArray();
                $aData[$i]['min_src'] = $aData[$i]['images_data']['preview']['file'];
                $aData[$i]['med_src'] = $aData[$i]['images_data']['med']['file'];
                $aData[$i]['min_width'] = isset($aData[$i]['images_data']['preview']['width']) ? $aData[$i]['images_data']['preview']['width'] : '';
                $aData[$i]['min_height'] = isset($aData[$i]['images_data']['preview']['height']) ? $aData[$i]['images_data']['preview']['height'] : '';
            }
            // КОНЕЦ: Операция для совместимости с шаблоном в БД.
        }

        $aData = array(
            'list' => $aData,
            'section' => $this->pageId,
            'titleOnMain' => ''
        );

        return \skTwig::renderSource( $sContent, $aData );
    }
}