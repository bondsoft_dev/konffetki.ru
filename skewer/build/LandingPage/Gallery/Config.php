<?php

$aConfig['name']     = 'Gallery';
$aConfig['title']    = 'LandingPage. Галерея';
$aConfig['version']  = '1.000a';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::LANDING_PAGE;
$aConfig['useNamespace'] = true;
$aConfig['languageCategory']     = 'forms';

return $aConfig;
