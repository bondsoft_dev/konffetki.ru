```css
.[block_class] .b-section {
    background: #ccc;
}
/*-__-*/
.carousel {
    width: 98%;
    margin: 0 auto;
}
/*--------------------------------------------------------------------*/

.jcarousel-skin-tango {
    padding: 0;
    width: 100%;
    background: #fff;
    margin-bottom: 1em;
}
.jcarousel-skin-tango .jcarousel-container {
    /*-moz-border-radius: 10px;
    background: rgb(0,0,0);    */
    background: transparent;
    /*border: 1px solid #346F97;*/
}
.jcarousel-skin-tango .image {
    width: 500px;
    overflow: hidden;
    position: relative;
}
.jcarousel-skin-tango .image img {
}

.jcarousel-skin-tango .jcarousel-direction-rtl {
    direction: rtl;
}

.jcarousel-skin-tango .jcarousel-container-horizontal {
    /*width: 100%;*/
    padding: 15px 25px 5px;
    margin: 0 auto;
}

.jcarousel-skin-tango .jcarousel-container-vertical {
    width: 75px;
    height: 245px;
    padding: 40px 20px;
}

.jcarousel-skin-tango .jcarousel-clip-horizontal {
    width: 100%;
    height: 130px;
    overflow: hidden;
}

.jcarousel-skin-tango .jcarousel-clip-vertical {
    width:  90px;
    height: 245px;
}

.jcarousel-skin-tango .jcarousel-item {
    width: 160px!important;
    height: 120px;
    text-align: center;
}

.jcarousel-skin-tango .jcarousel-item-horizontal {
    margin-right: 3px;
}

.jcarousel-skin-tango .jcarousel-direction-rtl .jcarousel-item-horizontal {
    margin-left: 10px;
    margin-right: 0;
}

.jcarousel-skin-tango .jcarousel-item-vertical {
    margin-bottom: 10px;
}

.jcarousel-skin-tango .jcarousel-item-placeholder {
    background: #D5BFB0;
    color: #000;
}

/**
 *  Horizontal Buttons
 */
.jcarousel-skin-tango .jcarousel-next-horizontal {
    position: absolute;
    top: 15px;
    bottom: 5px;
    right: 0;
    width: 13px;
    cursor: pointer;
    background: url(/skewer/build/Page/GalleryViewer/images/slider.arr.right.png) no-repeat 50% 50%;
}

.jcarousel-skin-tango .jcarousel-direction-rtl .jcarousel-next-horizontal {
    left: 5px;
    right: auto;
    background-image: url(/skewer/build/Page/GalleryViewer/images/slide.arrback.gif);
}

.jcarousel-skin-tango .jcarousel-next-disabled-horizontal,
.jcarousel-skin-tango .jcarousel-next-disabled-horizontal:hover,
.jcarousel-skin-tango .jcarousel-next-disabled-horizontal:active {
    cursor: default;
}

.jcarousel-skin-tango .jcarousel-prev-horizontal {
    position: absolute;
    top: 15px;
    bottom: 5px;
    left: 0;
    width: 13px;
    cursor: pointer;
    background: url(/skewer/build/Page/GalleryViewer/images/slider.arr.left.png) no-repeat 50% 50%;
}

.jcarousel-skin-tango .jcarousel-direction-rtl .jcarousel-prev-horizontal {
    left: auto;
    right: 5px;
    background-image: url(/skewer/build/Page/GalleryViewer/images/slide.arrnext.gif);
}

.jcarousel-skin-tango .jcarousel-prev-disabled-horizontal,
.jcarousel-skin-tango .jcarousel-prev-disabled-horizontal:hover,
.jcarousel-skin-tango .jcarousel-prev-disabled-horizontal:active {
    cursor: default;
}

/**
 *  Vertical Buttons
 */
.jcarousel-skin-tango .jcarousel-next-vertical {
    position: absolute;
    bottom: 5px;
    left: 43px;
    width: 32px;
    height: 32px;
    cursor: pointer;
    background: transparent url(/skewer/build/Page/GalleryViewer/images/next-vertical.png) no-repeat 0 0;
}

.jcarousel-skin-tango .jcarousel-next-vertical:hover {
    background-position: 0 -32px;
}

.jcarousel-skin-tango .jcarousel-next-vertical:active {
    background-position: 0 -64px;
}

.jcarousel-skin-tango .jcarousel-next-disabled-vertical,
.jcarousel-skin-tango .jcarousel-next-disabled-vertical:hover,
.jcarousel-skin-tango .jcarousel-next-disabled-vertical:active {
    cursor: default;
    background-position: 0 -96px;
}

.jcarousel-skin-tango .jcarousel-prev-vertical {
    position: absolute;
    top: 5px;
    left: 43px;
    width: 32px;
    height: 32px;
    cursor: pointer;
    background: transparent url(/skewer/build/Page/GalleryViewer/images/prev-vertical.png) no-repeat 0 0;
}

.jcarousel-skin-tango .jcarousel-prev-vertical:hover {
    background-position: 0 -32px;
}

.jcarousel-skin-tango .jcarousel-prev-vertical:active {
    background-position: 0 -64px;
}

.jcarousel-skin-tango .jcarousel-prev-disabled-vertical,
.jcarousel-skin-tango .jcarousel-prev-disabled-vertical:hover,
.jcarousel-skin-tango .jcarousel-prev-disabled-vertical:active {
    cursor: default;
    background-position: 0 -96px;
}

/***********************************************************************************************************/
.image_carousel {
    /*padding: 15px 0 15px 40px;*/
    position: relative;
    padding: 0 50px;
}
.image_carousel li {
    border: 1px solid #ccc;
    background-color: white;
    padding: 9px;
    margin: 7px;
    display: inline-block;
    vertical-align: top;
}
a.prev, a.next {
    background: url(/skewer/build/Page/GalleryViewer/images/miscellaneous_sprite.png) no-repeat transparent;
    width: 45px;
    height: 50px;
    display: block;
    position: absolute;
    top: 85px;
}
a.prev {
    left: -22px;
    background-position: 0 0; }
a.prev:hover {		background-position: 0 -50px; }
a.prev.disabled {	background-position: 0 -100px !important;  }
a.next {
    right: -22px;
    background-position: -50px 0;
}
a.next:hover {		background-position: -50px -50px; }
a.next.disabled {	background-position: -50px -100px !important;  }
a.prev.disabled, a.next.disabled {
    cursor: default;
}

a.prev span, a.next span {
    display: none;
}
.pagination {
    text-align: center;
}
.pagination a {
    background: url(/skewer/build/Page/GalleryViewer/images/miscellaneous_sprite.png) 0 -300px no-repeat transparent;
    width: 15px;
    height: 15px;
    margin: 0 5px 0 0;
    display: inline-block;
}
.pagination a.selected {
    background-position: -25px -300px;
    cursor: default;
}
.pagination a span {
    display: none;
}
.clearfix {
    float: none;
    clear: both;
}
ul.gallery_slider {
    margin: 0;
    padding: 0;
    text-align: center;
}
```

```html
<div class="b-section">
    <div class="section__inner">
        <div class="section__content">
            <div class="b-slide-title">
                {% if titleOnMain or Design.modeIsActive() %}
                    <h2 sktag="editor.h2" skeditor="GalleryViewer/titleOnMain">{{ titleOnMain }}</h2>
                {% endif %}
            </div>
            <div class="image_carousel">
                <ul id="gallery_slider" class="gallery_slider">
                    {%  for aItem in list  %}
                        <li><a class="single_3" rel="carousel-group" href="{{ aItem.med_src }}"><img src="{{ aItem.min_src }}" alt="{{ aItem.alt_title }}" height="160px" /></a></li>
                    {% endfor %}
                </ul>
                <div class="clearfix"></div>
                <a class="prev" id="foo2_prev" href="#"><span>prev</span></a>
                <a class="next" id="foo2_next" href="#"><span>next</span></a>
                <div class="pagination" id="foo2_pag"></div>
            </div>
            <div class="g-clear"></div>
        </div>
    </div>
</div>
```