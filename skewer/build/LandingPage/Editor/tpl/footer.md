```css
[block_class] .b-section {
    background-color: #ddd;
}
[block_class] .section__content {
    height: 100px;
}
[block_class] .b-absolute {
    position: relative;
    width: 100%;
}
[block_class] .b-absolute .absolute__item {
    position: absolute;
    padding: 0;
    z-index: 15;
}
[block_class] .b-absolute .absolute__1 {
    top: 0;
    left: 0;
    width: 30%;
}
[block_class] .b-absolute .absolute__2 {
    top: 0;
    left: 33%;
    width: 30%;
}
[block_class] .b-absolute .absolute__3 {
    top: 0;
    left: 66%;
    width: 30%;
}
```

```html
<div class="class b-section">
    <div class="section__inner">
        <div class="section__content">
            <h2 class="section__header">{string|Название}</h2>
            <div class="b-absolute">
                <div class="absolute__item absolute__1">
                    <div class="b-editor">
                        {wyswyg|Левый блок}
                    </div>
                </div>
                <div class="absolute__item absolute__2">
                    <div class="b-editor">
                       {wyswyg|Средний блок}
                    </div>
                </div>
                <div class="absolute__item absolute__3">
                    <div class="b-editor">
                        {wyswyg|Правый блок}
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
```