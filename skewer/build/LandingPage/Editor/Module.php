<?php

namespace skewer\build\LandingPage\Editor;

use skewer\build\libs\LandingPage as LP;

/**
 * Class Module
 * @package skewer\build\Page\LandingPage\Editor
 */
class Module extends LP\PagePrototype {

    /**
     * Отдает полностью сформированный текст блока
     * @return string
     */
    protected function getRenderedText() {
        return LP\Api::getViewForSection( $this->pageId );
    }

}