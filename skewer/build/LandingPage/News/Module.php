<?php

namespace skewer\build\LandingPage\News;

use skewer\build\Component\Section\Parameters;
use skewer\build\libs\LandingPage as LP;
use skewer\build\Adm\News;

/**
 * Class Module
 * @package skewer\build\Page\LandingPage\Editor
 */
class Module extends LP\PagePrototype {

    /**
     * Отдает собранный html код для сборки блока
     * @return string
     */
    protected function getRenderedText() {

        $sContent = LP\Api::getViewForSection( $this->pageId );

        /**
         * @todo group object??
         */
        $sGetFromSection = Parameters::getValByName( $this->pageId, 'object', 'getFromSection', true );

        $iOnPage= Parameters::getValByName( $this->pageId, 'object', 'onPage', true );

        $aNewsList = \skewer\models\News::getPublicList(array(
            'page' => $this->getInt( 'page', 1 ),
            'order'    => 'DESC',
            'future'   => '',
            'byDate'   => '',
            'on_page'  => $iOnPage,
            'on_main'  => '',
            'section'  => $this->pageId . ( $sGetFromSection ? ',' . $sGetFromSection : '' ),
            'archive'  => '',
            'all_news' => '',
            'show_list'=> ''
        ));

        $aData = array();
        foreach ( $aNewsList->getModels() as $oNews ) {
            /**
             * @var \skewer\models\News $oNews
             */
            $aNews = $oNews->getAttributes();
            if ( $oNews->parent_section == $this->pageId )
                $aNews['without_href'] = true;
            $aData['aNewsList'][] = $aNews;
        }

        return \skTwig::renderSource( $sContent, $aData );

    }

}