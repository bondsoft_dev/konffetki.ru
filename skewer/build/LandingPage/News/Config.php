<?php

$aConfig['name']     = 'News';
$aConfig['title']    = 'LandingPage. Новости';
$aConfig['version']  = '1.000a';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::LANDING_PAGE;
$aConfig['useNamespace'] = true;
$aConfig['languageCategory']     = 'news';

return $aConfig;
