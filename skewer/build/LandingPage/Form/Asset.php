<?php

namespace skewer\build\LandingPage\Form;


use skewer\build\Page\Forms as PageForms;

class Asset extends PageForms\Asset {
    
    public $depends = [
        'skewer\assets\JqueryAsset',
        'skewer\assets\FancyboxAsset',
        'skewer\build\LandingPage\Main\Asset'
    ];

}