<?php

namespace skewer\build\LandingPage\Form;

use skewer\build\Component\Section\Tree;
use skewer\build\Component\Section\Parameters;
use skewer\build\libs\LandingPage as LP;
use skewer\build\Page\Forms as PageForms;
use skewer\build\Component\Forms;
use skewer\build\Component\ReachGoal;
use yii;

/**
 * Модуль вывода формы на странице LandingPage
 * Class Module
 * @package skewer\build\Page\LandingPage\Form
 */
class Module extends LP\PagePrototype {

    /** ассет форм-билдера */
    const assetPath = '\skewer\build\Page\Forms\Asset';

    /** @var string Путь к веб-директории */
    protected $sModuleWebDir = '';

    public function init(){

        $sAssetClass = static::assetPath;

        if ( class_exists($sAssetClass) ) {

            if ( !is_subclass_of( $sAssetClass, 'yii\web\AssetBundle') )
                throw new \Exception('Module asset must be extended from yii\web\AssetBundle');

            $view = new \yii\web\View();
            /** @var yii\web\AssetBundle $sAssetClass */
            $bundle = $sAssetClass::register($view);

            $this->sModuleWebDir =  $bundle->baseUrl;

        }

        parent::init();
    }

    /**
     * Отдает полностью сформированный текст блока
     * @return string
     */
    protected function getRenderedText() {

        $cmd = $this->getStr('cmd');

        if ( $cmd == 'send' ) {

            $iFormId    = $this->getInt( 'form_id' );
            $sLabel     = $this->getStr( 'label', $this->oContext->getLabel() );

            try {

                $oFormRow = Forms\Table::get4Section( $this->pageId );

                if ( !$oFormRow || $iFormId != $oFormRow->getId() || $this->oContext->getLabel() != $sLabel )
                    return $this->actionShowForm();

                /** @var Forms\Entity $oForm */
                $oForm = Forms\Table::build( $iFormId, $_POST );

                $formHash = $oForm->getHash( $this->pageId, $this->oContext->getLabel() , false );

                if ( !$oForm->validate( $formHash ) )
                    throw new \Exception( $oForm->getError() );

                switch( $oForm->getHandlerType() ){

                    case 'toMail':

                        $bRes = $oForm->send2Mail( 'letter.twig', $this->getModuleDir() . $this->getTplDirectory() );
                        break;

                    case 'toMethod':

                        $bRes = $oForm->send2Method();
                        break;

                    case 'toBase':

                        $oForm->send2Base( $this->pageId );
                        $bRes = $oForm->send2Mail( 'letter.twig', $this->getModuleDir() . $this->getTplDirectory() );
                        break;

                    default:
                        throw new \Exception('handler_not_found');# обработчик неизвестен - выходим
                }

                // вывести сообщение об успешной / неуспешной отправке
                if ( $oForm->getFormRedirect() ) {
                    \Yii::$app->getResponse()->redirect($oForm->getFormRedirect(),'301');
                    return '';
                }

                // добавление обработчика ReachGoal
                if ( $bRes ) {
                    $sTarget = $oForm->getTarget();
                    if ( $sTarget and ReachGoal\Yandex::isActive() ) {
                        $aData['yandexReachGoal'] = array(
                            'id' => rand(0, 1000),
                            'cnt' => ReachGoal\Yandex::getCounter(),
                            'target' => $sTarget
                        );
                    }
                }

                $sData = ($bRes)?'success':'error';

                $aData[$sData] = 1;
                $aData['form_section'] = $this->pageId;

            }
            catch( \Exception $e ) {

                $aData[$e->getMessage()] = 1;
            }

            // текст шаблона
            $sTplText = LP\Tpl::getForSection( $this->pageId );

            // шаблон ответа
            $sAnsTpl = Parameters::getShowValByName( $this->pageId, LP\Api::groupMain, 'answer', true );

            // полный шаблон - вставляем форму в основной
            $aAllTpl = str_replace( '{:form}', $sAnsTpl, $sTplText );

            // парсим lp метки
            $sContent = LP\Parser::render(
                $aAllTpl,
                LP\Api::getDataRows( $this->pageId )
            );

            // парсим twig метки
            return \skTwig::renderSource( $sContent, $aData );

        }

        if ( $cmd != 'send' ) {

            return $this->actionShowForm();
        }

        return '';

    }

    /**
     * Отдает текст формы в виде html разметки
     * @return string
     * @throws \Exception
     */
    protected function actionShowForm() {

        // текст шаблона
        $sTplText = LP\Tpl::getForSection( $this->pageId );

        // шаблон формы
        $sFormText = LP\Tpl::getForSection( $this->pageId, 'form' );

        // полный шаблон - вставляем форму в основной
        $aAllTpl = str_replace( '{:form}', $sFormText, $sTplText );

        // парсим lp метки
        $sContent = LP\Parser::render(
            $aAllTpl,
            LP\Api::getDataRows( $this->pageId )
        );

        // запрос формы для страницы
        $oFormRow = Forms\Table::get4Section( $this->pageId );
        if ( !( $oFormRow instanceof Forms\Row ) )
            return '';

        $oForm = Forms\Table::build( $oFormRow->form_id );

        $label = $this->oContext->getLabel();//$this->get( 'label', $this->oContext->getLabel() );

        $aData = array(
            'oForm' => $oForm,
            'iRandVal' => rand(0, 1000),
            'form_section' => '#' . Tree::getSectionAlias( $this->pageId ),
            'formHash' => md5( md5( $label . $oForm->getId() ) . $this->pageId ),
            'label' => $label,
            'moduleWebPath' => $this->sModuleWebDir
        );

        // парсим twig метки
        return \skTwig::renderSource( $sContent, $aData );

    }

}