```css
```

tpl

```html
<div class="b-section">
    <div class="section__inner">
        <div class="section__content">
            <h2 class="section__header">{Заголовок секции}</h2>

            {:form}

        </div>
    </div>
</div>```

form

```html
            <div class="b-form" sktag="modules.forms">
                <form id="form_{{formHash}}" method="post" action="{{form_section}}" enctype="multipart/form-data"{% if ajaxShow %} ajaxShow="1"{% endif %}{% if ajaxForm %} ajaxForm="1"{% endif %}>

                {% set aFieldList = oForm.getFields() %}
                {% for oField in aFieldList %}

                {% if oField.getType() == 'calendar' %}
                <div class="form__col-{{oField.getClassVal()}} form__label-{{oField.getVal('label_position')}} form__date">
                    <img src="/skewer/build/libs/datepicker/images/calendar.jpg" class="js_ui-datepicker-trigger" alt="" />
                    <label class="form__label">{{oField.getVal('param_title')}}{% if oField.getVal('param_required')==1 %} <ins class="form__mark">*</ins>{% endif %}</label>
                    <div class="form__inputwrap">
                        <input type="text" class="js_init_datepicker" value="{{oField.getVal('param_default')}}" id="datepicker_{{oField.getVal('param_name')}}" name="{{oField.getVal('param_name')}}" cur_year="{{oField.getYear()}}" />
                    </div>
                </div>
                {% elseif oField.getType() == 'checkbox' %}
                <div class="form__col-{{oField.getClassVal()}}{% if oField.getVal('label_position')=='left' %} form__label-left{% endif %} form__checkbox">
                    <div class="form__inputwrap">
                        <input type="checkbox" name="{{oField.getVal('param_name')}}" id="{{oField.getVal('param_name')}}" value="{{oField.getVal('param_default')}}" {{oField.getVal('param_man_params')}} />
                    </div>
                    <label class="form__label" for="check_err">{{oField.getVal('param_title')}}{% if oField.getVal('param_required')==1 %} <ins class="form__mark">*</ins>{% endif %}</label>
                    <p class="form__info">{{oField.getVal('param_description')}}</p>
                </div>
                {% elseif oField.getType() == 'delimiter' %}
                <div class="form__col-{{oField.getClassVal()}}">
                    <p class="form__title">{{oField.getVal('param_default')}}</p>
                </div>
                {% elseif oField.getType() == 'file' %}
                <div class="form__col-{{oField.getClassVal()}} form__label-{{oField.getVal('label_position')}}">
                    <label class="form__label">{{oField.getVal('param_title')}}{% if oField.getVal('param_required')==1 %} <ins class="form__mark">*</ins>{% endif %}</label>
                    <div class="form__inputwrap">
                        <input type="file" name="{{oField.getVal('param_name')}}"  {{oField.getVal('param_man_params')}} />
                        <input type="hidden" value="41943040" name="MAX_FILE_SIZE" />
                    </div>
                    <p class="form__info">{{oField.getVal('param_description')}}</p>
                </div>
                {% elseif oField.getType() == 'input' %}
                <div class="form__col-{{oField.getClassVal()}} form__label-{{oField.getVal('label_position')}}">
                    <label class="form__label">{{oField.getVal('param_title')}}{% if oField.getVal('param_required')==1 %}<ins class="form__mark">*</ins>{% endif %}</label>
                    <div class="form__inputwrap">
                        <input type="text" name="{{oField.getVal('param_name')}}" id="{{oField.getVal('param_name')}}" value="{{oField.getVal('param_default')}}"  {{oField.getVal('param_man_params')}} />
                    </div>
                    <p class="form__info">{{oField.getVal('param_description')}}</p>
                </div>
                {% elseif oField.getType() == 'radio' %}
                {% set aItems = oField.getViewItems() %}
                <div class="form__col-{{oField.getClassVal()}} form__label-{{oField.getVal('label_position')}} form__radio">
                    <label class="form__label" for="{{aItem.param_id}}">{{oField.getVal('param_title')}}{% if oField.getVal('param_required')==1 %} <ins class="form__mark">*</ins>{% endif %}</label>
                    {% for aItem in aItems %}
                    <div class="form__inputwrap">
                        <input type="radio" name="{{oField.getVal('param_name')}}" id="{{aItem.param_id}}"  {{aItem.checked}} value="{{aItem.value}}" {{oField.getVal('param_man_params')}} />
                        <label class="form__label" for="{{aItem.param_id}}">{{oField.getVal('param_title')}}</label>
                    </div>
                    {% endfor %}
                    <p class="form__info">{{oField.getVal('param_description')}}</p>
                </div>
                {% elseif oField.getType() == 'select' %}
                <div class="form__col-{{oField.getClassVal()}} form__label-{{oField.getVal('label_position')}}">
                    <label class="form__label">{{oField.getVal('param_title')}}{% if oField.getVal('param_required')==1 %} <ins class="form__mark">*</ins>{% endif %}</label>
                    <div class="form__inputwrap">
                        <select name="{{oField.getVal('param_name')}}" id="{{oField.getVal('param_name')}}" {{oField.getVal('param_man_params')}}>
                        {% set aItems = oField.getViewItems() %}
                        <option selected disabled>{{oField.getVal('param_title')}}</option>
                        {% for aItem in aItems %}
                        <option value="{{aItem.value}}">{{aItem.title}}</option>
                        {% endfor %}
                        </select>
                    </div>
                    <p class="form__info">{{oField.getVal('param_description')}}</p>
                </div>
                {% elseif oField.getType() == 'textarea' %}
                <div class="form__col-{{oField.getClassVal()}} form__label-{{oField.getVal('label_position')}}">
                    <label class="form__label">{{oField.getVal('param_title')}}{% if oField.getVal('param_required')==1 %} <ins class="form__mark">*</ins>{% endif %}</label>
                    <div class="form__inputwrap">
                        <textarea name="{{oField.getVal('param_name')}}" id="{{oField.getVal('param_name')}}">{{oField.getVal('param_default')}}</textarea>
                    </div>
                    <p class="form__info">{{oField.getVal('param_description')}}</p>
                </div>
                {% endif %}
                {% endfor %}

                {% if oForm.getFormCaptcha() %}
                <div class="form__col-1 form__label-left form__captha">
                    <label class="form__label">{{ Lang.get('forms.captcha') }}</label>
                    <div class="form__inputwrap">
                        <img src="/ajax/captcha.php?v={{iRandVal}}" class="img_captcha" id="img_captcha" />
                        <input type="text" value="" name="captcha" id="captcha" maxlength="4" />
                    </div>
                </div>
                {% endif %}
                {% set agreedData = oForm.getAgreedData() %}
                {% if agreedData %}
                <div class="form__col-1 form__checkbox">
                    <div class="form__inputwrap">
                        <input type="checkbox" id="agreed" name="agreed" value="1" />
                    </div>
                    <label class="form__label" for="check_ok">{{ agreedData.agreed_title }} <span class="agreed_readmore">{{ Lang.get('Main.readmore') }}</span></label>

                    <div class="agreed_text" style="display: none;">{{ agreedData.agreed_text }}</div>
                </div>
                {% endif %}

                <div class="form__col-1">
                    <p class="form__info"><ins class="form__mark">*</ins> - {{ Lang.get('forms.required') }}</p>
                </div>

                <div class="form__col-1 form__align_center">
                    <button type="submit" class="b-btnbox">{{ Lang.get('forms.send') }}</button>
                </div>

                <input type="hidden" name="cmd" value='send' />
                <input type="hidden" name="form_id" id="form_id" value="{{oForm.getId()}}" />
                <input type="hidden" class="_rules" value='{{oForm.getRules()}}' />
                </form>
            </div>
```

answer

```html
<div class="b-answer">
    {% if success==1 %}
        <p>{{ Lang.get('forms.ans_success') }}</p>
        {% if yandexReachGoal %}
            <script language="JavaScript">
                var interval{{yandexReachGoal.id}} = setInterval(
                    function() {
                        if ( typeof yaCounter{{ yandexReachGoal.cnt }} !== 'undefined' ) {
                            yaCounter{{ yandexReachGoal.cnt }}.reachGoal('{{ yandexReachGoal.target }}');
                            clearInterval( interval{{yandexReachGoal.id}} );
                        }
                    },
                    2000
                );
            </script>
        {% endif %}
    {% endif %}
    {% if error==1 %}
        <p>{{ Lang.get('forms.ans_error') }}</p>
        {% if err_msg %}
            <p>{{ err_msg }}</p>
        {% endif %}
    {% endif %}

    {% if form_not_found_error==1 %}
        <div>{{ Lang.get('forms.ans_not_found') }}</div>
    {% endif %}
    {% if form_not_exist_error==1 %}
        <div>{{ Lang.get('forms.ans_not_exists') }}</div>
    {% endif %}
    {% if captcha_error==1 %}
        <p>{{ Lang.get('forms.ans_captcha_error') }}</p>
    {% endif %}
    {% if agreed_error==1 %}
        <p>Ошибка! Вы должны дать согласие на обработку персональных данных.</p>
    {% endif %}
    {% if validation_error==1 %}
        <p>{{ Lang.get('forms.ans_validation') }}</p>
    {% endif %}
    {% if data_error==1 %}
        <p>{{ Lang.get('forms.ans_data_error') }}</p>
    {% endif %}
    {% if fileupload_error==1 %}
        <p>{{ Lang.get('forms.ans_upload') }}</p>
    {% endif %}
    {% if file_maxsize_error==1 %}
        <p>{{ Lang.get('forms.ans_maxsize') }}</p>
    {% endif %}
    {% if filetype_error==1 %}
        <p>{{ Lang.get('forms.ans_format') }}</p>
    {% endif %}
    {% if handler_not_found==1 %}
        <p>{{ Lang.get('forms.ans_handler') }}</p>
    {% endif %}

</div>

```