<?php

namespace skewer\build\Design\Zones;

use skewer\build\Component\Section\Parameters;
use skewer\build\Component\Section\Tree;

use skewer\build\Design\Frame;
use yii\helpers\ArrayHelper;

/**
 * Библиотека методов работы с редактором зон
 * Class Api
 * @package skewer\build\Design\Zones
 */
class Api {

    /** имя группы с хранилищем зон */
    const layoutGroupName = '.layout';

    /** имя параметра с названием шаблона */
    const layoutTitleName = '.title';

    /** имя параметра со значением для сортировки шаблонов */
    const layoutOrderName = '.order';

    /** имя параметра для хранения доступных меток отображения */
    const layoutParamName = 'layout';

    /** значение веса - не связанный */
    const weightNone = 0;

    /** значение веса - родительский */
    const weightParent = 1;

    /** значение веса - текуущий */
    const weightCurrent = 2;

    /**
     * Отдает id родительского раздела шаблонов
     * @return int
     */
    protected static function getTplRootSection() {
        return (int)\Yii::$app->sections->templates();
    }

    /**
     * Отдает id родительского раздела шаблонов
     * @return array
     */
    protected static function getRootSections() {
        return \Yii::$app->sections->getValues('root');
    }

    /**
     * Отдает набор доступных шаблонов
     * @static
     * @param string $sShowUrl
     * @return array
     */
    public static function getTplList( $sShowUrl = '/' ) {

        // запросить все параметры со спец метками
        $aParamList = Parameters::getList()
            ->group( self::layoutGroupName )
            ->name( self::layoutTitleName )
            ->asArray()
            ->get()
        ;

        $aOrderParams = Parameters::getList()
            ->group( self::layoutGroupName )
            ->name( self::layoutOrderName )
            ->asArray()
            ->get()
        ;

        $aOrderList = [];

        foreach ( $aOrderParams as $aOrderItem )
            $aOrderList[(int)$aOrderItem['parent']] = (int)$aOrderItem['value'];

        // контейнер для шаблонов
        $aTplList = array();

        // вычислить id текущей страницы
        $iShowSectionId = self::getSectionIdByPath( $sShowUrl );

        // получить иерархию разделов наследования в виде набора разделов
        $aParentList = Parameters::getParentTemplates( $iShowSectionId );

        // флаг "основной шаблон найден"
        $bFoundMainTpl = false;

        // перебрать все параметры
        foreach ( $aParamList as $aParam ) {

            // id раздела
            $iSestionId = (int)$aParam['parent'];

            // вес
            if ( $iSestionId===$iShowSectionId ) {
                $iWeight = self::weightCurrent;
                $bFoundMainTpl = true;
            }elseif ( in_array($iSestionId,$aParentList) ) {
                $iWeight = self::weightParent;
            } else {
                $iWeight = self::weightNone;
            }

            // добавить в выходной массив
            $aTplList[$iSestionId] = array(
                'id' => $iSestionId,
                'title' => $aParam['value'],
                'weight' => $iWeight,
                'order' => isset($aOrderList[$iSestionId]) ? $aOrderList[$iSestionId] : 1000000+$aParam['id']
            );

        }

        // если не найден основной шаблон - взять первый родительский
        if ( !$bFoundMainTpl ) {
            foreach ( $aParentList as $iParentId ) {
                if ( isset( $aTplList[$iParentId] ) ) {
                    $aTplList[$iParentId]['weight'] = self::weightCurrent;
                    break;
                }
            }
        }

        // исключть корневой раздел
        foreach(self::getRootSections() as $iRootSectionId)
            if ( isset($aTplList[$iRootSectionId]) )
                unset($aTplList[$iRootSectionId]);

        // сортировка
        uasort( $aTplList, array(__CLASS__, 'tplSort') );

        return array_values( $aTplList );

    }

    /**
     *
     * @param array $a
     * @param array $b
     * @return mixed
     */
    protected static function tplSort($a,$b) {
        return $a['order'] - $b['order'];
    }

    /**
     * Отдает набор зон для заданного шаблона
     * @static
     * @param $iTplId
     * @return array
     */
    public static function getZoneList( $iTplId ) {

        $iTplId = (int)$iTplId;

        $aParamList = Parameters::getList($iTplId)
            ->group(self::layoutGroupName)
            ->fields([
                'id',
                'parent',
                'group',
                'name',
                'value',
                'title',
                'access_level'
            ])
            ->asArray()
            ->rec()
            ->get();

        $aOut = [];

        if ($aParamList){

            // перебрать все записи
            foreach ( $aParamList as $aParam ) {

                // не включать служебные параметры
                if ( in_array($aParam['name'], array(self::layoutTitleName, self::layoutOrderName) ) )
                    continue;

                // занести в выходной массив с разделением на собственные и наследованные
                $aOut[] = array(
                    'id' => $aParam['id'],
                    'title' => $aParam['title'],
                    'own' => (int)$aParam['parent']===$iTplId
                );

            }
        }

        return $aOut;

    }

    /**
     * Удаление зоны
     * @static
     * @param int $iZoneId
     * @param int $iTplId
     * @throws \Exception
     * @return int
     */
    public static function deleteZone( $iZoneId, $iTplId ) {

        if ( in_array( (int)$iTplId, self::getRootSections() ) )
            throw new \Exception('Удаление зон из основных настроек запрещено');

        $oParam = Parameters::getById($iZoneId);
        if ($oParam && $oParam->parent == $iTplId)
            return $oParam->delete();

        return 0;

    }

    /**
     * Отдает
     * @static
     * @param int $iZoneId
     * @param int $iTplId
     * @return array
     * @throws \Exception
     */
    public static function getLabelList( $iZoneId, $iTplId ) {

        // получить запись с зоной
        $aParam = self::getZoneRow( $iZoneId );

        // флаг собственных параметров
        $bOwn = (int)$aParam->parent === $iTplId;

        // набор меток
        $aLabelList = explode(',', $aParam->value);

        // набор связей метка-модуль
        $aLabelToModuleName = self::getLabelToModuleNameArray( $iTplId );

        // собрать набор меток
        $aOut = array();
        foreach ( $aLabelList as $sLabelName ) {
            if ( !$sLabelName )
                continue;

            // имя модуля
            $sModule = isset($aLabelToModuleName[$sLabelName]) ? $aLabelToModuleName[$sLabelName] : $sLabelName;
            $sModule = preg_replace( '/Module$/', '', $sModule );

            $aOut[] = array(
                'name' => $sLabelName,
                'title' => Frame\Api::getModuleTitleByName( $sModule ),
                'own' => $bOwn
            );
            
        }

        // отдать набор меток
        return $aOut;

    }

    /**
     * Список возможных для добавления элементов
     * @static
     * @param int $iZoneId
     * @param int $iTplId
     * @return array
     */
    public static function getAddLabelList( $iZoneId, $iTplId ) {

        $aOut = array();

        // имя зоны
        $sZoneName = self::getZoneName( $iZoneId );

        // список подключенных меток
        $aSetLabelList = self::getAllLabelsForTpl( $iTplId );

        // набор связей метка-модуль
        $aLabelToModuleName = self::getLabelToModuleNameArray( $iTplId );

        $aParamList = Parameters::getList( $iTplId )
            ->groups()
            ->rec()
            ->asArray()
            ->get();

        foreach ( $aParamList as $sLabelName => $aLabelParams ) {

            // пропустить корневую
            if ( $sLabelName === Parameters::settings )
                continue;

            $aLabelParams = ArrayHelper::index($aLabelParams, 'name');
            // ищем группы с заданным параметром положения
            if ( !isset($aLabelParams[self::layoutParamName]) or !$aLabelParams[self::layoutParamName]['value'] )
                continue;

            // доступные для отображения зоны
            $aAllowedForGroup = explode(',', $aLabelParams[self::layoutParamName]['value']);

            // если есть в списке терущая группа
            if ( in_array($sZoneName, $aAllowedForGroup) ) {

                // имя модуля
                $sModule = isset($aLabelToModuleName[$sLabelName]) ? $aLabelToModuleName[$sLabelName] : $sLabelName;

                $aOut[] = array(
                    'name' => $sLabelName,
                    'title' => Frame\Api::getModuleTitleByName( $sModule ),
                    'own' => !in_array( $sLabelName, $aSetLabelList )
                );


                $aAllowedLabels[] = $sLabelName;
            }

        }

        return $aOut;

    }


    /**
     * Проверяет принадлежность зоны к шаблону.
     * создает новую запись если чужая и возвращает id
     * @static
     * @param $iZoneId
     * @param $iTplId
     * @throws \Exception
     * @return int
     */
    public static function getZoneForTpl( $iZoneId, $iTplId ) {

        // получить запись с зоной
        $aParam = self::getZoneRow( $iZoneId );

        // приведение типов
        $iTplId = (int)$iTplId;

        // если параметр принадлежит текущему шаблону - выйти
        if ( (int)$aParam->parent === $iTplId )
            return $iZoneId;

        // Копируем
        $aParam = Parameters::copyToSection($aParam, $iTplId);

        return $aParam->id;

    }

    /**
     * Отдает id зоны по имени и номеруц шаблона
     * @static
     * @param string $sZoneName
     * @param int $iTplId
     * @return int
     */
    public static function getZoneIdByName( $sZoneName, $iTplId ) {

        // запросить параметр рекурсивно по шаблонам
        $aParam = Parameters::getByName( $iTplId, self::layoutGroupName, $sZoneName, true );

        return $aParam ? $aParam->id : 0;

    }

    /**
     * Сортирует набор меток для зоны
     * @static
     * @param string[] $aLabels
     * @param int $iZoneId
     * @param int $iTpl
     * @throws \Exception
     * @return int
     */
    public static function saveLabels( $aLabels, $iZoneId, $iTpl ) {

        // получить запись с зоной
        $oParam = self::getZoneRow( $iZoneId );

        // Делаем наследование
        if ($oParam->parent != $iTpl){
            $oParam = Parameters::copyToSection($oParam, $iTpl);
        }

        // убрать повторяющиеся
        $aLabels = array_unique($aLabels);

        // сборка параметра для сохранения
        $oParam->value = implode(',',$aLabels);

        // сохранение записи
        $oParam->save();
        return $oParam->id;

    }

    /**
     * Удаляет метку по имени
     * @static
     * @param int $sLabelName
     * @param int $iZoneId
     * @throws \Exception
     * @return bool
     */
    public static function deleteLabel( $sLabelName, $iZoneId ) {

        // получить запись с зоной
        $aParam = self::getZoneRow( $iZoneId );

        // разобрать значение на подстроки
        $aLabels = explode(',', $aParam->value);

        // если параметр есть
        if ( in_array( $sLabelName, $aLabels ) ) {

            // удалить параметр
            unset( $aLabels[array_search( $sLabelName, $aLabels )] );

            // сборка параметра для сохранения
            $aParam->value = implode(',', $aLabels);

            // сохранение записи
            $aParam->save();
            return (bool)$aParam->id;

        } else {
            return false;
        }

    }

    /**
     * Отдает запись с зоной или выбрасывает исключение
     * @static
     * @param $iZoneId
     * @return \skewer\models\Parameters
     * @throws \Exception
     */
    protected static function getZoneRow( $iZoneId ) {

        // запросить параметр
        $aParam = Parameters::getById( $iZoneId );
        if ( !$aParam )
            throw new \Exception('Зона не обнаружена');
        return $aParam;
    }

    /**
     * Отдает имя зоны или выбрасывает исключение
     * @static
     * @param $iZoneId
     * @return string
     * @throws \Exception
     */
    protected static function getZoneName( $iZoneId ) {

        // запросить параметр
        $aParam = Parameters::getById( $iZoneId );
        if ( !$aParam )
            throw new \Exception('Зона не обнаружена');
        return $aParam->name;
    }

    /**
     * Отдает массив связей метка-модуль для заданного раздела
     * @static
     * @param int $iTplId
     * @return array
     */
    private static function getLabelToModuleNameArray( $iTplId ) {

        $aOut = [];

        // взять все параметры
        $aParamList = Parameters::getList( $iTplId )
            ->asArray()
            ->rec()
            ->addOrder('name', 'DESC')
            ->groups()
            ->get();

        // перебрать их
        foreach ( $aParamList as $sGroup => $GroupParams ) {
            // пропустить корневую
            if ( $sGroup === Parameters::settings )
                continue;

            foreach ( $GroupParams as $aLabelParams ) {

                // ищем группы с заданным параметром модуля
                if ( $aLabelParams['name'] == Parameters::object && $aLabelParams['value'] ){
                    $aOut[$sGroup] = $aLabelParams['value'];
                }

                // ищем группы с заданным заголовком
                if ( $aLabelParams['name'] == self::layoutTitleName && $aLabelParams['value'] ){
                    $aOut[$sGroup] = $aLabelParams['value'];
                }

            }
        }

        return $aOut;

    }

    /**
     * Отдает все подключенные метки для шаблона
     * @static
     * @param $iTplId
     * @return array
     */
    private static function getAllLabelsForTpl( $iTplId ) {

        // запросить все параметры
        $aAllParamList = Parameters::getList( $iTplId )
            ->asArray()
            ->rec()
            ->groups()
            ->get();

        // проверить наличие обязательного параметра
        if ( !isset($aAllParamList[self::layoutGroupName]) )
            return array();

        // параметры служебной группы
        $aLayoutParams = $aAllParamList[self::layoutGroupName];

        // выходной массив
        $aOut = array();

        // собрать все метки
        foreach ( $aLayoutParams as $sParamName => $aParam ) {

            // служебный параметр пропускаем
            if ( $sParamName === self::layoutTitleName )
                continue;

            // значение
            $sValue = $aParam['value'];
            if ( !$sValue )
                continue;

            $aOut = array_merge($aOut, explode(',', $sValue));

        }

        // отдать набор уникальных значений
        return array_unique($aOut);

    }

    /**
     * Отдает id раздела по пути
     * @static
     * @param $sShowUrl
     * @return int
     */
    public static function getSectionIdByPath( $sShowUrl ) {

        // достаем путь из url
        $sPath = ArrayHelper::getValue( parse_url($sShowUrl), 'path', '/' );

        if ( $sPath==='/' )
            return \Yii::$app->sections->main();

        // отдать id страницы
        return (int)Tree::getSectionByPath( $sPath, $s, \Yii::$app->sections->getDenySections() );

    }

    /**
     * Отдает id шаблона для заданного url
     * @static
     * @param string $sShowUrl
     * @return int
     */
    public static function getTplIdByPath( $sShowUrl ) {

        $iShowId = self::getSectionIdByPath( $sShowUrl );

        // получить иерархию разделов наследования в виде набора разделов
        $aParentList = Parameters::getParentTemplates($iShowId);

        // добавить к списку текущий раздел
        array_unshift($aParentList,$iShowId);

        // найти первый раздел с определенной зоной layout
        while ( $iSectionId = array_shift($aParentList) ) {
            if ( Parameters::getByName( $iSectionId, self::layoutGroupName, self::layoutTitleName, false ) )
                return $iSectionId;
        }

        return 0;

    }

}
