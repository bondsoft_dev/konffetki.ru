<?php

namespace skewer\build\Design\CSSEditor;

use skewer\build\Component\UI;
use skewer\build\Cms;

/**
 * Модуль работы с редактором пользовательского css кода
 */
class Module extends Cms\Tabs\ModulePrototype {

    protected $name = 'CSS Редактор'; /** @fixme почему тут русский текст? */

    /** @var string режим отображения (по-умолчанию или pda версия) */
    protected $sViewMode = \Design::versionDefault;

    /**
     * Пользовательская функция инициализации модуля
     */
    protected function onCreate() {
        $this->addJSListener( 'urlChange', 'checkVersion' );
    }

    protected function actionCheckVersion() {

        // получить тип отображения
        list( $sUrl ) = $this->get('params');
        if ( !$sUrl ) throw new \Exception('Url не задан');
        $sType = \Design::getVersionByUrl( $sUrl );

        // если не совпадает с текущим
        if ( $sType !== $this->sViewMode ) {
            $this->sViewMode = $sType;
            $this->actionInit();
        }

    }

    /**
     * Метод, выполняемый в начале обработки
     */
    protected function preExecute() {

        // тип отображения сайта
        $this->sViewMode = \Design::getValidVersion( $this->getStr('type', $this->sViewMode) );

        // протестировать наличие необходимх файлов
        $this->testFileAccess();

    }

    /**
     * Состаяние. Выбор корневого набора разделов
     * @return bool
     */
    protected function actionInit() {

        // объект для построения списка
        $oForm = new \ExtForm();

        $aItems = array();

        /* Файл */
        $aItems['css_text'] = array(
            'name' => 'css_text',
            'title' => 'Использовать локальные настройки',
            'view' => 'text_css',
            'value' => $this->getValFromFile(),
            'hideLabel' => true,
            'heightInPanel' => true,
            'height' => '100%',
            'margin' => '0 1 0 0',
            //'disabled' => true,
        );

        $oForm->setInitParam( 'autoScroll', false );

        $oForm->addBtnSave();
        $oForm->addBtnCancel();

        $oForm->setFields($aItems);
        $oForm->setValues(array());

        $this->setInterface($oForm);

    }

    /**
     * Состаяние. Выбор корневого набора разделов
     * @throws \Exception
     * @return bool
     */
    protected function actionSave() {

        // значение для сохранения
        $sText = (string)$this->getInDataVal( 'css_text' );

        // имя файла
        $sFilePath = \Design::getAddCssFilePath($this->sViewMode);

        // если данные есть
        if ( $sText ) {

            // создать
            $handle = fopen($sFilePath, 'w+');

            // записать
            fwrite($handle, $sText);

            // сохранить
            fclose($handle);

        } else {

            // удалить файл
            if ( file_exists($sFilePath) )
                unlink($sFilePath);

        }

        // перегрузить фрейм отображения
        $this->fireJSEvent('reload_display_form');

        // открыть заново формуы
        $this->actionInit();

    }

    /**
     * Установка служебных данных
     * @param UI\State\BaseInterface $oIface
     */
    protected function setServiceData( UI\State\BaseInterface $oIface ) {
        $sTitle = sprintf('%s: %s сайта', $this->name, \Design::getVersionTitle( $this->sViewMode ));
        $oIface->setPanelTitle($sTitle);
    }

    /**
     * Проверяет наличие файлов и доступность их на запись
     */
    protected function testFileAccess() {

        Api::testFileAccess();

    }

    /**
     * Отдает данные из нужного файла
     * @throws \Exception
     * @return string
     */
    protected function getValFromFile() {

        $sFileName = \Design::getAddCssFilePath( $this->sViewMode );
        if ( file_exists( $sFileName ) )
            return (string)file_get_contents( $sFileName );
        else
            return '';

    }

}
