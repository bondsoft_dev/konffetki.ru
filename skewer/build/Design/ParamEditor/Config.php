<?php

$aConfig['name']     = 'ParamEditor';
$aConfig['title']    = '';
$aConfig['version']  = '1.0';
$aConfig['description']  = '';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::DESIGN;
$aConfig['languageCategory']     = 'design';

return $aConfig;
