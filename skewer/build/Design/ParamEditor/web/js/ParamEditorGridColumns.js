/**
 * Продублирован код Ext.grid.property.Grid
 * Внесены изменения по составу полей
 */
Ext.define('Ext.Design.ParamEditorGridColumns', {

    extend: 'Ext.grid.header.Container',

    nameWidth: 115,

    // private - strings used for locale support
    nameText : designLang.paramsNameTitle,
    valueText : designLang.paramsValueTitle,
    dateFormat : 'dd.mm.yy',//todo m/j/Y
    trueText: 'true',
    falseText: 'false',

    // private
    nameColumnCls: Ext.baseCSSPrefix + 'grid-property-name',

    /**
     * Creates new HeaderContainer.
     */
    constructor : function(grid, store) {
        var me = this;

        me.grid = grid;
        me.store = store;
        me.callParent([{
            items: [{
                width: 50,
                menuDisabled :true,
                itemId: 'grid-ac',
                xtype: 'actioncolumn',
                align: 'center',
                items: [{
                    getClass: function() {
                        return 'icon-reload';
                    },
                    handler: function(grid, rowIndex) {
                        var panel = this.up('gridpanel');
                        var rec = panel.store.getAt(rowIndex);
                        if ( panel.onRevert )
                            panel.onRevert( rec.data );
                        return false;
                    }
                },
                    {
                        getClass: function(value, meta, rec) {
                            var panel = this.up('gridpanel');
                            var name = rec.data.name;
                            var active = panel.actives[name];
                            if (active !== null) {

                                if (active === '1') {
                                    return 'icon-linkbreak';
                                }
                                else if (active === '0') {
                                    return 'icon-link';
                                }
                            }

                        },
                        handler: function(grid, rowIndex) {

                            var panel = this.up('gridpanel');
                            var rec = panel.store.getAt(rowIndex);
                            var name = rec.data.name;
                            var active = panel.actives[name];

                            var rootCont = processManager.getMainContainer(grid);
                            processManager.setData(rootCont.path,{cmd: 'activeLink', active: active, name: name});
                            processManager.postData();
                        }
                    }

                ]
            }, {
                header: me.nameText,
                width: grid.nameColumnWidth || me.nameWidth,
                sortable: true,
                dataIndex: grid.nameField,
                renderer: Ext.Function.bind(me.renderProp, me),
                itemId: grid.nameField,
                menuDisabled :true,
                tdCls: me.nameColumnCls
            }, {
                header: me.valueText,
                renderer: Ext.Function.bind(me.renderCell, me),
                getEditor: Ext.Function.bind(me.getCellEditor, me),
                flex: 1,
                fixed: true,
                dataIndex: grid.valueField,
                itemId: grid.valueField,
                menuDisabled: true
            }]
        }]);
    },

    getCellEditor: function(record){
        return this.grid.getCellEditor(record, this);
    },

    // private
    // Render a property name cell
    renderProp : function(v) {
        return this.getPropertyName(v);
    },

    // private
    // Render a property value cell
    renderCell : function(val, meta, rec) {
        var me = this,
            renderer = me.grid.customRenderers[rec.get(me.grid.nameField)],
            result = val;

        if (renderer) {
            return renderer.apply(me, arguments);
        }
        if (Ext.isDate(val)) {
            result = me.renderDate(val);
        } else if (Ext.isBoolean(val)) {
            result = me.renderBool(val);
        }
        return Ext.util.Format.htmlEncode(result);
    },

    // private
    renderDate : Ext.util.Format.date,

    // private
    renderBool : function(bVal) {
        return this[bVal ? 'trueText' : 'falseText'];
    },

    // private
    // Renders custom property names instead of raw names if defined in the Grid
    getPropertyName : function(name) {
        var pn = this.grid.propertyNames;
        return pn && pn[name] ? pn[name] : name;
    }

});
