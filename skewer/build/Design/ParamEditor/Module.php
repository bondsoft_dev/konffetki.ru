<?php

namespace skewer\build\Design\ParamEditor;

use skewer\build\Cms;


/**
 * Класс для редактирования набора параметров дизайнерского режима
 * Class Module
 * @package skewer\build\Design\ParamEditor
 */
class Module extends Cms\Frame\ModulePrototype {

    /**
     * Состаяние. Выбор корневого набора разделов
     * @return bool
     */
    protected function actionInit() {

        // команда инициализации
        $this->setCmd('init');

        $this->addLibClass('ParamEditorGrid');
        $this->addLibClass('ParamEditorGridColumns');

    }


    /**
     * Состаяние. Выбор корневого набора разделов
     * @param null $iGroupId
     * @return bool
     */
    protected function actionLoadItems( $iGroupId=null ) {

        // id группы
        if ( is_null($iGroupId) )
            $iGroupId = $this->getInt( 'groupId' );

        // команда отображения списка
        $this->setCmd('loadItems');

        // запросить данные для вывода
        $this->setData('items', \DesignManager::getParamsByGroup( $iGroupId ) );
        $this->setData('groupId',$iGroupId);
    }

    /**
     * Обновление параметра
     */
    protected function actionUpdParam() {

        // входной набор эдементов
        $iId = $this->getStr('id');
        $sValue = $this->getStr('value');

        // сохранить
        $bRes = \DesignManager::saveCSSParamValue( $iId, $sValue );
        if ( $bRes ) {
            /**
             * #22585 убрал автообновление в режиме дизайнера
             */
            //$this->addMessage('Значение сохранено');
            //$this->actionLoadItems( DesignManager::getGroupByParam($iId) );
            $this->fireJSEvent('reload_show_frame');
            \CacheUpdater::setUpdFlag( \CacheUpdater::type_css );
        } else {
            $this->addError('Значение не сохранено');
        }


    }

    /**
     * Сбрасывает значение параметра на стандартное
     */
    protected function actionRevertParam() {

        // входной набор эдементов
        $iId = $this->getInt('id');

        // откатить
        if ( \DesignManager::revertSCCParam( $iId ) )
            $this->actionLoadItems();

        $this->fireJSEvent('reload_show_frame');
        \CacheUpdater::setUpdFlag( \CacheUpdater::type_css );

    }


    /**
     * Сохраняет значение отступов при перетаскивании мышкой блоков в шапке
     */
    protected function actionSaveCssParams() {

        $aData = $this->get('data');
        $aParamH = \DesignManager::getParam( $aData['paramPath'].'.h_value' );
        if($aParamH){
            $aParamH['value'] = ($aData['hValue']*1)."px";
            \DesignManager::saveCSSParam($aParamH);
        }

        $aParamV = \DesignManager::getParam( $aData['paramPath'].'.v_value' );
        if($aParamV){
            $aParamV['value'] = ($aData['vValue']*1)."px";
            \DesignManager::saveCSSParam($aParamV);
        }

        \CacheUpdater::setUpdFlag( \CacheUpdater::type_css );
        $this->fireJSEvent('reload_show_frame');

    }

    protected function actionActiveLink() {

        $name = $this->get('name');
        $active = $this->get('active');

        \DesignManager::setActiveParamRefs($name, $active);

        $this->fireJSEvent('reload_param_editor');
        $this->fireJSEvent('reload_show_frame');
        \CacheUpdater::setUpdFlag( \CacheUpdater::type_css );
    }
}
