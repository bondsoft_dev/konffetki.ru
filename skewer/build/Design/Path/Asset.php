<?php
namespace skewer\build\Design\Path;

use yii\web\AssetBundle;

class Asset extends AssetBundle
{
    public $sourcePath = '@skewer/build/Design/Path/web';
}
