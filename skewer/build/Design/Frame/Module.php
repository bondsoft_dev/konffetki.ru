<?php

namespace skewer\build\Design\Frame;


use skewer\build\Component\Section\Parameters;


/**
 * Class Module
 * @package skewer\build\Design\Frame
 */
class Module extends \skModule {

    public function init(){

        $this->setParser(parserPHP);

    }// func

    public function execute() {

        // установка директории для шаблонов
        $this->setData('lang', ucfirst(\Yii::$app->i18n->getTranslateLanguage()));
        $this->setData('moduleDir',$this->getModuleWebDir());
        $this->setData('ver',\Design::getLastUpdatedTime());

        // проверка прав доступа к дизайнерскому режиму
        if ( !\CurrentAdmin::allowDesignAccess() ) {
            $this->setTemplate('denied.twig');
            return psComplete;
        }

        switch ( (string)$this->getStr('mode') ) {

            // страница загрузки
            case 'loading':
                $this->setTemplate('loading.php');

                break;

            // панель редактора
            case 'editor':

                $oProcessSession = new \skProcessSession();
                $sTicket = $oProcessSession->createSession();

                $this->setData('sessionId',$sTicket);
                $this->setData('dictVals', json_encode($this->getDictVals()) );
                $this->setTemplate('editor.php');

                break;

            // запрос пунктов контекстного меню
            case 'menu':

                // Реинициализация ExtJS - если были сброшены assets, то восстанавливаются
                \Ext::init();

                // сборка меню
                $aMenu = array();
                $sVersion = $this->getStr('version','default');
                $iSectionId = $this->getInt('sectionId');
                $aGroupList = \DesignManager::getGroupList( $sVersion, array('id','name','title') );
                foreach ( $aGroupList as $aGroup ) {
                    $aMenu[$aGroup['name']] = array(
                        'id' => $aGroup['id'],
                        'title' => $aGroup['title'],
                    );
                }

                echo json_encode(array(
                    'menu'=>$aMenu,
                    'modules' => $this->getModuleTitltes( $iSectionId )
                ));
                return psRendered;

                break;

            // окно с 2 панелями: отображения и редактора
            default:

                $this->setTemplate('index.php');

                break;

        }

        // глобальных флаг активации дизайнерского режима
        \Design::setModeGlobalFlag();

        return psComplete;
    }// func

    /**
     * Отдает массив названий админских модулей для раздела
     * В качестве ключей идут метки
     * @param $iSectionId
     * @return array
     */
    protected function getModuleTitltes( $iSectionId ) {

        $aOut = array();

        $aParamList = Parameters::getList( $iSectionId )
            ->fields(['name', 'value'])
            ->asArray()
            ->name(Parameters::object)
            ->rec()
            ->asArray()
            ->get();

        foreach ( $aParamList as $aParam )
            if ( $aParam['value'] )
                $aOut[ $aParam['group'] ] = Api::getModuleTitleByName( $aParam['value'] );

        return $aOut;

    }

    public function shutdown(){

    }// func

    /**
     * Отдает набор языковых метод для работы интерфейса
     * @return array()
     */
    private function getDictVals() {
        return $this->parseLangVars([
            'fileBrowserSelect',
            'fileBrowserFile',
            'galleryBrowserSelect',
            'delRowHeader',
            'delRow',
            'delRowNoName',
            'allowDoHeader',
            'confirmHeader',
            'clear',
            'start',
            'end',
            'editorCloseConfirmHeader',
            'editorCloseConfirm',
            'error',
        ]);
    } // func

}// class
