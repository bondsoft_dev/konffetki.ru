<?php

namespace skewer\build\Design\Frame;

/**
 * Библиотека классов для дизайнерского режима
 */
class Api {

    /**
     * Отдает название модуля по имени
     * @static
     * @param string $sModule
     * @return string
     */
    public static function getModuleTitleByName( $sModule ) {

        if ( $sModule === 'content' )
            return 'Основной модуль страницы';

        if ( \Yii::$app->register->moduleExists( $sModule, \Layer::PAGE ) ) {
            return \Yii::$app->register->getModuleConfig( $sModule, \Layer::PAGE )->getTitle();
        }

        return $sModule;

    }

}
