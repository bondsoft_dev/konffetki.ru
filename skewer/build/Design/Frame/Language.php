<?php

$aLanguage = array();

$aLanguage['ru']['Frame.Design.tab_name'] = 'Дизайнерский режим';
$aLanguage['ru']['design_page_title'] = 'Дизайнерский режим';
$aLanguage['ru']['reseted'] = 'Настройки дизайна сброшены';


$aLanguage['en']['Frame.Design.tab_name'] = 'Designer mode';
$aLanguage['en']['design_page_title'] = 'Designer mode';
$aLanguage['en']['reseted'] = 'Design setting was reseted';

return $aLanguage;