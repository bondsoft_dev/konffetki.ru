<?php

namespace skewer\build\Design\ParamPanel;

use skewer\build\Cms;


/**
 * Модуль для вывода панели с редакторм параметров
 * Class Module
 * @package skewer\build\Design\ParamPanel
 */
class Module extends Cms\Frame\ModulePrototype {

    public function execute() {

        $this->addChildProcess(new \skContext('tree','skewer\build\Design\ParamTree\Module',ctModule,array()));
        $this->addChildProcess(new \skContext('params','skewer\build\Design\ParamEditor\Module',ctModule,array()));

        return psComplete;
    }

}
