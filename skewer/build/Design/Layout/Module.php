<?php

namespace skewer\build\Design\Layout;

use skewer\build\Cms;


/**
 * Модуль вывода основного интерфейсного контейнера
 * Class Module
 * @package skewer\build\Design\Layout
 */
class Module extends Cms\Frame\ModulePrototype {

    public function execute() {

        $this->addChildProcess(new \skContext('head','skewer\build\Design\Header\Module',ctModule,array()));
        $this->addChildProcess(new \skContext('tabs','skewer\build\Design\Tabs\Module',ctModule,array()));

        return psComplete;
    }

}
