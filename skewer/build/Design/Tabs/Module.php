<?php

namespace skewer\build\Design\Tabs;

use skewer\build\Cms;
use skewer\build\Component\Section\Parameters;



/**
 * Модуль для вывода нескольких интерфейсов в дизайнерском режиме
 * Class Module
 * @package skewer\build\Design\Tabs
 */
class Module extends Cms\Frame\ModulePrototype {

    /**
     * Первичная инициализация
     * @return int
     */
    public function actionInit() {

        $this->addChildProcess(new \skContext('param_panel','skewer\build\Design\ParamPanel\Module',ctModule,array()));
        $this->addChildProcess(new \skContext('templates_panel', 'skewer\build\Design\Templates\Module', ctModule, array()));
        $this->addChildProcess(new \skContext('css_panel','skewer\build\Design\CSSEditor\Module',ctModule,array()));
        $this->addChildProcess(new \skContext('zone_panel','skewer\build\Design\Zones\Module',ctModule,array()));

        return psComplete;
    }

    /**
     * Добавляет модуль
     */
    protected function actionAddModule() {

        $sLabelName = $this->getStr( 'labelName' );
        $iSectionId = $this->getInt( 'sectionId', \Yii::$app->sections->main() );

        // запросить параметр в метке с админкой
        $aParam = Parameters::getByName( $iSectionId, $sLabelName, Parameters::objectAdm, true );

        // если параметр есть
        if ( $aParam ) {

            // добавление модуля
            $this->addModule( $sLabelName, $aParam['value'], $iSectionId );

            return;

        }

        // запросить параметр в обычного объекта
        $aParam = Parameters::getByName( $iSectionId, $sLabelName, Parameters::object, true );
        if ( $aParam ) {
            // попробовать скомпоновать админское имя
            $sModuleName = preg_replace( '/PageModule$/', 'AdmModule', $aParam['value'] );

            if ( $this->hasAdmModuleInSection( $sModuleName, $iSectionId ) ) {
                $this->addModule( $sModuleName, $sModuleName, $iSectionId );
                return;
            }

        }

        $this->addError('Модуль управления не найден');

    }

    /**
     * Добавляет модуль как подчиненный
     * @param $sLabelName
     * @param $sModuleName
     * @param $iSectionId
     */
    protected function addModule( $sLabelName, $sModuleName, $iSectionId ) {

        $sObjName = 'module_' . $sLabelName;

        if ( !class_exists($sModuleName) )
            $sModuleName = \Module::getClassOrExcept($sModuleName, \Layer::ADM);

        // создать
        $this->addChildProcess(new \skContext($sObjName,$sModuleName,ctModule,array()));

        // добавить параметр "id раздела"
        $process = $this->getChildProcess($sObjName);
        $process->addRequest('sectionId', $iSectionId);

        // инструкции для интерфейсной части
        $this->setCmd('load_module');
        $this->setData('tabPath',$process->getLabelPath());

    }

    /**
     * Удаляет набор модулей
     */
    protected function actionDelModule() {

        // набор путей для удаления
        $aList = $this->get('items');
        if ( !is_array($aList) )
            throw new \Exception('Неверный формат посылки');

        $sCurrentPath = $this->oContext->oProcess->getLabelPath();

        // перебираем все
        foreach ( $aList as $sPath ) {

            // процесс должен быть подчиненным
            if ( strpos( $sPath, $sCurrentPath.'.' )!==0 )
                continue;

            $sLabel = substr( $sPath, strlen($sCurrentPath)+1 );

            $this->removeChildProcess( $sLabel );

        }

        // инструкции для интерфейсной части
        $this->setCmd('close_module');
        $this->setData('list',$aList);

    }

    /**
     * Определяет подключен ли модуль в разделе
     * @param string $sModuleName
     * @param int $iSectionId
     * @return bool
     */
    private function hasAdmModuleInSection( $sModuleName, $iSectionId ) {

        $aParamList = Parameters::getList( $iSectionId )
            ->fields(['name', 'value'])
            ->name(Parameters::objectAdm)
            ->asArray()
            ->rec()
            ->get();

        foreach ( $aParamList as $aParam )
            if ( $aParam['value']===$sModuleName )
                return true;

        return false;

    }

    /**
     * Добавление редактора
     */
    protected function actionAddEditor() {

        $sEditorId = $this->getStr( 'editorId' );
        $iSectionId = $this->getInt( 'sectionId', \Yii::$app->sections->main() );

        $sObjName = 'editor_'.str_replace(array('/','.'),'_',$sEditorId);

        $lang = Parameters::getLanguage($iSectionId);
        if ($lang)
            \Yii::$app->language = $lang;

        // создать
        $this->addChildProcess(new \skContext($sObjName,'skewer\build\Adm\Editor\SimpleModule',ctModule,array()));

        // добавить параметр "id раздела"
        $process = $this->getChildProcess($sObjName);
        $process->addRequest('sectionId', $iSectionId);
        $process->addRequest('editorId',  $sEditorId);

        // инструкции для интерфейсной части
        $this->setCmd('load_module');
        $this->setData('tabPath',$process->getLabelPath());

    }

}
