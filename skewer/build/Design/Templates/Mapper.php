<?php

namespace skewer\build\Design\Templates;


class Mapper extends \skMapperPrototype {

    /** @var string Текущая таблица */
    protected static $sCurrentTable = 'css_templates';

    /** @var array Список полей */
    protected static $aParametersList = array(
        'id' => 'i:hide:ID',
        'name' => 's:str:Имя шаблона',
        'current' => 'i:check:Текущий'
    );
}