<?php
namespace skewer\build\Design\Templates;

use yii\web\AssetBundle;

class Asset extends AssetBundle
{
    public $sourcePath = '@skewer/build/Design/Templates/web';
}
