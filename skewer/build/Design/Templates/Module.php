<?php

namespace skewer\build\Design\Templates;

use skewer\build\Cms;
use yii\base\UserException;


/**
 * Модуль редактирования шаблонов настроек дизайнерского режима
 * Class Module
 * @package skewer\build\Design\Templates
 */
class Module extends Cms\Frame\ModulePrototype {

    /** @var string имя вкладки */
    protected $sTabName = 'Шаблоны';

    /** @var string режим отображения (по-умолчанию или pda версия) */
    protected $sViewMode = \Design::versionDefault;

    /**
     * Список шаблонов и исключений
     * @return void
     */
    protected function actionInit() {

        $this->addLibClass('TemplatesList');
        $this->addLibClass('TemplatesRefs');

        $this->setData('exceptions', Api::getExceptions());
        $templates = Api::getTemplates();
        if (isset($templates['items'])) {
            $this->setData('templates', $templates['items']);
        }

        $this->setCmd('init');
    }

    /**
     * Восстанавливает связь с базовыми настройками
     * @return void
     */
    protected function actionRevertLink() {

        $ids = $this->get('ids');
        list($ancestor, $descendant) = explode('_', $ids);

        $ancestorParam = \CssParamsMapper::getItem($ancestor);
        $descendantParam = \CssParamsMapper::getItem($descendant);

        $ancestorName = (isset($ancestorParam['name'])) ? $ancestorParam['name'] : false;
        $descendantName = (isset($descendantParam['name'])) ? $descendantParam['name'] : false;

        Api::revertLink($ancestorName, $descendantName);

        $this->fireJSEvent('reload_show_frame');
        \CacheUpdater::setUpdFlag();
        $this->fireJSEvent('reload_param_editor');

        $this->actionInit();
    }

    /**
     * Возвращает список исключений
     * @return void
     */
    protected function actionGetItems() {

        $this->setData('exceptions', Api::getExceptions() );
        $this->setCmd('load_tpl');
    }

    /**
     * Добавляет новый шаблон
     * @return void
     */
    protected function actionAddTemplate(){

        $name = $this->get('name');
        if ($name) {
            Api::addTemplate($name);
        }

        $this->actionInit();
    }

    /**
     * Установить шаблон текущим
     * @return void
     */
    protected function actionSetCurrent(){

        $templateId = $this->get('id');
        Api::setCurrent($templateId);

        $this->fireJSEvent('reload_show_frame');
        \CacheUpdater::setUpdFlag();
        $this->fireJSEvent('reload_param_editor');

        $this->actionInit();
    }

    /**
     * Удаление шаблона
     * @throws UserException
     */
    protected function actionDelTemplate(){

        $templateId = $this->get('id');
        $template = Mapper::getItem($templateId);

        if (!$template)
            throw new UserException('Шаблон не найден');

        if ($template['current'])
            throw new UserException('Нельзя удалить текущий шаблон');

        Api::delTemplate($templateId);

        $this->actionInit();
    }

    /**
     * Обновляет имя шаблона
     * @throws UserException
     */
    protected function actionUpdTemplate(){

        $data = $this->get('data');

        if ( !$data )
            throw new UserException('Нет данных для обновления');

        Mapper::saveItem($data);

        $this->actionInit();
    }
}