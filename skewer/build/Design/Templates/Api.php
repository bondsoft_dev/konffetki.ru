<?php

namespace skewer\build\Design\Templates;


class Api {

    /** @var array Кэш для групп */
    private static $groups = [];

    /**
     * Возвращает список исключений
     * @return array
     */
    public static function getExceptions(){

        $query = "SELECT `p1`.`title` AS `basic`,
                         `p2`.`title` AS `extend`,
                         `p1`.`name` AS `p_basic`,
                         `p2`.`name` AS `p_extend`,
                         CONCAT(`p1`.`id`,'_',`p2`.`id`) AS `link_ids`,
                         `p1`.`group` AS `ancestor_group`,
                         `p2`.`group` AS `descendant_group`
                    FROM `css_data_references` AS `cdr`
              INNER JOIN `css_data_params` AS `p1`
                      ON `cdr`.`ancestor`=`p1`.`name`
              INNER JOIN `css_data_params` AS `p2`
                      ON `cdr`.`descendant`=`p2`.`name`
                   WHERE `cdr`.`active`=0;";

        $oResult = \skewer\build\Component\orm\Query::SQL( $query );

        $exceptions = array();
        while ( $aRow = $oResult->fetchArray() ) {

            $aRow['extend'] = implode(' - ', array_reverse(self::getParamTitlePathAsArray($aRow['descendant_group']))).' - '.$aRow['extend'];
            $aRow['basic'] = implode(' - ', array_reverse(self::getParamTitlePathAsArray($aRow['ancestor_group']))).' - '.$aRow['basic'];

            $exceptions[] = $aRow;
        }

        return $exceptions;
    }

    /**
     * Возвращает путь к параметру
     * @param $groupId
     * @return array
     */
    public static function getParamTitlePathAsArray($groupId){

        $groups = [];

        $group = [];
        $group['parent'] = $groupId;

        do{

            if (isset(self::$groups[$group['parent']])){
                $group = self::$groups[$group['parent']];
            }else{
                $oResult = \skewer\build\Component\orm\Query::SQL(
                    "SELECT `id`,`title`,`parent` FROM `css_data_groups` WHERE `id`=:parent;",
                    array( 'parent' => $group['parent'] )
                );

                $group = $oResult->fetchArray();
                if ($group)
                    self::$groups[$group['id']] = $group;
            }

            if ($group)
                $groups[] = $group['title'];

        }while($group['parent']);

        return $groups;
    }

    /**
     * Восстанавливает связь параметров
     * @param $ancestor
     * @param $descendant
     * @return bool
     */
    public static function revertLink($ancestor, $descendant){

        $oResult = \skewer\build\Component\orm\Query::SQL(
            "UPDATE `css_data_references` SET `active`=1 WHERE `ancestor`=:ancestor AND `descendant`=:descendant;",
            array(
                'ancestor' => $ancestor,
                'descendant' => $descendant
            )
        );

        return (bool)$oResult->affectedRows();
    }

    /**
     * Добавляет шаблон
     * @param $name
     * @return int
     */
    public static function addTemplate($name){

        $templateId = (int)\skewer\build\Component\orm\Query::SQL(
            "INSERT INTO `css_templates` (`name`) VALUES (:name);",
            array( 'name' => $name )
        )->lastId();

        self::copyParamsToTpl($templateId);
        self::copyReferenceToTpl($templateId);

        return $templateId;
    }

    /**
     * Возвращает список шаблонов
     * @return array
     */
    public static function getTemplates(){
        return Mapper::getItems();
    }

    /**
     * Отдает id текущего шаблона или 0, если не выбран
     * @return int
     */
    public static function getCurrentTplId() {

        $oResult = \skewer\build\Component\orm\Query::SQL(
            "SELECT `id` FROM `css_templates` WHERE `current`=1 LIMIT 1"
        );

        $oldCurrentTemplateId = 0;
        if ( $row = $oResult->fetchArray() )
            $oldCurrentTemplateId = (int)$row['id'];

        return $oldCurrentTemplateId;

    }

    /**
     * Делает шаблон текущим
     * @param $newTplId
     */
    public static function setCurrent( $newTplId ) {

        $oldTplId = self::getCurrentTplId();

        if ( $oldTplId ) {

            //Чистим данные предыдущего текущего шаблона
            self::clearTemplate($oldTplId);

            // Копируем текущие данные в текущий шаблон
            self::copyDataToTpl($oldTplId);

        }

        //Чистим таблицы с данными о связах
        self::clearReferences();

        //Копируем данные из нового текущего шаблона
        self::copyDataFromTpl($newTplId);

        //Делаем выбранный шаблон текущим
        if ( $oldTplId )
            \skewer\build\Component\orm\Query::SQL( "UPDATE `css_templates` SET `current`=0;" );

        \skewer\build\Component\orm\Query::SQL(
            "UPDATE `css_templates` SET `current`=1 WHERE `id`=:template_id;",
            array( 'template_id' => $newTplId )
        );
    }

    /**
     * Удаляет шаблон
     * @param $templateId
     */
    public static function delTemplate($templateId){
        Mapper::delItem($templateId);
        self::clearTemplate($templateId);
    }

    /**
     * Копирует данные из шаблона
     * @param $templateId
     */
    private static function copyDataFromTpl($templateId){

        self::copyParamsTableFromTemplate($templateId);
        self::copyReferencesTableFromTemplate($templateId);
    }

    /**
     * Копирует данные в шаблон
     * @param $templateId
     */
    private static function copyDataToTpl($templateId){
        self::copyParamsToTpl($templateId);
        self::copyReferenceToTpl($templateId);
    }

    /**
     * Копирует текущие css параметры в указанный шаблон
     * @param $templateId
     */
    private static function copyParamsToTpl($templateId){

        \skewer\build\Component\orm\Query::SQL(
            "INSERT `css_templates_params`
                  SELECT null AS `id`,t.* , :template_id AS `template_id`
                    FROM `css_data_params` AS `t`",
            array( 'template_id' => $templateId )
        );
    }

    /**
     * Копирует данные о текущих связах в указанный шаблон
     * @param $templateId
     */
    private static function copyReferenceToTpl($templateId){

        \skewer\build\Component\orm\Query::SQL(
            "INSERT `css_templates_references`
                  SELECT null,
                         `ancestor`,
                         `descendant`,
                         `active`,
                         :template_id
                    FROM `css_data_references`",
            array( 'template_id' => $templateId )
        );

    }

    /**
     * Копирует параметры из указанного шаблона в текущие
     * @param $templateId
     */
    private static function copyParamsTableFromTemplate($templateId){

        \skewer\build\Component\orm\Query::SQL(
            "UPDATE `css_data_params` AS `csp`
                    JOIN `css_templates_params` AS `ctp`
                      ON `ctp`.`name`=`csp`.`name`
                     SET `csp`.`value`=`ctp`.`value`
                   WHERE `ctp`.`template_id`=:template_id",
            array( 'template_id' => $templateId )
        );
    }

    /**
     * Копирует связи из указанного шаблона в текущие
     * @param $templateId
     */
    private static function copyReferencesTableFromTemplate($templateId){

        \skewer\build\Component\orm\Query::SQL(
            "INSERT `css_data_references`
                  SELECT `t`.`ancestor`,`t`.`descendant`,`t`.`active`
                    FROM `css_templates_references` AS `t`
                   WHERE `t`.`template_id`=:template_id",
            array( 'template_id' => $templateId )
        );
    }

    /**
     * Очищает таблицы
     * @return void
     */
    private static function clearReferences(){

        \skewer\build\Component\orm\Query::SQL(
            "TRUNCATE TABLE `css_data_references`"
        );

    }

    /**
     * Очищает данные шаблона
     * @param $templateId
     */
    private static function clearTemplate($templateId){

        \skewer\build\Component\orm\Query::SQL(
            "DELETE FROM `css_templates_params` WHERE `template_id`=:template_id",
            array( 'template_id' => $templateId )
        );

        \skewer\build\Component\orm\Query::SQL(
            "DELETE FROM `css_templates_references` WHERE `template_id`=:template_id",
            array( 'template_id' => $templateId )
        );
    }
}