Ext.define('Ext.Design.Templates', {

    extend: 'Ext.panel.Panel',
    title: 'Шаблоны',
    region: 'center',
    items: [],
    layout: 'border',
    refs: null,
    tpls: null,

    initComponent: function(){

        // заккоментирован, поскольку была гонка при перетаскивании
        //      модулей в диз режиме и не страбатывал сброс кэша
        //      модуль стоит в очереди на удаление #34068
        //processManager.addEventListener('reload_show_frame',this.path, this.reloadShowFrame, this);

        this.items = [
            this.tpls = Ext.create('Ext.Design.TemplatesList', {path: this.path}),
            this.refs = Ext.create('Ext.Design.TemplatesRefs', {path: this.path})
        ];

        this.callParent();
    },

    execute: function( data, cmd ){

        switch ( cmd ) {

            case 'init':

                this.path = data.path;
                this.refs.loadData( data.exceptions );
                this.tpls.loadData( data.templates );
                break;

            case 'load_tpl':

                this.refs.loadData( data.exceptions );
                break;
        }

        this.setLoading( false );
        this.refs.setLoading( false );
        this.tpls.setLoading( false );

        return true;
    },

    reloadShowFrame: function (){
        this.refs.setLoading( true );
        processManager.setData(this.path,{cmd: 'getItems'});
        processManager.postData();
    },

    addTemplate: function() {

        //console.log(arguments);
    }

});