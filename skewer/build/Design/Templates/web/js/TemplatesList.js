Ext.define('Ext.Design.TemplatesList', {

    extend: 'Ext.panel.Panel',
    region: 'north',
    html: 'Выпадающие списки и прочее',
    height: '40%',
    path: null,
    dockedItems: [{
        xtype: 'toolbar',
        dock: 'top',
        items: [
            {
                text: 'Создать',
                iconCls: 'icon-add',
                handler: function(btn) {

                    var cont = btn.up('panel');
                    Ext.MessageBox.prompt('Новый шаблон', 'Имя шаблона:', function(btn, text){
                        if (btn == 'ok' && text) {

                            processManager.setData(cont.path,{cmd: 'addTemplate', name: text});
                            processManager.postData();
                        }
                    });
                }
            }
        ]
    }],

    initComponent: function() {

        var me = this;

        this.store = Ext.create('Ext.data.ArrayStore', {
            fields: [
                {name: 'id'},
                {name: 'name'},
                {name: 'current'}
            ],
            data: this.exceptions || []
        });

        var grid = Ext.create('Ext.grid.Panel', {
            store: this.store,
            columns: [
                {
                    text: 'Шаблон',
                    flex: 1,
                    sortable: false,
                    menuDisabled: true,
                    dataIndex: 'name'
                },
                {
                    xtype: 'actioncolumn',
                    sortable: false,
                    menuDisabled: true,
                    width: 50,
                    align: 'center',
                    dataIndex: 'current',
                    getClass: function(value, meta, rec) {

                        if (rec.get('current')) {
                            return 'icon-star'
                        }
                        else return 'icon-star-inactive';
                    },
                    handler: function(grid, rowIndex) {

                        processManager.getProcess( me.path).setLoading( true );

                        var rec = grid.getStore().getAt(rowIndex);
                        if (!rec.get('current')) {
                            processManager.setData(me.path,{cmd: 'setCurrent', id: rec.get('id')});
                            processManager.postData();
                        }
                    }
                },
                {
                    xtype: 'actioncolumn',
                    sortable: false,
                    menuDisabled: true,
                    width: 50,
                    align: 'center',
                    dataIndex: 'id',
                    items: [
                        {
                            iconCls: 'icon-edit',
                            handler: function(grid, rowIndex) {

                                var rec = grid.getStore().getAt(rowIndex);

                                Ext.MessageBox.prompt('Новый шаблон', 'Имя шаблона:', function(btn, text){
                                    if (btn == 'ok' && text) {

                                        var data = {};
                                        data.name = text;
                                        data.id = rec.get('id');
                                        processManager.setData(me.path,{cmd: 'updTemplate', data: data });
                                        processManager.postData();
                                    }
                                }, null, false, rec.get('name'));
                            }
                        },
                        {
                            iconCls: 'icon-delete',
                            handler: function(grid, rowIndex) {

                                var rec = grid.getStore().getAt(rowIndex);
                                if (!rec.get('current')) {
                                    processManager.setData(me.path,{cmd: 'delTemplate', id: rec.get('id')});
                                    processManager.postData();
                                }
                                else {
                                    sk.error('Нельзя удалить текущий шаблон');
                                }
                            }
                        }
                    ]
                }
            ],
            height: '100%',
            width: '100%'
        });

        this.items = [ grid ];

        this.callParent();
    },

    loadData: function( items ) {
        this.store.loadData( items );
    }
});
