Ext.define('Ext.Design.TemplatesRefs', {

    extend: 'Ext.panel.Panel',
    title: 'Исключения',
    items: [],
    region: 'center',
    exceptions: [],
    path: null,

    initComponent: function() {

        var me = this;

        this.store = Ext.create('Ext.data.ArrayStore', {
            fields: [
                {name: 'ancestor_title'},
                {name: 'descendant_title'},
                {name: 'link_ids'}
            ],
            data: this.exceptions || []
        });

        var grid = Ext.create('Ext.grid.Panel', {
            store: this.store,
            columns: [
                {
                    text: 'Расширенные настройки',
                    flex: 1,
                    sortable: false,
                    menuDisabled: true,
                    dataIndex: 'extend'
                },
                {
                    text: 'Базовые настройки',
                    flex: 1,
                    menuDisabled: true,
                    sortable: false,
                    dataIndex: 'basic'
                },
                {
                    xtype: 'actioncolumn',
                    sortable: false,
                    menuDisabled: true,
                    width: 50,
                    align: 'center',
                    iconCls: 'icon-reload',
                    dataIndex: 'link_ids',
                    handler: function(grid, rowIndex) {

                        var rec = grid.getStore().getAt(rowIndex);
                        processManager.setData(me.path,{cmd: 'revertLink', ids: rec.data.link_ids});
                        processManager.postData();
                    }
                }
            ],
            height: '100%',
            width: '100%'
        });

        this.items = [ grid ];

        this.callParent();
    },

    loadData: function( items ) {
        var me = this;
        processManager.getProcess( me.path).setLoading( true );
        this.store.loadData( items );
    }
});