<?php

$aConfig['name']     = 'Templates';
$aConfig['title']    = 'Шаблоны';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Шаблоны настроек дизайна';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::DESIGN;
$aConfig['languageCategory']     = 'design';

return $aConfig;