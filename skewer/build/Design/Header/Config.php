<?php

$aConfig['name']     = 'Header';
$aConfig['title']    = 'Редактор CSS';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Редактор CSS';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::DESIGN;
$aConfig['languageCategory']     = 'design';

return $aConfig;
