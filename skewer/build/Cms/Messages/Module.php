<?php

namespace skewer\build\Cms\Messages;

use skewer\build\Cms;


class Module extends Cms\Frame\ModulePrototype {

    protected function actionInit() {
        $this->setCmd('init');
        $this->checkMsg();
    }

    protected function actionUpdate() {
        $this->setCmd( 'update' );
        $this->checkMsg();
    }

    private function checkMsg(){

        $unreadMessages = \skewer\build\Tool\Messages\Api::getUnreadMessages();
        $unreadMessages['text'] = \skewer\build\Tool\Messages\Api::getMessagesSuffix($unreadMessages['count']);
        $body = \skParser::parseTwig('view.twig', $unreadMessages, BUILDPATH.'Cms/Messages/templates/');
        $this->setCmd('init');
        $this->setData('message', $body);
    }

}