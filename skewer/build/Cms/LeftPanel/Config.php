<?php

$aConfig['name']     = 'LeftPanel';
$aConfig['title']    = 'Левая панель';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Левая панель';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::CMS;
$aConfig['languageCategory'] = 'adm';

return $aConfig;
