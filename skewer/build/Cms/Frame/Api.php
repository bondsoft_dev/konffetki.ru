<?php

namespace skewer\build\Cms\Frame;
use yii\helpers\ArrayHelper;


/**
 * Class Api
 * @package skewer\build\Cms\Footer
 */
class Api {

    public static function isValidBrowser(){

        $oBrowser = new \skBrowser();
        $sBrowser = $oBrowser->getBrowser();

        $iValidVersion = ArrayHelper::getValue(\Yii::$app->params['browser'], $sBrowser, 0);

        if ( !$iValidVersion || $oBrowser->getVersion()<$iValidVersion ) return false;

        return true;
    }

}//class