<?php
/**
 * @var int $sessionId
 * @var string $layoutMode
 * @var string $moduleDir
 * @var string $dictVals
 * @var string $ver
 * @var string $lang
 *
 * @var \yii\web\View $this
 */

$bundle = skewer\build\Cms\Frame\Asset::register($this);

$this->beginPage()
?><!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title><?

        if ($layoutMode == 'fileBrowser' or $layoutMode == 'designFileBrowser')
            echo \Yii::t('adm', 'file_page_title');
        elseif ( $layoutMode == 'galleryBrowser' )
            echo \Yii::t('gallery', 'gallery_browser_title');
        else
            echo \Yii::t('adm', 'admin_page_title');

        ?> - <?= Site::getSiteTitle() ?></title>

    <link rel="shortcut icon" href="<?= Design::get('page','favicon') ?>" type="image/png" />

    <script type="text/javascript">
        var sessionId = '<?= $sessionId ?>';
        var layoutMode = '<?= $layoutMode ?>';
        var lang = '<?= $lang ?>';
        var dict = <?= $dictVals ?>;

        var rootPath = '<?= $bundle->baseUrl.'/js' ?>';
        var extJsDir = '<?= $this->getAssetManager()->getBundle(skewer\assets\ExtJsAsset::className())->baseUrl ?>';
        var pmDir = '<?= $this->getAssetManager()->getBundle(skewer\assets\ExtJsProcessManagerAsset::className())->baseUrl ?>';
        var ckedir = '<?= $this->getAssetManager()->getBundle(skewer\assets\CkEditorAsset::className())->baseUrl ?>';

    </script>

    <?php $this->head() ?>

</head>
<body>

<?php $this->beginBody() ?>

    <div id="js_admin_preloader" class="admin-preloader">
        <img src="<?= $bundle->baseUrl ?>/img/preloader.gif" />

    </div>

    <form id="history-form" class="x-hide-display">
        <input type="hidden" id="x-history-field" />
        <iframe id="x-history-frame"></iframe>
    </form>

<?php $this->endBody() ?>

</body>
</html>

<?php $this->endPage() ?>
