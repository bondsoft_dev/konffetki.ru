<?php

namespace skewer\build\Cms\Frame;

use skewer\build\Component\Site;


/**
 * Class Module
 * @package skewer\build\Cms\Frame
 */
class Module extends \skModule {

    public function init(){

        $this->setParser(parserPHP);
        $this->setTemplate('index.php');
        
    }

    public function execute() {

        if ( !Api::isValidBrowser() )
            $this->setTemplate('not_valid.php');

        $oProcessSession = new \skProcessSession();
        $sTicket = $oProcessSession->createSession();

        $this->setData('sessionId',$sTicket);
        $this->setData('layoutMode',$this->getStr('mode','Cms'));
        $this->setData('moduleDir',$this->getModuleWebDir());
        $this->setData('dictVals', json_encode($this->getDictVals()) );
        $this->setData('ver',\Design::getLastUpdatedTime());
        $this->setData('lang', \Yii::$app->i18n->getTranslateLanguage());

        return psComplete;
    }// func

    public function shutdown(){

    }

    /**
     * Отдает набор языковых метод для работы интерфейса
     * @return array()
     */
    private function getDictVals() {
        return $this->parseLangVars([
            'fileBrowserSelect',
            'fileBrowserFile',
            'galleryBrowserSelect',
            'delRowHeader',
            'delRowsHeader',
            'delRow',
            'delRowNoName',
            'delRowsNoName',
            'allowDoHeader',
            'confirmHeader',
            'clear',
            'start',
            'end',
            'editorCloseConfirmHeader',
            'editorCloseConfirm',
            'error',
            'ajax_error'
        ]);
    } // func

}// class
