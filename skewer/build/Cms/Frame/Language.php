<?php

$aLanguage = array();

$aLanguage['ru']['Frame.Cms.tab_name'] = 'Управление сайтом';
$aLanguage['ru']['admin_page_title'] = 'Управление сайтом';
$aLanguage['ru']['file_page_title'] = 'Выбор файла на сервере';
$aLanguage['ru']['browser_h1'] = 'Данный сайт не поддерживается Вашим браузером.';
$aLanguage['ru']['browser_text'] = 'Пожалуйста обновите свой браузер или воспользуйтесь альтернативными браузерами';

$aLanguage['ru']['fileBrowserSelect'] = 'Выбрать';
$aLanguage['ru']['fileBrowserFile'] = 'Файл';
$aLanguage['ru']['galleryBrowserSelect'] = 'Галерея';
$aLanguage['ru']['ajax_error'] = 'Ошибка при отправке запроса (';

$aLanguage['ru']['add'] = 'Добавить';
$aLanguage['ru']['sort'] = 'Отсортировать';
$aLanguage['ru']['del'] = 'Удалить';
$aLanguage['ru']['edit'] = 'Редактировать';
$aLanguage['ru']['err'] = 'Ошибка';
$aLanguage['ru']['upd'] = 'Редактировать';
$aLanguage['ru']['view'] = 'Просмотреть';
$aLanguage['ru']['save'] = 'Сохранить';
$aLanguage['ru']['cancel'] = 'Отмена';
$aLanguage['ru']['reset'] = 'Сбросить';
$aLanguage['ru']['clear'] = 'Очистить';
$aLanguage['ru']['back'] = 'Назад';
$aLanguage['ru']['error'] = 'Ошибка';
$aLanguage['ru']['clone'] = 'Дублировать';
$aLanguage['ru']['by_link'] = 'ссылке';
$aLanguage['ru']['change'] = 'Изменить';
$aLanguage['ru']['set_first'] = 'Сделать первым';
$aLanguage['ru']['sort'] = 'Отсортировать';

$aLanguage['ru']['language'] = 'Язык';

$aLanguage['ru']['delRowHeader'] = 'Удаление записи';
$aLanguage['ru']['delRowsHeader'] = 'Удаление записей';
$aLanguage['ru']['delRow'] = 'Удалить ';
$aLanguage['ru']['delRowNoName'] = 'запись';
$aLanguage['ru']['delRowsNoName'] = 'записи';
$aLanguage['ru']['allowDoHeader'] = 'Подтверждение на выполнение!';
$aLanguage['ru']['confirmHeader'] = 'Подтверждение выролнения';
$aLanguage['ru']['clear'] = 'Очистить';
$aLanguage['ru']['start'] = 'Начало';
$aLanguage['ru']['end'] = 'Конец';
$aLanguage['ru']['editorCloseConfirmHeader'] = 'Подтверждение продолжения без сохранения';
$aLanguage['ru']['editorCloseConfirm'] = 'Данные не сохранены. Продолжить?';
$aLanguage['ru']['errorInvalidFied'] = 'Поле `{0}` не прошло валидацию';

$aLanguage['ru']['link_developer'] = 'http://www.web-canape.ru/';
$aLanguage['ru']['link_help'] = 'http://help.web-canape.ru/';

$aLanguage['ru']['by_link'] = 'ссылке';

/* en */

$aLanguage['en']['Frame.Cms.tab_name'] = 'Site management';
$aLanguage['en']['admin_page_title'] = 'Site management';
$aLanguage['en']['file_page_title'] = 'Choose a file on a server';
$aLanguage['en']['browser_h1'] = 'This site is not supported by your browser.';
$aLanguage['en']['browser_text'] = 'Please refresh your browser or use an alternative browser';

$aLanguage['en']['fileBrowserSelect'] = 'Select';
$aLanguage['en']['fileBrowserFile'] = 'File';
$aLanguage['en']['galleryBrowserSelect'] = 'Gallery';
$aLanguage['en']['ajax_error'] = 'Error sending the request (';

$aLanguage['en']['add'] = 'Add';
$aLanguage['en']['del'] = 'Delete';
$aLanguage['en']['edit'] = 'Edit';
$aLanguage['en']['err'] = 'Error';
$aLanguage['en']['upd'] = 'Edit';
$aLanguage['en']['view'] = 'Show';
$aLanguage['en']['save'] = 'Save';
$aLanguage['en']['cancel'] = 'Cancel';
$aLanguage['en']['reset'] = 'Reset';
$aLanguage['en']['clear'] = 'Clear';
$aLanguage['en']['back'] = 'Back';
$aLanguage['en']['error'] = 'Error';
$aLanguage['en']['clone'] = 'Clone';
$aLanguage['en']['by_link'] = 'link';
$aLanguage['en']['change'] = 'Change';
$aLanguage['en']['set_first'] = 'Set first';
$aLanguage['en']['sort'] = 'Sort';

$aLanguage['en']['language'] = 'Language';

$aLanguage['en']['delRowHeader'] = 'Deleting record';
$aLanguage['en']['delRowsHeader'] = 'Deleting records';
$aLanguage['en']['delRow'] = 'delete ';
$aLanguage['en']['delRowNoName'] = 'entry';
$aLanguage['en']['delRowsNoName'] = 'records';
$aLanguage['en']['allowDoHeader'] = 'Confirmation of fulfillment!';
$aLanguage['en']['confirmHeader'] = 'Confirmation';
$aLanguage['en']['clear'] = 'Clear';
$aLanguage['en']['start'] = 'Start';
$aLanguage['en']['end'] = 'End';
$aLanguage['en']['editorCloseConfirmHeader'] = 'Confirm continuation without saving';
$aLanguage['en']['editorCloseConfirm'] = 'The data has not been saved. Will you continue?';
$aLanguage['en']['errorInvalidFied'] = 'Field `{0}` is not valid';

$aLanguage['en']['link_developer'] = 'http://www.web-canape.com/';
$aLanguage['en']['link_help'] = 'http://help.web-canape.com/';

$aLanguage['en']['by_link'] = 'link';

return $aLanguage;