<?php

$aConfig['name']     = 'Frame';
$aConfig['title']    = 'Frame';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Frame';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::CMS;
$aConfig['languageCategory']     = 'adm';

return $aConfig;