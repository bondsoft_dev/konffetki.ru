<?php

namespace skewer\build\Cms\Header;

use skewer\build\Cms;


/**
 * Class Module
 * @package skewer\build\Cms\Header
 */
class Module extends Cms\Frame\ModulePrototype {

    protected function actionInit() {

        $this->addInitParam('renderData' ,[
            'href' => '/admin/',
            'logoImg' => $this->getModuleWebDir().'/img/canape_cms_logo.png'
        ]);

        // добавить панель авторизации
        $this->addChildProcess(new \skContext('auth','skewer\build\Cms\Auth\Module',ctModule,array()));

        $this->addChildProcess(new \skContext('lang','skewer\build\Cms\Lang\Module',ctModule,array()));

        $this->addChildProcess(new \skContext('messages','skewer\build\Cms\Messages\Module',ctModule,array()));

        return psComplete;

    }// func

    public function shutdown(){

    }// func

}// class
