<?php

$aConfig['name']     = 'Header';
$aConfig['title']    = 'Шапка';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Шапка';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::CMS;
$aConfig['languageCategory'] = 'adm';

return $aConfig;