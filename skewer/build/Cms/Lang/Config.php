<?php

/* main */
$aConfig['name']     = 'Lang';
$aConfig['title']    = 'Выбор языка';
$aConfig['version']  = '1.000';
$aConfig['description']  = 'Выбор языка в CMS';
$aConfig['useNamespace']  = true;
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::CMS;
$aConfig['languageCategory'] = 'languages';

return $aConfig;
