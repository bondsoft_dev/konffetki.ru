<?php

namespace skewer\build\Cms\GalleryBrowser;

use skewer\build\Cms;
use skewer\build\Component\Gallery;
/**
 * Модуль для отображения галереи
 * Подчиненные модули:
 * Панель с файлами из основного интерфейса
 * Class Module
 * @package skewer\build\Cms\GalleryBrowser
 */
class Module extends Cms\Frame\ModulePrototype {

    protected function actionInit() {

        // подключаем модули
        $this->addChildProcess(new \skContext('files','skewer\build\Adm\Gallery\Module',ctModule,
            [
                'iCurrentAlbumId' => 0,
                'onlyAlbumEditor' => true,
                'popup' => true,
            ]
        ));

        $this->setCmd('findAlbum');

    }

    protected function actionShowAlbum() {// $this->getChildProcess('file')->getModule()->set('iCurrentAlbumId',33);//->setParam('iCurrentAlbumId',33);

        // TODO ПЕРЕПИСАТЬ
        //$this->set('currentAlbumId',33);
        //$this->getChildProcess('files')->getModule()->set('iCurrentAlbumId',33);
        //var_dump( $this->getChildProcess('files')->getStatus() );
        //$this->getChildProcess('files')->setStatus(psWait);

        $iAlbumId = $this->get('item');
        $format = $this->get('format');

        // если альбом еще не создан для данного объекта
        if (!$iAlbumId) {
            $iAlbumId = Gallery\Album::setAlbum([
                'owner' => 'entity',
                'profile_id' => $format?:Gallery\Format::TYPE_SECTION, // todo сейчас у профилей нет имен!!! нужно ввести и сделать ссылку по имени
                'section_id' => 0 // $this->iSectionId
            ]);
        }

        $this->addChildProcess(new \skContext('files','skewer\build\Adm\Gallery\Module',ctModule,
            [
                'iCurrentAlbumId' => $iAlbumId,
                'onlyAlbumEditor' => true,
                'popup' => true,
            ]
        ));

        $this->setCmd('showAlbum');
        $this->setData('album',$iAlbumId);
    }

}
