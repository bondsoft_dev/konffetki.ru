/**
 * Библиотека для вывода раскладки файловго менеджера
 * TODO не нашел где используется, походу рудемент, может выпилить?
 */
Ext.define('Ext.Cms.GalleryBrowser', {
    extend: 'Ext.Viewport',
    title: 'gallery',
    height: '90%',
    width: '90%',
    layout: 'border',
    closeAction: 'hide',
    modal: true,
    componentsInited: false,

    senderData: {},

    defaults: {
        margin: '3 3 3 3'
    },

    defaultSection: 1,

    items: [{
        region: 'center',
        html: 'viewport'
    }],

    initComponent: function() {

        this.callParent();

        // событие при событии выбора раздела во всплывающем окне
        //processManager.addEventListener('tabs_load', this.path, 'onSectionSelect');

        // событие при выборе файла во всплывающем окне
        //processManager.addEventListener('select_file_set', this.path, 'onGallerySelect');

        processManager.addEventListener('request_add_info',this.path, 'showData');

    },

    showData: function( text ){
        sk.error( text );
    },


    execute: function( data, cmd ) {

        switch ( cmd ) {

            case 'findAlbum':

                var album_id = this.parseUrl('currentAlbumId');
                var format = this.parseUrl('format');

                if ( album_id ) {
                    processManager.setData(this.path,{
                        cmd: 'showAlbum',
                        format: format,
                        item: album_id
                    });
                    processManager.postData();
                }

                break;

            case 'showAlbum':

                var album_id = data['album'];

                this.onGallerySelect(album_id);

                break;
        }

        if ( data.error )
            sk.error( data.error );
    },

    /**
     * При воборе файла во всплывающем окне
     * @param value
     */
    onGallerySelect: function( value ) {

        if ( !window.top.opener )
            return false;

        if ( !window.top.opener['processManager'] )
            return true;

        window.top.opener['processManager'].fireEvent('set_gallery', {
            ticket: this.parseUrl('ticket'),
            value: value
        });
        //window.top.close();
        //window.top.opener.focus();

        return true;

    },

    parseUrl: function(name) {
        name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
        var regexS = "[\\?&]"+name+"=([^&#]*)";
        var regex = new RegExp( regexS );
        var results = regex.exec( window.location.href );
        if (null == results) {
            return '';
        }
        return results[1];
    }

});
