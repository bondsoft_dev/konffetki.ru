<?php

$aConfig['name']     = 'GalleryBrowser';
$aConfig['title']    = 'Просмотр изображений фотогалереи';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Просмотр изображений фотогалереи';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::CMS;
$aConfig['languageCategory'] = 'gallery';

return $aConfig;