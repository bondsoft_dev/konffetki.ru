<?php

$aConfig['name']     = 'Tabs';
$aConfig['title']    = 'Вкладки';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Вкладки';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::CMS;
$aConfig['languageCategory'] = 'adm';

return $aConfig;