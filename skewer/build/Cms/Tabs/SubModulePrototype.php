<?php

namespace skewer\build\Cms\Tabs;

use skewer\build\Component\UI;


class SubModulePrototype {

    protected $oModule = null;

    function __construct( ModulePrototype $oModule ) {

        // todo проверка на общий namespace

        $this->oModule = $oModule;

        $this->init();

    }

    /**
     * Функция выполняется при инициализации класса
     */
    public  function init() {}


    final protected function setInterface( UI\State\BaseInterface $oInterface ) {
        $this->oModule->setInterface( $oInterface );
    }

    /**
     * Устанавливает название панели
     * @param string $sNewName - новое имя
     * @param bool $bAddTabName - нужно ли добавить в начало имя вкладки
     */
    protected function setPanelName( $sNewName, $bAddTabName = false ){
        $this->oModule->setPanelName( $sNewName, $bAddTabName );
    }

    /**
     * Метод - исполнитель функционала
     */
    public function execute() {
        return false;
    }

    /**
     * Функция получения защищенных параметров основного объекта
     * @param string $sParamName Имя параметра
     * @return null
     */
    public function getParam( $sParamName ) {
        return $this->oModule->getParam( $sParamName );
    }

    /**
     * Функция сохранения защищенных параметров основного объекта
     * @param string $sParamName Имя параметра
     * @param $sValue
     * @return null
     * todo эти функции должны быть вынесены в прототип для составных модулей и пересмотрены с позиции безопасности
     */
    public function setParam( $sParamName, $sValue ) {
        return $this->oModule->setParam( $sParamName, $sValue );
    }


    public function getInData() {
        return $this->oModule->getInData();
    }

}