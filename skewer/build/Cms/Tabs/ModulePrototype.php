<?php

namespace skewer\build\Cms\Tabs;

use skewer\build\Component\UI;
use skewer\build\Cms;

/**
 * Протитип модулей на основе автопостроителя
 */
abstract class ModulePrototype extends Cms\Frame\ModulePrototype {

    /**
     * Имя модуля
     * @var string
     */
    protected $sTabName = '';

    /**
     * Отдает название модуля
     * @return string
     */
    public function getTitle() {
        if ( !$this->sTabName )
            return $this->title;
        return \Yii::t($this->getCategoryMessage(), $this->sTabName); //используется ли это вариант???
    }

    /**
     * Имя панели
     * @var string
     */
    protected $sPanelName = '';

    /**
     * Флаг использования автоматического построения интерфейсов
     * @var bool
     */
    protected $bUseBuilder = false;

    /** @var UI\ModuleConstructor Построитель интерфейсов */
    protected $oBuilder = null;

    /**
     * Массив внутренних данных.
     * Работает на внутреннем механизме хранения состояний.
     * Данные будут сброшены как только вкладка будет закрыта/перезагружена
     * Для работы используются методы getInnerData, setInnerData, hasInnerData
     * @var mixed[]
     */
    protected $aInnerData = array();


    /**
     * Отдает данные из внутреннего сессионного хранилища.
     * Данные будут сброшены как только вкладка будет закрыта/перезагружена
     * @param string $sName имя параметра
     * @param string $mDefault значение по умолчанию, если не найдено в хранилище
     * @return mixed
     */
    public function getInnerData( $sName, $mDefault='' ) {
        if ( isset($this->aInnerData[$sName]) )
            return $this->aInnerData[$sName];
        else
            return $mDefault;
    }

    /**
     * Отдает данные из внутреннего сессионного хранилища, приведенные к int.
     * Если данных нет, отдает $mDefault как есть, даже если она не инт
     * Данные будут сброшены как только вкладка будет закрыта/перезагружена
     * @param string $sName имя параметра
     * @param mixed $mDefault значение по умолчанию, если не найдено в хранилище
     * @return int
     */
    public function getInnerDataInt( $sName, $mDefault=0 ) {
        if ( $this->hasInnerData($sName) )
            return (int)$this->getInnerData($sName);
        else
            return $mDefault;
    }

    /**
     * Сохраняет данные во внутреннее хранилище значений
     * Данные будут сброшены как только вкладка будет закрыта/перезагружена
     * @param $sName
     * @param $mValue
     */
    public function setInnerData($sName, $mValue) {
        $this->aInnerData[$sName] = $mValue;
    }

    /**
     * Отдает флаг наличия данных во внутреннем хранилище по имени
     * Данные будут сброшены как только вкладка будет закрыта/перезагружена
     * @param string $sName имя параметра
     * @return bool
     */
    public function hasInnerData( $sName ) {
        return isset($this->aInnerData[$sName]);
    }

    /**
     * Устанавливает название панели
     * @param string $sNewName - новое имя
     * @param bool $bAddTabName - нужно ли добавить в начало имя вкладки
     */
    public function setPanelName($sNewName, $bAddTabName = false ){
        $sPrefix = $bAddTabName && $this->getTitle() ? $this->getTitle() : '' ;
        $sDelimiter = $sPrefix && $sNewName ? ': ' : '';
        $this->sPanelName = $sPrefix.$sDelimiter.$sNewName;
    }

    /**
     * Отдает флаг использования автопостроителя
     * @return bool
     */
    public function useBuilder() {

        return $this->bUseBuilder;
    }

    /**
     * Состояние при инициализации вкладки
     */
    public function actionInitTab() {

        $oTab = new \ExtLoadTab();

        // флаг инициализации вкладки
        $this->setData( 'initTabFlag', true );

        $this->setInterface( $oTab );

    }

    /**
     * Установка служебных данных
     * @param UI\State\BaseInterface $oIface
     */
    protected function setServiceData( UI\State\BaseInterface $oIface ) {}

    /**
     * Добавляет данные для вывода в шаблонизатор
     * @param UI\State\BaseInterface $oInterface - модуль, для которого идет вызов
     * @return bool
     */
    final public function setInterface( UI\State\BaseInterface $oInterface ) {

        // установка заголовков
        $oInterface->setTitle( $this->getTitle() );
        $oInterface->setPanelTitle( $this->sPanelName ? $this->sPanelName : $this->getTitle() );

        // установка служебных данных из модуля для передачи
        $this->setServiceData( $oInterface );

        // добавление интерфейсных данных в посылку
        $oInterface->setInterfaceData( $this );

        return true;

    }

    /**
     * Работа с данными
     */

    /**
     * Получить массив пришедших данных, с возможностью фильтрации
     * @param array $aFilter массив имен необходимых полей
     * @param bool $bExclude - флаг исключение указанных полей
     *          true - !все указанные в фильтре поля
     *          false - все что есть в ответе, кроме заданных
     * @return array
     */
    public function getInData( $aFilter=array(), $bExclude=false ){

        // получить данные
        $aData = $this->get('data');
        if ( !is_array($aData) )
            $aData = array();

        // если есть ограничение по полям
        if ( $aFilter ) {
            $aOut = array();

            // если флаг исключения
            if ( $bExclude ) {
                // из полученных полей
                foreach ( $aData as $sName ) {
                    // убрать те, что есть в фильтре
                    if ( !in_array($sName,$aFilter) ) {
                        $aOut[$sName] = (string)$aData[$sName];
                    }
                }
            } else {
                // взять список необходимых полей
                foreach ( $aFilter as $sName ) {
                    // добавить в вывод те поля, которые есть в посылке,
                    // остальные заполнить пустышками
                    $aOut[$sName] = isset($aData[$sName]) ? (string)$aData[$sName] : '';
                }
            }

        } else {
            // не задан фильтр - просто вернуть все, что есть в посылке
            $aOut = $aData;
        }

        return $aOut;

    }

    /**
     * Отдает флаг фозможности создания наследников для данного модуля
     * в дереве процессов
     * @return bool
     */
    protected function canBeParent() {
        return false;
    }

}
