<?php

$aConfig['name']     = 'Auth';
$aConfig['title']    = 'Авторизация';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Авторизация';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::CMS;
$aConfig['languageCategory']     = 'auth';

return $aConfig;