/**
 * Форма авторизации
 */
Ext.define('Ext.Cms.AuthForm', {

    extend: 'Ext.form.Panel',

    title: '',
    bodyPadding: 5,
    width: 350,
    topRatio: 0.4,

    // Fields will be arranged vertically, stretched to full width
    layout: 'anchor',
    defaults: {
        labelWidth: 75,
        anchor: '100%'
    },

    // The fields
    defaultType: 'textfield',
    items: [],
    buttons: [],
    listeners: {
        afterrender: function(me){
            // выбрать первое поле
            me.down('textfield').focus();
        }
    },

    generateItems: function(){
        var me = this;
        me.items = [{
            fieldLabel: me.lang.authLoginTitle,
            name: 'login',
            allowBlank: false
        },{
            fieldLabel: me.lang.authPassTitle,
            name: 'pass',
            inputType: 'password',
            allowBlank: true,
            listeners: {
                specialkey: function(field, e){
                    if (e.getKey() == e.ENTER) {
                        var form = field.up('form');
                        form.onSubmit();
                    }
                }
            }
        }]
    },
    generateButtons: function(){
        var me = this;
        me.buttons = [{
            text: me.lang.authLoginButton,
            formBind: true, //only enabled once the form is valid
            disabled: true,
            handler: function() {
                this.up('form').onSubmit();
            }
        }]
    },

    initComponent: function(){
        var me = this;

        me.title = me.lang.authPanelTitle;

        me.generateButtons();
        me.generateItems();
        me.callParent();
    },

    execute: function( data, cmd ){

        var me = this;

        switch ( cmd ) {

            case 'login':

                // если авторизация не удалась
                if ( !data['success'] ) {
                    sk.error( me.lang.authLoginIncorrect );
                } else {
                    window.location.reload();
                }

                break;

        }

    },

    onSubmit:function(){

        // иниициализация
        var form = this.getForm(),
            values = form.getValues()
        ;

        // собрать посылку
        values.cmd = 'login';
        processManager.setData(form.path,values);

        // отослать
        processManager.postData();

    }

});
