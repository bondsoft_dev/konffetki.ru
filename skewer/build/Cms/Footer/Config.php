<?php

$aConfig['name']     = 'Footer';
$aConfig['title']    = 'Подвал';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Подвал';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::CMS;
$aConfig['languageCategory'] = 'adm';

return $aConfig;
