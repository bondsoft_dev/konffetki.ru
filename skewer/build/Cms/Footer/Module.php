<?php

namespace skewer\build\Cms\Footer;

use skewer\build\Cms;


/**
 * Class Module
 * @package skewer\build\Cms\Footer
 */
class Module extends Cms\Frame\ModulePrototype {

    public function execute() {

        // отдать перекрывающий инициализационный параметр для модуля
        $this->setJSONHeader('init', array(
            'html' => $this->renderTemplate( 'view.twig', array(
                'logoImg' => $this->getModuleWebDir().'/img/logo.png',
                'version' => \Site::getCmsVersion()
            ) )
        ) );

        return psComplete;

    }// func

    public function shutdown(){

    }// func

}// class
