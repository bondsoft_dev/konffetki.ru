<?php

namespace skewer\build\Cms\FileBrowser;

use skewer\build\Cms;


/**
 * Модуль для отображения раскладки фалового менеджера для дизайнерского режима
 * Подчиненные модули:
 *  Панель с файлами из основного интерфейса
 * Class DesignModule
 * @package skewer\build\Cms\FileBrowser
 */
class DesignModule extends Cms\Frame\ModulePrototype {

    protected function actionInit() {

        $this->addChildProcess(new \skContext('files','skewer\build\Adm\Files\DesignBrowserModule',ctModule,array()));

    }

}
