<?php

$aConfig['name']     = 'FileBrowser';
$aConfig['title']    = 'Просмотр файлов';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Просмотр файлов';
$aConfig['revision'] = '0001';
$aConfig['layer']     = Layer::CMS;
$aConfig['languageCategory'] = 'files';

return $aConfig;