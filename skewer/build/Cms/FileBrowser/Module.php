<?php

namespace skewer\build\Cms\FileBrowser;

use skewer\build\Cms;
use skewer\build\Component\Section;
use skewer\build\Component\Section\Tree;
use yii\web\ServerErrorHttpException;


/**
 * Модуль для отображения раскладки фалового менеджера
 * Подчиненные модули:
 *  Дерево из основного интерфейса
 *  Панель с файлами из основного интерфейса
 * Class Module
 * @package skewer\build\Cms\FileBrowser
 */
class Module extends Cms\Frame\ModulePrototype {

    protected function actionInit() {

        // задаем раздел по умолчанию
        $this->addInitParam('defauluSection', \Yii::$app->sections->library());

        // подключаем модули
        $this->addChildProcess(new \skContext('tree','skewer\\build\\Adm\\Tree\\FileBrowserModule',ctModule,array()));
        $this->addChildProcess(new \skContext('files','skewer\\build\\Adm\\Files\\BrowserModule',ctModule,array()));

        $this->setModuleLangValues(array(
            'fileBrowserPanelTitle'=>'fileBrowserPanelTitle'
        ));

        $this->setCmd('init');

    }

    /**
     * Пытается найти id раздела для заданного модуля или создать новый
     */
    protected function actionGetModuleNodeId() {

        // запрашиваем имя модуля
        $sModuleName = $this->get( 'module' );

        // проверяем имя модуля
        if ( !$sModuleName )
            throw new ServerErrorHttpException('Имя модуля не задано');

        // проверяем валидность имени модуля
        // todo после переезда модулей на namespace убрать вторую часть #namespace
        if ( !strpos($sModuleName, '_') and !preg_match('/^[\w]+(Adm|Design|Tool)Module$/',$sModuleName) )
            throw new ServerErrorHttpException('Неверное имя модуля');

        // id раздела библиотек
        $iLibSectionId = \Yii::$app->sections->library();

        // проверяем наличие раздела
        $iSectionId = Tree::getSectionByAlias( $sModuleName, $iLibSectionId );

        // есть - отдать id
        if ( $iSectionId ) {
            $this->setCmd('openNode');
            $this->setData('nodeId',$iSectionId);
        }

        // нет раздела - создать
        else {

            if ( strpos($sModuleName, '_') ) {
                list($sLayer, $sModule) = explode( '_', $sModuleName, 2 );
                $sClassName = sprintf( '\skewer\build\%s\%s\Module', $sLayer, $sModule );

                if ( !class_exists($sClassName) ) {
                    $oConfig = \Yii::$app->register->getModuleConfig( $sModule, $sLayer );
                    if ( $oConfig and !$oConfig->isUseNamespace() )
                        $sClassName = $oConfig->getNameFull();
                }

            } else {
                $sClassName = $sModuleName;
            }

            if ( !class_exists($sClassName) )
                throw new ServerErrorHttpException("Модуль [$sModuleName] не найден");

            /** @var Cms\Tabs\ModulePrototype $oModule объект модуля */
            $oModule = new $sClassName( new \skContext( 'sub', $sClassName, ctModule, array()) );

            // проверяем классовую принадлежность
            if ( !($oModule instanceof Cms\Tabs\ModulePrototype) )
                throw new ServerErrorHttpException("Модуль [$sClassName] должен быть унаследован от интерфейса AdminTabModulePrototype");

            // достаем имя модуля
            $sModuleTitle = $oModule->getTitle();

            $section = Tree::addSection( $iLibSectionId, $sModuleTitle, 0, $sModuleName, Section\Visible::VISIBLE );
            $section->type = Tree::typeDirectory;
            $section->save();
            $iSectionId = $section->id;

            // есть - отдать id
            if ( $iSectionId ) {

                /* Обновление кеша политик доступа */
                \Policy::incPolicyVersion();
                \CurrentAdmin::reloadPolicy();

                $this->setCmd('openNode');
                $this->setData('nodeId',$iSectionId);
            } else {
                throw new ServerErrorHttpException("Ошибка создания новой папки для модуля [$sModuleName]");
            }

        }

    }

}
