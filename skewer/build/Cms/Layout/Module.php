<?php

namespace skewer\build\Cms\Layout;

use skewer\build\Cms;


/**
 * Class Module
 * @package skewer\build\Cms\Layout
 */
class Module extends Cms\Frame\ModulePrototype {

    const labelHeader = 'header';
    const labelLeft = 'left';
    const labelTabs = 'tabs';
    const labelLog = 'log';
    const labelFooter = 'footer';

    public function allowExecute() {
        return true;
    }

    public function execute() {

        $this->addChildProcess(new \skContext(self::labelHeader,'skewer\build\Cms\Header\Module',ctModule,array()));
        $this->addChildProcess(new \skContext(self::labelLeft,'skewer\build\Cms\LeftPanel\Module',ctModule,array()));
        $this->addChildProcess(new \skContext(self::labelTabs,'skewer\build\Cms\Tabs\Module',ctModule,array()));
        if(\CurrentAdmin::isSystemMode())
            $this->addChildProcess(new \skContext(self::labelLog,'skewer\build\Cms\Log\Module',ctModule,array()));
        else
            $this->addChildProcess(new \skContext(self::labelFooter,'skewer\build\Cms\Footer\Module',ctModule,array()));

        return psComplete;

    }// func



}// class
