<?php

$aConfig['name']     = 'Layout';
$aConfig['title']    = 'Сетка';
$aConfig['version']  = '1.0';
$aConfig['description']  = 'Сетка';
$aConfig['revision'] = '0001';
$aConfig['layer']     = \Layer::CMS;
$aConfig['languageCategory'] = 'adm';

return $aConfig;