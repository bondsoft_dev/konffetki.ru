<?php

namespace skewer\models;

use skewer\build\Adm\Tree\Search;
use skewer\build\Component\Section;
use skewer\build\Component\Section\Tree;
use yii\base\Event;
use yii\base\ModelEvent;
use yii\base\UserException;
use yii\db\AfterSaveEvent;
use yii\helpers\FileHelper;
use skewer\build\Component;
use yii\db\ActiveRecord;


/**
 * This is the model class for table "tree_section".
 *
 * @property integer $id
 * @property string $alias
 * @property string $title
 * @property integer $parent
 * @property integer $visible
 * @property integer $type
 * @property integer $position
 * @property string $alias_path
 * @property string $link
 * @property integer $level
 * @property string $last_modified_date
 * @method static TreeSection findOne($condition)
 */
class TreeSection extends ActiveRecord
{

    const EVENT_AFTER_CREATE = 'EVENT_AFTER_CREATE';

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tree_section';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            //[['alias', 'title', 'parent', 'visible', 'type', 'position', 'alias_path', 'link', 'level'], 'required'],
            [['parent', 'visible', 'type', 'position', 'level'], 'integer'],
            [['last_modified_date'], 'safe'],
            [['alias', 'title', 'alias_path', 'link'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'alias' => 'Alias',
            'title' => 'Title',
            'parent' => 'Parent',
            'visible' => 'Visible',
            'type' => 'Type',
            'position' => 'Position',
            'alias_path' => 'Alias Path',
            'link' => 'Link',
            'level' => 'Level',
            'last_modified_date' => 'Last Modified Date',
        ];
    }


    public function setTemplate( $tpl ) {

        // id шаблона
        if ( !Tree::getSection( $tpl ) )
            return false;

        Section\Parameters::setParams( $this->id, Section\Parameters::settings, Section\Parameters::template, $tpl );

        // todo вот тут и должны дублироваться параметры
        // запросить данные
        $aAddParams = Section\Parameters::getList( $tpl )
            ->level( Section\Params\ListSelector::alPos )
            ->fields(['id','title','value','show_val','access_level'])
            ->get();

        // добавить все параметры
        foreach ( $aAddParams as $oParam ) {
            Section\Parameters::copyToSection( $oParam, $this->id );
        }

        /** Нужно выкинуть сообщение о том, что раздел создан уже после копирования в него параметров из его шаблона */
        Event::trigger( self::className(), self::EVENT_AFTER_CREATE, new AfterSaveEvent([
            'sender' => $this,
            'changedAttributes' => $this->getAttributes()
        ]));


        return true;
    }

    public function getTemplate() {

        return Section\Parameters::getTpl( $this->id );
    }


    public function save($runValidation = true, $attributeNames = null) {

        // флаг того, что страница - главная на сайте
        //@todo lang сделать alias_path = '/' для всех главных  на мультиязычных сайтах
        $bMain = in_array( $this->id, \Yii::$app->sections->getValues('main') );

        $this->title = mb_substr( $this->title, 0, 100 );
        $this->last_modified_date = date("Y-m-d H:i:s", time());
        $this->checkAlias();
        $this->checkPosition();
        $this->checkLevel();

        // походу хак для корневых разделов, но главную выводим
        if ( !$this->parent and !$bMain )
            $this->visible = -1;

        // изменить alias_path если изменен alias
        $bChangeAlias = $this->isAttributeChanged('alias') ||
            $this->isAttributeChanged('parent') ||
            $this->isAttributeChanged('visible')
        ;
        if ( $bChangeAlias ) {
            if ( $parent = self::findOne(['id' => $this->parent]) ) {
                $basePath = $parent->alias_path ?: '/';
                $this->alias_path = $this->genAliasPath( $basePath );
            }
        }

        // главная должна быть с корневым url
        if ( $bMain ) {
            $oLangRootSection = self::findOne(['id' => \Yii::$app->sections->getValue(Section\Page::LANG_ROOT, Section\Parameters::getLanguage($this->parent))]);

            if ($oLangRootSection && $oLangRootSection->alias_path){
                $this->alias_path = $oLangRootSection->alias_path;
            }else{
                $this->alias_path = '/';
            }

        }

        $res = parent::save($runValidation, $attributeNames);


        // todo перевести на событийную модель
        \Policy::incPolicyVersion();
        \CurrentAdmin::reloadPolicy();

        // рекурсивное обновление alias_path у дочерних разделов
        if ( $bChangeAlias )
            $this->changeAliasPath( $this->alias_path );

        return $res;

    }


    /**
     * Проверка и геренация псевдонима
     */
    protected function checkAlias() {

        if ( !$this->alias )
            $this->alias = $this->title ?: 'section';

        $alias = \skTranslit::generateAlias( $this->alias );
        $alias = mb_substr( $alias, 0, 60 );

        $i = '';
        do {
            $this->alias = $alias.$i;

            $res = self::find()
                ->andWhere( ['alias' => $this->alias] )
                ->andWhere( ['<>', 'id', (int)$this->id] )
                ->one();

            $i++;
        } while ( $res );

    }


    /**
     * Проверка и генерация веса для сортировки
     */
    protected function checkPosition() {

        // если родительского раздела нет (ни к чему не привязан),
        //      то не перестраиваем позицию
        if ( !$this->parent )
            return true;

        // Если родительский раздел не изменился, то пропускаем
        // Такое может быть при создании нового и при переносе в другой раздел
        if( !$this->isAttributeChanged('parent') )
            return true;

        // если позиция была задана принудительно - пропускаем
        if ( $this->isAttributeChanged('position') )
            return true;

        /** @var self $section */
        $section = self::find()
            ->where( ['parent' => $this->parent] )
            ->orderBy( ['position' => SORT_DESC] )
            ->one();

        $this->position = $section ? $section->position + 1 : 1;

        return true;
    }


    /**
     * Генератор поля alias_path
     * @param string $basePath Значение alias_path родителя
     * @return string
     */
    protected function genAliasPath( $basePath ) {

        if ( $this->visible == Section\Visible::HIDDEN_FROM_PATH ) {
            $path = $basePath;
        } else {
            $path = $basePath . ($this->alias ?: $this->id) . '/';
        }

        return $path;
    }


    /**
     * Рекурсивное обновление alias_path для дочерних разделов
     * @param string $basePath
     */
    protected function changeAliasPath( $basePath = '/' ) {

        foreach ( self::findAll(['parent' => $this->id]) as $section ) {
            $path = $section->genAliasPath( $basePath );
            self::updateAll(['alias_path' => $path], ['id' => $section->id]);
            $section->changeAliasPath( $path );
        }

    }


    /**
     * Перенос ветки разделов по дереву
     * @param TreeSection $section Раздел в который переносится ветка
     * @param string $direction Тип переноса append|before|after
     * @return bool
     * @throws UserException
     */
    public function changePosition( TreeSection $section, $direction = 'append' ) {

        // направление переноса
        switch ( $direction ) {

            // добавить как подчиненный
            case 'append':

                // изменить положение и родителя раздела
                $this->parent = $section->id;
                $this->save();

                break;

            // вставить до/после элемента
            case 'before':
            case 'after':

                // в корень писать нельзя, из него забирать нельзя, но сортировать можно
                if ( $this->parent xor $section->parent )
                    throw new UserException('badData');

                $position = ($direction == 'before') ? $section->position : $section->position + 1;

                $query = self::find()
                    ->where(
                        "`parent` = :parent AND `position` >= :position",
                        [':parent' => $section->parent, ':position' => $position]
                    );

                foreach ( $query->each() as $curSection )
                    $curSection->updateCounters(['position' => 1]);

                $this->parent = $section->parent;
                $this->position = $position;

                if ( $parent = self::findOne(['id' => $this->parent]) ) {
                    $basePath = $parent->alias_path ?: '/';
                    $this->alias_path = $this->genAliasPath( $basePath );
                }

                $this->save();

                $this->changeAliasPath( $this->alias_path );

                break;

            // неподдерживаемый вариант
            default:
                throw new UserException('badData');

        }

        return True;
    }


    /**
     * Возможность доступа текущего админа к разделу
     * @return bool
     */
    public function testAdminAccess() {
        return \CurrentAdmin::canRead( $this->id );
    }


    /**
     * Набор подразделов
     * @return self[]
     */
    public function getSubSections() {
        return self::findAll(['parent' => $this->id]);
    }


    private function checkLevel() {

        if ( !$this->parent || $this->level )
            return true;

        /** @var self $section */
        $section = self::findOne(['id' => $this->parent]);

        $this->level = $section ? $section->level + 1 : 0;

        return true;
    }

    /**
     * @inheritDoc
     */
    public function delete() {

        foreach ( $this->getSubSections() as $oSubSection )
            $oSubSection->delete();

        return parent::delete();
    }

    /**
     * @inheritDoc
     */
    public function beforeDelete() {

        if (parent::beforeDelete()) {

            // сначала рекурсивно удаляем подчиненные разделы
            foreach ( $this->getSubSections() as $oSubSection )
                $oSubSection->delete();

            return true;

        } else {

            return false;

        }

    }

    /**
     * @inheritDoc
     */
    public function afterSave($insert, $changedAttributes) {

        parent::afterSave($insert, $changedAttributes);

        $search = new Search();

        // если поменялись нужные поля - рекурсивно перестроить индекс по дереву
        if ( array_intersect( ['visible', 'alias', 'link'], array_keys($changedAttributes) ) )
            $search->setRecursiveResetFlag();

        $search->updateByObjectId( $this->id );

    }

    /**
     * @inheritDoc
     */
    public function afterDelete() {

        parent::afterDelete();

        /**
         * @todo events!
         */
        $search = new Search();
        $search->deleteByObjectId( $this->id );

    }

    /**
     * Метод, вызываемый при удалении раздела
     * @param ModelEvent $event
     */
    public static function onSectionDelete( ModelEvent $event ) {

        FileHelper::removeDirectory( FILEPATH.$event->sender->id );

        Component\Search\Api::removeFromIndexBySection( $event->sender->id );

    }

    /**
     * Класс для сборки списка автивных поисковых движков
     * @param Component\Search\GetEngineEvent $event
     */
    public static function getSearchEngine( Component\Search\GetEngineEvent $event ) {
        $event->addSearchEngine( Search::className() );
    }

    /**
     * Отдает флаг наличия собственного url у раздела
     *  по которому он может быть открыт
     * @return bool
     */
    public function hasRealUrl() {

        if ($this->link)
            return false;

        return in_array(
            $this->visible,
            Section\Visible::$aOpenByLink
        );
    }

}
