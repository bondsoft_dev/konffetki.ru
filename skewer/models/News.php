<?php

namespace skewer\models;

use skewer\build\Component\Section\Tree;

use Yii;
use yii\data\ActiveDataProvider;
use \yii\base\ModelEvent;
use skewer\build\Adm\News\Search;
use skewer\build\Component;


/**
 * This is the model class for table "news".
 *
 * @property integer $id
 * @property string $news_alias
 * @property integer $parent_section
 * @property string $publication_date
 * @property string $title
 * @property string $announce
 * @property string $full_text
 * @property integer $active
 * @property integer $archive
 * @property integer $on_main
 * @property string $hyperlink
 * @property string $last_modified_date
 *
 * @property string $format_announce
 */
class News extends \yii\db\ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'news';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent_section', 'title'], 'required'],
            [['parent_section', 'active', 'archive', 'on_main'], 'integer'],
            [['publication_date', 'last_modified_date'], 'safe'],
            [['announce', 'full_text'], 'string'],
            [['news_alias', 'title', 'hyperlink'], 'string', 'max' => 255],
            // todo это решение мне тоже не очень нраится, лучше пересмотреть
            [['archive', 'on_main'],'filter','filter'=>function($i){return (bool)$i;}]
            // было так, но это работает только с php >= 5.5
            // [['archive', 'on_main'],'filter','filter'=>'boolval']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app/news', 'ID'),
            'news_alias' => Yii::t('app/news', 'News Alias'),
            'parent_section' => Yii::t('app/news', 'Parent Section'),
            'publication_date' => Yii::t('app/news', 'Publication Date'),
            'title' => Yii::t('app/news', 'Title'),
            'announce' => Yii::t('app/news', 'Announce'),
            'full_text' => Yii::t('app/news', 'Full Text'),
            'active' => Yii::t('app/news', 'Active'),
            'archive' => Yii::t('app/news', 'Archive'),
            'on_main' => Yii::t('app/news', 'On Main'),
            'hyperlink' => Yii::t('app/news', 'Hyperlink'),
            'last_modified_date' => Yii::t('app/news', 'Last Modified Date'),
        ];
    }

    public static function getPublicNewsByAlias( $sNewsAlias )
    {
        return News::findOne(['news_alias'=>$sNewsAlias]);
    }

    public static function getPublicNewsById( $iNewsId )
    {
        return News::findOne(['id'=>$iNewsId]);
    }

    public function getFormat_Announce()
    {
        return  str_replace(
            'data-fancybox-group="button"',
            'data-fancybox-group="news' . $this->id . '"',
            $this->announce
        );
    }

    /**
     * Creates data provider instance with search query applied
     *
     * @param array $params
     *
     * @return ActiveDataProvider
     */
    public static function getPublicList($params)
    {
        $query = News::find()
            ->andFilterWhere(['active'=>1])
            ->orderBy([
                'publication_date'=>($params['order'] == 'DESC') ? SORT_DESC : SORT_ASC
            ])
        ;

        if (!$params['on_main'] && Yii::$app->getRequest()->getQueryParam('per-page')){
            $params['on_page'] = Yii::$app->getRequest()->getQueryParam('per-page');
        }

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => (isset($params['on_page']))?$params['on_page']:10,
                'page'=>$params['page']-1
            ],
        ]);

        if (isset($params['on_page'])) {
            // добавим рулесы в UrlManager для правильного построителя ЧПУ
            // нужно для коректной генерации пагинатора
            News::routerRegister();
        }

        /* Если есть фильтр по дате */
        if ( isSet($params['byDate']) && !empty($params['byDate']) ){
            $query->andFilterWhere(['>','publication_date',$params['byDate'].' 00:00:00']);
            $query->andFilterWhere(['<','publication_date',$params['byDate'].' 23:59:59']);
        }

        if ( !$params['all_news'] && $params['section'] ) {
            $query->andFilterWhere(['parent_section'=>$params['section']]);
        }

        if( $params['on_main'] ) {
            $query->andFilterWhere(['on_main' => 1]);
        }
        if( $params['on_main'] || $params['all_news'] ) {
            /**
             * @todo наверное можно проще
             */
            $aSections = Tree::getAllSubsection(\Yii::$app->sections->languageRoot());
            $query->andFilterWhere(['parent_section' => $aSections]);
        }
        if( $params['archive'] )
            $query->andFilterWhere(['archive'=>1]);

        if ( $params['future'] )
            $query->andFilterWhere(['publication_date','>',date('Y-m-d H:i:s', time())]);

        return $dataProvider;
    }

    public static function routerRegister(){
        $url = Yii::$app->getRequest()->getPathInfo();
        $url = preg_replace('/page(.)*/','${2}',$url);

        Yii::$app->getUrlManager()->addRules([$url.'page/<page:[\w\.]+>' => 'site/index']);
    }

    public static function getNewRow($aData = array()){
        $oRow = new News();

        $oRow->title = \Yii::t('news', 'new_news');
        $oRow->publication_date = date("Y-m-d H:i:s", time());
        $oRow->active = 1;

        $oRow->announce = '';
        $oRow->full_text = '';
        $oRow->hyperlink = '';
        $oRow->on_main = 0;

        if ($aData)
            $oRow->setAttributes($aData);
        return $oRow;
    }

    public function beforeSave($insert) {

        if ( !$this->news_alias )
            $sValue = \skTranslit::change( $this->title );
        else
            $sValue = \skTranslit::change( $this->news_alias );

        // к числам прибавляем префикс
        if ( is_numeric($sValue) ){
            $sValue = 'news-' . $sValue;
        }

        // приводим к нужному виду
        $sValue = \skTranslit::changeDeprecated( $sValue );
        $sValue = \skTranslit::mergeDelimiters( $sValue );
        $sValue = trim($sValue,'-');

        $this->news_alias = self::checkAlias($sValue, $this->id);

        // format wyswyg fields
        if ( $this->full_text && $this->parent_section )
            $this->full_text = \ImageResize::wrapTags( $this->full_text, $this->parent_section );

        if ( $this->announce && $this->parent_section  )
            $this->announce = \ImageResize::wrapTags( $this->announce, $this->parent_section );

        if( $this->hyperlink && (strpos($this->hyperlink,'http') === false) and ($this->hyperlink[0] !== '/') and ($this->hyperlink[0] !== '[') )
            $this->hyperlink = 'http://' . $this->hyperlink;

        if ( !$this->publication_date || ($this->publication_date == 'null') )
            $this->publication_date = date("Y-m-d H:i:s", time());

        return parent::beforeSave($insert);
    }

    /**
     * @inheritDoc
     */
    public function afterSave($insert, $changedAttributes) {

        parent::afterSave($insert, $changedAttributes);

        $oSearch = new Search();
        $oSearch->updateByObjectId($this->id);

    }

    /**
     * @inheritDoc
     */
    public function afterDelete() {

        parent::afterDelete();

        $oSearch = new Search();
        $oSearch->deleteByObjectId($this->id);

    }

    public static function checkAlias( $sAlias, $iId ) {

        if ( !($sAlias) ) $sAlias = date('d-m-Y-H-i');

        $aItems = News::find()->where(['news_alias'=>$sAlias])->all();
        $iSize = count( $aItems );

        if ( !$iSize || ($iSize==1 && ($iId == $aItems[0]->id) ) )
            return $sAlias;

        return self::checkAlias( $sAlias.++$iSize, $iId );
    }

    /**
     * Удаление всех новостей для раздела
     * @param ModelEvent $event
     */
    public static function removeSection( ModelEvent $event ) {
        self::deleteAll(['parent_section' => $event->sender->id]);
    }

    /**
     * Класс для сборки списка автивных поисковых движков
     * @param Component\Search\GetEngineEvent $event
     */
    public static function getSearchEngine( Component\Search\GetEngineEvent $event ) {
        $event->addSearchEngine( Search::className() );
    }

}
