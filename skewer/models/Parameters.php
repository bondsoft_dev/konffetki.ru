<?php

namespace skewer\models;

use skewer\build\Component\Section\Params\Type;
use skewer\build\Component\Section;
use Yii;
use yii\base\Event;
use yii\base\ModelEvent;

/**
 * This is the model class for table "parameters".
 *
 * @property string $id
 * @property integer $parent Раздел
 * @property string $group Группа
 * @property string $name Имя
 * @property string $value Значение
 * @property string $title Название
 * @property integer $access_level Уровень доступа
 * @property string $show_val Текстовое значение
 */
class Parameters extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'parameters';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['parent', 'access_level'], 'integer'],
            [['name', 'parent', 'group'], 'required'],
            [['name'], 'unique', 'targetAttribute' => ['name', 'parent', 'group']],
            [['show_val'], 'string'],
            [['group', 'name'], 'string', 'max' => 24],
            [['value'], 'string', 'max' => 255],
            [['title'], 'string', 'max' => 50],
            [['parent'], 'integer', 'min' => 1],
            ['value', '\skewer\build\Component\Section\Params\TemplateValidator', 'when' => function( $model ){
                /** @var Parameters $model */
                return ( $model->group == Section\Parameters::settings && $model->name == Section\Parameters::template  );
            }]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'parent' => 'Parent',
            'group' => 'Group',
            'name' => 'Name',
            'value' => 'Value',
            'title' => 'Title',
            'access_level' => 'Access Level',
            'show_val' => 'Show Val',
        ];
    }


    /**
     * Список аттрибутов модели
     * @return array
     */
    public static function getAttributeList(){
        return [
            'id',
            'parent',
            'group',
            'name',
            'value',
            'title',
            'access_level',
            'show_val'
        ];
    }


    /**
     * @inheritdoc
     */
    public function save($runValidation = true, $attributeNames = null) {

        $this->parent = (int)$this->parent;
        $this->name = (string)$this->name;
        $this->group = (string)$this->group;
        $this->value = (string)$this->value;
        $this->show_val = (string)$this->show_val;
        $this->title = (string)$this->title;
        $this->access_level = (int)$this->access_level;

        return parent::save($runValidation, $attributeNames);
    }


    /**
     * Проверка на висивиг
     * @return bool
     */
    public function isWysWyg(){
        return ( abs($this->access_level) == Type::paramWyswyg );
    }


    /**
     * Проверка на список
     * @return bool
     */
    public function isSelect(){
        return ( abs($this->access_level) == Type::paramSelect );
    }


    /**
     * Текстовое поле
     * @return bool
     */
    public function hasTextField(){
        return in_array( abs($this->access_level), Type::getTextFieldList());
    }


    /**
     * Использование расширенного значения
     * @return bool
     */
    public function hasUseShowVal(){
        return in_array( abs($this->access_level), Type::getShowValFieldList());
    }


    protected function validateTpl(){

    }


    /**
     * Удаление по разделу
     * @param Event $event
     */
    public static function removeSection( Event $event ){

        self::deleteAll(['parent' => $event->sender->id]);

    }

}
