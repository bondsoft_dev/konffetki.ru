<?php

namespace skewer\models;

use Yii;
use yii\data\ActiveDataProvider;
use yii\db\Query;

/**
 * This is the model class for table "log".
 *
 * @property integer $id
 * @property string $event_time
 * @property integer $event_type
 * @property integer $log_type
 * @property string $title
 * @property string $module
 * @property integer $initiator
 * @property string $ip
 * @property string $proxy_ip
 * @property string $external_id
 * @property string $description
 * @method static Log findOne($condition)
 */
class Log extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'log';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['event_time'], 'safe'],
            [['event_type', 'title', 'ip' ], 'required'],
            [['event_type', 'log_type', 'initiator'], 'integer'],
            [['description'], 'string'],
            [['title', 'module'], 'string', 'max' => 255],
            [['ip', 'proxy_ip'], 'string', 'max' => 255],
            [['external_id'], 'string', 'max' => 20]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id'            => 'field_id',
            'event_time'    => 'field_event_time',
            'event_type'    => 'field_event_type',
            'log_type'      => 'field_log_type',
            'title'         => 'field_title',
            'module'        => 'field_module',
            'initiator'     => 'field_initiator',
            'ip'            => 'field_ip',
            'proxy_ip'      => 'field_proxy_ip',
            'external_id'   => 'field_external_id',
            'description'   => 'field_description',
        ];
    }


    public static function getItemsList($aInputData){

        // маска входного фильтра, он же массив данных для подстановки в запрос

        $f = [ // filter mask
            'limit.start'=> 0,
            'limit.count'=> 100,
            'order.field'=> 'id',
            'order.way'  => 'DESC',
            'login'      => false,
            'module'     => null,
            'event_type' => null,
            'log_type'   => null,
            'event_time.sign'  => null,
            'event_time.value' => null,
        ];


        // покрываем маску фильтра значениями из входного массива

        if(is_array($aInputData))
            foreach($aInputData as $k0=>$val0)
                if(is_array($val0))
                    foreach($val0 as $k1=>$val1)
                        $f[$k0.'.'.$k1]=$val1;
                else
                    $f[$k0]=$val0;

        // собираем запрос на выборку

        $q = (new Query())
            ->select("`log`.*, `users`.login as login, `group_policy`.`id` as `gP`")
            ->from('log')
            ->leftJoin('users', '`users`.id=`log`.initiator')
            ->leftJoin('group_policy', '`group_policy`.id=`users`.group_policy_id')
            ->andFilterWhere([ 'event_type' => $f['event_type'] ])
            ->andFilterWhere([ 'log_type'   => $f['log_type'] ])
            ->andFilterWhere([
                'like',
                'module',
                $f['module']
            ])
            ->orderBy( $f['order.field'].' '.$f['order.way']);

        // если логин пустой выводим только системные сообщения
        if( $f['login']===0 ) $q->andWhere("!`initiator`");
        if( $f['login'] ) $q->andWhere([ 'login' => $f['login'] ]);

        // если не сисадмин - вырезаем сисадминские записи из выборки
        if( !\CurrentAdmin::isSystemMode() ) $q->andWhere("`group_policy`.alias != 'sysadmin'");

        // обрабатываем окно времени или границу по дате
        if($f['event_time.sign'] and $f['event_time.value']){

            if(($f['event_time.sign']=='BETWEEN') and is_array($f['event_time.value']))
                $timeQ = [
                    $f['event_time.sign'],
                       'event_time',
                    $f['event_time.value'][0],
                    $f['event_time.value'][1]
                ];
            else
                $timeQ = [
                    $f['event_time.sign'],
                       'event_time',
                    $f['event_time.value']
                ];

            $q->andWhere($timeQ);
        }


        // Выполняем запрос и собираем выходной массив

        $provider = new ActiveDataProvider([
            'query'      => $q,
            'pagination' => [
                'page'      => floor($f['limit.start'] / $f['limit.count']),
                'pageSize'  => $f['limit.count'],
            ],
        ]);

        return [
            'items' => $provider->getModels(),
            'count' => $provider->getTotalCount()
        ];

    }

}
