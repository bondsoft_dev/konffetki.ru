<?php

$skewerParams = require(__DIR__.'/config.php');

return [
    'id' => 'basic-console',
    'basePath' => ROOTPATH,
    'language' => 'ru',
    'timeZone'=>'Europe/Moscow',
    'bootstrap' => ['log'],
    'controllerNamespace' => 'app\skewer\console',
    'aliases'=>[
        '@skewer' => RELEASEPATH,
        '@tests' => ROOTPATH . '/tests',
        '@webroot' => WEBPATH,
        '@web' => WEBPATH,
    ],
    'components' => [
        'db' => defined('IS_UNIT_TEST') ? null : require(ROOTPATH . '/config/config.db.php'),
        'cache' => 'yii\caching\FileCache',
        'sections' => '\skewer\build\Component\I18N\DBSections',
        'register' => 'skewer\core\Component\Config\BuildRegistry',
        'log' => [
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                    'logFile' => '@app/log/error.log',
                    'logVars' => [],
                    'maxFileSize' => 1024 * 2,
                    'maxLogFiles' => 3,
                ],
            ],
        ],
        'i18n' => array(
            'class' => '\skewer\build\Component\I18N\I18N',
            'translations' => array(
                'app*' => [
                    'class' => '\skewer\build\Component\I18N\MessageSource',
                    'sourceLanguage' => 'ru',
                    'forceTranslation' => true,
                    'basePath' => '@app/cache/language',
                ],
                '*' => [
                    'class' => '\skewer\build\Component\I18N\MessageSource',
                    'sourceLanguage' => 'ru',
                    'forceTranslation' => true,
                    'basePath' => '@app/cache/language',
                ],
            ),
        ),
    ],
    'params' => $skewerParams,
];
