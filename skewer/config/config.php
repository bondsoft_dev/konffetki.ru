<?php

/*
 * Типовая конфигурация приложения
 */

$aConfig = [];

/*main*/

$aConfig['path']['root'] = ROOTPATH;
$aConfig['path']['core'] = COREPATH;
$aConfig['url']['root']  = WEBROOTPATH;

/* Пользователи */
$aConfig['users'][0] = array(
    'login' => '',
    'pass'  => '',
);

/*cache*/

/** @config string cache.rootPath Путь к корневой директории хранения файлового кеша  */
$aConfig['cache']['rootPath'] = WEBPATH.'cache/';

/** @config string cache.css Путь к корневой директории хранения скомпилированных файлов стилей  */
$aConfig['cache']['css'] = WEBPATH.'cache/css/';

/*Security*/

/** @config string security.vector Вектор шифрования для Blowfish. Используется в генерации и проверке подписей
 *  Gateway */
$aConfig['security']['vector'] = 'x03nMwK34x&ciSUH0I1got';

/*debug mode*/

/** @config boolean debug.wsdl Флаг отладки Web сервиса
 * (WSDL документ не кешируется и пересоздается каждый раз по запросу)
 * @deprecated Проверить использование
 */
$aConfig['debug']['wsdl'] = false;

/** @config boolean debug.parser Флаг отладки для парсера
 * (Шаблоны компилируются постоянно)
 */
$aConfig['debug']['parser'] = true;

/** @config boolean debug.config Флаг отладки реестра
 * (Файлы конфигураций с модулей собираются постоянно)
 */
$aConfig['debug']['config'] = false;

/** @config boolean debug.css Флаг очистки директории хранения кеша css-файлов
 * (Скомпилированные файлы стилей в debug режиме собираются постоянно)
 */
$aConfig['debug']['css'] = false;

/** @config boolean debug.asset Флаг очистки директории хранения ресурсов сайта
 */
$aConfig['debug']['asset'] = false;

/* Настройки логирования */

/** @config boolean log.enable Флаг включения логирования
 */
$aConfig['log']['enable']  = true;

/** @config boolean log.users Флаг включения логирования действий пользователей
 */
$aConfig['log']['users']  = true;

/** @config boolean log.cron Флаг включения логирования планировщика заданий
 */
$aConfig['log']['cron']   = true;

/** @config boolean log.system Флаг включения логирования системного журнала
 */
$aConfig['log']['system'] = true;

/** @config boolean log.debug Флаг включения логирования журнала отладки
 */
$aConfig['log']['debug']  = true;

/* allow browsers */
/* Наименьшая допустимая версия браузера, поддерживаемая системой */
$aConfig['browser']['Internet Explorer'] = 8.0;

$aConfig['browser']['Opera'] = 9.0;

$aConfig['browser']['Firefox'] = 3.5;

$aConfig['browser']['Mozilla'] = 5.0;

$aConfig['browser']['Chrome'] = 10;

$aConfig['browser']['Safari'] = 5;

/*auth*/
/** @config integer id группы публичного пользователя */
$aConfig['auth']['public_default_id'] = 2;

/** @config integer id группы авторизованного пользователя */
$aConfig['auth']['group_user_id'] = 3;

/*session*/

/** @config string Session.tickets.key Ключ сессии для хранения тикетов класса skSessionTicket */
$aConfig['session']['tickets']['key'] = '_tickets';

/** @config string Session.process.key Ключ сессии для хранения дерева процессов класса skProcessSession */
$aConfig['session']['process']['key'] = '_processSession';
$aConfig['session']['process']['designKey'] = '_designProcessSession';

$aConfig['notifications']['noreplay_email'] = 'no-reply@'.str_replace('/','',WEBROOTPATH);

/** Parser */

$aConfig['parser']['default']['paths'] = array( BUILDPATH.'common/templates/' );

/* Upload options */

/** @config array upload.form.maxsize Максимально допустимый размер для загрузки файлов в формы в байтах */
$aConfig['upload']['form']['maxsize'] = 64 * 1024 * 1024;

/** @config array upload.maxsize Максимально допустимый размер для загрузки изображений в байтах */
$aConfig['upload']['maxsize'] = 100 * 1024 * 1024;

/** @config array upload.images.maxWidth Максимально допустимый размер для загрузки изображений px по ширине */
$aConfig['upload']['images']['maxWidth'] = 4000;

/** @config array upload.images.maxHeight Максимально допустимый размер для загрузки изображений px по высоте */
$aConfig['upload']['images']['maxHeight'] = 4000;

/** @config array upload.allow.images Список разрешенных для загрузки расширений файлов изображений */
$aConfig['upload']['allow']['images'] = array('jpg', 'jpeg', 'gif', 'png');

/** @config array upload.allow.images Список разрешенных для загрузки расширений flash-файлов */
$aConfig['upload']['allow']['flash'] = array('swf', 'flv');

/** @config array upload.allow.images Список разрешенных для загрузки расширений media-файлов */
$aConfig['upload']['allow']['media'] = array(
    'aiff', 'asf', 'avi', 'bmp', 'fla', 'flv', 'gif', 'jpeg', 'jpg', 'mid',
    'mov', 'mp3', 'mp4', 'mpc', 'mpeg', 'mpg', 'png', 'qt', 'ram', 'rm',
    'rmi', 'rmvb', 'swf', 'tif', 'tiff', 'wav', 'wma', 'wmv'
);

/** @config array upload.allow.images Список разрешенных для загрузки расширений файлов */
$aConfig['upload']['allow']['files'] = array(
    '7z', 'aiff', 'asf', 'avi', 'bmp', 'csv', 'doc', 'docx', 'fla', 'flv', 'gif',
    'gz', 'gzip', 'ico', 'jpeg', 'jpg', 'mid', 'mov', 'mp3', 'mp4', 'mpc', 'mpeg',
    'mpg', 'ods', 'odt', 'pdf', 'png', 'ppt', 'pptx', 'pxd', 'qt', 'ram', 'rar',
    'rm', 'rmi', 'rmvb', 'rtf', 'sdc', 'sitd', 'swf', 'sxc', 'sxw', 'tar', 'tgz',
    'tif', 'tiff', 'txt', 'vsd', 'wav', 'wma', 'wmv', 'xls', 'xlsx', 'xml', 'zip',
    // для верстальщиков
    'js', 'css', 'ttf', 'svg', 'oet', 'woff'
);



/** @config array page.503 Страницы - заглушки */
$aConfig['page']['503'] = '503.twig';

/** @config array language настрйка языка */
$aConfig['language']['current'] = 'ru';
$aConfig['language']['path'] = ROOTPATH.'cache/language/';

return $aConfig;
