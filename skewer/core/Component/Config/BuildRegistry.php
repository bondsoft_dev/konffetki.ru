<?php

namespace skewer\core\Component\Config;

use skewer\build\Component\Installer;
use skewer\core\Component\Config;
use yii\base\Event;

/**
 * Конфигурация сборки
 */
class BuildRegistry extends Prototype {

    /** Вид отображения по умолчанию */
    const DEFAULT_TYPE = 'default';

    /**
     * Загружает набор данных
     * @throws Exception
     * @return void
     */
    protected function loadData() {
        $aData = \skRegistry::getStorage();
        if ( !$aData )
            throw new Exception( 'Build storage is empty' );
        $this->setData( $aData );
    }

    /**
     * Отдает путь до модуля
     * @param string $sModule имя модуля
     * @param string $sLayer имя слоя
     * @return mixed|null
     */
    private function getModulePath( $sModule, $sLayer ) {
        return array(
            Vars::LAYERS,
            $sLayer,
            Vars::MODULES,
            $sModule
        );
    }

    /**
     * Отдает конфиг модуля
     * @param string $sModule имя модуля
     * @param string $sLayer имя слоя
     * @throws Exception
     * @return ModuleConfig
     */
    public function getModuleConfig( $sModule, $sLayer ) {

        if ( !$this->moduleExists( $sModule, $sLayer ) )
            throw new Exception( "Module [$sModule] not found in layer [$sLayer]" );

        return new ModuleConfig( $this->get( $this->getModulePath( $sModule, $sLayer ) ) );

    }

    /**
     * Отдает параметр конфигурации модуля
     * @param string $sParamPath путь до параметра в конфиге
     * @param string $sModule имя модуля
     * @param string $sLayer имя слоя
     * @return mixed|null
     */
    public function getModuleConfigParam( $sParamPath, $sModule, $sLayer ) {
        $aPath = $this->getModulePath( $sModule, $sLayer );
        $aPath[] = $sParamPath;
        return $this->get( implode( $this->getDelimiter() ,$aPath ) );
    }


    /**
     * Отдает флаг наличия модуля
     * @param string $sModule имя модуля
     * @param string $sLayer
     * @param string $sLayer имя слоя
     * @return bool
     */
    public function moduleExists( $sModule, $sLayer ) {
        return $this->exists( $this->getModulePath( $sModule, $sLayer ) );
    }

    /**
     * Отдает набор событий
     * @param string $sEventName имя события
     * @return array
     */
    public function getEvents( $sEventName ) {

        $aEvents = $this->get( array(
            Vars::EVENTS,
            $sEventName
        ) );

        return $aEvents ? : [];

    }

    /**
     * Отдает dct зарегистрированные события
     * @return array
     */
    public function getAllEvents() {
        return $this->get( [Vars::EVENTS] ) ? : [];
    }

    /**
     * Отдает набор функциональных политик модуля
     * @param string $sModule
     * @param string $sLayer
     * @return array
     */
    public function getFuncPolicyItems( $sModule, $sLayer ) {

        $aList = $this->get( array(
            Vars::POLICY,
            $sLayer,
            $sModule,
            Vars::POLICY_ITEMS
        ) );

        return is_array($aList) ? $aList : array();

    }

    /**
     * Отдает набор имен слоев
     * @return string[]
     */
    public function getLayerList() {
        $aLayers = $this->get( Vars::LAYERS );
        return is_array($aLayers) ? array_keys( $aLayers ) : array();
    }

    /**
     * Отдает имен модулей слоя
     * @param string $sLayer имя слоя
     * @return string[]
     */
    public function getModuleList( $sLayer ) {
        $aModules = $this->get( array(
            Vars::LAYERS,
            $sLayer,
            Vars::MODULES,
        ) );
        return is_array($aModules) ? array_keys( $aModules ) : array();
    }

    /**
     * Обновляет данные конфигурации и словарей всех установленных в системе модулей.
     */
    public function actualizeAllModuleConfig() {
        $installer = new Installer\Api();
        $installer->updateAllModulesConfig();
    }

    /**
     * Инициализаця событий из реестра
     */
    public function initEvents() {

        foreach ($this->getAllEvents() as $eventName => $aEventList) {
            foreach ($aEventList as $aEvent) {

                // если есть указание прослушиваемого класса
                if (isset($aEvent[Config\Vars::EVENT_TO_CLASS])) {

                    // вешаем обработчик на событие конкретного класса
                    Event::on(
                        $aEvent[Config\Vars::EVENT_TO_CLASS],
                        $eventName,
                        [
                            $aEvent[Config\Vars::EVENT_CLASS],
                            $aEvent[Config\Vars::EVENT_METHOD]
                        ]
                    );

                }

                // иначе вешаем глобальное событие
                else {

                    \Yii::$app->on(
                        $eventName,
                        [
                            $aEvent[Config\Vars::EVENT_CLASS],
                            $aEvent[Config\Vars::EVENT_METHOD]
                        ]
                    );

                }

            }
        }

    }

}
