<?php

namespace skewer\core\Component\Config;

/**
 * Прототип конфигурационных файлов
 */
abstract class Prototype {

    /**
     * Набор конфигурационных данных
     * @var array
     */
    protected $aData;

    /**
     * Разделитель
     * @var string
     */
    protected $sPathDelim = '.';

    /**
     * Создает объект, проводит инициализацию
     */
    public function __construct() {
        
        $this->loadData();
        
    }

    /**
     * @todo lang
     * Отдает значение по имени или NULL, если не найдено
     * @param string|array $mName
     * @throws Exception
     * @return mixed|null
     */
    public function get( $mName ) {

        if ( is_null($this->aData) )
            throw new Exception( 'Config file not loaded' );

        if ( is_array( $mName ) )
            return $this->getByArray( $mName );

        if ( !is_string( $mName ) )
            throw new Exception( '`Name` must be a string or an array' );

        return $this->getByArray( explode( $this->getDelimiter(), $mName ) );

    }

    /**
     * Отдает флаг наличия значения
     * @param string|array $mName
     * @return bool
     */
    public function exists( $mName ) {

        return !is_null( $this->get( $mName ) );

    }

    /**
     * Отдает данные по массиву ключей
     * @param $aNameItems
     * @return array|null
     */
    private function getByArray( $aNameItems ) {

        $pConfig = &$this->aData;

        // спуститься по набору имен
        foreach ( $aNameItems as $sSubName ) {

            if ( !is_array( $pConfig ) )
                return null;

            if ( !isset($pConfig[$sSubName]) )
                return null;

            $pConfig = &$pConfig[$sSubName];

        }

        // вернуть то, до чего дошли
        return $pConfig;

    }

    /**
     * Загружаем данные во внутренний массив
     * @param array $aData
     */
    protected function setData( $aData ) {
        $this->aData = $aData;
    }

    /**
     * Отдает данные конфигурации
     * вне конфигов можно использовать для отладки
     * для работы с контентом нужно задать метод
     * @return array
     */
    public function getData() {
        return $this->aData;
    }

    /**
     * Загружает набор данных
     * @return void
     */
    abstract protected function loadData();

    /**
     * Отдает символ "разделитель" для путей
     * @return string
     */
    public function getDelimiter() {
        return $this->sPathDelim;
    }

    /**
     * Перезагружает данные из источников заново
     */
    public function reloadData() {

        // сброс значений
        $this->setData( array() );

        // повторная загрузка
        $this->loadData();

    }

}
