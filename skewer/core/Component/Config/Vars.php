<?php

namespace skewer\core\Component\Config;

/**
 * Класс с константами для путей в конфигах
 */
class Vars {

    /** корневая запись со слоями */
    const LAYERS = 'layers';

    /** набор модулей */
    const MODULES = 'modules';

    /** константа событий */
    const EVENTS = 'events';

    /** имя поля с классом для события - класс из которого будем вызывать */
    const EVENT_CLASS = 'class';

    /** имя поля с методом для события (из класса EVENT_CLASS) */
    const EVENT_METHOD = 'method';

    /** имя поля с названием события */
    const EVENT_NAME = 'event';

    /**
     * имя поля с классом на который вешается событие (не путать с EVENT_CLASS.)
     *  - это класс, событие которого прослушивается
     */
    const EVENT_TO_CLASS = 'eventClass';

    /** css файлы */

    /** имя css файла */
    const CSS_NAME = 'filePath';

    /** имя css поля с условием */
    const CSS_CONDITION = 'condition';

    /** имя css поля - флаг переноса в начало */
    const CSS_TO_START = 'toStart';

    /** имя css поля с условием */
    const CSS_VIEW_TYPE = 'layer';

    /** css имя стандартного типа отображения */
    const CSS_DEFAULT_VIEW = 'default';

    /** css имя стандартного условия*/
    const CSS_DEFAULT_CONDITION = 'default';

    /** js файлы */
    const JS = 'js';

    /** имя js файла */
    const JS_NAME = 'filePath';

    /** имя js поля с условием */
    const JS_CONDITION = 'condition';

    /** имя js поля с условием */
    const JS_VIEW_TYPE = 'layer';

    /** js имя стандартного типа отображения */
    const JS_DEFAULT_VIEW = 'default';

    /** js имя стандартного условия*/
    const JS_DEFAULT_CONDITION = 'default';

    /** функциональные политики */
    const POLICY = 'policy';

    /** функциональные политики - контейнер набора параметров */
    const POLICY_ITEMS = 'items';

    /** функциональные политики */
    const POLICY_TITLE = 'title';

    /** функциональные политики - имя параметра */
    const POLICY_VAL_NAME = 'name';

    /** функциональные политики - название параметра*/
    const POLICY_VAL_TITLE = 'title';

    /** функциональные политики - значение по умолчанию */
    const POLICY_VAL_DEFAULT = 'default';

    /** имя слоя */
    const LAYER_NAME = 'layer';

    /** имя модуля */
    const MODULE_NAME = 'module';

}