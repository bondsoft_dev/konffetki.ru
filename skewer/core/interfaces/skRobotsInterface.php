<?php
/**
 * Прототип для правил для robots.txt
 * @class skRobotsInterface
 *
 * @author Dmitry
 * @version $Revision$
 * @date $Date$
 * @project Skewer
 */

interface skRobotsInterface {
    /**
     * Возвращает паттерны для robots.txt для запрета страниц
     * @static
     * @return bool | array
     */
    public static function getRobotsDisallowPatterns();

    /**
     * Возвращает паттерны для robots.txt для разрешения страниц
     * @static
     * @return bool | array
     */
    public static function getRobotsAllowPatterns();
}// interface
