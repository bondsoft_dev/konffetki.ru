<?php
/**
 *
 * @class skPatchInterface
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project JetBrains PhpStorm
 * @package kernel
 */
interface skPatchInterface {

    public function execute();

}
