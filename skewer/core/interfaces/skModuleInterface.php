<?php

/**
 * Загрузка ресурсов по имени класса
 * 
 * @class skModuleInterface
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project Skewer
 * @package kernel
 *
 */

interface skModuleInterface {
    /**
     * Метод - исполнитель функционала
     * @abstract
     */
    public function execute();



}
