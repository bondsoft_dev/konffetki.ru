<?php
/**
 *
 * @class CodeTplException
 * @extends Exception
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project JetBrains PhpStorm
 * @package kernel
 */
class CodeTplException extends Exception {
}// class
