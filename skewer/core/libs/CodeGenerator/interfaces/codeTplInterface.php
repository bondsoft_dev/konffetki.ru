<?php
/**
 *
 * @class codeTplInterface
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project Skewer
 * @package Libs
 */

interface codeTplInterface {

    public function make();

    public function remove();

}// iface
