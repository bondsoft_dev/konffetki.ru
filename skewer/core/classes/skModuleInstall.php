<?php

use \skewer\core\Component\Config\ModuleConfig;

/**
 * Класс-прототип файлов установки, обновления и удаления модулей.
 * @class skModuleInstall
 * @extends skUpdateHelper
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project Skewer
 * @package Kernel
 */
abstract class skModuleInstall extends skUpdateHelper {

    /** @var ModuleConfig Конфиг модуля */
    protected $config = null;

    /** @var array Языки */
    protected $words = [];

    public function __construct( ModuleConfig $config ) {
        parent::__construct();
        $this->config = $config;
    }

    /**
     * Возвращает параметр конфига модуля
     * @param $sParamName
     * @return string
     */
    public function getConfigParam( $sParamName ){
        return $this->config->getVal($sParamName);
    }

    /**
     * Имя устанавлеваимого модуля
     * @return string
     */
    public function getModuleName(){
        return $this->config->getName();
    }

    /**
     * Имя слоя, куда устанавливается модуль
     * @return string
     */
    public function getLayer(){
        return $this->config->getLayer();
    }

    /**
     * Прототип функции инициализации перед выполнением операций над модулем.
     * @return bool
     */
    abstract public function init();

    /**
     * Прототип функции для инструкций установки модуля
     * @return bool
     */
    abstract public function install();

    /**
     * Прототип функции для инструкций удаления модуля
     * @return bool
     */
    abstract public function uninstall();

    /**
     * @todo Временная, не должна использоваться!
     * @deprecated
     * Отдает значение языковой метки для данного модуля
     * Если передано более одного параметра - работает пол принципу sprintf
     * @param string $sKey
     * @param mixed [$mVal] значения по принциапу sprintf
     * @return string
     */
    public function lang( $sKey ) {

        return $sKey;

    }


    /**
     * Провести апгрейд модуля с версии до версии
     * @param $iPreviosVersion
     * @param $iNextVersion
     */
//    abstract public function upgrade($iPreviosVersion, $iNextVersion);

}// class
