<?php
use yii\web\ServerErrorHttpException;

/**
 * Класс запуска обновлений на установку
 * @class skPatchInstaller
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project Skewer
 * @package Kernel
 *
 * Что делать с патчами?
 * Патчи могут запускаться 3-мя разными способами:
 * 1. Локальные патчи - лежат в директории сборки сайта и видны в админке в списке доступных либо недоступных к установке.
 * Для их запуска нужно указать путь к ним.
 * 2. Remote патчи - лежат в сборках кластера - не видны из админки запускаются в процессе установки обновлений.
 * Для запуска нужно указать путь.
 * 3.
 * Получается, что патч всегда выполняется только в рамках конкретной площадки с конкретной версией сборки. Т.е. проблем
 * с именами реестра нет.
 * В случае если на площадке с версией blue0010 запускается патч из версии сборки blue0011, то у него проблем с реестром нет
 * (работает с текущим, потом переименование вручную), но есть проблемы с путями до файлов шаблонов генератора кода, Пути нужно
 * указывать руками относительно корня кластера с учетом сборки.
 *
 */
class skPatchInstaller {

    /**
     * Экземпляр файла установки патча
     * @var null|skUpdateHelper
     */
    protected $oPatch = null;

    /**
     * Функция обратного вызова, для проверки записи о том, что текущий патч не был установлен ранее
     * @var null|callable|callback
     */
    protected $mCheckCallback = null;
    /**
     * @param $sPatchFile
     * @throws UpdateException
     */

    /**
     * Имя файла патча
     * @var string
     */
    protected $sPatchFile = '';

    public function __construct($sPatchFile) {

        if(!is_file($sPatchFile)) throw new UpdateException("Patch install error: Patch file not found [$sPatchFile]!");
            $this->sPatchFile = basename($sPatchFile);

        require_once($sPatchFile);
        /* Создать экземпляр*/

        $this->oPatch = new PatchInstall();

        /* Проверка на правильность формата */
        if(!($this->oPatch instanceof PatchInstallPrototype))
            throw new UpdateException('Patch install error: Patch has invalid format');

        return true;
    }// func

    /**
     * Запускает установку обновления
     * @throws ServerErrorHttpException
     * @return bool|array true/false/массив для отправки
     */
    public function install() {
        /* Заблокировать сессию установки */
        /* Проверить был ли установлен данный патч до текущего момента */
        /* Установить патч */

        /* Спрашиваем у площадки, устанавливался ли этот патч ранее */
        if( is_callable($this->mCheckCallback) AND call_user_func_array($this->mCheckCallback, array($this->sPatchFile))) {

            $fileName = $this->sPatchFile;
            $fileName = explode(".",$fileName);
            $fileName = $fileName[0];

            skLogger::dump('Patch '.$fileName.' install started  At '.date('r'));
            $mResult = $this->oPatch->execute();

            if ( is_null($mResult) )
                $mResult = true;

            skLogger::dump('Patch '.$fileName.' install complete At '.date('r'));

            if ( $this->oPatch->bUpdateCache ) {
                \Yii::$app->register->actualizeAllModuleConfig();
                \Yii::$app->clearAssets();
            }

        } else {

            throw new ServerErrorHttpException( "Patch ".(int)$this->sPatchFile." already installed." );

        }

        return $mResult;

    }// func

    /**
     * Возвращает описание патча, если таковое присутствует
     * @return string
     */
    public function getDescription() {
        return $this->oPatch->sDescription;
    }// func


    /**
     * Устанавливает функцию обратного вызова для проверки устанавливаемого патча на факт более ранней его установки.
     * @param callable|callback $mCallback
     * @return bool
     */
    public function setChecker($mCallback) {

        if(!is_callable($mCallback)) return false;
        $this->mCheckCallback = $mCallback;
        return true;

    }// func

}// class
