<?php
/**
 * 
 * @class skMailer
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project Skewer
 * @package kernel
 */ 
class skMailer {

    public static function checkSenderMail($sMail) {

        if( !$sMail ) return false;

        if( strpos($sMail, ',')!== false ) {
            $sMail = explode(',',$sMail);
            return trim($sMail[0]);
        }
        else return trim($sMail);

    }// func

    /**
     * Производит поиск изображений в теле сообщения и добавляет их в аттач
     * @static
     * @param $aMail
     * @param string $sEncoding
     * @return array
     */
    private static function attachImgInMail($aMail, $sEncoding='utf-8'){

        $aMail['bound'] = $bound = "_1_AA123AA123BB"; // разделитель

        // достаем картинки
        $pattern = '/<img[^>]*src=["\']?([^\s>"\']+)["\']?/i';
        preg_match_all($pattern, $aMail['Body'], $imgs);
        $attach = "";
        $i = 0;
        foreach($imgs[1] as $val){
            $i++;
            $file_name	= substr($val,strrpos($val,'/')+1);
            $file_ex	= substr($val,strrpos($val,'.')+1);
            if($file_ex == 'jpg') $file_ex = 'jpeg';
            $attach		.= "\n--$bound\n";
            $attach		.= "Content-Type: image/$file_ex; name=\"$file_name\"\n";
            $attach		.= "Content-Transfer-Encoding: base64\n";
            $attach		.= "Content-Disposition: inline\n"; // \n  attachment
            $attach		.= "Content-ID: <spravkaweb_img_$i>\n\n";
            $cur_file	= file_get_contents(WEBPATH.$val);

            if($cur_file){
                $attach		.= wordwrap(base64_encode($cur_file), 75, "\n", true);
                $aMail['Body']	= str_replace($val, "cid:spravkaweb_img_$i"	, $aMail['Body']);	//	"cid:spravkaweb_img_$i"
            }
        }

        // тело
        $body		= "--$bound\n";
        $body		.= "content-type: text/html; charset=\"$sEncoding\"\n";
        $body		.= "content-transfer-encoding: base64\n\n";
        $body		.= wordwrap(base64_encode($aMail['Body']), 75, "\n", true);

        // аттач
        $body		.= $attach."\n--$bound--\n\n";

        $aMail['Body'] = $body;

        return $aMail;
    }

    /**
     * Формирует массив параметров сообщения для рассылки
     * @static
     * @param $sSubject
     * @param $sBody
     * @param $sMailFromAddr
     * @param string $sMailFromName
     * @param string $sEncoding
     * @return array|bool
     */
    public static function getMail($sSubject, $sBody, $sMailFromAddr, $sMailFromName = '', $sEncoding='utf-8'){

        $aMail = array('MailFrom'=>'','hdMailFrom'=>'','Subject'=>'','Body'=>'');

        $sHdMailFrom = $sMailFromAddr;
        $sSubject = trim($sSubject);
        $sMailFrom = self::checkSenderMail($sMailFromAddr);
        if(!$sMailFrom) return false;

        switch( $sEncoding ){

            case "utf-8":
                $sSubject = '=?utf-8?B?'.base64_encode($sSubject).'?=';
                if($sMailFromName) $sHdMailFrom = '=?utf-8?B?'.base64_encode($sMailFromName).'?='."<$sMailFrom>";
                break;

            case "koi8-r":

                $sBody = iconv('UTF-8', 'WINDOWS-1251', $sBody);
                $sBody = convert_cyr_string (stripslashes($sBody),'w','k');

                $sSubject = iconv('UTF-8', 'WINDOWS-1251', $sSubject);
                $sSubject = '=?koi8-r?B?'.base64_encode(convert_cyr_string(stripslashes($sSubject), "w","k")).'?=';

                if($sMailFromName){
                    $sMailFromName = iconv('UTF-8', 'WINDOWS-1251', $sMailFromName);
                    $sMailFromName = convert_cyr_string (stripslashes($sMailFromName),'w','k');
                    $sHdMailFrom = '=?koi8-r?B?'.base64_encode($sMailFromName).'?='."<$sMailFrom>";
                } // if
                break;
        }


        $aMail['Subject'] = $sSubject;
        $aMail['Body'] = $sBody;
        $aMail['MailFrom'] = $sMailFrom;
        $aMail['hdMailFrom'] = $sHdMailFrom;

        $aMail = self::attachImgInMail($aMail,$sEncoding); // добавление в аттач изображений в теле

        return $aMail;
    }

    /**
     * Отправляет письма на набор адресов $sMailTo используя уже сформированный массив данных письма $aMail
     * @static
     * @param $aMail
     * @param $sMailTo
     * @return bool
     */
    public static function sendReadyMail($aMail, $sMailTo){

        $aMail['hdMailFrom'] = self::convertEmail( $aMail['hdMailFrom'] );
        $aMail['MailFrom'] = self::convertEmail( $aMail['MailFrom'] );
        $sMailTo = self::convertEmail( $sMailTo );

        $sHeaders = 'From: '.$aMail['hdMailFrom']."\r\n".
            'Reply-To: '.$aMail['MailFrom']. "\r\n".
            //'Content-Type: text/html; charset="'.$sEncoding.'"'."\r\n".
            'Content-Type: multipart/related; boundary="'.$aMail['bound'].'"'."\r\n".
            'Content-Transfer-Encoding: 8bit'."\r\n".
            'MIME-Version: 1.0'."\r\n".
            'X-Mailer: PHP/'.phpversion();

        if ( defined('SIMPLE_MAIL') && SIMPLE_MAIL )
            return mail($sMailTo, $aMail['Subject'], $aMail['Body'], $sHeaders);
        else
            return mail($sMailTo, $aMail['Subject'], $aMail['Body'], $sHeaders, '-f '.$aMail['hdMailFrom']);

    }


    public static function sendMail($sMailTo, $sMailFrom, $sSubject, $sBody, $sEncoding='utf-8') {

        $sMailTo = self::convertEmail( $sMailTo );
        $sMailFrom = self::convertEmail( $sMailFrom );

        $sSubject = trim($sSubject);
        $sMailReplyTo = self::checkSenderMail($sMailFrom);

        // нет обратного адреса или не валидный
        if ( !skValidator::isEmail($sMailFrom) )
            return false;

        if(!$sMailReplyTo) $sMailReplyTo = $sMailFrom;

        switch( $sEncoding ){

            case "utf-8":
                $sSubject = '=?utf-8?B?'.base64_encode($sSubject).'?=';

                break;

            case "koi8-r":

                $sBody = iconv('UTF-8', 'WINDOWS-1251', $sBody);
                $sBody = convert_cyr_string (stripslashes($sBody),'w','k');

                $sSubject = iconv('UTF-8', 'WINDOWS-1251', $sSubject);
                $sSubject = '=?koi8-r?B?'.base64_encode(convert_cyr_string(stripslashes($sSubject), "w","k")).'?=';

                break;
        }

        $sHeaders = 'From: '.$sMailFrom."\r\n".
            'Reply-To: '.$sMailReplyTo. "\r\n".
            'Content-Type: text/html; charset="'.$sEncoding.'"'."\r\n".
            'Content-Transfer-Encoding: 8bit'."\r\n".
            'MIME-Version: 1.0'."\r\n".
            'X-Mailer: PHP/'.phpversion();
        if ( defined('SIMPLE_MAIL') && SIMPLE_MAIL )
            return mail($sMailTo, $sSubject, $sBody, $sHeaders);
        else
            return mail($sMailTo, $sSubject, $sBody, $sHeaders, '-f '.$sMailFrom);

    }


    /**
     * Отправка сообщения с файлами аттача
     * @param $sMailTo
     * @param $sMailFrom
     * @param $sSubject
     * @param $sBody
     * @param $aAttach
     * @param string $sEncoding
     * @return bool
     */
    public static function sendMailWithAttach($sMailTo, $sMailFrom, $sSubject, $sBody, $aAttach, $sEncoding='utf-8') {

        $sMailTo = self::convertEmail( $sMailTo );
        $sMailFrom = self::convertEmail( $sMailFrom );

        $sSubject = trim($sSubject);
        $sMailReplyTo = self::checkSenderMail($sMailFrom);

        if(!$sMailReplyTo) $sMailReplyTo = $sMailTo;

        switch( $sEncoding ){

            case "utf-8":
                $sSubject = '=?utf-8?B?'.base64_encode($sSubject).'?=';

                break;

            case "koi8-r":

                $sBody = iconv('UTF-8', 'WINDOWS-1251', $sBody);
                $sBody = convert_cyr_string (stripslashes($sBody),'w','k');

                $sSubject = iconv('UTF-8', 'WINDOWS-1251', $sSubject);
                $sSubject = '=?koi8-r?B?'.base64_encode(convert_cyr_string(stripslashes($sSubject), "w","k")).'?=';

                break;

        }


        // -- attach add
        $bound = "_1_AA123AA123BB";
        $sAttach = "";

        foreach($aAttach as $sAttachName=>$fAttach){
            $sAttach		.= "\n--$bound\n";
            $sAttach		.= "Content-Type: file; name=\"$sAttachName\"\n";
            $sAttach		.= "Content-Transfer-Encoding: base64\n";
            $sAttach		.= "Content-Disposition: inline\n"; // \n  attachment
            $sAttach		.= "Content-ID: <attach_item_$sAttachName>\n\n";
            $sAttach		.= wordwrap(base64_encode($fAttach), 75, "\n", true);
        }

        // тело
        $body		= "--$bound\n";
        $body		.= "content-type: text/html; charset=\"$sEncoding\"\n";
        $body		.= "content-transfer-encoding: base64\n\n";
        $body		.= wordwrap(base64_encode($sBody), 75, "\n", true);

        $body		.= $sAttach."\n--$bound--\n\n";

        $sHeaders = 'From: '.$sMailFrom."\r\n".
            'Reply-To: '.$sMailReplyTo. "\r\n".
            //'Content-Type: text/html; charset="'.$sEncoding.'"'."\r\n".
            'Content-Type: multipart/related; boundary="'.$bound.'"'."\r\n".
            'Content-Transfer-Encoding: 8bit'."\r\n".
            'MIME-Version: 1.0'."\r\n".
            'X-Mailer: PHP/'.phpversion();

        if ( defined('SIMPLE_MAIL') && SIMPLE_MAIL )
            return mail($sMailTo, $sSubject, $body, $sHeaders);
        else
            return mail($sMailTo, $sSubject, $body, $sHeaders, '-f '.$sMailFrom);

    }

    /**
     * Перевод кирилический email в понятный мэйлеру вид
     * Может обрабатывать списки email, переданные через запятую
     * @param $sMail
     * @return string
     */
    private static function convertEmail( $sMail ){

        $converter = new idna_convert(['idn_version' => 2008]);

        $aOut = [];

        foreach ( explode(',', $sMail) as $s )
            $aOut[] = $converter->encode(trim($s));

        return join(',', $aOut);

    }

}
