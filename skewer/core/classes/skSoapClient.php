<?php
/**
 * Класс, реализующий Soap клиент с поддержкой шифрования трафика
 * @class skSoapClient
 * @extends SoapClient
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project Skewer
 * @package Kernel
 *
 * @uses skBlowfish
 */

class skSoapClient extends SoapClient {

    /**
     * Класс, реализующий шифрование
     * @var null|skBlowfish
     */
    private $oCryptProxy = null;

    /**
     * Флаг, указывающий на необходимость шифрования/дешифровки трафика
     * @var bool
     */
    private $bCryptMode = false;

    /**
     * Ключ шифрования (рекомендуемая длина ключа не менее 32 бит)
     * @var string
     */
    private $sCryptKey = '';

    /**
     * Массив нешифруемых заголовоков прикрепляемых к каждому запросу в рамках сессии.
     * @var array
     */
    private $aStaticHeaders = array();

    /**
     * Теги статических заголовков
     * @var array
     */
    protected $aStaticHeaderTags = array('<staticHeader>','</staticHeader>');

    /**
     * Создает экземпляр Soap клиента на основе WSDL документа $wsdl и опций $options.
     * Если в массиве настроек присутствуют ключи crypt.key (ключ для шифрования трафика) и
     * crypt.vector(вектор инициализации для CBC режима шифрования), то сообщения будут отправлятся
     * после предварительного шифрования объектом oCryptProxy с помощью ключа crypt.key.
     * Все входящие пакеты так же будут подвергаться процессу дешифровки. Для того, чтобы
     * соединение было установлено в Security режиме нужно идентичным образом и с одинаковыми ключами и
     * векторами сконфигурировать серверную и клиентскую части.
     * @param string $wsdl Ссылка на валидный WSDL документ WEB сервиса.
     * @param array $options Массив настроек клиента.
     * @param array $aStaticHeaders Массив нешифруемой заголовочной информации, прикрепляемой к каждому запросу
     */
    public function __construct($wsdl, $options, $aStaticHeaders = array()) {

        /* Если требуется шифрование трафика - создаем и настраиваем прокси */
        if(isSet($options['crypt']) AND $options['crypt'])
            if(isSet($options['crypt']['key']) AND $options['crypt']['vector']){

                $this->oCryptProxy  = new skBlowfish();
                $this->sCryptKey    = $options['crypt']['key'];
                $this->oCryptProxy->setIv($options['crypt']['vector']);
                $this->bCryptMode   = true;
            }

        if(count($aStaticHeaders))
            $this->aStaticHeaders = $aStaticHeaders;

        parent::SoapClient($wsdl, $options = array());
    }// constructor

    /**
     * Отправляет запрос $request
     * @param $request
     * @param $location
     * @param $action
     * @param $version
     * @return bool|string
     */
    function __doRequest($request, $location, $action, $version) {

        /* Шифруем если надо */
        if($this->bCryptMode)
            $request = $this->oCryptProxy->encrypt($request, $this->sCryptKey);

        $request = $this->makeStaticHeaders().$request;

        $response = parent::__doRequest($request, $location, $action, $version);

        /* Дешифруем если надо */
        if($this->bCryptMode)
            $response = $this->oCryptProxy->decrypt($response, $this->sCryptKey);

        return $response;
    }// func

    /**
     * Собирает строку заголовков из массива данных
     * @return string|bool
     */
    protected function makeStaticHeaders() {

        if(!count($this->aStaticHeaders)) return false;
        $sHeadersPattern = $this->aStaticHeaderTags[0].'%s'.$this->aStaticHeaderTags[1];
        return sprintf($sHeadersPattern, http_build_query($this->aStaticHeaders));

    }//  func

}// class
