<?php
/**
 * Класс реализующий заголовок-подпись при отправке пакетов через SoapGateway
 * @class skSoapHeaderSecurityToken
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project Skewer
 * @package Kernel
 */
class skSoapHeaderSecurityToken {

    /**
     * Сертификат, отправляемый в качестве подписи к пакетам запросов
     * @var string
     */
    public $sCertificate = '';

    /**
     * @param string $sAppKey Ключ приложения (площадки)
     * @param string $sVector Вектор шифрования
     */
    public function __construct($sAppKey, $sVector) {
        $this->sCertificate = static::makeCertificate($sAppKey, $sVector);
        if(!$this->sCertificate) return false;

        return true;
    }// func

    /**
     * Генерирует подпись на основе ключа приложения и вектора шифрования
     * @static
     * @param string $sAppKey Ключ приложения (площадки)
     * @param string $sVector Вектор шифорвания
     * @return string Возвращает сгенерированную подпись
     */
    public static function makeCertificate($sAppKey, $sVector) {
        return md5($sAppKey.$sVector);
    }// func

    /**
     * Проверят подпись $sPossibleCertificate на валидность для данной площадки
     * @static
     * @param string $sPossibleCertificate Проверяемая подпись (возможная подпись)
     * @param string $sAppKey Ключ текущего приложения
     * @param string $sVector Вектор шифрования в кластере
     * @return bool Возвращает true если $sPossibleCertificate является валидной подписью для данного приложения
     * либо false в случае, если подпись не верна
     */
    public static function checkCertificate($sPossibleCertificate, $sAppKey, $sVector) {

        return ($sPossibleCertificate == static::makeCertificate($sAppKey, $sVector))? true: false;

    } // constructor
}