<?php
/**
 * Wrapper для CodeGenerator
 * @class skCodeGenerator
 * @extends CodeGenerator
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project JetBrains PhpStorm
 * @package kernel
 */
require_once(COREPATH.'libs/CodeGenerator/classes/CodeGenerator.php');
require_once(COREPATH.'libs/CodeGenerator/interfaces/codeTplInterface.php');
require_once(COREPATH.'libs/CodeGenerator/exceptions/CodeTplException.php');
require_once(COREPATH.'libs/CodeGenerator/classes/codeTplPrototype.php');

require_once(COREPATH.'libs/CodeGenerator/templates/ConstantsTpl.php');
require_once(COREPATH.'libs/CodeGenerator/templates/HtaccessTpl.php');

class skCodeGenerator extends CodeGenerator {

}// class
