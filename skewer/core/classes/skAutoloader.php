<?php
/**
 * Загрузка ресурсов по имени класса
 *
 * @class skAutoloader
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 *
 * @project Skewer
 * @package kernel
 *
 * @description
 * Паттерн: [name[2|4 additional]?][layer]?[type]?
 *
 * @example
 * Примеры загружаемых классов:
 * <br> $t = new skProcess();
 * <br> $t = new ProcessModule();
 * <br> $t = new ProcessApi();
 * <br> $t = new Process2GalleryModule();
 * <br> $t = new Process4GalleryApi();
 * <br> $t = new ProcessAdmModule(); // todo ???
 * <br> $t = new ProcessAdmApi(); // todo ???
 */

class skAutoloader {
    /**
     * Абсолютный путь до корня ядра
     * @var string
     */
    static protected $sCorePath = '';
    /**
     * Абсолютный путь до корня сборки
     * @var string
     */
    static protected $sBuildPath = '';

    /**
     * Типы допустимых функциональных расширений
     * @var array
     */
    static protected $sTypes = array(
        'Module',
        'Api',
        'Exception',
        'Interface',
        'Routing',
        'Install',
        'Mapper',
        'Model',
        'Ajax',
        'Service',
        'Prototype',
    );

    /**
     * Типы допустимых функциональных слоев
     * @var array
     */
    static protected $sLayers = array(
        'Page',
        'Adm',
        'Cms',
        'Tool',
        'Design',
        'Catalog',
    );

    /**
     * Закрываем возможность вызова напряумю
     */
    private function __construct() {

    }// constructor

    /**
     * Регистрация загрузчика согласно пути
     * @static
     * @param string $sCorePath путь директории ядра
     * @param string $sBuildPath путь директории сборки
     * @return bool
     */
    public static function registerPath($sCorePath,$sBuildPath) {

        self::$sCorePath = $sCorePath;
        self::$sBuildPath = $sBuildPath;

        spl_autoload_register(array(new self, 'autoload'));
        return true;
    }// func

/**
     * Автоподгрузка из пространства имен
     * @param $sClassName
     * @throws Exception
     * @return bool
     */
    static protected function autoloadFromNamespace( $sClassName ) {

        $sFullFilePath = RELEASEPATH.str_ireplace( '\\', DIRECTORY_SEPARATOR, substr($sClassName,7) ).'.php';
        return self::requireFile($sFullFilePath);

    }

    /**
     * Разбор имени файла, подгрузка файла класса.
     * @static
     * @param string $sClassName Имя класса
     * @return bool
     */
    static public function autoload($sClassName) {

        if ( strpos( $sClassName, '\\' ) !== false ) {
            return self::autoloadFromNamespace( $sClassName );
        }

        /**
         * [moduleName] - название модуля
         * [mode] - слой
         * [type] - тип файла (Api/skModule/other)
         */
        preg_match_all('/^(sk)?+(?<moduleName>[-_a-z0-9]+)((2|4)[-_a-z0-9]+)?(?<mode>'.implode('|',self::$sLayers).')?(?<type>'.implode('|',self::$sTypes).'){1}?$/Ui', $sClassName, $aEntry, PREG_SET_ORDER);

        // флаг класса ядра
        $isCore = ((strpos($sClassName,'sk')===0) && ($sClassName!=='sk'));

        if($aEntry) {

            $aEntry = $aEntry[0];

            // определение параменных
            $sModuleName = $aEntry['moduleName'];
            $sMode = $aEntry['mode'];
            $sType = $aEntry['type'];

            // префикс слоя
            //  имена классов - CamelCase а директории в нижнем регистре
            $sModePath = !empty($sMode) ? $sMode.'/' : '';

            /*-----------*/
            switch ($sType) {

                case 'Exception':
                    if ( $isCore )
                        $sFilePath = 'classes/'.$sClassName.'.php';
                    else
                        $sFilePath = 'exceptions/'.$sClassName.'.php';
                    break;

                case 'Interface':
                    $sFilePath = 'interfaces/'.$sClassName.'.php';
                    break;

                case 'Prototype':
                    if ( $isCore )
                        $sFilePath = 'classes/'.$sClassName.'.php';
                    else
                        $sFilePath = 'prototypes/'.$sClassName.'.php';
                    break;

                case 'Mapper':

                    if ( $isCore ) {

                        $sFilePath = 'mappers/'.$sClassName.'.php';
                    } else {

                        // загружаем по основному пути
                        $sFilePath = $sModePath.$sModuleName.DIRECTORY_SEPARATOR.$sClassName.'.php';
                        $sFullFilePath = self::$sBuildPath.$sFilePath;
                        if(self::requireFile($sFullFilePath))
                            return true;

                        // не получилось - смотрим в сборку
                        $sFilePath = 'mappers/'.$sClassName.'.php';
                        $sFullFilePath = self::$sBuildPath.$sFilePath;
                        if(self::requireFile($sFullFilePath))
                            return true;

                        return false;

                    }
                    break;

                default:

                    $sFilePath = $sModePath.$sModuleName.DIRECTORY_SEPARATOR.$sClassName.'.php';

                    if ( $isCore ) $sFilePath = 'classes/'.$sClassName.'.php';
                    break;

            }// of type

        } else {

            $sFilePath = 'classes/'.$sClassName.'.php';
        }

        $sFullFilePath = ($isCore)? self::$sCorePath.$sFilePath: self::$sBuildPath.$sFilePath;

        if(self::requireFile($sFullFilePath)) return true;

        return false;

    }// func

    /**
     * Подлючает файл, если он есть. Возвращает результат подключения
     * @param string $sClassName класс либо интерфейс, содержащийся в файле
     * @param string $sFileName Путь к файлу
     * @return bool Возвращает true, если файл был найден и подключен либо false в случае ошибки
     */
    protected static function requireFile($sFileName) {

        if(!file_exists($sFileName))
            return false;
        /** @noinspection PhpIncludeInspection */
        require_once($sFileName);
        return true;
    }

}
