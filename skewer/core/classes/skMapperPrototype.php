<?php

use skewer\build\Component\orm\Query;


/**
 * Class skMapperPrototype
 * @author ArmiT, $Author: $
 * @deprecated Перевести все Mapper's на ActiveRecord
 */
abstract class skMapperPrototype extends skModelPrototype {

    /** @var string Имя таблицы */
    protected static $sCurrentTable;

    /** @var string Имя ключевого поля */
    protected static $sKeyFieldName = 'id';

    /**
     * @static Метод возвращает имя рабочей таблицы
     * @return string
     */
    public static function getCurrentTable() {
        return static::$sCurrentTable;
    }

    public static function getKeyFieldName() {
        return static::$sKeyFieldName;
    }


    /**
     * Метод сохранения записи
     * @static
     * @param array $aInputData
     * @return bool|int
     */
    public static function saveItem( $aInputData = array() ) {

        if ( !$aInputData ) return false;

        // имя ключевого поля - обязательное
        $sKeyFieldName = static::getKeyFieldName();

        // флаг наличия поля идентификатора
        $bFoundId = false;
        // значение поля идентификатора
        $mIdKeyVal = 'NULL';

        $aData      = array();
        $aFields    = array();
        /*
         * Проходимся по массиву переданных извне параметров и, при совпадении варианта switch с параметром входного массива,
         * добавляем соответствующий элемент к массиву плейсхолдеров и к строке запроса
         */
        foreach( $aInputData as $sKey=>$sValue ){

            // запросить описание поля
            $aFieldParams = static::getParameter($sKey);

            // если поле найдено
            if ( $aFieldParams ){

                // если поле - первичный ключ
                if ( $sKeyFieldName === $sKey ) {

                    if ( $sValue ){
                        $mIdKeyVal = $sValue;
                        $bFoundId = true;
                    }
                    else {

                        $mIdKeyVal = 'NULL';
                    }

                } else {

                    // тип данных
                    //$sType = $aFieldParams['type'];

                    if ( is_null($sValue) ) {
                        //$sType = 'q';
                        $sValue = null;//'NULL';
                    }

                    // добавить в список элементов на сохранение
                    $aFields[] = "`$sKey`=:$sKey";

                    // добавить в массив для меток в запросе
                    $aData[$sKey] = $sValue;

                }

            }

        }//foreach $aNewsData

        if ( sizeof($aFields) ){

            // Если массив с элементами запроса не пуст - собираем из него строку
            $sFields = implode(',', $aFields);

            // имя паблицы
            $sTableName = static::getCurrentTable();

            // имя и значение ключевого поля
            //$aData['sKeyFieldName'] = $sKeyFieldName;
            $aData['mIdKeyVal'] = $mIdKeyVal;

            // если найден id
            if ( $bFoundId ) {

                $sQuery = "UPDATE `$sTableName` SET $sFields WHERE `$sKeyFieldName` = :mIdKeyVal";

            } else {

                $sQuery = "INSERT INTO `$sTableName` SET $sFields ON DUPLICATE KEY UPDATE $sFields;";

            }

            $oResult = Yii::$app->db->createCommand($sQuery,$aData)->execute();

            // если сохранение
            if ( $bFoundId ) {

                if ( $oResult != -1 )
                    return $aInputData[$sKeyFieldName];
                else
                    return false;

            } else {
                // вставка - вернуть новый id
                return Yii::$app->db->getLastInsertID();
            }

        }

        return false;
    }

    /**
     * @static Метод получения списка записей по фильтру
     *
     * Фильтр имеет вид:
     *      array = (
     *          'select_fields'     => array( <имя поля 1>, ... ),
     *          'where_condition'   => array( <имя поля> => array (
     *                                      'sign' => <операция сравнения>,
     *                                      'value' => <значение>
     *                                       )
     *                                 ),
     *          'limit'             => array(
     *              'start' => <начальный элемент>,
     *              'count' => <количество в выборке>
     *          )
     *          'order' => array(
     *              'field' => <имя поля>,
     *              'way' => <направление сортировки>
     *          )
     *
     * @param $aFilter array
     * @return array
     */
    public static function getItems( $aFilter=array() ){

        // Получаем имя класса, вызывающего метод
        $sCurrentClass = get_called_class();
        $aData = array();

        $sSelectFields = '*';
        // Обрабатываем элементы фильтра, перечисляющие выбираемые поля
        if ( isset($aFilter['select_fields']) && sizeof($aFilter['select_fields']) ){

            $aSelectFields = array();

            foreach( $aFilter['select_fields'] as $sValue ){

                // Получаем тип поля
                /** @noinspection PhpUndefinedMethodInspection */
                $aValueParams = $sCurrentClass::getParameter($sValue);

                if ( is_array($aValueParams) && sizeof($aValueParams) ){

                    // Собираем поля в массив
                    $aSelectFields[] = $sValue;
                }

            }
            // Собираем условие по выбираемым полям
            if ( sizeof($aSelectFields) ) $sSelectFields = '`'.implode('`, `', $aSelectFields).'`';
            else return false;
        }


        // Обрабатываем элементы фильтра, формирующие условие
        $sWhereCondition = '';
        if ( isset($aFilter['where_condition']) && $aFilter['where_condition'] ){

            $aWhereCondition = array();

            foreach( $aFilter['where_condition'] as $sKey=>$aValue ){

                // Получаем тип поля
                /** @noinspection PhpUndefinedMethodInspection */
                $aValueParams = $sCurrentClass::getParameter($sKey);


                if ( is_array($aValueParams) && sizeof($aValueParams) ){

                    // Собираем условие WHERE для выборки элементов
                    switch( $aValue['sign'] ){

                        case 'BETWEEN':
                            $aWhereCondition[] = "`$sKey` BETWEEN :".$sKey.'_from'." AND :".$sKey.'_to';
                            $aData[$sKey.'_from'] = $aValue['value'][0];
                            $aData[$sKey.'_to'] = $aValue['value'][1];
                            break;

                        case 'IN':
                            /**
                             * Потому как не работало
                             */
                            if (is_array($aValue['value'])){
                                if (count($aValue['value'])){
                                    $aValue['value'] = array_map(
                                        function( $s ) use ( &$aData ) {
                                            if (is_numeric( $s )){
                                                return (int)$s;
                                            }else{
                                                $sVarName = 'val_'.(count($aData)+1);
                                                while(isset($aData[$sVarName])){
                                                    $sVarName = $sVarName.'1';
                                                }
                                                $aData[$sVarName] = $s;
                                                return ':'.$sVarName;
                                            }
                                        },
                                        $aValue['value']
                                    );

                                    $aWhereCondition[] = "`$sKey` IN (".implode(',', $aValue['value']).")";
                                }
                            }else{
                                $aWhereCondition[] = "`$sKey` {$aValue['sign']} (:$sKey)";
                                $aData[$sKey] = $aValue['value'];
                            }
                            break;
                        default:
                            $aWhereCondition[] = "`$sKey` {$aValue['sign']} :$sKey";
                            $aData[$sKey] = $aValue['value'];
                            break;
                    }
                }
            }
            if ( sizeof($aWhereCondition) ) $sWhereCondition = "WHERE ".implode(' AND ', $aWhereCondition);
            else return false;
        }

        $sOrderCondition = '';

        if ( isset($aFilter['order']) ){

            $sOrderCondition = " ORDER BY {$aFilter['order']['field']} {$aFilter['order']['way']} ";
        }

        $sLimitCondition = '';

        // Обрабатываем ограничение по количеству выбираемых элементов
        if ( isset($aFilter['limit']) && $aFilter['limit'] ){

            $aData['start_limit'] = (int)$aFilter['limit']['start'];
            $aData['count_limit'] = (int)$aFilter['limit']['count'];
            $sLimitCondition = " LIMIT :start_limit, :count_limit";
        }

        /** @noinspection PhpUndefinedMethodInspection */
        $sTableName = $sCurrentClass::getCurrentTable();

        // SQL_CALC_FOUND_ROWS используется для подсчета общего количества записей
        $sQuery = "
            SELECT
            SQL_CALC_FOUND_ROWS
                $sSelectFields
            FROM
                `$sTableName`
            $sWhereCondition
            $sOrderCondition
            $sLimitCondition;";

        $rResult = Query::SQL( $sQuery, $aData );

        if ( !$rResult ) return false;

        // Собирается результирующий массив
        $aItems = array('items'=>array());
        while( $aRow = $rResult->fetchArray() ){

            // приведение к нужному типу

            foreach ( $aRow AS $sFieldName => $mFieldVal ) {
                $aParam = self::getParameter($sFieldName);
                if ( $aParam and $aParam['type'] === 'i' and !is_null($mFieldVal) )
                    $aRow[$sFieldName] = (int)$mFieldVal;
            }

            // выдача записи
            $aItems['items'][] = $aRow;

        }

        // Получаем общее число записей из таблицы
        $sCountQuery = "SELECT FOUND_ROWS() as `rows`;";
        $aItems['count'] = (int)Query::SQL($sCountQuery)->getValue( 'rows' );

        return $aItems;
    }

    /**
     * Возвращает требуемую по id или фильтру запись в виде одномерного массива
     * @static
     * @param int|array $mFilter - фильтр по параметрам
     * @return array
     */
    public static function getItem( $mFilter=array() ) {

        // если пришло число
        if ( is_numeric($mFilter) ) {
            // запросить по id
            $aFilter = array();
            $aFilter['where_condition'][static::getKeyFieldName()] = array(
                'sign' => '=',
                'value' => $mFilter
            );
        } elseif ( is_array($mFilter) ) {
            // если массив - использовать его как фильтр
            $aFilter = $mFilter;
        } else {
            // иначе отдать пустой массив
            return array();
        }

        // если нет ограничения по количеству
        if ( !isset($aFilter['limit']) ) {
            // добавить его
            $aFilter['limit'] = array(
                'start' => 0,
                'count' => 1
            );
        }

        // запросить элементы по фильтру
        $aItems = static::getItems( $aFilter );

        // если нашлись записи
        if ( $aItems['count'] ) {
            // отдать первую запись
            return $aItems['items'][0];
        } else {
            // наче вернуть пустой массив
            return array();
        }

    }

    /**
     * Удаление записи
     * @static
     * @param mixed $mFilter - id или массив фильтра
     * @return int|bool - false - ошибка, int - сколько удалено
     */
    public static function delItem( $mFilter ) {

        // имя ключевого поля - обязательное
        $sKeyFieldName = static::getKeyFieldName();

        if ( !$sKeyFieldName ) return false;

        if ( is_array($mFilter) ) {
            $iId = isset($mFilter[$sKeyFieldName]) ? (int)$mFilter[$sKeyFieldName] : 0;
        } else {
            $iId = (int)$mFilter;
        }

        if ( !$iId ) return false;

        $sTableName = static::getCurrentTable();

        $sQuery = "DELETE FROM `$sTableName` WHERE `{$sKeyFieldName}`=?;";

        return Query::SQL( $sQuery, $iId )->affectedRows();

    }// function

}// class
