<?php
/**
 *
 * @class skGateway
 *
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project JetBrains PhpStorm
 * @package kernel
 */
class skGateway {

    /**
     * @deprecated use init
     * todo delete it in 2016
     * @param $sGatewayServer
     * @param $sAppKey
     * @return skGatewayClient
     */
    public static function connect(/** @noinspection PhpUnusedParameterInspection */
        $sGatewayServer, $sAppKey) {
        return self::createClient();
    }

    /**
     * Устанавливает соединение с GatewayServer $sGatewayServer площадки. В качестве параметров аутентификации используется
     * Ключ площадки $sAppKey.
     * @example
     * try {
     *
     *  $oClient = skGateway::init();
     *
     *  $oClient->addHeader('MyHeader', '123');
     *  $oClient->addMethod('TestClass', 'TestMethod', array(1,2), array(new ResTest(), 'respo'));
     *
     *  if(!$oClient->doRequest()) throw new GatewayException($oClient-getError());
     *
     * } catch(GatewayException $e) {
     *  echo $e->getMessage();
     * }
     * после корректной инициализации вернет экземпляр skGatewayClient
     * @static
     * @throws GatewayException
     * @return skGatewayClient
     */
    public static function createClient() {

        // todo после перевода на SITE_ID убрать
        $iSiteId = defined('SITE_ID') ? SITE_ID : 0;

        if ( !defined('INCLUSTER') or !INCLUSTER )
            throw new \GatewayException( 'Not in cluster (by config param)' );

        if ( !defined('CLUSTERGATEWAY') or !CLUSTERGATEWAY )
            throw new \GatewayException( 'Gateway path not provided in config' );

        if ( !defined('APPKEY') or !APPKEY )
            throw new \GatewayException( 'Gateway application key not provided in config' );

        $oClient = new skGatewayClient(CLUSTERGATEWAY, $iSiteId, skGatewayClient::StreamTypeEncrypt);

        $oClient->setKey(APPKEY);
        if ( $_SERVER['HTTP_HOST'] )
            $oClient->setClientHost( $_SERVER['HTTP_HOST'] );

        $oCrypt = new skBlowfish();
        $oCrypt->setIv( \Yii::$app->params['security']['vector'] );

        $oClient->onEncrypt(array($oCrypt, 'encrypt'));
        $oClient->onDecrypt(array($oCrypt, 'decrypt'));

        return $oClient;

    }

}
