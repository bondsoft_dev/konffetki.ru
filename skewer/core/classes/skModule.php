<?php

/**
 * Прототип модуля
 *
 * @class skModule
 * @project Canape 3.0 (skewer)
 * @package kernel
 * @implements skModuleInterface
 * @author ArmiT $Author: sapozhkov $
 * @version $Revision: 2702 $
 * @date $Date: 2013-12-11 17:21:05 +0400 (Ср., 11 дек. 2013) $
 *
 */
abstract class skModule extends \yii\base\Component implements skModuleInterface {

    /**
     * Экземпляр Контекста вызова
     * @var null|skContext
     */
    protected $oContext = NULL;

    /**
     * Директория с CSS файлами модуля от корня директории модуля
     * @var string
     */
    private $sCSSDir = 'css';

    /**
     * Директория с шаблонами модуля от корня директории модуля
     * @var string
     */
    protected $sTemplateDir = 'templates';

    /**
     * Директория с JS файлами модуля от корня директории модуля
     * @var string
     */
    private $sJSDir = 'js';

    /**
     * Флаг использования правил разбора url
     * @var bool
     */
    private $bUseRouting = true;

    /**
     * Зона вывода
     * @var string
     */
    protected $zone = '';

    /**
     * Категория для словаря
     * @var string
     */
    protected $languageCategory = '';

    /**
     * Имя модуля
     * @var string
     */
    protected $title = '';

    /**
     * Категория для словаря
     * @return string
     */
    public function getCategoryMessage(){
        return $this->languageCategory;
    }

    /**
     * Задает флаг использования правил разбора url
     * @param boolean $bUseRouting
     */
    public function setUseRouting( $bUseRouting ) {
        $this->bUseRouting = (bool)$bUseRouting;
    }

    /**
     * Отдает флаг использования правил разбора url
     * @return boolean
     */
    public function useRouting() {
        return $this->bUseRouting;
    }


    /**
     * Получение группы в которой находится метка модуля
     * @return string
     */
    public function getLabel() {
        return $this->oContext->getLabel();
    }

    /**
     * Имя модуля
     * @return string
     */
    public function getTitle(){
        return $this->title;
    }

    /** @noinspection PhpMissingParentConstructorInspection
     * Создает экземпляр модуля
     * @param skContext $oContext Передаваемый контекст
     * @noinspection PhpMissingParentConstructorInspection
     */
    public function __construct(skContext $oContext) {

        $sClassName  = get_class($this);
        $sModulePath = Yii::getAlias('@' . str_replace('\\', '/', $sClassName) . '.php');

        $this->oContext = $oContext;

        $this->oContext->setModuleDir(dirname($sModulePath));
        $this->oContext->setModuleWebDir($this->getAssetWebDir($sModulePath));
        $this->oContext->setTplDirectory( $this->getTplDirectory() );

        // данные по модулю - слой и имя
        $aPathItems = explode(DIRECTORY_SEPARATOR, trim(dirname($sModulePath), DIRECTORY_SEPARATOR));
        $this->oContext->setModuleName(  array_pop($aPathItems) );
        $this->oContext->setModuleLayer( array_pop($aPathItems) );

        $oConfig = \Yii::$app->register->getModuleConfig($this->oContext->getModuleName(), $this->oContext->getModuleLayer());

        $this->languageCategory = $oConfig->getLanguageCategory();

        $this->title = $oConfig->getTitle();

        $this->overlayParams($oContext->getParams());

        // Пользовательская функция инициализации модуля
        $this->onCreate();

        return true;

    }

    /**
     * Отдает флаг того, что модуль использует namespace
     * @return bool
     */
    public function useNamespace() {
        return strpos( get_class($this), '\\' ) !== false;
    }

    /**
     * Пользовательская функция инициализации модуля
     */
    protected function onCreate() {}

    /**
     * Перекрывает свойства классу модуля
     * @param array|null $aOverlayParams
     * @return bool
     */
    final public function overlayParams($aOverlayParams = null) {


        if(!count($aOverlayParams))
            return;

        if (isset($aOverlayParams['zone']))
            $this->zone = $aOverlayParams['zone'];

        $oRef = new ReflectionClass(get_class($this));
        $aPropValues = $oRef->getDefaultProperties();
        $aProperties = $oRef->getProperties(ReflectionProperty::IS_PUBLIC | ReflectionProperty::IS_PROTECTED);

        if (count($aProperties)) {
            foreach ($aProperties as $oProperty) {

                if (!($oProperty instanceof ReflectionProperty)) continue;

                /** Не перекрывать системные параметры модуля */
                if ($oProperty->class == 'skModule') continue;

                if ($oProperty->name == 'oContext') continue;
                $sName = $oProperty->name;
                $this->$sName = $aPropValues[$sName];
                if (isSet($aOverlayParams[$sName]))
                    $this->$sName = $aOverlayParams[$sName];

            }
        }

    }

    /**
     * Закрытый деструктор без возможности перекрытия
     */
    final public function __destruct() {

    }// func

    /**
     * Прототип - выполняется до вызова метода Execute
     * @return bool
     */
    public function init() {

    }// func

    /**
     * Прототип - вызывается после метода Execute
     * @return bool
     */
    public function shutdown() {

    }// func

    /**
     * Разрешить выполнение модуля
     * @return bool
     */
    public function allowExecute() {
        return true;
    }// func

    /**
     * Добавляет данные для вывода в шаблонизатор
     * @final
     * @param string $sLabel Метка для вставки данных
     * @param mixed $mData Данные
     * @return bool
     */
    final public function setData($sLabel, $mData) {
        $this->oContext->setData($sLabel, $mData);
        return true;
    }// func

    /**
     * Возвращает Данные, установленные модулем в процессе выполнения
     * @param string $sLabel Если указан параметр sLabel -  будет возвращено только то значение,
     * которое к нему прикреплено.
     * @return mixed
     */
    final public function getData($sLabel = '') {

        return $this->oContext->getData($sLabel);
    }// func

    /**
     * Устанавливает шаблон для вывода
     * @final
     * @param string $sTemplate Шаблон для рендеринга данных
     * @return bool
     */
    final public function setTemplate($sTemplate) {

        $this->oContext->setTemplate($sTemplate);
        return true;
    }// func

    /**
     * Устанавливает вывод, обработанный шаблонизатором
     * @final
     * @param string $sOut
     * @return bool
     */
    final public function setOut($sOut) {
        $this->oContext->setOut($sOut);
        return true;
    }// func

    /**
     * Указывает тип шаблонизатора
     * @final
     * @param int $iParserType
     * @return bool
     */
    final public function setParser($iParserType) {
        $this->oContext->setParser($iParserType);
        return true;
    }// func

    /**
     * Возвращает путь к директории модуля
     * @final
     * @return string
     */
    final public function getModuleDir() {
        return $this->oContext->getModuleDir();
    }// func

    /**
     * Возвращает web путь к директории модуля
     * @final
     * @return string
     */
    final public function getModuleWebDir() {
        return $this->oContext->getModuleWebDir();
    }// func

    /**
     * Ищет разобранный GET/POST/JSON целочисленный параметр.
     * @param string $sName Имя исходного параметра
     * @param int $mDefault Значение, подставляемое по-умолчанию
     * @return int
     */
    final public function getInt($sName, $mDefault = 0) {
        return (int)self::getStr($sName, $mDefault);
    }// func

    /**
     *  Ищет разобранный GET/POST/JSON строковый параметр.
     * @param string $sName Имя исходного параметра
     * @param string $mDefault Значение, подставляемое по-умолчанию
     * @return string
     */
    final public function getStr($sName, $mDefault = '') {

        /**
         * Ищем в POST и JSON параметрах
         * Перекрываем GET параметрами
         */
        $sVal = skRequest::getStr($sName,$this->oContext->sLabelPath);
        if(!is_null($sVal)) return $sVal;
        //if(skRequest::getStr($this->oContext->sLabelPath, $sName, $sVal)) return $sVal;
        if($this->oContext->oProcess->oRouter->getStr($sName,$sVal)) return $sVal;
        /** @todo Добавить проверку (Запрос через skRequest) */
        //if($this->oContext->oProcessor->oRouter->getStr($sName,$sVal)) return $sVal;

        return $mDefault;
    }// func


    /**
     *  Ищет разобранный GET/POST/JSON неопределенный параметр.
     * @param string $sName Имя исходного параметра
     * @param string $mDefault Значение, подставляемое по-умолчанию
     * @return mixed
     */
    final public function get($sName, $mDefault = '') {

        /**
         * Ищем в POST и JSON параметрах
         * Перекрываем GET параметрами
         */

        //$val = skRequest::getStr($this->oContext->sLabelPath, $sName, $sVal);
        $sVal = skRequest::getStr($sName,$this->oContext->sLabelPath);
        // @todo я заипался уже с нашим реквестером, сделайте уже что-нибудь.
        if($mDefault !== $sVal && !is_null($sVal)) return $sVal;
        if($this->oContext->oProcess->oRouter->getStr($sName,$sVal)) return $sVal;
        /** @todo Добавить проверку (Запрос через skRequest) */
        //if($this->oContext->oProcessor->oRouter->getStr($sName,$sVal)) return $sVal;

        return $mDefault;
    }// func

    /**
     * Сохраняет во входной массив (GET/POST/JSON) значение
     * @param $sName
     * @param $mValue
     */
    final function set( $sName, $mValue ) {
        skRequest::set($this->oContext->sLabelPath, $sName, $mValue);
    }


    /**
     * Получение параметров пришедших на изменение для модуля
     */
    final protected function getPost() {
        // TODO !!! сделать проверку на принадлежность данных этому модулю в этой метке вывода
        return $_POST;
    }


    /**
     * Возвращает имя текущего модуля
     * @return string
     */
    final public function getModuleName() {
        return $this->oContext->getModuleName();
    }

    /**
     * отдает имя слоя
     * @return string
     */
    public function getLayerName() {
        return $this->oContext->getModuleLayer();
    }

    final public function getModuleNameAdm() {

        return $this->getModuleName().'Adm';
    }

    /**
     * Устаналивает имя текущего модуля
     * @param string $sModuleName Имя модуля
     * @return bool
     */
    final public function setModuleName($sModuleName) {
        return $this->oContext->setModuleName($sModuleName);
    }// func


    /**
     * Возвращает значение внутреннего поля
     * @param string $sFieldName Имя поля
     * @param null $mDef Значение по умолчанию
     * @return mixed
     */
    public function getModuleField( $sFieldName, $mDef = null ) {

        return isSet( $this->$sFieldName ) ? $this->$sFieldName : $mDef;
    }

    /**
     * Возвращает экземпляр процесса по цепочке меток до него
     * @final
     * @param string $sPath Путь от корневого процесса до искомого
     * @param integer $iStatus Статус искомого процесса. psAll - выбрать все
     * @return \skProcess|int
     */
    final public function getProcess($sPath, $iStatus = psComplete) {
        return \Yii::$app->processList->getProcess($sPath, $iStatus);
    }// func

    /**
     * Adding child process in parent process
     * @param skContext $oContext
     * @return \skProcess|bool
     */
    public function addChildProcess(skContext $oContext) {
        return $this->oContext->oProcess->addChildProcess($oContext);
    }// func

    /**
     * Возвращает список ссылок на дочерние процессы.
     * @return skProcess[]
     */
    final public function getChildProcesses() {
        return $this->oContext->oProcess->processes;
    }// func

    /**
     * Возвращает ссылку на дочерний процесс по метке $sLabel его вызова
     * @param string $sLabel Метка вызова дочернего процесса
     * @return bool|skProcess Возвращает ссылку на процесс либо false
     */
    final public function getChildProcess($sLabel) {
        return (isSet($this->oContext->oProcess->processes[$sLabel]))? $this->oContext->oProcess->processes[$sLabel]: false;
    }// func

    /**
     * Устанавливает статус $iStatus дочернему процессу в метке вызова $sLabel
     * @param string $sLabel Название метки вызова для дочернего процесса
     * @param integer $iStatus Константа статуса
     * @return bool Возвращает true если процесс найден и false в противном случае
     */
    final public function setChildProcessStatus($sLabel, $iStatus) {
        if(isSet($this->oContext->oProcess->processes[$sLabel])) {
            /** @noinspection PhpUndefinedMethodInspection */
            $this->oContext->oProcess->processes[$sLabel]->setStatus($iStatus);
            return true;
        }
        return false;
    }// func

    /**
     * Удаляет подчиненный процесс
     * @param $sLabel
     * @return bool
     */
    final public function removeChildProcess( $sLabel ) {
        return $this->oContext->oProcess->removeChildProcess( $sLabel );
    }

    /**
     * Удаляет все подчиненные процессы
     * @return int количество удаленных процессов
     */
    final public function removeAllChildProcess() {

        $aProcesses = $this->getChildProcesses();

        // удалить все по очереди
        /* @var $oProcess skProcess */
        foreach ( $aProcesses as $oProcess ) {
            $this->removeChildProcess( $oProcess->getLabel() );
        }

        return count($aProcesses);

    }

    /**
     * Устанавливает новый параметр окружения с именем $sName и значением $mValue
     * @param string $sName Имя параметра окружения
     * @param string $mValue Значение параметра окружения
     */
    final public function setEnvParam($sName, $mValue) {
        \Yii::$app->environment->set($sName, $mValue);
    }

    /**
     * Возвращает параметр окружения $sName либо $mDefault в случае, если параметр отсутствует.
     * @param string $sName Имя параметра окружения
     * @param bool|mixed $mDefault Значение, возвращаемое, если параметр не был найден.
     * @return mixed
     */
    final public function getEnvParam($sName, $mDefault = false) {
        return \Yii::$app->environment->get($sName, $mDefault);
    }

    /**
     * Возвращает список параметров окружения (обших переменных в рамках дерева процессов)
     * @return mixed
     */
    final public function getEnvParamsList() {
        return \Yii::$app->environment->getAll();
    }

    /**
     * Добавляет в список вывода на страницу JS файл $sFileName. В условии $sCondition, если задано.
     * @param string $sFileName - Имя JS-файла в директории текущего модуля
     * @param bool $sCondition Условие (Используется совместно с <!--[if ]><![endif]-->)
     * @return bool
     */
    final public function addJSFile($sFileName, $sCondition = false){

        if ( $sFileName[0] === '/' )
            $sPath = '';
        else
            $sPath = $this->getModuleWebDir().DIRECTORY_SEPARATOR.$this->sJSDir.DIRECTORY_SEPARATOR;

        $sFilePath = $sPath.$sFileName;

        return skLinker::addJSFile( $sFilePath, $sCondition);
    }// func

    /**
     * Добавляет в список вывода на страницу CSS файл $sFileName. В условии $sCondition, если задано.
     * @param string $sFileName - Имя CSS-файла в директории текущего модуля
     * @param bool $sCondition Условие (Используется совместно с <!--[if ]><![endif]-->)
     * @param bool $bToStart добавить файл в начало списка
     * @return bool
     */
    final public function addCSSFile($sFileName, $sCondition = false, $bToStart = false){

        if ( $sFileName[0] === '/' )
            $sPath = '';
        else
            $sPath = $this->getModuleWebDir().DIRECTORY_SEPARATOR.$this->sCSSDir.DIRECTORY_SEPARATOR;

        return $this->addCssByPath( $sPath.$sFileName, $sCondition, $bToStart);
    }// func

    /**
     * Добавление css файла с полным указанием пути
     * @param string $sFilePath - полный путь к CSS-файлу
     * @param bool $sCondition Условие (Используется совместно с <!--[if ]><![endif]-->)
     * @param bool $bToStart добавить файл в начало списка
     * @return bool
     */
    final public function addCssByPath( $sFilePath, $sCondition = false, $bToStart = false ) {
        return skLinker::addCssFile( $sFilePath, $sCondition, $bToStart );
    }

    /**
     * Возвращает путь к директории хранения JS файлов модуля
     * @return string
     */
    final public function getJSDirectory(){

        return $this->sJSDir;

    }// func

    /**
     * Изменяет путь к директории хранения JS файлов модуля
     * @param string $sDir Директория расположения JS файлов модуля
     * @return string
     */
    final public function setJSDirectory($sDir){

        return $this->sJSDir = $sDir;

    }// func

    /**
     * Возвращает путь к директории хранения CSS файлов модуля
     * @return string
     */
    public function getCSSDirectory(){

        return $this->sCSSDir;

    }// func

    /**
     * Изменяет путь к директории хранения CSS файлов модуля
     * @param string $sDir Директория расположения CSS файлов модуля
     * @return string
     */
    public function setCSSDirectory($sDir){

        return $this->sCSSDir = $sDir;

    }// func

    /**
     * Возвращает путь к директории хранения шаблонов модуля
     * @return string
     */
    public function getTplDirectory(){

        return $this->sTemplateDir;

    }// func

    /**
     * Изменяет путь к директории хранения шаблонов модуля
     * @param string $sDir Директория расположения CSS файлов модуля
     * @return string
     */
    public function setTplDirectory($sDir){

        $this->oContext->setTplDirectory( $sDir );
        return $this->sTemplateDir = $sDir;

    }// func

    /**
     * Генирирует массив для постраничного вывода.
     * @param integer $iPage Текущая страница постраничного
     * @param integer $iCount Количество элементов в списке (например, товаров в категории)
     * @param integer $iSectionId Раздел для вывода постраничного списка
     * @param array $aURLParams массив дополнительных GET параметров
     * @param array $aParams Параметры настройки внешнего вида постраничного см. \skPaginator
     * @param string $sLabel Метка вывода сгенерированного массива в шаблон.
     * @return string
     */
    final public function getPageLine($iPage, $iCount, $iSectionId, $aURLParams = array(), $aParams = array(), $sLabel = 'aPages') {

        $aURL[$this->oContext->sClassName] = (count($aURLParams))? $aURLParams: array();

        return $this->oContext->setData($sLabel, Paginator::getPageLine($iPage, $iCount, $iSectionId, $aURL, $aParams));

    }// func

    /**
     * Установить дополнительный JSON Header в response package от модуля
     * @param string $sKey Заголовок
     * @param mixed $mValue Значение заголовка
     */
    final public function setJSONHeader( $sKey, $mValue ) {
        $this->oContext->setJSONHeader( $sKey, $mValue );
    }

    /**
     * Удалить дополнительный JSON Header в response package от модуля
     * @param string $sKey Заголовок
     * @return bool
     */
    final public function unsetJSONHeader( $sKey ) {
        return $this->oContext->unsetJSONHeader( $sKey );
    }

    /**
     * Получить дополнительный JSON Header по названию $sKey
     * @param $sKey
     * @return null|mixed
     */
    final public function getJSONHeader( $sKey ) {
        return $this->oContext->getJSONHeader( $sKey );
    }

    /**
     * Отрендерить шаблон, вернуть строку с результатом
     * @param $sTemplate - шаблон
     * @param array $aData - массив для парсинга
     * @return string
     */
    protected function renderTemplate( $sTemplate, $aData=array() ) {
        return skParser::parseTwig( $sTemplate, $aData, $this->getModuleDir().$this->getTplDirectory().DIRECTORY_SEPARATOR );

    }

    /**
     * Возвращает параметр конфигурации модуля по ключу $key в слое $sLayer
     * @param string $sParamPath
     * @return mixed
     */
    final protected function getConfigParam($sParamPath){

        return \Yii::$app->register->getModuleConfigParam(
            $sParamPath,
            $this->getModuleName(),
            $this->getLayerName()
        );

    }

    public function addCriticalReport($sTitle, $sDescription, $iLogType, $sCalledClass, $sTitleCalled = ''){
        return skLogger::addCriticalReport($sTitle, $sDescription, $iLogType, $sCalledClass, $sTitleCalled);
    }

    public function addWarningReport($sTitle, $sDescription, $iLogType, $sCalledClass, $sTitleCalled = ''){
        return skLogger::addWarningReport($sTitle, $sDescription, $iLogType, $sCalledClass, $sTitleCalled);
    }

    public function addErrorReport($sTitle, $sDescription, $iLogType, $sCalledClass, $sTitleCalled = ''){
        return skLogger::addErrorReport($sTitle, $sDescription, $iLogType, $sCalledClass, $sTitleCalled);
    }

    public function addNoticeReport($sTitle, $sDescription, $iLogType, $sCalledClass, $sTitleCalled = ''){
        return skLogger::addNoticeReport($sTitle, $sDescription, $iLogType, $sCalledClass, $sTitleCalled);
    }

    /**
     * Вычисляет директорию для клиентских файлов (с учетом asset механики)
     * @param string $sModulePath полное имя основного класса модуля
     * @return string отдает имя директории от корня url
     * #stab_fix можно использовать имя класса, а не полный путь
     * @throws Exception
     */
    protected function getAssetWebDir($sModulePath) {
        $sDirName = '/skewer/build' . substr(dirname($sModulePath), strlen(BUILDPATH) - 1);

        $sAssetClass = str_replace(DIRECTORY_SEPARATOR, '\\', substr($sDirName,1)).'\Asset';

        if ( class_exists($sAssetClass) ) {

            if ( !is_subclass_of($sAssetClass, 'yii\web\AssetBundle') )
                throw new \Exception('Module asset must be extended from yii\web\AssetBundle');

            // #stab_fix view объект должен быть один, а не создаваться каждый раз
            $view = new \yii\web\View();
            /** @var yii\web\AssetBundle $sAssetClass */
            $bundle = $sAssetClass::register($view);
            return $bundle->baseUrl;

        }

        return $sDirName;
    }


    /**
     * Парсинг массива сообщений для модуля
     * @param $aMessages
     * @return array
     */
    public function parseLangVars( $aMessages ){

        $aOut = [];

        foreach ( $aMessages as $mKey => $sAlias ) {

            $aOut[is_numeric($mKey) ? $sAlias : $mKey] = \Yii::t($this->getCategoryMessage(), $sAlias);

        }

        return $aOut;
    }

}// class
