<?php
/**
 * Представление данных

 * ~~~
 * #stb_move
 * 1. перенести в компонент
 * 2. попробовать использовать yii-twig
 * 3. пройтись по методам $aParserHelpers
 * 3.1 - возможно использоватль стандартный механизм для $aParserHelpers
 * ~~~
 * @class skParser
 * @project Canape 3.0 (skewer)
 * @package kernel
 * @Author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 *
 */
class skParser {

    /**
     * Массив ссылок на классы-хелперы в шаблонах
     * @var array
     */
    static private $aParserHelpers = array();

    /**
     * Добавялет класс-helper для использования в шаблонах
     * @param object $oHelper Передаваемый в шаблон класс
     * @param string $sName Имя для использования объекта
     * @return bool
     */
    public static function setParserHelper(&$oHelper,$sName='') {

        if(!is_object($oHelper)) return false;
        self::$aParserHelpers[$sName?$sName:get_class($oHelper)] = &$oHelper;

        return true;
    }// func

    /**
     * Возвращает массив ссылок на объекты и static классы хелперов, доступных в шаблонах
     * @return array
     */
    public static function getParserHelpers() {

        return self::$aParserHelpers;
    }// func



    /**
     * Производит запуск указанного в контексте шаблонизатора. Используя шаблон и данные, формирует вывод.
     * @static
     * @param skContext $oContext Контекст обрабатываемого процесса
     * @throws skException
     * @return bool|string
     */
    public static function render(skContext &$oContext) {

        $sOut       = '';
        $aData      = $oContext->getData();
        $aData['_objectId'] = $oContext->getLabel();

        if (strpos($oContext->getTplDirectory(), '/') === 0) {
            $sModuleDir = $oContext->getTplDirectory();
            $oContext->setTplDirectory('');
        }
        else {
            $sModuleDir = $oContext->getModuleDir().$oContext->getTplDirectory().DIRECTORY_SEPARATOR;
        }

        switch ($oContext->getParser()) {
            case parserPHP:
                // todo #yii чтобы пока не трогать базу с шаблонами, подменим путь к вьюхе
                $template = str_replace('.twig','.php',$sModuleDir.$oContext->getTemplate());

                // Если в данных есть настройки зон расположения - производим их обработку

                if(isset($aData['.layout']))
                    foreach($aData['.layout'] as $sZoneName=>$aZoneData) {

                        $aData['layout'][$sZoneName] = '';

                        foreach($aZoneData as $sLabel){
                            if(is_array($sLabel)){

                                // обрабатываем инклуды
                                $aData['layout'][$sZoneName] .=
                                    self::parseTwig( $sLabel['templateFile'], $aData, $sLabel['templateDir']);
                            } // if
                            else {
                                if(isset($aData[$sLabel])) {
                                    if ( is_string($aData[$sLabel]) )
                                        $aData['layout'][$sZoneName] .= $aData[$sLabel];
                                    elseif ( is_array($aData[$sLabel]) and isset($aData[$sLabel]['text']) )
                                        $aData['layout'][$sZoneName] .= $aData[$sLabel]['text'];
                                }
                            } // else
                        } // foreach

                    } // if / foreach

                $sOut = Yii::$app->getView()->renderFile($template,$aData);
                break;
            case parserTwig:

                // Если в данных есть настройки зон расположения - производим их обработку

                if(isset($aData['.layout']))
                    foreach($aData['.layout'] as $sZoneName=>$aZoneData) {

                        $aData['layout'][$sZoneName] = '';

                        foreach($aZoneData as $sLabel){
                            if(is_array($sLabel)){


                                // обрабатываем инклуды
                                $aData['layout'][$sZoneName] .=
                                    self::parseTwig( $sLabel['templateFile'], $aData, $sLabel['templateDir']);
                            } // if
                            else {
                                if(isset($aData[$sLabel])) {
                                    if ( is_string($aData[$sLabel]) )
                                        $aData['layout'][$sZoneName] .= $aData[$sLabel];
                                    elseif ( is_array($aData[$sLabel]) and isset($aData[$sLabel]['text']) )
                                        $aData['layout'][$sZoneName] .= $aData[$sLabel]['text'];
                                }
                            } // else
                        } // foreach

                    } // if / foreach

                $tplName = $oContext->getTemplate();

                if ( !$tplName ) {
                    $sOut = '';
                    break;
                }

                    
                if (strpos($tplName, DIRECTORY_SEPARATOR)!==false){
                    $sModuleDir = dirname($tplName);
                    $tplName = basename($tplName);
                }

                $sOut = self::parseTwig( $tplName, $aData, $sModuleDir );

                break;

            case parserContenter:

                break;

            case parserJSON:

                if($oContext->oProcess->getStatus() == psComplete)
                    \Yii::$app->jsonResponse->addJSONResponse($oContext);

                break;
        }// switch parser type

        return $sOut;

    }// func

    /**
     * Отрендерить шаблон, вернуть строку с результатом
     * @param $sTemplate - шаблон
     * @param array $aData - массив для парсинга
     * @param string $sTemplateDir - адрес шаблона
     * @return string
     */
    public static function parseTwig( $sTemplate, $aData, $sTemplateDir='' ) {

        // набор предустановленных в конфиге путей для парсинга
        $aConfigPaths = \Yii::$app->params['parser']['default']['paths'];
        if ( !is_array($aConfigPaths) ) $aConfigPaths = array();

        $aTplPaths = array_merge($aConfigPaths,array($sTemplateDir));
        $aTplPaths = array_diff($aTplPaths, array(''));

        /** @fixme Добавил путь до папки commons/templates */
        skTwig::setPath( $aTplPaths );

        /* Получить список хелперов для шаблонов */
        $aParserHelpers = skParser::getParserHelpers();
        if(count($aParserHelpers))
            foreach($aParserHelpers as $sHelperName => $oHelperObject)
                skTwig::assign($sHelperName, $oHelperObject);

        foreach ($aData as $sLabel => $mData) {
            skTwig::assign($sLabel, $mData);
        }

        return skTwig::render($sTemplate);

    }

}// class
