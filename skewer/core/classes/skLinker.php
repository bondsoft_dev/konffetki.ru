<?php

use skewer\core\Component\Config as Config;

/**
 * Агрегатор clientside файлов (css/js)
 * @class ClientFiles
 *
 * @deprecated Пересмотреть назначение класса
 * @author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @project Skewer
 * @package kernel
 * @singleton
 */ 
class skLinker {

    /**
     * Экземпляр класса
     * @var null
     */
    static private $oInstance = NULL;

    /**
     * Список подключенных JS файлов
     * @var array
     */
    static private $aJSFiles = array();

    /**
     * Список подключенных CSS файлов
     * @var array
     */
    static private $aCSSFiles = array();

    /**
     * Путь до корневой build - директории
     * @var string
     */
    static private $sCorePath = '';

    /**
     * Закрывающий CSS файл - подключается последним
     * @var string
     */
    static private $sEndCSSFile = '';

    /**
     * Закрытый конструктор
     * @param string $sCorePath Путь до корневой build - директории
     */
    private function __construct($sCorePath) {

        self::$sCorePath = $sCorePath;

    }// func

    /**
     * Инициализирует сборщик файлов (Linker)
     * @static
     * @param string $sCorePath Путь до корневой build - директории
     * @return null
     */
    public static function init($sCorePath) {

        if(isset(self::$oInstance) and (self::$oInstance instanceof self)){

            return self::$oInstance;
        }else{

            self::$oInstance= new self($sCorePath);

            return self::$oInstance;
        }
    }// func

    /**
     * Добавляет в Linker JS-Файл $mFilePath. Если $mFilePath - массив, то возможна установка
     * нескольких значений и учитываются фильтры, устанавливаемые вложенным массивом.
     * @static
     * @param array|string $mFilePath Путь либо массив путей до файлов
     * @param bool|string $sCondition Условие вывода файла в браузере
     * @return bool
     *
     * @example:
     * Ниже пример массива файлов с условиями:
     * array(
     * 'skewer/build/Page/Main/js/init.js',
     * 'skewer/build/Page/Main/js/init.ie.js' => array('condition' => 'IE 7'));
     */
    static public function addJSFile($mFilePath, $sCondition = false) {

        if(!$mFilePath) return false;

        if(is_array($mFilePath)) {
            foreach($mFilePath as $sKey=>$aParams){
                if(is_array($aParams))
                    self::$aJSFiles[$sKey] = $aParams;
                else
                    self::$aJSFiles[$aParams] = ($sCondition)? array('condition' => $sCondition): false;
            }
        } else {
            self::$aJSFiles[$mFilePath] = ($sCondition)? array('condition' => $sCondition): false;
        }

        return true;
    }// func

    /**
     * Добавляет в Linker CSS-Файл $mFilePath.
     * @static
     * @param array|string $mFilePath Путь
     * @param bool|string $sCondition Условие вывода файла в браузере для строкового первого параметра
     * @param bool $bToStart добавить файл в начало списка
     * @return bool
     */
    static public function addCssFile($mFilePath, $sCondition = false, $bToStart = false) {

        if(!$mFilePath)
            return false;

        if ( $sCondition and $sCondition===Config\Vars::CSS_DEFAULT_CONDITION )
            $sCondition = false;

        $aData = array(
            Config\Vars::CSS_NAME => $mFilePath,
            Config\Vars::CSS_CONDITION => $sCondition
        );

        if ( $bToStart ) {
            // todo можно заменить на веса
            $aData[ Config\Vars::CSS_TO_START ] = $bToStart;
        }

        self::$aCSSFiles[$mFilePath] = $aData;

        return true;

    }// func

    /**
     * Возвращает список собранных классом Linker JS файлов
     * @static
     * @param bool $bValVersion подстановка версии
     * @return array
     */
    static public function getJSFiles($bValVersion = true) {

        $aOut = array();
        $lastUpdatedTime = Design::getLastUpdatedTime();

        reset(self::$aJSFiles);
        while (list($sFilePath, $aParams) = each(self::$aJSFiles)) {
            if ($lastUpdatedTime && $bValVersion) $sFilePath.= '?v='.$lastUpdatedTime;
            $aOut[] = array(
                'condition' => ($aParams and isset($aParams['condition']))? $aParams['condition']: false,
                'filePath'  => $sFilePath,
            );
        }// each file
        
        return $aOut;

    }// func

    /**
     * Возвращает список собранных классом Linker CSS файлов
     * @static
     * @param bool $bValVersion подстановка версии
     * @return array
     */
    static public function getCssFiles($bValVersion = true) {

        $aOut = array();
        $lastUpdatedTime = Design::getLastUpdatedTime();

        foreach ( self::$aCSSFiles as $sFilePath => $aParams ) {
            if ($lastUpdatedTime && $bValVersion) $sFilePath.= '?v='.$lastUpdatedTime;
            $aData = array(
                'condition' => ( isset($aParams['condition']) )? $aParams['condition']: false,
                'filePath'  => $sFilePath,
            );
            if ( isset( $aParams[Config\Vars::CSS_TO_START] ) and $aParams[Config\Vars::CSS_TO_START] )
                array_unshift( $aOut, $aData );
            else
                array_push( $aOut, $aData );
        }// each file

        // добавляем финальный css в конец общего массива css
        if (self::$sEndCSSFile) {
            if ($lastUpdatedTime) $sFilePath = self::$sEndCSSFile.'?v='.$lastUpdatedTime;
            else $sFilePath = self::$sEndCSSFile;
            $aOut[] = array(
                'condition' => false,
                'filePath'  => $sFilePath,
            );
        }

        return $aOut;
    }// func

    /**
     * Очищает набор установленных css файлов
     * @static
     */
    static public function clearCssFiles(){
        self::$aCSSFiles = array();
    }

    /**
     * Очищает набор установленных js файлов
     * @static
     */
    static public function clearJsFiles(){
        self::$aJSFiles = array();
    }

    /**
     * Добавляет в Linker CSS-Файл $mFilePath, который будет подключен в конце общего списка
     * CSS файлов .
     * @static
     * @param string $mFilePath Путь до файла
     * @return bool
     */
    static public function setEndCSSFile($mFilePath) {

        if(!$mFilePath) return false;
        self::$sEndCSSFile = $mFilePath;
        return true;

    }

    /**
     * Отдает значение внутренней переменной с набором js файлов - без обработки
     * @return array
     */
    public static function getJSList() {
        return self::$aJSFiles;
    }

    /**
     * Задает значение внутренней переменной с набором js файлов
     * @param array $aJSFiles
     */
    public static function setJSList( $aJSFiles ) {
        self::$aJSFiles = $aJSFiles;
    }


}// class
