<?php
/**
 * Менеджер POST(JSON) параметров.
 *
 * @class RequestParams
 * @project Skewer
 * @package kernel
 * @Author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 * @static
 * @singleton
 */
class skRequest {

    /**
     * Набор POST параметров
     * @var array
     */
    static private $aParams = array();

    /**
     * Массив запрашиваемых на выполнение events
     * @var array
     */
    static private $aEvents = array();

    /**
     * true, если запрос для админского интерфейса
     * @var bool
     */
    static private $bIsCmsRequest = false;

    /**
     * Идентификатор сессии
     * @var string
     */
    static private $sSessionId = '';

    /**
     * Экземпляр текущего класса
     * @var null
     */
    static private  $oInstance = NULL;

    /**
     * Набор JSON заголовков, если пришли
     * @var array
     */
    static private $aJsonHeaders = array();

    /**
     * Закрываем возможность вызвать clone
     */
    private function __clone() {}

    /**
     * Инициализация менеджера входящих параметров
     * @static
     * @return null
     */
    static public function init() {

        if(isset(self::$oInstance) and (self::$oInstance instanceof self)){

            return self::$oInstance;
        }else{

            self::$oInstance= new self();

            return self::$oInstance;
        }
    }// func

    /**
     * Создает экземпляр менеджера параметров. Обрабатывает POST параметры.
     * В случае обнаружения JSON Package - разбирает его.
     */
    private function __construct() {

       //return true;
        /* Обычные post параметры */
        if(count($_POST))
            foreach($_POST as $sKey=>$sData)
                self::$aParams['post'][$sKey] = $sData;


        if(count($_GET))
            foreach($_GET as $sKey=>$sData)
                self::$aParams['get'][$sKey] = $sData;


        // @todo ынести в метод #yii
        $contentType = Yii::$app->getRequest()->getContentType();
        if (($pos = strpos($contentType, ';')) !== false) {
            // e.g. application/json; charset=UTF-8
            $contentType = substr($contentType, 0, $pos);
        }

        // если тип запроса - JSON
        //if($contentType=='application/json') {
        if($contentType=='application/json') {
            $aRequest = Yii::$app->getRequest()->getBodyParams();
            self::$bIsCmsRequest = true;

            if($aRequest['sessionId'] && !empty($aRequest['sessionId']))
                self::$sSessionId = $aRequest['sessionId'];

            if(isSet($aRequest['data'])) unSet($aRequest['data']);

            /*Смотрим есть ли запросы на обработку событий*/
            if(isSet($aRequest['events']) && count($aRequest['events']))
                foreach($aRequest['events'] as $sEvent=>$aParams) {
                    self::$aEvents[$sEvent] = $aParams;
                }// each param

            self::$aJsonHeaders = $aRequest;

        } else {

            /*Отработка посылки в POST*/

            /**
             * Прошу простить меня за этот код, я бы с самого начала против этого говнокода.
             * Эти костыли были созданы под строгим надзором и полным одобрением
             * нынешнего технического директора Сапожкова Александра. За сим откланююсь,
             * прошу еще раз принять и простить
             */
            self::$sSessionId = Yii::$app->getRequest()->post('sessionId',false);
            $sPath = Yii::$app->getRequest()->post('path',false);
            if($sPath) {
                $data = ['data'=>[$sPath => Yii::$app->getRequest()->getBodyParams()]];
                Yii::$app->getRequest()->setBodyParams($data);
            }

            // если есть обязательные параметры - взвести флаг
            // если пришел файл, для правильной обработки JSON
            if ( $sPath and self::$sSessionId )
                self::$bIsCmsRequest = true;
        }

        return true;
    }// constructor

    /**
     * Возвращает значение строкового параметра по имени и пути меток до него
     * @param string $sName Имя параметра
     * @param string $sLabelPath Путь меток вызова до него
     * @param string $sDefaultValue Значение, возвращаемое по-умолчанию
     * @return string
     */
    static public function getStr($sName, $sLabelPath = '', $sDefaultValue = null){

        $request = Yii::$app->getRequest();

        $data = $request->getBodyParams();
        
        if ($sLabelPath && $sLabelPath !== '') {
            if (isset($data['data'][$sLabelPath][$sName])) {
                return $data['data'][$sLabelPath][$sName];
            }
        }

        if ($sName == 'data' && isset($data['data'][$sLabelPath])){
            return $sDefaultValue;
        }

        if (isset($data[$sName])) {
            return $data[$sName];
        }

        $data = $request->getQueryParams();
        if (isset($data[$sName])) {
            return $data[$sName];
        }

        return $sDefaultValue;
    }

    /**
     * Возвращает true, если запрос в админском интерфейсе
     * @static
     * @return bool
     */
    static public function isCmsRequest() {
        return (bool)self::$bIsCmsRequest;
    }

    /**
     * Отдает набор JSON заголовков
     * @return array
     */
    public static function getJsonHeaders() {
        return self::$aJsonHeaders;
    }

    /**
     * Возвращает идентификатор сессии дерева процессов.
     * @static
     * @return string
     */
    static public function getSessionId() {

        return self::$sSessionId;
    }// func

    /**
     * Возвращает разобранный JSON Package в случае его нахождения либо false.
     * @static
     * @return bool|array
     */
    static public function getJSONPackage() {
        return (isSet(self::$aParams['json']))? self::$aParams['json']: false;
    }// func

    /**
     * Устанавливает новое значение параметра в JSON Package
     * @static
     * @param string $sLabelPath Путь по меткам вызова
     * @param string $sName Название параметра
     * @param mixed $mValue Значение параметра
     * @param bool $bOverlay Флаг жесткой либо мягкой вставки (перекрывать или нет существующиее значение)
     * @return bool
     */
    static public function set($sLabelPath, $sName, $mValue, $bOverlay = true) {

        if($bOverlay OR is_null(self::getStr($sName,$sLabelPath))) {
            $data = Yii::$app->request->getBodyParams();
            $data['data'][$sLabelPath][$sName] = $mValue;
            Yii::$app->request->setBodyParams($data);
            self::$aParams['json'][$sLabelPath]['params'][$sName] = $mValue;
            return true;
        }

        return false;
    }// func

}// class
