<?php
require_once COREPATH.'libs/Twig/Autoloader.php';
/**
 * Класс-шаблонизатор Обертка для skTwig
 * @class skTwig
 * @project Skewer
 * @package kernel
 *
 * @Author ArmiT, $Author$
 * @version $Revision$
 * @date $Date$
 *
 * @source http://twig.kron0s.com/
 *
 * @dependig Twig_Autoloader
 *
 * @notice Обновлено до версии 1.13
 *
 */

class skTwig {
    
    /**
     * Экземпляр шаблонизатора
     * @var null|\skTwig $instance
     */
    private static $instance    = NULL;
    /**
     * Массив путей в директориям шаблонов
     * @var array
     */
    private static $aTplPath    = array();
    /**
     * Путь к директории хранения кеша шаблонизатора
     * @var string
     */
    private static $sCache      = '';
    /**
     * Экземпляр skTwig File system class
     * @var null|\Twig_Loader_Filesystem
     */
    private static $oLoader     = NULL;
    /**
     * Экземпляр skTwig Environment class
     * @var null|\Twig_Environment
     */
    private static $oEnv        = NULL;
    /**
     * Хранилище данных, вставляемых в шаблон
     * @var array
     */
    private static $aStack      = array();
    /**
     * Флаг режима отладки
     * @var bool
     */
    private static $bDebugMode  = false;

    /**
     * Список алиасов зарегистрированных пользовательских функций
     * @var array
     */
    protected static $userFunctions = array();

    /**
     * Список алиасов зарегистрированных пользовательских фильтров
     * @var array
     */
    protected static $userFilters= array();

    /** @var bool Скрывать копейки при форматировании цен */
    private static $bHidePriceFractional = false;

    /**
     * Инициализирует шаблонизатор, устанавливает начальные настройки.
     */
    private function __construct() {

        Twig_Autoloader::register();

        //if(!count(self::$aTplPath)) return false;

        $filter = new Twig_SimpleFilter('truncate', function ($text, $max = 500) {
            if (strlen($text) >= $max)
            {
                $text = substr($text, 0, $max);
                $lastSpace = strrpos($text,' ');
                $text = substr($text, 0, $lastSpace).'...';
            }
            return $text;
        });

        self::$oLoader = new Twig_Loader_Filesystem(self::$aTplPath);
        self::$oEnv = new Twig_Environment(self::$oLoader, array(
          'cache' => self::$sCache,
          'debug' => self::$bDebugMode,
          'autoescape' => false, 
        ));
        self::$oEnv->addFilter($filter);

        /**
         * @todo Перенести это отсюда, когда появится единая точка для инициализации
         */
        self::$bHidePriceFractional = \SysVar::get('catalog.hide_price_fractional');
        $filter = new Twig_SimpleFilter('price_format', function ($string) {
            return self::priceFormat($string);
        });

        self::$oEnv->addFilter($filter);

        $filter = new Twig_SimpleFilter('ampersandReplace', function ($string) {
            return preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $string);
        });
        self::$oEnv->addFilter($filter);

    }// constructor

    /**
     * Запрещаем вызов извне.
     */
    private function __clone() {

    }// clone

    /**
     * Создает / возвращает экземпляр шаблонизатора
     * @static
     * @param array $aTplPath Массив путей к директориям шаблонов
     * @param string $sCache Путь к директроии хранения кеша
     * @param bool $bDebugMode Флаг режима отладки
     * @return null|\skTwig /skTwig
     */
    public static function Load($aTplPath, $sCache = '', $bDebugMode = false) {

        if(isset(self::$instance) and (self::$instance instanceof self)) {

			return self::$instance;
		} else {
            self::$aTplPath     = $aTplPath;
            self::$sCache       = (String)$sCache;
            self::$bDebugMode   = (bool)$bDebugMode;
            self::$instance     = new self();

			return self::$instance;
		}
    }// func

    /**
     * Включает режим отладки для шаблонизатора. В случае, если вызов происходит без предварительной инициализации,
     * то метод возвращает false либо true в случае успешной установки режима
     * @static
     * @return bool
     */
    public static function enableDebug() {

        if(!(self::$oEnv instanceof Twig_Environment)) return false;
        self::$bDebugMode = true;
        self::$oEnv->enableDebug();
        self::$oEnv->enableAutoReload();

        return false;
    }// func

    /**
     * Выключает режим отладки для шаблонизатора. В случае, если вызов происходит без предварительной инициализации,
     * то метод возвращает false либо true в случае успешной установки режима
     * @static
     * @return bool
     */
    public static function disableDebug() {

        if(!(self::$oEnv instanceof Twig_Environment)) return false;
        self::$bDebugMode = false;
        self::$oEnv->disableDebug();
        self::$oEnv->disableAutoReload();

        return false;
    }// func

    /**
     * Возвращает режим работы шаблонизатора. Debug/Production
     * @static
     * @return bool
     */
    public static function isDebugMode() {

        if(!(self::$oEnv instanceof Twig_Environment)) return false;
        return self::$oEnv->isDebug();

    }// func

    /**
     * Присваивает имя данным, по которому они будут доступны в шаблоне
     * @static
     * @param string $sName
     * @param mixed $uValue
     * @return bool
     */
    public static function assign($sName, $uValue) {

        if(empty($sName)) return false;
        self::$aStack[$sName] = $uValue;

        return true;
    }// func

    /**
     * Устанавливает путь к директории шаблонов
     * @static
     * @param array $aPaths
     * @param bool $bForceWrite
     * @return bool
     */
    public static function setPath($aPaths = array(), $bForceWrite=true) {
        
        if(!(self::$oLoader instanceof Twig_Loader_Filesystem)) return false;
        self::$oLoader->setPaths($aPaths);
        
        return true;
    }// func

    /**
     * Возвращает код шаблона после обработки шаблонизатором
     * @static
     * @param string $sTplName Ссылка на шаблон в рамках ранее указанной среды
     * @return bool|string
     */
    public static function render($sTplName) {

        $oTpl = self::$oEnv->loadTemplate($sTplName);
        $sOut = $oTpl->render(self::$aStack);
        self::$aStack = array();
        return $sOut;
    }// func

    /**
     * Возвращает код шаблона после обработки шаблонизатором
     * @param string $sTplSource Код шаблона
     * @param array $aData
     * @return string
     */
    public static function renderSource( $sTplSource, $aData = array() ) {

        // todo оформить через внутреннее состояние и параметры
        $twig = new \Twig_Environment( new \Twig_Loader_String(), array(
            'cache' => self::$sCache,
            'debug' => self::$bDebugMode,
            'autoescape' => false,
        ) );

        $aParserHelpers = skParser::getParserHelpers();
        if(count($aParserHelpers))
            foreach($aParserHelpers as $sHelperName => $oHelperObject)
                $aData[$sHelperName] = $oHelperObject;

        $sOut = $twig->render( $sTplSource, $aData );

        return $sOut;
    }

    /**
     * Добавляет в пространство имен шаблона функцию с именем $alias и телом $function
     * @param $alias
     * @param callable $function
     * @return bool
     */
    public static function addFunction($alias , \Closure $function) {

        if(isSet(self::$userFunctions[$alias])) return false;
        self::$userFunctions[$alias] = $alias;

        $func = new Twig_SimpleFunction($alias, $function);
        self::$oEnv->addFunction($func);
        return true;
    }

    /**
     * Добавляет в пространство имен шаблона фильтр с именем $alias и телом $function
     * @param $alias
     * @param callable $function
     * @return bool
     */
    public static function addFilter($alias , \Closure $function) {

        if(isSet(self::$userFilters[$alias])) return false;
        self::$userFilters[$alias] = $alias;

        $filter = new Twig_SimpleFilter($alias, $function);
        self::$oEnv->addFilter($filter);
        return true;
    }

    /**
     * Форматирование цены
     * @param $string
     * @return int|string
     */
    public static function priceFormat($string)
    {
        if (!is_numeric($string)) {
            return $string;
        }
        return number_format((float)$string, (self::$bHidePriceFractional)?0:2, '.', ' ');
    }

}// class
