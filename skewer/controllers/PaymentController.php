<?php

namespace skewer\controllers;

use skewer\build\Tool\Payments;
use skewer\build\Adm\Order\model\Status;
use skewer\build\Adm\Order\Service;
use skewer\build\Component\Site\Type;



class PaymentController extends Prototype
{

    public function actionIndex()
    {
        if ( !Type::isShop() ){
            return 'FAIL';
        }

        //Получаем данные
        $oPayment = Payments\Api::make( $this->getType() );

        if (!$oPayment){
            return 'FAIL';
        }

        try{
            if ($oPayment->checkResult()){
                //Меняем статус заказа, рассылаем письма и пр.
                if (Service::changeStatus( $oPayment->getOrderId(), Status::getIdByPaid(), $oPayment->getSum() )){
                    $sResult = $oPayment->getSuccess();
                } else
                    $sResult = $oPayment->getFail();

                Service::sendMailChangeOrderStatus( $oPayment->getOrderId(), Status::getIdByNew(), Status::getIdByPaid() );
            }
            else {
                Service::changeStatus( $oPayment->getOrderId(), Status::getIdByFail(), $oPayment->getSum());
                Service::sendMailChangeOrderStatus( $oPayment->getOrderId(), Status::getIdByNew(), Status::getIdByFail() );
                $sResult = $oPayment->getFail();
            }
        }
        catch (\Exception $e){
            $sResult = 'FAIL';
        }

        return $sResult;

    }

    /**
     * Получение типа агрегатора по get параметрам
     * #stab_fix можно перевести на динамическое определение, когда каждый из установленных модулей
     *      сам запустит то, что надо и скажет если подходит
     * @return string
     */
    private function getType(){

        $sType = \skRequest::getStr('MNT_TYPE');

        if (!$sType){
            $sType = \skRequest::getStr('shp_type');
        }

        if (!$sType){
            $txn_id = \skRequest::getStr('txn_id');
            if ($txn_id){
                $sType = 'paypal';
            }
        }

        if (!$sType){
            if (\skRequest::getStr('action') == 'checkOrder' && \skRequest::getStr('md5'))
                $sType = 'yandexkassa';
        }

        return $sType;
    }

}
