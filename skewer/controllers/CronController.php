<?php

namespace skewer\controllers;

use skewer\build\Component\QueueManager\Api;
use skewer\build\Component\QueueManager\Manager;


class CronController extends Prototype
{
    public function actionIndex(){

        if(rand(1,10) < 3)
            Api::collectGarbage();

        // добавление задач из планировщика

        $iStartTime = time();
        $iLastStartTime = (int)\SysVar::get('SheduleLastStartTime');

        /**
         * @todo Пока не трогал, Пересмотреть это позже
         * не должно быть мапперов как минимум
         */
        $aScheduleItems = \skewer\build\Tool\Schedule\Mapper::getAllItems();

        $iCnt = 0;

        for($iCurTime = $iLastStartTime; $iCurTime < $iStartTime; $iCurTime+=60){

            if ( ++$iCnt > 100 )
                break;

            $sCurTime = date('i:H:d:m:w',$iCurTime);
            $aCurTime = explode(':',$sCurTime);

            foreach($aScheduleItems as $aCurTask){

                if( $aCurTask['status'] &&
                    (is_null($aCurTask['c_min']) || $aCurTask['c_min']==(int)$aCurTime[0]) &&
                    (is_null($aCurTask['c_hour']) || $aCurTask['c_hour']==(int)$aCurTime[1]) &&
                    (is_null($aCurTask['c_day']) || $aCurTask['c_day']==(int)$aCurTime[2]) &&
                    (is_null($aCurTask['c_month']) || $aCurTask['c_month']==(int)$aCurTime[3]) &&
                    (is_null($aCurTask['c_dow']) || $aCurTask['c_dow']==(int)$aCurTime[4])){

                    $aData = [
                        'title' => $aCurTask['title'],
                        'priority' => $aCurTask['priority'],
                        'resource_use' => $aCurTask['resource_use'],
                        'target_area' => $aCurTask['target_area']
                    ];

                    $command = json_decode($aCurTask['command'], true);
                    //Если указан метод - запустим универсальную задачу на выполнение одного метода
                    if (isset($command['method'])){
                        $aData['class'] = '\skewer\build\Component\QueueManager\MethodTask';
                        $aData['parameters'] = [
                            'parameters' => $command['parameters'],
                            'class' => $command['class'],
                            'method' => $command['method']
                        ];
                    }else{
                        $aData['class'] = $command['class'];
                        $aData['parameters'] = $command['parameters'];
                    }

                    Api::addTask(
                        $aData
                    );

                }

            }

        }

        // save last start time
        \SysVar::set('SheduleLastStartTime',$iStartTime+59); //защита от нескольких запусков в рамках одной минуты

        /**
         * Запуск менеджера
         */
        $oManager = Manager::getInstance();

        $oManager->execute();

        return 1;
    }

}
