<?php

namespace skewer\controllers;

use skewer\build\Component\Section;
use \skewer\build\Component\Section\Parameters;
use \yii\helpers\ArrayHelper;
use \skewer\build\Component\Section\Page;
use \skewer\build\Component\Section\Tree;
use \skewer\models\TreeSection;
use yii\web\ServerErrorHttpException;

/**
 * Контроллер лицавой части сайта
 */
class SiteController extends Prototype  {

    /**
     * Дополнителные действия
     * @return array
     */
    public function actions() {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }


    /**
     * проверим robots.txt
     */
    public function checkRobots(){

        if ($_SERVER['REQUEST_URI']=='/robots.txt'){

            $host = $_SERVER['HTTP_HOST'];

            $patterns = array();

            $patterns[] = '/^[\S]*sk[\d]{3}.ru$/';
            $patterns[] = '/^[\S]*sktest.ru$/';
            $patterns[] = '/^[\S]*twinslab.ru$/';


            $bDenyAll = false;

            foreach($patterns as $pattern){
                if (preg_match($pattern,$host)){
                    // запретим всё
                    $bDenyAll = true;
                }
            }

            $testDomain = ArrayHelper::getValue( \Yii::$app->params, 'test_domains' );

            if ($testDomain && is_array($testDomain)){
                if (array_search($host, $testDomain)!==false){
                    $bDenyAll = true;
                };
            }

            header("Content-Type: text/plain");

            // если домен тестовый или файла robots.txt нет в корне, то доступ запрещаем
            if ($bDenyAll || !file_exists(WEBPATH.'robots.txt')){
                exit("User-agent: *\nDisallow: /");
            } else {
                exit(file_get_contents(WEBPATH.'robots.txt'));
            }
        }
    }


    /**
     * Лицевая страница сайта
     * @return string
     * @throws ServerErrorHttpException
     * @throws \Exception
     */
    public function actionIndex() {

        $this->checkRobots();

        $this->addHelpers();

        /* Определяем стартовый раздел */
        $mainSection = \Auth::getMainSection();

        \Yii::$app->router->iPageId = \Yii::$app->router->getSection($mainSection);

        if(!\Yii::$app->router->iPageId)
            \Yii::$app->router->setPage(page404);

        // блок проверки на закрытие остраницы т индексации (возможно должен находиться не здесь)
        $page = TreeSection::findOne(\Yii::$app->router->iPageId);
        if ( $page ) {
            if ( $page->visible == Section\Visible::HIDDEN_NO_INDEX ) {
                \Yii::$app->getResponse()->redirect('/', '301')->send();
            }
        }

        /** @var $oPage \skProcess */
        $oPage = null;

        /** Сперва определяем язык, а затем нужные параметры */
        $this->initLang();

        $this->initParameters();

        \Yii::$app->on('reload_page_id', [$this, 'initParameters']);

        if (\Yii::$app->router->iPageId == \Yii::$app->sections->main() && \Yii::$app->router->getURLTail())
            \Yii::$app->router->setPage(page404);

        /** Редирект после языков! */
        $this->checkSiteRedirect();

        $iCnt = 0;

        do {

            // Проверяем доступ к разделу
            if ( !\CurrentUser::canRead(\Yii::$app->router->iPageId) )
                \Yii::$app->router->setPage(pageAuth);

            \Yii::$app->processList->removeProcess('out');

            $oPage = $this->initRootProcess();

            $iStatus = $this->executeRootProcess();

            if ( ++$iCnt>10 )
                throw new ServerErrorHttpException( 'Infinite loop in page id determination' );

        } while($iStatus == psExit);

        $oPage->render();

        return \Yii::$app->router->rewriteURLs($oPage->getOut());
    }


    /**
     * Инициализировать корневой процесс
     * @throws ServerErrorHttpException
     * @return \skProcess
     */
    private function initRootProcess() {

        $aParams = Page::getByGroup( Parameters::settings );

        $aParams = ArrayHelper::map($aParams, 'name', 'value');
        if ( isset($aParams['object']) and $aParams['object'] )
            $sClassName = $aParams['object'];
        else
            throw new ServerErrorHttpException( sprintf(
                'No root module found for section [%d]',
                \Yii::$app->router->iPageId
            ) );

        $aParams['_params'] = $aParams;
        $aParams['pageId'] = \Yii::$app->router->iPageId;

        $oProcess = \Yii::$app->processList->addProcess( new \skContext('out', $sClassName, ctModule, $aParams) );

        if ( !$oProcess )
            throw new ServerErrorHttpException(
                sprintf('Root process [%s] not inited for section [%d]'),
                $sClassName,
                \Yii::$app->router->iPageId
            );

        return $oProcess;
    }

    /**
     * Выполнить коневой процесс
     * @return bool|int
     */
    private function executeRootProcess() {

        $mainSection = \Yii::$app->sections->main();

        $iStatus = \Yii::$app->processList->executeProcessList();

        // если не главный раздел и url при этом не разобран до конца
        /** @noinspection PhpUndefinedMethodInspection */
        if ( \Yii::$app->router->iPageId !== $mainSection and !\Yii::$app->router->getUrlParsed() ) {

            // выдать 404
            \Yii::$app->router->setPage( page404 );
            $iStatus = psExit;
        }

        return $iStatus;

    }

    /**
     * Инициализация языков
     * @return void
     */
    protected function initLanguage(){
        /** Перекрываем родительский метод. Инициализация будет проведена позже в initLang */
    }

    /**
     * Инициализация языков
     */
    private function initLang(){
        $sLanguage = Parameters::getLanguage(\Yii::$app->router->iPageId);
        \Yii::$app->language = ($sLanguage)?:\Yii::$app->language;
    }

    /**
     * Инициализация параметров
     * @return void
     */
    public function initParameters(){
        Page::init(\Yii::$app->router->iPageId);
    }


    /**
     * Проверка на редиректы раздела
     */
    private function checkSiteRedirect(){

        $oSection = Tree::getSection( \Yii::$app->router->iPageId );

        if (in_array(\Yii::$app->router->iPageId, [\Yii::$app->sections->getValue(Page::LANG_ROOT), \Yii::$app->sections->getValue('main')]) && \Yii::$app->router->getURLTail() != '')
            return;

        if ( $oSection ) {
            if ( $oSection->link ) {
                if ( preg_match('/^\[\d+\]$/', $oSection->link) )
                    $sRedirectUrl = \Yii::$app->router->rewriteURL( $oSection->link );
                else
                    $sRedirectUrl = $oSection->link;
                if ( $sRedirectUrl !== $_SERVER['REQUEST_URI'] ) {
                    \Yii::$app->getResponse()->redirect($sRedirectUrl,'301')->send();
                }
            }
        }
    }

    /**
     * Добавление хелперов для парсера
     */
    private function addHelpers()
    {
        /* Добавялем класс Design для доступа в шаблонах */
        \skParser::setParserHelper(new \Design());
        \skParser::setParserHelper(new \Vars());
    }

}
