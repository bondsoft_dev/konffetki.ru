<?php

namespace skewer\controllers;


class GatewayController extends Prototype
{

    /**
     * Экземпляр сервера
     * @var null|\skGatewayServer
     */
    protected $oGatewayServer = null;

    /**
     *  Вызывается после прихода заголовка
     * @param \skGatewayServer $oServer
     */
    public function onHeaderLoad(&$oServer) {

        /* Здесь смотрим - если есть флаг того, что пакет зашифрован, то пытаемся по заголовкам
           определить, кто прислал пакет и получить ключ площадки */

        $oServer->setKey(APPKEY);

    }// func

    /**
     * Шлюз работает всегда
     */
    public function isAllowedStart(){
        return true;
    }// func

    public function actionIndex()
    {

        set_time_limit(0);

        \Yii::$app->params['debug']['parser'] = true;
        \Yii::$app->params['debug']['config'] = true;

        /* Режим отладки дляшаблонизатора */
        (\Yii::$app->params['debug']['parser'])? \skTwig::enableDebug(): \skTwig::disableDebug();

        $this->oGatewayServer = new \skGatewayServer(\skGatewayServer::StreamTypeEncrypt);

        $this->oGatewayServer->addParentClass('ServicePrototype');

        $oCrypt = new \skBlowfish();
        $oCrypt->setIv( \Yii::$app->params['security']['vector'] );

        $this->oGatewayServer->onLoadHeaderHandler(array($this, 'onHeaderLoad'));
        $this->oGatewayServer->onEncrypt(array($oCrypt, 'encrypt'));
        $this->oGatewayServer->onDecrypt(array($oCrypt, 'decrypt'));

        $this->oGatewayServer->handler();

    }

}
