<?php

namespace skewer\controllers;

use skewer\core\Component\Config as Config;
use skewer\build\Component\Installer as Installer;
use \skewer\build\Component\Site;

use yii\web\Controller;

/**
 * Прототип контроллеров сборки skewer
 * Выполняет первичную инициализацию
 */
abstract class Prototype extends Controller
{

    /**
     * Инициализация языков
     * @return void
     */
    protected function initLanguage(){
    }

    /**
     * Проверяет режим работы процессора
     */
    public function isAllowedStart(){
        $procEnable = \SysVar::get('ProcessorEnable');
        return $procEnable && $procEnable != '0';
    }

    /**
     * Занимается инициализацией окружения перед выполнением
     */
    public function init() {

        /* Проверяем режим работы - если процессоры выключены, говорим клиенту об этом и завершаем работу */
        if(!$this->isAllowedStart()) {
            $response = \Yii::$app->getResponse();
            $response->setStatusCode(503);
            $response->headers->add('Retry-After','3600');
            $response->content = \skParser::parseTwig(\Yii::$app->params['page']['503'], array());
            $response->send();
        }

        // инициализация файлов
        \skFiles::init(FILEPATH, PRIVATE_FILEPATH);

        // инициализация парсера
        \skParser::setParserHelper(new \LangHelper(), 'Lang');

        // инициализация языков
        $this->initLanguage();

        // обработка режима сброса кэша
        $this->execDebugMode();

        // инициализация событий
        \Yii::$app->register->initEvents();

        return true;

    }

    protected function execDebugMode()
    {
        /* Авторизованы в режиме дизайнера или под админом - включаем режим отладки */
        if (\CurrentAdmin::isLoggedIn())
        {
            if (\CacheUpdater::hasConfigFlag())
                \Yii::$app->params['debug']['config'] = true;

            if (\CacheUpdater::hasParserFlag())
                \Yii::$app->params['debug']['parser'] = true;

            if (\CacheUpdater::hasCssFlag())
                \Yii::$app->params['debug']['css'] = true;

            if (\CacheUpdater::hasAssetFlag())
                \Yii::$app->params['debug']['asset'] = true;

            \CacheUpdater::unsetUpdFlag();
        }

        // очистка asset файлов
        if (\Yii::$app->params['debug']['asset'])
        {
            \Yii::$app->clearAssets();
        }

        /* Режим отладки для шаблонизатора */
        if (\Yii::$app->params['debug']['parser'])
            \skTwig::enableDebug();
        else
            \skTwig::disableDebug();

        // Режим отладки для системы - переустановить все модули
        if (\Yii::$app->params['debug']['config'])
        {
            \Yii::$app->getI18N()->clearCache();
            \Yii::$app->register->actualizeAllModuleConfig();
        }

    }

}
