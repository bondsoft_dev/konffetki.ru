<?php

namespace skewer\controllers;

use Captcha;
use skRequest;

class AjaxController extends Prototype
{

    public function actionAjax()
    {

        // очистить набор дополнительных файлов
        \skLinker::clearCssFiles();
        \skLinker::clearJsFiles();

        $sCmd = skRequest::getStr('cmd',false);
        $sModule = skRequest::getStr('moduleName',false);

        if(!$sCmd OR !$sModule) return 'bad request';

        $oProcess = null;

        $sLanguage = skRequest::getStr('language', '', \Yii::$app->language);

        \Yii::$app->language = $sLanguage;

        // #stab_fix переделать проверку на классы
        $sModuleName = $sModule;
        if ( strpos( $sModuleName, 'Page') ) {
            // не неймспейсы, значит \ мы не ждем
            $sModuleName = str_replace('\\', '', $sModule . 'Module');
        }
        else {
            // иначе у нас неймспейсы
            $realClassPath = \Module::getClassOrExcept($sModuleName,false);
            $aPath = explode('\\',$realClassPath);
            // #stab_fix перенести в конфиги модулей

            if ($aPath && isset($aPath[2])){
                if ($aPath[2]!='Page') return false;

                // на будущее фильтр еще и для модулей, привет Илье, сейчас будет switch
                if (isset($aPath[3])){
                    switch ($aPath[3]){
                        // тут список пейджевых модулей разрешенных в ajax
                        // в каждом кейсе можно будет допилить еще и проверку на методы
                        case 'Cart':
                        case 'Subscribe':
                        case 'Forms':
                        case 'Poll':

                            break;
                        default:
                            // все остальные модули запрещены
                            return false;
                            break;
                    }
                } else return false;
            } else return false;
        }

        $oProcess = \Yii::$app->processList->addProcess(new \skContext( 'out', $sModuleName, ctModule, array()));
        \Yii::$app->processList->executeProcessList();

        $oProcess->render();
        $aOut = array(
            'html' => $oProcess->getOut(),
            'status' => $oProcess->getStatus(),
        );

        if(count($aData = $oProcess->getData()))
            $aOut['data'] = $aData;

        return json_encode($aOut);

    }

    public function actionCaptcha()
    {
        //$v = isSet($_GET['v']) ? $_GET['v'] : 0;
        $h = isSet($_GET['h']) ? $_GET['h'] : 'none';
        $oCaptcha = Captcha::getInstance();
        $oCaptcha->setFont( 18, '#000', '');//BUILDPATH.'common/fonts/palab.ttf'
        $oCaptcha->setSize( 90, 40 );
        $oCaptcha->show( $h );
    }

    public function actionUploader(){

        if ( !\CurrentAdmin::isLoggedIn() )
            return false;

        $sSelectMode = skRequest::getStr( 'selectMode' );

        switch( $sSelectMode ) {
            case 'designFileBrowser':
                $mSectionId = \Design::imageDirName;
                break;
            default:
                $mSectionId = skRequest::getStr('section');
                break;
        }
        if ( !$mSectionId )
            $mSectionId = \Yii::$app->sections->main();

        $aFiles = \skewer\build\Adm\Files\Api::uploadFiles( $mSectionId );
        \skLogger::dump( $aFiles );

        if ( !$aFiles['loaded'] )
            return json_encode( array(
                'file' => '',
                'success' => false,
                'message'=>nl2br(array_shift($aFiles['errors']))
            ) );;

        $sFileName = sprintf(
            '/files/%s/%s',
            $mSectionId,
            array_shift( $aFiles['files'] )
        );

        return json_encode( array(
            'file' => $sFileName,
            'success' => true
        ) );

    }

}
