<?php

namespace skewer\controllers;

use skewer\build\Tool\Backup;

class LocalController extends Prototype
{

    public function actionIndex()
    {
        // проверка на авторизацию
        if ( !\CurrentAdmin::isLoggedIn() ) {
            \Yii::$app->getResponse()->setStatusCode(401);

            // при отсутствии авторизации перегружает родительское
            //  окно, а текущее закрывает
            echo '<script type="text/javascript">
                if ( window.opener ) {
                    window.opener.location.reload();
                    window.close();
                }
            </script>not authorized';
        }

        // запрос контроллера
        if ( is_null($sController = \skRequest::getStr('ctrl')) )
            return 'controller not declared';

        /**
         * #stab_fix это нужно переделать на событийную модель
         */
        switch($sController){
            case Backup\Local::getLocalLabel():
                // добавление процесса
                \Yii::$app->processList->addProcess( new \skContext( 'out', Backup\Local::className(), ctModule ) );
                // выполнение процесса
                \Yii::$app->processList->executeProcessList();
                break;
            default:
                return 'Access denied';
                break;
        }

        return '';

    }

}
