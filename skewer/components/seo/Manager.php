<?php
/**
 * User: Александр
 * Date: 15.09.2015
 * Time: 14:01
 */

namespace skewer\components\seo;

use yii\base\Component;
use yii\base\Application;


/**
 * Class Manager
 * @property Manager $seo
 * @package skewer\components\seo
 */
class Manager extends Component {

    /**
     * @inheritDoc
     */
    public function init() {

        // ставим прослушку событие перестроения контента
        \Yii::$app->on('CHANGE_CONTENT',function($event){

            // сбрасываем событие, чтобы больше раза не отрабатывало
            \Yii::$app->off('CHANGE_CONTENT');

            // ставим событие на перестроение sitemap. выполнится в конце
            \Yii::$app->on( Application::EVENT_AFTER_REQUEST,
                ['skewer\build\Component\SEO\Service','updateSiteMap'] );

        });

    }

}