<?php

namespace app\skewer\console;

use \skewer\build\Adm\Order;
use skewer\build\Component\Catalog\GoodsRow;

/**
 * Класс для работы с товарами
 */
class GoodsController extends Prototype
{

    /**
     * Добавляет в зеденный раздел заданное количество рандомных товаров
     * @param int $section id разедеда
     * @param int $cnt количество добавляемых товаров
     * @param string $card имя карточки
     */
    public function actionGenerate( $section, $cnt, $card = 'dopolnitelnye_parametry' ){

        echo "\nДобавляем $cnt записей в раздел №$section\n\n";

        $sRand = rand(10000, 99999);

        for ($i=1; $i<=$cnt; $i++) {
            $row = GoodsRow::create( $card );
            $row->setData([
                'title' => sprintf('good %d/%d sect %d', $i, $sRand, $section) ,
                'price' => rand(10000, 99999),
                'article' => sprintf('art%d-%d', $i, $sRand)
            ]);
            $row->save();
            $row->setViewSection([$section]);

            echo '.';

            if ( !($i % 100) )
                echo " № $i\n";

        }

        echo "\n\nАвтоматическое добавление завершено\n";

    }

}
