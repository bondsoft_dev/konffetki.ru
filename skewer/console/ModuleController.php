<?php

namespace app\skewer\console;

use skewer\build\Component\Installer as Installer;
use yii\helpers\Console;

/**
 * Класс для работы с установленными модулями системы
 */
class ModuleController extends Prototype {

    public $defaultAction = 'list';

    /**
     * Возвращает список модулей
     * @param string $layer Page / Cms / ...
     * @param string $filter new / installed / all
     * @throws Installer\Exception
     */
    public function actionList( $layer='', $filter='' ) {

        // выбираем слой, если нужно
        if ( !$layer ) {
            // формируем список слоев
            $layerList = Installer\Api::getLayers();
            $layerList = array_combine($layerList, $layerList);
            $layerList[''] = 'Page';

            // выбираем слой
            $layer = $this->select('Выберите слой [Page по умолчанию]:', $layerList);
            if (!$layer)
                $layer = 'Page';
        }

        // выбираем тип фильтрации
        if ( !$filter ) {
            $filter = $this->select('Какие модули показать [all]:', [
                'new' => 'Не установленные',
                'installed' => 'Установленные',
                'all' => 'Все',
                '' => 'Все'
            ]);

            if (!$filter)
                $filter = 'all';
        }

        switch($filter) {

            case 'new':
                $filter = Installer\Api::N_INSTALLED;
                break;

            case 'installed':
                $filter = Installer\Api::INSTALLED;
                break;

            case 'all':
                $filter = Installer\Api::INSTALLED | Installer\Api::N_INSTALLED;
                break;

            default:
                $this->stderr("Неизвестный фильтр [$filter]");

        }

        $out = '';
        foreach(Installer\Api::getModules($layer, $filter) as $module) {

            $this->stdout($module->alreadyInstalled ? '[inst] ' : '       ');
            $this->stdout($module->moduleName, Console::UNDERLINE);

            $this->stdout(" - ".$module->moduleConfig->getDescription());

            $this->stdout("\r\n");

        }

        $this->stdout($out);
    }

    /**
     * Устанавливает модуль
     * @param string $moduleName имя модуля
     * @param string $layer имя слоя
     * @throws Installer\Exception
     */
    public function actionInstall( $moduleName, $layer ) {

        $api = new Installer\Api();
        $tree = $api->install($moduleName, $layer);

        $this->stdout("Installation result:\r\n");

        /** @var Installer\Module $item */
        foreach($tree as $item)
            $this->stdout($item->moduleName."\r\n", Console::UNDERLINE);

    }

    /**
     * Деинсталировать модуль
     * @param string $moduleName имя модуля
     * @param string $layer имя слоя
     * @throws Installer\Exception
     */
    public function actionUninstall( $moduleName, $layer ) {

        $api = new Installer\Api();
        $api->uninstall($moduleName, $layer);

        $this->stdout( sprintf(
            "Module %s successfully uninstalled\r\n",
            $this->ansiFormat($moduleName, Console::UNDERLINE)
        ) );

    }

    /**
     * Переустанавливает модуль
     * @param string $moduleName имя модуля
     * @param string $layer имя слоя
     * @throws Installer\Exception
     */
    public function actionReinstall( $moduleName, $layer ) {

        $this->actionUninstall($moduleName, $layer);
        $this->actionInstall($moduleName, $layer);

    }

    /**
     * Переустанавливает модуль
     * @param string $moduleName имя модуля
     * @param string $layer имя слоя
     * @throws Installer\Exception
     */
    public function actionUpdateModule( $moduleName, $layer ) {

        $api = new Installer\Api();
        $api->updateConfig($moduleName, $layer);

        $this->stdout( sprintf(
            "Config file for module %s successfully updated \r\n",
            $this->ansiFormat($moduleName, Console::UNDERLINE)
        ) );

    }


}