<?php

namespace app\skewer\console;


use skewer\build\Tool\Patches\Api;
use yii\helpers\Console;
use yii\helpers\FileHelper;

class PatchesController extends Prototype{


    /**
     * Список установленных патчей
     * @param int $iPage Начало выборки
     * @param int $iOnPage Количество выбираемых
     */
    public function actionInstalled( $iPage = 1, $iOnPage = 50 ){

        $aPatches = Api::getAppliedPatches($iPage, $iOnPage);

        if (!$aPatches)
            return;

        $this->stdout("Список установленных патчей:");

        foreach( $aPatches as $aPatch){

            $this->stdout("\r\n" . $aPatch['install_date']. ' ' . $aPatch['patch_uid'] . ' ' . $aPatch['description']);

        }
        $this->stdout("\r\n");

    }

    /**
     * Список патчей, доступных для установки
     */
    public function actionAvailable(){

        $aPatches = Api::getAvailablePatches(PATCHPATH);

        if ($aPatches)
            $this->stdout("Список доступных патчей:");

        foreach( $aPatches as $sPatch){

            $description = Api::getDescFormFile($sPatch);

            $sPatch = substr($sPatch, 0, strpos($sPatch, '/'));

            $this->stdout("\r\n"  . $sPatch . ' ' . $description);

        }
        $this->stdout("\r\n");

    }

    /**
     * Создание патча
     * @param int $uid Номер
     * @param string $description Описание
     */
    public function actionCreate( $uid = 0, $description = '' ){

        while (!$uid)
            $uid = $this->prompt('Введите номер патча:');

        $sPatchFile = PATCHPATH . $uid . DIRECTORY_SEPARATOR . $uid . '.php';

        if (file_exists($sPatchFile)) {
            $this->stderr('Патч с номером ' . $uid . ' уже присутствует на площадке!' . "\r\n");
            return;
        }

        while (!$description)
            $description = $this->prompt('Введите описание патча:');

        if (!FileHelper::createDirectory(PATCHPATH . $uid )){
            $this->stderr('Не удалось создать директорию!' . "\r\n");
            return;
        }

        if (file_put_contents($sPatchFile, $sOut = \Yii::$app->getView()->renderFile(BUILDPATH . 'common/templates/patches.php', ['description' => $description]))){
            $this->stdout('Патч ' . $uid . ' успешно создан!' . "\r\n");
        } else {
            $this->stderr('Не удалось создать файл патча!' . "\r\n");
            return;
        }

    }


    /**
     * Установка патча
     * @param $uid
     */
    public function actionInstall( $uid = 0 ){

        while (!$uid)
            $uid = $this->prompt('Введите номер патча:');

        $patch_file = $uid . DIRECTORY_SEPARATOR . $uid . '.php';

        if (!Api::checkPatch($uid . '.php')) {
            $this->stderr('Патч ' . $uid . ' уже установлен!' . "\r\n");
            return;
        }

        try {
            Api::installPatch($patch_file);
            $this->stdout('Патч установлен!');
        }
        catch (\Exception $e){
            $this->stderr("В ходе установки патча произошли ошибки: \n" . $this->ansiFormat($e->getMessage(), Console::FG_RED)  . "\r\n\r\n");
            $this->stderr((string)$e);
            $this->stderr("\r\n");
            return;
        }
    }


}