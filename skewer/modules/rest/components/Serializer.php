<?php

namespace skewer\modules\rest\components;

use Yii;
use yii\base\Arrayable;
use yii\base\Component;
use yii\base\Model;
use yii\data\DataProviderInterface;
use yii\data\Pagination;
use yii\helpers\ArrayHelper;
use yii\web\Link;
use yii\web\Request;
use yii\web\Response;



class Serializer extends \yii\rest\Serializer {


    public function serialize($data)
    {
        if ($data instanceof Model && $data->hasErrors()) {
            return $this->serializeModelErrors($data);
        } elseif ($data instanceof Arrayable) {
            return $this->serializeModel($data);
        } elseif ($data instanceof DataProviderInterface) {
            return $this->serializeDataProvider($data);
        } else {

            if ( isSet($data['0'][$this->totalCountHeader]) ) {
                $hdr = $this->response->getHeaders();
                if ( isSet( $data['0'][$this->totalCountHeader] ) )
                    $hdr->set($this->totalCountHeader, $data['0'][$this->totalCountHeader]);
                if ( isSet( $data['0'][$this->pageCountHeader] ) )
                    $hdr->set($this->pageCountHeader, $data['0'][$this->pageCountHeader]);
                if ( isSet( $data['0'][$this->currentPageHeader] ) )
                    $hdr->set($this->currentPageHeader, $data['0'][$this->currentPageHeader]);
                if ( isSet( $data['0'][$this->perPageHeader] ) )
                    $hdr->set($this->perPageHeader, $data['0'][$this->perPageHeader]);
                array_shift( $data );
            }

            return $data;
        }
    }

}