<?php

namespace skewer\modules\rest\controllers;

use skewer\modules\rest\models\TreeSection;
use yii\rest\Serializer;
use yii\web\NotFoundHttpException;

class SectionController extends \yii\rest\Controller {

    public $serializer = [
        'class' => 'yii\rest\Serializer',
        'totalCountHeader' => 'X_Pagination_Total_Count',
        'pageCountHeader' => 'X_Pagination_Page_Count',
        'currentPageHeader' => 'X_Pagination_Current_Page',
        'perPageHeader' => 'X_Pagination_Per_Page'
    ];

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET']
        ];
    }

    public function actionView($id){

        $section = TreeSection::find($id)->with('children')->one();
        if (!$section) throw new NotFoundHttpException("not found");

        return $section;
    }


    public function actionIndex(){
        return TreeSection::publicSearch(\Yii::$app->request->get());
    }


}