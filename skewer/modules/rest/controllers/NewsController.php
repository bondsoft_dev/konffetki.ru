<?php

namespace skewer\modules\rest\controllers;

use skewer\models\News;
use yii\web\NotFoundHttpException;


class NewsController extends \yii\rest\Controller {

    public $serializer = [
        'class' => 'skewer\modules\rest\components\Serializer',
        'totalCountHeader' => 'X_Pagination_Total_Count',
        'pageCountHeader' => 'X_Pagination_Page_Count',
        'currentPageHeader' => 'X_Pagination_Current_Page',
        'perPageHeader' => 'X_Pagination_Per_Page'
    ];

    protected $fields = [
        'title' => 'title',
        'publication_date' => 'date',
        'alias' => 'news_alias',
        'section' => 'parent_section',
        'id' => 'id',
    ];

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET']
        ];
    }

    public function actionView( $id ) {

        $news = News::findOne( ['id' => $id] );

        if ( !$news ) throw new NotFoundHttpException("not found");

        return [
            'id' => $news->id,
            'title' => $news->title,
            'alias' => $news->news_alias,
            'section' => $news->parent_section,
            'announce' => $news->announce,
            'full_text' => $news->full_text,
            'date' => $news->publication_date,
        ];
    }


    public function actionIndex() {

        $sSortField = \Yii::$app->getRequest()->getQueryParam( 'sort' );

        $iPage = \Yii::$app->getRequest()->getQueryParam( 'page', 1 );
        $iOnPage = \Yii::$app->getRequest()->getQueryParam( 'per-page', 20 );

        $iSection = (int)\Yii::$app->getRequest()->getQueryParam( 'section', 0 );

        if ( $iPage < 1 ) $iPage = 1;

        $sSortWay = 'up';
        if ( strpos( $sSortField, '-' ) === 0 ) {
            $sSortWay = 'down';
            $sSortField = substr( $sSortField, 1 );
        }

        $query = News::find()->where( ['active' => 1] );

        if ( $iSection )
            $query->andWhere( ['parent_section' => $iSection] );

        if ( isSet( $this->fields[$sSortField] ) )
            $sSortField = $this->fields[$sSortField];
        else
            $sSortField = 'publication_date';

        $query->orderBy( [$sSortField=>($sSortWay == 'down') ? SORT_DESC : SORT_ASC ]);
        $query->limit( $iOnPage )->offset( ($iPage - 1) * $iOnPage );

        $list = [];

        /** @var News $news */
        foreach ( $query->each() as $news ) {

            $list[] = [
                'id' => $news->id,
                'title' => $news->title,
                'alias' => $news->news_alias,
                'section' => $news->parent_section,
                'announce' => $news->announce,
                'date' => $news->publication_date,
            ];
        }

        $count = $query->count();
        array_unshift( $list, [
            'X_Pagination_Total_Count' => $count,
            'X_Pagination_Page_Count' => ceil( $count / $iOnPage ),
            'X_Pagination_Current_Page' => $iPage,
            'X_Pagination_Per_Page' => $iOnPage
        ]);

        return $list;
    }

}