<?php

namespace skewer\modules\rest\controllers;

use skewer\build\Component\Catalog;
use yii\helpers\ArrayHelper;
use yii\web\NotFoundHttpException;


class CatalogController extends \yii\rest\Controller {

    public $serializer = [
        'class' => 'skewer\modules\rest\components\Serializer',
        'totalCountHeader' => 'X_Pagination_Total_Count',
        'pageCountHeader' => 'X_Pagination_Page_Count',
        'currentPageHeader' => 'X_Pagination_Current_Page',
        'perPageHeader' => 'X_Pagination_Per_Page'
    ];

    /**
     * @inheritdoc
     */
    protected function verbs()
    {
        return [
            'index' => ['GET', 'HEAD'],
            'view' => ['GET']
        ];
    }

    public function actionView( $id ) {

        $goods = Catalog\GoodsSelector::get( $id, 'base_card' );

        if ( !$goods ) throw new NotFoundHttpException("not found");

        return [
            'id' => $goods['id'],
            'title' => $goods['title'],
            'article' => ArrayHelper::getValue( $goods, 'fields.article.html', '' ),
            'section' => $goods['main_secton'],
            'price' => ArrayHelper::getValue( $goods, 'fields.price.value', '' ),
            'currency' => ArrayHelper::getValue( $goods, 'fields.price.attrs.measure', '' ),
            'description' => ArrayHelper::getValue( $goods, 'fields.description.html', '' ),
            'gallery' => ArrayHelper::getValue( $goods, 'fields.gallery.gallery.images', [] ),
        ];
    }


    public function actionIndex() {

        $sSortField = \Yii::$app->getRequest()->getQueryParam( 'sort' );

        $iPage = \Yii::$app->getRequest()->getQueryParam( 'page', 1 );
        $iOnPage = \Yii::$app->getRequest()->getQueryParam( 'per-page', 20 );

        $iSection = \Yii::$app->getRequest()->getQueryParam( 'section', 0 );
        $sTitleFilter = \Yii::$app->getRequest()->getQueryParam( 'title', '' );

        if ( $iPage < 1 ) $iPage = 1;

        $sSortWay = 'up';
        if ( strpos( $sSortField, '-' ) === 0 ) {
            $sSortWay = 'down';
            $sSortField = substr( $sSortField, 1 );
        }

        if ( $iSection )
            $query = Catalog\GoodsSelector::getList4Section( $iSection );
        else
            $query = Catalog\GoodsSelector::getList( 'base_card' );

        $query->condition( 'active', true );

        if ( $sTitleFilter )
            $query->condition( 'title LIKE ?', '%' . $sTitleFilter . '%' );

        if ( in_array( $sSortField, ['price','title'] ) ) {
            $query->sort( $sSortField, ($sSortWay == 'down') ? 'DESC' : 'ASC' );
        }

        $count = 0;
        $query->limit( $iOnPage, $iPage, $count );

        $list = [];

        foreach ( $query->parse() as $item ) {

            $list[] = [
                'id' => $item['id'],
                'title' => $item['title'],
                'article' => ArrayHelper::getValue( $item, 'fields.article.html', '' ),
                'price' => ArrayHelper::getValue( $item, 'fields.price.value', '' ),
                'currency' => ArrayHelper::getValue( $item, 'fields.price.attrs.measure', '' ),
                'gallery' => ArrayHelper::getValue( $item, 'fields.gallery.gallery.images.0.images_data', '' ),
            ];
        }

        array_unshift( $list, [
            'X_Pagination_Total_Count' => $count,
            'X_Pagination_Page_Count' => ceil( $count / $iOnPage ),
            'X_Pagination_Current_Page' => $iPage,
            'X_Pagination_Per_Page' => $iOnPage
        ]);

        return $list;
    }


}