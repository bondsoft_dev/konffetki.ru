<?php

namespace skewer\modules\rest;

class Module extends \yii\base\Module
{
    public $controllerNamespace = 'skewer\modules\rest\controllers';

    public function init()
    {
        parent::init();
        \Yii::$app->user->enableSession = false;

    }
}
