<?php

namespace skewer\modules\rest\models;

use yii\data\ActiveDataProvider;

/**
 * @property TreeSection $children
 */
class TreeSection  extends \skewer\models\TreeSection{

    public $text;

    public function fields(){
        return [
            'id',
            'title',
            'text',
            'alias',
            'visible',
            'position',
            'alias_path',
            'last_modified_date',
            'children'
        ];
    }

    /**
     * @param array $filter
     * @return ActiveDataProvider
     */
    public static function publicSearch($filter){

        $aDenySections = \Auth::getDenySections('public');
        $query = TreeSection::find()
            ->select('tree_section.*,parameters.show_val as text')
            ->leftJoin('parameters', '`parameters`.`parent` = `tree_section`.`id` AND `parameters`.`name` = "staticContent"')

            //->leftJoin('parameters',['parameters.parent = tree_section.id'],['parameters.name = "staticContent"'])
            ->andFilterWhere(['NOT IN','tree_section.id',$aDenySections])
        ;

        if (isset($filter['id'])){
            $query->andFilterWhere(['tree_section.id'=>(int)$filter['id']]);
        }

        if (isset($filter['parent'])){
            $query->andFilterWhere(['tree_section.parent'=>(int)$filter['parent']]);
        }

        return $dataProvider = new ActiveDataProvider([
                'query' => $query
            ]
        );
    }

    public function getChildren(){
        $aDenySections = \Auth::getDenySections('public');
        return $this->hasMany(\skewer\models\TreeSection::className(),['parent'=>'id'])->andWhere(['NOT IN','tree_section.id',$aDenySections]);
    }


}