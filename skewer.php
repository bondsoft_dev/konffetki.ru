<?php
/**
 * Created by PhpStorm.
 * User: Артем
 * Date: 13.12.13
 * Time: 16:56
 */

ini_set('display_errors', 0);
session_start();
define('VERSION', '4');       # версия теста


define('HOME_DOMAIN', 'http://book.web-canape.ru//ect/');       # домашний адрес теста
define('VERSION_FILE', 'checkversion.php');       # адрес для возврата текущей версии

define('MIN_PHPVERSION','5.3.1');      # минимально допустимая версия PHP
define('OPT_PHPVERSION','5.4');         # рекомендуемая версия PHP
define('MIN_EXEC_TIME',30);             # минимальное время работы скрипта
define('MIN_FILE_ULOADS',10);           # минимально допустимое кол-во одновременно загружаемых файлов
define('MIN_MEMORY_LIMIT', '30M');      # минимально допустимое кол-во памяти для работы
define('MIN_POST_SIZE', '30M');         # минимально допустимое кол-во памяти POST
define('MAX_UPLOAD_FILESIZE', '30M');   # максимальный размер файла через POST
define('SERVER_SOFTWARE', 'apache');    # веб сервер
define('MIN_AVAILABLE_SPACE', '2G');    # минимально допустимое дисковое пространство
define('MIN_MYSQL_MAJOR_VER', '5');     # минимально мажорная версия mysql



# требуемые расширения php с уровнем критичиности
$reqExtensions = array(
    'curl' => 'crit',
    'date' => 'warn',
    //'fileinfo' => 'crit',
    'filter' => 'crit',
    'gd' => 'crit',
    'hash' => 'crit',
    'json' => 'crit',
    'mbstring' => 'crit',
    'pdo' => 'crit',
    'pcre' => 'crit',
    'reflection' => 'crit',
    'soap' => 'warn',
    'spl' => 'crit',

);

$mainTest = array(

    array(
        'test'   => 'php',
        'title'  => 'Версия PHP',
        'value'  => 'PHP 5.4+',
        'method' => phpv(),
    ),

    array(
        'test'   => 'server',
        'title'  => 'Версия web-сервера',
        'value'  => 'Apache 2+',
        'method' => null,
    ),
    
    array(
        'test'   => 'db',
        'title'  => 'Версия сервера баз данных',
        'value'  => 'MySQL 5+',
        'method' => null,
    ),

);


# требуемые расширения apache с уровнем критичности
$reqApacheModules = array(
    'mod_php5' => 'crit',
    'mod_rewrite' => 'crit',
);

/**
 * msgs array
 */
$l = array(
    'mquotes'                   => 'magic_quotes_gpc включена',
    'phpv_opt'                  => 'Версия PHP минимально допустимая [%s]. Корректная работа гарантируется на сборках до 23 версии',
    'phpv_min'                  => 'Недопустимая версия PHP. Текущая: %s, требуемая: %s',
    'urlfopen'                  => 'Открытие файлов по URL недоступно. Директива allow_url_fopen отключена',
    'disclass'                  => 'Некоторые из классов заблокированы [%s]',
    'show_errors'               => 'Включен вывод ошибок',
    'disshell'                  => 'Некоторые из функций заблокированы! Установка в автоматическом режиме недоступна',
    'disbase64'                 => 'Функции для работы с base64 заблокированы',
    'disomefunc'                => 'Некоторые из функций заблокированы [%s]',
    'fileuploads'               => 'Опция file_uploads отключена',
    'openbasedir'               => 'Возможны проблемы с доступом к файлам. open_basedir=%s',
    'registerglob'              => 'register_globals включен',
    'safe_mode_on'              => 'safe_mode включен',
    'max_exec_time'             => 'max_execution_time меньше минимально допустимого. Текущее: %s, требуемое: %s',
    'max_fileupload'            => 'max_file_uploads меньше минимально допустимого [%s]. Возможны ограничения в работе multiload',
    'memory_limit'              => 'memory_limit меньше минимально допустимого [доступно %s, требуется %s]',
    'post_max_size'             => 'post_max_size меньше минимально допустимого [доступно %s, требуется %s]',
    'upload_filesize'           => 'upload_max_filesize меньше минимально допустимого [доступно %s, требуется %s]',
    'extension_nloaded'         => 'Расширение %s не доступно',
    'server_software'           => 'Не найдена информация о Web-сервере',
    'apache_notfound'           => 'Apache не используется в качестве web-сервера',
    'apache_mod_disable'        => 'Модуль Apache %s не доступен',
    'apache_mod_unknown'        => 'Информация о модулях web-сервера не доступна',
    'docroot_notfound'          => 'Данные о DOCUMENT_ROOT не найдены',
    'disk_space_unknown'        => 'Размер доступного дискового пространства не известен',
    'min_avail_space'           => 'Минимально допустимое дисковое пространство равно %s Доступно %s',
    'writer_openerror'          => 'Невозможно создать файл в корневой директории. Автоматическая установка невозможна.',
    'writer_unable'             => 'Запись в файлы невозможна. Возможно, требуется установка корректных прав для файлов',
    'session_unable'            => 'Работа с сессиями недоступна',
    'session_disabled'          => 'Механизм работы с сессиями отключен',
    'session_none'              => 'Механизм работы с сессиями включен, но сессия не создана',
    'session_dir_notwritable'   => 'Директория хранения сессий PHP не доступна для записи',
);

$letterPattern = "[%s] - %s \r\n";

/***** system *****/

$res      = '';
$info     = array();
$warning  = array();
$critical = array();

function Y() {
    global $l;
    global $info;
    global $warning;
    global $critical;

    if(!func_num_args())  return;

    $text = (isset($l[func_get_arg(1)]))? $l[func_get_arg(1)]: false;

    if(func_num_args()<3) return ${func_get_arg(0)}[] = $text;


    $vals = func_get_args();
    unSet($vals[0]);
    unSet($vals[1]);

    return ${func_get_arg(0)}[] = vsprintf($text, $vals);
}

function inf() {
    return call_user_func_array('Y', array_merge(array('info'), func_get_args()));

}

function warn() {
    return call_user_func_array('Y', array_merge(array('warning'), func_get_args()));

}

function crit() {
    return call_user_func_array('Y', array_merge(array('critical'), func_get_args()));
}

function bool($value) {
    if(empty($value) OR $value == '0') return false;
    return true;
}

function bytes($value) {
    $value = trim($value);
    $last = strtolower($value[strlen($value)-1]);
    switch($last) {

        case 'g':
            $value *= 1024;
        case 'm':
            $value *= 1024;
        case 'k':
            $value *= 1024;
    }
    return $value;
}

function toPrefix($input) {

    $si_prefix = array( 'B', 'K', 'M', 'G', 'T', 'E', 'Z', 'Y' );
    $base = 1024;
    $class = min((int)log($input , $base) , count($si_prefix) - 1);

    return $available = sprintf('%1.2f' , $input / pow($base,$class)).$si_prefix[$class];
}

function getParam($name, $default = '') {

    $request = file_get_contents('php://input');
    $request = json_decode($request, true);

    $method = $_SERVER['REQUEST_METHOD'];
    if ($method == 'POST' && isSet($_SERVER['HTTP_X_HTTP_METHOD']))
        if ($_SERVER['HTTP_X_HTTP_METHOD'] == 'DELETE')
            $method = 'DELETE';
        elseif ($_SERVER['HTTP_X_HTTP_METHOD'] == 'PUT')
            $method = 'PUT';
        else
            die('Unexpected Header');


        if(isSet($_GET[$name]))
            return addslashes($_GET[$name]);

        if(isSet($_POST[$name]))
            return addslashes($_POST[$name]);

        if(isSet($request[$name]))
            return addslashes($request[$name]);

        return $default;

}

function sendResponse($data) {

    echo json_encode($data);
}

/***** tests *****/

function phpv() {

    if (version_compare(PHP_VERSION, MIN_PHPVERSION, '>=') &&
        version_compare(PHP_VERSION, OPT_PHPVERSION, '<=')) {

        warn('phpv_opt', PHP_VERSION);
        return false;
    }

    if (!version_compare(PHP_VERSION, OPT_PHPVERSION, '>=')) {
        crit('phpv_min',PHP_VERSION, MIN_PHPVERSION);
        return false;   
    }
    
    return true;
}

function allowUrlFopen() {

    $val = bool(ini_get('allow_url_fopen'));
    if(!$val)
        crit('urlfopen');
    return $val;
}

function disableClasses() {

    $val = ini_get('disable_classes');
    if(!bool($val)) return;

    warn('disclass', $val);

    return;
}

function disableFunctions() {

    $val = ini_get('disable_functions');

    if(!bool($val)) return;

    $val = explode(',',$val);

    if(in_array('exec', $val) OR in_array('shell_exec', $val)) {
        warn('disshell');
        return;
    }

    if(in_array('base64_encode', $val) OR in_array('base64_decode', $val)) {
        crit('disbase64');
        return;
    }

    warn('disomefunc', implode(',',$val));
    return;
}

function displayErrors() {

    if(bool(ini_get('display_errors')))
        inf('show_errors');
}

function fUploads() {

    if(!bool(ini_get('file_uploads')))
        crit('fileuploads');
}

function mQuotes() {

    if(bool(ini_get('magic_quotes_gpc')))
        inf('mquotes');
}

function openBaseDir() {

    $val = ini_get('open_basedir');

    if(!bool($val)) return;

    warn('openbasedir');

    return;
}

function registerGlobals() {

    $val = ini_get('register_globals');

    if(!bool($val)) return;

    inf('registerglob');

    return;
}

function safeMode() {

    $val = ini_get('safe_mode');

    if(!bool($val)) return;

    warn('safe_mode_on');

    return;
}

function maxExecutionTime() {

    $val = ini_get('max_execution_time');
    if($val != '0')
        if((int)$val < MIN_EXEC_TIME)
            crit('max_exec_time', $val, MIN_EXEC_TIME);
}

function maxFileUploads() {

    $val = ini_get('max_file_uploads');
    if((int)$val < MIN_FILE_ULOADS)
        warn('max_fileupload', (int)$val);

}

function memoryLimit() {

    $val = ini_get('memory_limit');

    if(bytes($val) < bytes(MIN_MEMORY_LIMIT))
        crit('memory_limit', $val, MIN_MEMORY_LIMIT);
}

function postMaxSize() {

    $val = ini_get('post_max_size');

    if(bytes($val) < bytes(MIN_POST_SIZE))
        warn('post_max_size', $val, MIN_POST_SIZE);
}

function uploadMaxFilesize() {

    $val = ini_get('upload_max_filesize');

    if(bytes($val) < bytes(MAX_UPLOAD_FILESIZE))
        warn('upload_filesize', $val, MAX_UPLOAD_FILESIZE);
}

function extLoaded() {
    global $reqExtensions;

    foreach($reqExtensions as $ext => $level)
        if(!extension_loaded(strtolower($ext)))
            $level('extension_nloaded', $ext);

}

function buff($out) {
    global $res;
    $res = $out;
}

function serverSoftware() {
    global $reqApacheModules;
    global $res;

    if(!isSet($_SERVER['SERVER_SOFTWARE'])) {
        warn('server_software');
        return;
    }

    if(strpos(strtolower($_SERVER['SERVER_SOFTWARE']), strtolower(SERVER_SOFTWARE)) === false) {
        crit('apache_notfound');
        return;
    }


    if(function_exists('apache_get_modules')) {

        $modules = apache_get_modules();

    } else {
        
        ob_start("buff");
        phpinfo(INFO_MODULES);
        ob_end_flush();

        preg_match_all('/Loaded Modules \<\\/td\>\<td class\=\"v\"\>(?<modules>[-_a-z0-9\s]+)+\<\\/td\>/i', $res, $entry, PREG_PATTERN_ORDER);

        $modules = array();
        
        if(count($entry) && isSet($entry['modules'][0])) {
            $modules = $entry['modules'][0];
            $modules = explode(' ', trim($modules));
        }
    }
    
    
        if(empty($modules)) {
            warn('apache_mod_unknown');
            return;
        }
        
            foreach($reqApacheModules as $module => $level) {
    
                if(!in_array($module, $modules))
                    $level('apache_mod_disable', $module);
            }

    return;
}

function getSpace() {

    function achtung($errno, $errstr, $errfile, $errline) {
        
        warn('disk_space_unknown');
    }

    if(!isset($_SERVER['DOCUMENT_ROOT']))
        crit('docroot_notfound');
    
    set_error_handler("achtung", E_ALL);
    
    $available = disk_free_space($_SERVER['DOCUMENT_ROOT']);
    
    if(!$available) return;
    toPrefix($available);

    if(bytes(MIN_AVAILABLE_SPACE) > bytes(toPrefix($available)))
        crit('min_avail_space', MIN_AVAILABLE_SPACE, toPrefix($available));

    restore_error_handler();

}

function writeEnable() {
    $filename = dirname(__FILE__).'/'.rand(1,1000).'.tmp';
    $f = @fopen($filename, 'w+');

    if(!$f) return crit('writer_openerror');

    if(!fwrite($f, md5(rand(0, 1000)),32)) {
        //unlink($filename);
        return crit('writer_unable');

    }

    fclose($f);
    return unlink($filename);

}

function sessionTest() {

    $sessSavePath = session_save_path();
    
    /* Cut possible directive that determines the number of directory levels for
    your session files will be spread around in */
    if(strpos($sessSavePath, ';') !== false)
        $sessSavePath = substr($sessSavePath, strpos($sessSavePath, ';')+1);    
    
    if(!is_writeable($sessSavePath))
        crit('session_dir_notwritable');

}

function commonTest() {
    global $info;
    global $warning;
    global $critical;

    $data = array();

    phpv();
    allowUrlFopen();
    disableClasses();
    disableFunctions();
    displayErrors();
    fUploads();
    mQuotes();
    openBaseDir();
    registerGlobals();
    safeMode();
    maxExecutionTime();
    maxFileUploads();
    memoryLimit();
    postMaxSize();
    uploadMaxFilesize();
    extLoaded();
    serverSoftware();
    getSpace();
    writeEnable();
    sessionTest();

    if(count($info))
        foreach($info as $item)
            $data[] = array(
                'level' => 'info',
                'text'  => $item
            );

    if(count($warning))
        foreach($warning as $item)
            $data[] = array(
                'level' => 'warning',
                'text'  => $item
            );

    if(count($critical))
        foreach($critical as $item)
            $data[] = array(
                'level' => 'danger',
                'text'  => $item
            );

    return $data;
}

function checkMailAddress($email) {

    $pattern = '/^[_a-z0-9-]+(\.[_a-z0-9-]+)*@[a-z0-9-]+(\.[a-z0-9-]+)*(\.[a-z]{2,3})$/';
    return preg_match($pattern, $email)? true: false;

}

function checkMail($emailTo) {

    global $letterPattern;


    $mailFrom = 'skewer-test@'.str_replace(array('http://', 'www'), '', $_SERVER['HTTP_HOST']);

    $subject = "Skewer - Тестирование хостингов";

    $message = '<div>Потенциальных проблем не обнаружено</div>';

    $headers = "From: $mailFrom \r\n" .
        "Reply-To: $mailFrom \r\n" .
        'X-Mailer: PHP/' . phpversion();

    return mail($emailTo, $subject, $message, $headers);
}

function checkDB($host, $user, $pass, $dbname) {


    try{
        $DBH = new PDO("mysql:host=$host;dbname=$dbname", $user, $pass);
        $DBH->setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );  
    }
    catch (PDOException $e){

        $result['status'] = 'danger';
        $result['text'] = 'Соединение не установлено';
        return $result;
    }

    $result['status'] = 'success';
    $result['text'] = 'Соединение упешно установлено.';
    return $result;
}


/***** views *****/

function init() {
    global $info;
    global $warning;
    global $critical;


    ?>
    <!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN">
    <html>
    <head>
        <meta http-equiv="content-type" content="text/html; charset=utf-8">
        <link rel="stylesheet" href="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/css/bootstrap.min.css">
        <link rel="stylesheet" href="<?= HOME_DOMAIN ?>css/style.css">

        <!-- script src="//code.jquery.com/jquery-1.10.2.min.map"></script -->
        <script src="http://code.jquery.com/jquery-1.10.2.min.js"></script>
        <script src="http://netdna.bootstrapcdn.com/bootstrap/3.0.3/js/bootstrap.min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/underscore.js/1.5.2/underscore-min.js"></script>
        <script src="http://cdnjs.cloudflare.com/ajax/libs/backbone.js/1.1.0/backbone-min.js"></script>

        <script type="text/template" id="tpl_TestItem">
            <div class="alert alert-{%- level %}">{%- text %}</div>
        </script>
        <script>
            _.templateSettings = {interpolate : /\{\{(.+?)\}\}/g,      // print value: {{ value_name }}
                      evaluate    : /\{%([\s\S]+?)%\}/g,   // excute code: {% code_to_execute %}
                      escape      : /\{%-([\s\S]+?)%\}/g};
        </script>

        <style>
            td.option-min {
                background-color: #fcf8e3;
            }
            td.option-error {
                background-color: #f2dede;
            }
        </style>
        
        <script type="text/template" id="tpl_commontest_legend">
            <div class="well">

                <table class="table" style="width: 35%;">
                    <tr>
                        <td><div class="alert alert-info" style="width: 30px; height: 30px;"></div></td>
                        <td><small>Принять к сведению. На работоспособность сайта не влияет</small></td>
                    </tr>
                    <tr>
                        <td><div class="alert alert-warning" style="width: 30px; height: 30px;"></div></td>
                        <td><small>Возможны проблемы. Обратитесь к разработчику за дополнительными пояснениями</small></td>
                    </tr>
                    <tr>
                        <td><div class="alert alert-danger" style="width: 30px; height: 30px;"></div></td>
                        <td><small>Недопустимый параметр конфигурации</small></td>
                    </tr>
                </table>

            </div>
        </script>

        <script type="text/template" id="tpl_phpinfo">
            <iframe id="iframe" src="{%- url %}" style="width: 100%; height: 85%;"></iframe>
        </script>

        <script type="text/template" id="tpl_reqparams">

                    <div class="well">

                            <p>Ниже указаны основные требования к набору программного обеспечения и его версий для корректной
                                работы сайта</p>
                            <p>Для проведения детального тестирования перейдите на вкладку <a href="#!/commontest">"Общий тест"</a></p>
                    </div>
                    <table class="table" style="width: 35%;">
                        
                        {% _.each(items, function(item){ %}
                        <tr>
                            <td>{%- item.title %}</td>
                            <td class="option-{%- item.observed %}">{%- item.value %}</td>
                        </tr>
                        {% }); %}
                    </table>

        </script>

        <script type="text/template" id="tpl_mailmodal">

            <!-- Modal mail checker -->
            <form class="modalForm" id="mailChecker_form">
                <div class="modal fade" id="mailChecker" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel"></h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group has-feedback">
                                    <label class="control-label" for="email">Введите email адрес для проверки работы почтового сервера</label>
                                    <input class="form-control" id="email" name="email" type="text" placeholder="Введите email для тестирования" value="">
                                </div>
                                <p class="help-block">
                                    На указанный адрес будет отправлено письмо с результатами общего теста для текущего хостинга
                                </p>
                            </div>
                            <div class="modal-footer">
                                <!-- button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button -->
                                <button type="button" class="btn btn-default closer">Закрыть</button>
                                <button type="button" class="btn btn-primary sender">Отправить</button>
                                <input type="hidden" name="cmd" value="checkmail" />
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>
            </form>
            <!-- /modal mail checker -->

        </script>

        <script type="text/template" id="tpl_DBmodal">

            <!-- Modal db checker -->
            <form class="modalForm" id="dbChecker_form">
                <div class="modal fade" id="dbChecker" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <div class="modal-header">
                                <h4 class="modal-title" id="myModalLabel"></h4>
                            </div>
                            <div class="modal-body">
                                <div class="form-group has-feedback">
                                    <label class="control-label" for="host"></label>
                                    <input class="form-control" id="host" name="host" type="text" placeholder="Адрес сервера баз данных" value="localhost">
                                </div>

                                <div class="form-group has-feedback">
                                    <label class="control-label" for="user"></label>
                                    <input class="form-control" id="user" name="user" type="text" placeholder="Имя пользователя БД" value="">
                                </div>

                                <div class="form-group has-feedback">
                                    <label class="control-label" for="pass"></label>
                                    <input class="form-control" id="pass" name="pass" type="password" placeholder="Пароль пользователя БД" value="">
                                </div>

                                <!-- div class="form-group has-feedback">
                                    <label class="control-label" for="dbname"></label>
                                    <input class="form-control" id="dbname" name="dbname" type="text" placeholder="Имя БД" value="">
                                </div -->

                                <p class="help-block">
                                    Будет протестирована возможность соединения с сервером баз данных
                                </p>

                            </div>
                            <div class="modal-footer">
                                <!-- button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button -->
                                <button type="button" class="btn btn-default closer">Закрыть</button>
                                <button type="button" class="btn btn-primary sender">Проверить</button>
                                <input type="hidden" name="cmd" value="check_db" />
                            </div>
                        </div><!-- /.modal-content -->
                    </div><!-- /.modal-dialog -->
                </div>
            </form>
            <!-- /modal mail checker -->

        </script>

        <script type="text/template" id="tpl_response">
            <div class="alert alert-{%- status %}">{%- text %}</div>
        </script>

        <script src="<?= HOME_DOMAIN ?>js/application.js"></script>

        <title>Skewer - тестирование хостингов</title>

        <script>
            jQuery(function(){ init('<?php echo $_SERVER['PHP_SELF']; ?>') });
        </script>

    </head>
    <body>
    <?php $version = (int)file_get_contents(HOME_DOMAIN.VERSION_FILE) ?>
    <?php if(allowUrlFopen() and ($version != 0) and $version != VERSION): ?>


        <div class="jumbotron">
            <h1>Обновите тест</h1>
            <p>Версия вашего теста устарела. Вы используте версию: <?= VERSION ?></p>
            <p>Последняя актуальная версия: <?= $version ?></p>
         </div>
    <?php else: ?>

        <div id="app">
            <div  class="withlogo">
                <a href="<?php echo $_SERVER['PHP_SELF']; ?>"><img src="<?= HOME_DOMAIN ?>images/logo.png" /></a>
                <h3>Skewer - тестирование хостингов</h3>
            </div>
            <div>
                <ul class="nav nav-pills">
                    <li><a href="#!/reqparams"><span class="glyphicon glyphicon-list-alt"></span> Обязательные параметры</a></li>
                    <li><a href="#!/commontest"><span class="glyphicon glyphicon-list-alt"></span> Общий тест</a></li>
                    <li><a href="#!/checkmail"><span class="glyphicon glyphicon-envelope"></span> Проверить почтовый сервер</a></li>
                    <li><a href="#!/checkdb"><span class="glyphicon glyphicon-search"></span> Проверить MySQL</a></li>
                    <li><a href="#!/phpinfo"><span class="glyphicon glyphicon-list-alt"></span> Информация о PHP</a></li>
                </ul>
            </div>
            <div id="content"></div>
        </div>

    <?php endif ?>

    </body>
    </html>


<?
}

switch(getParam('cmd')) {

    default;
        init();
        break;

    case 'testitems':

        sendResponse(commonTest());

        break;
        
    case 'main-testitems':
        
        foreach($mainTest as &$item) {
        
           switch($item['method']){
                default:
                case '':
                    if ($item['test'] == 'php') {
                        if (version_compare(PHP_VERSION, MIN_PHPVERSION, '<')) {
                            $item['observed'] = 'error';
                        }
                        else if (version_compare(PHP_VERSION, OPT_PHPVERSION, '<')) {
                            $item['observed'] = 'min';
                        }
                    } else $item['observed'] = 'unknown';
                    
                    break;
                case true:
                    $item['observed'] = 'observed';
                    break;
                case false:

    
                    $item['observed'] = 'not-observed';
                    break;
           }
        }
        
        sendResponse($mainTest);
        break;

    case 'check_mail':

        if(!checkMailAddress(getParam('email'))) {
            sendResponse(array('status' => 'warning', 'text' => 'Введенный адрес не является правильным email адресом'));
            break;
        }

        if(checkMail(getParam('email'))) {

            sendResponse(array('status' => 'success', 'text' => 'Письмо успешно отправлено. Проверьте почтовый ящик,
            адрес которого вы указали.'));

        } else {
            sendResponse(array('status' => 'warning', 'text' => 'В процессе проверки работы системы отправки писем
            произошла ошибка.'));

        }

        break;

    case 'check_db':
        $host = getParam('host');
        $user = getParam('user');
        $pass = getParam('pass');
        $dbname = getParam('dbname');



        if(empty($host) OR empty($user) OR empty($pass) OR empty($dbname)) {

            sendResponse(array('status' => 'warning', 'text' => 'Данные для проверки имеют неправильный формат'));
            break;
        }

        if(!$result = checkDB(
            $host,
            $user,
            $pass,
            $dbname
        )) {

            sendResponse(array('status' => 'warning', 'text' => 'В процессе проверки работы БД
            произошла ошибка.'));

        } else {

            sendResponse($result);

        }



        break;

    case 'phpinfo':
        phpinfo();
        break;


}